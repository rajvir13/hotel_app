//============latests m4 code commited by mickey 10nov====== 

import AsyncStorage from "@react-native-community/async-storage";
import { NavigationContainer } from "@react-navigation/native";
import React, { Component } from "react";
import firebase from "react-native-firebase";
import { MenuProvider } from 'react-native-popup-menu';
import { Provider } from "react-redux";
import Store from "./src/redux/store";
import ScreenNavigator from "./src/routers/AppNavigator";

export default class App extends Component {
    componentDidMount() {
        // if (Platform.OS !== "android") {
            console.log("app class called")
            this.checkPermission();
        // }
    }

    componentWillUnmount() {
        this.checkPermission();
    }

    checkPermission = async () => {
        // const enabled = await firebase.messaging().hasPermission();
        // console.log("enabled status app class", enabled)
        // // If Premission granted proceed towards token fetch
        // if (enabled) {
        //     this.getToken();
        // } else {
        //     // If permission hasn’t been granted to our app, request user in requestPermission method.
        //     // this.requestPermission();
        // }
        
        firebase.messaging().hasPermission().then(granted => {
            if (granted) {
                firebase.messaging().getToken().then(token => {
                    if (token) 
                        // this.setState({token});
                        console.log('token>>>>>>>>=====',token)
                    })
            } else {
                firebase.messaging().requestPermission().then(() => {
                        // this.checkForNotificationsPermission();
                }).catch(err => {
                    //This is where I get the error:  "failed to grant permissions" (on err.message)
                })
            }
        })
    }

    getToken = async () => {
        let fcmToken = await AsyncStorage.getItem('fcmToken');
        if (!fcmToken) {
            fcmToken = await firebase.messaging().getToken();
            if (fcmToken) {
                // user has a device token
                console.warn('fcm==========>', fcmToken);
                await AsyncStorage.setItem('fcmToken', fcmToken);
            }
        }
    }

    requestPermission = async () => {
        // try {

        //     const authStatus = await firebase.messaging().requestPermission();
        //     const enabled =
        //         authStatus === firebase.messaging.AuthorizationStatus.AUTHORIZED || authStatus === firebase.messaging.AuthorizationStatus.PROVISIONAL;
        //     debugger
        //     if (enabled) {
        //         this.getToken();
        //         console.log('Authorization status:', authStatus);
        //     }
        //     else {
        //         this.requestPermission()
        //     }

        // } catch (error) {
        //     // User has rejected permissions
        //     console.log('error', error);
        //     console.log('app.js  class permission rejected');
        // }

        try {
            await firebase.messaging().requestPermission();
            // User has authorised
            this.getToken();
          } catch (error) {
            // User has rejected permissions
            console.log('error', error);
            console.log("permission rejected");
          }
    }

    render() {
        return (
            <Provider store={Store}>
                {/*<NetworkProvider>*/}
                <NavigationContainer>
                    <MenuProvider>
                        <ScreenNavigator
                            ref={(nav) => {
                                this.navigator = nav;
                            }}
                        />
                    </MenuProvider>
                </NavigationContainer>
                {/*</NetworkProvider>*/}
            </Provider>
        );
    }
}
