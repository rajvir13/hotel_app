// ----------------------------------------
// ----------------------------------------
// ContactUs
// ----------------------------------------

export const LOGIN_FAIL = "LOGIN_FAIL";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";

//register
export const REGISTER_SUCCESS = "REGISTER_SUCCESS";
export const REGISTER_FAIL = "REGISTER_FAIL";

// ----------------------------------------
// Forgot Password
// ----------------------------------------
export const FORGOT_FAIL = "FORGOT_FAIL";
export const FORGOT_SUCCESS = "FORGOT_SUCCESS";

//Social Login
export const SocialLogin_SUCCESS = "SocialLogin_SUCCESS";
export const SocialLogin_FAIL = "SocialLogin_FAIL";

// ----------------------------------------
// Get user profile
// ----------------------------------------
export const GET_PROFILE_FAIL = "GET_PROFILE_FAIL";
export const GET_PROFILE_SUCCESS = "GET_PROFILE_SUCCESS";



//Register Hotels

export const GET_HOTEL_REGISTER_SUCCESS="GET_HOTEL_REGISTER_SUCCESS";
export const GET_HOTEL_REGISTER_FAIL="GET_HOTEL_REGISTER_FAIL";

// ----------------------------------------
// Update user profile
// ----------------------------------------
export const UPDATE_PROFILE_FAIL = "UPDATE_PROFILE_FAIL";
export const UPDATE_PROFILE_SUCCESS = "UPDATE_PROFILE_SUCCESS";

// ----------------------------------------
// Owner Registration
// ----------------------------------------
export const OWNER_REG_NAME_FAIL = "OWNER_REG_NAME_FAIL";
export const OWNER_REG_NAME_SUCCESS = "OWNER_REG_NAME_SUCCESS";

//***********  Hotel Policy ******************* */

export const HOTEL_POLICY_SUCCESS="HOTEL_POLICY_SUCCESS";
export const HOTEL_POLICY_FAIL="HOTEL_POLICY_FAIL";



//*******************HOtel Register ******************** */

export const HOTEL_REGISTER_SUCCESS="HOTEL_REGISTER_SUCCESS";
export const HOTEL_REGISTER_FAIL="HOTEL_REGISTER_FAIL";

//******************* LOGOUT ******************** */

export const LOGOUT_FAIL = "LOGOUT_FAIL";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";

//============= Update hotel Address==========
export const UPDATE_HOTEL_ADDRESS_FAIL = "UPDATE_HOTEL_ADDRESS_FAIL";
export const UPDATE_HOTEL_ADDRESS_SUCESS = "UPDATE_HOTEL_ADDRESS_SUCESS";

//============= Update room details==========
export const UPDATE_ROOM_DETAILS_FAIL = "UPDATE_ROOM_DETAILS_FAIL";
export const UPDATE_ROOM_DETAILS_SUCESS = "UPDATE_ROOM_DETAILS_SUCESS";


//============= Update hotel policy==========
export const UPDATE_HOTEL_POLICY_FAIL = "UPDATE_HOTEL_POLICY_FAIL";
export const UPDATE_HOTEL_POLICY_SUCCESS = "UPDATE_HOTEL_POLICY_SUCESS";

//============= Near By hotels==========
export const NEAR_BY_HOTEL_FAIL = "NEAR_BY_HOTEL_FAIL";
export const NEAR_BY_HOTEL_SUCCESS = "NEAR_BY_HOTEL_SUCCESS";


//============= Save Hotel Details==========
export const SAVE_HOTEL_DETAIL_FAIL = "SAVE_HOTEL_DETAIL_FAIL";
export const SAVE_HOTEL_DETAIL_SUCCESS = "SAVE_HOTEL_DETAIL_SUCCESS";