import * as types from '../events'


//register submit reducer

const initialState = {
    responseRegister: undefined,

};
const RegisterReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.REGISTER_SUCCESS:
            return {...state, responseRegister: action.response};
        case types.REGISTER_FAIL:
            return {...state, responseRegister: action.error};
        default:
            return state
    }
};

export {
    RegisterReducer
}
