import * as types from '../events'


//update hotel policy reducer

const initialState = {
    HotelPolicyResponse: undefined,

};
const UpdateHotelPolicyReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.UPDATE_HOTEL_POLICY_SUCCESS :

            return {...state, HotelPolicyResponse: action.response};
        case types.UPDATE_HOTEL_POLICY_FAIL:
            return {...state, HotelPolicyResponse: action.error};
        default:
            return state
    }
};
export default UpdateHotelPolicyReducer