import * as types from '../events'


//update hotel address reducer

const initialState = {
    HotelAddressResponse: undefined,

};
const UpdateHotelAddressReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.UPDATE_HOTEL_ADDRESS_SUCESS :

            return {...state, HotelAddressResponse: action.response};
        case types.UPDATE_HOTEL_ADDRESS_FAIL:
            return {...state, HotelAddressResponse: action.error};
        default:
            return state
    }
};
export default UpdateHotelAddressReducer