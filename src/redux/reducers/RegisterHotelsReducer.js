import * as types from '../events'

const initialState = {
    hotelRoomResponse: undefined,

};
const RegisterHotelsReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.GET_HOTEL_REGISTER_SUCCESS:

            return {...state, hotelRoomResponse: action.response,};
        case types.GET_HOTEL_REGISTER_FAIL:
            return {...state, hotelRoomResponse: action.error};
        default:
            return state
    }
};

export default RegisterHotelsReducer
