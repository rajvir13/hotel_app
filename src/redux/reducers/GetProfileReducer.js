import * as types from '../events'

const initialState = {
    getProfileResponse: undefined,

};
const GetProfileReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.GET_PROFILE_SUCCESS:
            return {...state, getProfileResponse: action.response,};
        case types.GET_PROFILE_FAIL:
            return {...state, getProfileResponse: action.error};
        default:
            return state
    }
};

export default GetProfileReducer
