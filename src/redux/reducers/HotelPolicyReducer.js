import * as types from '../events'

const initialState = {
    hotelPolicyResponse: undefined,

};
const HotelPolicyReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.HOTEL_POLICY_SUCCESS:

            return {...state, hotelPolicyResponse: action.response,};
        case types.HOTEL_POLICY_FAIL:
            return {...state, hotelPolicyResponse: action.error};
        default:
            return state
    }
};

export default HotelPolicyReducer
