import * as types from '../events'

const initialState = {
    OwnerNameResponse: undefined,

};
const OwnerNameReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.OWNER_REG_NAME_SUCCESS:
            return {...state, OwnerNameResponse: action.response,};
        case types.OWNER_REG_NAME_FAIL:
            return {...state, OwnerNameResponse: action.error};
        default:
            return state
    }
};

export default OwnerNameReducer