import * as types from '../events'


//update hotel policy reducer

const initialState = {
    RoomDetailResponse: undefined,

};
const UpdateRoomDetailsReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.UPDATE_ROOM_DETAILS_SUCESS :

            return {...state, RoomDetailResponse: action.response};
        case types.UPDATE_ROOM_DETAILS_FAIL:
            return {...state, RoomDetailResponse: action.error};
        default:
            return state
    }
};
export default UpdateRoomDetailsReducer