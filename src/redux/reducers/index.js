import {combineReducers} from 'redux';
import LoginReducer from "./LoginReducer";
import ForgotReducer from './ForgotReducer'
import SocialLoginReducer from './SocialLoginReducer'
import GetProfileReducer from './GetProfileReducer'

import  {RegisterReducer} from './registerReducer';
import OwnerRegNameReducer from './OwnerNameReducer'
import HotelPolicyReducer from './HotelPolicyReducer'

import RegisterHotelsReducer from './RegisterHotelsReducer';
import LogoutReducer from "./Logout";

import UpdateHotelAddressReducer from '../reducers/UpdateHotelAddressReducer'
import UpdateHotelPolicyReducer from '../reducers/UpdateHotelPolicyReducer'
import UpdateRoomDetailsReducer from "./UpdateRoomDetails";

// const allReducers = Object.assign({}, RegisterReducer, RegisterCountryReducer,RegisterStateReducer,RegisterCityReducer);


const rootReducer = (state, action) => {
    if (action.type === "LOGOUT_SUCCESS") {
        state = undefined
    }
    return appReducer(state, action)
};

const appReducer = combineReducers({
    LoginReducer,
    RegisterReducer,
    ForgotReducer,
    SocialLoginReducer,
    GetProfileReducer,
    RegisterHotelsReducer,
    HotelPolicyReducer,
    OwnerRegNameReducer,
    LogoutReducer,
    UpdateHotelAddressReducer,
    UpdateHotelPolicyReducer,
    UpdateRoomDetailsReducer

});
export default rootReducer;
