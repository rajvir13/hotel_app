import * as types from '../events'


//register submit reducer

const initialState = {
    SocialResponse: undefined,

};
const SocialLoginReducer = (state = initialState, action) => {

    switch (action.type) {

        case types.SocialLogin_SUCCESS :

            return {...state, SocialResponse: action.response};
        case types.SocialLogin_FAIL:
            return {...state, SocialResponse: action.error};
        default:
            return state
    }
};
export default SocialLoginReducer