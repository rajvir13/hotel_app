import {API, BASE_URL} from "../constant";
import axios from "axios";

export const OwnerCancelPaindingBooking = (payload, ownerCancelPaindingBookingResponse) => {
    let formData = new FormData();
    formData.append("cancelled_by", payload.cancelled_by);
    formData.append("cancelled_by_date", payload.cancelled_by_date);

    axios.put(BASE_URL + API.OWNER_CANCEL_PAINDING_BOOKING + payload.bookingId + "/", formData, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer ' + payload.accessToken,
        }
    })
        .then((response) => {
            ownerCancelPaindingBookingResponse(response.data)
        })
        .catch((error) => {
            ownerCancelPaindingBookingResponse(error)
        }).done();
};
