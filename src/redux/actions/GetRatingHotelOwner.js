import {API, BASE_URL} from "../constant";

export const GetRatingHotelOwner = (payload, ratingResponse) => {
  
    let filterByurl = '';
    if (payload.star !== "" && payload.star !== "Select Stars") {
        filterByurl = BASE_URL + API.GET_OWNER_RATING + payload.hotelId + "/" + "?to=" + payload.toDate + "&from=" + payload.fromDate + "&stars=" + payload.star;
    } else {
        filterByurl = BASE_URL + API.GET_OWNER_RATING + payload.hotelId + "/" + "?to=" + payload.toDate + "&from=" + payload.fromDate;
    }
    console.log("rating payload is",payload)
    console.log("url is",filterByurl)
    fetch(filterByurl, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer ' + payload.accessToken,
        }
    })
        .then((response) => response.json())
        .then((responseData) => {
            ratingResponse(responseData)
        })
        .catch((error) => {
            ratingResponse(error)
        }).done()
    
};


export const SaveCustomerRating = (payload, onSaveRatingResponse) => {

    fetch(BASE_URL + API.SAVE_CUSTOMER_RATING, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer ' + payload.accessToken,
        },
        body: JSON.stringify({
            "hotel": payload.hotel_id,
            "rating": payload.rating,
            "provided_by": payload.customer_id,
            "comment": payload.comment,
            "booking": payload.booking_id
        })
    })
        .then((response) => response.json())
        .then((responseData) => {
            onSaveRatingResponse(responseData)
        })
        .catch((error) => {
            onSaveRatingResponse(error)
        }).done()
};



