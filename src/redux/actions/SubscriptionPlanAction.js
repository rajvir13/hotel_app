import {API,BASE_URL} from "../constant";
import axios from 'axios';
import * as types from '../events/index';


export const BasicSubsciptionAction = (payload, basicCommisionResponse) => {

    console.log("base commison called")

    fetch(BASE_URL + API.SUBSCRIPTION_FOR_BASIC_PLAN + payload.ownerId+ "/", {
        method: 'GET',
       headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer ' + payload.accessToken,
        }
    })
        .then((response) => response.json())
        .then((responseData) => {
            basicCommisionResponse(responseData)
        })
        .catch((error) => {
            basicCommisionResponse(error)
        }).done()
};


export const CommisionSubsciptionAction = (payload, CommisionResponse) => {

      console.log("payload commison",payload)
    fetch(BASE_URL + API.GET_COMMISSION_TO_PAY + payload.ownerId+ "/", {
        method: 'GET',
       headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer ' + payload.accessToken,
        }
    })
        .then((response) => response.json())
        .then((responseData) => {
            CommisionResponse(responseData)
        })
        .catch((error) => {
            CommisionResponse(error)
        }).done()
};


