import { API, BASE_URL } from "../constant";

export const OwnerPendingConfirmation = (
  payload,
  pendingConfirmationResponse
) => {

  fetch(BASE_URL + API.PENDING_CONFIRMATION + payload.ownerId + "/", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "bearer " + payload.accessToken,
    },
  })
    .then((response) => response.json())
    .then((responseData) => {
      pendingConfirmationResponse(responseData);
    })
    .catch((error) => {
      pendingConfirmationResponse(error);
    })
    .done();
};

export const OwnerBookedData = (payload, ownerBookedData) => {
  console.warn("booked is", payload);

  if (payload.month !== "" && payload.year !== "") {
    fetch(
      BASE_URL +
        API.BOOKED_DATA +
        payload.ownerId +
        "/" +
        "?month=" +
        payload.month +
        "&year=" +
        payload.year,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: "bearer " + payload.accessToken,
        },
      }
    )
      .then((response) => response.json())
      .then((responseData) => {
        ownerBookedData(responseData);
      })
      .catch((error) => {
        ownerBookedData(error);
      })
      .done();
  } else {
    fetch(BASE_URL + API.BOOKED_DATA + payload.ownerId + "/", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "bearer " + payload.accessToken,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        ownerBookedData(responseData);
      })
      .catch((error) => {
        ownerBookedData(error);
      })
      .done();
  }
};

export const OwnerCancelData = (payload, ownerCancelData) => {
  fetch(BASE_URL + API.CANCEL_DATA + payload.ownerId + "/", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "bearer " + payload.accessToken,
    },
  })
    .then((response) => response.json())
    .then((responseData) => {
      ownerCancelData(responseData);
    })
    .catch((error) => {
      ownerCancelData(error);
    })
    .done();
};

export const OwnerCheckOutData = (payload, ownerCheckOutData) => {
  if (payload.month !== "" && payload.year !== "") {
    fetch(
      BASE_URL +
        API.CHECK_OUT_DATA +
        payload.ownerId +
        "/" +
        "?month=" +
        payload.month +
        "&year=" +
        payload.year,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: "bearer " + payload.accessToken,
        },
      }
    )
      .then((response) => response.json())
      .then((responseData) => {
        ownerCheckOutData(responseData);
      })
      .catch((error) => {
        ownerCheckOutData(error);
      })
      .done();
  } else {
    fetch(BASE_URL + API.CHECK_OUT_DATA + payload.ownerId + "/", {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "bearer " + payload.accessToken,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        ownerCheckOutData(responseData);
      })
      .catch((error) => {
        ownerCheckOutData(error);
      })
      .done();
  }
};

/**----------------Owner Checking Out Booking------------------- */

export const OwnerCheckingOutBooking = (payload, ownerCheckingOut) => {
  fetch(BASE_URL + API.OWNER_PUT_CHECK_OUT + payload.bookingId + "/", {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: "bearer " + payload.accessToken,
    },
  })
    .then((response) => response.json())
    .then((responseData) => {
      ownerCheckingOut(responseData);
    })
    .catch((error) => {
      ownerCheckingOut(error);
    })
    .done();
};
