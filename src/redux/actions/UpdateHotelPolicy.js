import { API, BASE_URL} from '../constant';
import * as types from '../events/index';
import axios from "axios";

export const UpdateHotelPolicyAction = (payload) => {
 console.log("payload is",payload)
    return function (dispatch) {
        let data = new FormData();
        data.append("check_in", payload.inDate24Hour);
        data.append("check_out", payload.outDate24Hour);
        data.append("description", payload.GeneralInstruction);
        console.log("payload is",payload)
       if(payload.pdfFileName !== "")
       {

        data.append('file', {
            name: payload.pdfFile.name,
            uri: payload.pdfFile.uri,
            type: payload.pdfFile.type
        });
    }
        axios.put(BASE_URL + API.UPDATE_HOTEL_POLICY + payload.hotelId + "/" , data, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `${"Bearer" + " " + payload.authToken}`
            }
        })
            .then((response) => {
                dispatch(UpdateHotelPolicySussess(response.data))
            })
            .catch((error) => {
                dispatch(UpdateHotelPolicyFail(error))
            }).done();
    }
};
export const UpdateHotelPolicySussess = (responseData) => {
    return {
        type: types.UPDATE_HOTEL_POLICY_SUCCESS,
        response: responseData
    }
};
export const UpdateHotelPolicyFail = (error) => {
    return {
        type: types.UPDATE_HOTEL_POLICY_FAIL,
        error: error.message
    }
};
