import axios from "axios";
import {API} from "../constant";
import * as types from "../events";

export const LogoutAction = (payload) => {
    return function (dispatch) {

        fetch(API.BASE_URL + API.LOGOUT, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `${"Bearer" + " " + payload.accessToken}`,
            }
        })
            .then((response) => response.json())
            .then((responseData) => {
                dispatch(LogoutSuccess(responseData))
            })
            .catch((error) => {
                dispatch(LogoutFail(error))

            }).done();
    }
};

export const LogoutSuccess = (responseData) => {

    return {
        type: types.LOGOUT_SUCCESS,
        response: responseData
    }
};
export const LogoutFail = (error) => {

    return {
        type: types.LOGOUT_FAIL,
        error: error.message
    }
};
