import {
    API, DeviceType,
} from '../constant/index';

import * as types from '../events/index';
import axios from 'axios';
import {BASE_URL} from "../constant";



export const GetCustomerGivenRating = (payload, ratingResponse) => {
    console.warn("payload is",payload);
     fetch(BASE_URL + API.GET_CUSTOMER_HOTEL_RATING + payload.customerId+ "/", {
         method: 'GET',
        headers: {
             'Content-Type': 'application/json',
             'Authorization': 'bearer ' + payload.accessToken,
         }
     })
         .then((response) => response.json())
         .then((responseData) => {
             ratingResponse(responseData)
         })
         .catch((error) => {
             ratingResponse(error)
         }).done()
 };
 