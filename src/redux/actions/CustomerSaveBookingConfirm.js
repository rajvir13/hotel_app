import { API, BASE_URL } from "../constant";
import * as types from "../events/index";
import axios from "axios";

export const CustomerSaveBookingConfirm = (
  payload,
  OwnerSaveBookingResponse
) => {
  let data;
console.log("confirm bookign payload is",payload)
  if (payload.no_of_hours !== null && payload.no_of_hours !== undefined) {
    data = {
      // no_of_hours: payload.no_of_hours,
      check_in_time: payload.check_in_time,
      no_of_days:0
    };
  } else {
    data = {
      no_of_days: payload.no_of_days,
      check_in_time: payload.check_in_time,
    };
  }
console.log("data is",data)
  fetch(BASE_URL + API.SAVE_CONFIRM_BOOKING_DETAILS + payload.id + "/", {
    method: "PUT",
    headers: {
      "Content-Type": "application/json",
      Authorization: `${"bearer" + " " + payload.accessToken}`,
    },
    body: JSON.stringify(data),
  })
    .then((response) => response.json())
    .then((responseData) => {
      console.warn("confirm booking response is----------", responseData);
      OwnerSaveBookingResponse(responseData);
    })
    .catch((error) => {
      console.warn("error is----------", error);

      OwnerSaveBookingResponse(error);
    })
    .done();
};
