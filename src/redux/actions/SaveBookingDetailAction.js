import { API, BASE_URL, DeviceType } from "../constant/index";
import * as types from "../events/index";



export const SaveBookingDetailAction = (payload, SaveBookingDetailResponse) => {
  fetch(BASE_URL + API.SAVE_BOOKING_DETAIL , {
    method: "POST",
      headers: {
          'Content-Type': 'application/json',
          'Authorization': 'bearer ' + payload.accessToken,
      },
      body: JSON.stringify({
        room: payload.room,
        hotel: payload.hotel,
        customer: payload.customer,
      }),
  }) 
  .then((response) => response.json())
  .then((responseData) => {
    SaveBookingDetailResponse(responseData)
  })
  .catch((error) => {
    SaveBookingDetailResponse(error)
  }).done()
};
