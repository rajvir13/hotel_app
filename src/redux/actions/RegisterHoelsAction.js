import {API, DeviceType} from "../constant/index";

import * as types from "../events/index";
import axios from 'axios';
import {BASE_URL} from "../constant";


export const RegisterHotelsAction = (payload) => {
    //modified code
    let item_data = [];
    for (let index = 0; index < payload.data.length; index++) {
       
       if(payload.data[index].RoomType === 6)
       {
        item_data.push({
            room_type: parseInt(payload.data[index].RoomType),
            no_of_guest: payload.data[index].Numberofguest,
            regular_price: payload.data[index].Regular_price,
            discounted_price: parseInt(payload.data[index].Discount_Price),
            room_services: [],
            room_images: [],
            no_of_hours:Number(payload.data[index].timeSlot)

        });
       }
       else{

        item_data.push({
            room_type: parseInt(payload.data[index].RoomType),
            no_of_guest: payload.data[index].Numberofguest,
            regular_price: payload.data[index].Regular_price,
            discounted_price: parseInt(payload.data[index].Discount_Price),
            room_services: [],
            room_images: [],

        });
       }
       
        payload.data[index].AddComplimentery_service.map((item, index1) => {

            item_data[index].room_services.push(item);
        });


        //push images
        payload.data[index].RoomImages.map((item1, index1) => {
            item_data[index].room_images.push(item1.completeData.completeData.data);
        });
    }

    //checked
    let checked = [];
    payload.checked.map((item2, index2) => {
        checked.push(item2);
    });

//hotel service
    let hotelServices = [];
    payload.otherService.map((item3, index3) => {
        hotelServices.push(item3);
    });

    return function (dispatch) {
        // let formdata = new FormData();
        // formdata.append("hotel_amenities", checked);
        // formdata.append("hotel_services", hotelServices);
        // formdata.append("rooms", item_data);

        let formData = JSON.stringify({
            "hotel_amenities": checked,
            "hotel_services": hotelServices,
            "rooms": item_data
        })

        //end nisha code

        axios.put(BASE_URL + API.Register_Hotels + payload.HotelId + "/",
            JSON.stringify({
                "hotel_amenities": checked,
                "hotel_services": hotelServices,
                "rooms": item_data
            }), {
                headers: {
                    "Content-Type": "application/json",
                    'Authorization': `${'Bearer ' + payload.accessToken}`,
                },
            })
            .then((responseData) => {
                dispatch(RegisterHotelSuccess(responseData.data));
            })
            .catch((error) => {
                dispatch(RegisterHotelFail(error));
            })
            .done();
    };
};

export const RegisterHotelSuccess = (responseData) => {
    return {
        type: types.GET_HOTEL_REGISTER_SUCCESS,
        response: responseData,
    };
};
export const RegisterHotelFail = (error) => {
    return {
        type: types.GET_HOTEL_REGISTER_FAIL,
        error: error.message,
    };
};


export const AddHotelRoomsAction = (payload, addRoomResponse) => {
    
    //modified code
    let item_data = [];
    // let dataLength = payload.data.length;
    for (let index = 0; index < payload.data.length; index++) {
       
       if(payload.data[index].RoomType === 6)
       {
        item_data.push({
            room_type: parseInt(payload.data[index].RoomType),
            no_of_guest: payload.data[index].Numberofguest,
            regular_price: payload.data[index].Regular_price,
            discounted_price: parseInt(payload.data[index].Discount_Price),
            room_services: [],
            room_images: [],
            no_of_hours:Number(payload.data[index].timeSlot)
            

        });
       }
       else
       {
        item_data.push({
            room_type: parseInt(payload.data[index].RoomType),
            no_of_guest: payload.data[index].Numberofguest,
            regular_price: payload.data[index].Regular_price,
            discounted_price: parseInt(payload.data[index].Discount_Price),
            room_services: [],
            room_images: [],
          
            

        });
       }
       
        
        payload.data[index].AddComplimentery_service.map((item, index1) => {

            item_data[index].room_services.push(item);
        });


        //push images
        payload.data[index].RoomImages.map((item1, index1) => {
            item_data[index].room_images.push(item1.completeData.completeData.data);
        });
    }

    //checked
    let checked = [];
    payload.checked.map((item2, index2) => {
        checked.push(item2);
    });

//hotel service
    let hotelServices = [];
    payload.otherService.map((item3, index3) => {
        hotelServices.push(item3);
    });


    //
    // let formData = JSON.stringify({
    //     "hotel_amenities": checked,
    //     "hotel_services": hotelServices,
    //     "rooms": item_data
    // });

    //end nisha code

    axios.put(BASE_URL + API.Register_Hotels + payload.HotelId + "/",
        JSON.stringify({
            "hotel_amenities": checked,
            "hotel_services": hotelServices,
            "rooms": item_data
        }), {
            headers: {
                "Content-Type": "application/json",
                'Authorization': `${'Bearer ' + payload.accessToken}`,
            },
        })
        .then((responseData) => {
            addRoomResponse(responseData.data);
        })
        .catch((error) => {
            addRoomResponse(error);
        })
        .done();
};
