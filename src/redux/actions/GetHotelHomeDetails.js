import {API, BASE_URL} from "../constant";

export const HotelHomeDetailAction = (payload, hotelHomeDetailResponse) => {
    let url= BASE_URL + API.GET_HOTEL_HOME + payload.id + "/"
    console.log("url is",url)
    fetch(url, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + payload.accessToken,
        }
    })
        .then((response) => response.json())
        .then((responseData) => {
            hotelHomeDetailResponse(responseData)
        })
        .catch((error) => {
            hotelHomeDetailResponse(error)
        }).done()
};
