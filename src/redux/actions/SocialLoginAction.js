/**
 * Created by Mickey on 30/04/18.
 */

import {
    API,
    BASE_URL, DeviceType,
} from '../constant/index';

export const SocialLoginAction = (payload, socialResponse) => {
  

    fetch(BASE_URL + API.SOCIAL_LOGIN, {
        method: 'POST',
        headers:
            {
                'Content-Type': 'application/json',
                'Authorization': `${'Bearer ' + payload.accessToken}`,
            },
        body: JSON.stringify(
            {
                "username": payload.username,
                "device_id": payload.device_id,
                "device_token": payload.deviceToken,
                "device_type": DeviceType,
                "social_id": payload.social_id,
            })
    })
        .then((response) => response.json())
        .then((responseData) => {
            socialResponse(responseData)
        })
        .catch((error) => {
            socialResponse(error)
        }).done();
};
