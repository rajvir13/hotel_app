import {API, BASE_URL} from "../constant";

export const GetCustomerBooking = (payload, customerPaindingBookingResponse) => {

    if (payload.month !== "" && payload.year !== "") {
        let url = BASE_URL + API.GET_PAINDING_BOOKING_CUSTOMER + payload.customerId + "/" + "?month=" + payload.month + "&year=" + payload.year;
        fetch(url, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "bearer " + payload.accessToken,
            },
            // body: formData,
        })
            .then((response) => response.json())
            .then((responseData) => {
                customerPaindingBookingResponse(responseData);
            })
            .catch((error) => {
                customerPaindingBookingResponse(error);
            })
            .done();
    } else {
        let urlData = BASE_URL + API.GET_PAINDING_BOOKING_CUSTOMER + payload.customerId + "/";
        fetch(urlData, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
                Authorization: "bearer " + payload.accessToken,
            },
            // body: formData,
        })
            .then((response) => response.json())
            .then((responseData) => {
                console.warn("res-------", responseData);
                customerPaindingBookingResponse(responseData);
            })
            .catch((error) => {
                console.warn("error--------", error);
                customerPaindingBookingResponse(error);
            })
            .done();
    }
};
