import { API, BASE_URL} from '../constant';
import * as types from '../events/index';
import axios from "axios";

export const ForgotAction = (payload) => {
    return function (dispatch) {
        let data = new FormData();
        data.append("email", payload.emailText);

        axios.post(BASE_URL + API.FORGOT_PASSWORD, data, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `${"Bearer" + " " + payload.accessToken}`
            }
        })
            .then((response) => {
                dispatch(ForgotSuccess(response.data))
            })
            .catch((error) => {
                dispatch(ForgotFail(error))
            }).done();
    }
};
export const ForgotSuccess = (responseData) => {
    return {
        type: types.FORGOT_SUCCESS,
        response: responseData
    }
};
export const ForgotFail = (error) => {
    return {
        type: types.FORGOT_FAIL,
        error: error.message
    }
};


export const TokenAuthAction = (payload, tokenAuthResponse) => {
    // fetch(BASE_URL + API.FORGOT_PASSWORD, {
    //     method: 'POST',
    //     headers: {
    //         'Content-Type': 'application/json',
    //         'Authorization': `${"Bearer" + " " + payload.accessToken}`,
    //     },
    //     body: data
    // })
    //     .then((response) => response.json())
    //     .then((responseData) => {
    //         dispatch(ForgotSuccess(responseData))
    //     })
    //     .catch((error) => {
    //         dispatch(ForgotFail(error))
    //     }).done();
};
