import {API, BASE_URL} from "../constant";

export const NearByAction = (payload, nearByResponse) => {

    console.warn("payload is",payload)
    fetch(BASE_URL + API.NEAR_BY_HOTELS + payload.lat + "/" + payload.long + "/", {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + payload.accessToken,
        }
    })

    
        .then((response) => response.json())
        .then((responseData) => {
            nearByResponse(responseData)
        })
        .catch((error) => {
            nearByResponse(error)
        }).done()
};
