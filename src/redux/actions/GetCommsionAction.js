import { API, BASE_URL } from "../constant";
import axios from "axios";
import * as types from "../events/index";

export const GetCommsionAction = (payload, commisonResponse) => {
  fetch(BASE_URL + API.OWNER_COMMISION + payload.ownerId + "/", {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "bearer " + payload.accessToken,
    },
  })
    .then((response) => response.json())
    .then((responseData) => {
      commisonResponse(responseData);
    })
    .catch((error) => {
      commisonResponse(error);
    })
    .done();
};

export const GetCommsionPaidAction = (payload, paidCommisonResponse) => {
  console.log("payload paid is", payload);

  let url =
    BASE_URL +
    API.PAID_COMMISION +
    payload.ownerId +
    "/" +
    "?month=" +
    payload.month +
    "&year=" +
    payload.year;

  console.log("************", url);
  fetch(url, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: "bearer " + payload.accessToken,
    },
  })
    .then((response) => response.json())
    .then((responseData) => {
      console.log("responseData", responseData);
      paidCommisonResponse(responseData);
    })
    .catch((error) => {
      console.log("error", error);

      paidCommisonResponse(error);
    })
    .done();
};
