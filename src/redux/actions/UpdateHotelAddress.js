import {API, BASE_URL} from "../constant";
import * as types from "../events/index";
import axios from "axios";

export const UpdateHotelAction = (payload) => {
    return function (dispatch) {
        // let data = new FormData();
        let base64Images = [];
        payload.newAddedImages.map((item) => {
            base64Images.push(item.completeData.data)
        });
        let data = undefined;
        if (payload.deletedImages.length !== 0) {
            data = JSON.stringify({
                "first_name": payload.ownerName,
                "last_name": payload.ownerLastName,
                "name": payload.hotelName,
                "address": payload.hotelAddress,
                "latitude": payload.valueLat,
                "longitude": payload.valueLng,
                "image": base64Images,
                "hotel_images": payload.deletedImages,
            })
        } else {
            data = JSON.stringify({
                "first_name": payload.ownerName,
                "last_name": payload.ownerLastName,
                "name": payload.hotelName,
                "address": payload.hotelAddress,
                "latitude": payload.valueLat,
                "longitude": payload.valueLng,
                "image": base64Images,
            })
        }

      

        axios.put(BASE_URL + API.UPDATE_HOTEL_ADDRESS + payload.hotelID + "/", data,
            {
                headers: {
                    "Content-Type":
                        "application/json",
                    Authorization:
                        `${"Bearer" + " " + payload.authToken}`,
                }
                ,
            }
        )
            .then((response) => {
                dispatch(UpdateHotelAddressSussess(response.data));
            })
            .catch((error) => {
                dispatch(UpdateHotelAddressFail(error));
            })
            .done();
    };
};
export const UpdateHotelAddressSussess = (responseData) => {
    return {
        type: types.UPDATE_HOTEL_ADDRESS_SUCESS,
        response: responseData,
    };
};
export const UpdateHotelAddressFail = (error) => {
    return {
        type: types.UPDATE_HOTEL_ADDRESS_FAIL,
        error: error.message,
    };
};


