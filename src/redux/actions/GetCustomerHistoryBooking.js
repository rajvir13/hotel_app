import {API, BASE_URL} from "../constant";

export const GetCustomerHistoryBooking = (payload, customerHistoryBookingResponse) => {
    
  if(payload.month !=="" && payload.year !== "")
  {
    fetch(BASE_URL + API.CUSTOMER_BOOKING_HISTORY + payload.customerId + "/" + "?month=" +payload.month + "&year=" + payload.year, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer ' + payload.accessToken,
        },
        // body: formdata
    })
        .then((response) => response.json())
        .then((responseData) => {
            customerHistoryBookingResponse(responseData)
        })
        .catch((error) => {
            customerHistoryBookingResponse(error)
        }).done()

  }
  else{
    fetch(BASE_URL + API.CUSTOMER_BOOKING_HISTORY + payload.customerId + "/" , {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer ' + payload.accessToken,
        },
        // body: formdata
    })
        .then((response) => response.json())
        .then((responseData) => {
            customerHistoryBookingResponse(responseData)
        })
        .catch((error) => {
            customerHistoryBookingResponse(error)
        }).done()

  }

   
};
