/**
 * Created by Nisha-React on 4/23/20.
 */

import {
    API, DeviceType, BASE_URL
} from '../constant/index';
import axios from 'axios';

export const TokenAuthAction = (payload, tokenAuthResponse) => {
    let data = new FormData();
    data.append("client_id", "g6w9EOTdDgHvwkIPQDZNRN1ZV9exZAnESAcc0CRu");
    data.append("client_secret", "ri7mXNDeagK7CMGBiVu0gi9MKwjEyJGCQdq44yCwMbXWK1xmvrKgMTy1dTKTqzO9JcnC4RmaSnZX2Bmim9eniPEU8uFDltQKa2aX73zWeiqLKiyLv7Se8zXZq5uazrSL");
    data.append("username", "appuser@admin.com");
    data.append("password", "admin@123");
    data.append("grant_type", "password");

    axios.post(BASE_URL + API.TOKEN_AUTH, data, {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        }
    })
        .then((response) => {
            tokenAuthResponse(response.data)
        })
        .catch((error) => {
            tokenAuthResponse(error)

        }).done();
};
