/**
 * Created by Rajnish-React on 8/23/18.
 */

import {
    API,
    BASE_URL, DeviceType,
} from '../constant/index';

import * as types from '../events/index';


// ***************** Register action  **********************
export const RegisterAction = (payload) => {
    return function (dispatch) {
        let data;
        if (payload.social_id === undefined) {
            data = JSON.stringify({
                "first_name": payload.first_name,
                "last_name": payload.last_name,
                "password": payload.password,
                "mobile_number": payload.mobile_number,
                "city": payload.city,
                "state": payload.state,
                "country": payload.country,
                "email": payload.email,
                "device_id": payload.device_id,
                "device_token": payload.deviceToken,
                "device_type": DeviceType,
                "country_code": payload.callingCode
            })
        } else {
            data = JSON.stringify({
                "first_name": payload.first_name,
                "last_name": payload.last_name,
                "password": payload.password,
                "mobile_number": payload.mobile_number,
                "city": payload.city,
                "state": payload.state,
                "country": payload.country,
                "email": payload.email,
                "device_id": payload.device_id,
                "device_token": payload.deviceToken,
                "device_type": DeviceType,
                "social_id": payload.social_id,
                "registeration_type": payload.registeration_type,
                "country_code": payload.callingCode
            })
        }


        fetch(BASE_URL + API.REGISTER_USER, {
            method: 'POST',
            headers:
                {
                    'Content-Type': 'application/json',
                    'Authorization': `${'Bearer ' + payload.accessToken}`,
                },
            body: data
        })
            .then((response) => response.json())
            .then((responseData) => {
                dispatch(RegisterSuccess(responseData))
            })
            .catch((error) => {
                dispatch(RegisterFail(error))
            }).done();
    }
};
export const RegisterSuccess = (responseData) => {
    return {
        type: types.REGISTER_SUCCESS,
        response: responseData
    }
};
export const RegisterFail = (error) => {
    return {
        type: types.REGISTER_FAIL,
        error: error.message
    }
};


/************** Country list *************/

export const RegisterCountryAction = (payload, countryListResponse) => {
    fetch(BASE_URL + API.GET_COUNTRY_LIST, {
        method: 'GET',
        headers: {
            'Authorization': `${'Bearer ' + payload.accessToken}`
        },
    })
        .then((response) => response.json())
        .then((responseData) => {
            countryListResponse(responseData)
        })
        .catch((error) => {
            countryListResponse(error)
        }).done();
};

/************* State list *************/

export const RegisterStateAction = (payload, stateListResponse) => {
    fetch(BASE_URL + API.GET_STATE_LIST + `${payload.id}/`, {
        method: 'GET',
        headers: {
            'Authorization': `${'Bearer ' + payload.accessToken}`
        },
    })
        .then((response) => response.json())
        .then((responseData) => {
            stateListResponse(responseData)
        })
        .catch((error) => {
            stateListResponse(error)
        }).done();
};

/************* City list *************/

export const RegisterCityAction = (payload, cityListResponse) => {
    fetch(BASE_URL + API.GET_CITY_LIST + `${payload.id}/`, {
        method: 'GET',
        headers: {
            'Authorization': `${'Bearer ' + payload.accessToken}`
        },
    })
        .then((response) => response.json())
        .then((responseData) => {
            cityListResponse(responseData)
        })
        .catch((error) => {
            cityListResponse(error)
        }).done();
};

/************* Email Verify *************/


export const EmailVerifyAction = (payload, emailVerifyResponse) => {
    fetch(BASE_URL + API.EMAIL_VERIFY, {
        method: 'POST',
        headers: {
            'Authorization': `${'Bearer ' + payload.accessToken}`,
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "email": payload.email
        })
    })
        .then((response) => response.json())
        .then((responseData) => {
            emailVerifyResponse(responseData)
        })
        .catch((error) => {
            emailVerifyResponse(error)
        }).done();
};

/************* mobile Verify *************/


export const MobileVerifyAction = (payload, mobileVerifyResponse) => {
    fetch(BASE_URL + API.PHONE_VERIFY, {
        method: 'POST',
        headers: {
            'Authorization': `${'Bearer ' + payload.accessToken}`,
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({
            "mobile_number": payload.phoneNumber
        })
    })
        .then((response) => response.json())
        .then((responseData) => {
            mobileVerifyResponse(responseData)
        })
        .catch((error) => {
            mobileVerifyResponse(error)
        }).done();
};
