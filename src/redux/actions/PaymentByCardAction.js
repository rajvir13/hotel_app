import { API, BASE_URL } from "../constant";
import * as types from "../events/index";
import axios from "axios";

export const PaymentByCard = (payload, paymentResponse) => {


  fetch(BASE_URL + API.PAYMENT_BY_CARD, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "bearer " + payload.accessToken,
    },
    body: JSON.stringify({
      card_holder_name: payload.card_holder_name,
      card_number: payload.card_number,
      expiry_mm: payload.expiry_mm,
      expiry_yy: payload.expiry_yy,
      cvc: payload.cvc,
      amount: payload.amount,
      user_id: payload.user_id,
      commission_amount: payload.commission_amount,
      plan_amount: payload.plan_amount,
    }),
  })
    .then((response) => response.json())
    .then((responseData) => {
      paymentResponse(responseData);
    })
    .catch((error) => {
      paymentResponse(error);
    })
    .done();
};
