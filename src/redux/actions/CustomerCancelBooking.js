
import { API, BASE_URL } from "../constant";
import * as types from "../events/index";
import axios from "axios";

export const CustomerCancelBooking = (payload, OwnerCancelBookingResponse) => {
  
  console.log("cancel payload is",payload)
  
  fetch(BASE_URL + API.CUSTOMER_BOOKING_CANCELD + payload.id + "/", {
    method: "PUT",
    headers: {
        "Content-Type": "application/json",
         Authorization: `${"bearer" + " " + payload.accessToken}`,
    },
    body: JSON.stringify({
        cancelled_by: payload.cancelled_by,
        cancelled_by_date: payload.cancelled_by_date,
    }),
  })
    .then((response) => response.json())
    .then((responseData) => {
        console.warn("canecl booking response is----------", responseData);
      OwnerCancelBookingResponse(responseData);
    })
    .catch((error) => {
        console.warn("error is----------", error);

      OwnerCancelBookingResponse(error);
    })
    .done();
};
