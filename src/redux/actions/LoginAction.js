/**
 * Created by Rajnish-React on 8/23/18.
 */

import {
    API, DeviceType,
} from '../constant/index';

import * as types from '../events/index';

export const LoginAction = (payload) => {
    console.warn("apiiiiii==>", payload);
    return function (dispatch) {
        fetch(API.BASE_URL + API.LOGIN, {
            method: 'POST',
            headers:
                {
                    'Content-Type': 'application/json',
                    'Authorization':  `${'Bearer ' + payload.accessToken}`,
                },
            body: JSON.stringify(
                {
                    "username": payload.email,
                    "password": payload.password,
                    "device_id": payload.device_id,
                    "device_token": payload.deviceToken,
                    "device_type": DeviceType,
                })

        })
            .then((response) => response.json())
            .then((responseData) => {

                dispatch(LoginSuccess(responseData))
            })
            .catch((error) => {

                dispatch(LoginFail(error))
            }).done();
    }
};


export const LoginSuccess = (responseData) => {

    return {
        type: types.LOGIN_SUCCESS,
        response: responseData
    }
};
export const LoginFail = (error) => {

    return {
        type: types.LOGIN_FAIL,
        error: error.message
    }
};

