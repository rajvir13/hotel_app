
import {
    API, DeviceType,
} from '../constant/index';

import * as types from '../events/index';
import axios from 'axios';
import {BASE_URL} from "../constant";

export const HotelPolicyAction = (payload) => {
        return function (dispatch) {
            let formdata = new FormData();
            formdata.append("check_in", payload.check_in);
            formdata.append("check_out", payload.check_out);
            formdata.append("description", payload.description);

            if(payload.file !== undefined)
            {
                formdata.append('file', {
                    name: payload.file.name,
                    uri: payload.file.uri,
                    type: payload.file.type
                });
            }

            axios.put(BASE_URL + API.Add_Hotl_Policy + payload.hotelId + "/", formdata,{
                headers: {
                  'Authorization': `${'bearer ' + payload.accessToken}`,
                  'Content-Type': 'application/json',
                },
              })
            .then((responseData) => {
                dispatch(HotelPolicySuccess(responseData.data))
            })
            .catch((error) => {

                dispatch(HotelPolicyFail(error))
            }).done();
        }
    };


export const HotelPolicySuccess = (responseData) => {

    return {
        type: types.HOTEL_POLICY_SUCCESS,
        response: responseData
    }
};
export const HotelPolicyFail = (error) => {

    return {
        type: types.HOTEL_POLICY_FAIL,
        error: error.message
    }
};


