import {API} from "../constant";

export const GetHotelDetailAction = (payload, hotelDetailResponse) => {
   
   console.log("payload is11",payload)
    let url = "";
    if (payload.customerId !== undefined && payload.customerId !== null) {
        url = API.BASE_URL + API.HOTEL_DETAIL + payload.id + "/" + "?customer_id=" + payload.customerId
    } else {
        url = API.BASE_URL + API.HOTEL_DETAIL + payload.id + "/"
    }
  
    fetch(url, {
        method: 'GET',
        headers:
            {
                'Content-Type': 'application/json',
                'Authorization': `${"Bearer" + " " + payload.accessToken}`,
            },
    })
        .then((response) => response.json())
        .then((responseData) => {
            hotelDetailResponse(responseData)
        })
        .catch((error) => {
            hotelDetailResponse(error)
        }).done();
};


export const GetHotelRoomDetailAction = (payload, hotelRoomDetailResponse) => {
    let url=""
    if(payload.customerId !== undefined && payload.customerId!==null)
    {
         url=API.BASE_URL + API.HOTEL_ROOM_DETAIL + payload.id + "/" +"?customer_id="+payload.customerId 

    }
    else
    {
         url=API.BASE_URL + API.HOTEL_ROOM_DETAIL + payload.id + "/" 

    }
    fetch(url, {
        method: 'GET',
        headers:
            {
                'Content-Type': 'application/json',
                'Authorization': `${"bearer" + " " + payload.accessToken}`,
            },
    })
        .then((response) => response.json())
        .then((responseData) => {
            console.log("res is",responseData)
            hotelRoomDetailResponse(responseData)
        })
        .catch((error) => {
            hotelRoomDetailResponse(error)
        }).done();
};
