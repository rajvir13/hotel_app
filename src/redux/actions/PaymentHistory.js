import {API,BASE_URL} from "../constant";
import axios from 'axios';
import * as types from '../events/index';



export const PaymentHistoryAction = (payload, paymentHistoryResponse) => {
    console.log("payload0----------",payload)
    fetch(BASE_URL + API.PAYMENT_HISTORY_ACTION + payload.ownerId+ "/", {
        method: 'GET',
       headers: {
            'Content-Type': 'application/json',
            'Authorization': 'bearer ' + payload.accessToken,
        }
    })
        .then((response) => response.json())
        .then((responseData) => {
            paymentHistoryResponse(responseData)
        })
        .catch((error) => {
            paymentHistoryResponse(error)
        }).done()
};
