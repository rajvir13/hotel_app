import {
    API, DeviceType,
} from '../constant/index';

import * as types from '../events/index';
import axios from 'axios';
import {BASE_URL} from "../constant";

export const GetProfileAction = (payload) => {
    return function (dispatch) {
        fetch(API.BASE_URL + API.GET_PROFILE, {
            method: 'GET',
            headers:
                {
                    'Content-Type': 'application/json',
                    'Authorization': `${"Bearer" + " " + payload.loginToken}`,
                },
        })
            .then((response) => response.json())
            .then((responseData) => {
                dispatch(GetProfileSuccess(responseData))
            })
            .catch((error) => {
                dispatch(GetProfileFail(error))
            }).done();
    }
};

export const UpdateProfileAction = (payload) => {
    return function (dispatch) {
        let data = new FormData();
        if (payload.isChangedValue) {
            data.append("first_name", payload.firstNameText);
            data.append("last_name", payload.lastName);
            data.append("city", payload.cityId);
            data.append("state", payload.stateId);
            data.append("country", payload.countryId);
            data.append("profile_image", payload.profileImage.data);
        } else {
            data.append("first_name", payload.firstNameText);
            data.append("last_name", payload.lastName);
            data.append("city", payload.cityId);
            data.append("state", payload.stateId);
            data.append("country", payload.countryId);
        }

        axios.put(BASE_URL + API.UPDATE_PROFILE + payload.userId + '/', data, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `${"Bearer" + " " + payload.loginToken}`,
            }
        })
            .then((response) => {
                dispatch(GetProfileSuccess(response.data))
            })
            .catch((error) => {
                dispatch(GetProfileFail(error))

            }).done();
    }
};

export const GetProfileSuccess = (responseData) => {

    return {
        type: types.GET_PROFILE_SUCCESS,
        response: responseData
    }
};
export const GetProfileFail = (error) => {

    return {
        type: types.GET_PROFILE_FAIL,
        error: error.message
    }
};
