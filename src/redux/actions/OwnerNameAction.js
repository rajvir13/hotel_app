import {API, BASE_URL, DeviceType} from "../constant/index";

import * as types from "../events/index";
import axios from "axios";

export const OwnerRegNameAction = (payload) => {
    return function (dispatch) {
        let data;
        if (payload.social_id === undefined) {
            let imagesResponse = [];
            payload.imagess.map((item, index) => {
                imagesResponse.push(item.base64Data);
            });
            data = JSON.stringify({
                "first_name": payload.ownerFirstName,
                "last_name": payload.ownerLastName,
                "password": payload.passwordText,
                "mobile_number": payload.phoneNoText,
                "email": payload.emailText,
                "country_code": payload.callingCode,
                "name": payload.hotelName,
                "address": payload.hotelAddress,
                "latitude": payload.lat,
                "longitude": payload.lng,
                "user_type": payload.user_type,
                "device_token": payload.deviceToken,
                "device_id": payload.deviceId,
                "device_type": DeviceType,
                "image": imagesResponse,
                "description": payload.description
            })
        } else {
            let imagesResponse = [];
            payload.imagess.map((item, index) => {
                imagesResponse.push(item.base64Data);
            });
            data = JSON.stringify({
                "first_name": payload.ownerFirstName,
                "last_name": payload.ownerLastName,
                "password": payload.passwordText,
                "mobile_number": payload.phoneNoText,
                "email": payload.emailText,
                "country_code": payload.callingCode,
                "name": payload.hotelName,
                "address": payload.hotelAddress,
                "latitude": payload.lat,
                "longitude": payload.lng,
                "user_type": payload.user_type,
                "device_token": payload.deviceToken,
                "device_id": payload.deviceId,
                "device_type": DeviceType,
                "image": imagesResponse,
                "registeration_type": payload.registeration_type,
                "social_id": payload.social_id
            })
        }

        axios.post("https://hotel.appsndevs.com/api/accounts/register_hotel_owner/", data, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `${"Bearer" + " " + payload.authToken}`
            }
        })
            .then((responseData) => {
                dispatch(OwnerRegNameActionSuccess(responseData.data));
            })
            .catch((error) => {
                dispatch(OwnerRegNameActionFail(error));
            }).done();
    };
};

export const OwnerRegNameActionSuccess = (responseData) => {
    return {
        type: types.OWNER_REG_NAME_SUCCESS,
        response: responseData,
    };
};
export const OwnerRegNameActionFail = (error) => {
    return {
        type: types.OWNER_REG_NAME_FAIL,
        error: error.message,
    };
};

