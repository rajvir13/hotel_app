import { API, BASE_URL } from "../constant";
import * as types from "../events/index";
import axios from "axios";

export const UpdateRoomDetailAction = (payload) => {
  return function (dispatch) {
    let base64Images = [];
    payload.imagesArray.map((item) => {
      base64Images.push(item.completeData.data);
    });
    let data = undefined;
    if (payload.deletedImages.length !== 0) {
      
      if (payload.data[0].room_type === 6) {
        data = JSON.stringify({
          room_type: payload.data[0].room_type,
          no_of_guest: payload.data[0].no_of_guest,
          regular_price: payload.data[0].regular_price,
          discounted_price: parseInt(payload.data[0].discounted_price),
          room_services: payload.data[0].room_service,
          room_images: payload.deletedImages,
          image: base64Images,
          no_of_hours: Number(payload.data[0].no_of_hours),
        });
      }
       else {
        data = JSON.stringify({
          room_type: payload.data[0].room_type,
          no_of_guest: payload.data[0].no_of_guest,
          regular_price: payload.data[0].regular_price,
          discounted_price: parseInt(payload.data[0].discounted_price),
          room_services: payload.data[0].room_service,
          room_images: payload.deletedImages,
          image: base64Images,
         
        });
      }
    } else {
      if (payload.data[0].room_type === 6) {
        data = JSON.stringify({
          room_type: payload.data[0].room_type,
          no_of_guest: payload.data[0].no_of_guest,
          regular_price: payload.data[0].regular_price,
          discounted_price: parseInt(payload.data[0].discounted_price),
          room_services: payload.data[0].room_service,
          image: base64Images,
          no_of_hours: Number(payload.data[0].no_of_hours),
        });
      } else {
        data = JSON.stringify({
          room_type: payload.data[0].room_type,
          no_of_guest: payload.data[0].no_of_guest,
          regular_price: payload.data[0].regular_price,
          discounted_price: parseInt(payload.data[0].discounted_price),
          room_services: payload.data[0].room_service,
          image: base64Images,
         
        });
      }
    }

    console.log("data update",data)

    axios
      .put(BASE_URL + API.UPDATE_ROOM_DETAIL + payload.id + "/", data, {
        headers: {
          "Content-Type": "application/json",
          Authorization: `${"Bearer" + " " + payload.accessToken}`,
        },
      })
      .then((response) => {
        dispatch(UpdateRoomDetailSussess(response.data));
      })
      .catch((error) => {
        dispatch(UpdateRoomDetailsFail(error));
      })
      .done();
  };
};
export const UpdateRoomDetailSussess = (responseData) => {
  return {
    type: types.UPDATE_ROOM_DETAILS_SUCESS,
    response: responseData,
  };
};
export const UpdateRoomDetailsFail = (error) => {
  return {
    type: types.UPDATE_ROOM_DETAILS_FAIL,
    error: error.message,
  };
};

/**=======================Delete Room======================*/

export const DeleteRoomAction = (payload, deleteRoomResponse) => {
  axios
    .put(BASE_URL + API.DELETE_ROOM + payload.id + "/", "", {
      headers: {
        "Content-Type": "application/json",
        Authorization: "Bearer " + payload.accessToken,
      },
    })
    .then((response) => {
      deleteRoomResponse(response.data);
    })
    .catch((error) => {
      deleteRoomResponse(error);
    })
    .done();
};
