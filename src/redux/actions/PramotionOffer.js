import { API, DeviceType } from "../constant/index";

import * as types from "../events/index";
import axios from "axios";
import { BASE_URL } from "../constant";

export const GetPramotionOffer = (payload, pramotionResponse) => {
  console.log("payload promotions", payload.month);

  if (payload.month !== "" && payload.year !== "") {
    let url =
      BASE_URL +
      API.GET_PRAMOTION +
      payload.idForCustomer +
      "/" +
      "?month=" +
      payload.month +
      "&year=" +
      payload.year;
    fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "bearer " + payload.accessToken,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        console.warn("response of notificaton data is", responseData);
        pramotionResponse(responseData);
      })
      .catch((error) => {
        pramotionResponse(error);
      })
      .done();
  }
   else {
    let url = BASE_URL + API.GET_PRAMOTION + payload.idForCustomer + "/";
console.warn("url is",url)
    fetch(url, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: "bearer " + payload.accessToken,
      },
    })
      .then((response) => response.json())
      .then((responseData) => {
        console.warn("response of notificaton data is", responseData);
        pramotionResponse(responseData);
      })
      .catch((error) => {
        pramotionResponse(error);
      })
      .done();
  }
};
