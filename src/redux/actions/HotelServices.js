import {
    API, DeviceType,
} from '../constant/index';

import * as types from '../events/index';
export const GetRoomTypeAction = (payload, handleRoomTypeApi) => {
  console.log("pay",payload)
    fetch(API.BASE_URL + API.GET_ROOM_TYPES, {
        method: 'GET',
        headers:
            {
                'Content-Type': 'application/json',
                'Authorization': `${'Bearer ' + payload.authToken}`,
            },

    })
        .then((response) => response.json())
        .then((responseData) => {
            handleRoomTypeApi(responseData)
        })
        .catch((error) => {
            handleRoomTypeApi(error)
        }).done();

};


//********************************* Get Hotel Aminities type ************************** */
export const GetHotelAminitiesAction = (payload, handleHotelAminitiesApi) => {
    fetch(API.BASE_URL + API.GET_HOTEL_AMINITIES, {
        method: 'GET',
        headers:
            {
                'Content-Type': 'application/json',
                'Authorization': `${'Bearer ' + payload.authToken}`,

            },
    })
        .then((response) => response.json())
        .then((responseData) => {
            handleHotelAminitiesApi(responseData)
        })
        .catch((error) => {
            handleHotelAminitiesApi(error)
        }).done();

};


//********************************* end hotel aminities ******************************/

//********************************* get all hotel services ******************************/


export const GetAllHotelServiceAction = (payload, handleGetAllHotelServiceApi) => {


    fetch(API.BASE_URL + API.GET_HOTEL_SERVICE, {
        method: 'GET',
        headers:
            {
                'Content-Type': 'application/json',
                'Authorization': `${'Bearer ' + payload.authToken}`,

            },

    })
        .then((response) => response.json())
        .then((responseData) => {
            handleGetAllHotelServiceApi(responseData)
        })
        .catch((error) => {
            handleGetAllHotelServiceApi(error)
        }).done();

};


//********************************* end get all hotel services ******************************/


export const GetRoomComplemantryServiceAction = (payload, handleGetAllHotelServiceApi) => {

    fetch(API.BASE_URL + API.Get_ROOM_COMPLEMENTARYSERVICE, {
        method: 'GET',
        headers:
            {
                'Content-Type': 'application/json',
                'Authorization': `${'Bearer ' + payload.authToken}`,

            },

    })
        .then((response) => response.json())
        .then((responseData) => {
            handleGetAllHotelServiceApi(responseData)
        })
        .catch((error) => {
            handleGetAllHotelServiceApi(error)
        }).done();

};


