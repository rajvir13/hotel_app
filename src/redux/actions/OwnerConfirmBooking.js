import { API, BASE_URL } from "../constant";
import axios from "axios";
export const OwnerConfirmBooking = (payload, OwnerConfirmBookingResponse) => {
  
  console.log("payload is",payload)
    let data;
 if (payload.no_of_days !== null) {
    data= {
      verified_code: payload.verified_code,
      booking_id: payload.booking_id,
      no_of_days: payload.no_of_days,
      check_in_time: payload.check_in_time,
    };
  } else {
    data= {
      verified_code: payload.verified_code,
      booking_id: payload.booking_id,
      // no_of_days: payload.no_of_days,
      no_of_hours: payload.no_of_hours,
      check_in_time: payload.check_in_time,
    };
  }

  fetch(BASE_URL + API.HOTEL_VERIFY_OTP, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: "bearer " + payload.accessToken,
    },
    body: JSON.stringify(data),
  })
    .then((response) => response.json())
    .then((responseData) => {
      OwnerConfirmBookingResponse(responseData);
    })
    .catch((error) => {
      OwnerConfirmBookingResponse(error);
    })
    .done();
};
