import {API, BASE_URL} from "../constant";

export const OwnerChangePasswordAction = (payload, ownerPasswordResponse) => {
    fetch(BASE_URL + API.CHANGE_PASSWORD + payload.id + "/", {
        method: 'PUT',
        body: JSON.stringify({password: payload.password}),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + payload.accessToken,
        }
    })
        .then((response) => response.json())
        .then((responseData) => {
            ownerPasswordResponse(responseData)
        })
        .catch((error) => {
            ownerPasswordResponse(error)
        }).done()
};
