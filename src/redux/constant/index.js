// ----------------------------------------
// ----------------------------------------
// Base Url
// ----------------------------------------
import {Platform} from 'react-native';

module.exports.BASE_URL = "https://hotel.appsndevs.com";

// Local URL
// module.exports.BASE_URL = "http://10.8.18.54:7084";

// ----------------------------------------
// ----------------------------------------
// Device Type
// ----------------------------------------
export const DeviceType = Platform.OS === 'ios' ? 2 : 1;
// ----------------------------------------
// ----------------------------------------
// End Points
// ----------------------------------------
export const API = {

    IMAGE_BASE_URL:'https://hotel.appsndevs.com/media/',
    IMAGE_BASE_URL1:'https://hotel.appsndevs.com',


    BASE_URL :"https://hotel.appsndevs.com",
    TOKEN_AUTH: '/o/token/',
    // LOGIN: "v1/login",
    GET_COUNTRY_LIST : '/api/app/countries/',
    GET_STATE_LIST: '/api/app/states/',
    GET_CITY_LIST : '/api/app/cities/',
    REGISTER_USER : '/api/accounts/register/',
    SOCIAL_LOGIN : '/api/accounts/get_social_user/',
    EMAIL_VERIFY: '/api/accounts/emailverify/',
    PHONE_VERIFY: '/api/accounts/validatemobile/',

   // LOGIN: "v1/login",
    LOGIN:"/api/accounts/signin/",
    FORGOT_PASSWORD:"/api/accounts/forget_password/",
    GET_ROOM_TYPES: "/api/hotel/get_all_room_types/",
    GET_HOTEL_AMINITIES : '/api/hotel/get_all_hotel_amenities/',
    GET_HOTEL_SERVICE : '/api/hotel/get_all_hotel_services/',
    Get_ROOM_COMPLEMENTARYSERVICE:'/api/hotel/get_all_room_complimentory/',
    Register_Hotels:'/api/hotel/add_hotel_room_detail/',
    Add_Hotl_Policy: '/api/hotel/update_hotel_policy_documents/',

    LOGOUT: '/api/accounts/signout/',


    ///Owner
    OWNER_REG_NAME_ADDRESS : '/api/accounts/register_hotel_owner/',
    HOTEL_DETAIL: '/api/hotel/get_hotel_details/',
    HOTEL_ROOM_DETAIL: '/api/hotel/get_hotel_room_details/',
    UPDATE_HOTEL_ADDRESS:'/api/hotel/update_hotel_address/',
    UPDATE_ROOM_DETAIL:'/api/hotel/update_hotel_room_detail/',
    UPDATE_HOTEL_POLICY:'/api/hotel/update_hotel_policy_documents/',
    GET_HOTEL_HOME: '/api/hotel/get_all_counts/',
    CHANGE_PASSWORD:'/api/accounts/update_password/',
    DELETE_ROOM:"/api/hotel/delete_hotel_room/",
    PENDING_CONFIRMATION:"/api/hotel/get_pending_bookings/",
    BOOKED_DATA: '/api/hotel/get_confirm_bookings/',
    CANCEL_DATA: '/api/hotel/get_cancelled_bookings/',
    CHECK_OUT_DATA: '/api/hotel/get_checkout_bookings/',
    OWNER_PUT_CANCEL:'/api/hotel/save_booking_cancelled/',
    OWNER_PUT_CHECK_OUT:'/api/hotel/save_booking_checkout/',

    // Customer
    GET_PROFILE:"/api/accounts/get_customer_profile/",
    UPDATE_PROFILE:"/api/accounts/update_customer_profile/",
    NEAR_BY_HOTELS: '/api/hotel/get_near_by_hotel/',
    PARTICULAR_HOTEL_DETAIL: '/api/hotel/get_hotel_details/',
    SAVE_CUSTOMER_RATING:'/api/hotel/save_rating/?',


    //save booking detail

    SAVE_BOOKING_DETAIL: '/api/hotel/save_booking_details/',

    HOTEL_VERIFY_OTP:'/api/hotel/verify_otp/',
    CUSTOMER_BOOKING_CANCELD:'/api/hotel/save_booking_cancelled/',
    GET_PAINDING_BOOKING_CUSTOMER:'/api/hotel/get_pending_bookings_customer/',
    SAVE_CONFIRM_BOOKING_DETAILS:'/api/hotel/save_confirm_booking_details/',
    CUSTOMER_BOOKING_HISTORY:'/api/hotel/get_booking_history/',
    OWNER_CANCEL_PAINDING_BOOKING:'/api/hotel/save_booking_cancelled/',
    OWNER_COMMISION:'/api/hotel/get_commisions/',
    SUBSCRIPTION_PLAN:'/api/subscription/get_all_subscription_plans/',
    GET_OWNER_RATING :'/api/hotel/get_rating/',
    GET_CUSTOMER_HOTEL_RATING:'/api/hotel/get_customer_rating/',
    GET_PRAMOTION:'/api/subscription/offers_promotions_list/',
    PAYMENT_HISTORY_ACTION:'/api/hotel/get_payment_history/',
    PAYMENT_BY_CARD:'/api/subscription/verify_card_details/',
    SUBSCRIPTION_FOR_BASIC_PLAN:"/api/accounts/get_user_subscription_details/",
    GET_COMMISSION_TO_PAY:'/api/hotel/get_commisions_to_pay/',
    PAID_COMMISION:'/api/hotel/get_commisions_paid/'



};
