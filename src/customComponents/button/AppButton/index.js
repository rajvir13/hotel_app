import React from "react";
import {TouchableOpacity} from "react-native";
import {MainContainer, PrimaryButtonView, ButtonTextContainer} from "./styles";
import COLORS from "../../../utils/Colors";

const PrimaryButton = ({btnText, onPress, color, textColor, disabled, containerColor}) => {

    return (
        <MainContainer>
            <TouchableOpacity disabled={disabled === undefined ? false : disabled}
                              style={{
                                  justifyContent: 'center',
                                  alignItems: 'center',
                                  backgroundColor: containerColor
                              }}
                              activeOpacity={0.8}
                              onPress={onPress}>
                <PrimaryButtonView style={{backgroundColor: color ? color : COLORS.app_theme_color}}>
                    <ButtonTextContainer
                        style={{color: textColor ? textColor : COLORS.white_color}}>{btnText}</ButtonTextContainer>
                </PrimaryButtonView>
            </TouchableOpacity>
        </MainContainer>
    )
};

export {
    PrimaryButton
}
