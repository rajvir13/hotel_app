import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {FONT} from "../../../utils/FontSizes";
import styled from 'styled-components/native';

const MainContainer = styled.View`
backgroundColor: white;
`;

const PrimaryButtonView = styled.View`
justifyContent: center;
flex-Direction: row;
alignItems: center;
borderRadius: 8;
paddingVertical: ${wp("3%")};
width: ${wp("55%")};
        borderRadius: 24;
`;

const ButtonTextContainer = styled.Text`
font-size:${FONT.TextMedium}
`;

export {
    MainContainer,
    PrimaryButtonView,
    ButtonTextContainer
}
