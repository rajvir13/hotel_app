import React from 'react'
import {AccessToken, GraphRequest, GraphRequestManager, LoginManager} from 'react-native-fbsdk'
import {ICONS} from "../../../utils/ImagePaths";
import {TouchableOpacity,Image,Alert} from "react-native";

class FacebookService {
    constructor() {
        this.accessToken = undefined;
    }

    facebookLoginButton(callback) {
        return (
            <TouchableOpacity onPress={() => {
                try {
                    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
                        function (result) {
                            if (result.isCancelled) {
                                console.log('LoginReducer cancelled');
                            } else {
                                console.log(
                                    'LoginReducer success with permissions: ' +
                                    result.grantedPermissions.toString(),
                                );
                                return AccessToken.getCurrentAccessToken().then(
                                    (data) => {
                                        const responseInfoCallback = async (error, result) => {
                                            if (error) {
                                                console.warn(error);
                                            } else {
                                                // create a new firebase credential with the token
                                                // login with credential
                                                callback(result)
                                            }
                                        };
                                        const infoRequest = new GraphRequest(
                                            '/me',
                                            {
                                                accessToken: data.accessToken,
                                                parameters: {
                                                    fields: {
                                                        string: 'email,name,first_name,middle_name,last_name,picture.type(large)',
                                                    },
                                                },
                                            },
                                            responseInfoCallback,
                                        );
                                        // Start the graph request.
                                        new GraphRequestManager().addRequest(infoRequest).start();
                                    });
                            }
                        },
                        function (error) {
                            Alert.alert("please check your internet Connection")
                            console.log('LoginReducer fail with error: ' + error);
                        });
                } catch (e) {
                    console.warn("eee===>", e)
                }

            }}>
                <Image source={ICONS.FACEBOOK_ICON} resizeMode={'contain'}/>
            </TouchableOpacity>

        )
    }
}

export const facebookService = new FacebookService();
