import {GoogleSignin, statusCodes} from "@react-native-community/google-signin";
import {TouchableOpacity, Image,Alert} from "react-native";
import {ICONS} from "../../../utils/ImagePaths";
import React from "react";

class GoogleService {
    googleLogin (callBack) {
        return (
            <TouchableOpacity onPress={async () => {
                try {
                    await GoogleSignin.hasPlayServices();
                    const userInfo = await GoogleSignin.signIn();
                    callBack(userInfo);
                } catch (error) {
                    callBack(error);
                    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
                        console.log('userInfo error1==>', error);
                        // user cancelled the login flow
                    } else if (error.code === statusCodes.IN_PROGRESS) {
                        console.log('userInfo error2==>', error);
                        // operation (f.e. sign in) is in progress already
                    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
                        console.log('userInfo error3==>', error);
                        // play services not available or outdated
                    } else {
                        // some other error happened
                        Alert.alert("please check your internet Connection")

                        console.log('userInfo error4==>', error);
                    }
                }
            }}>
                <Image source={ICONS.GOOGLE_ICON} resizeMode={'contain'}/>
            </TouchableOpacity>
        )

    };
}

export const googleService = new GoogleService();
