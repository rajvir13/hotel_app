import React, { Component } from 'react';
import styles from './styles'
import { View, FlatList, TouchableWithoutFeedback, ImageBackground, Image } from 'react-native';
import COLORS from "../../utils/Colors";
import { ICONS } from '../../utils/ImagePaths'
import ImagePicker from 'react-native-image-crop-picker';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'

let imagesList = []
let refresh = false
let selected = false
const ImagePickerComponent = (props) => {
    return (
        <View style={{ flexDirection: "row" }}>
            <FlatList
                horizontal
                style={{ padding: wp('5%'), }}
                data={imagesList}
                renderItem={this._renderImageList}
                ItemSeparatorComponent={this._itemSeparator}
                extraData={refresh}
                keyExtractor={(item, index) => index}
                selected={selected}
            />
            <TouchableWithoutFeedback onPress={() => this.AddImageAction()}>
                <ImageBackground style={styles.imgOuterView}>
                    <TouchableWithoutFeedback onPress={() => this.AddImageAction()}>
                        <Image style={{ height: 40, width: 40, alignSelf: 'center' }} source={ICONS.ADD_IMAGE_ICON} />
                    </TouchableWithoutFeedback>
                </ImageBackground>
            </TouchableWithoutFeedback>
        </View>
    );
};
/*   // =============================================================================================
   // Render method for Listview
   // =============================================================================================
   */

_renderImageList = ({ item, index }) => (
    <Image style={styles.imageStyle}
        source={item !== undefined ? item : ''} />

)

// =============================================================================================
// Click on Row of FlatList //// separator
// =============================================================================================

AddImageAction = () => {
    ImagePicker.openPicker({
        multiple: true,
        maxFiles: 10
    })
        .then(response => {
            let tempArray = []
            imagesList: response
            response.forEach((item) => {
                let image = {
                    uri: item.path,
                }
                tempArray.push(image)
                imagesList: tempArray
                // console.log('savedimageuri====='+item.path);

            })

        })
        refresh : !refresh

}
_itemSeparator = () => (<View style={{ width: wp('5%') }} />);

ImagePicker.defaultProps = {

};
export default ImagePickerComponent;
