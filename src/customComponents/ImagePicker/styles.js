import React from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { StyleSheet, Dimensions } from 'react-native';
import {FONT} from "../../utils/FontSizes";
import COLORS from "../../utils/Colors";

const styles = StyleSheet.create({
    
    containerStyle: {
        marginVertical:wp(0.5),
        width:wp('100%'),
    },
    imagesContainer:{
        justifyContent: 'center',flexDirection:"row"
    },
    imgOuterView:{
        height: wp('20%'),
        width: wp('20%'),
        borderRadius: wp('20%') / 2,
        padding:wp('1%'),
        backgroundColor:'lightgray',
        justifyContent:"center"
    },
    imageStyle: {
        resizeMode: "stretch",
        height: wp('20%'),
        width: wp('20%'),
        borderRadius: wp('20%') / 2,
    },
})
export default styles;