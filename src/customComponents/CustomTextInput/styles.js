import React from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { StyleSheet, Dimensions } from 'react-native';
import {FONT} from "../../utils/FontSizes";
import COLORS from "../../utils/Colors";

const styles = StyleSheet.create({
    inputStyle: {
        color: COLORS.black_color,
        fontSize: FONT.TextSmall_2,
        // fontFamily: 'Montserrat',
        paddingHorizontal:wp(2),
        paddingVertical:wp(3),
    },
    containerStyle: {
        marginVertical:wp(0.5),
        width:wp('85%'),
    },
})
export default styles;
