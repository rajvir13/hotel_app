import React, {Component} from 'react';
import styles from './styles'
import {TextInput, View} from 'react-native';
import {ShadowViewContainer} from "../../utils/BaseStyle";
import COLORS from "../../utils/Colors";

const RenderInput = ({moveToNext, inputvalue, isEditable, isPointerEvent, isSecure, ipOnChangeText, placeholder, returnKeyType, keyboardType, nextRef, maxLength}) => {
    const {inputStyle, containerStyle} = styles;
    return (
        <ShadowViewContainer style={containerStyle}>
            <TextInput
                maxLength={maxLength}
                placeholderTextColor={COLORS.placeholder_color}
                autoCorrect={false}
                placeholder={placeholder}
                style={inputStyle}
                value={inputvalue}
                onChangeText={ipOnChangeText}
                returnKeyType={returnKeyType}
                onSubmitEditing={() => moveToNext && moveToNext.focus()}
                keyboardType={keyboardType}
                pointerEvents={isPointerEvent}
                editable={isEditable}
                secureTextEntry={isSecure}
                ref={nextRef}
              
            />
        </ShadowViewContainer>
    );
};

RenderInput.defaultProps = {
    isEditable: true,
    isPointerEvent: 'auto',
    isSecure: false
};
export default RenderInput;
