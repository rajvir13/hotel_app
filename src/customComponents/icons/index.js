import React from "react";
import {Image, TouchableWithoutFeedback, View} from "react-native";
import {ICONS, HEADER_ICONS} from '../../utils/ImagePaths';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import Icon from 'react-native-vector-icons/Entypo';
import COLORS from "../../utils/Colors";
import LogoutComponent from "../../../assets/images/TabView/LogoutSVG";
import HotelListSVG from "../../../assets/images/CustomerHome/HotelListingSVG";
import LocationIconSVG from "../../../assets/images/CustomerHome/LocationIconSVG";

const LeftIcon = ({onPress, drawer}) => {
    return (
        <TouchableWithoutFeedback onPress={onPress} hitSlop={{top: 20, bottom: 20, left: 30, right: 30}}>
            <View>
                {drawer ?
                    <Image
                        resizeMode={'contain'}
                        style={{
                            width: wp(8),
                            height: wp(8),
                            marginLeft: 5,
                            tintColor: COLORS.greyButton_color2
                        }}
                        source={HEADER_ICONS.MENU_ICON}/>

                    : <Image resizeMode={'contain'}
                             style={{width: 25, height: 25, marginLeft: 5}}
                             source={HEADER_ICONS.BACK_ICON}/>
                }
            </View>
        </TouchableWithoutFeedback>
    )
};

const RightIcon = ({onPress}) => {
    return (
        <TouchableWithoutFeedback onPress={onPress} hitSlop={{top: 20, bottom: 20, left: 30, right: 30}}>
            <View>
                <LogoutComponent fillColor={COLORS.black_color}/>
            </View>
        </TouchableWithoutFeedback>
    )
};

const PaymentHistoryIcon = ({onPress}) => {
    return (
        <TouchableWithoutFeedback onPress={onPress} hitSlop={{top: 20, bottom: 20, left: 30, right: 30}}>
            <View>
            <Image source={ICONS.paymentHistory} style={{width: 30, height: 30}} />  
            </View>
        </TouchableWithoutFeedback>
    )
};
const DeleteIcon =({onPress})=>{

    return (
        <TouchableWithoutFeedback onPress={onPress} hitSlop={{top: 20, bottom: 20, left: 30, right: 30}}>
            <View>
                {/* <LogoutComponent fillColor={COLORS.black_color}/> */}
                <Image source={ICONS.DeleteIcon} style={{width: 30, height: 30}} />  

            </View>
        </TouchableWithoutFeedback>
    )

}

const ListingIcon = ({onPress}) =>{
    return (
        <TouchableWithoutFeedback onPress={onPress} hitSlop={{top: 20, bottom: 20, left: 30, right: 30}}>
            <View>
                <HotelListSVG fillColor={COLORS.black_color}/>
            </View>
        </TouchableWithoutFeedback>
    )
}

const LocationIcon = ({onPress}) =>{
    return (
        <TouchableWithoutFeedback onPress={onPress} hitSlop={{top: 20, bottom: 20, left: 30, right: 30}}>
            <View>
                <LocationIconSVG fillColor={COLORS.black_color}/>
            </View>
        </TouchableWithoutFeedback>
    )
};


// const SecondRightIcon = ({onPress}) => {
//     return (
//         <TouchableWithoutFeedback onPress={onPress} hitSlop={{top: 20, bottom: 20, left: 30, right: 30}}>
//             <View style={{flexDirection:'row',width:wp('20%'),justifyContent:'space-evenly'}}>
//                 <LogoutComponent fillColor={COLORS.black_color}/>
//                 </View>
//                 </TouchableWithoutFeedback>
//     )
// };


const OwnerRightIcon = ({onPress}) => {
    return (
        <TouchableWithoutFeedback onPress={onPress} hitSlop={{top: 20, bottom: 20, left: 30, right: 30}}>
            <View>
                <Image
                    resizeMode={'contain'}
                    style={{
                        width: wp(8),
                        height: wp(8),
                        marginRight: 5,
                        tintColor: COLORS.greyButton_color2
                    }}
                    source={HEADER_ICONS.OWNER_EDIT_ICON}/>
            </View>
        </TouchableWithoutFeedback>
    )
};


export {
    LeftIcon,
    RightIcon,
    OwnerRightIcon,
    ListingIcon,
    LocationIcon,
    DeleteIcon,
    PaymentHistoryIcon
    
    // SecondRightIcon
}
