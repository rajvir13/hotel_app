import React from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  headerView: {
    height: wp('12%'), width: wp('100%'),
    alignContent: 'center', alignContent: 'center', 
    justifyContent: 'center',backgroundColor:'#F1F1F1'
  },
  horizontalView: {
    height: wp('12%'), width: wp('100%'), alignItems: 'center',
    alignContent: 'center', flexDirection: 'row', paddingVertical: 5, 
    padding: 5, justifyContent: "space-between"
  },
  headerText: {
    alignSelf: 'center', marginVertical: 5,
    fontStyle: "normal",
    fontWeight: "600",
    fontSize: 18,
    lineHeight: 27,
    display: "flex",
    alignItems: "center", color: '#5A5A5A'
  },
  backImage: {
    height: wp('6%'), width: wp('6%'), alignSelf: 'center',
    marginVertical:7,
  }

});

export default styles;