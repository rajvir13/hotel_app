// local import
import {
    Image,
    View, Text, TouchableOpacity
} from 'react-native';
import styles from './styles'
// package import
import React, { Component } from 'react';
const HeaderComponent = (props) => {
    const RightIcon = () => {
        return ((props.showMenu) ? <TouchableOpacity style={{ alignSelf: 'center' }} onPress={() => props.hamPress()}>
            <Image source={require('../../../assets/images/back.png')} style={styles.backImage} />
        </TouchableOpacity> : <View />);
    }
    return (
        <View style={styles.headerView}>
            <View style={styles.horizontalView}>
                <TouchableOpacity style={{ alignSelf: 'center' }} onPress={() => props.backPress()}>
                    <Image source={require('../../../assets/images/back.png')} style={styles.backImage} />
                </TouchableOpacity>
                <Text style={styles.headerText}>{props.headerTitle}</Text>
                <RightIcon />
            </View>
        </View>
    )
}
export default HeaderComponent
