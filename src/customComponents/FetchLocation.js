import {
    Platform, PermissionsAndroid,
} from 'react-native'
import Geolocation from "@react-native-community/geolocation";

class CurrentLocation {
    async fetchLocation(callBack) {
        if (Platform.OS === 'ios') {
         await Geolocation.requestAuthorization();
            Geolocation.getCurrentPosition(
                (position) => {
                    console.log("position in ios is",position)
                    callBack(position)
                },
                (error) => {
                    console.warn(error.message);
                    callBack(error)
                },
                {enableHighAccuracy: true, timeout: 20000, maximumAge: 3000},
            );
        } else {
            try {
                const granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    callBack(true)
                } else {
                    console.warn("permission", PermissionsAndroid.RESULTS.GRANTED)
                }
            } catch (err) {
                console.warn("permissionError", err)
            }
        }
    }
}

export const currentLocation = new CurrentLocation();
