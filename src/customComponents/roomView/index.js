import React, { Component } from "react";
import {
  Modal,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import propTypes from "prop-types";
import { Input } from "react-native-elements";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import COLORS from "../../utils/Colors";
import { FONT } from "../../utils/FontSizes";
import { FONT_FAMILY } from "../../utils/Font";
import FastImage from "react-native-fast-image";
import { Spacer } from "../../customComponents/Spacer";
import CrossComponent from "../../../assets/images/BookingIcons/CrossSVG";

import { ICONS } from "../../utils/ImagePaths";
import {
  SafeAreaViewContainer,
  ShadowViewContainer,
} from "../../utils/BaseStyle";
import BaseClass from "../../utils/BaseClass";
import styles from "./styles";
import { API } from "../../redux/constant";

export default class RoomView extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      modelData: props.value,
      userName: this.props.userName,
      comesFromCancelBooking: this.props.comesFromCancelBooking,
    };
  }

  closeModal = () => {
    this.props.closeModal();
  };



getAdditionalDiscount=(regularPrice)=>{

  
  return (Number(regularPrice) * 40)/100
    }
  render() {
    const { modelData, userName } = this.state;

    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.isModalVisible}
      >
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          behavior={Platform.OS === "ios" ? "padding" : null}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              Keyboard.dismiss();
            }}
          >
            <View style={styles.centeredView}>
              <View
                style={{
                  backgroundColor: "white",
                  width: wp("90%"),
                  height: hp("60%"),
                }}
              >
                <FastImage
                  style={styles.roomImageView}
                  source={{
                    uri:
                      modelData.room_images !== null &&
                      modelData.room_images.image !== null
                        ? API.IMAGE_BASE_URL + modelData.room_images.image
                        : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                  }}
                  resizeMode={FastImage.resizeMode.cover}
                />
                <View
                  style={{
                    flexDirection: "row",
                    marginTop: wp("5%"),
                    justifyContent: "space-between",
                    padding: wp("5%"),
                  }}
                >
                  <View>
                    <View>
                      <Text style={styles.boldText}>
                        Booked by : {modelData.customer_name}
                      </Text>
                      <Text style={styles.grayText}>
                        Date : {modelData.booking_created}
                      </Text>
                    </View>

                    <View style={{ marginTop: wp("2%") }}>
                      <Text style={styles.boldText}>Room Type </Text>
                      <Text style={styles.grayText}>{modelData.room_type}</Text>
                    </View>
                  </View>

                  <View>
                    {modelData.is_special === true ? (
                      <View style={{ flexDirection: "row" }}>
                        <Text style={styles.boldText}>Price :</Text>
                        <Text style={styles.amountPrice}>
                          {this.getAdditionalDiscount(modelData.room_regular_price)}
                          {"\u20B9"}{" "}
                        </Text>
                      </View>
                    ) : (
                      <View style={{ flexDirection: "row" }}>
                        <Text style={styles.boldText}>Price :</Text>
                        <Text style={styles.amountPrice}>
                          {modelData.room_price}
                          {"\u20B9"}{" "}
                        </Text>
                      </View>
                    )}

                    {userName !== undefined ? (
                      <Text numberOfLines={2} style={styles.amountPrice}>
                        Cancelled by :{" "}
                        {userName === modelData.cancelled_by_name
                          ? "me"
                          : modelData.cancelled_by_name}
                      </Text>
                    ) : (
                      console.log("")
                    )}
                    {this.props.comesFromBooking !== undefined &&
                      (modelData.no_of_hours !== null &&
                      modelData.no_of_hours !== undefined ? (
                        <Text
                          numberOfLines={2}
                          style={{
                            textAlign: "center",
                            width: wp(18),
                            color: COLORS.green_color,
                            fontSize: FONT.TextExtraSmall,
                            fontFamily: FONT_FAMILY.MontserratSemiBold,
                          }}
                        >
                          Stay {modelData.no_of_hours} hr
                        </Text>
                      ) : (
                        <Text
                          numberOfLines={2}
                          style={{
                            textAlign: "center",
                            width: wp(18),
                            color: COLORS.green_color,
                            fontSize: FONT.TextExtraSmall,
                            fontFamily: FONT_FAMILY.MontserratSemiBold,
                          }}
                        >
                          Stay {modelData.no_of_days} Days
                        </Text>
                      ))}
                  </View>
                </View>
                <TouchableOpacity
                  hitSlop={{ top: wp(5), left: wp(5), bottom: wp(5) }}
                  style={{
                    position: "absolute",
                    alignSelf: "flex-end",
                    marginTop: wp(-3),
                  }}
                  onPress={() => {
                    this.closeModal();
                  }}
                >
                  <CrossComponent width={wp(9)} height={wp(9)} />
                </TouchableOpacity>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </Modal>
    );
  }
}
