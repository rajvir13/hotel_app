import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Platform, StyleSheet } from "react-native";
import { FONT } from "../../utils/FontSizes";
import COLORS from "../../utils/Colors";
import { FONT_FAMILY } from "../../utils/Font";

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    roomImageView:{

     height:wp('60%'),
     width:wp('90%'),
     borderRadius: wp(3),
     marginLeft:wp('1.5%'),
     marginTop:wp('1.5%'),
     marginRight:wp('1.5%'),
     marginBottom:wp('1.5%'),
     alignSelf:'center',
     resizeMode:'cover'


    },

 boldText:{
     fontWeight:'bold',
     fontSize:FONT.TextSmall,
     padding:wp('1%')
 },
 amountPrice:{
    fontWeight:'bold',
    fontSize:FONT.TextSmall,
    padding:wp('1%'),
    color:'red',
    marginRight:wp('8%'),
    width:wp('30%')

 },
 grayText:{
  fontSize:FONT.TextSmall_2,
  padding:wp('1%'),
  color:COLORS.greyButton_color
  


 }
   
});

export default styles;
