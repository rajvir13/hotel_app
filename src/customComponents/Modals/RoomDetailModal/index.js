import React, { Component } from "react";
import {
  View,
  Text,
  Modal,
  StyleSheet,
  Image,
  TouchableOpacity,
  Alert,
  FlatList,
} from "react-native";

import propTypes from "prop-types";
import * as _ from "lodash";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Spacer } from "../../Spacer";
import GreenTick from "@assets/images/CustomerHome/greenTick.svg";
import { FONT_FAMILY } from "../../../utils/Font";
import { FONT } from "../../../utils/FontSizes";
import COLORS from "../../../utils/Colors";
import { API } from "../../../redux/constant";
import STRINGS from "../../../utils/Strings";
import { ICONS } from "../../../utils/ImagePaths";
import FastImage from "react-native-fast-image";
import CrossComponent from "../../../../assets/images/BookingIcons/CrossSVG";

export default class RoomDetailModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisiblePass: false,
      value: "",
      indexValue: 0,
    };
  }

  static propTypes = {
    roomModalVisible: propTypes.any,
    onHotelClick: propTypes.func,
    value: propTypes.string,
    closeRoomModal: propTypes.func,
    onGetRoomClick: propTypes.func,
    indexRoom: propTypes.number,
  };

  /**
   * life cycle method executing some logic after render method is called.
   */
  componentDidMount() {
    this.setState({
      modalVisiblePass: this.props.roomModalVisible,
    });
  }

  /**
   * Life cycle method for receiving the data in props
   */
  componentWillReceiveProps(nextProps) {
    this.setState({
      modalVisiblePass: nextProps.roomModalVisible,
      value: nextProps.value,
      indexValue: nextProps.indexRoom,
    });
  }

  /**
   * method defined for handling the onPress event of  Cancel button.
   */
  onCancelPress() {
    this.props.closeRoomModal();
    console.warn("cancel");
  }

  onGetButtonPress = () => {
    const { value } = this.props;
    const { indexValue } = this.state;
    let roomId = "";
    if (value[indexValue].id !== undefined) {
      roomId = value[indexValue].id;
    } else {
      roomId = "";
    }
    this.props.onGetRoomClick(roomId);
    this.onCancelPress();
  };

  onHotelViewButtonPress = () => {
    this.props.onHotelClick();
    this.onCancelPress();
  };

  /**
   * Life cycle method which contains the UI part.
   */
  render() {
    const { value } = this.props;
    const { indexValue } = this.state;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.roomModalVisible}
      >
        <View style={styles.centeredView}>
          <View
            style={{
              marginBottom: wp(8),
              backgroundColor: "white",
              justifyContent: "center",
              alignItems: "center",
              width: wp(90),
              borderRadius: wp(3),
            }}
          >
            <View style={{ height: wp(77), paddingHorizontal: wp(1) }}>
              {value[indexValue].room_images.length !== 0 ? (
                <FlatList
                  horizontal={true}
                  enableEmptySections={true}
                  style={{
                    marginRight: wp(1),
                    borderRadius: wp(3),
                  }}
                  contentContainerStyle={{ alignSelf: "center" }}
                  data={value[indexValue].room_images}
                  renderItem={({ item, index }) => (
                    <FastImage
                      style={{
                        justifyContent: "center",
                        marginVertical: wp(1),
                        borderRadius: wp(3),
                        width: wp(87),
                        height: wp(75),
                      }}
                      source={{
                        uri:
                          item.image !== null
                            ? API.IMAGE_BASE_URL + item.image
                            : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                      }}
                      resizeMode={FastImage.resizeMode.stretch}
                    />
                  )}
                />
              ) : (
                <Image
                  style={{
                    marginVertical: wp(1),
                    borderRadius: wp(3),
                    width: wp(88),
                    height: wp(75),
                  }}
                  resizeMode={"stretch"}
                  source={{
                    uri:
                      "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                  }}
                />
              )}

              <TouchableOpacity
                hitSlop={{ top: wp(5), left: wp(5), bottom: wp(5) }}
                style={{
                  position: "absolute",
                  alignSelf: "flex-end",
                  marginTop: wp(-3),
                }}
                onPress={() => {
                  this.onCancelPress();
                }}
              >
                <CrossComponent width={wp(9)} height={wp(9)} />
              </TouchableOpacity>
            </View>

            <Spacer space={2} />
            <View>
              <View
                style={{
                  width: wp(80),
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <View>
                  <Text
                    style={{
                      fontSize: FONT.TextSmall,
                      fontFamily: FONT_FAMILY.Montserrat,
                      fontWeight: "bold",
                    }}
                  >
                    Room Type: {value[indexValue].room_type}
                  </Text>
                  <Spacer space={0.8} />
                  <Text
                    style={{
                      fontSize: FONT.TextSmall,
                      lineHeight: wp(6),
                      fontFamily: FONT_FAMILY.Poppins,
                      color: COLORS.greyButton_color,
                      fontWeight: "500",
                    }}
                  >
                    {STRINGS.no_of_Guests}: {value[indexValue].no_of_guest}
                  </Text>
                </View>
                <View
                  style={{
                    // justifyContent:'flex-end',
                    flexDirection: "column",
                    alignItems: "center",
                  }}
                >
                  <Text
                    style={{
                      fontFamily: FONT_FAMILY.Roboto,
                      fontSize: wp(4.1),
                      fontWeight: "bold",
                      fontStyle: "normal",
                    }}
                  >
                    {"\u20B9"}
                    {value[indexValue].discounted_price} /
                    <Text
                      style={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: wp(4.3),
                        color: "red",
                        fontWeight: "bold",
                        fontStyle: "normal",
                        textDecorationLine: "line-through",
                      }}
                    >
                      {"\u20B9"}
                      {value[indexValue].regular_price}
                    </Text>
                  </Text>
                  <Text
                    style={{
                      fontWeight: "bold",
                      fontSize: wp(3.5),
                      fontFamily: FONT_FAMILY.Montserrat,
                      fontStyle: "normal",
                    }}
                  >
                    20% Discount{" "}
                  </Text>
                </View>
              </View>
              <Spacer space={1} />

              <View>
                {value[indexValue].room_service.length > 0 && (
                  <View>
                    <Text
                      style={{
                        fontSize: FONT.TextSmall,
                        fontFamily: FONT_FAMILY.MontserratBold,
                      }}
                    >
                      Complimentary Services
                    </Text>
                    <Spacer space={0.5} />
                    <Text
                      style={{
                        width: wp(80),
                        paddingHorizontal: wp(0.5),
                        fontSize: FONT.TextSmall_2,
                        color: COLORS.number_OfGuest_color,
                      }}
                    >
                      {value[indexValue].room_service
                        .map((e) => e.service)
                        .join(", ")}
                    </Text>
                  </View>
                )}

                <Spacer space={0.5} />
              </View>
              <Spacer space={1} />
              <View
                style={{
                  alignItems: "center",
                  paddingHorizontal: wp(1),
                  flexDirection: "row",
                  paddingVertical: wp(1),
                }}
              >
                {value[indexValue].is_available ? (
                  <>
                    <GreenTick width={wp(4)} height={wp(4)} />
                    <Spacer row={0.5} />
                    <Text
                      style={{
                        fontFamily: FONT_FAMILY.MontserratBold,
                        fontSize: FONT.TextSmall_2,
                        color: COLORS.green_color,
                      }}
                    >
                      Room Available
                    </Text>
                  </>
                ) : (
                  <>
                    <CrossComponent width={wp(4)} height={wp(4)} />
                    <Spacer row={0.5} />
                    <Text
                      style={{
                        fontFamily: FONT_FAMILY.MontserratBold,
                        fontSize: FONT.TextSmall_2,
                        color: COLORS.failure_Toast,
                      }}
                    >
                      Room Unavailable
                    </Text>
                  </>
                )}
              </View>

              {/*<Spacer space={1.5}/>*/}
              <View
                style={{
                  justifyContent: "space-around",
                  alignItems: "center",
                  paddingHorizontal: wp(1),
                  flexDirection: "row",
                  paddingTop: wp(6),
                  paddingBottom: wp(12),
                }}
              >
                {value[indexValue].is_available ? (
                  <TouchableOpacity
                    disabled={!value[indexValue].is_available}
                    onPress={() => this.onGetButtonPress()}
                    style={{
                      borderRadius: wp(3),
                      width: wp(33),
                      paddingVertical: wp(3.5),
                      backgroundColor: COLORS.app_theme_color,
                    }}
                  >
                    <Text
                      style={{
                        textAlign: "center",
                        letterSpacing: wp(0.1),
                        fontSize: FONT.TextSmall,
                        fontFamily: FONT_FAMILY.MontserratSemiBold,
                        color: COLORS.white_color,
                      }}
                    >
                      Get Room
                    </Text>
                  </TouchableOpacity>
                ) : (
                  console.log("")
                )}

                {/* <TouchableOpacity
                                    onPress={() => this.onHotelViewButtonPress()}
                                    style={{
                                        borderRadius: wp(3),
                                        width: wp(33),
                                        paddingVertical: wp(3.5),
                                        backgroundColor: COLORS.customerType,
                                    }}
                                >
                                    <Text
                                        style={{
                                            textAlign: "center",
                                            letterSpacing: wp(0.1),
                                            fontSize: FONT.TextSmall,
                                            fontFamily: FONT_FAMILY.MontserratSemiBold,
                                            color: COLORS.white_color,
                                        }}
                                    >
                                        Hotel View
                                    </Text>
                                </TouchableOpacity> */}
                {/*{this._renderButton(COLORS.app_theme_color, "Get Room",*/}
                {/*    (value[indexValue].id !== undefined) ? this.onGetButtonPress(value[indexValue].id) : this.onGetButtonPress(""))}*/}
                {/*{this._renderButton(COLORS.customerType, "Hotel View", this.onHotelViewButtonPress)}*/}
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "flex-end",
    // justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.6)",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});
