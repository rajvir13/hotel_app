import React, { Component } from "react";
import {
  View,
  Text,
  Modal,
  StyleSheet,
  Image,
  TouchableOpacity,
  Alert,
  FlatList,
} from "react-native";

import propTypes from "prop-types";
import * as _ from "lodash";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Spacer } from "../../Spacer";
import GreenTick from "@assets/images/CustomerHome/greenTick.svg";
import { FONT_FAMILY } from "../../../utils/Font";
import { FONT } from "../../../utils/FontSizes";
import COLORS from "../../../utils/Colors";
import { API } from "../../../redux/constant";
import STRINGS from "../../../utils/Strings";
import { ICONS } from "../../../utils/ImagePaths";
import FastImage from "react-native-fast-image";
import CrossComponent from "../../../../assets/images/BookingIcons/CrossSVG";

export default class DisplayRoomImages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisiblePass: false,
      value: "",
      indexValue: 0,
    };
  }

  render() {
    const { value } = this.props;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.roomModalVisible}
      >
        <View style={styles.centeredView}>
          <View
            style={{
              marginBottom: wp(8),
              backgroundColor: "transparent",
              width: wp(100),
              borderRadius: wp(3),
              height: hp(70),
              alignSelf: "center",
            }}
          >
            <View
              style={{
                height: hp(50),
                width: wp(100),
                paddingHorizontal: wp(1),
              }}
            >
              {this.props.currentClickedData.room_images.length !== 0 ? (
                <FlatList
                  horizontal={true}
                  enableEmptySections={true}
                  style={{
                    marginRight: wp(1),
                    borderRadius: wp(3),
                  }}
                  contentContainerStyle={{
                    alignSelf: "center",
                    height: hp(50),
                  }}
                  data={this.props.currentClickedData.room_images}
                  renderItem={({ item, index }) => (
                    <FastImage
                      style={{
                        justifyContent: "center",
                        marginVertical: wp(1),
                        borderRadius: wp(3),
                        width: wp(100),
                        height: hp(60),
                      }}
                      source={{
                        uri:
                          item.image !== null
                            ? API.IMAGE_BASE_URL + item.image
                            : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                      }}
                      resizeMode={FastImage.resizeMode.stretch}
                    />
                  )}
                />
              ) : (
                <Image
                  style={{
                    marginVertical: wp(1),
                    borderRadius: wp(3),
                    width: wp(88),
                    height: wp(75),
                  }}
                  resizeMode={"stretch"}
                  source={{
                    uri:
                      "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                  }}
                />
              )}

              <TouchableOpacity
                hitSlop={{ top: wp(5), left: wp(5), bottom: wp(5) }}
                style={{
                  position: "absolute",
                  alignSelf: "flex-end",
                  marginTop: wp(-3),
                }}
                onPress={() => {
                  this.props.closeRoomModal();
                }}
              >
                <CrossComponent width={wp(9)} height={wp(9)} />
              </TouchableOpacity>
            </View>

            <Spacer space={2} />
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "flex-end",
    // justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.6)",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});
