import React from 'react';
import {FlatList, Modal, Text, TextInput, TouchableOpacity, View} from "react-native";
import propTypes from "prop-types";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";

import BaseClass from "../../../utils/BaseClass";
import {MainContainer, SafeAreaViewContainer, ShadowViewContainer} from "../../../utils/BaseStyle";
import ProfileComponent from "../../../../assets/images/TabView/ProfileSVG";
import COLORS from "../../../utils/Colors";
// import {Icon} from "react-native-vector-icons/index";


export default class CountryModal extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            modalVisibleCountry: this.props.visibleCountry,
            value: true,
            searchText: '',
            passedData: [],
            passedData2: [],
        }
    }

    static propTypes = {
        style: propTypes.style,
        visibleCountry: propTypes.any,
        onItemClick: propTypes.func,
        value: propTypes.string,
        closeCountryModal: propTypes.func,
        passingValue: propTypes.any,
        // passengerType: propTypes.any,
        // index: propTypes.any
    };

    /**
     * life cycle method defined for receiving the data in props
     * @param nextProps
     */
    componentWillReceiveProps(nextProps) {
        this.setState({
            modalVisibleCountry: nextProps.visibleCountry,
            value: nextProps.value,
            passedData: nextProps.passingValue,
            passedData2: nextProps.passingValue,
            passengerType: nextProps.passengerType,
            index: nextProps.index
        });
    }

    /**
     * method defined for searching the country by name
     * @param text
     */
    onSearchText(text) {
        text = text.replace(/[^a-zA-Z0-9]/g, "");
        this.setState({
            searchText: text
        });
        if (text === undefined || text === "") {
            // passedData = passedData2;
            this.setState({
                passedData: this.state.passedData2
            });
            return;
        }
        if (text.trim().length > 0) {
            let arr = this.state.passedData;
            let temp = arr.filter(item => {
                    return item.name.toUpperCase().indexOf(text.toUpperCase()) > -1
                }
            );
            // passedData = temp
            this.setState({
                passedData: temp
            })
        }
    }

    /**
     * for handling the on press of Close button
     */
    onCloseClick() {
        this.props.closeCountryModal();
        this.setState({
            modalVisibleCountry: false
        })
    }

    /**
     * for handling the onPress event on country names list items
     * @param item
     * @param props
     */
    onCountryPress(item, props) {
        this.props.onItemClick(item, props);
        this.setState({
            searchText: ""
        })
    }

    /**
     * Life cycle method which contains the UI part.
     * @returns {*}
     */
    render() {
        return (
            <Modal visible={this.props.visibleCountry}
                   animationType={'slide'}
                   transparent={false}
                // onRequestClose={this.props.onRequestClose}
            >
                <SafeAreaViewContainer>
                    <MainContainer>
                        <View>
                            {/** =======for search bar view======= */}
                            <View style={{
                                marginTop: 10, flexDirection: 'row',
                                width: wp(100),
                                alignItems: 'center',
                                justifyContent: 'space-evenly',
                            }}>
                                <ProfileComponent
                                    fillColor={COLORS.black_color}/>
                                <View style={{
                                    paddingHorizontal: wp(5),
                                    borderRadius: wp(5),
                                    backgroundColor: COLORS.white_color,
                                     marginTop:wp(2)
                                }}>
                                    <TextInput allowFontScaling={false}
                                               style={{
                                                   // backgroundColor: 'orange',
                                                   width: wp(100) / 1.7,
                                                   alignContent: 'center',
                                                   justifyContent: 'center',
                                                   paddingVertical: wp(2),
                                                  
                                               }}
                                               underlineColorAndroid='transparent'
                                               autoFocus={false}
                                               placeholder={"Search"}
                                               placeholderTextColor={'grey'}
                                               onChangeText={(text) => this.onSearchText(text)}
                                               value={this.state.searchText}
                                               multiline={false}
                                               maxLength={50}
                                               keyboardType='default'
                                               returnKeyType={"next"}
                                    />
                                    {(this.state.searchText.length !== 0) ?
                                        <TouchableOpacity
                                            style={{padding: 3, alignSelf: 'center'}}
                                            onPress={() => [this.onSearchText(this.state.searchText = ''), this.setState({searchText: ''})]}>
                                            {/*<Icon*/}
                                            {/*    name="close" size={30}*/}
                                            {/*    color={'#67696b'}*/}
                                            {/*/>*/}
                                        </TouchableOpacity>
                                        :
                                        < TouchableOpacity
                                            style={{padding: 3, alignSelf: 'center'}}
                                            onPress={() => [this.onSearchText(this.state.searchText = ''), this.setState({searchText: ''})]}
                                        >
                                            {/*<Icon*/}
                                            {/*    name="close" size={30}*/}
                                            {/*    color={'white'}*/}
                                            {/*/>*/}

                                        </TouchableOpacity>
                                    }
                                </View>
                                <TouchableOpacity
                                    // style={{
                                    //     justifyContent: 'center',
                                    //     alignSelf: 'center',
                                    // }}
                                    onPress={() => {
                                        this.onCloseClick()
                                    }}>
                                    <Text style={{
                                        fontSize: 15,
                                        color: 'red'
                                    }}>CLOSE</Text>
                                </TouchableOpacity>
                            </View>

                            {/** =======for country list view======= */}

                            {this.state.passedData.length>0 ? 
                             <FlatList extraData={this.state}
                             data={this.state.passedData}
                             renderItem={({item, index}) => (

                                 <View>
                                     <TouchableOpacity
                                         onPress={() => {
                                             this.onCountryPress(item, this.props)
                                         }}>
                                         <Text style={{
                                             fontSize: 16,
                                             marginVertical: 15,
                                             // marginHorizontal: 42,
                                             marginHorizontal: 25,
                                             fontWeight: 'bold'
                                         }}>{item.name}</Text>
                                     </TouchableOpacity>
                                     <View style={{
                                         height: 0.5,
                                         width: '95%',
                                         backgroundColor: 'grey',
                                         // alignSelf: 'flex-end'
                                         alignSelf: 'center'
                                     }}/>
                                 </View>
                             )}/>
                            :
                            <Text style={{
                                fontSize: 16,
                                marginVertical: 15,
                                // marginHorizontal: 42,
                                marginHorizontal: 25,
                                fontWeight: 'bold'
                            }}>No result found</Text>
                            }
                           
                        </View>
                    </MainContainer>
                </SafeAreaViewContainer>
            </Modal>
        )
    }
};
