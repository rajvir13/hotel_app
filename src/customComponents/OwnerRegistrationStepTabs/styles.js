import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { StyleSheet } from "react-native";
import COLORS from "../../utils/Colors";
const styles = StyleSheet.create({
  mainViewStyle: {
    height: wp("20%"),
    flexDirection: "row",
    // alignItems: "stretch",
    width: wp("100%"),

    justifyContent: "space-between",
    padding: 0,
    margin: 0,
  },

  contentView: {
    backgroundColor: COLORS.app_theme_color,
    flex: 0.33,
    padding: 10,
    justifyContent:'center',
    alignItems:'center'
  },

  SelectedImageViewStyle: {
    height: wp("5%"),
    flexDirection: "row",
    alignItems: "stretch",
    width: wp("100%"),
    justifyContent: "space-between",
    // padding: 0,
    // margin: 0,
    backgroundColor: COLORS.backGround_color
  },

  SelectedImageView: {
    width: wp("33.33%"),
    alignItems: "center",
    padding: 8,
  },

  StepIconStyle: {
    alignSelf: "center",
    marginVertical: -10,
  },

  textColor: {
    textAlign:'center',
    color: "white",
    width: wp(25)
  },
});

export default styles;
