import BaseClass from "../../utils/BaseClass";
import STRINGS from "../../utils/Strings";

import {
  SafeAreaViewContainer,
  MainContainer,
  ShadowViewContainer,
  ScrollContainer,
} from "../../utils/BaseStyle";
import React from "react";
import {
  View,
  Text,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Keyboard,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ImageBackground,
} from "react-native";

import { ICONS } from "../../utils/ImagePaths";
import { Spacer } from "../../customComponents/Spacer";
import COLORS from "../../utils/Colors";
import { FONT } from "../../utils/FontSizes";
import { PrimaryButton } from "../../customComponents/button/AppButton";
import * as Validations from "../../utils/Validations";
import styles from "../../customComponents/OwnerRegistrationStepTabs/styles";

export const OwnerRegistrationStepTabs = (props) => {
  console.warn("isvIS", props);

  if (props.visibleIndex === 1) {
    console.warn("current tab index is :", props.visibleIndex);
    return <First />;
  } else if (props.visibleIndex === 2) {
    console.warn("current tab index is :", props.visibleIndex);

    return <Second />;
  } else if (props.visibleIndex === 3) {
    console.warn("current tab index is :", props.visibleIndex);
    return <Third />;
  }
};

export const First = () => {
  return (
    <View>
      <View style={styles.mainViewStyle}>
        <View style={styles.contentView}>
          <Text style={styles.textColor}>Name and </Text>
          <Text style={styles.textColor}>Address </Text>
        </View>
        <View style={styles.contentView}>
          <Text style={styles.textColor}>Rooms and </Text>
          <Text style={styles.textColor}>Amenities </Text>
        </View>
        <View style={styles.contentView}>
          <Text style={styles.textColor}>Policy and </Text>
          <Text style={styles.textColor}>Discount </Text>
        </View>
      </View>

      <View style={styles.SelectedImageViewStyle}>
        {/* {console.warn("index is--------",props.visibleIndex)} */}

        <View style={styles.SelectedImageView}>
          <Image
            style={styles.StepIconStyle}
            source={ICONS.REGISTRATION_STEP_IMG_ICON}
          />
        </View>

        <View style={styles.SelectedImageView}/>

        <View style={styles.SelectedImageView}/>
      </View>
    </View>
  );
};
// ----------------------------------------

//********************* seond  */

export const Second = () => {
  return (
    <View>
      <View style={styles.mainViewStyle}>
        <View style={styles.contentView}>
          <Text style={styles.textColor}>Name and </Text>
          <Text style={styles.textColor}>Address </Text>
        </View>
        <View style={styles.contentView}>
          <Text style={styles.textColor}>Rooms and </Text>
          <Text style={styles.textColor}>Amenities </Text>
        </View>
        <View style={styles.contentView}>
          <Text style={styles.textColor}>Policy and </Text>
          <Text style={styles.textColor}>Discount </Text>
        </View>
      </View>

      <View style={styles.SelectedImageViewStyle}>
        {/* {console.warn("index is--------",props.visibleIndex)} */}

        <View style={styles.SelectedImageView}/>

        <View style={styles.SelectedImageView}>
          <Image
            style={styles.StepIconStyle}
            source={ICONS.REGISTRATION_STEP_IMG_ICON}
          />
        </View>

        <View style={styles.SelectedImageView}/>
      </View>
    </View>
  );
};

//************************ Third ******************/
export const Third = () => {
  return (
    <View>
      <View style={styles.mainViewStyle}>
        <View style={styles.contentView}>
          <Text style={styles.textColor}>Name and </Text>
          <Text style={styles.textColor}>Address </Text>
        </View>
        <View style={styles.contentView}>
          <Text style={styles.textColor}>Rooms and </Text>
          <Text style={styles.textColor}>Amenities </Text>
        </View>
        <View style={styles.contentView}>
          <Text style={styles.textColor}>Policy and </Text>
          <Text style={styles.textColor}>Discount </Text>
        </View>
      </View>

      <View style={styles.SelectedImageViewStyle}>
        {/* {console.warn("index is--------",props.visibleIndex)} */}

        <View style={styles.SelectedImageView}/>

        <View style={styles.SelectedImageView}/>

        <View style={styles.SelectedImageView}>
          <Image
            style={styles.StepIconStyle}
            source={ICONS.REGISTRATION_STEP_IMG_ICON}
          />
        </View>
      </View>
    </View>
  );
};
