import React, {useEffect, useState, useCallback, useRef} from "react";
import {
    Animated,
    Dimensions,
    StyleSheet,
    SafeAreaView,
    View,
} from "react-native";
import _ from "lodash";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {TabView, SceneMap, TabBar} from "react-native-tab-view";

import COLORS from "../../utils/Colors";
import TabTipSVG from "../../../assets/images/TabView/TabTipSVG";
import RoomsDetails from "../../screens/customer/particularHotelProfile/classComponents/tabClasses/roomsScreen";
import AboutHotel from "../../screens/customer/particularHotelProfile/classComponents/tabClasses/aboutHotel";
import PolicyDocumentHotel
    from "../../screens/customer/particularHotelProfile/classComponents/tabClasses/policyDocument";
import AmenitiesScreen from "../../screens/customer/particularHotelProfile/classComponents/tabClasses/amenitiesScreen";

const FirstRoute = ({aboutData}) => (
    <View style={[styles.container]}>
        <AboutHotel aboutData={aboutData}/>
    </View>
);
const SecondRoute = ({amenitiesData}) => (
    <View style={[styles.container]}>
        <AmenitiesScreen amenitiesData={amenitiesData}/>
    </View>
);

const ThirdRoute = ({data, buttonPress, isRoomModalOpen, onModalClose, onHotelModalClick, onRoomPress, roomIndex}) => (
    <View style={[styles.container]}>
        <RoomsDetails roomData={data}
                      onGetRoomPress={(roomId)=> onRoomPress(roomId)}
                      isRoomModalOpen={isRoomModalOpen}
                      onRoomModalClose={onModalClose}
                      onRoomModalOpen={(index) => buttonPress(index)}
                      onModalHotelClick={onHotelModalClick}
                      roomIndexClicked={roomIndex}/>
    </View>
);

const FourthRoute = ({policyData, openPdf}) => (
    <View style={[styles.container]}><PolicyDocumentHotel policyData={policyData} openDocument={(url) => openPdf(url)}/></View>
);


const TabIndicator = ({width, tabWidth, index}) => {
    const marginLeftRef = useRef(new Animated.Value(index ? tabWidth : 0))
        .current;
    useEffect(() => {
        Animated.timing(marginLeftRef, {
            toValue: tabWidth,
            duration: 400
        }).start();
    }, [tabWidth]);

    return (
        <Animated.View
            style={{
                justifyContent: "flex-end",
                alignItems: "center",
                flex: 1,
                width: width,
                marginLeft: marginLeftRef,
                position: 'absolute',
                bottom: -12,
            }}
        >
            <TabTipSVG fillColor={COLORS.tabBar_color}/>
        </Animated.View>
    );
};

const TabViewExample = ({
                            aboutData, isRoomModalOpen, roomData, onRoomModalClose, onHotelModalClick,
                            policyData, onButtonPress, amenitiesData, onRoomClick, roomIndex, openPdfDoc
                        }) => {
    const [index, setIndex] = useState(0);
    const routes = [
        {key: "first", title: "About"},
        {key: "second", title: "Amenities "},
        {key: "third", title: "Rooms "},
        {key: "fourth", title: "Policy Document "}
    ];

    const renderScene = ({route}) => {
        switch (route.key) {
            case 'first':
                return <FirstRoute aboutData={aboutData}/>;
            case 'second':
                return <SecondRoute amenitiesData={amenitiesData}/>;
            case 'third':
                return <ThirdRoute data={roomData} isRoomModalOpen={isRoomModalOpen}
                                   buttonPress={(index) => onButtonPress(index)}
                                   onModalClose={onRoomModalClose} onHotelModalClick={onHotelModalClick}
                                   onRoomPress={(roomId)=>onRoomClick(roomId)} roomIndex={roomIndex}/>;
            case 'fourth':
                return <FourthRoute policyData={policyData} openPdf={(url) => openPdfDoc(url)}/>;
            default:
                return null;
        }
    };

    const renderIndicator = useCallback(
        ({getTabWidth}) => {
            const tabWidth = _.sum([...Array(index).keys()].map(i => getTabWidth(i)));

            return (
                <TabIndicator
                    width={getTabWidth(index)}
                    tabWidth={tabWidth}
                    index={index}
                />
            );
        },
        [index]
    );

    return (
        <View style={styles.container}>
            <TabView
                navigationState={{
                    index,
                    routes
                }}
                renderScene={renderScene}
                onIndexChange={setIndex}
                // initialLayout={{width: Dimensions.get("window").width}}
                renderTabBar={props => (
                    <TabBar
                        {...props}
                        scrollEnabled
                        tabStyle={styles.tabBar}
                        // indicatorStyle={styles.indicator}
                        style={styles.tabBarContainer}
                        labelStyle={styles.labelStyle}
                        renderIndicator={renderIndicator}
                    />
                )}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    tabBar: {
        width: wp(100)/3,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.tabBar_color,
        marginHorizontal: wp(0.4)
    },
    indicator: {
        // backgroundColor: "#ccc"
        backgroundColor: "red",
    },
    tabBarContainer: {
        backgroundColor: COLORS.white_color,
        marginBottom: wp(3),
    },
    labelStyle: {
        marginVertical: 0,
        textAlign: 'center',
        // backgroundColor: "#fff",
        color: COLORS.black_color,
    }
});

export default TabViewExample
