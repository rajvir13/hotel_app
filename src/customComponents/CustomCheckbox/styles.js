import React from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { StyleSheet } from 'react-native';
const styles = StyleSheet.create({
    checkedView:
    {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
 
    checkBoxImage:
    {
      height: '80%',
      width: '80%',
      tintColor: 'white',
      resizeMode: 'contain'
    },
 
    uncheckedView:
    {
      flex: 1,
      backgroundColor: 'white'
    },
 
    checkBoxLabelText:
    {
      fontSize: 16,
      paddingLeft: 10
    }
});
export default styles;