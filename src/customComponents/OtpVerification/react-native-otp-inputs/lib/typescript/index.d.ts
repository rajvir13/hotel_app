import { PureComponent, ReactNode, RefObject } from 'react';
import { StyleProp, TextStyle, ViewStyle } from 'react-native';
import OtpInput from './OtpInput';
interface Props {
    autoCapitalize: 'none' | 'sentences' | 'words' | 'characters';
    clearTextOnFocus: boolean;
    containerStyles?: StyleProp<ViewStyle>;
    errorMessage?: string;
    errorMessageContainerStyles?: StyleProp<ViewStyle>;
    errorMessageTextStyles?: StyleProp<TextStyle>;
    focusStyles?: StyleProp<ViewStyle>;
    focusedBorderColor: string;
    handleChange: (otpCode: string) => void;
    inputContainerStyles?: StyleProp<ViewStyle>;
    inputStyles?: StyleProp<TextStyle>;
    inputTextErrorColor: string;
    inputsContainerStyles?: StyleProp<ViewStyle>;
    isRTL: boolean;
    keyboardType: 'default' | 'email-address' | 'numeric' | 'phone-pad';
    numberOfInputs: number;
    placeholder: string;
    secureTextEntry: boolean;
    selectTextOnFocus: boolean;
    testIDPrefix: string;
    unfocusedBorderColor: string;
}
interface State {
    loading: boolean;
    otpCode: Array<string>;
    previousCopiedText: string;
}
export default class OtpInputs extends PureComponent<Props, State> {
    static defaultProps: {
        autoCapitalize: string;
        clearTextOnFocus: boolean;
        focusedBorderColor: string;
        handleChange: {
            (message?: any, ...optionalParams: any[]): void;
            (message?: any, ...optionalParams: any[]): void;
            (message?: any, ...optionalParams: any[]): void;
        };
        inputTextErrorColor: string;
        keyboardType: string;
        numberOfInputs: number;
        secureTextEntry: boolean;
        selectTextOnFocus: boolean;
        unfocusedBorderColor: string;
        testIDPrefix: string;
        isRTL: boolean;
        placeholder: string;
    };
    inputs: RefObject<OtpInput>[];
    private _interval;
    constructor(props: Props);
    componentDidMount(): void;
    componentWillUnmount(): void;
    reset: () => void;
    private _listenOnCopiedText;
    private _handleAfterOtpAction;
    private _updateText;
    private _handleBackspace;
    private _focusInput;
    private _renderInputs;
    render(): ReactNode;
}
export {};
