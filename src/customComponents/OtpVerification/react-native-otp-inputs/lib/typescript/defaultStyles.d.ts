declare const _default: {
    container: {
        flex: number;
    };
    otpContainer: {
        borderBottomWidth: number;
        height: number;
        margin: number;
    };
    otpInput: {
        fontSize: number;
        paddingTop: number;
        textAlign: "center";
        width: number;
    };
    inputsContainer: {
        flex: number;
        flexDirection: "row";
        alignItems: "center";
        justifyContent: "space-between";
    };
    errorMessageContainer: {
        marginHorizontal: number;
    };
};
export default _default;
