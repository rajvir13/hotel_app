import { PureComponent, RefObject } from 'react';
import { NativeSyntheticEvent, StyleProp, TextInput, TextInputChangeEventData, TextInputKeyPressEventData, TextStyle, ViewStyle } from 'react-native';
interface Props {
    autoCapitalize?: 'none' | 'sentences' | 'words' | 'characters';
    clearTextOnFocus?: boolean;
    containerStyles?: StyleProp<ViewStyle>;
    error?: boolean;
    firstInput: boolean;
    focusStyles?: StyleProp<ViewStyle>;
    focusedBorderColor?: string;
    handleBackspace: (event: NativeSyntheticEvent<TextInputKeyPressEventData>) => void;
    inputStyles?: StyleProp<TextStyle>;
    keyboardType?: 'default' | 'email-address' | 'numeric' | 'phone-pad';
    numberOfInputs: number;
    placeholder?: string;
    secureTextEntry?: boolean;
    selectTextOnFocus?: boolean;
    testID: string;
    textErrorColor?: string;
    unfocusedBorderColor?: string;
    updateText: (event: NativeSyntheticEvent<TextInputChangeEventData>) => void;
    value?: string;
}
interface State {
    isFocused: boolean;
}
export default class OtpInput extends PureComponent<Props, State> {
    state: {
        isFocused: boolean;
    };
    private _onFocus;
    private _onBlur;
    input: RefObject<TextInput>;
    clear: () => void;
    focus: () => void;
    render(): JSX.Element;
}
export {};
