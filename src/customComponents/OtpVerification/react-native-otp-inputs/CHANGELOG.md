# Changelog

<a name="3.3.1"></a>
## 3.3.1 (2019-07-25)

### Added

- ✅ Update Snapshots [[106c842](https://github.com/dsznajder/react-native-otp-inputs/commit/106c842a6650f41c32d123ce235483af6e2cbc75)]

### Fixed

- 🐛 Fix reseting not only value but also inputs [[cb2db53](https://github.com/dsznajder/react-native-otp-inputs/commit/cb2db53ec66993491abe7e463ad281e1eb4a9865)]

### Miscellaneous

-  👷 Add Dependabot config [[653b820](https://github.com/dsznajder/react-native-otp-inputs/commit/653b820665aea6c4dfc2b1c8b540b1b8680a115e)]
- 📝 Update README with methods section [[e56bce7](https://github.com/dsznajder/react-native-otp-inputs/commit/e56bce7a3a7c153834f0612edeff77dabd8d81dd)]


<a name="3.3.0"></a>
## 3.3.0 (2019-07-25)

### Added

- ✨ Set max length to 1 [[44842e0](https://github.com/dsznajder/react-native-otp-inputs/commit/44842e00b182f2264983618c5c29376fb171a507)]
- ✨ Add testIDPrefix prop [[c2a00f6](https://github.com/dsznajder/react-native-otp-inputs/commit/c2a00f6222b4e0cd79d612a484e08bfb684ddae8)]
- ✨ RTL support and Placeholder support. ([#75](https://github.com/dsznajder/react-native-otp-inputs/issues/75)) [[e417b13](https://github.com/dsznajder/react-native-otp-inputs/commit/e417b1369df9df00099a5b047382eb4da08fd226)]
- ✨ Add reset method [[69e0146](https://github.com/dsznajder/react-native-otp-inputs/commit/69e0146a7e3353fac49d18b1712e5536d6dd4d8a)]
- ✅ Fix tests [[f413a26](https://github.com/dsznajder/react-native-otp-inputs/commit/f413a26f364ca7e67e604d2df17b2815c7448c39)]

### Security

- 🔒 Bump handlebars from 4.1.1 to 4.1.2 [[e6c671c](https://github.com/dsznajder/react-native-otp-inputs/commit/e6c671ceea2544cbb61a5d30c99adfcdd61ab04d)]

### Miscellaneous

- 📝 Update README [[3f0b3da](https://github.com/dsznajder/react-native-otp-inputs/commit/3f0b3dab76bcbb3190b3dc3a7820581fb58cf54b)]
-  Bump react-native from 0.59.8 to 0.59.9 [[6b7e823](https://github.com/dsznajder/react-native-otp-inputs/commit/6b7e823c0a241d613fe18c1cee55154f561f869f)]
-  Bump typescript from 3.4.5 to 3.5.2 [[ee80a2e](https://github.com/dsznajder/react-native-otp-inputs/commit/ee80a2e1c490188d391d999b69cbbc2ed6c64684)]
-  Bump @types/jest from 24.0.13 to 24.0.14 [[abc0b0f](https://github.com/dsznajder/react-native-otp-inputs/commit/abc0b0f467f07a3e3a77737850e53de7903eae0b)]
-  Bump release-it from 12.2.1 to 12.3.0 [[3b15819](https://github.com/dsznajder/react-native-otp-inputs/commit/3b15819b712f825ec3ab873b848c974ca690411a)]
-  Bump tslint from 5.16.0 to 5.17.0 [[1d748b9](https://github.com/dsznajder/react-native-otp-inputs/commit/1d748b9286d8b92408c347d1e891e3bdd78b2cac)]
-  Bump @types/react-native from 0.57.60 to 0.57.63 [[1ddb6b7](https://github.com/dsznajder/react-native-otp-inputs/commit/1ddb6b7f9fc59c5ff9e55d7009a45921245797bc)]
-  Bump @types/jest from 24.0.14 to 24.0.15 [[43c200f](https://github.com/dsznajder/react-native-otp-inputs/commit/43c200fdf837ab916bccb78e36766b081c7ebe6f)]
-  Bump husky from 2.3.0 to 2.4.1 [[2222b93](https://github.com/dsznajder/react-native-otp-inputs/commit/2222b93a04cd150902c8d782197785d1a7f3718a)]
-  Bump @types/react from 16.8.18 to 16.8.20 [[042f1a0](https://github.com/dsznajder/react-native-otp-inputs/commit/042f1a0e0a113c01ae43fc547f86f2a4dce3ac09)]
-  Bump @types/react-test-renderer from 16.8.1 to 16.8.2 [[1ba4b0b](https://github.com/dsznajder/react-native-otp-inputs/commit/1ba4b0bc2a22b959ed19834458343ccb1fd1c7f8)]
-  Bump @react-native-community/bob from 0.4.1 to 0.5.0 [[385c9cb](https://github.com/dsznajder/react-native-otp-inputs/commit/385c9cbce164175dc83d16a44c11e11a871c44b1)]
-  Bump @types/react from 16.8.20 to 16.8.22 [[1239233](https://github.com/dsznajder/react-native-otp-inputs/commit/1239233184dfc03bcd91b6550f256fe39b60233e)]
-  Bump tslint from 5.17.0 to 5.18.0 [[e3ed24d](https://github.com/dsznajder/react-native-otp-inputs/commit/e3ed24d27c29152dfa62d91165a3bc2b0c17db12)]
-  Bump react-test-renderer from 16.8.3 to 16.8.6 [[0974a97](https://github.com/dsznajder/react-native-otp-inputs/commit/0974a97ee022eed45526cd4542f15ef93ed820b0)]
-  Bump husky from 2.4.1 to 2.5.0 [[e549d9c](https://github.com/dsznajder/react-native-otp-inputs/commit/e549d9ca227b6a2b065e6b811688c42b2e5ce12c)]
-  Bump @types/react-native from 0.57.63 to 0.57.64 [[d3eb14f](https://github.com/dsznajder/react-native-otp-inputs/commit/d3eb14f2be249a7252e813a7523ff507f1911998)]
-  Bump husky from 2.5.0 to 2.7.0 [[343577f](https://github.com/dsznajder/react-native-otp-inputs/commit/343577ffbb871af4e9e5ed653c3b6e362ac262c0)]
-  Bump @react-native-community/bob from 0.5.0 to 0.6.0 [[577da22](https://github.com/dsznajder/react-native-otp-inputs/commit/577da22d2e1de5d1869d27883bb712ca62f6f947)]
-  Bump husky from 2.7.0 to 3.0.0 [[05b0c0a](https://github.com/dsznajder/react-native-otp-inputs/commit/05b0c0a88193f13ab80da7982db83c0a44be1073)]
-  Bump @react-native-community/bob from 0.6.0 to 0.6.1 [[bb04102](https://github.com/dsznajder/react-native-otp-inputs/commit/bb041028ae035fc79c92d74ea848529724b1b315)]
-  Bump @types/react-native from 0.57.64 to 0.57.65 [[a91362b](https://github.com/dsznajder/react-native-otp-inputs/commit/a91362b7d4130102794cdc2dc94b26a0382251a1)]
-  Bump react-native from 0.59.9 to 0.59.10 [[c2589a6](https://github.com/dsznajder/react-native-otp-inputs/commit/c2589a60ae7c0756a4694785b70751f5b6cb1e4d)]
-  Bump react and react-native [[7fb809a](https://github.com/dsznajder/react-native-otp-inputs/commit/7fb809a5298aac67ca00f63e9fe94f2057aa5875)]
-  Bump @types/react from 16.8.22 to 16.8.23 [[1aa86cd](https://github.com/dsznajder/react-native-otp-inputs/commit/1aa86cd509029d04cb9a561184c06c98a00abc23)]
-  Bump @babel/core from 7.4.5 to 7.5.0 [[4d88545](https://github.com/dsznajder/react-native-otp-inputs/commit/4d8854508e1110a4a0b3e2c7f4668c1dd8adfd12)]
-  Bump metro-react-native-babel-preset from 0.54.1 to 0.55.0 [[beb4c47](https://github.com/dsznajder/react-native-otp-inputs/commit/beb4c47cecb776d07f640e5958e06e1b3551413b)]
-  Bump release-it from 12.3.0 to 12.3.2 [[68e1929](https://github.com/dsznajder/react-native-otp-inputs/commit/68e19295fa5be0404d0db7a60b4a1b0e23ef3820)]
-  Bump @types/react-native from 0.57.65 to 0.60.0 [[1d5ee1e](https://github.com/dsznajder/react-native-otp-inputs/commit/1d5ee1e5b091f8af10a92482c18abec0d05fe9c7)]
-  Bump typescript from 3.5.2 to 3.5.3 [[123e508](https://github.com/dsznajder/react-native-otp-inputs/commit/123e5080ac661000352cc3b636f0abc64b9bf1e8)]
-  Bump @babel/core from 7.5.0 to 7.5.4 [[9402649](https://github.com/dsznajder/react-native-otp-inputs/commit/94026492c0b37f95048ca19d4b0196c55bf233df)]
-  Bump react-native from 0.60.0 to 0.60.3 [[0c61c98](https://github.com/dsznajder/react-native-otp-inputs/commit/0c61c9804d31c43b35aa77953fc3bcbd9ff7dc0a)]
-  Bump release-it from 12.3.2 to 12.3.3 [[83343ec](https://github.com/dsznajder/react-native-otp-inputs/commit/83343ece20895f846be6900ec918cdf5c053e3c7)]
-  Bump @types/react-native from 0.60.0 to 0.60.1 [[ab9da88](https://github.com/dsznajder/react-native-otp-inputs/commit/ab9da88597fd51451ad5381ffa0ce98fd0dbf010)]
-  Bump @types/react-native from 0.60.1 to 0.60.2 [[4f6cf7f](https://github.com/dsznajder/react-native-otp-inputs/commit/4f6cf7ff7659884e9bd71fb717502724ef737195)]
-  Bump @babel/core from 7.5.4 to 7.5.5 [[e5bce21](https://github.com/dsznajder/react-native-otp-inputs/commit/e5bce216db2aa768d9283e9dc714c13f73bb368f)]
-  Bump react-native from 0.60.3 to 0.60.4 [[f0a6e1b](https://github.com/dsznajder/react-native-otp-inputs/commit/f0a6e1b3b540762b9a6c22396f80624ccbf1054e)]
-  Bump husky from 3.0.0 to 3.0.1 [[4841f0c](https://github.com/dsznajder/react-native-otp-inputs/commit/4841f0cbee816c96886e9df6b1d06cc8e5335cf9)]
-  Bump @types/react-test-renderer from 16.8.2 to 16.8.3 ([#76](https://github.com/dsznajder/react-native-otp-inputs/issues/76)) [[0c52317](https://github.com/dsznajder/react-native-otp-inputs/commit/0c52317524da4df78c88f99ecd7141b805f7c66f)]
-  Bump release-it from 12.3.3 to 12.3.4 [[d278825](https://github.com/dsznajder/react-native-otp-inputs/commit/d2788250389a0ef699321b62b5fc2f92facdb9ae)]


<a name="3.1.1"></a>
## 3.1.1 (2019-05-28)

### Added

- ✅ Update testing environment [[ed938bf](https://github.com/dsznajder/react-native-otp-inputs/commit/ed938bf212777b66ddef5a901745cede256651b1)]

### Fixed

- 💚 Fix tests on ci [[a2851e0](https://github.com/dsznajder/react-native-otp-inputs/commit/a2851e0b61b9edaa54cfef33dc94f7ea1cdc7a6f)]

### Miscellaneous

- 🏷️ Update Style typings [[8be878a](https://github.com/dsznajder/react-native-otp-inputs/commit/8be878aabdfa1ad327c891b81bb0eab884132b9c)]


<a name="3.1.0"></a>
## 3.1.0 (2019-05-27)

### Miscellaneous

-  Fix react-native extension [[2bb09d3](https://github.com/dsznajder/react-native-otp-inputs/commit/2bb09d3d82f6be500f3064ad63a059a9b9c7413e)]
-  Prop focusStyles [[199a4ce](https://github.com/dsznajder/react-native-otp-inputs/commit/199a4cea07ac46715eb9ab3e729519a8dd086af7)]
-  remove logs [[c2cff59](https://github.com/dsznajder/react-native-otp-inputs/commit/c2cff5929540088e6462815cc5c4006e9eb7c82f)]
-  update readme with focusStyle [[83ac10c](https://github.com/dsznajder/react-native-otp-inputs/commit/83ac10cc2a32e7d098d3437d938765da26bc1173)]


<a name="3.0.2"></a>
## 3.0.2 (2019-05-09)

### Removed

- 🔥 Remove unnecessary files from npm [[4c70982](https://github.com/dsznajder/react-native-otp-inputs/commit/4c70982ee012d1dbdcc0b77b981d0281bf44f509)]

### Miscellaneous

-  Fix react-native extension [[e7262e3](https://github.com/dsznajder/react-native-otp-inputs/commit/e7262e3698b113bb5b83d1de4bd7175a4103dbed)]


<a name="3.0.0"></a>

## 3.0.0 (2019-05-02)

### Added

- ✨ Use Clipboard with listener to apply copied text [[8a706ad](https://github.com/dsznajder/react-native-otp-inputs/commit/8a706adee141c99cbfad26943aa14a9767d84f88)]
- ✨ Add oneTimeCode textContentType for iOS &gt;&#x3D; 12 [[f55b695](https://github.com/dsznajder/react-native-otp-inputs/commit/f55b695d300c573b52cfab7c6a7b532e80edef26)]
- ✅ Update Tests and snapshots [[999bd03](https://github.com/dsznajder/react-native-otp-inputs/commit/999bd0344ddb11aa46e645ba1913c104404ec930)]

### Changed

- ⬆️ Update dev Dependencies [[64daee5](https://github.com/dsznajder/react-native-otp-inputs/commit/64daee52d98b6bd5488c1fecd0fabf2efe27f7d9)]
- ⬆️ Update devDependencies [[e2a0ce7](https://github.com/dsznajder/react-native-otp-inputs/commit/e2a0ce75044124096d97722eea79732265617e2c)]
- 💄 Update default styling [[3d104bb](https://github.com/dsznajder/react-native-otp-inputs/commit/3d104bb18df5aaefc18dde0c9f4e512a643d9b41)]
- 🚚 Move files to src folder [[c4a8446](https://github.com/dsznajder/react-native-otp-inputs/commit/c4a84467e50f7b7309fd814b5c4628053ac5d5ed)]
- 🔧 Add release-it config [[94b9232](https://github.com/dsznajder/react-native-otp-inputs/commit/94b9232aa3f95ae824c4a74955290f0efd28c63f)]

### Miscellaneous

- 📝 Generate Changelog [[6736feb](https://github.com/dsznajder/react-native-otp-inputs/commit/6736febb71438c06d6d98ce334de9d7118cc9ac5)]
- Update documentation [[ef23f88](https://github.com/dsznajder/react-native-otp-inputs/commit/ef23f88d2d0f08236bbec3cba52f4529297680ec)]
- 🏷️ Change test files to .tsx [[a510972](https://github.com/dsznajder/react-native-otp-inputs/commit/a510972ed3464fedd375cb0ebb77bb63f9568910)]

<a name="2.0.1"></a>

## 2.0.1 (2019-04-06)

### Added

- ✅ Update tests to improve coverage [[eab5a4c](https://github.com/dsznajder/react-native-otp-inputs/commit/eab5a4cc70daa3442aa21c23b68a382967be013d)]

### Changed

- 🔧 Add additional config files [[fa6a26d](https://github.com/dsznajder/react-native-otp-inputs/commit/fa6a26d979409f500a6e2ca85d90cd4082797f65)]
- 🔧 Configure tests to use typescript files [[626e8ac](https://github.com/dsznajder/react-native-otp-inputs/commit/626e8accc02952f76bb16c3f8f85e91f28fc74d4)]

### Miscellaneous

- Update tests [[3a11f9b](https://github.com/dsznajder/react-native-otp-inputs/commit/3a11f9bee4c2588046535ac244d4d6b7a74ab7b0)]
- Update props inside README.md [[6e2b993](https://github.com/dsznajder/react-native-otp-inputs/commit/6e2b993c21ddf98ec7ecded0da7c2527916b562e)]
- Update tests and snapshots, add CI [[4a49723](https://github.com/dsznajder/react-native-otp-inputs/commit/4a4972382c2929c62b449ca2bd3acca91862f674)]
- Update readme [[9cee387](https://github.com/dsznajder/react-native-otp-inputs/commit/9cee387e2f3ba02a175548be83dcfb2cced221fd)]
- 👷 Add codecov [[d200415](https://github.com/dsznajder/react-native-otp-inputs/commit/d200415f6a122f3f0ca97c971755cf0fab301023)]
- 🏷️ Update typings [[9c5014b](https://github.com/dsznajder/react-native-otp-inputs/commit/9c5014bb0d47ea7e7e0a2f241b02354d4fccac79)]
- 📝 Add badges [[ff687bf](https://github.com/dsznajder/react-native-otp-inputs/commit/ff687bfa1b897a714468b3f93a1cb2ee43e401cd)]
- 🚀 Use &#x27;bob&#x27; for preparing publish [[f356b3c](https://github.com/dsznajder/react-native-otp-inputs/commit/f356b3c751198f5ccb88391e72b9256186fe521b)]
- Release 2.0.1 [[8ee129b](https://github.com/dsznajder/react-native-otp-inputs/commit/8ee129b9a773046cbc0ee84156cd5f99584d5010)]

<a name="2.0.0"></a>

## 2.0.0 (2019-03-09)

### Miscellaneous

- Update js.map [[3ecf968](https://github.com/dsznajder/react-native-otp-inputs/commit/3ecf9686d6cb3c36f40281ab0842bb1e3a2a1369)]
- Remove unnecessary &#x60;dist&#x60; files [[fd4ccec](https://github.com/dsznajder/react-native-otp-inputs/commit/fd4ccec94c15a6ca1748a19c9e7f71c273c352f1)]
- Lock &#x60;merge&#x60; package [[1c343a6](https://github.com/dsznajder/react-native-otp-inputs/commit/1c343a66484cf5dc07894643edb207818f855328)]
- Add possibility to paste otpCode [[c154c7a](https://github.com/dsznajder/react-native-otp-inputs/commit/c154c7a7ed954e129b9f09d3b5978d38ccb7dfdb)]

<a name="1.1.0"></a>

## 1.1.0 (2019-01-17)

### Miscellaneous

- Initial [[080018f](https://github.com/dsznajder/react-native-otp-inputs/commit/080018f4bd39100f79dbca7baabcee28cb0ad1a3)]
- OTP Package [[4059e64](https://github.com/dsznajder/react-native-otp-inputs/commit/4059e64bba5639c3a4ca138d70cf233f20cc1ce5)]
- Add basic Readme with props description [[65b8877](https://github.com/dsznajder/react-native-otp-inputs/commit/65b8877cc0694586301c4a273d043cb4c4fbe7a6)]
- Babel transpilation [[755065f](https://github.com/dsznajder/react-native-otp-inputs/commit/755065f8bbf46114996a492e81d87105cf33c33a)]
- Proper method for rendering inputs [[69e5cd8](https://github.com/dsznajder/react-native-otp-inputs/commit/69e5cd81d070ad2ac1d915ff6718110aab38aa0f)]
- v0.1.1 [[330dfde](https://github.com/dsznajder/react-native-otp-inputs/commit/330dfde98b492db703470c77a7b4f54643f35cc3)]
- Add better styling, fix imports on build [[65e0540](https://github.com/dsznajder/react-native-otp-inputs/commit/65e0540c2ee8c6854e368bc96aba973e0e8b0e29)]
- v0.1.2 [[f15e470](https://github.com/dsznajder/react-native-otp-inputs/commit/f15e470824ff4dd351f01d44eb70032ab1c35ccb)]
- v0.1.3 [[76c019b](https://github.com/dsznajder/react-native-otp-inputs/commit/76c019b6afb00fc761f8e07b3e03a76c9662befd)]
- Add function callback for getting otp values, justify + align input text [[a8cab0d](https://github.com/dsznajder/react-native-otp-inputs/commit/a8cab0d8acb2edbdf29070294d10f3d2772d47b9)]
- v0.2.0 [[3e5e849](https://github.com/dsznajder/react-native-otp-inputs/commit/3e5e849ef2e81201ddaebee2f8c31cc58ac11b17)]
- Add proper styling for inputs content. Add callback on removing otpCode [[fb85696](https://github.com/dsznajder/react-native-otp-inputs/commit/fb85696deddae27e73bd3e25b0cfe6cac6715a0e)]
- v0.3.0 [[d2cb423](https://github.com/dsznajder/react-native-otp-inputs/commit/d2cb4231eaef64e001d21d7a557353c032407e06)]
- Add key for rendering OtpInputs [[9d1acd0](https://github.com/dsznajder/react-native-otp-inputs/commit/9d1acd04667776bf8d47cb9f1c5c60b01199ed06)]
- v0.3.1 [[204ec4d](https://github.com/dsznajder/react-native-otp-inputs/commit/204ec4dc96260621d9fa6bc35e45cf9d069738d4)]
- Update README.md [[befbbbb](https://github.com/dsznajder/react-native-otp-inputs/commit/befbbbbfb342703c6712ced119ac5a8a6aeeaec9)]
- Add tests, fix props on OtpInput error, goal 100% coverage [[2ecfea4](https://github.com/dsznajder/react-native-otp-inputs/commit/2ecfea4bfdb62d188718682ff0f90595b29cf31e)]
- v0.4.0 [[2a156a8](https://github.com/dsznajder/react-native-otp-inputs/commit/2a156a877a781f2d7f27c866958c46557bd07284)]
- Change unfocused border color from fixed string to default prop [[da816b8](https://github.com/dsznajder/react-native-otp-inputs/commit/da816b84971d027195e021e38e484afcbc52c597)]
- v0.5.0 [[b4fc042](https://github.com/dsznajder/react-native-otp-inputs/commit/b4fc042d9aa452c81ffc06f516dc66482235028e)]
- Use ts for package [[f1b604e](https://github.com/dsznajder/react-native-otp-inputs/commit/f1b604ed4e4f9abe9f777c633ef1c34947802248)]
- Apply feedback [[26f6659](https://github.com/dsznajder/react-native-otp-inputs/commit/26f6659b2f188255b9a3459f395aa7cb00f857f7)]
- Add selectTextOnFocus and clearTextOnFocus props [[927ba04](https://github.com/dsznajder/react-native-otp-inputs/commit/927ba04865dda5e460c4046e77f6e979facd4d72)]
- 0.6.0 [[dc0376b](https://github.com/dsznajder/react-native-otp-inputs/commit/dc0376b292ed9a1da55edcc40c9bca64922211ea)]
- 0.6.1 [[5a9b2ba](https://github.com/dsznajder/react-native-otp-inputs/commit/5a9b2bae6fe742db927f91e20d5a796502bdc501)]
- Add keyboard type [[d1c2f49](https://github.com/dsznajder/react-native-otp-inputs/commit/d1c2f49d33cd62117f5eae61daf68ff7aa0e60c1)]
- 0.7.0 [[4ea3fbe](https://github.com/dsznajder/react-native-otp-inputs/commit/4ea3fbe3a79244898d263a6c72e20804e228c42d)]
- v1.0.0 [[884f3e9](https://github.com/dsznajder/react-native-otp-inputs/commit/884f3e9f32bba024594b75163c30c46b11cfc820)]
- Add &#x60;inputsContainerStyles&#x60; and &#x60;secureTextEntry&#x60; [[021d907](https://github.com/dsznajder/react-native-otp-inputs/commit/021d907c5f7dfd26b64c46727747cbb91baf924c)]
- Update Readme.md [[2ac8592](https://github.com/dsznajder/react-native-otp-inputs/commit/2ac85921dbb60437eea160715c64294bd5b55585)]
- v1.1.0 [[c572da6](https://github.com/dsznajder/react-native-otp-inputs/commit/c572da6c58a421ac5cd734019fe7b041b3e41625)]
