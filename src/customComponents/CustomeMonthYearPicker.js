import React, { useState } from "react";
import {
  View,
  Text,
  Modal,
  TouchableOpacity,
  Alert,
  StyleSheet,
} from "react-native";
import moment from "moment";
import MonthPicker from "react-native-month-picker";
import propTypes from "prop-types";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
// import CrossComponent from "../../../../../../assets/images/BookingIcons/CrossSVG";
import CrossComponent from "../../assets/images/BookingIcons/CrossSVG";

import BaseClass from "../utils/BaseClass";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
  },
  input: {
    backgroundColor: "white",
    paddingVertical: 12,
    paddingHorizontal: 20,
    borderWidth: 0.5,
    borderRadius: 5,
    width: "100%",
    marginVertical: 6,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  inputText: {
    fontSize: 16,
    fontWeight: "500",
  },
  contentContainer: {
    flexDirection: "column",
    justifyContent: "center",
    height: "100%",
    backgroundColor: "rgba(0,0,0,0.5)",
  },
  content: {
    backgroundColor: "#fff",
    marginHorizontal: 20,
    marginVertical: 70,
  },
  confirmButton: {
    borderWidth: 0.5,
    padding: 15,
    margin: 10,
    borderRadius: 5,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
});

export class CustomeMonthYearPicker extends BaseClass {
  constructor(props) {
    super(props);
    console.warn("visiblity is", props.isMonthYearPickerVisible);
    this.state = {
      modalVisiblePass: props.isMonthYearPickerVisible,
      // selectedDate:new Date(),
      value: undefined,
    };
  }

  // const [isOpen, toggleOpen] = useState(false);
  // const [value, onChange] = useState(null);

  dateConfirm = () => {
    const { value } = this.state;
    this.props.newDateMonthSelected(value);
  };


  crossButtonClick =()=>{
    console.warn("close model 1")

    this.props.crossPickerAction();


  }
  render() {
    const { modalVisiblePass, value } = this.state;
    return (
      <View style={styles.container}>
        <Modal transparent animationType="fade" visible={modalVisiblePass}>
          <View style={styles.contentContainer}>
            <View style={styles.content}>
              <TouchableOpacity
                hitSlop={{ top: wp(3), left: wp(3), bottom: wp(3) }}
                style={{
                  position: "absolute",
                  alignSelf: "flex-end",
                  marginTop:wp(-3),
                  zIndex: 100,
                  elevation: 100,
                }}
                onPress={() => {
                  this.crossButtonClick();
                }}
              >
                <CrossComponent />
              </TouchableOpacity>
              <MonthPicker
                selectedDate={value || new Date()}
                onMonthChange={(value1) => this.setState({ value: value1 })}
              />
              <TouchableOpacity
                style={styles.confirmButton}
                onPress={(value) => this.dateConfirm()}
              >
                <Text>Confirm</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

// dummyScreen.defaultProps = {
//   placeholder: 'Select date',
// };
