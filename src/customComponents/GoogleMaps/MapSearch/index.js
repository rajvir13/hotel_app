import React from 'react';
import { Image, Text } from 'react-native';
import { ICONS } from "../../../utils/ImagePaths";
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ShadowViewContainer } from "../../../utils/BaseStyle";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'


const homePlace = { description: 'Home', geometry: { location: { lat: 48.8152937, lng: 2.4597668 } }};
const workPlace = { description: 'Work', geometry: { location: { lat: 48.8496818, lng: 2.2940881 } }};

const GooglePlacesInput = (props) =>  {

        return (
          <ShadowViewContainer>
            <GooglePlacesAutocomplete
              placeholder='Hotel Address'
              minLength={2} // minimum length of text to search
              autoFocus={false}
              returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
              keyboardAppearance={'light'} // Can be left out for default keyboardAppearance https://facebook.github.io/react-native/docs/textinput.html#keyboardappearance
              listViewDisplayed='auto'    // true/false/undefined
              fetchDetails={true}
              renderDescription={row => row.description} // custom description render
              onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                console.warn(data, details);
              }}

              getDefaultValue={() => ''}

              query={{
                // available options: https://developers.google.com/places/web-service/autocomplete
                key: 'AIzaSyDF5kIjSa5_LL91rHzKt6VVtXrKC25UEbQ',
                language: 'en', // language of the results
                types: '(cities)' // default: 'geocode'
                // types: '(hotels)'
              }}

              styles={{
                textInputContainer: {
                  width: '100%',backgroundColor:'white',borderRadius:10,
                  padding:0,margin:0,paddingHorizontal: wp(1),paddingVertical: wp(1),
                  borderTopColor:'white'
                },
                description: {
                  fontWeight: 'bold'
                },
                predefinedPlacesDescription: {
                  color: 'black'// color: '#1faadb'
                }
              }}

              currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
              currentLocationLabel="Current location"
              nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
              GoogleReverseGeocodingQuery={{
                // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
              }}
              GooglePlacesSearchQuery={{
                // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
                rankby: 'distance',
                type: 'cafe'
              }}

              GooglePlacesDetailsQuery={{
                // available options for GooglePlacesDetails API : https://developers.google.com/places/web-service/details
                fields: 'formatted_address',
              }}

              filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
              predefinedPlaces={[homePlace, workPlace]}
              // predefinedPlaces={[]}

              debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
              renderLeftButton={()  => <Image
                //source={require('path/custom/left-icon')}
                 />}
              renderRightButton={() =>
              <TouchableOpacity
              onPress={() => props.MoveToMap()}
               >
                 <Image
                source={ICONS.MAP_ICON}
                 />
                 </TouchableOpacity>}
            />
            </ShadowViewContainer>
          );


};
export default GooglePlacesInput
