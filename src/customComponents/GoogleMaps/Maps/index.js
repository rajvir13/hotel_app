/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import React from "react";
import { View, StatusBar, Text, DeviceEventEmitter } from "react-native";
import BaseClass from "../../../utils/BaseClass";
import { SafeAreaViewContainer, MainContainer } from "../../../utils/BaseStyle";
import GAutoComplete from "../MapSearch";
import styles from "./styles";
import COLORS from "../../../utils/Colors";
import { LeftIcon } from "../../../customComponents/icons";
import { FONT } from "../../../utils/FontSizes";
import STRINGS from "../../../utils/Strings";
import { currentLocation } from "../../FetchLocation";

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Header, Input } from "react-native-elements";
import MapView, { PROVIDER_GOOGLE } from "react-native-maps";
import Geolocation from "@react-native-community/geolocation";
import { TouchableOpacity } from "react-native-gesture-handler";
import * as DeviceInfo from "react-native-device-info";
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import { FONT_FAMILY } from "../../../utils/Font";

const ASPECT_RATIO = wp(100) / hp(100);
const LATITUDE = 0;
const LONGITUDE = 0;
const LATITUDE_DELTA = 1.0922;
// const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
const LONGITUDE_DELTA = 1.0922;
const GOOGLE_MAPS_APIKEY = "AIzaSyCVMy9UnWEogD7U9a85pF3tQeDsIgxRKY0";

// Geolocation.setRNConfiguration(config);
export default class GMaps extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      },
      markers: [],
      lat: "",
      lng: "",
    };
  }

  componentDidMount() {
    // Geolocation.getCurrentPosition(
    //     position => {
    //         this.setState({
    //             region: {
    //                 latitude: position.coords.latitude,
    //                 longitude: position.coords.longitude,
    //                 latitudeDelta: LATITUDE_DELTA,
    //                 longitudeDelta: LONGITUDE_DELTA,
    //             }
    //         });
    //     },
    //     (error) => console.log(error.message),
    //     {enableHighAccuracy: true, timeout: 20000, maximumAge: 1000},
    // );
    // this.watchID = Geolocation.watchPosition(
    //     position => {
    //         this.setState({
    //             region: {
    //                 latitude: position.coords.latitude,
    //                 longitude: position.coords.longitude,
    //                 latitudeDelta: LATITUDE_DELTA,
    //                 longitudeDelta: LONGITUDE_DELTA,
    //             }
    //         });
    //     }
    // );
    this.getCurrentLocation();
  }

  getCurrentLocation() {
    currentLocation.fetchLocation((position) => {
      if (
        Platform.OS === "ios" &&
        position !== false &&
        position !== undefined
      ) {
        if (position.message !== "User denied access to location services.") {
          this.setState({
            region: {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA,
            },
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          });
        } else {
          DeviceInfo.getAvailableLocationProviders().then((providers) => {
            if (providers.gps || providers.network) {
              Geolocation.getCurrentPosition(
                (position) => {
                  this.setState({
                    region: {
                      latitude: position.coords.latitude,
                      longitude: position.coords.longitude,
                      latitudeDelta: LATITUDE_DELTA,
                      longitudeDelta: LONGITUDE_DELTA,
                    },
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                  });
                },
                (error) =>
                  console.warn(
                    "Please turn on device GPS for using location services.",
                    error
                  )
              );
            }
          });
        }
      } else if (Platform.OS === "android" && position !== false) {
        DeviceInfo.getAvailableLocationProviders().then((providers) => {
          if (!providers.gps && !providers.network) {
            LocationServicesDialogBox.checkLocationServicesIsEnabled({
              message:
                "<h2 style='color: #0af13e'>Use Location ?</h2>This app wants to change your device settings:<br/><br/>Please enable GPS/location services for better experience<br/><br/>",
              ok: "YES",
              cancel: "NO",
              enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
              showDialog: true, // false => Opens the Location access page directly
              openLocationServices: true, // false => Directly catch method is called if location services are turned off
              preventOutSideTouch: false, //true => To prevent the location services popup from closing when it is clicked outside
              preventBackClick: false, //true => To prevent the location services popup from closing when it is clicked back button
              providerListener: true, // true ==> Trigger "locationProviderStatusChange" listener when the location state changes
            })
              .then(
                function (success) {
                  console.warn("success message", success);
                  // success => {alreadyEnabled: true, enabled: true, status: "enabled"}
                  Geolocation.getCurrentPosition(
                    (position) => {
                      this.setState({
                        region: {
                          latitude: position.coords.latitude,
                          longitude: position.coords.longitude,
                          latitudeDelta: LATITUDE_DELTA,
                          longitudeDelta: LONGITUDE_DELTA,
                        },
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                      });
                    },
                    (error) =>
                      console.warn(
                        "Please turn on device GPS for using location services.",
                        error
                      )
                  );
                }.bind(this)
              )
              .catch((error) => {
                console.warn("error", error.message);
              });

            DeviceEventEmitter.addListener(
              "locationProviderStatusChange",
              function (status) {
                // only trigger when "providerListener" is enabled
                console.log(status); //  status => {enabled: false, status: "disabled"} or {enabled: true, status: "enabled"}
              }
            );

            /** ------------------------------- */
          } else if (providers.gps || providers.network) {
            Geolocation.getCurrentPosition(
              (position) => {
                this.setState({
                  region: {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                    latitudeDelta: LATITUDE_DELTA,
                    longitudeDelta: LONGITUDE_DELTA,
                  },
                  lat: position.coords.latitude,
                  lng: position.coords.longitude,
                });
              },
              (error) =>
                console.warn(
                  "Please turn on device GPS for using location services.",
                  error
                )
            );
          }
        });
      }
    });
  }

  // componentWillUnmount() {
  //     Geolocation.clearWatch(this.watchID);
  // }

  // =============================================================================================
  // Render method for Header
  // =============================================================================================

  _renderHeader() {
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.backGround_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={<LeftIcon onPress={() => navigation.goBack()} />}
        centerComponent={{
          text: "Address",
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        rightComponent={
          <TouchableOpacity onPress={() => this.getPlaceName()}>
            <Text>Done</Text>
          </TouchableOpacity>
        }
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  getPlaceName = () => {
    const { lat, lng } = this.state;
    const { navigation } = this.props;
    return fetch(
      "https://maps.googleapis.com/maps/api/geocode/json?address=" +
        lat +
        "," +
        lng +
        "&key=" +
        GOOGLE_MAPS_APIKEY
    )
      .then((response) => response.json())
      .then((responseJson) => {
        console.warn(
          "ADDRESS GEOCODE is BACK!! => " + JSON.stringify(responseJson)
        );
        this.props.route.params.callback(lat, lng, responseJson);
        console.warn(this.props.route.params.callback);
        // navigation.pop()
        this.props.navigation.goBack();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  onMarkerPress = (event) => {
    const { coordinate } = event.nativeEvent;
    const { region } = this.state;
    const newRegion = { ...region };

    newRegion.latitude = coordinate.latitude;
    newRegion.longitude = coordinate.longitude;
    newRegion.latitudeDelta = LATITUDE_DELTA;
    newRegion.longitudeDelta = LONGITUDE_DELTA;

    this.setState({
      region: newRegion,
      lat: coordinate.latitude,
      lng: coordinate.longitude,
    });
    this.refs.mapView.animateToRegion(newRegion);
  };

  render() {
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <MainContainer>
          <MapView
            ref="mapView"
            // provider={PROVIDER_GOOGLE}
            // mapPadding={{top: 50, right: 0, bottom: 50, left: 0}}
            style={styles.map}
            region={this.state.region}
            moveOnMarkerPress={true}
            showsUserLocation={true}
            zoomEnabled={true}
            enableZoomControl={true}
            zoomTapEnabled={true}
            // onRegionChange={ region => this.setState({region}) }
            // onRegionChangeComplete={ region => this.setState({region}) }
            onPress={(e) => this.onMarkerPress(e)}
          >
            <MapView.Marker
              // onDragEnd={(e) => {
              //     console.warn("fsvdkfk", e)
              //     this.SendLatLong(e.nativeEvent.coordinate.latitude, e.nativeEvent.coordinate.longitude)
              // }}
              coordinate={this.state.region}
              // coordinate={{
              //     latitude: this.state.lat,
              //     longitude: this.state.lng
              // }}
              // title={"title"}
              // description={"description"}
            />
          </MapView>
        </MainContainer>
      </SafeAreaViewContainer>
    );
  }
}
