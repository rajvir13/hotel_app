// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react";
import {
  View,
  FlatList,
  Text,
  
  Platform,
  StatusBar,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  Dimensions,
} from "react-native";
import { Image } from 'react-native-elements';

import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Header } from "react-native-elements";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";
import { CommonActions } from "@react-navigation/native";
import * as _ from "lodash";
import { SearchBar } from "react-native-elements";

import BaseClass from "../../../utils/BaseClass";
import {
  ShadowViewContainer,
  SafeAreaViewContainer,
  MainContainer,
  ScrollContainer,
} from "../../../utils/BaseStyle";
import { Spacer } from "../../../customComponents/Spacer";
import COLORS from "../../../utils/Colors";
import {
  ListingIcon,
  LocationIcon,
  RightIcon,
} from "../../../customComponents/icons";
import STRINGS from "../../../utils/Strings";
import { FONT } from "../../../utils/FontSizes";
import SearchIconSVG from "../../../../assets/images/CustomerHome/searchIcon.svg";
import { FONT_FAMILY } from "../../../utils/Font";
import { LogoutAction } from "../../../redux/actions/LogoutAction";
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import styles from "./styles";
import { NearByAction } from "../../../redux/actions/CustomerHomeNearByAction";
import { FilterData } from "../../../utils/FixtureData";
import CustomerHomeFilter from "./component/filter";
import RenderHotelItem from "./component/renderHotelItem";
import RoomTypeDropdown from "./component/roomTypeDropDown";
import { GetRoomTypeAction } from "../../../redux/actions/HotelServices";
import MapComponent from "./component/mapComponent";
import Geolocation from "@react-native-community/geolocation";
import { API } from "../../../redux/constant";
import { ICONS } from "../../../utils/ImagePaths";
// import Menu, { MenuItem, MenuDivider } from "react-native-material-menu";
import FastImage from "react-native-fast-image";
import { Dropdown } from "react-native-material-dropdown";
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from "react-native-popup-menu";

let filterIndex = undefined;
let roomTypeIndex = undefined;
let roomType = undefined;
const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

const LATITUDE_DELTA = 0.0122;
const LONGITUDE_DELTA =
  (Dimensions.get("window").width / Dimensions.get("window").height) * 0.0122;

//======================M4 final Code=====================================

class CustomerHome extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      searchText: "",
      isFirst: false,
      totalHotelFound: 0,
      filterData: [],
      hotelDetail: [
        {
          hotel_images: [
            {
              image:
                "https://images.unsplash.com/photo-1520250497591-112f2f40a3f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80",
            },
          ],
          name: "Grand Royal Hotel",
          address: "Hotel Grand Royal London",
          rating: 4,
          review: "160 Reviews",
          distance: "2",
          discount: "20% Discount",
          min_discounted_price: "200",
          min_regular_price: "300",
        },
      ],
      displayNoOfRoomsModal: false,
      isCountryDropDownVisible: false,
      // searchData: [],
      regionType: "Country",
      accessToken: "",
      isLogOut: false,
      isRoomType: false,
      roomTypeData: [
        { value: "Single", isChecked: false },
        { value: "Double", isChecked: false },
        { value: "Triple", isChecked: false },
        { value: "Quad", isChecked: false },
        { value: "Twin", isChecked: false },
        { value: "All", isChecked: false },
      ],
      filterArr: [],
      isRoomTypeSelected: false,
      filteredListArray: [],

      region: {
        latitude: 0.0,
        longitude: 0.0,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
        distance: "2 miles away",
      },
      isDestinationSelected: false,
      destinationHotel: {
        latitude: undefined,
        longitude: undefined,
      },

      sourceHotel: {
        latitude: undefined,
        longitude: undefined,
      },
      toggleMapView: true,
      mapHotelData: [],
      user_profile: undefined,
      isMapRoomType: false,
      isMapSorting: false,
    };
  }

  componentDidMount() {
    // this.props.navigation.navigate('hotelListOnMap');
    this.currentLocationForMap();
    AsyncStorage.getItem(STRINGS.loginToken, (error, result) => {
      if (result !== undefined && result !== null) {
        let accessToken = JSON.parse(result);
        if (accessToken !== undefined) {
          this.setState({ accessToken: accessToken });
          this.GetRoomType(accessToken);
          AsyncStorage.getItem(STRINGS.latlong, (error, result) => {
            if (result !== undefined && result !== null) {
              let latlong = JSON.parse(result);
              if (latlong !== undefined) {
                this.setState({
                  filterArr: FilterData,
                });
                NearByAction(
                  {
                    accessToken: accessToken,
                    lat: latlong.latitude,
                    long: latlong.longitude,
                  },
                  (data) => this.getNearByApiData(data)
                );
                this.showDialog();
              }
            } else {
              NearByAction(
                {
                  accessToken: accessToken,
                  lat: this.state.region.latitude,
                  long: this.state.region.longitude,
                },
                (data) => this.getNearByApiData(data)
              );
              this.showDialog();
            }
          });
        }
      }
    });
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.onFocusFunction();
    });
    this.asyncDataGettingMethod();
  }

  asyncDataGettingMethod = () => {
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let response = JSON.parse(result);
        if (response !== undefined) {
          if (response.profile_image !== "") {
            this.setState({
              user_profile: API.IMAGE_BASE_URL1 + response.profile_image,
            });
          }
        }
      }
    });
  };

  componentWillReceiveProps(nextProps, nextContext) {
    const { isLogOut } = this.state;
    const { navigation } = this.props;
    const { logoutResponse } = nextProps.logoutState;
    if (logoutResponse === "Network request failed") {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      const { message, status } = logoutResponse;
      if (status === 200) {
        this.hideDialog();
        this.setState({
          isLogOut: !isLogOut,
        });
        AsyncStorage.removeItem(STRINGS.loginToken);
        AsyncStorage.removeItem(STRINGS.loginData);
        AsyncStorage.removeItem(STRINGS.loginCredentials);
        AsyncStorage.removeItem(STRINGS.fcmToken);
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{ name: "login" }],
          })
        );
      } else if (status === 111) {
        this.hideDialog();
        console.warn(message);
      } else {
        this.showToastAlert(STRINGS.session_expired);
        this.hideDialog();
      }
    }
  }

  //******************* method call every time ************** */
  onFocusFunction = () => {
    let UseraccessToken;
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        UseraccessToken = loginData.access_token;
      }
    });

    AsyncStorage.getItem(STRINGS.latlong, (error, result) => {
      if (result !== undefined && result !== null) {
        let latlong = JSON.parse(result);
        if (latlong !== undefined) {
          this.setState({
            filterArr: FilterData,
          });
          NearByAction(
            {
              accessToken: UseraccessToken,
              lat: latlong.latitude,
              long: latlong.longitude,
            },
            (data) => this.getNearByApiData(data)
          );
          this.showDialog();
        }
      }
    });
    this.asyncDataGettingMethod();
  };

  //******************end here */

  componentWillUnmount() {
    this._unsubscribe();
    Geolocation.clearWatch(this.watchID);
  }

  currentLocationForMap = () => {
    Geolocation.getCurrentPosition(
      (position) => {
        if (position !== undefined) {
          this.setState({
            region: {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA,
            },
          });
        }
      },
      (error) => console.log(error.message),
      { enableHighAccuracy: false }
    );

    this.watchID = Geolocation.watchPosition((position) => {
     if(position!==undefined)
     {
      this.setState({
        region: {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        },
      });
     }
     
    });
  };

  getNearByApiData = (data) => {
    const { mapHotelData } = this.state;
    const { navigation } = this.props;
    this.hideDialog();
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      const { response, message, status } = data;
      if (response !== undefined) {
        if (status === 200) {
          this.hideDialog();
          // let data = response;
          response.map((item) => {
            if (item.hotel_images.length === 0) {
              item.hotel_images = [{ image: null }];
              return item;
            } else {
              return item;
            }
          });

          this.setState({
            hotelDetail: _.cloneDeep(response),
            filteredListArray: _.cloneDeep(response),
            mapHotelData: _.cloneDeep(response),
            totalHotelFound: response.length,
          });
          if (filterIndex !== undefined) {
            this.appliedFiltersAction(filterIndex);
          }

          if (roomType !== undefined && roomTypeIndex !== undefined) {
            this.dropdownAction(roomTypeIndex, roomType);
          }
        }
      } else if (status === 111) {
        if (message.non_field_errors !== undefined) {
          // if (message.non_field_errors[0] === "User with this social_id does not exist")
        } else {
          AsyncStorage.removeItem(STRINGS.loginCredentials);
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{ name: "login" }],
            })
          );
          this.showToastAlert(message);
        }
      } else if (status === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      }
    }
  };

  GetRoomType = (tokenAuth) => {
    let payload = {
      authToken: tokenAuth,
    };
    GetRoomTypeAction(payload, (res) => this.RoomTypeApiResponse(res));
  };

  RoomTypeApiResponse(response1) {
    if (response1.response !== undefined && response1.status === 200) {
      let tempRoomType = [];

      response1.response.forEach((item) => {
        let aa = {
          value: item.name,
          id: item.id,
        };

        tempRoomType.push(aa);
      });
      // this.setState({ roomTypeData: tempRoomType });
    } else this.showToastAlert("server error");
  }

  ratingCompleted(rating) {
    // console.log("Rating is: " + rating)
  }

  logoutUser = () => {
    const { isLogOut, accessToken } = this.state;
    Alert.alert(
      "Hotel App",
      STRINGS.EXIT_ALERT,
      [
        {
          text: "Cancel",
          style: "cancel",
        },
        {
          text: "OK",
          onPress: () => {
            this.setState({ isLogOut: !isLogOut });
            this.props.logout({ accessToken });
            // AsyncStorage.removeItem(STRINGS.loginToken);
            // AsyncStorage.removeItem(STRINGS.loginData);
            // AsyncStorage.removeItem(STRINGS.loginCredentials);
            this.showDialog();
          },
        },
      ],
      { cancelable: false }
    );
  };

  /** Filter Menu Rendering and Functions */
  onMenuPress = (index) => {
    const { filterArr } = this.state;

    this.setState({
      filterArr: _.map(filterArr, (data, arrIndex) => {
        if (index === arrIndex) {
          data.isChecked = true;

          return data;
        } else {
          data.isChecked = false;
          return data;
        }
      }),
    });

    this.appliedFiltersAction(index);
  };

  appliedFiltersAction = (index) => {
    filterIndex = index;
    const { hotelDetail, filteredListArray, totalHotelFound } = this.state;
    let tempArr = [];
    if (hotelDetail.length > 0) {
      switch (index) {
        case 0:
          tempArr = hotelDetail.sort(
            (a, b) =>
              Math.floor(b.avg_rating.rating__avg) -
              Math.floor(a.avg_rating.rating__avg)
          );
          break;
        case 1:
          tempArr = hotelDetail.sort(
            (a, b) =>
              parseFloat(b.min_regular_price) - parseFloat(a.min_regular_price)
          );
          break;
        case 2:
          tempArr = hotelDetail.sort(
            (a, b) =>
              parseFloat(a.min_regular_price) - parseFloat(b.min_regular_price)
          );
          break;
        case 3:
          tempArr = hotelDetail;
          break;
        default:
          break;
      }
      console.log("total hotel is", tempArr.length);
      this.setState({
        filteredListArray: tempArr,
        mapHotelData: tempArr,
        totalHotelFound: tempArr.length,
      });
    }
  };

  appliedFiltersActionOnMap = (index) => {
    const { mapHotelData } = this.state;
    let filteredMapListArray = _.cloneDeep(mapHotelData);
    let tempArr = [];
    if (mapHotelData.length > 0) {
      switch (index) {
        case 0:
          this.showToastAlert(STRINGS.COMING_SOON);
          tempArr = mapHotelData;
          // tempArr = filteredMapListArray.sort((a, b) => parseFloat(b.rating) - parseFloat(a.rating));
          break;
        case 1:
          tempArr = filteredMapListArray.sort(
            (a, b) =>
              parseFloat(b.min_regular_price) - parseFloat(a.min_regular_price)
          );
          break;
        case 2:
          tempArr = filteredMapListArray.sort(
            (a, b) =>
              parseFloat(a.min_regular_price) - parseFloat(b.min_regular_price)
          );
          break;
        case 3:
          tempArr = mapHotelData;
          break;
        default:
          break;
      }
      this.setState({
        mapHotelData: tempArr,
        isMapSorting: true,
      });
    }
  };

  onHotelItemPress = (id) => {
    // const {accessToken} = this.state;
    this.props.navigation.navigate("particularHotelProfile", {
      id: id,
      // accessToken: accessToken
    });
  };

  dropdownAction = (value, type) => {
    roomTypeIndex = value;
    roomType = type;

    const { hotelDetail, filteredListArray, totalHotelFound } = this.state;
    let tempArr = [];

    switch (value) {
      case 0:
        hotelDetail.filter((item) =>
          item.room_hotel.filter((item1) => {
            if (item1.room_type === "Single") {
              tempArr.push(item);
            }
          })
        );
        break;
      case 1:
        hotelDetail.filter((item) =>
          item.room_hotel.filter((item1) => {
            if (item1.room_type === "Double") {
              tempArr.push(item);
            }
          })
        );
        break;
      case 2:
        hotelDetail.filter((item) =>
          item.room_hotel.filter((item1) => {
            if (item1.room_type === "Triple") {
              tempArr.push(item);
            }
          })
        );

        break;
      case 3:
        hotelDetail.filter((item) =>
          item.room_hotel.filter((item1) => {
            if (item1.room_type === "Quad") {
              tempArr.push(item);
            }
          })
        );
        break;
      case 4:
        hotelDetail.filter((item) =>
          item.room_hotel.filter((item1) => {
            if (item1.room_type === "Twin") {
              tempArr.push(item);
            }
          })
        );
        break;
      case 5:
        tempArr = hotelDetail;
        break;
      default:
        break;
    }

    if (type === "map") {
      this.setState({ isMapRoomType: true, mapHotelData: tempArr });
    } else {
      this.setState({
        isRoomType: true,
        filteredListArray: tempArr,
        totalHotelFound: tempArr.length,
      });
    }
  };

  /** ================================================================================
     // ================================== MAP Functions ===============================
     // ================================================================================ */

  //================================= hotel details click===========================

  //================================== hotel direction click=========================
  onDirectionPress = (id) => {
    const { mapHotelData } = this.state;

    mapHotelData.map((item, index) => {
      if (item.id === id) {
        this.setState({
          destinationHotel: {
            latitude: item.latitude,
            longitude: item.longitude,
          },
          isDestinationSelected: true,
        });
      }
    });
  };

  showUserProfile = (userPic) => {
    if (userPic !== undefined && userPic !== null) {
      this.props.navigation.navigate("ShowFullViewImg", {
        imgUrl: userPic,
      });
    }
  };

  _renderHeader() {
    const { toggleMapView, user_profile } = this.state;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={
          <View>
            <View>
              <TouchableOpacity
                onPress={() => this.showUserProfile(user_profile)}
              >
                <Image
                  // resizeMode={'stretch'}
                  style={styles.drawerIconStyles}
                  source={
                    user_profile !== undefined && user_profile !== null
                      ? { uri: user_profile }
                      : ICONS.PROFILE_PLACEHOLDER
                  }
                />
              </TouchableOpacity>
            </View>
          </View>
        }
        rightComponent={
          <View style={{ flexDirection: "row", alignItems: "center" }}>
            {toggleMapView ? (
              <LocationIcon
                onPress={() => this.setState({ toggleMapView: false })}
              />
            ) : (
              <ListingIcon
                onPress={() => this.setState({ toggleMapView: true })}
              />
            )}
            <Spacer row={3} />
            <RightIcon onPress={() => this.logoutUser()} />
          </View>
        }
        centerComponent={{
          text: STRINGS.home_header_title,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  removeEmojis = (searchText) => {
    // emoji regex from the emoji-regex library
    const regex = /\uD83C\uDFF4(?:\uDB40\uDC67\uDB40\uDC62(?:\uDB40\uDC65\uDB40\uDC6E\uDB40\uDC67|\uDB40\uDC77\uDB40\uDC6C\uDB40\uDC73|\uDB40\uDC73\uDB40\uDC63\uDB40\uDC74)\uDB40\uDC7F|\u200D\u2620\uFE0F)|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC68(?:\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D)?\uD83D\uDC68|(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3]))|\uD83D\uDC69\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D(?:\uD83D[\uDC68\uDC69])|\uD83D[\uDC68\uDC69])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|\uD83D\uDC69\u200D\uD83D\uDC66\u200D\uD83D\uDC66|(?:\uD83D\uDC41\uFE0F\u200D\uD83D\uDDE8|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\uD83D\uDC68(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\u200D[\u2695\u2696\u2708])|(?:(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)\uFE0F|\uD83D\uDC6F|\uD83E[\uDD3C\uDDDE\uDDDF])\u200D[\u2640\u2642]|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDD6-\uDDDD])(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|\u200D[\u2640\u2642])|\uD83D\uDC69\u200D[\u2695\u2696\u2708])\uFE0F|\uD83D\uDC69\u200D\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D\uDC68(?:\u200D(?:(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D[\uDC66\uDC67])|\uD83C[\uDFFB-\uDFFF])|\uD83C\uDFF3\uFE0F\u200D\uD83C\uDF08|\uD83D\uDC69\u200D\uD83D\uDC67|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|\uD83D\uDC69\u200D\uD83D\uDC66|\uD83C\uDDF6\uD83C\uDDE6|\uD83C\uDDFD\uD83C\uDDF0|\uD83C\uDDF4\uD83C\uDDF2|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])|\uD83C\uDDED(?:\uD83C[\uDDF0\uDDF2\uDDF3\uDDF7\uDDF9\uDDFA])|\uD83C\uDDEC(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEE\uDDF1-\uDDF3\uDDF5-\uDDFA\uDDFC\uDDFE])|\uD83C\uDDEA(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDED\uDDF7-\uDDFA])|\uD83C\uDDE8(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDEE\uDDF0-\uDDF5\uDDF7\uDDFA-\uDDFF])|\uD83C\uDDF2(?:\uD83C[\uDDE6\uDDE8-\uDDED\uDDF0-\uDDFF])|\uD83C\uDDF3(?:\uD83C[\uDDE6\uDDE8\uDDEA-\uDDEC\uDDEE\uDDF1\uDDF4\uDDF5\uDDF7\uDDFA\uDDFF])|\uD83C\uDDFC(?:\uD83C[\uDDEB\uDDF8])|\uD83C\uDDFA(?:\uD83C[\uDDE6\uDDEC\uDDF2\uDDF3\uDDF8\uDDFE\uDDFF])|\uD83C\uDDF0(?:\uD83C[\uDDEA\uDDEC-\uDDEE\uDDF2\uDDF3\uDDF5\uDDF7\uDDFC\uDDFE\uDDFF])|\uD83C\uDDEF(?:\uD83C[\uDDEA\uDDF2\uDDF4\uDDF5])|\uD83C\uDDF8(?:\uD83C[\uDDE6-\uDDEA\uDDEC-\uDDF4\uDDF7-\uDDF9\uDDFB\uDDFD-\uDDFF])|\uD83C\uDDEE(?:\uD83C[\uDDE8-\uDDEA\uDDF1-\uDDF4\uDDF6-\uDDF9])|\uD83C\uDDFF(?:\uD83C[\uDDE6\uDDF2\uDDFC])|\uD83C\uDDEB(?:\uD83C[\uDDEE-\uDDF0\uDDF2\uDDF4\uDDF7])|\uD83C\uDDF5(?:\uD83C[\uDDE6\uDDEA-\uDDED\uDDF0-\uDDF3\uDDF7-\uDDF9\uDDFC\uDDFE])|\uD83C\uDDE9(?:\uD83C[\uDDEA\uDDEC\uDDEF\uDDF0\uDDF2\uDDF4\uDDFF])|\uD83C\uDDF9(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDED\uDDEF-\uDDF4\uDDF7\uDDF9\uDDFB\uDDFC\uDDFF])|\uD83C\uDDE7(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEF\uDDF1-\uDDF4\uDDF6-\uDDF9\uDDFB\uDDFC\uDDFE\uDDFF])|[#\*0-9]\uFE0F\u20E3|\uD83C\uDDF1(?:\uD83C[\uDDE6-\uDDE8\uDDEE\uDDF0\uDDF7-\uDDFB\uDDFE])|\uD83C\uDDE6(?:\uD83C[\uDDE8-\uDDEC\uDDEE\uDDF1\uDDF2\uDDF4\uDDF6-\uDDFA\uDDFC\uDDFD\uDDFF])|\uD83C\uDDF7(?:\uD83C[\uDDEA\uDDF4\uDDF8\uDDFA\uDDFC])|\uD83C\uDDFB(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDEE\uDDF3\uDDFA])|\uD83C\uDDFE(?:\uD83C[\uDDEA\uDDF9])|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDD6-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u261D\u270A-\u270D]|\uD83C[\uDF85\uDFC2\uDFC7]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66\uDC67\uDC70\uDC72\uDC74-\uDC76\uDC78\uDC7C\uDC83\uDC85\uDCAA\uDD74\uDD7A\uDD90\uDD95\uDD96\uDE4C\uDE4F\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD30-\uDD36\uDDB5\uDDB6\uDDD1-\uDDD5])(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u231A\u231B\u23E9-\u23EC\u23F0\u23F3\u25FD\u25FE\u2614\u2615\u2648-\u2653\u267F\u2693\u26A1\u26AA\u26AB\u26BD\u26BE\u26C4\u26C5\u26CE\u26D4\u26EA\u26F2\u26F3\u26F5\u26FA\u26FD\u2705\u270A\u270B\u2728\u274C\u274E\u2753-\u2755\u2757\u2795-\u2797\u27B0\u27BF\u2B1B\u2B1C\u2B50\u2B55]|\uD83C[\uDC04\uDCCF\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE1A\uDE2F\uDE32-\uDE36\uDE38-\uDE3A\uDE50\uDE51\uDF00-\uDF20\uDF2D-\uDF35\uDF37-\uDF7C\uDF7E-\uDF93\uDFA0-\uDFCA\uDFCF-\uDFD3\uDFE0-\uDFF0\uDFF4\uDFF8-\uDFFF]|\uD83D[\uDC00-\uDC3E\uDC40\uDC42-\uDCFC\uDCFF-\uDD3D\uDD4B-\uDD4E\uDD50-\uDD67\uDD7A\uDD95\uDD96\uDDA4\uDDFB-\uDE4F\uDE80-\uDEC5\uDECC\uDED0-\uDED2\uDEEB\uDEEC\uDEF4-\uDEF9]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD70\uDD73-\uDD76\uDD7A\uDD7C-\uDDA2\uDDB0-\uDDB9\uDDC0-\uDDC2\uDDD0-\uDDFF])|(?:[#*0-9\xA9\xAE\u203C\u2049\u2122\u2139\u2194-\u2199\u21A9\u21AA\u231A\u231B\u2328\u23CF\u23E9-\u23F3\u23F8-\u23FA\u24C2\u25AA\u25AB\u25B6\u25C0\u25FB-\u25FE\u2600-\u2604\u260E\u2611\u2614\u2615\u2618\u261D\u2620\u2622\u2623\u2626\u262A\u262E\u262F\u2638-\u263A\u2640\u2642\u2648-\u2653\u265F\u2660\u2663\u2665\u2666\u2668\u267B\u267E\u267F\u2692-\u2697\u2699\u269B\u269C\u26A0\u26A1\u26AA\u26AB\u26B0\u26B1\u26BD\u26BE\u26C4\u26C5\u26C8\u26CE\u26CF\u26D1\u26D3\u26D4\u26E9\u26EA\u26F0-\u26F5\u26F7-\u26FA\u26FD\u2702\u2705\u2708-\u270D\u270F\u2712\u2714\u2716\u271D\u2721\u2728\u2733\u2734\u2744\u2747\u274C\u274E\u2753-\u2755\u2757\u2763\u2764\u2795-\u2797\u27A1\u27B0\u27BF\u2934\u2935\u2B05-\u2B07\u2B1B\u2B1C\u2B50\u2B55\u3030\u303D\u3297\u3299]|\uD83C[\uDC04\uDCCF\uDD70\uDD71\uDD7E\uDD7F\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE02\uDE1A\uDE2F\uDE32-\uDE3A\uDE50\uDE51\uDF00-\uDF21\uDF24-\uDF93\uDF96\uDF97\uDF99-\uDF9B\uDF9E-\uDFF0\uDFF3-\uDFF5\uDFF7-\uDFFF]|\uD83D[\uDC00-\uDCFD\uDCFF-\uDD3D\uDD49-\uDD4E\uDD50-\uDD67\uDD6F\uDD70\uDD73-\uDD7A\uDD87\uDD8A-\uDD8D\uDD90\uDD95\uDD96\uDDA4\uDDA5\uDDA8\uDDB1\uDDB2\uDDBC\uDDC2-\uDDC4\uDDD1-\uDDD3\uDDDC-\uDDDE\uDDE1\uDDE3\uDDE8\uDDEF\uDDF3\uDDFA-\uDE4F\uDE80-\uDEC5\uDECB-\uDED2\uDEE0-\uDEE5\uDEE9\uDEEB\uDEEC\uDEF0\uDEF3-\uDEF9]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD70\uDD73-\uDD76\uDD7A\uDD7C-\uDDA2\uDDB0-\uDDB9\uDDC0-\uDDC2\uDDD0-\uDDFF])\uFE0F|(?:[\u261D\u26F9\u270A-\u270D]|\uD83C[\uDF85\uDFC2-\uDFC4\uDFC7\uDFCA-\uDFCC]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66-\uDC69\uDC6E\uDC70-\uDC78\uDC7C\uDC81-\uDC83\uDC85-\uDC87\uDCAA\uDD74\uDD75\uDD7A\uDD90\uDD95\uDD96\uDE45-\uDE47\uDE4B-\uDE4F\uDEA3\uDEB4-\uDEB6\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD26\uDD30-\uDD39\uDD3D\uDD3E\uDDB5\uDDB6\uDDB8\uDDB9\uDDD1-\uDDDD])/g;

    return searchText.replace(STRINGS.emojiText, "");
  };
  /*
   // =============================================================================================
   // Render method for List Item
   // =============================================================================================
   */

  searchData(text) {
    this.setState({
      // filterData: newData,
      searchText: text,
    });
  }
  //================================Search================================
  _renderSearch = () => {
    return (
      <View style={styles.myViewStyle}>
        <ShadowViewContainer style={{ marginBottom: 0, borderRadius: wp(20) }}>
          <TextInput
            style={styles.searchTextInputStyle}
            // editable={false}
            onChangeText={(text) => this.setState({ searchText: text })}
            placeholder={"Location,Hotel name"}
            // value={value}
          />
        </ShadowViewContainer>
        <Spacer row={1} />

        <SearchIconSVG />
      </View>
    );
  };

  //================================Search================================

  filterList = (ratingHotelData) => {
    return ratingHotelData.filter(
      (listItem) =>
        listItem.name
          .toLowerCase()
          .includes(this.state.searchText.toLowerCase()) ||
        listItem.address
          .toLowerCase()
          .includes(this.state.searchText.toLowerCase())
    );
  };

  _renderRoomView = () => {
    return (
      <>
        <View style={styles.roomView}>
          <Text style={styles.noOfRoomText}>Number of Rooms</Text>
          <Spacer row={2} />
          <View style={styles.viewHorizontalLine} />
          <Spacer row={2} />

          <Text style={styles.noOfAdultText}>1 Room - 2 Adults</Text>
        </View>
      </>
    );
  };

  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  //************Filter Code ************ */

  hotelFilter = () => {
    const { filterArr } = this.state;
    return (
      <View>
        <Menu>
          <MenuTrigger>
            <View style={{ flexDirection: "row" }}>
              <Text style={{ marginRight: 10 }}>Sort By</Text>
              <Image source={ICONS.FILTERS_ICON} />
            </View>
          </MenuTrigger>
          <MenuOptions>
            <MenuOption>
              {_.map(filterArr, (item1, index) => {
                return (
                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: wp("2%"),
                      marginBottom: wp("2%"),
                    }}
                  >
                    <TouchableOpacity onPress={() => this.onMenuPress(index)}>
                      <FastImage
                        resizeMode={FastImage.resizeMode.contain}
                        style={{
                          borderRadius: wp(1),
                          borderColor: COLORS.black_color,
                          borderWidth: wp(0.2),
                          width: wp(4.5),
                          height: wp(4.5),
                          justifyContent: "center",
                        }}
                        source={
                          item1.isChecked
                            ? ICONS.CHECKED_ICON
                            : ICONS.UNCHECKED_ICON
                        }
                      />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onMenuPress(index)}>
                      <Text
                        style={{
                          justifyContent: "center",

                          color: COLORS.black_color,
                          fontFamily: FONT_FAMILY.Montserrat,
                          fontSize: FONT.TextMedium,
                          alignSelf: "center",
                          marginLeft: wp("2%"),
                          marginRight: wp("2%"),
                        }}
                      >
                        {item1.value}
                      </Text>
                    </TouchableOpacity>
                  </View>
                );
              })}
            </MenuOption>
          </MenuOptions>
        </Menu>
      </View>
    );
  };
  EmptyListMessage = () => {
    return (
      <View>
        <Text
          style={{
            textAlign: "center",
            fontSize: FONT.TextNormal,
            fontFamily: FONT_FAMILY.MontserratBold,
          }}
        >
          NO DATA{" "}
        </Text>
      </View>
    );
  };

  filterSearchListCount = (ratingHotelData) => {
    var filterSearchData = ratingHotelData.filter(
      (listItem) =>
        listItem.name
          .toLowerCase()
          .includes(this.state.searchText.toLowerCase()) ||
        listItem.address
          .toLowerCase()
          .includes(this.state.searchText.toLowerCase())
    );

    return filterSearchData.length;
  };

  //**************end filter code *************** */

  render() {
    const {
      hotelDetail,
      filteredListArray,
      filterArr,
      isRoomType,
      roomTypeData,
      toggleMapView,
      destinationHotel,
      isDestinationSelected,
      region,
      mapHotelData,
      isMapRoomType,
      isMapSorting,
      totalHotelFound,
      searchText,
    } = this.state;

    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <DismissKeyboard>
          <ScrollContainer>
            <MainContainer>
              <Spacer space={2} />
              {this._renderSearch()}
              <View>
                {toggleMapView ? (
                  <>
                    <Spacer space={2} />
                    {/* {this._renderRoomView()} */}
                    <Spacer space={2} />
                    <View style={styles.outerView}>
                      <Spacer space={2} />
                      <RoomTypeDropdown
                        roomTypeData={roomTypeData}
                        isRoomType={isRoomType}
                        onRoomTypeSelect={(value, index) =>
                          // dropdownRenderRow(value)
                          // dropdownButton(value)
                          this.dropdownAction(index, "list")
                        }
                      />
                      <View style={styles.filterView}>
                        {searchText !== "" ? (
                          <Text style={{ justifyContent: "flex-start" }}>
                            {this.filterSearchListCount(filteredListArray)}{" "}
                            hotels found
                          </Text>
                        ) : (
                          <Text style={{ justifyContent: "flex-start" }}>
                            {totalHotelFound} hotels found
                          </Text>
                        )}

                        {this.hotelFilter()}
                        {/* <CustomerHomeFilter
                          filterArr={filterArr}
                          onMenuPress={(index) => this.onMenuPress(index)}
                        /> */}
                      </View>
                      <Spacer space={1} />
                      <View style={styles.viewLine} />
                      <Spacer space={2} />

                      {totalHotelFound !== 0 ? (
                        <FlatList
                          style={styles.listStyle}
                          extraData={this.state}
                          data={this.filterList(filteredListArray)}
                          keyExtractor={(item, index) => index}
                          ListEmptyComponent={() => this.EmptyListMessage()}
                          renderItem={({ item, index }) => (
                            <RenderHotelItem
                              item={item}
                              index={index}
                              onRatingClick={() => this.ratingCompleted()}
                              onItemPress={(id) => this.onHotelItemPress(id)}
                            />
                          )}
                        />
                      ) : (
                        <View>
                          <Text
                            style={{
                              textAlign: "center",
                              fontSize: FONT.TextNormal,
                              fontFamily: FONT_FAMILY.MontserratBold,
                            }}
                          >
                            NO DATA{" "}
                          </Text>
                        </View>
                      )}
                    </View>
                  </>
                ) : (
                  <View>
                    <MapComponent
                      destinationRegion={destinationHotel}
                      hotelData={this.filterList(mapHotelData)}
                      isDestinationSelected={isDestinationSelected}
                      onDetailPress={(id) => this.onHotelItemPress(id)}
                      onDirectionPress={(id) => this.onDirectionPress(id)}
                      region={region}
                      tempRoomType={roomTypeData}
                      sortingType={FilterData}
                      onSortingChoose={(item, index) =>
                        this.appliedFiltersActionOnMap(index)
                      }
                      roomTypeAction={(item, index) =>
                        this.dropdownAction(index, "map")
                      }
                      isSortingClicked={isMapSorting}
                      isMapRoomType={isMapRoomType}
                    />
                  </View>
                )}
              </View>
              {this._renderCustomLoader()}
            </MainContainer>
          </ScrollContainer>
        </DismissKeyboard>
      </SafeAreaViewContainer>
    );
  }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state) => ({
  logoutState: state.LogoutReducer,
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    logout: (payload) => dispatch(LogoutAction(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomerHome);
