import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
import {Platform, StyleSheet} from 'react-native';
import {FONT} from "../../../utils/FontSizes";
import COLORS from "../../../utils/Colors";
import { FONT_FAMILY } from "../../../utils/Font";

const styles = StyleSheet.create({


  outerView:{
    backgroundColor: COLORS.white_color,
    width: wp(100),
    justifyContent: "center",
    alignItems: "center",
  },
  searchTextInputStyle:{
    borderColor: 'gray', marginLeft: wp(2), width: wp(70), paddingVertical: wp(2.5)
  
  },

    drawerIconStyles:{
        width: wp(10),
        height: wp(10),
        marginLeft: 5,
        borderRadius: wp(10) / 2,
        borderWidth: wp(0.2),
        borderColor: COLORS.light_grey3
    },
    locationIconStyles:{
        width: wp(8), height: wp(8),
        marginRight: wp('3%'),
       alignSelf:"center"
    },
noOfRoomText:{
    alignItems: "center", fontFamily: FONT_FAMILY.Montserrat 
},
    filterView:{
        flexDirection: "row",
        padding: 5,
        justifyContent: "space-between",
        alignContent: "stretch",
        width: wp("90%"),

    },
    viewLine:{
        width: wp(100),
        height: wp(0.3),
        alignItems: "center",
        backgroundColor: COLORS.backGround_color,

    },
 listStyle:{
    width: wp(100), paddingBottom: wp(17) 
 },
 myViewStyle:{

    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },

locTextStyle:{

    borderColor: 'gray', marginLeft: wp(2), width: wp(70), paddingVertical: wp(2.5)
},
roomView:{
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",

},
viewHorizontalLine:{
    height: wp(8),
    justifyContent: "center",
    alignSelf: "center",
    width: wp(0.5),
    backgroundColor: COLORS.step_indicator_unfinished_circle,

},
noOfAdultText:{
    alignItems: "center",
    width: wp(45),
    paddingVertical: wp(2),
    fontFamily: FONT_FAMILY.Montserrat,

}

});

export default styles;
