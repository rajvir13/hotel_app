import React, { useState } from "react";
import * as _ from "lodash";
import { MainContainer, ScrollContainer } from "../../../../../utils/BaseStyle";
import { Spacer } from "../../../../../customComponents/Spacer";
import {
  RoomTypeAndSorting,
  SearchHotel,
} from "../../../mapView/components/mapSearch";
import {
  Image,
  Keyboard,
  Platform,
  Button,
  Modal,
  Alert,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  TouchableWithoutFeedback,PermissionsAndroid,
  View,
  alert,
} from "react-native";
import styles from "../../../mapView/styles";
import MapViewDirections from "react-native-maps-directions";
import MapView, { PROVIDER_DEFAULT, PROVIDER_GOOGLE } from "react-native-maps";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ICONS } from "../../../../../utils/ImagePaths";
import Geolocation from "@react-native-community/geolocation";

const APIKEY = "AIzaSyCVMy9UnWEogD7U9a85pF3tQeDsIgxRKY0";
// const DismissKeyboard = ({children}) => (
//     <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
//         {children}
//     </TouchableWithoutFeedback>
// );

const MapComponent = ({
  tempRoomType,
  sortingType,
  region,
  destinationRegion,
  onSortingChoose,
  isSortingClicked,
  isMapRoomType,
  roomTypeAction,
  hotelData,
  isDestinationSelected,
  onDirectionPress,
  onDetailPress,
}) => {
  const [isDetailDirectionModelVisible, setDetailDirectionModel] = useState(
    false
  );

  //========================== render google map and show user current locaiton and hotels =============================
  const _renderHotelOnMap = () => {
    const mapDirectionView = (
      <MapViewDirections
        origin={region}
        destination={destinationRegion}
        apikey={APIKEY}
        strokeWidth={3}
        strokeColor={"blue"}
        optimizeWaypoints={true}
        resetOnChange={false}
        precision={"high"}
      />
    );

    const markerDetailDirection = (marker) => {
   
      Alert.alert(
        "Hotel App",
        `Get details of ${marker.name}`,
        [
          { text: "Cancel", style: "cancel" },
          {
            text: "Detail",
            onPress: () => onDetailPress(marker.id),
          },
          { text: "Direction", onPress: () => onDirectionPress(marker.id) },
        ],
        { cancelable: false }
      );
    };

    const checkPermission = async() => {
      if (Platform.OS === "ios") {
        Geolocation.requestAuthorization();
        return true;
      }
       else {
        let granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: "App Geolocation Permission",
            message: "App needs access to your phone's location.",
          }
        );
      console.log("")
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          return true;
        } else {
          return false;
        }
      }
    };

    //=============================End detail direction=================================

    return (
      <View style={{ flex: 1 }}>
        {console.log("checkPermission",checkPermission())}
        {checkPermission() &&
          (Platform.OS === "ios" ? (
            <MapView
              provider={PROVIDER_GOOGLE}
              style={styles.container}
              showsUserLocation={true}
              zoomEnabled={true}
              initialRegion={region}
              showsMyLocationButton={true}
              followsUserLocation={true}
              showsCompass={true}
              toolbarEnabled={true}
              zoomControlEnabled={true}
              rotateEnabled={true}
              region={region}
              showsTraffic={true}
            >
              {_.map(hotelData, (marker) => (
                <MapView.Marker
                  key={marker.id}
                  coordinate={{
                    latitude: marker.latitude,
                    longitude: marker.longitude,
                  }}
                  style={{ zIndex: 5 }}
                  // title={marker.title}
                >
                  <Image
                    style={{ height: wp("14%"), width: wp("8%") }}
                    source={ICONS.HOTEL_APP_ICON}
                    // onLayout={() => this.setState({initialRender: false})}
                  />
                  <View style={styles.distanceFromUserView}>
                    <Text style={styles.distanceFromUserText}>
                      {marker.distance}
                    </Text>
                    <Text style={styles.distanceFromUserText}> km away</Text>
                  </View>

                  <MapView.Callout tooltip style={styles.directionDetailView}>
                    <MapView.CalloutSubview
                      onPress={() => onDirectionPress(marker.id)}
                    >
                      <TouchableOpacity>
                        <Text style={styles.directionDetailTextStyle}>
                          Direction
                        </Text>
                      </TouchableOpacity>
                    </MapView.CalloutSubview>

                    <MapView.CalloutSubview
                      onPress={() => onDetailPress(marker.id)}
                    >
                      <View style={styles.lineView} />
                      <TouchableOpacity>
                        <Text style={styles.directionDetailTextStyle}>
                          Detail
                        </Text>
                      </TouchableOpacity>
                    </MapView.CalloutSubview>
                  </MapView.Callout>
                </MapView.Marker>
              ))}

              <MapView.Marker coordinate={region} title={"your location"}>
                <Image
                  style={{ height: wp("12%"), width: wp("8%") }}
                  source={ICONS.USER_CURRENT_LOC_ICON}
                  // onLayout={() => this.setState({initialRender: false})}
                />
              </MapView.Marker>

              {console.log("isDestinationSelected", isDestinationSelected)}

              {isDestinationSelected === true && mapDirectionView}
            </MapView>
          ) : (
            //=============================================Android view===============================
            <MapView
              // provider={PROVIDER_GOOGLE}
              style={styles.container}
              showsUserLocation={true}
              zoomEnabled={true}
              showsMyLocationButton={true}
              initialRegion={region}
              followsUserLocation={true}
              showsCompass={true}
              toolbarEnabled={true}
              zoomControlEnabled={true}
              rotateEnabled={true}
              // region={region}
              showsTraffic={true}
            >
              {_.map(hotelData, (marker) => (
                <MapView.Marker
                  key={marker.id}
                  coordinate={{
                    latitude: marker.latitude,
                    longitude: marker.longitude,
                  }}
                  style={{ zIndex: 5 }}
                  onPress={() => markerDetailDirection(marker)}
                >
                  <Image
                    style={{ height: wp("14%"), width: wp("8%") }}
                    source={ICONS.HOTEL_APP_ICON}
                    // onLayout={() => this.setState({initialRender: false})}
                  />
                  <View style={styles.distanceFromUserView}>
                    <Text style={styles.distanceFromUserText}>
                      {marker.distance}
                    </Text>
                    <Text style={styles.distanceFromUserText}> km away</Text>
                  </View>
                </MapView.Marker>
              ))}

              <MapView.Marker coordinate={region} title={"your location"}>
                <Image
                  style={{ height: wp("12%"), width: wp("8%") }}
                  source={ICONS.USER_CURRENT_LOC_ICON}
                  // onLayout={() => this.setState({initialRender: false})}
                />
              </MapView.Marker>

              {console.log("isDestinationSelected", isDestinationSelected)}

              {isDestinationSelected === true && mapDirectionView}
            </MapView>
          ))}
      </View>
    );
  };

  return (
    <ScrollContainer>
      <MainContainer>
        <Spacer space={2} />
        {/*<SearchHotel/>*/}
        {/*<Spacer space={1}/>*/}
        <View style={styles.roomTypeAndSortingMainView}>
          <RoomTypeAndSorting
            lable={"Room Type"}
            data={tempRoomType}
            selectedRoomType={(item, index) => roomTypeAction(item, index)}
            isSortingClicked={isMapRoomType}
          />

          <RoomTypeAndSorting
            lable={"Sorting"}
            data={sortingType}
            selectedRoomType={(item, index) => onSortingChoose(item, index)}
            isSortingClicked={isSortingClicked}
          />
        </View>

        <Spacer space={1} />
        {_renderHotelOnMap()}
        {/*{_renderCustomLoader()}*/}
      </MainContainer>
    </ScrollContainer>
  );
};

export default MapComponent;

