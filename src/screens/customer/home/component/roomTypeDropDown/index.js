import React from 'react';
import COLORS from "../../../../../utils/Colors";
import {ShadowViewContainer} from "../../../../../utils/BaseStyle";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Dropdown} from "react-native-material-dropdown";

const RoomTypeDropdown = ({roomTypeData, isRoomType, onRoomTypeSelect}) => {

    /**
     * Set Salutation dropdown list
     * @param rowData
     * @returns {*}
     */



    // const dropdownRenderRow = (rowData) => {
    //     return (
    //         <TouchableOpacity>
    //             <View>
    //                 <Text allowFontScaling={false}
    //                       style={{fontSize: 16, marginVertical: 8, marginHorizontal: 5, color: COLORS.black_color}}>
    //                     {`${rowData}`}
    //                 </Text>
    //             </View>
    //         </TouchableOpacity>
    //     );
    // };

  
   
    return (
   
        <ShadowViewContainer style={{width: wp(90),}}>
            <Dropdown
                containerStyle={{
                    width: wp(90),
                    backgroundColor: COLORS.white_color,
                    borderRadius: 10,
                    justifyContent: 'center',
                    alignContent: 'center',
                    alignSelf: 'center',
                    paddingHorizontal: wp(2)
                }}
            
                label={!isRoomType && 'Room Type'}
                data={roomTypeData}
                visible={false}
                textStyle={{color: 'black'}}
                onChangeText={(value,index) => onRoomTypeSelect(value,index)}
            />
        </ShadowViewContainer>
    )
};

export default RoomTypeDropdown;
