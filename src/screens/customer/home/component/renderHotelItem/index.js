import React from "react";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { AirbnbRating } from "react-native-elements";
import { Text, TouchableOpacity, View } from "react-native";
import FastImage from "react-native-fast-image";
import { ShadowViewContainer } from "../../../../../utils/BaseStyle";

import { Spacer } from "../../../../../customComponents/Spacer";
import { FONT } from "../../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../../utils/Font";
import LocationIconSVG from "../../../../../../assets/images/CustomerHome/LocationIconSVG";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

const RenderHotelItem = ({ item, index, onItemPress, onRatingClick }) => {
  return (
    <ShadowViewContainer style={{ width: wp(92) }}>
      <TouchableOpacity onPress={() => onItemPress(item.id)}>
        <FastImage
          source={{
            uri:
              item.hotel_images[0].image !== null
                ? item.hotel_images[0].image
                : "https://www.shivavilaspalacehotel.com/images/homepage/39.jpg",
            priority: FastImage.priority.normal,
          }}
          style={{
            borderTopLeftRadius: wp(3),
            borderTopRightRadius: wp(3),
            height: wp(60),
          }}
          // resizeMode={FastImage.resizeMode.stretch}
        />

        <Spacer space={2} />
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-around",
          }}
        >
          <View style={{ paddingLeft: wp(3) }}>
            <Text
              style={{
                width: wp(55),
                fontWeight: "bold",
                fontSize: FONT.TextSmall,
                fontFamily: FONT_FAMILY.Roboto,
              }}
            >
              {item.name}
            </Text>
            <Spacer space={1} />
            {/* //test */}
            <Text style={{ width: wp(55), fontFamily: FONT_FAMILY.Montserrat }}>
              {item.address}
            </Text>
            <Spacer space={1} />
            <View style={{ width: wp(35), flexDirection: "row" }}>
              <AirbnbRating
                isDisabled={true}
                type={"star"}
                starStyle={{ margin: 0 }}
                size={wp(3)}
                showRating={false}
                // defaultRating={item.rating}
                defaultRating={item.avg_rating.rating__avg}
                onFinishRating={onRatingClick}
              />
              <Spacer row={1} />
              {/*<Text style={{fontFamily: FONT_FAMILY.Montserrat}}>{item.review}</Text>*/}
              <Text style={{ fontFamily: FONT_FAMILY.Montserrat }}>
              {item.review_count}  Reviews
              </Text>
            </View>
            <Spacer space={1} />
            <View style={{ flexDirection: "row" }}>
              <LocationIconSVG width={wp(5)} height={wp(5)} />
              <Text
                style={{ width: wp(55), fontFamily: FONT_FAMILY.Montserrat }}
              >
                {item.distance} km to city
              </Text>
            </View>
            <Spacer space={1} />
          </View>
          <Spacer row={2} />
          <View style={{}}>
            <Text style={{ width: wp(25), fontFamily: FONT_FAMILY.Montserrat }}>
              20% Discount
            </Text>
            <Spacer space={1} />
            <View style={{ width: wp(25) }}>
              <Text
                style={{
                  fontWeight: "bold",
                  fontSize: FONT.TextSmall,
                  fontFamily: FONT_FAMILY.RobotoBold,
                }}
              >
                {"\u20B9"}
                {item.min_discounted_price} /
                <Text
                  style={{
                    fontWeight: "bold",
                    fontSize: FONT.TextSmall,
                    fontFamily: FONT_FAMILY.RobotoBold,
                    color: "red",
                    textDecorationLine: "line-through",
                  }}
                >
                  {" "}
                  {"\u20B9"}
                  {item.min_regular_price}{" "}
                </Text>{" "}
              </Text>
              <Text style={{ fontFamily: FONT_FAMILY.Montserrat }}>
                /Per Night
              </Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    </ShadowViewContainer>
  );
};
export default RenderHotelItem;
