import React from "react";
import { Image, Text, TouchableOpacity, View ,Modal,KeyboardAvoidingView} from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import Menu, { MenuItem, MenuDivider } from "react-native-material-menu";
import COLORS from "../../../../../utils/Colors";
import { ICONS } from "../../../../../utils/ImagePaths";
import * as _ from "lodash";
import { FONT_FAMILY } from "../../../../../utils/Font";
import { FONT } from "../../../../../utils/FontSizes";
import { Spacer } from "../../../../../customComponents/Spacer";
import FastImage from "react-native-fast-image";
import BaseClass from "../../../../../utils/BaseClass";

// const CustomerHomeFilter = ({item, filterArr, onMenuPress}) => {

//     const _menu = [];

//     const hideMenu = (item) => {
//         _menu[item].hide();
//     };

//     const showMenu = (item) => {
//         _menu[item].show();
//     };

//     const onMenuItemPress = (item, index) => {
//         hideMenu(item);
//         onMenuPress(index);
//     };

//     return (
//         <View style={{
//             // paddingHorizontal: wp(2),
//             // paddingVertical: wp(1),
//         }}>
//             <Menu style={{
//                 // borderWidth: wp(0.2),
//                 // borderRadius: wp(2),
//                 // borderColor: COLORS.placeholder_color,
//             }}
//                   ref={menu => {
//                       _menu[item] = menu;
//                   }}
//                   button={
//                       <TouchableOpacity
//                           onPress={() => {
//                               showMenu(item);
//                           }}
//                           style={{flexDirection: "row", justifyContent: "space-between"}}>
//                           <Text style={{marginRight: 10}}>Filters</Text>
//                           <Image source={ICONS.FILTERS_ICON}/>
//                       </TouchableOpacity>
//                   }>
//                 {_.map(filterArr, (item1, index) => {
//                     return (
//                         <MenuItem style={{
//                             backgroundColor: COLORS.backGround_color,
//                             paddingLeft: wp(2),
//                             justifyContent: 'flex-start',
//                             alignItems:'center',

//                             flexDirection: 'row',
//                         }} onPress={() => onMenuItemPress(item, index)}>
//                             <FastImage
//                                 resizeMode={FastImage.resizeMode.contain}
//                                 style={{
//                                     borderRadius: wp(1),
//                                     borderColor: COLORS.black_color,
//                                     borderWidth: wp(0.2),
//                                     width: wp(4.5),
//                                     height: wp(4.5),
//                                     justifyContent:'center'
//                                 }} source={item1.isChecked && ICONS.CHECKED_ICON}/>
//                             <Text style={{
//                                                                     justifyContent:'center',

//                                 color: COLORS.black_color,
//                                 fontFamily: FONT_FAMILY.Montserrat,
//                                 fontSize: FONT.TextMedium,
//                             }}> {item1.value}</Text>
//                         </MenuItem>
//                     )
//                 })}
//             </Menu>
//         </View>
//     )
// };

class CustomerHomeFilter extends React.PureComponent {
  _menu = null;

  setMenuRef = (ref) => {
    this._menu = ref;
  };

  hideMenu = () => {
    this._menu.hide();
  };

  showMenu = () => {
    this._menu.show();
  };

  onMenuItemPress = (index) => {
    this.hideMenu();
    this.props.onMenuPress(index);
  };

  render() {
    return (
      <View  onStartShouldSetResponder={() => console.log("View click")}>
        <Menu
          ref={this.setMenuRef}
          button={
            <TouchableOpacity
              onPress={() => {
                // this.showMenu();
                // alert("hi")
              }}
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <Text style={{ marginRight: 10 }}>Filters</Text>
              <Image source={ICONS.FILTERS_ICON} />
            </TouchableOpacity>
          }
        >
          {_.map(this.props.filterArr, (item1, index) => {
            return (
              <MenuItem
                style={{
                  backgroundColor: COLORS.backGround_color,
                  paddingLeft: wp(2),
                  justifyContent: "flex-start",
                  alignItems: "center",

                  flexDirection: "row",
                }}
                onPress={() => this.onMenuItemPress(index)}
              >
                <FastImage
                  resizeMode={FastImage.resizeMode.contain}
                  style={{
                    borderRadius: wp(1),
                    borderColor: COLORS.black_color,
                    borderWidth: wp(0.2),
                    width: wp(4.5),
                    height: wp(4.5),
                    justifyContent: "center",
                  }}
                  source={item1.isChecked ? ICONS.CHECKED_ICON : ICONS.UNCHECKED_ICON}
                  />
                <Text
                  style={{
                    justifyContent: "center",

                    color: COLORS.black_color,
                    fontFamily: FONT_FAMILY.Montserrat,
                    fontSize: FONT.TextMedium,
                    alignSelf: "center",
                  }}
                >
                  {" "}
                  {item1.value}
                </Text>
              </MenuItem>
            );
          })}
        </Menu>
      </View>
    );
  }
}
  
export default CustomerHomeFilter;
