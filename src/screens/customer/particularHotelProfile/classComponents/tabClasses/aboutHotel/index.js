import React from 'react';
import {View, Text} from 'react-native';
import styles from './styles';

const AboutHotel = ({aboutData}) => {
    return (
        <View style={styles.mainContainer}>
            {aboutData.trim().length !== 0 ?
                <Text style={styles.aboutTextStyle}>{aboutData}</Text>
                :
                <Text style={styles.NoDataStyle}>NO DATA </Text>
            }
        </View>
    )
};

export default AboutHotel;
