import React from 'react';
import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import COLORS from "@utils/Colors";
import {FONT} from '../../../../../../utils/FontSizes';
import {FONT_FAMILY} from "@utils/Font";

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: COLORS.backGround_color,
        flex: 1,
        width: wp(100)
    },
    flatListContainer: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    flatListMainContainer: {
        flexDirection: 'row',
        padding: wp(3),
        width: wp(95)
    },
    imageStyle: {
        width: wp(25),
        height: wp(35)
    },
    roomTypeContainer: {
        width: wp(62),
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginLeft:wp('2%')
    },
    roomTypeText: {
        fontSize: wp(3.7),
        fontFamily: FONT_FAMILY.Montserrat,
        fontWeight: '500',
        width: wp(38)
    },

  roomAvailibilityText:{

    fontSize: wp(3.7),
    fontFamily: FONT_FAMILY.Montserrat,
    fontWeight: '500',
    width: wp(60)
  },

    guestsText: {
        color: COLORS.number_OfGuest_color,
        fontSize: FONT.TextSmall_2,
        lineHeight: wp(6),
        fontFamily: FONT_FAMILY.Montserrat
    },
    priceView: {
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent:'center',
        width:wp('30%'),
    },
    discountedPrice:{
        fontFamily: FONT_FAMILY.Roboto,
        fontSize: FONT.TextSmall_2,
    },
    actualPrice:{
        fontFamily: FONT_FAMILY.Roboto,
        fontSize: FONT.TextSmall_2,
        color: "red",
        textDecorationLine: 'line-through'
    },
    discountedText:{
        fontWeight: 'bold',
        fontSize: FONT.TextXExtraSmall,
        fontFamily: FONT_FAMILY.Montserrat,
        marginTop:wp('1%')
    },
    complimentaryText:{
        fontSize: FONT.TextSmall_2,
        fontFamily: FONT_FAMILY.MontserratBold,
    },
    servicesText:{
        color: COLORS.number_OfGuest_color,
        fontSize: FONT.TextExtraSmall,
        fontFamily: FONT_FAMILY.Montserrat
    },
    roomAvailableContainer:{
        alignItems: 'center',
        paddingHorizontal: wp(1),
        flexDirection: 'row',
        paddingVertical: wp(1),
    },
    roomText:{
        fontFamily: FONT_FAMILY.MontserratBold,
        fontSize: FONT.TextSmall_2,
        color: COLORS.green_color,
    },
    roomButton:{
        paddingHorizontal: wp(2),
        paddingVertical: wp(1.5),
        borderRadius: wp(4),
        backgroundColor: COLORS.app_theme_color,
    },
    getRoomText:{
        fontFamily: FONT_FAMILY.Montserrat,
        textAlign: 'center',
        fontSize: FONT.TextSmall_2,
        color: COLORS.white_color
    }
});

export default styles;
