// ------------Import Libraries-----------
import React from "react";
import { View, FlatList, Text, Image, TouchableOpacity } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

// ------------Local  Libraries-----------
import { Spacer } from "@customComponents/Spacer";
import { ShadowViewContainer } from "@utils/BaseStyle";
import GreenTick from "@assets/images/CustomerHome/greenTick.svg";
import styles from "./styles";
import RoomDetailModal from "../../../../../../customComponents/Modals/RoomDetailModal";
import { FONT } from "../../../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../../../utils/Font";
import { API } from "../../../../../../redux/constant";
import STRINGS from "../../../../../../utils/Strings";
import FastImage from "react-native-fast-image";
import CrossComponent from "../../../../../../../assets/images/BookingIcons/CrossSVG";
import COLORS from "../../../../../../utils/Colors";

const getAdditionalDiscount=(regularPrice)=>{

  
  return (Number(regularPrice) * 40)/100
    }

const RoomsDetails = ({
  roomData,
  onRoomModalOpen,
  isRoomModalOpen,
  onRoomModalClose,
  onModalHotelClick,
  onGetRoomPress,
  roomIndexClicked,
}) => {
  return (
    <View style={styles.mainContainer}>
      <Spacer space={1} />
      {roomData.length !== 0 ? (
        <>
          <FlatList
            nestedScrollEnabled={true}
            contentContainerStyle={styles.flatListContainer}
            data={roomData}
            keyExtractor={(item, index) => index}
            renderItem={({ item, index }) => (
              <View>
                {item.is_admin_approved == 1 && (
                  <ShadowViewContainer>
                    <View style={styles.flatListMainContainer}>
                      <TouchableOpacity onPress={() => onRoomModalOpen(index)}>
                        <FastImage
                          resizeMode={FastImage.resizeMode.stretch}
                          style={styles.imageStyle}
                          source={{
                            uri:
                              item.room_images.length !== 0
                                ? API.IMAGE_BASE_URL + item.room_images[0].image
                                : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                          }}
                        />
                      </TouchableOpacity>
                      <Spacer row={1} />
                      <View style={{ justifyContent: "center" }}>
                        <View style={styles.roomTypeContainer}>
                          <View>
                            <Text numberOfLines={1} style={styles.roomTypeText}>
                              Room Type: {item.room_type}
                            </Text>
                            <Text style={styles.guestsText}>
                              {STRINGS.no_of_Guests}: {item.no_of_guest}
                            </Text>
                          </View>

                          {item.is_special_discount===true? 
                           <View style={styles.priceView}>
                           <Text style={styles.discountedPrice}>
                             {"\u20B9"}
                             {getAdditionalDiscount(item.regular_price)}/
                             <Text style={styles.actualPrice}>
                               {" "}
                               {item.regular_price}
                             </Text>
                           </Text>
                           <Text style={styles.discountedText}>
                             40% Discount{" "}
                           </Text>
                         </View>
                         :
                         <View style={styles.priceView}>
                         <Text style={styles.discountedPrice}>
                           {"\u20B9"}
                           {item.discounted_price}/
                           <Text style={styles.actualPrice}>
                             {" "}
                             {item.regular_price}
                           </Text>
                         </Text>
                         <Text style={styles.discountedText}>
                           20% Discount{" "}
                         </Text>
                       </View>
                        }
                         
                        </View>
                        <Spacer space={1} />

                        {item.no_of_hours !== null &&
                          item.no_of_hours !== undefined && (
                            <View style={{ paddingHorizontal: wp(1) }}>
                              <View>
                                <Text style={styles.roomAvailibilityText}>
                                  Room availability :{item.no_of_hours} hour
                                </Text>
                                <Spacer space={0.5} />
                              </View>
                            </View>
                          )}
                        <Spacer space={1} />

                        <View style={{ paddingHorizontal: wp(1) }}>
                          {item.room_service.length > 0 && (
                            <View>
                              <Text style={styles.complimentaryText}>
                                Complimentary Services
                              </Text>
                              <Spacer space={0.5} />
                              <Text
                                style={{
                                  width: wp(80),
                                  paddingHorizontal: wp(0.5),
                                  fontSize: FONT.TextSmall_2,
                                  color: COLORS.number_OfGuest_color,
                                }}
                              >
                                {item.room_service
                                  .map((e) => e.service)
                                  .join(", ")}
                              </Text>
                            </View>
                          )}
                        </View>
                        <Spacer space={0.5} />
                        <View style={styles.roomAvailableContainer}>
                          {item.is_available ? (
                            <>
                              <GreenTick width={wp(4)} height={wp(4)} />
                              <Spacer row={1} />
                              <Text style={styles.roomText}>
                                Room Available
                              </Text>
                            </>
                          ) : (
                            <>
                              <CrossComponent width={wp(4)} height={wp(4)} />
                              <Spacer row={0.5} />
                              <Text
                                style={[
                                  styles.roomText,
                                  { color: COLORS.failure_Toast },
                                ]}
                              >
                                Room Unavailable
                              </Text>
                            </>
                          )}
                          <Spacer row={1} />

                          {item.is_available ? (
                            <TouchableOpacity
                              disabled={!item.is_available}
                              onPress={() => onGetRoomPress(item.id)}
                            >
                              <View style={styles.roomButton}>
                                <Text
                                  numberOfLines={1}
                                  style={styles.getRoomText}
                                >
                                  Get Room
                                </Text>
                              </View>
                            </TouchableOpacity>
                          ) : (
                            console.log("")
                          )}
                        </View>
                      </View>
                    </View>
                  </ShadowViewContainer>
                )}

                <Spacer space={0.5} />
              </View>
            )}
          />
          <RoomDetailModal
            roomModalVisible={isRoomModalOpen}
            value={roomData}
            closeRoomModal={() => {
              onRoomModalClose();
            }}
            onHotelClick={() => {
              onModalHotelClick();
            }}
            onGetRoomClick={(id) => {
              onGetRoomPress(id);
            }}
            indexRoom={roomIndexClicked}
          />
        </>
      ) : (
        <Text
          style={{
            textAlign: "center",
            fontSize: FONT.TextNormal,
            fontFamily: FONT_FAMILY.MontserratBold,
          }}
        >
          NO DATA{" "}
        </Text>
      )}
    </View>
  );
};

export default RoomsDetails;
