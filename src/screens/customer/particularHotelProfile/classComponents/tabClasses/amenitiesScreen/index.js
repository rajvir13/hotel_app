import React from 'react';
import {FlatList, View} from 'react-native';
import {CheckBox} from "react-native-elements";

import styles from "./styles";

const AmenitiesScreen = ({amenitiesData}) => {
    return (
        <View style={styles.mainProfileContainer}>
            <FlatList
                data={amenitiesData}
                numColumns={2}
                keyExtractor={(item) => item.id}
                renderItem={({item, index}) =>
                    <View style={styles.renderAmenitiesOuterView}>
                        <CheckBox
                            textStyle={styles.checkBoxTextStyle}
                            style={styles.checkboxStyle}
                            title={item.name}
                            containerStyle={
                                styles.checkBoxContainerStyle
                            }
                            // onPress={() =>
                            //     // this.handleChange(index, item.id)
                            // }
                            checked={amenitiesData[index].isSelected}
                        />
                    </View>
                }
            />
        </View>
    )
};

export default AmenitiesScreen;
