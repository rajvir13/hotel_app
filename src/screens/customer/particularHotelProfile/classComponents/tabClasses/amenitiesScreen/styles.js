import React from "react";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { StyleSheet, Dimensions } from "react-native";
import COLORS from "@utils/Colors";

const styles = StyleSheet.create({

    mainProfileContainer: {
        // height: hp("100%"),
        alignItems: "center",
        backgroundColor: COLORS.backGround_color,
    },

    renderAmenitiesOuterView:{
        margin: 1,
        width: wp(45)

    },
    checkBoxTextStyle:{
        color: COLORS.placeholder_color,fontSize:wp('4%')
    },
    checkBoxContainerStyle:{
        borderColor: COLORS.white_color,
        backgroundColor: COLORS.backGround_color,
    },
    checkboxStyle: {
        height: 100,
    },

});
export default styles;
