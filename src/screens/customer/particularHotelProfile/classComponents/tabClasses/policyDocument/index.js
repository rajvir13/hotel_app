import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import styles from './styles';
import {ICONS} from "@utils/ImagePaths";
import STRINGS from "@utils/Strings";
import {FONT} from "../../../../../../utils/FontSizes";
import {FONT_FAMILY} from "../../../../../../utils/Font";

let policyDescription = '';
let policyDocument = '';

const PolicyDocumentHotel = ({policyData, openDocument}) => {
    if (policyData.length !== 0) {
        policyDescription = policyData[0].title;
        policyDocument = policyData[0].file;
    }

  
    return (
        <View style={styles.mainContainer}>
            {policyData.length !== 0 ? <View><Text style={styles.aboutTextStyle}>{policyDescription}</Text>
                    <TouchableOpacity onPress={() => openDocument(policyDocument)}>
                        <View style={styles.documentView}>
                            <Image source={ICONS.PDF_ICON}
                                   resizeMode={'contain'}
                                   style={styles.imageStyle}/>
                            <Text style={styles.policyText}>{STRINGS.view_policy_document}</Text>
                        </View>
                    </TouchableOpacity>
                </View> :
                <Text style={{
                    textAlign: 'center',
                    fontSize: FONT.TextNormal,
                    fontFamily: FONT_FAMILY.MontserratBold
                }}>NO
                    DATA </Text>}
        </View>
    )
};

export default PolicyDocumentHotel;
