import React from 'react';
import {StyleSheet} from "react-native";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";

import COLORS from "@utils/Colors";
import {FONT} from "@utils/FontSizes";
import {FONT_FAMILY} from "@utils/Font";

const styles = StyleSheet.create({
    mainContainer: {
        paddingVertical: wp(3),
        backgroundColor: COLORS.backGround_color,
        alignSelf: 'center'
    },
    aboutTextStyle: {
        width: wp(90),
        fontSize: FONT.TextSmall,
        fontFamily: FONT_FAMILY.Montserrat,
        color: COLORS.placeholder_color
    },
    documentView: {
        marginTop: wp(8),
        width: wp(90),
        flexDirection:'row',
        alignItems:'center'
    },
    imageStyle:{
        width: wp(20),
        height: wp(20),
    },
    policyText: {
        paddingHorizontal: wp(3),
        fontSize: FONT.TextMedium_2,
        fontFamily: FONT_FAMILY.MontserratSemiBold,
        color: COLORS.tabBarActiveColor

    }
});

export default styles;
