// ------------Import Libraries-----------
import React from "react";
import { FlatList, Linking, Text, TouchableOpacity, View } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { AirbnbRating } from "react-native-elements";

// ------------Local  Libraries-----------
import { ShadowViewContainer } from "@utils/BaseStyle";
import COLORS from "@utils/Colors";
import { Spacer } from "@customComponents/Spacer";
import LocationIconSVG from "@assets/images/CustomerHome/LocationIconSVG";
import ContactIconSVG from "@assets/images/CustomerHome/contact.svg";
import GreenTick from "@assets/images/CustomerHome/greenTick.svg";
import RenderButtons from "../buttonsView";
import RenderCheckInCheckOutView from "../chechInOut";
import styles from "./styles";
import CrossComponent from "../../../../../../../assets/images/BookingIcons/CrossSVG";

const dialCall = (number) => {
  let phoneNumber = "";
  if (Platform.OS === "android") {
    phoneNumber = `tel:${number}`;
  } else {
    phoneNumber = `telprompt:${number}`;
  }
  Linking.openURL(phoneNumber);
};
const RenderHotelDetail = ({
  hotelDetail,
  onRatingClick,
  onFeedbackButtonsClick,
  onGetButtonsClick,
}) => {
  return (
    <ShadowViewContainer>
      <View style={styles.mainContainer}>
        <View style={{ flexDirection: "row" }}>
          <Text numberOfLines={1} style={styles.titleText}>
            {hotelDetail.name}
          </Text>
          {/* {console.log("hotel details are",)} */}
          <View style={styles.ratingContainer}>
            <AirbnbRating
              selectedColor={COLORS.detail_color}
              isDisabled={true}
              type={"star"}
              starStyle={{ margin: 0.5 }}
              size={wp(4.5)}
              showRating={false}
              defaultRating={Number(hotelDetail.avg_rating.rating__avg)}
              onFinishRating={onRatingClick}
            />
          </View>
        </View>
        <Spacer space={1} />
        <View style={styles.hotelAddressView}>
          <LocationIconSVG
            fillColor={COLORS.location_grey_color}
            width={wp(4)}
            height={wp(4)}
          />
          <Spacer row={0.5} />
          <Text numberOfLines={2} style={styles.addressText}>
            {hotelDetail.address}
          </Text>
        </View>
        <Spacer space={1} />
        <View style={styles.contactContainer}>
          <ContactIconSVG width={wp(4)} height={wp(4)} />
          <Spacer row={0.5} />
          <TouchableOpacity
            onPress={() => dialCall(hotelDetail.owner.mobile_number)}
          >
            <View style={{flexDirection:'row'}}>
              <Text numberOfLines={2} style={styles.addressText}>
                Contact : 
              </Text>
              <Text numberOfLines={2} style={styles.contactNumberText}>
               {" "+ hotelDetail.owner.mobile_number}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <Spacer space={1.5} />
        <View style={styles.availableViewContainer}>
          <View style={{ flexDirection: "row" }}>
            {hotelDetail.is_available_hotel ? (
              <>
                <GreenTick width={wp(4)} height={wp(4)} />
                <Spacer row={1} />
                <Text style={styles.availableText}>Available</Text>
              </>
            ) : (
              <>
                <CrossComponent width={wp(4)} height={wp(4)} />
                <Spacer row={1} />
                <Text
                  style={[
                    styles.availableText,
                    { color: COLORS.failure_Toast },
                  ]}
                >
                  Unavailable
                </Text>
              </>
            )}
          </View>
          <View style={{ flexDirection: "row" }}>
            {hotelDetail.is_rating_given !== null &&
              hotelDetail.is_rating_given !== true && (
                <RenderButtons
                  text={"Feedback"}
                  textColor={COLORS.white_color}
                  backColor={COLORS.detail_color}
                  onButtonClick={() =>
                    onFeedbackButtonsClick(hotelDetail.booking_id)
                  }
                />
              )}

            {hotelDetail.is_available_hotel ? (
              <RenderButtons
                text={"Get Room"}
                textColor={COLORS.white_color}
                backColor={COLORS.app_theme_color}
                onButtonClick={() => onGetButtonsClick()}
              />
            ) : (
              console.log("no room ")
            )}
          </View>
        </View>
        <Spacer space={2} />
        <View style={styles.separator} />
        <Spacer space={2} />
        <RenderCheckInCheckOutView data={hotelDetail} />
        <Spacer space={1} />
        <FlatList
          style={styles.flatListStyle}
          data={hotelDetail.hotel_service}
          numColumns={4}
          renderItem={({ item, index }) => {
            return (
              <>
                <Text style={styles.otherTextStyle}>{item.service_name},</Text>
                <Spacer row={1} />
              </>
            );
          }}
        />
      </View>
    </ShadowViewContainer>
  );
};

export default RenderHotelDetail;
