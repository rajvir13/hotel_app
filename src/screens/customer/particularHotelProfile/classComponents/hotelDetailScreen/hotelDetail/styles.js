import React from 'react';
import {StyleSheet} from "react-native";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {FONT} from "@utils/FontSizes";
import {FONT_FAMILY} from "@utils/Font";
import COLORS from "@utils/Colors";

const styles = StyleSheet.create({
    mainContainer: {
        borderRadius: wp(2),
        width: wp(90),
        paddingVertical: wp(4),
        backgroundColor: 'white'
    },
    titleText: {
        paddingHorizontal: wp(2),
        fontSize: FONT.TextSmall,
        fontWeight: '500',
        fontFamily: FONT_FAMILY.Poppins, width: wp(62)
    },
    ratingContainer: {
        alignSelf: 'center',
        paddingHorizontal: wp(1),
        width: wp(25),
    },
    hotelAddressView: {
        marginLeft: wp(2),
        flexDirection: 'row',
        alignItems: 'center',
    },
    addressText: {
    
        fontSize: FONT.TextExtraSmall,
        fontFamily: FONT_FAMILY.Montserrat,
    },
    contactNumberText:{

        fontSize: FONT.TextExtraSmall,
        fontFamily: FONT_FAMILY.Montserrat,
        color:COLORS.green_color
    },
    contactContainer: {
        marginLeft: wp(2),
        flexDirection: 'row',
        alignItems: 'center',
    },
    availableViewContainer: {
        alignItems: 'center',
        marginLeft: wp(2),
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    availableText: {
        fontFamily: FONT_FAMILY.Montserrat,
        fontSize: FONT.TextExtraSmall,
        color: COLORS.green_color
    },
    separator: {
        height: wp(0.2),
        backgroundColor: COLORS.light_grey3
    },
    flatListStyle: {
        alignSelf: 'center',
        width: wp(85)
    },
    otherTextStyle:{
        fontFamily: FONT_FAMILY.Montserrat,
        fontSize: FONT.TextExtraSmall,
        color: COLORS.black_color
    }
});

export default styles;
