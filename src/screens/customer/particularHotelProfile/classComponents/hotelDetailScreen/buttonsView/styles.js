import React from 'react';
import {StyleSheet} from "react-native";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {FONT} from "@utils/FontSizes";
import {FONT_FAMILY} from "@utils/Font";

const styles = StyleSheet.create({
    touchableStyle:{
        marginHorizontal: wp(2),
        borderRadius: wp(5),
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: wp(3),
        paddingVertical: wp(0.5)
    },
    textStyle:{
        letterSpacing: wp(0.1),
        fontSize: FONT.TextExtraSmall,
        fontFamily: FONT_FAMILY.Poppins
    }
});

export default styles;
