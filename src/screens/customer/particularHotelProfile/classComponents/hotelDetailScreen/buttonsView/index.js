// ------------Import Libraries-----------
import React from "react";
import {Text, TouchableOpacity} from "react-native";

// ------------Local  Libraries-----------
import styles from './styles';

const RenderButtons = ({backColor, text, textColor , onButtonClick }) => {
    return (
        <TouchableOpacity onPress={() => onButtonClick()}
                          style={[styles.touchableStyle, {backgroundColor: backColor}]}>
            <Text style={[styles.textStyle, {color: textColor,alignSelf:'center'}]}>
                {text}
            </Text>
        </TouchableOpacity>
    )
};

export default RenderButtons
