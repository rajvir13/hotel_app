// ------------Import Libraries-----------
import React from "react";
import { Text, View } from "react-native";

// ------------Local  Libraries-----------
import styles from "./styles";

const RenderCheckInCheckOutView = ({ data }) => {
  // const formatAMPM = (data) => {
  //     if (data !== undefined && data !== null && data !== "") {
  //         const [time] = data.split(" ");

  //         let [hours, minutes] = time.split(":");

  //         var ampm = hours >= 12 ? "PM" : "AM";
  //         hours = hours % 12;
  //         hours = hours ? hours : 12; // the hour '0' should be '12'
  //         minutes = minutes < 10 ? "0" + minutes : minutes;
  //         var strTime = hours + ":" + minutes + " " + ampm;
  //         return strTime;
  //     } else {
  //         return "12:00 PM";
  //     }
  // };

  const formatAMPM = (data) => {
    if(data!==null && data!== undefined)
    {
      const [time] = data.split(" ");
      let [sHours, minutes] = time.split(":");
  
      // const [sHours, minutes] = time24.match(/([0-9]{1,2}):([0-9]{2})/).slice(1);
      const period = +sHours < 12 ? "AM" : "PM";
      const hours = +sHours % 12 || 12;
  
      return `${hours}:${minutes} ${period}`;
    }
    else {
       return "12:00 PM";
    }
   
  };

  return (
    <View style={styles.mainContainer}>
      <View style={{ flexDirection: "row" }}>
        <Text style={styles.checkInText}>CheckIn: </Text>

        <Text style={styles.timeText}>{formatAMPM(data.check_in)}</Text>
      </View>
      <View style={{ flexDirection: "row" }}>
        <Text style={styles.checkInText}>CheckOut: </Text>
        {console.log("check OUt----", data.hotel_regular_price)}
        <Text style={styles.timeText}>{formatAMPM(data.check_out)}</Text>
      </View>
      {data.hotel_regular_price !== null ? (
        <View style={styles.priceContainer}>
          <Text style={styles.rateText}>
            {"\u20B9"}
            {data.hotel_discount_price}/
            <Text style={styles.actualText}>
              {"\u20B9"}
              {data.hotel_regular_price}
            </Text>
          </Text>
          <Text style={styles.discountText}>20% Discount </Text>
        </View>
      ) : (
        <View style={styles.priceContainer}>
          <Text style={styles.rateText}>
            {"\u20B9"}0/
            <Text style={styles.actualText}>{"\u20B9"}0</Text>
          </Text>
          <Text style={styles.discountText}>20% Discount </Text>
        </View>
      )}
    </View>
  );
};

export default RenderCheckInCheckOutView;
