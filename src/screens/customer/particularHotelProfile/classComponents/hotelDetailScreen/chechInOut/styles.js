import React from 'react';
import {StyleSheet} from "react-native";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {FONT} from "@utils/FontSizes";
import {FONT_FAMILY} from "@utils/Font";
import COLORS from "@utils/Colors";

const styles = StyleSheet.create({
    mainContainer: {
        marginHorizontal: wp(2),
        justifyContent: 'space-around',
        flexDirection: 'row'
    },
    checkInText: {
        fontSize: FONT.TextExtraSmall,
        fontFamily: FONT_FAMILY.Poppins
    },
    timeText: {
        fontSize: FONT.TextExtraSmall,
        fontFamily: FONT_FAMILY.Poppins,
        color: COLORS.app_theme_color
    },
    priceContainer: {
        flexDirection: 'column',
        alignItems: 'center'
    },
    rateText: {
        fontFamily: FONT_FAMILY.Roboto,
        fontSize: FONT.TextSmall_2,
        fontWeight: 'bold'
    },
    actualText: {
        fontFamily: FONT_FAMILY.Roboto,
        fontSize: FONT.TextSmall_2,
        fontWeight: 'bold',
        color: "red",
        textDecorationLine: 'line-through'
    },
    discountText: {
        fontSize: FONT.TextExtraSmall,
        fontFamily: FONT_FAMILY.Montserrat
    }
});

export default styles;
