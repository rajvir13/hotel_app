import React from 'react';
import {StyleSheet} from "react-native";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import COLORS from "../../../../../../utils/Colors";
import {FONT} from "../../../../../../utils/FontSizes";
import {FONT_FAMILY} from "../../../../../../utils/Font";

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "rgba(0,0,0,0.6)",
    },
    centeredTextView: {
        // flexDirection: "row",
        alignItems: 'center',
        paddingVertical: wp(4),
        paddingHorizontal: wp(2),
        backgroundColor: COLORS.white_color,
        width: wp(93),
    },
    alertTextView: {
        fontSize: FONT.TextMedium_2,
        fontFamily: FONT_FAMILY.MontserratSemiBold,
        color: COLORS.white_color,
    },
    alertTitileView: {
        alignItems: "center",
        paddingVertical: wp(4),
        backgroundColor: COLORS.app_theme_color,
        justifyContent: "flex-start",
    },
    alertMainView: {
        backgroundColor: COLORS.white_color,
        width: wp(93),
        borderRadius: wp(5),
    },
    alertHeaderView: {
        backgroundColor: "transparent",
        alignItems: "center",
        width: wp(98),
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2,
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center",
    },
    cancelView: {
        borderRadius: wp(2),
        alignSelf: "center",
        width: wp(40),
        paddingVertical: wp(3),
        backgroundColor: COLORS.customerType,
    },

    cancelTextStyle: {
        textAlign: "center",
        color: COLORS.white_color,
        fontSize: FONT.TextSmall,
        fontFamily: FONT_FAMILY.MontserratSemiBold,
    },

    confirmView: {
        borderRadius: wp(2),
        alignSelf: "center",
        width: wp(40),
        paddingVertical: wp(3),
        backgroundColor: COLORS.green_color,
    },

    confirmText: {
        textAlign: "center",
        color: COLORS.white_color,
        fontSize: FONT.TextSmall,
        fontFamily: FONT_FAMILY.MontserratSemiBold,
    },
    alertTextStyle: {
        width: wp(75),
        textAlign: 'center',
        fontSize: FONT.TextSmall,
        fontFamily: FONT_FAMILY.Montserrat
    }
});

export {styles};
