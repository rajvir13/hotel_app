import React from "react";
import {
    Modal,
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    Platform,
    TouchableWithoutFeedback,
    Keyboard,
} from "react-native";
import propTypes from "prop-types";
import {Spacer} from "../../../../../../customComponents/Spacer";
import BaseClass from "../../../../../../utils/BaseClass";
import {styles} from './styles'
import {FONT_FAMILY} from "../../../../../../utils/Font";
import {FONT} from "../../../../../../utils/FontSizes";
import COLORS from "../../../../../../utils/Colors";
import {ShadowViewContainer} from "../../../../../../utils/BaseStyle";
import CrossComponent from "../../../../../../../assets/images/BookingIcons/CrossSVG";
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from "react-native-responsive-screen";
import {AirbnbRating, Input} from "react-native-elements";

export class FeedbackModel extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            modalVisiblePass: props.isModelVisible,
            rating: 0,
            comment: ''
        };
    }

    static propTypes = {
        isModelVisible: propTypes.boolean,
        value: propTypes.string,
        cancelClick: propTypes.func,
        feedbackClick: propTypes.func,
    };

    onCancelClick = () => {
        this.props.cancelClick()
    };

    onFeedbackClick = () => {
        const {rating, comment} = this.state;
        if (rating === 0) {
            this.showToastAlert("Please give rating.")
        } else if (comment === '' || comment.trim().length === 0) {
            this.showToastAlert('Please add comment.');
        } else {
            this.props.cancelClick();
            this.props.feedbackClick(rating, comment);
        }
    };


    render() {
        const {modalVisiblePass, rating, comment} = this.state;
        // const {value} = this.props;
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisiblePass}>

                <KeyboardAvoidingView
                    style={{flex: 1}}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                    <TouchableWithoutFeedback
                        onPress={() => {
                            Keyboard.dismiss()
                        }}>
                        <View style={styles.centeredView}>
                            <View style={{backgroundColor: 'transparent', alignItems: 'center', width: wp(98)}}>
                                <View style={{
                                    backgroundColor: COLORS.white_color,
                                    width: wp(93),
                                    borderRadius: wp(5)
                                }}>
                                    <View style={{
                                        alignItems: 'center',
                                        paddingVertical: wp(4),
                                        backgroundColor: COLORS.app_theme_color,
                                        justifyContent: 'flex-start'
                                    }}>
                                        <Text style={{
                                            fontSize: FONT.TextMedium_2,
                                            fontFamily: FONT_FAMILY.MontserratSemiBold,
                                            color: COLORS.white_color
                                        }}>Feedback
                                        </Text>
                                    </View>
                                    <View style={{
                                        width: wp(85),
                                        paddingVertical: wp(4),
                                        justifyContent: 'center',
                                        alignSelf: 'center',
                                    }}>
                                        <AirbnbRating
                                            type={'star'}
                                            starStyle={{margin: 10}}
                                            size={wp(10)}
                                            showRating={false}
                                            defaultRating={rating}
                                            onFinishRating={(value) => this.setState({rating: value})}
                                        />
                                    </View>
                                    {/*<Spacer space={2}/>*/}
                                    <ShadowViewContainer>
                                        <TouchableOpacity activeOpacity={1}>
                                            <Input
                                                inputContainerStyle={{
                                                    borderBottomColor: "white",
                                                }}
                                                multiline={true}
                                                numberOfLines={1}
                                                placeholder={"Your Feedback"}
                                                placeholderTextColor={COLORS.placeholder_color}
                                                ref="Detail"
                                                value={comment}
                                                onChangeText={(text) => this.setState({
                                                    comment: text,
                                                })}
                                                returnKeyType={"done"}
                                                inputStyle={{
                                                    paddingLeft: 10,
                                                    fontSize: FONT.TextSmall,
                                                    fontFamily: FONT_FAMILY.Montserrat
                                                }}
                                                containerStyle={{
                                                    paddingTop: 0,
                                                    width: wp("85%"),
                                                    height: hp(Platform.OS === "ios" ? '12%' : '16%'),
                                                }}
                                                blurOnSubmit={true}
                                                // onSubmitEditing={() => {
                                                //     DismissKeyboard()
                                                // }}
                                            />
                                        </TouchableOpacity>
                                    </ShadowViewContainer>
                                    <TouchableOpacity style={{
                                        borderRadius: wp(5),
                                        alignSelf: 'center',
                                        width: wp(80),
                                        paddingVertical: wp(2.5),
                                        backgroundColor: COLORS.app_theme_color
                                    }} onPress={() => {
                                        this.onFeedbackClick()
                                    }}>
                                        <Text style={{
                                            textAlign: 'center',
                                            color: COLORS.white_color,
                                            fontSize: FONT.TextSmall,
                                            fontFamily: FONT_FAMILY.MontserratSemiBold
                                        }}>Submit</Text>
                                    </TouchableOpacity>
                                    <Spacer space={2}/>
                                </View>
                                <TouchableOpacity
                                    hitSlop={{top: wp(5), left: wp(5), bottom: wp(5)}}
                                    style={{
                                        position: 'absolute',
                                        alignSelf: 'flex-end',
                                        marginTop: wp(-3),
                                    }}
                                    onPress={() => {
                                        this.onCancelClick();
                                    }}>
                                    <CrossComponent width={wp(9)} height={wp(9)}/>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </Modal>
        );
    }
}

