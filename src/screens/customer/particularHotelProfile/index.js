// ------------Import Libraries-----------

import React from "react";
import {
  Image,
  Platform,
  View,
  StatusBar,
  TouchableOpacity,
  Alert,
  Linking,
  Text,
  FlatList,
} from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Header } from "react-native-elements";

// ------------Local  Libraries-----------

import BaseClass from "@utils/BaseClass";
import {
  SafeAreaViewContainer,
  ShadowViewContainer,
  ScrollContainer,
} from "@utils/BaseStyle";
import COLORS from "@utils/Colors";
import { LeftIcon } from "@customComponents/icons";
import { FONT } from "@utils/FontSizes";
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import { Spacer } from "@customComponents/Spacer";
// import TabViewExample from "@customComponents/customerTopNavigator";
import { Data } from "@utils/FixtureData";
import RenderHotelDetail from "./classComponents/hotelDetailScreen/hotelDetail";
import TabViewExample from "../../../customComponents/customerTopNavigator";
import { FONT_FAMILY } from "../../../utils/Font";
import { styles } from "../../hotelOwner/hotelDetailScreens/styles";
import FastImage from "react-native-fast-image";
import {
  GetHotelDetailAction,
  GetHotelRoomDetailAction,
} from "../../../redux/actions/GetHotelDetail";
import { SaveBookingDetailAction } from "../../../redux/actions/SaveBookingDetailAction";
import { connect } from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";
import { CommonActions } from "@react-navigation/native";
import { FeedbackModel } from "./classComponents/hotelDetailScreen/feedbackModal";
import { SaveCustomerRating } from "../../../redux/actions/GetRatingHotelOwner";
import STRINGS from "../../../utils/Strings";

let checkUpdate = false;

export default class ParticularHotelProfile extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      isRoomModalOpen: false,
      amenitiesData: [
        {
          id: 1,
          name: "Personal Items",
        },
        {
          id: 2,
          name: "Hair Dryer",
        },
        {
          id: 3,
          name: "Travel",
        },
        {
          id: 4,
          name: "Parking",
        },
        {
          id: 5,
          name: "Driving",
        },
      ],
      hotelData: [],
      roomsList: [],
      roomSearched: 0,
      accessToken: undefined,
      hotelId: undefined,
      roomId: undefined,
      loginUserId: undefined,
      currentRoomModelOpenIndex: undefined,
      isFeedbackModalVisible: false,
      bookingId: undefined,
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.onFocusFunction();
    });
  }

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS
  ): void {
    console.log("prevProps", checkUpdate);
    if (
      prevProps.route.params.bookingId !== undefined &&
      prevProps.route.params.bookingId !== null &&
      checkUpdate === false
    ) {
      this.setState({
        hotelId: prevProps.route.params.id,
        bookingId: prevProps.route.params.bookingId,
        isFeedbackModalVisible: true,
      });
      checkUpdate = true;
    }
  }

  componentWillUnmount(): void {
    checkUpdate = false;
    this._unsubscribe();
  }

  onFocusFunction = () => {
    this.setState({ hotelId: this.props.route.params.id });
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        this.setState({
          accessToken: loginData.access_token,
          loginUserId: loginData.id,
        });
        this.hitHotelDetailApi(
          loginData.id,
          loginData.access_token,
          this.props.route.params.id
        );
      }
    });
  };

  hitHotelDetailApi = (customerId, token, hotelId) => {
    this.showDialog();
    GetHotelDetailAction(
      {
        id: hotelId,
        customerId: customerId,
        accessToken: token,
      },
      (data) => this.hotelDetailResponse(data)
    );

    GetHotelRoomDetailAction(
      {
        id: hotelId,
        accessToken: token,
        customerId:customerId
      },
      (data) => this.hotelRoomDetailResponse(data)
    );
  
  };

  // =============================================================================================
  // Methods
  // =============================================================================================

  hotelDetailResponse = (data) => {
    this.hideDialog();
    const { navigation } = this.props;
    const { amenitiesData } = this.state;
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      const { response, message, status } = data;
      if (response !== undefined) {
        if (status === 200) {
          this.hideDialog();
          // let data = response;
          response.map((item) => {
            if (item.hotel_images.length === 0) {
              item.hotel_images = [{ image: null }];
              return item;
            } else {
              return item;
            }
          });
          let amenitiesResponse = response[0].hotel_amenties;
          let result = amenitiesResponse.map((a) => a.amenity_id);
          amenitiesData.map((item) => {
            if (result.includes(item.id)) {
              item.isSelected = true;
              return item;
            } else {
              item.isSelected = false;
              return item;
            }
          });

          this.setState({
            hotelData: response,
            amenitiesData: amenitiesData,
          });
        }
      } else if (status === 111) {
        if (message.non_field_errors !== undefined) {
          // if (message.non_field_errors[0] === "User with this social_id does not exist")
        } else {
          AsyncStorage.removeItem(STRINGS.loginCredentials);
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{ name: "login" }],
            })
          );
          this.showToastAlert(message);
        }
      } else if (status === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      }
    }
    this.hideDialog();
  };

  hotelRoomDetailResponse = (data) => {
    this.hideDialog();
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      const { response, message, status } = data;
      if (response !== undefined) {
        if (status === 200) {
          this.hideDialog();
          this.setState({
            roomsList: response[0].room_hotel,
          });
        }
      } else if (status === 111) {
        this.hideDialog();
        if (message.non_field_errors !== undefined) {
          // if (message.non_field_errors[0] === "User with this social_id does not exist")
        }
      } else if (status === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      }
    }
    this.hideDialog();
  };

  ratingCompleted(rating) {
    console.log("Rating is: " + rating);
  }

  // =============================================================================================
  // Render methods for Header
  // =============================================================================================

  _renderHeader() {
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={<LeftIcon onPress={() => navigation.goBack()} />}
        centerComponent={{
          text: STRINGS.hotel_profile,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  _renderHotelImage = (hotelData) => {
    const { hotel_images } = hotelData[0];
    return (
      <>
        <FlatList
          horizontal={true}
          style={{
            alignSelf: "center",
            width: wp(90),
            height: wp(60),
            borderRadius: wp(3),
          }}
          data={hotel_images}
          renderItem={({ item, index }) => (
            <ShadowViewContainer style={{ marginBottom: 0 }}>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("ShowFullViewImg", {
                    imgUrl:
                      item.image !== null
                        ? item.image
                        : "https://www.shivavilaspalacehotel.com/images/homepage/39.jpg",
                  })
                }
              >
                <FastImage
                  style={styles.imageStyle}
                  source={{
                    uri:
                      item.image !== null
                        ? item.image
                        : "https://www.shivavilaspalacehotel.com/images/homepage/39.jpg",
                  }}
                  resizeMode={FastImage.resizeMode.cover}
                />
              </TouchableOpacity>
            </ShadowViewContainer>
          )}
        />
      </>
    );
  };

  onGetRoomsPress = (index) => {
    this.setState({
      roomSearched: index,
      isRoomModalOpen: true,
      currentRoomModelOpenIndex: index,
    });
  };

  onRoomModalClose = () => {
    this.setState({
      isRoomModalOpen: false,
    });
  };

  componentWillReceiveProps(nextProps, nextContext) {
    console.warn("response appera1");
  }

  getDerivedStateFromProps(nextProps, prevState) {
    console.warn("response appera");
    if (nextProps.someValue !== prevState.someValue) {
      return { someState: nextProps.someValue };
    } else return null;
  }

  saveBookingDetail = (roomId) => {

    console.log("room clicked",roomId)
    const { accessToken, hotelId, loginUserId } = this.state;
    this.showDialog();
    SaveBookingDetailAction(
      {
        room: roomId,
        hotel: hotelId,
        customer: loginUserId,
        accessToken: accessToken,
      },
      (data) => this.saveHotelDetailData(data)
    );
  };

  saveHotelDetailData = (data) => {
    const { loginUserId, accessToken, hotelId } = this.state;
    this.hideDialog();
    const { response, status, message } = data;

    //new
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (response !== undefined) {
        if (status === 200) {
          this.showToastSucess("Success");
          this.hitHotelDetailApi(loginUserId, accessToken, hotelId);
        } else if (status === 111) {
          this.hideDialog();
          this.showToastAlert(message);
        } else if (status === 401) {
          this.hideDialog();
          this.showToastAlert(message);
        } else {
          this.hideDialog();
          this.showToastAlert(message);
        }
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      } else {
        this.hideDialog();
        this.showToastAlert(message);
      }
      this.hideDialog();
    }
    this.hideDialog();
  };

  onFeedbackClick = (bookingId) => {
    console.warn("feedback");
    this.setState({
      isFeedbackModalVisible: true,
      bookingId: bookingId,
    });
  };

  onModelClose = () => {
    this.setState({
      isFeedbackModalVisible: false,
    });
  };

  onModalFeedbackClick = (rating, comment) => {
    const { loginUserId, accessToken, bookingId } = this.state;
    this.showDialog();
    SaveCustomerRating(
      {
        hotel_id: this.props.route.params.id,
        customer_id: loginUserId,
        accessToken: accessToken,
        rating: rating,
        comment: comment,
        booking_id: bookingId,
      },
      (data) => this.onSavingRatingResponse(data)
    );
   
  };

  onSavingRatingResponse = (data) => {
    const { loginUserId, accessToken, hotelId } = this.state;
    this.hideDialog();
    const { response, status, message } = data;
    console.warn("rating response", data);
    //new
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (response !== undefined) {
        if (status === 200) {
          this.showToastSucess("Success");
          this.hitHotelDetailApi(loginUserId, accessToken, hotelId);
        } else if (status === 111) {
          this.hideDialog();
          this.showToastAlert(message);
        } else if (status === 401) {
          this.hideDialog();
          this.showToastAlert(message);
        } else {
          this.hideDialog();
          this.showToastAlert(message);
        }
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      } else {
        this.hideDialog();
        this.showToastAlert(message);
      }
      this.hideDialog();
    }
  };

  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  //end

  // Main Render method

  render() {
    const {
      isRoomModalOpen,
      amenitiesData,
      hotelData,
      roomsList,
      roomSearched,
      hotelId,
      accessToken,
      loginUserId,
      isFeedbackModalVisible,
    } = this.state;
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <ScrollContainer>
          {hotelData.length !== 0 ? (
            <View>
              <Spacer space={2} />
              {this._renderHotelImage(hotelData)}
              <Spacer space={2} />
              <RenderHotelDetail
                hotelDetail={hotelData[0]}
                onGetButtonsClick={() =>
                  this.props.navigation.navigate("customerRoomList", {
                    hotelId: hotelId,
                    customerId: loginUserId,
                    accessToken: accessToken,
                  })
                }
                onFeedbackButtonsClick={(bookingId) =>
                  this.onFeedbackClick(bookingId)
                }
                onRatingClick={this.ratingCompleted}
              />
              <TabViewExample
                isRoomModalOpen={isRoomModalOpen}
                roomData={roomsList}
                aboutData={hotelData[0].description}
                policyData={hotelData[0].hotel_documents}
                onButtonPress={(index) => this.onGetRoomsPress(index)}
                onRoomModalClose={this.onRoomModalClose}
                onHotelModalClick={() =>
                  // this.showToastAlert(STRINGS.COMING_SOON)
                  console.log("")
                }
                amenitiesData={amenitiesData}
                onRoomClick={(roomId) => this.saveBookingDetail(roomId)}
                roomIndex={roomSearched}
                openPdfDoc={(url) =>
                  this.props.navigation.navigate("PdfViewScreen", {
                    pdfData: url,
                  })
                }
              />
            </View>
          ) : (
            <Text
              style={{
                paddingVertical: wp(3),
                textAlign: "center",
                fontSize: FONT.TextNormal,
                fontFamily: FONT_FAMILY.MontserratBold,
              }}
            >
              NO DATA{" "}
            </Text>
          )}

          {this._renderCustomLoader()}
          {isFeedbackModalVisible && (
            <FeedbackModel
              // value={}
              cancelClick={() => this.onModelClose()}
              feedbackClick={(rating, comment) =>
                this.onModalFeedbackClick(rating, comment)
              }
              isModelVisible={isFeedbackModalVisible}
            />
          )}
        </ScrollContainer>
      </SafeAreaViewContainer>
    );
  }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------
