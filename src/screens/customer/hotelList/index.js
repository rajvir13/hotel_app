//------------Import libraries ------------

import React from 'react';
import {FlatList, Image, Platform, StatusBar, Text, TouchableOpacity, View} from 'react-native';
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {AirbnbRating, Header} from 'react-native-elements';

//--------------local libraries-------------

import BaseClass from "../../../utils/BaseClass";
import {SafeAreaViewContainer, MainContainer, ShadowViewContainer} from "../../../utils/BaseStyle";
import COLORS from "../../../utils/Colors";
import {LeftIcon, OwnerRightIcon} from "../../../customComponents/icons";
import STRINGS from "../../../utils/Strings";
import {FONT} from "../../../utils/FontSizes";
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import {Spacer} from "../../../customComponents/Spacer";
import {FONT_FAMILY} from "../../../utils/Font";
import LocationIconSVG from "../../../../assets/images/CustomerHome/LocationIconSVG";


class HotelListNearMe extends BaseClass {
    constructor(props) {
        super(props);
        console.warn("hotel list ")
        this.state = {
            hotelListData: [
                {
                    imageUrl: 'https://images.unsplash.com/photo-1520250497591-112f2f40a3f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80',
                    hotelName: 'Grand Royal Hotel',
                    hotelLocation: 'Hotel Grand Royal London',
                    rating: 4,
                    review: '150 Reviews',
                    distance: '2 Km',
                    discount: '20% Discount',
                    price: '200',
                    totalPrice: '300',
                }, {
                    imageUrl: 'https://img.etimg.com/thumb/width-640,height-480,imgsize-131118,resizemode-1,msid-57048258/tata-group-to-rebuild-brand-taj.jpg',
                    hotelName: 'Grand Royal Hotel',
                    hotelLocation: 'Hotel Grand Royal London',
                    rating: 4,
                    review: '150 Reviews',
                    distance: '2 Km',
                    discount: '20% Discount',
                    price: '200',
                    totalPrice: '300',

                }, {
                    imageUrl: 'https://pix10.agoda.net/hotelImages/6800928/0/3f32f89a2a5952e7811229a9c76a4f4f.jpg?s=1024x768',
                    hotelName: 'Grand Royal Hotel',
                    hotelLocation: 'Hotel Grand Royal London',
                    rating: 2,
                    review: '150 Reviews',
                    distance: '2 Km',
                    discount: '20% Discount',
                    price: '200',
                    totalPrice: '300',

                }, {
                    imageUrl: 'https://www.shivavilaspalacehotel.com/images/homepage/39.jpg',
                    hotelName: 'Grand Royal Hotel',
                    hotelLocation: 'Hotel Grand Royal London',
                    rating: 3,
                    review: '150 Reviews',
                    distance: '2 Km',
                    discount: '20% Discount',
                    price: '200',
                    totalPrice: '300',

                }, {
                    imageUrl: 'https://i.ytimg.com/vi/PEfR_XLTGAQ/hqdefault.jpg',
                    hotelName: 'Hotel Grand Royal',
                    hotelLocation: 'Hotel Grand Royal London',
                    rating: 1,
                    review: '150 Reviews',
                    distance: '2 Km',
                    discount: '20% Discount',
                    price: '300',
                    totalPrice: '500',

                }
            ],
        }
    }

    // =============================================================================================
    // Methods
    // =============================================================================================


    ratingCompleted(rating) {
        console.log("Rating is: " + rating)
    }

    // =============================================================================================
    // Render methods for Header
    // =============================================================================================

    _renderHeader() {
        const {navigation} = this.props;
        return (
            <Header
                backgroundColor={COLORS.white_color}
                barStyle={"dark-content"}
                statusBarProps={{
                    translucent: true,
                    backgroundColor: COLORS.transparent,
                }}
                leftComponent={<LeftIcon onPress={() => navigation.goBack()}/>}
                centerComponent={{
                    text: STRINGS.near_to_me, style: {
                        color: COLORS.greyButton_color,
                        fontSize: FONT.TextMedium_2,
                        fontFamily: FONT_FAMILY.PoppinsBold
                    }
                }}
                containerStyle={{
                    borderBottomColor: COLORS.greyButton_color,
                    paddingTop: Platform.OS === 'ios' ? 0 : 25,
                    height: Platform.OS === 'ios' ? 60 : StatusBar.currentHeight + 60,
                }}
            />
        )
    }

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT}/>
        )
    };

    _renderHotelView = (item) => {
        return (
            <View style={{width: wp(44)}}>
                <Text numberOfLines={1}
                      style={{
                          padding: wp(1),
                          width: wp(44),
                          fontSize: FONT.TextSmall,
                          fontFamily: FONT_FAMILY.Poppins
                      }}>{item.hotelName}</Text>
                <View style={{
                    paddingHorizontal: wp(1),
                    width: wp(35),
                    flexDirection: 'row'
                }}>
                    <AirbnbRating
                        isDisabled={true}
                        type={'star'}
                        starStyle={{margin: 0}}
                        size={wp(3)}
                        showRating={false}
                        defaultRating={item.rating}
                        onFinishRating={this.ratingCompleted()}
                    />
                </View>
                <Spacer space={1}/>
                <View style={{flexDirection: 'row', paddingHorizontal: wp(0.5)}}>
                    <LocationIconSVG width={wp(5)} height={wp(5)} />
                    <Text numberOfLines={1}
                          style={{
                              width: wp(44),
                              fontFamily: FONT_FAMILY.Montserrat
                          }}>{item.distance} to
                        city</Text>
                </View>
            </View>
        )
    };

    _renderDiscountView = (item) => {
        return (
            <View style={{width: wp(21)}}>
                <Text style={{
                    paddingHorizontal: wp(1),
                    paddingTop: wp(1),
                    lineHeight: wp(5),
                    fontWeight: 'bold',
                    fontSize: FONT.TextSmall_2,
                    fontFamily: FONT_FAMILY.RobotoBold
                }}>{'\u20B9'}{item.price} /
                    <Text style={{
                        fontWeight: 'bold',
                        fontSize: FONT.TextSmall_2,
                        fontFamily: FONT_FAMILY.RobotoBold,
                        color: COLORS.price_Background_color
                    }}> {item.totalPrice} </Text> </Text>
                <Text style={{
                    width: wp(20),
                    paddingHorizontal: wp(1),
                    fontSize: FONT.TextXExtraSmall,
                    fontFamily: FONT_FAMILY.Montserrat
                }}>{item.discount}</Text>
            </View>
        )
    };

    _renderButtons = (backColor, text, textColor, index) => {
        return (
            <TouchableOpacity onPress={() => console.warn(index)}
                              style={{
                                  marginHorizontal: wp(2),
                                  borderRadius: wp(2),
                                  justifyContent: 'center',
                                  alignItems: 'center',
                                  paddingHorizontal: wp(3),
                                  backgroundColor: backColor,
                                  paddingVertical: wp(0.5)
                              }}>
                <Text style={{
                    letterSpacing: wp(0.1),
                    color: textColor,
                    // fontWeight: 'bold',
                    fontSize: FONT.TextSmall_2,
                    fontFamily: FONT_FAMILY.Poppins
                }}>
                    {text}
                </Text>
            </TouchableOpacity>
        )
    };

    render() {
        const {hotelListData} = this.state;
        return (
            <SafeAreaViewContainer>
                {this._renderHeader()}
                <MainContainer>
                    <FlatList
                        style={{width: wp(95), marginTop: wp(3)}}
                        data={hotelListData}
                        keyExtractor={((item, index) => index)}
                        extraData={this.state}
                        renderItem={({item, index}) =>
                            <View style={{
                                marginTop: wp(1),
                                flexDirection: 'row',
                                justifyContent: 'center',
                            }}>
                                <Image
                                    // resizeMode={'contain'}
                                    style={{
                                        width: wp(25),
                                        height: wp(30)
                                    }}
                                    source={{uri: item.imageUrl}}/>
                                <Spacer row={1}/>
                                <ShadowViewContainer style={{marginBottom: wp(2)}}>
                                    <View style={{
                                        height: wp(30),
                                        borderRadius: wp(2),
                                        backgroundColor: COLORS.white_color
                                    }}>
                                        <View style={{
                                            flexDirection: 'row',
                                            width: wp(65),
                                        }}>
                                            {this._renderHotelView(item)}
                                            {this._renderDiscountView(item)}
                                        </View>
                                        <Spacer space={2}/>
                                        <View style={{justifyContent: 'space-between', flexDirection: 'row'}}>
                                            {this._renderButtons(COLORS.app_theme_color, "View", COLORS.white_color, index)}
                                            {this._renderButtons(COLORS.detail_color, "Details", COLORS.white_color, index)}
                                        </View>
                                    </View>
                                </ShadowViewContainer>
                            </View>
                        }
                    />
                    {this._renderCustomLoader()}
                </MainContainer>
            </SafeAreaViewContainer>
        )
    }
}

export default HotelListNearMe;
