import React from "react";
import {
  FlatList,
  Platform,
  StatusBar,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import BaseClass from "../../../utils/BaseClass";
import styles from "../particularHotelProfile/classComponents/tabClasses/roomsScreen/styles";
import FastImage from "react-native-fast-image";
import { API } from "../../../redux/constant";
import STRINGS from "../../../utils/Strings";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import RoomDetailModal from "../../../customComponents/Modals/RoomDetailModal";
import { FONT } from "../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../utils/Font";
import {
  SafeAreaViewContainer,
  ShadowViewContainer,
} from "../../../utils/BaseStyle";
import AsyncStorage from "@react-native-community/async-storage";
import { GetHotelRoomDetailAction } from "../../../redux/actions/GetHotelDetail";
import { Spacer } from "../../../customComponents/Spacer";
import GreenTick from "@assets/images/CustomerHome/greenTick.svg";
import { Header } from "react-native-elements";
import COLORS from "../../../utils/Colors";
import { LeftIcon } from "../../../customComponents/icons";
import { SaveBookingDetailAction } from "../../../redux/actions/SaveBookingDetailAction";
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import CrossComponent from "../../../../assets/images/BookingIcons/CrossSVG";

export default class CustomerRoomList extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      roomsList: [],
      accessToken: "",
      hotelId: "",
      isRoomModalOpen: false,
      roomIndexClicked: 0,
      customerId: "",
      isLoading: false,
    };
  }

  componentDidMount() {
    this.setState({
      hotelId: this.props.route.params.hotelId,
      accessToken: this.props.route.params.accessToken,
      customerId: this.props.route.params.customerId,
      isLoading: true,
    });
    // AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
    //     if (result !== undefined && result !== null) {
    //         let loginData = JSON.parse(result);
    //         console.warn("login data is****", loginData);
    //         this.setState({
    //             accessToken: loginData.access_token,
    //             loginUserId: loginData.id,
    //         });
    //     }
    // });
    this.showDialog();
    GetHotelRoomDetailAction(
      {
        id: this.props.route.params.hotelId,
        accessToken: this.props.route.params.accessToken,
        customerId: this.props.route.params.customerId,
      },
      (data) => this.hotelRoomDetailResponse(data)
    );
  }

  hotelRoomDetailResponse = (data) => {
    this.hideDialog();
    this.setState({ isLoading: false });
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      const { response, message, status } = data;
      if (response !== undefined) {
        if (status === 200) {
          this.hideDialog();
          this.setState({
            roomsList: response[0].room_hotel,
          });
        }
      } else if (status === 111) {
        this.hideDialog();
        if (message.non_field_errors !== undefined) {
          // if (message.non_field_errors[0] === "User with this social_id does not exist")
        }
      } else if (status === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      }
    }
    this.hideDialog();
  };

  onRoomModalClose = () => {
    const { isRoomModalOpen } = this.state;
    this.setState({
      isRoomModalOpen: !isRoomModalOpen,
    });
    console.warn("close");
  };

  onModalHotelClick = () => {
    console.warn("Modal");
  };

  onGetRoomPress = (id) => {
    this.getRoomApi(id);
  };

  getRoomApi = (id) => {
    const { customerId, accessToken, hotelId } = this.state;
    this.showDialog();
    SaveBookingDetailAction(
      {
        room: id,
        hotel: hotelId,
        customer: customerId,
        accessToken: accessToken,
      },
      (data) => this.saveHotelDetailData(data)
    );
  };

  saveHotelDetailData = (data) => {
    this.hideDialog();
    const { navigation } = this.props;
    const { response, status, message } = data;

    //new
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (response !== undefined) {
        if (status === 200) {
          this.showToastSucess("Success");
          this.props.navigation.goBack();
        }
      } else if (status === 111) {
        if (message.non_field_errors !== undefined) {
          // if (message.non_field_errors[0] === "User with this social_id does not exist")
        } else {
          AsyncStorage.removeItem(STRINGS.loginCredentials);
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{ name: "login" }],
            })
          );
          this.showToastAlert(message);
        }
      } else if (status === 401) {
        this.showToastAlert(message);
      } else if (message.non_field_errors !== undefined) {
        this.showToastAlert(message.non_field_errors[0]);
      } else {
        this.showToastAlert(message);
      }
      this.hideDialog();
    }
    this.hideDialog();
  };

  // =============================================================================================
  // Render methods for Header
  // =============================================================================================

  _renderHeader() {
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={<LeftIcon onPress={() => navigation.goBack()} />}
        centerComponent={{
          text: STRINGS.room_list_header,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  getAdditionalDiscount = (regularPrice) => {
    return (Number(regularPrice) * 40) / 100;
  };

  render() {
    const { roomsList, isRoomModalOpen, roomIndexClicked } = this.state;
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <View style={styles.mainContainer}>
          <Spacer space={1} />
          {roomsList.length !== 0 ? (
            <View style={{ paddingTop: wp(2) }}>
              <FlatList
                nestedScrollEnabled={true}
                contentContainerStyle={styles.flatListContainer}
                data={roomsList}
                keyExtractor={(item, index) => index}
                renderItem={({ item, index }) => (
                  <View>
                    {item.is_admin_approved == 1 && (
                      <ShadowViewContainer>
                        <View style={styles.flatListMainContainer}>
                          <TouchableOpacity
                            onPress={() =>
                              this.setState({
                                isRoomModalOpen: !isRoomModalOpen,
                                roomIndexClicked: index,
                              })
                            }
                          >
                            <FastImage
                              resizeMode={FastImage.resizeMode.stretch}
                              style={styles.imageStyle}
                              source={{
                                uri:
                                  item.room_images.length !== 0
                                    ? API.IMAGE_BASE_URL +
                                      item.room_images[0].image
                                    : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                              }}
                            />
                          </TouchableOpacity>
                          <Spacer row={1} />
                          <View>
                            <View style={styles.roomTypeContainer}>
                              <View>
                                <Text
                                  numberOfLines={1}
                                  style={styles.roomTypeText}
                                >
                                  Room Type: {item.room_type}
                                </Text>
                                <Text style={styles.guestsText}>
                                  {STRINGS.no_of_Guests}: {item.no_of_guest}
                                </Text>
                              </View>

                              {item.is_special_discount === true ? (
                                <View style={styles.priceView}>
                                  <Text style={styles.discountedPrice}>
                                    {"\u20B9"}
                                    {this.getAdditionalDiscount(
                                      item.regular_price
                                    )}
                                    /
                                    <Text style={styles.actualPrice}>
                                      {" "}
                                      {item.regular_price}
                                    </Text>
                                  </Text>
                                  <Text style={styles.discountedText}>
                                    40% Discount{" "}
                                  </Text>
                                </View>
                              ) : (
                                <View style={styles.priceView}>
                                  <Text style={styles.discountedPrice}>
                                    {"\u20B9"}
                                    {item.discounted_price}/
                                    <Text style={styles.actualPrice}>
                                      {" "}
                                      {item.regular_price}
                                    </Text>
                                  </Text>
                                  <Text style={styles.discountedText}>
                                    20% Discount{" "}
                                  </Text>
                                </View>
                              )}
                            </View>
                            <Spacer space={1} />
                            {item.no_of_hours !== null &&
                              item.no_of_hours !== undefined && (
                                <View style={{ paddingHorizontal: wp(1) }}>
                                  <View>
                                    <Text style={styles.roomAvailibilityText}>
                                      Room availability :{item.no_of_hours} hour
                                    </Text>
                                    <Spacer space={0.5} />
                                  </View>
                                </View>
                              )}

                            <Spacer space={1} />
                            {item.room_service.length > 0 && (
                              <View style={{ paddingHorizontal: wp(1) }}>
                                <Text style={styles.complimentaryText}>
                                  Complimentary Services
                                </Text>
                                <Spacer space={0.5} />
                                <Text
                                  style={{
                                    width: wp(80),
                                    paddingHorizontal: wp(0.5),
                                    fontSize: FONT.TextSmall_2,
                                    color: COLORS.number_OfGuest_color,
                                  }}
                                >
                                  {item.room_service
                                    .map((e) => e.service)
                                    .join(", ")}
                                </Text>
                              </View>
                            )}

                            <Spacer space={0.5} />
                            <View style={styles.roomAvailableContainer}>
                              {item.is_available ? (
                                <>
                                  <GreenTick width={wp(4)} height={wp(4)} />
                                  <Spacer row={0.5} />
                                  <Text style={styles.roomText}>
                                    Room Available
                                  </Text>
                                </>
                              ) : (
                                <>
                                  <CrossComponent
                                    width={wp(4)}
                                    height={wp(4)}
                                  />
                                  <Spacer row={0.5} />
                                  <Text
                                    style={[
                                      styles.roomText,
                                      { color: COLORS.failure_Toast },
                                    ]}
                                  >
                                    Room Unavailable
                                  </Text>
                                </>
                              )}
                              <Spacer row={1} />

                              {item.is_available ? (
                                <TouchableOpacity
                                  disabled={!item.is_available}
                                  onPress={() => this.getRoomApi(item.id)}
                                >
                                  <View style={styles.roomButton}>
                                    <Text
                                      numberOfLines={1}
                                      style={styles.getRoomText}
                                    >
                                      Get Room
                                    </Text>
                                  </View>
                                </TouchableOpacity>
                              ) : (
                                console.log("")
                              )}
                            </View>
                          </View>
                        </View>
                      </ShadowViewContainer>
                    )}
                    <Spacer space={0.5} />
                  </View>
                )}
              />
              <RoomDetailModal
                roomModalVisible={isRoomModalOpen}
                value={roomsList}
                closeRoomModal={() => this.onRoomModalClose()}
                onHotelClick={() => this.onModalHotelClick()}
                onGetRoomClick={(id) => {
                  this.onGetRoomPress(id);
                }}
                indexRoom={roomIndexClicked}
              />
            </View>
          ) : (
            <Text
              style={{
                textAlign: "center",
                fontSize: FONT.TextNormal,
                fontFamily: FONT_FAMILY.MontserratBold,
              }}
            >
              NO DATA{" "}
            </Text>
          )}
        </View>
        {this._renderCustomLoader()}
      </SafeAreaViewContainer>
    );
  }
}
