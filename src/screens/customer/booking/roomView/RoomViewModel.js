import React, { Component } from "react";
import {
  View,
  Text,
  Modal,
  StyleSheet,
  Image,
  TouchableOpacity,
  Alert,
  FlatList,
} from "react-native";

import propTypes from "prop-types";
import * as _ from "lodash";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Spacer } from "../../../../customComponents/Spacer";
import { FONT_FAMILY } from "../../../../utils/Font";
import { FONT } from "../../../../utils/FontSizes";
import COLORS from "../../../../utils/Colors";
import STRINGS from "../../../../utils/Strings";
import { ICONS } from "../../../../utils/ImagePaths";
import FastImage from "react-native-fast-image";
import { API } from "../../../../redux/constant";

import CrossComponent from "../../../../../assets/images/BookingIcons/CrossSVG";

export default class RoomDetailModal extends Component {
  onCancelPress() {
    this.props.closeRoomModal();
    console.warn("cancel");
  }

  getAdditionalDiscount = (regularPrice) => {
    return (Number(regularPrice) * 40) / 100;
  };

  render() {
    const { value } = this.props;
    console.log("value is", value);
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={value.roomModalVisible}
      >
        <View style={styles.centeredView}>
          <View
            style={{
              marginBottom: wp(8),
              backgroundColor: "white",
              justifyContent: "center",
              alignItems: "center",
              width: wp(90),
              borderRadius: wp(3),
            }}
          >
            <View style={{ height: wp(77), paddingHorizontal: wp(1) }}>
              <FastImage
                style={{
                  justifyContent: "center",
                  marginVertical: wp(1),
                  borderRadius: wp(3),
                  width: wp(87),
                  height: wp(75),
                }}
                source={{
                  uri:
                    value.room_images.image !== null
                      ? API.IMAGE_BASE_URL + value.room_images.image
                      : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                }}
                resizeMode={FastImage.resizeMode.stretch}
              />
              <TouchableOpacity
                hitSlop={{ top: wp(5), left: wp(5), bottom: wp(5) }}
                style={{
                  position: "absolute",
                  alignSelf: "flex-end",
                  marginTop: wp(-3),
                }}
                onPress={() => {
                  this.props.onCancelPress();
                }}
              >
                <CrossComponent width={wp(9)} height={wp(9)} />
              </TouchableOpacity>
            </View>

            <Spacer space={2} />
            <View>
              <View
                style={{
                  width: wp(80),
                  flexDirection: "row",
                  justifyContent: "space-between",
                }}
              >
                <View>
                  <Text
                    style={{
                      fontSize: FONT.TextSmall,
                      fontFamily: FONT_FAMILY.Montserrat,
                      fontWeight: "bold",
                      width: wp("40%"),
                    }}
                  >
                    Hotel Name: {value.hotel_name}
                  </Text>
                  <Spacer space={0.8} />
                  <Text
                    style={{
                      fontSize: FONT.TextSmall,
                      fontFamily: FONT_FAMILY.Montserrat,
                      fontWeight: "bold",
                    }}
                  >
                    Room Type: {value.room_type}
                  </Text>
                </View>

                {value.is_special === true ? (
                  <View
                    style={{
                      // justifyContent:'flex-end',
                      flexDirection: "column",
                      alignItems: "center",
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: wp(4.1),
                        fontWeight: "bold",
                        fontStyle: "normal",
                      }}
                    >
                      {"\u20B9"}
                      {this.getAdditionalDiscount(value.room_regular_price)}
                    </Text>
                    <Text
                      style={{
                        fontWeight: "bold",
                        fontSize: wp(3.5),
                        fontFamily: FONT_FAMILY.Montserrat,
                        fontStyle: "normal",
                      }}
                    >
                      40% Discount{" "}
                    </Text>
                  </View>
                ) : (
                  <View
                    style={{
                      // justifyContent:'flex-end',
                      flexDirection: "column",
                      alignItems: "center",
                    }}
                  >
                    <Text
                      style={{
                        fontFamily: FONT_FAMILY.Roboto,
                        fontSize: wp(4.1),
                        fontWeight: "bold",
                        fontStyle: "normal",
                      }}
                    >
                      {"\u20B9"}
                      {value.room_price}
                    </Text>
                    <Text
                      style={{
                        fontWeight: "bold",
                        fontSize: wp(3.5),
                        fontFamily: FONT_FAMILY.Montserrat,
                        fontStyle: "normal",
                      }}
                    >
                      20% Discount{" "}
                    </Text>
                  </View>
                )}
              </View>
              <Spacer space={1} />

              <View
                style={{
                  justifyContent: "space-around",
                  alignItems: "center",
                  paddingHorizontal: wp(1),
                  flexDirection: "row",
                  paddingTop: wp(6),
                  paddingBottom: wp(12),
                }}
              >
                <TouchableOpacity
                  onPress={() =>
                    this.props.onHotelViewButtonPress(value.hotel_id)
                  }
                  style={{
                    borderRadius: wp(3),
                    width: wp(33),
                    paddingVertical: wp(3.5),
                    backgroundColor: COLORS.customerType,
                  }}
                >
                  <Text
                    style={{
                      textAlign: "center",
                      letterSpacing: wp(0.1),
                      fontSize: FONT.TextSmall,
                      fontFamily: FONT_FAMILY.MontserratSemiBold,
                      color: COLORS.white_color,
                    }}
                  >
                    Hotel View
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    // justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.6)",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});
