import React from "react";
import {
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
} from "react-native";
import {
  MainContainer,
  ShadowViewContainer,
  ScrollContainer,
} from "../../../../../utils/BaseStyle";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLORS from "../../../../../utils/Colors";
import { FONT } from "../../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../../utils/Font";
import { Dropdown } from "react-native-material-dropdown";
import { ICONS } from "../../../../../utils/ImagePaths";
import BaseClass from "../../../../../utils/BaseClass";
import STRINGS from "../../../../../utils/Strings";
import { Spacer } from "../../../../../customComponents/Spacer";
import { Data } from "../../../../../utils/FixtureData";
import FastImage from "react-native-fast-image";
import AsyncStorage from "@react-native-community/async-storage";
import { GetCustomerHistoryBooking } from "../../../../../redux/actions/GetCustomerHistoryBooking";
import OrientationLoadingOverlay from "@customComponents/Loader";
import { CustomeMonthYearPicker } from "../../../../../customComponents/CustomeMonthYearPicker";
import moment from "moment";
import { API } from "../../../../../redux/constant";
import propTypes from "prop-types";
import { CommonActions } from "@react-navigation/native";
import styles from "./styles";
import * as _ from "lodash";
import RoomDetailModal from "../../roomView/RoomViewModel";

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from "react-native-popup-menu";
let checkVar = false;

class CustomerBookingHistory extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      historyBookingData: undefined,
      isMonthYearPickerVisible: false,
      monthSelected: undefined,
      yearSelected: undefined,
      customerLoginId: undefined,
      requestCount: 0,
      accessToken: undefined,
      currentData: undefined,
      roomModelPopUp: false,
      isLoading: false,
      selectdMonthText: "Select Month",
      filterArr: [
        { value: "Single", isChecked: false },
        { value: "Double", isChecked: false },
        { value: "Triple", isChecked: false },
        { value: "Quad", isChecked: false },
        { value: "Twin", isChecked: false },
        { value: "Day Slot", isChecked: false },
        { value: "All", isChecked: false },
      ],
      filteredListArray: [],
    };
  }

  componentDidMount() {
    this.hitApi();
  }

  hitApi = () => {
    const { filterArr } = this.state;

    filterArr.map((element) => (element.isChecked = false));

    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        this.setState({
          accessToken: loginData.access_token,
          customerLoginId: loginData.id,
          isLoading: true,
          selectdMonthText: "Select Month",
          filterArr: filterArr,
        });
        this.getCustomerHistoryBooking(
          loginData.id,
          loginData.access_token,
          "",
          ""
        );
      }
    });
  };

  static propTypes = {
    routeKey: propTypes.number,
  };

  componentDidUpdate(prevProps, preState, snapShot) {
    if (this.props.routeKey === 0) {
      if (checkVar === true) {
        checkVar = false;
        this.hitApi();
      }
    } else if (this.props.routeKey === 1) {
      checkVar = true;
    }
  }

  getCustomerHistoryBooking(loginId, accessToken, monthSelected, yearSelected) {
    this.showDialog();

    GetCustomerHistoryBooking(
      {
        accessToken: accessToken,
        customerId: loginId,
        month: monthSelected,
        year: yearSelected,
      },
      (data) => this.getCustomerBookingHistoryResponse(data)
    );
  }

  getCustomerBookingHistoryResponse = (data) => {
    this.setState({ isLoading: false });
    const { navigation } = this.props.navigationInfo;
    const { response, message, status } = data;
    this.hideDialog();
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (response !== undefined) {
        if (status === 200) {
          this.setState({
            historyBookingData: response,
            requestCount: response.length,
            filteredListArray: _.cloneDeep(response),
          });
        }
      } else if (status === 111) {
        if (message.non_field_errors !== undefined) {
          // if (message.non_field_errors[0] === "User with this social_id does not exist")
        } else {
          AsyncStorage.removeItem(STRINGS.loginCredentials);
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{ name: "login" }],
            })
          );
          this.showToastAlert(message);
        }
      } else if (status === 401) {
        this.showToastAlert(message);
      } else if (message.non_field_errors !== undefined) {
        this.showToastAlert(message.non_field_errors[0]);
      } else {
        this.showToastAlert(message);
      }
    }
  };
  monthYearViewClick = () => {
    this.setState({ isMonthYearPickerVisible: true });
  };

  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={true}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };
  setMonthYearDate = (monthYear) => {
    const {
      accessToken,
      customerLoginId,
      isMonthYearPickerVisible,
    } = this.state;

    if (monthYear.toString().length < 10) {
      var selectedmonth = monthYear.toString().slice(0, 2);
      var selectedyear = monthYear.toString().slice(3, 7);

      this.setState({ isMonthYearPickerVisible: !isMonthYearPickerVisible });
    } else {
      this.setState({ isMonthYearPickerVisible: !isMonthYearPickerVisible });

      var date = new Date().getDate();
      var month = new Date().getMonth() + 1;
      var year = new Date().getFullYear();
      this.setState({ isMonthYearPickerVisible: !isMonthYearPickerVisible });
    }
  };

  newMonthYearSelected = (monthYear) => {
    const {
      accessToken,
      customerLoginId,
      isMonthYearPickerVisible,
    } = this.state;
    var monthYearData = moment(monthYear).format("MM-YYYY");

    var selectedmonth = monthYearData.toString().slice(0, 2);
    var selectedyear = monthYearData.toString().slice(3, 7);

    this.setState({
      isMonthYearPickerVisible: !isMonthYearPickerVisible,
      selectdMonthText: selectedmonth + "/" + selectedyear,
    });

    this.getCustomerHistoryBooking(
      customerLoginId,
      accessToken,
      selectedmonth,
      selectedyear
    );
  };

  closeMonthYearPicker = () => {
    this.setState({ isMonthYearPickerVisible: false });
  };

  //-----------------------filter ---------------------

  onMenuPress = (index) => {
    const { filterArr } = this.state;

    this.setState({
      filterArr: _.map(filterArr, (data, arrIndex) => {
        if (arrIndex === index) {
          console.log("data", data);
          data.isChecked = true;
          return data;
        } else {
          data.isChecked = false;
          return data;
        }
      }),
    });

    this.appliedFiltersAction(index);
  };

  appliedFiltersAction = (index) => {
    filterIndex = index;
    const { historyBookingData, filteredListArray } = this.state;
    let tempArr = [];
    if (filteredListArray.length > 0) {
      switch (index) {
        case 0:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Single"
          );
          break;
        case 1:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Double"
          );

          break;
        case 2:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Triple"
          );

          break;
        case 3:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Quad"
          );
          break;
        case 4:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Twin"
          );
          break;

        case 5:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Day Slot"
          );
          break;
        case 6:
          tempArr = filteredListArray;
          break;
        default:
          break;
      }
      this.setState({
        historyBookingData: tempArr,
        requestCount: tempArr.length,
      });
    }
  };
  bookingFilter = () => {
    const { filterArr } = this.state;
    return (
      <View style={{ marginRight: wp("3%") }}>
        <Menu>
          <MenuTrigger>
            <View style={{ flexDirection: "row" }}>
              <Text style={{ marginRight: 10 }}>Filters</Text>
              <Image source={ICONS.FILTERS_ICON} />
            </View>
          </MenuTrigger>
          <MenuOptions>
            <MenuOption>
              {_.map(filterArr, (item1, index) => {
                return (
                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: wp("2%"),
                      marginBottom: wp("2%"),
                    }}
                  >
                    <TouchableOpacity onPress={() => this.onMenuPress(index)}>
                      <FastImage
                        resizeMode={FastImage.resizeMode.contain}
                        style={{
                          borderRadius: wp(1),
                          borderColor: COLORS.black_color,
                          borderWidth: wp(0.2),
                          width: wp(4.5),
                          height: wp(4.5),
                          justifyContent: "center",
                        }}
                        source={
                          item1.isChecked
                            ? ICONS.CHECKED_ICON
                            : ICONS.UNCHECKED_ICON
                        }
                      />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onMenuPress(index)}>
                      <Text
                        style={{
                          justifyContent: "center",

                          color: COLORS.black_color,
                          fontFamily: FONT_FAMILY.Montserrat,
                          fontSize: FONT.TextMedium,
                          alignSelf: "center",
                          marginLeft: wp("2%"),
                          marginRight: wp("2%"),
                        }}
                      >
                        {item1.value}
                      </Text>
                    </TouchableOpacity>
                  </View>
                );
              })}
            </MenuOption>
          </MenuOptions>
        </Menu>
      </View>
    );
  };

  onRoomModalOpen = (index) => {
    const { historyBookingData } = this.state;
    this.setState({
      currentData: historyBookingData[index],
      roomModelPopUp: true,
    });
  };

  jumpToHotel = (id) => {
    const { navigation } = this.props.navigationInfo;
    this.setState({ roomModelPopUp: false });
    navigation.navigate("particularHotelProfile", {
      id: id,
      // accessToken: accessToken
    });
  };
  render() {
    const {
      historyBookingData,
      isMonthYearPickerVisible,
      requestCount,
      currentData,
      roomModelPopUp,
      selectdMonthText,
    } = this.state;
    return (
      <View
        style={{
          backgroundColor: COLORS.backGround_color,
          flex: 1,
          // paddingBottom: wp(17),
        }}
      >
        <View style={styles.outerView}>
          <View style={styles.outerView1}>
            <TouchableOpacity onPress={() => this.monthYearViewClick()}>
              <View style={styles.monthYearView}>
                <Text style={{ color: COLORS.greyButton_color2 }}>
                  {selectdMonthText}
                </Text>
                <Image
                  style={styles.downAroowImgStyle}
                  source={ICONS.DOWN_ARROW}
                />
              </View>
            </TouchableOpacity>
          </View>

          <Spacer row={2} />
          {this.bookingFilter()}
        </View>

        {isMonthYearPickerVisible === true ? (
          <CustomeMonthYearPicker
            isMonthYearPickerVisible={isMonthYearPickerVisible}
            newDateMonthSelected={(date) => this.newMonthYearSelected(date)}
            crossPickerAction={() => this.closeMonthYearPicker()}
          />
        ) : (
          console.log("isMonthYearPickerVisible")
        )}
        <Spacer space={3} />

        {requestCount !== 0 ? (
          <FlatList
            style={{ paddingBottom: wp(17) }}
            data={historyBookingData}
            keyExtractor={(item, index) => index}
            renderItem={({ item, index }) => (
              <View style={{ width: wp(100) }}>
                <ShadowViewContainer>
                  <View style={styles.shadowInnerView}>
                    <TouchableOpacity
                      onPress={() => this.onRoomModalOpen(index)}
                    >
                      <FastImage
                        style={{
                          width: wp(30),
                          height: wp(45),
                        }}
                        source={{
                          uri:
                            item.room_images !== null &&
                            item.room_images.image !== null &&
                            item.room_images.image !== ""
                              ? API.IMAGE_BASE_URL + item.room_images.image
                              : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                        }}
                        resizeMode={FastImage.resizeMode.stretch}
                      />
                    </TouchableOpacity>
                    {console.log("status", item)}
                    <Spacer row={1} />
                    <View style={{ width: wp(57) }}>
                      <Spacer space={1} />
                      <Text numberOfLines={1} style={styles.textHotelName}>
                        Hotel Name : {item.hotel_name}
                      </Text>
                      <Spacer space={0.5} />
                      <Text numberOfLines={1} style={styles.textDate}>
                        Date : {item.booking_created}
                      </Text>
                      <Spacer space={1} />
                      <Text numberOfLines={1} style={styles.textRoomType}>
                        Room Type : {item.room_type}
                      </Text>
                      <Spacer space={1} />
                      <Text numberOfLines={1} style={styles.textCheckInTime}>
                        Check In Time: {item.check_in_time}
                      </Text>
                      <Spacer space={1} />
                      <Text numberOfLines={1} style={styles.checkInDateStyle}>
                        Check in date: {item.check_in}
                      </Text>
                      {item.no_of_hours !== null ? (
                        <View>
                          <Spacer space={1} />
                          <Text
                            numberOfLines={1}
                            style={styles.checkInDateStyle}
                          >
                            Staying hour: {item.no_of_hours} hr
                          </Text>
                        </View>
                      ) : (
                        <View>
                          <Spacer space={1} />
                          <Text
                            numberOfLines={1}
                            style={styles.checkInDateStyle}
                          >
                            Staying days: {item.no_of_days} days
                          </Text>
                        </View>
                      )}
                      <Spacer space={1} />
                      {console.log("sdfsf",item.is_pending)}
                      {item.is_pending === 1 ? (
                        <Text numberOfLines={1} style={styles.confirmCheckOutText}>
                         Confirmed
                        </Text>
                      ) : (
                        <Text numberOfLines={1} style={styles.confirmCheckOutText}>
                         CheckOut
                        </Text>
                      )}
                    </View>
                  </View>
                </ShadowViewContainer>
                <Spacer space={0} />
              </View>
            )}
          />
        ) : (
          <Text style={styles.noDataStyle}>NO DATA </Text>
        )}
        {this._renderCustomLoader}
        {roomModelPopUp === true && (
          <RoomDetailModal
            isModelVisible={roomModelPopUp}
            value={currentData}
            onHotelViewButtonPress={(id) => this.jumpToHotel(id)}
            onCancelPress={() =>
              this.setState({
                roomModelPopUp: !roomModelPopUp,
              })
            }
          />
        )}
      </View>
    );
  }
}

export default CustomerBookingHistory;
