import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Platform, StyleSheet } from "react-native";
import { FONT } from "../../../../../utils/FontSizes";
import COLORS from "../../../../../utils/Colors";
import { FONT_FAMILY } from "../../../../../utils/Font";

const styles = StyleSheet.create({
  outerView: {
    width: wp(100),
    flexDirection: "row",
    paddingVertical: wp(3),
    backgroundColor: COLORS.light_grey2,
    alignItems: "center",
    justifyContent: "flex-end",
    paddingRight: wp(3),
  },

  outerView1: {
    backgroundColor: COLORS.white_color,
    borderRadius: wp(4),
    width: wp("30%"),
    height: wp("8%"),
    justifyContent: "center",
  },
  monthYearView: {
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  downAroowImgStyle: {
    height: 15,
    width: 15,
    alignSelf: "center",
  },
  shadowInnerView: {
    flexDirection: "row",
    width: wp(90),
  },
  textHotelName: {
    lineHeight: wp(5),
    color: COLORS.black_color,
    fontSize: FONT.TextSmall,
    fontFamily: FONT_FAMILY.Poppins,
  },
  textDate: {
    color: COLORS.black_color,
    fontSize: FONT.TextSmall_2,
    fontFamily: FONT_FAMILY.Montserrat,
  },
  textRoomType: {
    color: COLORS.black_color,
    fontSize: FONT.TextSmall_2,
    fontFamily: FONT_FAMILY.Montserrat,
  },
  textCheckInTime: {
    fontSize: FONT.TextSmall_2,
    fontFamily: FONT_FAMILY.Roboto,
  },

  checkInDateStyle: {
    fontSize: FONT.TextSmall_2,
    fontFamily: FONT_FAMILY.Roboto,
  },

  noDataStyle: {
    paddingVertical: wp(3),
    textAlign: "center",
    fontSize: FONT.TextNormal,
    fontFamily: FONT_FAMILY.MontserratBold,
  },
confirmCheckOutText:{

  color:COLORS.green_color,
  fontSize:wp('3.5%'),
  fontWeight:'bold'
}

});
export default styles;
