import React from "react";
import {
  Modal,
  View,
  Text,
  TouchableOpacity,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import propTypes from "prop-types";
import { Spacer } from "../../../../../../customComponents/Spacer";
import BaseClass from "../../../../../../utils/BaseClass";
import { styles } from "./styles";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import CrossComponent from "../../../../../../../assets/images/BookingIcons/CrossSVG";
import COLORS from "../../../../../../utils/Colors";

export class SaveCancelBookingModel extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      modalVisiblePass: props.isModelVisible,
    };
  }

  static propTypes = {
    isModelVisible: propTypes.any,
    onHotelClick: propTypes.func,
    value: propTypes.string,
    cancelClick: propTypes.func,
    confirmClick: propTypes.func,
  };

  closeModal = () => {
    this.props.closeModal();
  };

  render() {
    const { modalVisiblePass } = this.state;
    const { value, currentItemClicked } = this.props;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisiblePass}
      >
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          behavior={Platform.OS === "ios" ? "padding" : null}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              Keyboard.dismiss();
            }}
          >
            <View style={styles.centeredView}>
              <View style={styles.alertHeaderView}>
                <View style={styles.alertMainView}>
                  <View style={styles.alertTitileView}>
                    <Text style={styles.alertTextView}>Alert</Text>
                  </View>
                  <View style={styles.centeredTextView}>
                    {currentItemClicked !== undefined && value == undefined ? (
                      <Text style={styles.alertTextStyle}>
                        Do you want to cancel or confirm the booking?
                      </Text>
                    ) : currentItemClicked === undefined &&
                      value !== undefined ? (
                      value.no_of_days !== undefined &&
                      value.no_of_days !== null ? (
                        <Text style={styles.alertTextStyle}>
                          Dear Customer, Please confirm your check In Time{" "}
                          {value.check_in_time} and {value.no_of_days} days
                          stay.
                        </Text>
                      ) : (
                        <Text style={styles.alertTextStyle}>
                          Dear Customer, Please confirm your check In Time{" "}
                          {value.check_in_time} and {value.no_of_hours} hr stay.
                        </Text>
                      )
                    ) : currentItemClicked !== undefined &&
                      value !== undefined ? (
                      currentItemClicked.id == value.booking_id ? (
                        value.no_of_days !== undefined &&
                        value.no_of_days !== null ? (
                          <Text style={styles.alertTextStyle}>
                            Dear Customer, Please confirm your check In Time{" "}
                            {value.check_in_time} and {value.no_of_days} days
                            stay.
                          </Text>
                        ) : (
                          <Text style={styles.alertTextStyle}>
                            Dear Customer, Please confirm your check In Time{" "}
                            {value.check_in_time} and {value.no_of_hours} hr
                            stay.
                          </Text>
                        )
                      ) : (
                        <Text style={styles.alertTextStyle}>
                          Do you want to cancel or confirm the booking?
                        </Text>
                      )
                    ) : (
                      <Text style={styles.alertTextStyle}>
                        Do you want to cancel or confirm the booking?
                      </Text>
                    )}

                    {/* {currentItemClicked !== undefined ? (
                      <Text style={styles.alertTextStyle}>
                        Do you want to cancel or confirm the booking?
                      </Text>
                    ) : value.no_of_days !== undefined &&
                      value.no_of_days !== null ? (
                      <Text style={styles.alertTextStyle}>
                        Dear Customer, Please confirm your check In Time{" "}
                        {value.check_in_time} and {value.no_of_days} days stay.
                      </Text>
                    ) : (
                      <Text style={styles.alertTextStyle}>
                        Dear Customer, Please confirm your check In Time{" "}
                        {value.check_in_time} and {value.no_of_hours} hr stay.
                      </Text>
                    )} */}
                  </View>
                  <Spacer space={3} />
                  <View style={{ flexDirection: "row", alignSelf: "center" }}>
                    <TouchableOpacity
                      style={styles.cancelView}
                      onPress={() => {
                        this.props.cancelClick();
                      }}
                    >
                      <Text style={styles.cancelTextStyle}>Cancel </Text>
                    </TouchableOpacity>
                    <Spacer row={2} />

                    {/* {currentItemClicked !== undefined &&
                      value !== undefined &&
                      currentItemClicked.id == value.booking_id ? (
                        <TouchableOpacity
                          disabled={false}
                          style={[
                            styles.confirmView,
                            {
                              backgroundColor: COLORS.green_color,
                            },
                          ]}
                          onPress={() => {
                            this.props.confirmClick();
                          }}
                        >
                          <Text style={styles.confirmText}>Confirm</Text>
                        </TouchableOpacity>
                      ):(

                        <TouchableOpacity
                        disabled={true}
                        style={[
                          styles.confirmView,
                          {
                            backgroundColor: COLORS.greyButton_color,
                          },
                        ]}
                        onPress={() => {
                          this.props.confirmClick();
                        }}
                      >
                        <Text style={styles.confirmText}>Confirm</Text>
                      </TouchableOpacity>


                      )} */}

                    {currentItemClicked !== undefined && value === undefined && (
                      <TouchableOpacity
                        disabled={currentItemClicked !== undefined}
                        style={[
                          styles.confirmView,
                          {
                            backgroundColor:
                              currentItemClicked !== undefined
                                ? COLORS.greyButton_color
                                : COLORS.green_color,
                          },
                        ]}
                        onPress={() => {
                          this.props.confirmClick();
                        }}
                      >
                        <Text style={styles.confirmText}>Confirm</Text>
                      </TouchableOpacity>
                    )}

                    {currentItemClicked !== undefined &&
                      value !== undefined &&
                      (currentItemClicked.id == value.booking_id ? (
                        <TouchableOpacity
                          disabled={false}
                          style={[
                            styles.confirmView,
                            {
                              backgroundColor: COLORS.green_color,
                            },
                          ]}
                          onPress={() => {
                            this.props.confirmClick();
                          }}
                        >
                          <Text style={styles.confirmText}>Confirm</Text>
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity
                          disabled={true}
                          style={[
                            styles.confirmView,
                            {
                              backgroundColor: COLORS.greyButton_color,
                            },
                          ]}
                          onPress={() => {
                            this.props.confirmClick();
                          }}
                        >
                          <Text style={styles.confirmText}>Confirm</Text>
                        </TouchableOpacity>
                      ))}

                    {currentItemClicked === undefined && value !== undefined && (
                      <TouchableOpacity
                        disabled={false}
                        style={[
                          styles.confirmView,
                          {
                            backgroundColor: COLORS.green_color,
                          },
                        ]}
                        onPress={() => {
                          this.props.confirmClick();
                        }}
                      >
                        <Text style={styles.confirmText}>Confirm</Text>
                      </TouchableOpacity>
                    )}
                  </View>
                  <Spacer space={4} />
                </View>
                <TouchableOpacity
                  hitSlop={{ top: wp(5), left: wp(5), bottom: wp(5) }}
                  style={{
                    position: "absolute",
                    alignSelf: "flex-end",
                    marginTop: wp(-3),
                  }}
                  onPress={() => {
                    this.closeModal();
                  }}
                >
                  <CrossComponent width={wp(9)} height={wp(9)} />
                </TouchableOpacity>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </Modal>
    );
  }
}
