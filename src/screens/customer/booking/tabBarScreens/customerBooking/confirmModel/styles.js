import React from "react";
import { StyleSheet } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLORS from "../../../../../../utils/Colors";
import { FONT } from "../../../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../../../utils/Font";

const styles = StyleSheet.create({
  mainView: {
    width: wp(100),
    flexDirection: "row",
    paddingVertical: wp(3),
    backgroundColor: COLORS.light_grey2,
    alignItems: "center",
    justifyContent: "space-around",
  },
  textTotalBooking: {
    color: COLORS.placeholder_color,
    fontSize: FONT.TextSmall,
    fontFamily: FONT_FAMILY.Montserrat,
    width: wp(38),
    marginLeft:wp('3%')
  },

  monthYearView: {
    backgroundColor: COLORS.white_color,
    width: wp("40%"),
    paddingVertical: 5,
    borderRadius: wp(4),
  },
  selectMonthYearView: {
    flexDirection: "row",
    width: wp("40%"),
    justifyContent: "space-evenly",
  },
  calenderImage: {
    height: 15,
    width: 15,
    alignSelf: "center",
  },

  hotelName: {
    color: COLORS.black_color,
    fontSize: FONT.TextSmall,
    fontFamily: FONT_FAMILY.Poppins,
  },
  dateText: {
    color: COLORS.black_color,
    fontSize: FONT.TextSmall_2,
    fontFamily: FONT_FAMILY.Montserrat,
  },
  priceText: {
    fontSize: FONT.TextSmall_2,
    fontFamily: FONT_FAMILY.MontserratSemiBold,
  },

  roomTypeText: {
    color: COLORS.black_color,
    fontSize: FONT.TextSmall_2,
    fontFamily: FONT_FAMILY.Montserrat,
  },

  priceTextView:{
    flexDirection: "row",
    justifyContent: "space-between",
  },

  cencelView:{
    marginRight: wp(2),
    backgroundColor: COLORS.customerType,
    borderRadius: wp(2),
    marginBottom:wp('1.5%')
  },

  cancelText:{
    color: COLORS.white_color,
    fontSize: FONT.TextSmall_2,
    fontFamily: FONT_FAMILY.Poppins,
    paddingHorizontal: wp(5),
    paddingVertical: wp(1),
  },

  noData:{
    paddingVertical: wp(3),
    textAlign: "center",
    fontSize: FONT.TextNormal,
    fontFamily: FONT_FAMILY.MontserratBold,
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.6)",
  },
  centeredTextView: {
    // flexDirection: "row",
    alignItems: "center",
    paddingVertical: wp(4),
    paddingHorizontal: wp(2),
    backgroundColor: COLORS.white_color,
    width: wp(93),
  },
  alertTextView: {
    fontSize: FONT.TextMedium_2,
    fontFamily: FONT_FAMILY.MontserratSemiBold,
    color: COLORS.white_color,
  },
  alertTitileView: {
    alignItems: "center",
    paddingVertical: wp(4),
    backgroundColor: COLORS.app_theme_color,
    justifyContent: "flex-start",
  },
  alertMainView: {
    backgroundColor: COLORS.white_color,
    width: wp(93),
    borderRadius: wp(5),
  },
  alertHeaderView: {
    backgroundColor: "transparent",
    alignItems: "center",
    width: wp(98),
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
  cancelView: {
    borderRadius: wp(2),
    alignSelf: "center",
    width: wp(40),
    paddingVertical: wp(3),
    backgroundColor: COLORS.customerType,
  },

  cancelTextStyle: {
    textAlign: "center",
    color: COLORS.white_color,
    fontSize: FONT.TextSmall,
    fontFamily: FONT_FAMILY.MontserratSemiBold,
  },

    confirmView: {
        borderRadius: wp(2),
        alignSelf: "center",
        width: wp(40),
        paddingVertical: wp(3),
    },

  confirmText: {
    textAlign: "center",
    color: COLORS.white_color,
    fontSize: FONT.TextSmall,
    fontFamily: FONT_FAMILY.MontserratSemiBold,
  },
  alertTextStyle: {
    width: wp(75),
    textAlign: "center",
    fontSize: FONT.TextSmall,
    fontFamily: FONT_FAMILY.Montserrat,
  },
});

export { styles };
