import React from "react";
import { FlatList, Image, Text, TouchableOpacity, View } from "react-native";
import { ShadowViewContainer } from "../../../../../utils/BaseStyle";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLORS from "../../../../../utils/Colors";
import { FONT } from "../../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../../utils/Font";
import { ICONS } from "../../../../../utils/ImagePaths";
import BaseClass from "../../../../../utils/BaseClass";
import STRINGS from "../../../../../utils/Strings";
import { Spacer } from "../../../../../customComponents/Spacer";
import FastImage from "react-native-fast-image";
import { SaveCancelBookingModel } from "./confirmModel/index";
import { CustomerCancelBooking } from "../../../../../redux/actions/CustomerCancelBooking";
import { CustomerSaveBookingConfirm } from "../../../../../redux/actions/CustomerSaveBookingConfirm";
import { GetCustomerBooking } from "../../../../../redux/actions/GetCustomerBooking";
import AsyncStorage from "@react-native-community/async-storage";
import { API } from "../../../../../redux/constant";
import OrientationLoadingOverlay from "@customComponents/Loader";
import { CustomeMonthYearPicker } from "../../../../../customComponents/CustomeMonthYearPicker";
import moment from "moment";
import propTypes from "prop-types";
import { CommonActions } from "@react-navigation/native";
import { styles } from "./confirmModel/styles";
import * as _ from "lodash";
import RoomDetailModal from "../../roomView/RoomViewModel";

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from "react-native-popup-menu";
let checkVar = false;
class CustomerBooking extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      isConfirmCancelModelVisible: false,
      currentItemClicked: undefined,
      accessToken: undefined,
      customerBookingData: undefined,
      customerLoginId: undefined,
      pendingRequestCount: undefined,
      isMonthYearPickerVisible: false,
      monthSelected: undefined,
      yearSelected: undefined,
      notificationResponse: undefined,
      selectdMonthText: "Select Month",
      currentData: undefined,
      roomModelPopUp: false,
      filterArr: [
        { value: "Single", isChecked: false },
        { value: "Double", isChecked: false },
        { value: "Triple", isChecked: false },
        { value: "Quad", isChecked: false },
        { value: "Twin", isChecked: false },
        { value: "Day Slot", isChecked: false },
        { value: "All", isChecked: false },
      ],
      filteredListArray: [],
    };
  }

  componentDidMount() {
    this.hitApi();
  }

  hitApi = () => {
    const { filterArr } = this.state;
    filterArr.map((element) => (element.isChecked = false));
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        this.setState({
          accessToken: loginData.access_token,
          customerLoginId: loginData.id,
          selectdMonthText: "Select Month",
          filterArr: filterArr,
        });
        this.getCustomerBooking(loginData.id, loginData.access_token, "", "");
      }
    });
  };

  static propTypes = {
    routeKey: propTypes.number,
    notificationData: propTypes.object,
  };

  componentDidUpdate(prevProps, preState, snapShot) {
    const { notificationResponse } = this.state;
    // notificationResponse
    debugger
    console.log('data--------is-----',this.props.notificationData)
    if (this.props.notificationData !== undefined) {
      
      this.setState({
        isConfirmCancelModelVisible: true,
        notificationResponse: this.props.notificationData,
      });
      
    }
    if (this.props.routeKey === 0) {
      if (checkVar === true) {
        checkVar = false;
        this.hitApi();
      }
    } else if (this.props.routeKey === 1) {
      checkVar = true;
    }
  }

  //=======================get customer booking ======================

  getCustomerBooking = (loginId, accessToken, month, year) => {
    this.showDialog();
    GetCustomerBooking(
      {
        accessToken: accessToken,
        customerId: loginId,
        month: month,
        year: year,
      },
      (data) => this.getCustomerBookingResponse(data)
    );
  };

  getCustomerBookingResponse = (data) => {
    const { navigation } = this.props.navigationInfo;
    const { response, message, status } = data;
    this.hideDialog();
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (response !== undefined) {
        if (status === 200) {
          this.setState({
            customerBookingData: response,
            pendingRequestCount: response.length,
            filteredListArray: _.cloneDeep(response),
          });
        }
      } else if (status === 111) {
        if (message.non_field_errors !== undefined) {
          // if (message.non_field_errors[0] === "User with this social_id does not exist")
        } else {
          AsyncStorage.removeItem(STRINGS.loginCredentials);
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{ name: "login" }],
            })
          );
          this.showToastAlert(message);
        }
      } else if (status === 401) {
        this.showToastAlert(message);
      } else if (message.non_field_errors !== undefined) {
        this.showToastAlert(message.non_field_errors[0]);
      } else {
        this.showToastAlert(message);
      }
    }
  };

  //==============cancel booking clicked ============

  cancelBookingClicked = () => {
    const {
      currentItemClicked,
      accessToken,
      customerLoginId,
      notificationResponse,
    } = this.state;
    const { notificationData } = this.props;
    this.setState({ isConfirmCancelModelVisible: false });
    let currentDate = this.getCurrentDate();
    console.log("notificationResponse", notificationResponse);
    console.log("currentItemClicked", currentItemClicked);

    if (
      notificationResponse !== undefined &&
      currentItemClicked !== undefined
    ) {
      this.showDialog()
      CustomerCancelBooking(
        {
          id: currentItemClicked.id,
          cancelled_by: customerLoginId,
          cancelled_by_date: currentDate,
          accessToken: accessToken,
        },
        (data) => this.getCancelConfirmBookingResponse(data)
      );
    } else if (
      notificationResponse === undefined &&
      currentItemClicked !== undefined
    ) {
      this.showDialog();
      CustomerCancelBooking(
        {
          id: currentItemClicked.id,
          cancelled_by: customerLoginId,
          cancelled_by_date: currentDate,
          accessToken: accessToken,
        },
        (data) => this.getCancelConfirmBookingResponse(data)
      );
    } else if (
      notificationResponse !== undefined &&
      currentItemClicked === undefined
    ) {
      this.showDialog();
      CustomerCancelBooking(
        {
          id: notificationResponse.booking_id,
          cancelled_by: customerLoginId,
          cancelled_by_date: currentDate,
          accessToken: accessToken,
        },
        (data) => this.getCancelConfirmBookingResponse(data)
      );
    }
  };

  getCurrentDate = () => {
    let date = new Date().getDate();
    let month = new Date().getMonth() + 1;
    let year = new Date().getFullYear();

    return year + "-" + month + "-" + date; //format: dd-mm-yyyy;
  };

  getCancelConfirmBookingResponse = (data) => {
    const {
      customerLoginId,
      accessToken,
      isConfirmCancelModelVisible,
      notificationResponse,
    } = this.state;
    const { navigation } = this.props;
    this.hideDialog();
    const { response, message, status } = data;

    if (data === "Network request failed" || data == "TypeError: Network request failed") {

      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (response !== undefined) {
        if (status === 200) {
          this.showToastSucess(message);
          this.setState({ isConfirmCancelModelVisible: false });
          this.getCustomerBooking(customerLoginId, accessToken, "", "");
        }
      } else if (status === 111) {
        if (message.non_field_errors !== undefined) {
          // if (message.non_field_errors[0] === "User with this social_id does not exist")
        } else {
          AsyncStorage.removeItem(STRINGS.loginCredentials);
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{ name: "login" }],
            })
          );
          this.showToastAlert(message);
        }
      } else if (status === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else {
        this.hideDialog();
        this.showToastAlert(message);
      }
      this.hideDialog();
    }
  };
  //=====================end================================

  //==================== confirm booking========================
  confirmBookingClicked = () => {
    const {
      currentItemClicked,
      accessToken,
      notificationResponse,
    } = this.state;
    const { notificationData } = this.props;

    this.setState({ isConfirmCancelModelVisible: false });
    this.showDialog();

    if (
      notificationResponse !== undefined &&
      currentItemClicked === undefined
    ) {
      if (
        notificationResponse.no_of_hours !== null &&
        notificationResponse.no_of_hours !== undefined
      ) {
        CustomerSaveBookingConfirm(
          {
            id: notificationResponse.booking_id,
            no_of_days: 0,
            no_of_hours: notificationResponse.no_of_hours,
            check_in_time:
              notificationResponse !== undefined
                ? notificationResponse.check_in_time
                : "",
            accessToken: accessToken,
          },
          (data) => this.getCancelConfirmBookingResponse(data)
        );
      } else {
        CustomerSaveBookingConfirm(
          {
            id: notificationResponse.booking_id,
            no_of_days: notificationResponse.no_of_days,
            check_in_time:
              notificationResponse !== undefined
                ? notificationResponse.check_in_time
                : "",
            accessToken: accessToken,
          },
          (data) => this.getCancelConfirmBookingResponse(data)
        );
      }
    } else if (
      notificationResponse !== undefined &&
      currentItemClicked !== undefined
    ) {
      if (
        currentItemClicked.no_of_hours !== null &&
        currentItemClicked.no_of_hours !== undefined
      ) {
        CustomerSaveBookingConfirm(
          {
            id: currentItemClicked.id,
            no_of_days: 0,
            no_of_hours: notificationResponse.no_of_hours,
            check_in_time:
              notificationResponse !== undefined
                ? notificationResponse.check_in_time
                : "",
            accessToken: accessToken,
          },
          (data) => this.getCancelConfirmBookingResponse(data)
        );
      } else {
        CustomerSaveBookingConfirm(
          {
            id: currentItemClicked.id,
            no_of_days: notificationResponse.no_of_days,
            check_in_time:
              notificationResponse !== undefined
                ? notificationResponse.check_in_time
                : "",
            accessToken: accessToken,
          },
          (data) => this.getCancelConfirmBookingResponse(data)
        );
      }
    }
  };

  newMonthYearSelected = (monthYear) => {
    const {
      accessToken,
      customerLoginId,
      isMonthYearPickerVisible,
    } = this.state;
    let monthYearData = moment(monthYear).format("MM-YYYY");

    let selectedmonth = monthYearData.toString().slice(0, 2);
    let selectedyear = monthYearData.toString().slice(3, 7);

    this.setState({
      isMonthYearPickerVisible: !isMonthYearPickerVisible,
      selectdMonthText: selectedmonth + "/" + selectedyear,
    });

    this.getCustomerBooking(
      customerLoginId,
      accessToken,
      selectedmonth,
      selectedyear
    );
  };

  monthYearViewClick = () => {
    this.setState({ isMonthYearPickerVisible: true });
  };
  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  closeMonthYearPicker = () => {
    this.setState({ isMonthYearPickerVisible: false });
  };

  //--------------------------Filter-----------------------------
  onMenuPress = (index) => {
    const { filterArr } = this.state;
    this.setState({
      filterArr: _.map(filterArr, (data, arrIndex) => {
        if (index === arrIndex) {
          // data.value = !data.value;

          data.isChecked = true;
          return data;
        } else {
          data.isChecked = false;
          return data;
        }
      }),
    });

    this.appliedFiltersAction(index);
  };

  appliedFiltersAction = (index) => {
    const { customerBookingData, filteredListArray } = this.state;
    let tempArr = [];
    if (filteredListArray.length > 0) {
      switch (index) {
        case 0:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Single"
          );
          break;
        case 1:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Double"
          );

          break;
        case 2:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Triple"
          );

          break;
        case 3:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Quad"
          );
          break;
        case 4:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Twin"
          );
          break;
        case 5:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Day Slot"
          );
          break;

        case 6:
          tempArr = filteredListArray;
          break;
        default:
          break;
      }
      this.setState({
        customerBookingData: tempArr,
        pendingRequestCount:tempArr.length
      });
    }
  };

  bookingFilter = () => {
    const { filterArr } = this.state;
    return (
      <View style={{ marginRight: wp("3%") }}>
        <Menu>
          <MenuTrigger>
            <View style={{ flexDirection: "row" }}>
              <Text style={{ marginRight: 10 }}>Filters</Text>
              <Image source={ICONS.FILTERS_ICON} />
            </View>
          </MenuTrigger>
          <MenuOptions>
            <MenuOption>
              {_.map(filterArr, (item1, index) => {
                return (
                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: wp("2%"),
                      marginBottom: wp("2%"),
                    }}
                  >
                    <TouchableOpacity onPress={() => this.onMenuPress(index)}>
                      <FastImage
                        resizeMode={FastImage.resizeMode.contain}
                        style={{
                          borderRadius: wp(1),
                          borderColor: COLORS.black_color,
                          borderWidth: wp(0.2),
                          width: wp(4.5),
                          height: wp(4.5),
                          justifyContent: "center",
                        }}
                        source={
                          item1.isChecked
                            ? ICONS.CHECKED_ICON
                            : ICONS.UNCHECKED_ICON
                        }
                      />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onMenuPress(index)}>
                      <Text
                        style={{
                          justifyContent: "center",

                          color: COLORS.black_color,
                          fontFamily: FONT_FAMILY.Montserrat,
                          fontSize: FONT.TextMedium,
                          alignSelf: "center",
                          marginLeft: wp("2%"),
                          marginRight: wp("2%"),
                        }}
                      >
                        {item1.value}
                      </Text>
                    </TouchableOpacity>
                  </View>
                );
              })}
            </MenuOption>
          </MenuOptions>
        </Menu>
      </View>
    );
  };

  onRoomModalOpen = (index) => {
    const { customerBookingData } = this.state;
    this.setState({
      currentData: customerBookingData[index],
      roomModelPopUp: true,
    });
  };

  jumpToHotel = (id) => {
    const { navigation } = this.props.navigationInfo;
    this.setState({ roomModelPopUp: false });
    navigation.navigate("particularHotelProfile", {
      id: id,
      // accessToken: accessToken
    });
  };

  closeConfirmCancelModel = () => {
    const { isConfirmCancelModelVisible, notificationResponse } = this.state;
    this.setState({
      isConfirmCancelModelVisible: false,
    });
  };

  getAdditionalDiscount=(regularPrice)=>{

  
    return (Number(regularPrice) * 40)/100
      }

  render() {
    const {
      isConfirmCancelModelVisible,
      customerBookingData,
      pendingRequestCount,
      isMonthYearPickerVisible,
      selectdMonthText,
      roomModelPopUp,
      currentData,
      currentItemClicked,
      notificationResponse,
    } = this.state;
    return (
      <View
        style={{
          backgroundColor: COLORS.backGround_color,
          flex: 1,
          // paddingBottom: wp(17),
        }}
      >
        <View style={styles.mainView}>
          <Text numberOfLines={1} style={styles.textTotalBooking}>
            Total Booking :{pendingRequestCount}
          </Text>
          <View style={styles.monthYearView}>
            <TouchableOpacity onPress={() => this.monthYearViewClick()}>
              <View style={styles.selectMonthYearView}>
                <Text style={{ color: COLORS.greyButton_color2 }}>
                  {selectdMonthText}
                </Text>
                <Image style={styles.calenderImage} source={ICONS.DOWN_ARROW} />
              </View>
            </TouchableOpacity>
          </View>

          {this.bookingFilter()}

          {/* <TouchableOpacity onPress={() => this.showToastAlert(STRINGS.COMING_SOON)}>
                        <Image source={ICONS.FILTERS_ICON}/>
                    </TouchableOpacity> */}
        </View>
        {isMonthYearPickerVisible === true && (
          <CustomeMonthYearPicker
            isMonthYearPickerVisible={isMonthYearPickerVisible}
            newDateMonthSelected={(date) => this.newMonthYearSelected(date)}
            crossPickerAction={() => this.closeMonthYearPicker()}
          />
        )}
        <Spacer space={3} />
        {pendingRequestCount !== 0 ? (
          <FlatList
            style={{ paddingBottom: wp(17) }}
            data={customerBookingData}
            keyExtractor={(item, index) => index}
            renderItem={({ item, index }) => (
              <View style={{ width: wp(100) }}>
                <ShadowViewContainer>
                  <View style={{ flexDirection: "row", width: wp(90) }}>
                    <TouchableOpacity
                      onPress={() => this.onRoomModalOpen(index)}
                    >
                      <FastImage
                        style={{
                          width: wp(30),
                          height: wp(35),
                        }}
                        source={{
                          uri:
                            item.room_images !== null &&
                            item.room_images.image !== null &&
                            item.room_images.image !== ""
                              ? API.IMAGE_BASE_URL + item.room_images.image
                              : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                        }}
                        resizeMode={FastImage.resizeMode.stretch}
                      />
                    </TouchableOpacity>
                    <Spacer row={1} />
                    <View style={{ width: wp(57) }}>
                      <Spacer space={1} />
                      <Text numberOfLines={1} style={styles.hotelName}>
                        Hotel Name : {item.hotel_name}
                      </Text>
                      <Spacer space={0.5} />
                      <Text numberOfLines={1} style={styles.dateText}>
                        Date : {item.booking_created}
                      </Text>
                      <Spacer space={1} />

                      <Text numberOfLines={1} style={styles.roomTypeText}>
                        Room Type : {item.room_type}
                      </Text>
                      <Spacer space={1} />
                      {item.no_of_hours !== null && (
                        <View>
                          <Text numberOfLines={1} style={styles.roomTypeText}>
                            Staying hour : {item.no_of_hours} hr
                          </Text>
                          <Spacer space={1} />
                        </View>
                      )}

                      <View style={styles.priceTextView}>
                       {item.is_special=== true ?
                        <View>
                        <Text numberOfLines={1} style={styles.priceText}>
                          Price : {"\u20B9"}
                          {this.getAdditionalDiscount(item.room_regular_price)}
                        </Text>
                        </View>
                        :
                        <View>
                        <Text numberOfLines={1} style={styles.priceText}>
                          Price : {"\u20B9"}
                          {item.room_price}
                        </Text>
                        </View>
                      }
                       
                        <TouchableOpacity
                          // disabled={true}
                          style={styles.cencelView}
                          onPress={() =>
                            this.setState({
                              isConfirmCancelModelVisible: true,
                              currentItemClicked: item,
                            })
                          }
                        >
                          <Text numberOfLines={1} style={styles.cancelText}>
                            Cancel
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </ShadowViewContainer>
                <Spacer space={0} />
              </View>
            )}
          />
        ) : (
          <Text style={styles.noData}>NO DATA </Text>
        )}
        {this._renderCustomLoader()}
        {isConfirmCancelModelVisible === true && (
          <SaveCancelBookingModel
            isModelVisible={isConfirmCancelModelVisible}
            cancelClick={() => this.cancelBookingClicked()}
            confirmClick={() => this.confirmBookingClicked()}
            value={notificationResponse}
            currentItemClicked={currentItemClicked}
            closeModal={() => this.closeConfirmCancelModel()}
          />
        )}

        {roomModelPopUp === true && (
          <RoomDetailModal
            isModelVisible={roomModelPopUp}
            value={currentData}
            onHotelViewButtonPress={(id) => this.jumpToHotel(id)}
            onCancelPress={() =>
              this.setState({
                roomModelPopUp: !roomModelPopUp,
              })
            }
          />
        )}
      </View>
    );
  }
}

CustomerBooking.defaultProps = {
  notificationData: undefined,
};

export default CustomerBooking;
