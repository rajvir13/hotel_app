import React from 'react';
import {Platform, StatusBar, View} from 'react-native';
import BaseClass from "../../../utils/BaseClass";
import {SafeAreaViewContainer, MainContainer} from "../../../utils/BaseStyle";
import CustomerBookingTopTab from "./bookingTopTab";
import {Header} from "react-native-elements";
import {FONT_FAMILY} from "../../../utils/Font";
import COLORS from "../../../utils/Colors";
import {FONT} from "../../../utils/FontSizes";
import STRINGS from "../../../utils/Strings";
import checkVar from './tabBarScreens/customerBooking/index'
export default class CustomerMainBooking extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            notificationData: undefined
        }
    }

    componentDidMount() {
        if (this.props.route.params !== null && this.props.route.params !== undefined) {
            if (this.props.route.params.data !== undefined) {
                this.setState({
                    notificationData: this.props.route.params.data
                })
            }
        }
    }

   componentWillMount()
   {
       console.warn("unmount called")
    //    checkVar=true
   }

    _renderHeader() {
        const {navigation} = this.props;
        return (
            <Header
                backgroundColor={COLORS.white_color}
                barStyle={"dark-content"}
                statusBarProps={{
                    translucent: true,
                    backgroundColor: COLORS.transparent,
                }}
                centerComponent={{
                    text: STRINGS.booking_header_title, style: {
                        color: COLORS.greyButton_color,
                        fontSize: FONT.TextMedium_2,
                        fontFamily: FONT_FAMILY.PoppinsBold
                    }
                }}
                containerStyle={{
                    borderBottomColor: COLORS.greyButton_color,
                    paddingTop: Platform.OS === 'ios' ? 0 : 25,
                    height: Platform.OS === 'ios' ? 60 : StatusBar.currentHeight + 60,
                }}
            />
        )
    }


    render() {
        return (
            <SafeAreaViewContainer>
                {this._renderHeader()}
                <View style={{backgroundColor: COLORS.backGround_color, flex: 1}}>
                    <CustomerBookingTopTab data={this.state.notificationData}  navigationInfo={this.props}/>
                </View>
            </SafeAreaViewContainer>
        )
    }
}
