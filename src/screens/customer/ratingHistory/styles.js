import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { StyleSheet, Dimensions } from "react-native";
import { FONT } from "../../../utils/FontSizes";
import COLORS from "../../../utils/Colors";
import { FONT_FAMILY } from "../../../utils/Font";

const window = Dimensions.get("window");

const styles = StyleSheet.create({
  ratingOuterView: {
    width: wp("100%"),
    flex: 1,
    backgroundColor: "white",
    paddingTop: wp("2.5%"),
  },
searchTextInputStyle:{
  borderColor: 'gray', marginLeft: wp(2), width: wp(70), paddingVertical: wp(2.5)

},
shadowContainerStyle:{
  marginBottom: 0, borderRadius: wp(20),
},
  hotelItemOuterView: {
    marginTop: wp(1),
    flexDirection: "row",
    // marginRight: wp("2.5%"),
    // marginLeft: wp("2.5%"),

    alignSelf: "center",
    borderRadius: wp(2),
    // borderWidth: 1,
    // borderColor: "gray",
    width: wp("90%"),
    // height: wp("30%"),
    padding: wp("2%"),
    backgroundColor: COLORS.white_color,
    // shadowColor: "#000",
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },
    //
    // shadowOpacity: 0.5,
    // shadowRadius: 3.84,
    //
    // elevation: 5,
  },

  hotelNameText: {
    fontSize: FONT.TextSmall,
    fontFamily: FONT_FAMILY.PoppinsBold,
    // fontWeight: "700",
    // marginLeft: wp("3%"),
    // textAlign: 'center',
    width: wp("35%"),
  },

  hotelImage: {
    height: wp("30%"),
    width: wp("25%"),
    // borderBottomLeftRadius: 10,
    // borderBottomStartRadius:10,
    // borderTopLeftRadius:10,
    // borderWidth: 1,
    // shadowColor: "#000",
    // shadowOffset: {
    //   width: 0,
    //   height: 2,
    // },

    // shadowOpacity: 0.5,
    // shadowRadius: 3.84,

    // elevation: 5,
  },
  datetext: {
    fontSize: FONT.TextSmall_2,
    fontFamily: FONT_FAMILY.Montserrat,
    // marginTop: wp("2%"),
    textAlign: "center",
    // alignSelf: 'flex-end',
  },

  searchMainView: {
    flexDirection: "row",
    width: wp("90%"),
    height: wp("14%"),
    alignContent: "center",
    alignItems: "center",
  },

  searchbarContainerStyle: {
    width: wp("75%"),
    height: wp("9%"),
    backgroundColor: "white",
    borderWidth: 1,
    borderRadius: wp("5%"),
    borderColor: "white",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    justifyContent: "center",
  },

  searchView: {
    width: wp("75%"),
    height: wp("10%"),
    backgroundColor: "white",
    borderWidth: 1,
    borderRadius: wp("5%"),
    borderColor: "white",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    alignSelf: "center",
  },

  viewHotelStyle: {
    marginVertical: 10,
    // alignSelf: "center",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: COLORS.app_theme_color,
    height: wp("6%"),
    width: wp("20%"),
    borderRadius: 5,
    borderColor: COLORS.app_theme_color,
    borderWidth: 2,
    // alignSelf:'flex-start',
    marginLeft: wp("3%"),
    marginTop: wp("3%"),
  },
  viewHotelText: {
    color: COLORS.white_color,
    fontWeight: "700",
    alignSelf: "center",
  },
  ratingView: {
    alignSelf: "flex-start",
    paddingHorizontal: wp(2),
    // marginTop:wp('3%')
  },
});
export default styles;
