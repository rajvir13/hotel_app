import React from "react";
import {
  View,
  Text,
  Image,
  Platform,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  FlatList,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  TextInput,
} from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Header } from "react-native-elements";
import AsyncStorage from "@react-native-community/async-storage";
import BaseClass from "../../../utils/BaseClass";
import {
  SafeAreaViewContainer,
  MainContainer,
  ScrollContainer,
  ShadowViewContainer,
} from "../../../utils/BaseStyle";
import { Spacer } from "../../../customComponents/Spacer";
import COLORS from "../../../utils/Colors";
import { LeftIcon } from "../../../customComponents/icons";
import STRINGS from "../../../utils/Strings";
import { FONT } from "../../../utils/FontSizes";
import { ICONS } from "../../../utils/ImagePaths";
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import styles from "./styles";
import { FONT_FAMILY } from "../../../utils/Font";
import { SearchHotel, HotelRatingItem } from "./component/ratingComponent";
import SearchIconSVG from "../../../../assets/images/CustomerHome/searchIcon.svg";
import { GetCustomerGivenRating } from "../../../redux/actions/GetCustomerRatingToHotel";
import RatingView from "./component/ratingViewModel";

//==================Dismiss keyboard  =======================
const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

//=================== main class declareation======================
export default class RatingHistory extends BaseClass {
  //===================== constructor================================

  constructor(props) {
    super(props);

    this.state = {
      accessToken: undefined,
      customerLoginId: undefined,
      ratingHotelData: [],
      searchText: "",
      isModelVisible: false,
      pendingRequestCount: undefined,
      itemValue: {},
    };
  }

  //====================== component life cycle method =========================

  componentDidMount() {
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        this.setState({
          accessToken: loginData.access_token,
          customerLoginId: loginData.id,
        });
        this.getCommentsAndRating(loginData.id, loginData.access_token);
      }
    });
  }
  getCommentsAndRating = (loginId, accessToken) => {
    this.showDialog();

    GetCustomerGivenRating(
      {
        accessToken: accessToken,
        customerId: loginId,
      },
      (data) => this.getRatingResponse(data)
    );
  };

  getRatingResponse(data) {
    this.hideDialog();
    if (data === "Network request failed" || data == "TypeError: Network request failed") {
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      const { response, message, status } = data;
      if (response !== undefined) {
        if (status === 200) {
          this.hideDialog();
          this.setState({
            ratingHotelData: response,
            pendingRequestCount: response.length,
          });
        }
      } else if (status === 111) {
        this.hideDialog();
        if (message.non_field_errors !== undefined) {
          this.showToastAlert("something went wrong");
        }
      } else if (status === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      }
    }
    this.hideDialog();
  }
  //========================= customer loder
  _renderCustomLoader = () => {
    const { isLoading } = this.state;

    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  //========================= customer loder ==========================
  _renderCustomLoader = () => {
    const { isLoading } = this.state;

    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  EmptyListMessage = () => {
    return (
      <View>
        <Text
          style={{
            textAlign: "center",
            fontSize: FONT.TextNormal,
            fontFamily: FONT_FAMILY.MontserratBold,
          }}
        >
          NO DATA{" "}
        </Text>
      </View>
    );
  };


  //============================ render header ===============================
  _renderHeader() {
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        // leftComponent={<LeftIcon onPress={() => navigation.goBack()}/>}
        centerComponent={{
          text: STRINGS.RATING_HEADER,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  _renderSearch = () => {
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <ShadowViewContainer style={styles.shadowContainerStyle}>
          <TextInput
            style={styles.searchTextInputStyle}
            // editable={false}
            onChangeText={(text) => this.setState({ searchText: text })}
            placeholder={"My location"}
            // value={value}
          />
        </ShadowViewContainer>
        <Spacer row={1} />
        <TouchableOpacity
          onPress={() => {
            // this.showToastAlert(STRINGS.COMING_SOON);
            // console.warn("Search")
          }}
        >
          <SearchIconSVG />
        </TouchableOpacity>
      </View>
    );
  };

  filterList = (ratingHotelData) => {
    return ratingHotelData.filter((listItem) =>
      listItem.hotel_name
        .toLowerCase()
        .includes(this.state.searchText.toLowerCase())
    );
  };

  //===================render method ======================
  render() {
    const {
      ratingHotelData,
      itemValue,
      pendingRequestCount,
      isModelVisible,
    } = this.state;
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <DismissKeyboard>
          <ScrollContainer>
            <MainContainer>
              <Spacer space={2} />
              {/*<SearchHotel/>*/}
              {this._renderSearch()}
              <Spacer space={1} />

              {pendingRequestCount !== 0 ? (
                <View style={styles.ratingOuterView}>
                  <FlatList
                    style={{ width: wp("100%"), paddingBottom: wp(17) }}
                    data={this.filterList(ratingHotelData)}
                    ListEmptyComponent={() => this.EmptyListMessage()}
                    renderItem={({ item }) => (
                      <HotelRatingItem
                        hotelItem={item}
                        onViewPress={() =>
                          this.setState({
                            isModelVisible: true,
                            itemValue: item,
                          })
                        }
                        onhotelImagePress={(img)=>
                          this.props.navigation.navigate("ShowFullViewImg", {
                            imgUrl:img
                            
                        })
                        }
                      />
                    )}
                  />
                </View>
              ) : (
                <View>
                  <Text
                    style={{
                      textAlign: "center",
                      fontSize: FONT.TextNormal,
                      fontFamily: FONT_FAMILY.MontserratBold,
                    }}
                  >
                    NO DATA{" "}
                  </Text>
                </View>
              )}
              {isModelVisible === true && (
                <RatingView
                  isRatingViewVisible={isModelVisible}
                  value={itemValue}
                  closeModal={() =>
                    this.setState({ isModelVisible: !isModelVisible })
                  }
                />
              )}
              {this._renderCustomLoader()}
            </MainContainer>
          </ScrollContainer>
        </DismissKeyboard>
      </SafeAreaViewContainer>
    );
  }
}
