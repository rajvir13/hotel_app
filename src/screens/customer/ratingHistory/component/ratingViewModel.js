import React, { Component } from "react";
import {
  Modal,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLORS from "../../../../utils/Colors";

import { FONT } from "../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../utils/Font";
import FastImage from "react-native-fast-image";
import { Spacer } from "../../../../customComponents/Spacer";

import STRINGS from "../../../../utils/Strings";
import { ICONS } from "../../../../utils/ImagePaths";
import {
  SafeAreaViewContainer,
  ShadowViewContainer,
} from "../../../../utils/BaseStyle";
import BaseClass from "../../../../utils/BaseClass";
import { RoomView } from "../../../../customComponents/roomView/index";
import CrossComponent from "../../../../../assets/images/BookingIcons/CrossSVG";
// import { API } from "../../../../../redux/constant";
import { API } from "../../../../redux/constant";
import { Rating, AirbnbRating } from "react-native-elements";

export default class RatingView extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      modelData: props.value,
      commentText: props.value.comment,
    };
  }

  closeModal = () => {
    this.props.closeModal();
  };

  render() {
    const { isDatePickerVisible, modelData } = this.state;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.isRatingViewVisible}
      >
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          behavior={Platform.OS === "ios" ? "padding" : null}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              Keyboard.dismiss();
            }}
          >
            <View style={styles.centeredView}>
              <View
                style={{
                  height: wp("70%"),
                  width: wp("90%"),
                  backgroundColor: "white",
                  borderRadius: wp("2%"),
                }}
              >
                <TouchableOpacity
                  hitSlop={{ top: wp(5), left: wp(5), bottom: wp(5) }}
                  style={{
                    position: "absolute",
                    alignSelf: "flex-end",
                    marginTop: wp(-3),
                  }}
                  onPress={() => {
                    this.closeModal();
                  }}
                >
                  <CrossComponent width={wp(8)} height={wp(8)} />
                </TouchableOpacity>
                <Rating
                  style={{ marginTop: wp("5%") }}
                  type="custom"
                  imageSize={50}
                  ratingColor={COLORS.detail_color}
                  readonly
                  startingValue={modelData.rating}
                  ratingCount={5}
                />
                <View style={styles.textAreaContainer}>
                  <Text multiline={true} style={styles.textArea}>
                    {" "}
                    {modelData.comment}
                  </Text>
                </View>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.6)",
  },

  textAreaContainer: {
    borderColor: COLORS.grey20,
    borderWidth: 1,
    padding: 5,
    borderRadius: wp("2%"),
    marginTop: wp("5%"),
    marginLeft: wp("2%"),
    marginRight: wp("2%"),
  },
  textArea: {
    height: wp("15%"),
    justifyContent: "flex-start",
  },

  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});
