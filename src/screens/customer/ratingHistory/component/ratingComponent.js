import React from "react";
import {
  View,
  FlatList,
  Text,
  Image,
  Platform,
  StatusBar,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { ICONS } from "../../../../utils/ImagePaths";
import styles from "../styles";
import { SearchBar } from "react-native-elements";
import { Dropdown } from "react-native-material-dropdown";
import SearchIconSVG from "../../../../../assets/images/CustomerHome/searchIcon.svg";
import {
  SafeAreaViewContainer,
  MainContainer,
  ShadowViewContainer,
} from "../../../../utils/BaseStyle";
import { Rating, AirbnbRating } from "react-native-elements";
import COLORS from "../../../../utils/Colors";
import { Spacer } from "../../../../customComponents/Spacer";
import FastImage from "react-native-fast-image";
import { API } from "../../../../redux/constant";

//================== room type and sorting view ===================

//================== search hotel view ===================

const SearchHotel = () => {
  return (
    <View style={styles.searchMainView}>
      <SearchBar
        containerStyle={styles.searchbarContainerStyle}
        inputContainerStyle={styles.searchView}
        searchIcon={null}
        placeholder="Search by hotel Name"
      />
      <View style={{ marginLeft: wp("2%") }}>
        <SearchIconSVG />
      </View>
    </View>
  );
};

const HotelRatingItem = (props) => {
  return (
    <ShadowViewContainer>
      <View style={styles.hotelItemOuterView}>
        <TouchableOpacity
          onPress={() =>
            props.onhotelImagePress(
              props.hotelItem.hotel_image !== null &&
                props.hotelItem.hotel_image !== undefined
                ? API.IMAGE_BASE_URL + props.hotelItem.hotel_image.image
                : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg"
            )
          }
        >
          <FastImage
            style={styles.hotelImage}
            source={{
              uri:
                props.hotelItem.hotel_image !== null &&
                props.hotelItem.hotel_image !== undefined
                  ? API.IMAGE_BASE_URL + props.hotelItem.hotel_image.image
                  : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
            }}
            resizeMode={FastImage.resizeMode.stretch}
          />
        </TouchableOpacity>

        <View>
          <View
            style={{
              flexDirection: "row",
              paddingHorizontal: wp(2),
              marginTop: wp("1.5%"),
              alignItems: "center",
            }}
          >
            <Text style={styles.hotelNameText}>
              {props.hotelItem.hotel_name}
            </Text>
            <Spacer row={1} />
            <Text style={styles.datetext}>
              {props.hotelItem.comment_created}
            </Text>
          </View>

          <View style={styles.ratingView}>
            <Rating
              type="custom"
              imageSize={25}
              ratingColor={COLORS.detail_color}
              // ratingBackgroundColor={COLORS.rating_inactive_color}
              readonly
              startingValue={props.hotelItem.rating}
              ratingCount={5}
            />
          </View>
          <TouchableOpacity onPress={() => props.onViewPress()}>
            <View style={styles.viewHotelStyle}>
              <Text style={styles.viewHotelText}>View</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </ShadowViewContainer>
  );
};

export { SearchHotel, HotelRatingItem };
