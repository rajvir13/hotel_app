import React from "react";
import {
    View,
    Text,
    Image,
    Platform,
    StatusBar,
    Dimensions,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Keyboard,
    Alert,
} from "react-native";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {Header} from "react-native-elements";
import AsyncStorage from "@react-native-community/async-storage";
import BaseClass from "../../../utils/BaseClass";
import {
    SafeAreaViewContainer,
    MainContainer,
    ScrollContainer,
} from "../../../utils/BaseStyle";
import {Spacer} from "../../../customComponents/Spacer";
import COLORS from "../../../utils/Colors";
import {ListingIcon, RightIcon} from "../../../customComponents/icons";
import STRINGS from "../../../utils/Strings";
import {FONT} from "../../../utils/FontSizes";
import {ICONS} from "../../../utils/ImagePaths";
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import styles from "./styles";
import {RoomTypeAndSorting, SearchHotel} from "./components/mapSearch";
import MapView, {PROVIDER_GOOGLE} from "react-native-maps";
import Geolocation from "@react-native-community/geolocation";
import MapViewDirections from "react-native-maps-directions";
import {NearByAction} from "../../../redux/actions/CustomerHomeNearByAction";

//==================Dismiss keyboard  =======================
const DismissKeyboard = ({children}) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);

const LATITUDE = 20.5937;
const LONGITUDE = 78.9629;
const LATITUDE_DELTA = 0.0122;
const LONGITUDE_DELTA =
    (Dimensions.get("window").width / Dimensions.get("window").height) * 0.0122;

const mode = "driving"; // 'walking';
const origin = "coords or address";
const destination = "coords or address";
const APIKEY = "AIzaSyBPiS_IauzMt843y5rF7FQ33aqcbPKDIGE";
const url =
    "https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${APIKEY}&mode=${mode}";

//=================== main class declareation======================
export default class HotelMapView extends BaseClass {
    //===================== constructor================================

    constructor() {
        super();
        this.state = {

            // region: {
            //     latitude: LATITUDE,
            //     longitude: LONGITUDE,
            //     latitudeDelta: LATITUDE_DELTA,
            //     longitudeDelta: LONGITUDE_DELTA,
            //     distance: "2 miles away",
            // },
            isDestinationSelected: false,
           
            desinationHotel: {
                latitude: undefined,
                longitude: undefined,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005,

            },

            sourcePoint: {
                latitude: undefined,
                longitude: undefined,
                latitudeDelta: 0.005,
                longitudeDelta: 0.005,
            },
            hotelData: [],
        };
    }

    //====================== component life cycle method =========================

    componentDidMount() {
        //api call

        Geolocation.getCurrentPosition(
          (position) => {
              if(position !== undefined)
              
            this.setState({
                sourcePoint: {
                latitude: position.coords.latitude,
                longitude: position.coords.longitude,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA,
              },
            });
            
          },
          (error) => console.log(error.message),
          { enableHighAccuracy: false }
        );

        this.watchID = Geolocation.watchPosition((position) => {
          this.setState({
            sourcePoint: {
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              latitudeDelta: LATITUDE_DELTA,
              longitudeDelta: LONGITUDE_DELTA,
            },
          });
        });

        this._getNearByHotelData();
    }

    //=========== api call get near by hotels data===========
    _getNearByHotelData = () => {
        AsyncStorage.getItem(STRINGS.loginToken, (error, result) => {
            if (result !== undefined && result !== null) {
                let accessToken = JSON.parse(result);
                if (accessToken !== undefined) {
                    AsyncStorage.getItem(STRINGS.latlong, (error, result) => {
                        if (result !== undefined && result !== null) {
                            let latlong = JSON.parse(result);
                            if (latlong !== undefined) {
                                this.setState({
                                    accessToken: accessToken,
                                    lat: latlong.latitude,
                                    long: latlong.longitude,
                                });
                                this.showDialog();
                                NearByAction(
                                    {
                                        accessToken: accessToken,
                                        lat: latlong.latitude,
                                        long: latlong.longitude,
                                    },
                                    (data) => this.nearHotelsResponse(data)
                                );
                            }
                        }
                    });
                }
            }
        });
    };


    nearHotelsResponse = (data) => {
        const {hotelData} = this.state;
        this.hideDialog();
        data.response.map((item, index) => {
            let hotelApiData = {
                id: item.id,
                coordinates: {
                    latitude: item.latitude,
                    longitude: item.longitude,
                },
                title: item.name,
                longitudeDelta: 50,
                latitudeDelta: 50,
                distance: item.distance,
            };

            hotelData.push(hotelApiData);
        });
        this.setState({hotelData});
    };

    //================== end====================

    componentWillUnmount() {
      Geolocation.clearWatch(this.watchID);
    }

    //========================= customer loder
    _renderCustomLoader = () => {
      const { isLoading } = this.state;

      return (
        <OrientationLoadingOverlay
          visible={isLoading}
          message={STRINGS.LOADING_TEXT}
        />
      );
    };

    // ============================ methods ===============================

    logoutUser = () => {
      const { isLogOut, accessToken } = this.state;
      Alert.alert(
        "Hotel App",
        STRINGS.EXIT_ALERT,
        [
          {
            text: "Cancel",
            style: "cancel",
          },
          {
            text: "OK",
            onPress: () => {
              this.setState({ isLogOut: !isLogOut });
              // this.props.logout({accessToken});
              AsyncStorage.removeItem(STRINGS.loginToken);
              AsyncStorage.removeItem(STRINGS.loginData);
              AsyncStorage.removeItem(STRINGS.loginCredentials);
              // this.showDialog()
            },
          },
        ],
        { cancelable: false }
      );
    };

    // ============================ render header ===============================

    _renderHeader() {
      const { navigation } = this.props;
      return (
        <Header
          backgroundColor={COLORS.white_color}
          barStyle={"dark-content"}
          statusBarProps={{
            translucent: true,
            backgroundColor: COLORS.transparent,
          }}
          leftComponent={
            <View style={{ flexDirection: "row" }}>
              {/* <TouchableWithoutFeedback
                onPress={() => {
                  this.showToastAlert(STRINGS.COMING_SOON);
                }}
              > */}
                <Image
                  style={styles.locationIconStyles}
                  resizeMode={"contain"}
                  source={ICONS.MAPVIEWUSER_ICON}
                />
              {/* </TouchableWithoutFeedback> */}
            </View>
          }
          rightComponent={
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <ListingIcon
                onPress={() => this.props.navigation.navigate("customerHome")}
              />
              <Spacer row={3} />
              <RightIcon onPress={() => this.logoutUser()} />
            </View>
          }
          centerComponent={{
            text: STRINGS.home_header_title,
            style: {
              color: COLORS.greyButton_color,
              fontSize: FONT.TextMedium,
              fontWeight: "bold",
            },
          }}
          containerStyle={{
            borderBottomColor: COLORS.greyButton_color,
            paddingTop: Platform.OS === "ios" ? 0 : 25,
            height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
          }}
        />
      );
    }

    //================================= hotel details click===========================
    _renderDetail = (id) => {
        console.warn("details click", id);
    };

    //================================== hotel direction click=========================
    _renderDirection = (id) => {
        console.warn("directin clicked", id);
        const {hotelData} = this.state;
       

        hotelData.map((item, index) => {
            if (item.id === id) {

                const destinationLoc = item.coordinates;
                this.setState({
                    desinationHotel: {
                        latitude: item.coordinates.latitude,
                        longitude: item.coordinates.longitude,
                    },
                    isDestinationSelected: true,
                });
            }
        });
    };

    //========================== render google map and show user current locaiton and hotels =============================

    _renderHotelOnMap = () => {
        const {
            sourcePoint,
            hotelData,
            isDestinationSelected,
            desinationHotel,
        } = this.state;
        const mapDirectionView = (
            <MapViewDirections
                origin={sourcePoint}
                destination={desinationHotel}
                apikey={APIKEY}
            />
        );

        return (
            <MapView
                provider={PROVIDER_GOOGLE}
                style={styles.container}
                showsUserLocation={true}
                zoomEnabled={true}
                showsMyLocationButton={true}
                followsUserLocation={true}
                showsCompass={true}
                toolbarEnabled={true}
                rotateEnabled={true}
                initialRegion={this.state.sourcePoint}

                // onRegionChange={(region) => this.setState({ region })}
                // onRegionChangeComplete={(region) => this.setState({ region })}
            >
                {hotelData.map((marker) => (
                    <MapView.Marker
                        key={marker.id}
                        coordinate={marker.coordinates}
                        style={{zIndex: 5}}
                        title={marker.title}
                    >
                        <Image
                            style={{height: wp("14%"), width: wp("8%")}}
                            source={ICONS.HOTEL_APP_ICON}
                            onLayout={() => this.setState({initialRender: false})}
                        />
                        <View style={styles.distanceFromUserView}>
                            <Text style={styles.distanceFromUserText}>{marker.distance}</Text>
                            <Text style={styles.distanceFromUserText}> km away</Text>
                        </View>

                        <MapView.Callout tooltip style={styles.directionDetailView}>
                            <MapView.CalloutSubview
                                onPress={() => this._renderDirection(marker.id)}
                            >
                                <TouchableOpacity>
                                    <Text style={styles.directionDetailTextStyle}>Direction</Text>
                                </TouchableOpacity>
                            </MapView.CalloutSubview>

                            <MapView.CalloutSubview
                                onPress={() => this._renderDetail(marker.id)}
                            >
                                <View style={styles.lineView}/>
                                <TouchableOpacity>
                                    <Text style={styles.directionDetailTextStyle}> Detail </Text>
                                </TouchableOpacity>
                            </MapView.CalloutSubview>
                        </MapView.Callout>
                    </MapView.Marker>
                ))}

                <MapView.Marker coordinate={this.state.sourcePoint} title={"your location"}>
                    <Image
                        style={{height: wp("12%"), width: wp("8%")}}
                        source={ICONS.USER_CURRENT_LOC_ICON}
                        onLayout={() => this.setState({initialRender: false})}
                    />
                </MapView.Marker>

                {isDestinationSelected !== undefined ? mapDirectionView : undefined}
            </MapView>
        );
    };

    _selectedRoomType = (id, index) => {
        console.warn("id", id);
    };

    _selectedShortingType = (id, index) => {
        console.warn("id", id);
    };

    render() {
        const {hotelDetail} = this.state;
        let tempRoomType = [
            {
                value: "Single",
            },
            {
                value: "Double",
            },
            {
                value: "Triple",
            },
        ];

        let sortingType = [
            {
                value: "NearBy",
            },
            {
                value: "Price",
            },
            {
                value: "Rating",
            },
        ];
        return (
            <SafeAreaViewContainer>
                {this._renderHeader()}
                <DismissKeyboard>
                    <ScrollContainer>
                        <MainContainer>
                            <Spacer space={2}/>
                            <SearchHotel/>
                            <Spacer space={1}/>
                            <View style={styles.roomTypeAndSortingMainView}>
                                <RoomTypeAndSorting
                                    lable={"Room Type"}
                                    data={tempRoomType}
                                    selectedRoomType={(item, index) =>
                                        this._selectedRoomType(item, index)
                                    }
                                />

                                <RoomTypeAndSorting
                                    
                                    lable={"Sorting"}
                                    data={sortingType}
                                    selectedRoomType={(item, index) =>
                                        this._selectedShortingType(item, index)
                                    }
                                />
                            </View>

                            <Spacer space={1}/>
                            {this._renderHotelOnMap()}
                            {this._renderCustomLoader()}
                        </MainContainer>
                    </ScrollContainer>
                </DismissKeyboard>
            </SafeAreaViewContainer>
        );
    }
}
