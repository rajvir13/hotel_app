import React from "react";
import {
  View,
  FlatList,
  Text,
  Image,
  Platform,
  StatusBar,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import MapView, { PROVIDER_GOOGLE } from "react-native-maps";
import { ICONS } from "../../../../utils/ImagePaths";
import styles from "../styles";
import { SearchBar } from "react-native-elements";
import { Dropdown } from "react-native-material-dropdown";
import SearchIconSVG from "../../../../../assets/images/CustomerHome/searchIcon.svg";

//================== room type and sorting view ===================
const RoomTypeAndSorting = ({isSortingClicked, lable, data, selectedRoomType}) => {
    return (
        <View style={{
            width: wp('43%'), height: wp('12%'), borderWidth: 2,
            borderRadius: wp("2%"),
            borderColor: "#ddd",
            paddingLeft: wp('2.5%'),
            paddingRight: wp('2.5%'),
            justifyContent:'center'
        }}>

            <Dropdown
                // containerStyle={{ width: wp("80%"), }}
                label={!isSortingClicked && lable}
                data={data}
                labelTextStyle={{fontSize: wp('4%'), margin: 0}}
                onChangeText={(value, id) =>
                    selectedRoomType(value, id)
                }
            />


        </View>
    );
};
//================== search hotel view ===================

const SearchHotel = () => {
  return (
    <View style={styles.searchMainView}>
      <SearchBar
        containerStyle={styles.searchbarContainerStyle}
        inputContainerStyle={styles.searchView}
        searchIcon={null}
        placeholder="All Location"
      />
      <SearchIconSVG />
    </View>
  );
};

const MyCustomCalloutView = () => {
  return (
    <View>
      <TouchableWithoutFeedback onPress={() => console.warn("direction click")}>
        <Text style={styles.directionDetailTextStyle}>Direction</Text>
      </TouchableWithoutFeedback>
      <View style={styles.lineView} />

      <TouchableWithoutFeedback onPress={() => console.warn("details click")}>
        <Text style={styles.directionDetailTextStyle}> Detail </Text>
      </TouchableWithoutFeedback>
    </View>
  );
};

export { SearchHotel, RoomTypeAndSorting, MyCustomCalloutView };
