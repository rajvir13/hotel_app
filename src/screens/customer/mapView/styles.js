import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { StyleSheet, Dimensions } from "react-native";
import { FONT } from "../../../utils/FontSizes";
import COLORS from "../../../utils/Colors";

const window = Dimensions.get("window");

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
  },
  container: {
    height: hp('70%'),
    width: wp('100%'),
  },


  locationIconStyles: {
    width: wp("10%"),
    height: wp("10%"),
    borderRadius: wp("5%"),
    marginLeft: wp("2%"),
    alignSelf: "center",
  },

  searchIconStyle: {
    width: wp("12%"),
    height: wp("12%"),
    borderRadius: wp("6%"),
    marginLeft: wp("5%"),
  },

  searchView: {
    width: wp("75%"),
    height: wp("10%"),
    backgroundColor: "white",
    borderWidth: 1,
    borderRadius: wp('5%'),
    borderColor: 'white',
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    alignSelf:'center'
  },

 searchbarContainerStyle:{
  width: wp("75%"),
    height: wp("9%"),
    backgroundColor: "white",
    borderWidth: 1,
    borderRadius: wp('5%'),
    borderColor: 'white',
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    justifyContent:'center'

 },

  searchMainView: {
    flexDirection: "row",
    width: wp("90%"),
    height: wp("12%"),
    alignContent: "center",
    alignItems: "center",
  },

  searchTextStyle:{

  justifyContent:'center',
  marginLeft:wp('2%'),
  paddingVertical:wp('0.5%')

  },

  roomTypeAndSortingMainView:{

    flexDirection: "row",
    width: wp("100%"),
    height: wp("20%"),
    alignContent: "center",
    alignItems: "center",
    justifyContent:'space-around',
    backgroundColor:'white',


  },
  roomTypeView:{

    alignSelf: "center",
    backgroundColor: COLORS.white_color,
    borderWidth: 2,
    borderRadius: wp("2%"),
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: "center",
    height: wp("10%"),
    width:wp('42%'),
    paddingLeft: wp("2%"),
    paddingRight: wp("2%"),

  },

  roomTypeText:{
   marginLeft:wp('2%'),
   paddingVertical:wp('1%')
  },
  dropDownImageStyle:{
  marginLeft:wp('8%'),
  alignSelf:'center',
  height:wp('3%'),
  resizeMode:'contain'

  },

  roomTypeInnerView:{

    flexDirection:'row',justifyContent:'space-around'
  },

  directionDetailView:{
    width: wp("30%"),
    height: wp("15%"),
    backgroundColor: COLORS.app_theme_color,
    borderWidth: 1,
    borderRadius: wp('2%'),
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    justifyContent:'center'

  },

  lineView:{

    height:wp('0.5%'),
    width: wp("30%"),
    backgroundColor:'white',

  },
  directionDetailTextStyle:{
    color:"white",
    fontWeight:'700',
    justifyContent:'center',
    alignSelf:"center",
    paddingVertical:wp('1%')

  },
  distanceFromUserText:{
    color:"white",
    fontWeight:'600',
    justifyContent:'center',
    alignSelf:"center",
    paddingVertical:wp('0.5%')

  },
  distanceFromUserView:{

    width: wp("30%"),
    height: wp("6%"),
    backgroundColor: COLORS.app_theme_color,
    borderWidth: 1,
    borderRadius: wp('2%'),
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    flexDirection:'row',
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    justifyContent:'center'


  }

});
export default styles;
