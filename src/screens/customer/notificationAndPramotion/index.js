// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react";
import {
  View,
  FlatList,
  Text,
  Image,
  Platform,
  StatusBar,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  ScrollView,
  Alert,
  Dimensions,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Header } from "react-native-elements";
import AsyncStorage from "@react-native-community/async-storage";
import { CommonActions } from "@react-navigation/native";
import * as _ from "lodash";

import BaseClass from "../../../utils/BaseClass";
import {
  ShadowViewContainer,
  SafeAreaViewContainer,
  MainContainer,
  ScrollContainer,
} from "../../../utils/BaseStyle";
import { Spacer } from "../../../customComponents/Spacer";
import COLORS from "../../../utils/Colors";
import {
  ListingIcon,
  LocationIcon,
  RightIcon,
  DeleteIcon,
} from "../../../customComponents/icons";
import STRINGS from "../../../utils/Strings";
import { FONT } from "../../../utils/FontSizes";
import SearchIconSVG from "../../../../assets/images/CustomerHome/searchIcon.svg";
import { FONT_FAMILY } from "../../../utils/Font";
import styles from "./styles";
import { LeftIcon } from "@customComponents/icons";
import { ICONS } from "../../../utils/ImagePaths";

import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";
import { GetPramotionOffer } from "../../../redux/actions/PramotionOffer";
// import { CustomeMonthYearPicker } from "../../../customComponents/CustomeMonthYearPicker";
import { CustomeMonthYearPicker } from "../../../customComponents/CustomeMonthYearPicker";

import {
  CalenderView,
  RenderUserNotification,
} from "./component/calnderComponent";
const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

class CustomerNotificationAndPramotion extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      searchText: "",
      selectdMonthText: "Select Month",
      isMonthYearPickerVisible: false,
      notificationData: [],
      pendingRequestCount: undefined,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        this.setState({
          accessToken: loginData.access_token,
        });
        this.getPramotion(2, loginData.access_token, "", "");
      }
    });

    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.onFocusFunction();
    });
  }

  onFocusFunction = () => {
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        this.setState({
          accessToken: loginData.access_token,
          selectdMonthText: "Select Month",
          searchText: "",
        });
        this.getPramotion(2, loginData.access_token, "", "");
      }
    });
  };

  getPramotion = (idForCustomer, accessToken, month, year) => {
    this.showDialog();

    GetPramotionOffer(
      {
        accessToken: accessToken,
        idForCustomer: idForCustomer,
        month: month,
        year: year,
      },
      (data) => this.getPramotionResponse(data)
    );
  };

  getPramotionResponse(data) {
    this.hideDialog();
    if (data === "Network request failed" || data == "TypeError: Network request failed") {
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      const { response, message, status } = data;

      if (response !== undefined) {
        if (status === 200) {
          this.hideDialog();
          this.setState({
            notificationData: response,
            pendingRequestCount: response.length,
          });
        }
      } else if (status === 111) {
        this.hideDialog();
        if (message.non_field_errors !== undefined) {
          this.showToastAlert("something went wrong");
        }
      } else if (status === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      }
    }
    this.hideDialog();
  }
  _renderHeader() {
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={<LeftIcon onPress={() => navigation.goBack()} />}
       
        centerComponent={{
          text: STRINGS.notification_pramotion,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium,
            fontFamily: FONT_FAMILY.PoppinsBold,
            width:wp('80%'),
            alignSelf:'center',
            justifyContent:'center',
            textAlign:'center'
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  deleteNotificaton = () => {};

  //=====================search ==================
  _renderSearch = () => {
    return (
      <View style={styles.myViewStyle}>
        <ShadowViewContainer style={{ marginBottom: 0, borderRadius: wp(20) }}>
          <TextInput
            keyboardType="ascii-capable"
            textInputProps={{
              keyboardType:
                Platform.OS === "android" ? "ascii-capable" : "ascii-capable",
            }}
            style={styles.locTextStyle}
            // editable={false}
            onChangeText={(text) => this.setState({ searchText: text })}
            // value={this.state.searchText}
            placeholder={"Search"}
            // value={value}
          />
        </ShadowViewContainer>
        <Spacer row={1} />
        {/* <TouchableOpacity
          onPress={() => {
            this.showToastAlert(STRINGS.COMING_SOON);
            // console.warn("Search")
          }}
        > */}
          <SearchIconSVG />
        {/* </TouchableOpacity> */}
      </View>
    );
  };
  //=====================search ==================

  monthView = () => {
    const { selectdMonthText } = this.state;
    return (
      <View style={styles.calnderToFromView}>
        <TouchableOpacity onPress={() => this.monthYearViewClick()}>
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-evenly",
            }}
          >
            <Text style={{ color: COLORS.greyButton_color2 }}>
              {selectdMonthText}
            </Text>
            <Image
              style={{
                height: wp("4%"),
                width: wp("4%"),
                marginLeft: wp("3%"),
                alignSelf: "center",
              }}
              source={ICONS.DOWN_ARROW}
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  monthYearViewClick = () => {
    this.setState({ isMonthYearPickerVisible: true });
  };

  newMonthYearSelected = (monthYear) => {
    const { accessToken, isMonthYearPickerVisible } = this.state;
    var monthYearData = moment(monthYear).format("MM-YYYY");

    var selectedmonth = monthYearData.toString().slice(0, 2);
    var selectedyear = monthYearData.toString().slice(3, 7);

    this.setState({
      isMonthYearPickerVisible: !isMonthYearPickerVisible,
      selectdMonthText: selectedmonth + "/" + selectedyear,
    });

    this.getPramotion(2, accessToken, selectedmonth, selectedyear);
  };

  closeMonthYearPicker = () => {
    this.setState({ isMonthYearPickerVisible: false });
  };

  filterList = (notificationData) => {
    return notificationData.filter(
      (listItem) =>
        listItem.name
          .toLowerCase()
          .includes(this.state.searchText.toLowerCase()) ||
        listItem.description
          .toLowerCase()
          .includes(this.state.searchText.toLowerCase())
    );
  };
  render() {
    const {
      isDatePickerVisible,
      notificationData,
      isMonthYearPickerVisible,
      pendingRequestCount,
      toDate,
      fromDate, 
    } = this.state;
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <DismissKeyboard>
          <ScrollView>
            <View
              style={{
                flex: 1,
                backgroundColor: "white",
                alignItems: "center",
              }}
            >
              <View
                style={{
                  backgroundColor: COLORS.backGround_color,
                  width: wp("100%"),
                  height: wp("35%"),
                }}
              >
                <Spacer space={2} />
                {this._renderSearch()}
                <Spacer space={1.5} />
                {this.monthView()}
                {isMonthYearPickerVisible === true && (
                  <CustomeMonthYearPicker
                    isMonthYearPickerVisible={isMonthYearPickerVisible}
                    newDateMonthSelected={(date) =>
                      this.newMonthYearSelected(date)
                    }
                    crossPickerAction={() => this.closeMonthYearPicker()}
                  />
                )}
              </View>

              <Spacer space={3} />

              {pendingRequestCount !== 0 ? (
                <View style={{ backgroundColor: "white", height: wp("100%") }}>
                  <FlatList
                    style={{ width: wp(100) }}
                    data={this.filterList(notificationData)}
                    keyExtractor={(item, index) => index}
                    renderItem={({ item, index }) => (
                      <RenderUserNotification item={item} />
                    )}
                  />
                </View>
              ) : (
                <View>
                  <Text
                    style={{
                      textAlign: "center",
                      fontSize: FONT.TextNormal,
                      fontFamily: FONT_FAMILY.MontserratBold,
                    }}
                  >
                    NO DATA{" "}
                  </Text>
                </View>
              )}
            </View>
          </ScrollView>
        </DismissKeyboard>
      </SafeAreaViewContainer>
    );
  }
}

export default CustomerNotificationAndPramotion;
