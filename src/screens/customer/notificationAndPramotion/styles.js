import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Platform, StyleSheet } from "react-native";
import { FONT } from "../../../utils/FontSizes";
import COLORS from "../../../utils/Colors";
import { FONT_FAMILY } from "../../../utils/Font";

const styles = StyleSheet.create({
  myViewStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  locTextStyle: {
    borderColor: "gray",
    marginLeft: wp(2),
    width: wp(70),
    paddingVertical: wp(2.5),
  },

  calnderToFromView: {
    flexDirection: "row",
    width: wp("40%"),
    alignSelf: "flex-start",
    justifyContent: "space-around",
    backgroundColor: "white",
    padding: wp("2%"),
    borderRadius: wp("5%"),
    marginLeft: wp("5%"),
  },

  calenderToFromTextInnerView: {
    justifyContent: "flex-start",
    width: wp("40%"),
    marginBottom: wp("-2%"),
  },
  toFromTextColor: {
    color: COLORS.greyButton_color2,
    fontFamily: FONT_FAMILY.Montserrat,
    fontStyle: "normal",
  },

  headerCalenderView: {
    height: wp("10%"),
    width: wp("90%"),
    flexDirection: "row",
    justifyContent: "space-evenly",
  },

  calendar_Style1: {
    flexDirection: "row",
    backgroundColor: "white",
    width: wp("40%"),
    justifyContent: "space-evenly",
    height: wp("10%"),
    marginTop: wp("4%"),
    borderRadius: wp("2%"),
    alignItems: "center",
  },
  calenderIcon: {
    height: wp("6%"),
    width: wp("6%"),
    marginLeft: wp("1%"),
  },
  dateTimeText: {
    marginLeft: wp("1.5%"),
    fontFamily: FONT_FAMILY.Montserrat,
    alignSelf: "center",
  },

  notificationView: {
    borderRadius: wp("2%"),
    backgroundColor: "white",
    marginTop: wp("2%"),
    padding: wp("3%"),
    marginLeft: wp("5%"),
    marginRight: wp("5%"),
    borderWidth: 1,
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: 1,
    borderColor: "gray",
    marginBottom: wp("1.5%"),
    marginTop: wp("2%"),
    
  },
});

export default styles;
