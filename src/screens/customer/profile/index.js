/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import React from "react";
import {
  View,
  Text,
  Image,
  Keyboard,
  StatusBar,TextInput,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Platform,
} from "react-native";
import { CommonActions } from "@react-navigation/native";
import styles from "./profileStyle";
import BaseClass from "../../../utils/BaseClass";
import STRINGS from "../../../utils/Strings";
import { Header } from "react-native-elements";
import { Spacer } from "../../../customComponents/Spacer";
import COLORS from "../../../utils/Colors";
import { LeftIcon } from "../../../customComponents/icons";
import { FONT } from "../../../utils/FontSizes";
import {
  SafeAreaViewContainer,
  MainContainer,
  ShadowViewContainer,
  ScrollContainer,
} from "../../../utils/BaseStyle";
import CustomTextInput from "../../../customComponents/CustomTextInput";
import { ICONS } from "../../../utils/ImagePaths";
import CountryModal from "../../../customComponents/Modals/CountryPicker";
import {
  GetProfileAction,
  UpdateProfileAction,
} from "../../../redux/actions/GetProfileAction";
import * as Validations from "../../../utils/Validations";
import {
  RegisterCountryAction,
  RegisterStateAction,
  RegisterCityAction,
} from "../../../redux/actions/registerAction";
// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import ImagePicker from "react-native-image-picker";
import { Input } from "react-native-elements";
import CountryPicker from "react-native-country-picker-modal";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import { FONT_FAMILY } from "../../../utils/Font";
import { API } from "../../../redux/constant";

const options = {
  title: "Select Image",
  //customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: "images",
  },
};
const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);
const userCountryData = "91";
const NORTH_AMERICA = [
  "AF",
  "AL",
  "DZ",
  "AS",
  "AD",
  "AO",
  "AI",
  "AG",
  "AR",
  "AM",
  "AW",
  "AU",
  "AT",
  "AZ",
  "BS",
  "BH",
  "BD",
  "BB",
  "BY",
  "BE",
  "BZ",
  "BJ",
  "BM",
  "BT",
  "BO",
  "BA",
  "BW",
  "BV",
  "BR",
  "IO",
  "VG",
  "BN",
  "BG",
  "BF",
  "BI",
  "KH",
  "CM",
  "CA",
  "CV",
  "KY",
  "CF",
  "TD",
  "CL",
  "CN",
  "CO",
  "KM",
  "CK",
  "CR",
  "HR",
  "CU",
  "CW",
  "CY",
  "CZ",
  "CD",
  "DK",
  "DJ",
  "DM",
  "DO",
  "EC",
  "EG",
  "SV",
  "GQ",
  "ER",
  "EE",
  "ET",
  "FK",
  "FO",
  "FJ",
  "FI",
  "FR",
  "GF",
  "PF",
  "TF",
  "GA",
  "GM",
  "GE",
  "DE",
  "GH",
  "GI",
  "GR",
  "GL",
  "GD",
  "GP",
  "GU",
  "GT",
  "GG",
  "GN",
  "GW",
  "GY",
  "HT",
  "HM",
  "HN",
  "HK",
  "HU",
  "IS",
  "IN",
  "ID",
  "IR",
  "IQ",
  "IE",
  "IM",
  "IL",
  "IT",
  "CI",
  "JM",
  "JP",
  "JE",
  "JO",
  "KZ",
  "KE",
  "KI",
  "XK",
  "KW",
  "KG",
  "LA",
  "LV",
  "LB",
  "LS",
  "LR",
  "LY",
  "LI",
  "LT",
  "LU",
  "MO",
  "MK",
  "MG",
  "MW",
  "MY",
  "MV",
  "ML",
  "MT",
  "MH",
  "MQ",
  "MR",
  "MU",
  "YT",
  "MX",
  "FM",
  "MD",
  "MC",
  "MN",
  "ME",
  "MS",
  "MA",
  "MZ",
  "MM",
  "NA",
  "NR",
  "NP",
  "NL",
  "NC",
  "NZ",
  "NI",
  "NE",
  "NG",
  "NU",
  "NF",
  "KP",
  "MP",
  "NO",
  "OM",
  "PK",
  "PW",
  "PS",
  "PA",
  "PG",
  "PY",
  "PE",
  "PH",
  "PN",
  "PL",
  "PT",
  "PR",
  "QA",
  "CG",
  "RO",
  "RU",
  "RW",
  "RE",
  "BL",
  "KN",
  "LC",
  "MF",
  "PM",
  "VC",
  "WS",
  "SM",
  "SA",
  "SN",
  "RS",
  "SC",
  "SL",
  "SG",
  "SX",
  "SK",
  "SI",
  "SB",
  "SO",
  "ZA",
  "GS",
  "KR",
  "SS",
  "ES",
  "LK",
  "SD",
  "SR",
  "SJ",
  "SZ",
  "SE",
  "CH",
  "SY",
  "ST",
  "TW",
  "TJ",
  "TZ",
  "TH",
  "TL",
  "TG",
  "TK",
  "TO",
  "TT",
  "TN",
  "TR",
  "TM",
  "TC",
  "TV",
  "UG",
  "UA",
  "AE",
  "GB",
  "US",
  "VI",
  "UY",
  "UZ",
  "VU",
  "VA",
  "VE",
  "VN",
  "WF",
  "EH",
  "YE",
  "ZM",
  "ZW",
  "AX",
];

class CustomerProfile extends BaseClass {
  constructor(props) {
    let userLocaleCountryCode = "IN";
    super(props);
    this.state = {
      profilePic: undefined,
      firstNameText: "",
      lastName: "",
      emailText: "",
      countryCode: "",
      phoneNoText: "",
      countryName: "",
      stateName: "",
      cityName: "",
      cca2: userLocaleCountryCode,
      callingCode: userCountryData,
      displayCountryModal: false,
      modal_type: "",
      accessToken: "",
      loginToken: "",
      userId: "",
      countryData: [],
      stateData: [],
      cityData: [],
      profileImage: "",
      isUpdateClicked: false,
      isChangedValue: false,
      loginData: {},
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.onFocusFunction();
    });
    this.asyncDataGettingMethod();
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let response = JSON.parse(result);
        if (response !== undefined) {
          this.setState({
            loginData: response,
          });
        }
      }
    });
  }

  asyncDataGettingMethod = () => {
    AsyncStorage.getItem(STRINGS.accessToken, (error, result) => {
      if (result !== undefined && result !== null) {
        let accessToken = JSON.parse(result);
        if (accessToken !== undefined) {
          this.setState({ accessToken: accessToken });
        }
      }
    });
    //call api
    AsyncStorage.getItem(STRINGS.loginToken, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginToken = JSON.parse(result);
        if (loginToken !== undefined) {
          this.setState({ loginToken: loginToken });
          this.props.GetProfileApi({
            loginToken,
          });
          this.showDialog();
        }
      }
    });
  };

  //******************* method call every time ************** */
  onFocusFunction = () => {
    this.asyncDataGettingMethod();
  };

  componentWillUnmount() {
    this._unsubscribe();
  }

  //******************end here */

  ///result here
  componentWillReceiveProps(nextProps, nextContext) {
    const { navigation } = this.props;
    const { isUpdateClicked, loginData } = this.state;
    const { getProfileResponse } = nextProps.GetProfileState;
    if (getProfileResponse !== undefined && getProfileResponse !== null) {
      if (getProfileResponse === "Network request failed") {
        this.hideDialog();
        this.showToastAlert(STRINGS.CHECK_INTERNET);
      } else {
        const { response, status, message, data } = getProfileResponse;
        if (response !== undefined) {
          this.hideDialog();
          if (status === 200) {
            this.hideDialog();
            this.setState({
              firstNameText: response.first_name,
              lastName: response.last_name,
              emailText: response.email,
              phoneNoText: response.mobile_number,
              countryName: response.country.name,
              countryId: response.country.id,
              stateName: response.state.name,
              stateId: response.state.id,
              cityName: response.city.name,
              cityId: response.city.id,
              userId: response.id,
              isChangedValue: false,
            });
            if (response.profile_image.length > 1) {
              this.setState({
                profilePic:
                  "https://hotel.appsndevs.com" + response.profile_image,
                profileImage:
                  "https://hotel.appsndevs.com" + response.profile_image,
              });
            } else {
              this.setState({
                profilePic: undefined,
                profileImage: "",
              });
            }
            const object2 = Object.assign({}, loginData, {
              profile_image: response.profile_image,
            });
            console.warn(object2);
            AsyncStorage.setItem(STRINGS.loginData, JSON.stringify(object2));
            if (isUpdateClicked) {
              this.showToastSucess("Profile details updated successfully");
              this.setState({
                isUpdateClicked: false,
              });
              this.props.navigation.dispatch(
                CommonActions.reset({
                  index: 0,
                  routes: [{ name: "customerHome" }],
                })
              );
            }
            this.getCountryList();
          }
        } else if (status === 111) {
          if (message.non_field_errors !== undefined) {
            // if (message.non_field_errors[0] === "User with this social_id does not exist")
          } else {
            AsyncStorage.removeItem(STRINGS.loginCredentials);
            navigation.dispatch(
              CommonActions.reset({
                index: 0,
                routes: [{ name: "login" }],
              })
            );
            this.showToastAlert(message);
          }
        } else if (status === 401) {
          this.hideDialog();
          console.warn(message);
        } else {
          this.hideDialog();
          this.showToastAlert("something went wrong");
        }
      }
    } else {
      this.hideDialog();
    }
  }

  //*********************************** get Country List ***************************************

  getCountryList = () => {
    RegisterCountryAction(
      {
        accessToken: this.state.accessToken,
      },
      (data) => this.countryResponse(data)
    );
    // this.showDialog();
  };

  countryResponse = (data) => {
    if (data !== undefined) {
      this.hideDialog();
      if (data.status === 200) {
        this.hideDialog();
        this.setState({
          countryData: data.response,
        });
      } else if (data.status === 401) {
        this.hideDialog();
        this.showToastAlert(data.message);
      }
    } else {
      this.hideDialog();
    }
  };

  stateResponse = (data) => {
    if (data !== undefined) {
      this.hideDialog();
      if (data.status === 200) {
        this.hideDialog();
        this.setState({
          stateData: data.response,
        });
      } else if (data.status === 401) {
        this.hideDialog();
        this.showToastAlert(data.message);
      }
    } else {
      this.hideDialog();
    }
  };

  cityResponse = (data) => {
    if (data !== undefined) {
      this.hideDialog();
      if (data.status === 200) {
        this.hideDialog();
        this.setState({
          cityData: data.response,
        });
      } else if (data.status === 401) {
        this.hideDialog();
        this.showToastAlert(data.message);
      }
    } else {
      this.hideDialog();
    }
  };

  /*
 // =============================================================================================
 // Render method for Header
 // =============================================================================================
 */
  _renderHeader() {
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.backGround_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        // leftComponent={(<LeftIcon onPress={() => navigation.goBack()}/>)}
        centerComponent={{
          text: STRINGS.cust_profile_header_title,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  // -------------------------------------------------------

  choosePicture() {
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log("User cancelled image picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else if (response.customButton) {
        console.log("User tapped custom button: ", response.customButton);
      } else {
        console.log("response is",response)
        const source = response.uri;
        console.log("set", response);
        this.setState({
          profilePic: source,
          profileImage: response,
          isChangedValue: true,
        });
      }
    });
  }

  /*
    // =============================================================================================
    // Render method for country code picker
    // =============================================================================================
    */
  _renderCountryPicker = () => {
    return (
      <CountryPicker
        ref={(picker) => {
          this.picker = picker;
        }}
        countryList={NORTH_AMERICA}
        onSelect={(value) => {
          this.setState({
            cca2: value.cca2,
            callingCode: value.callingCode[0],
          });
        }}
        hideAlphabetFilter={true}
        showCallingCode={true}
        filterPlaceholder={STRINGS.SEARCH_TEXT}
        closeable={true}
        filterable={true}
        cca2={this.state.cca2}
        translation="eng"
        autoFocusFilter={false}
        countryCode={this.state.cca2}
        withFilter={true}
      />
    );
  };

  /*
        // =============================================================================================
        // Render method for Phone number field
        // =============================================================================================
        */

  _renderPhoneNumberView = () => {
    const { phoneNoText, callingCode } = this.state;
    // let countryCodeLength = (callingCode.length > 2) ? 9 : 10;
    return (
      <View style={styles.phoneContainerStyle}>
        <ShadowViewContainer>
          <Input
            inputContainerStyle={{
              borderBottomColor: "transparent",
            }}
            editable={false}
            pointerEvents={"none"}
            // leftIcon={this._renderCountryPicker}
            leftIconContainerStyle={{ marginLeft: 0, alignItems: "center" }}
            value={`+${callingCode}`}
            returnKeyType={"next"}
            rightIcon={<Image source={ICONS.DOWN_ARROW} />}
            containerStyle={{
              width: wp("24%"),
              height: wp("12%"),
              
            }}
            inputStyle={{
              fontSize: FONT.TextSmall_2,
            }}
          />
        </ShadowViewContainer>
        <Spacer row={1.5} />
        <ShadowViewContainer>
          <Input
            inputContainerStyle={{
              borderBottomColor: "transparent",
            }}
            editable={false}
            placeholder={STRINGS.MOBILE_NUMBER}
            placeholderTextColor={COLORS.placeholder_color}
            ref="Mobile"
            value={phoneNoText}
            onChangeText={(text) =>
              this.setState({
                phoneNoText: text.trim() && text.replace(/^0+/, ""),
                // isChangedValue: true
              })
            }
            maxLength={10}
            keyboardType="phone-pad"
            returnKeyType={"next"}
            inputStyle={{
              paddingLeft: wp(0.5), //fontFamily: FONTNAME.RalewayRegular
              fontSize: FONT.TextSmall_2,
             
            }}
            containerStyle={{
              paddingBottom: 0,
              alignItems: "center",
              width: wp("58%"),
              height: wp("12%"),
              backgroundColor:'lightgray',
              borderRadius:wp('2%')
            }}
          />
        </ShadowViewContainer>
      </View>
    );
  };

  /**
   *  Handle modal trigger
   */
  triggerCountryPicker = (value) => {
    this.setState((prevState) => {
      return {
        displayCountryModal: true,
        modal_type: value,
        // index: index,
        // pass_type: passengerType
      };
    });
  };

  closeCountryModal() {
    this.setState({
      displayCountryModal: false,
    });
  }

  /**
   * handle item selected in modal
   */
  onItemClick = (item) => {
    const { accessToken } = this.state;
    if (this.state.modal_type === "country") {
      this.setState({
        displayCountryModal: false,
        countryName: item.name,
        countryId: item.id,
        stateName: "",
        stateId: "",
        cityName: "",
        cityId: "",
        stateData: [],
        cityData: [],
      });
      RegisterStateAction(
        {
          accessToken: accessToken,
          id: item.id,
        },
        (data) => this.stateResponse(data)
      );
      // this.showDialog();
    } else if (this.state.modal_type === "state") {
      this.setState({
        displayCountryModal: false,
        stateName: item.name,
        stateId: item.id,
        cityName: "",
        cityId: "",
        cityData: [],
      });
      RegisterCityAction(
        {
          accessToken: accessToken,
          id: item.id,
        },
        (data) => this.cityResponse(data)
      );
      // this.showDialog();
    } else {
      this.setState({
        displayCountryModal: false,
        cityName: item.name,
        cityId: item.id,
      });
    }
  };

  /*
        // =============================================================================================
        // Update button action and API CALL
        // =============================================================================================
    */
  imageTimestamp() {
    return Math.floor(100000 + Math.random() * 900000);
  }

  UpdatePress = () => {
    const {
      firstNameText,
      lastName,
      emailText,
      userId,
      countryName,
      stateName,
      cityName,
      loginToken,
      profileImage,
      countryId,
      stateId,
      cityId,
      isChangedValue,
    } = this.state;
    let timestamp = this.imageTimestamp();
    if (firstNameText.length < 1) {
      this.showToastAlert(STRINGS.empty_first_name);
    } else if (lastName.length < 1) {
      this.showToastAlert(STRINGS.empty_last_name);
    } else if (!Validations.validateEmail(emailText)) {
      this.showToastAlert(STRINGS.enter_valid_email);
    } else if (countryName.length < 1) {
      this.showToastAlert(STRINGS.empty_country_name);
    } else if (stateName.length < 1) {
      this.showToastAlert(STRINGS.empty_state_name);
    } else if (cityName.length < 1) {
      this.showToastAlert(STRINGS.empty_city_name);
      // } else if (!isChangedValue) {
      //     this.showToastAlert("There is nothing to update");
    } else {
      this.setState({
        isUpdateClicked: true,
      });

      this.props.UpdateProfileApi({
        firstNameText,
        lastName,
        countryId,
        stateId,
        cityId,
        loginToken,
        profileImage,
        userId,
        timestamp,
        isChangedValue,
      });
      this.showDialog();
    }
  };

  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  /*
          // =============================================================================================
          // Render method for screen
          // =============================================================================================
      */
  render() {
    const {
      firstNameText,
      lastName,
      emailText,
      displayCountryModal,
      countryName,
      modal_type,
      cityName,
      stateName,
      countryData,
      stateData,
      cityData,
    } = this.state;
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <DismissKeyboard>
          <ScrollContainer>
            <MainContainer>
              {/*<Spacer space={1}/>*/}
              {/*<View style={styles.profileImgContainer}>*/}
              <TouchableOpacity
                onPress={() => this.choosePicture()}
                style={{
                  alignSelf: "flex-end",
                  marginTop: wp(3),
                  marginRight: wp(3),
                }}
              >
                <Image style={styles.addIcon} source={ICONS.EDIT_IMG_ICON} />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate("ShowFullViewImg", {
                    imgUrl:
                      this.state.profilePic !== undefined &&
                      this.state.profilePic !== null
                        ? this.state.profilePic
                        : ICONS.PROFILE_PLACEHOLDER,
                  })
                }
              >
                <Image
                  style={styles.imgProfile}
                  source={
                    this.state.profilePic !== undefined
                      ? { uri: this.state.profilePic }
                      : ICONS.PROFILE_PLACEHOLDER
                  }
                />
              </TouchableOpacity>
              <Spacer space={3} />
              <View style={styles.bgStyle}>
                <Spacer space={1} />
                <CustomTextInput
                  placeholder={STRINGS.cust_profile_first_name_placeholder}
                  returnKeyType={"next"}
                  inputvalue={firstNameText}
                  ipOnChangeText={(text) =>
                    this.setState({
                      firstNameText: text,
                    })
                  }
                />
                <Spacer space={1} />
                <CustomTextInput
                  placeholder={STRINGS.cust_profile_last_name_placeholder}
                  inputvalue={lastName}
                  ipOnChangeText={(text) => this.setState({ lastName: text })}
                />
                <Spacer space={1} />
                {/* <CustomTextInput
                  placeholder={STRINGS.cust_profile_email_placeholder}
                  inputvalue={emailText}
                  isEditable={false}
                  ipOnChangeText={(text) => this.setState({ emailText: text })}
               
                
                /> */}

                <ShadowViewContainer style={styles.containerStyle}>
                  <TextInput
                    placeholderTextColor={COLORS.placeholder_color}
                    autoCorrect={false}
                    placeholder={STRINGS.cust_profile_email_placeholder}
                    style={[styles.inputStyle,{backgroundColor:'lightgray'}]}
                    value={emailText}
                    onChangeText={() => this.setState({ emailText: text })}
                    editable={false}
                  />
                </ShadowViewContainer>

                <Spacer space={1} />
                {this._renderPhoneNumberView()}
                <TouchableOpacity
                  onPress={() => this.triggerCountryPicker("country")}
                >
                  <ShadowViewContainer
                    style={{
                      width: wp("85%"),
                      // backgroundColor: 'hotpink',
                      height: Platform.OS === "ios" ? wp(10.5) : wp(12.5),
                    }}
                  >
                    <Input
                      inputContainerStyle={{
                        height: Platform.OS === "ios" ? wp(10.5) : wp(12.5),
                        backgroundColor: COLORS.white_color,
                        borderRadius: wp(3),
                        borderBottomWidth: 0,
                        width: wp("85%"),
                        marginLeft: -10,
                        
                      }}
                      editable={false}
                      placeholder={STRINGS.reg_country_placeholder}
                      placeholderTextColor={COLORS.placeholder_color}
                      value={countryName}
                      onChangeText={(text) =>
                        this.setState({
                          countryName: text.trim(),
                        })
                      }
                      inputStyle={{
                        color: COLORS.black_color,
                        fontSize: FONT.TextSmall_2,
                        marginLeft: wp(2),
                      }}
                      rightIconContainerStyle={{ marginRight: wp(2) }}
                      rightIcon={
                        <Image
                          source={ICONS.DOWN_ARROW}
                          resizeMode={"contain"}
                        />
                      }
                    />
                  </ShadowViewContainer>
                </TouchableOpacity>
                <Spacer space={0.5} />
                <TouchableOpacity
                  onPress={() => this.triggerCountryPicker("state")}
                >
                  <ShadowViewContainer
                    style={{
                      width: wp("85%"),
                      // backgroundColor: 'hotpink',
                      height: Platform.OS === "ios" ? wp(10.5) : wp(12.5),
                    }}
                  >
                    <Input
                      inputContainerStyle={{
                        height: Platform.OS === "ios" ? wp(10.5) : wp(12.5),
                        backgroundColor: COLORS.white_color,
                        borderRadius: wp(3),
                        borderBottomWidth: 0,
                        width: wp("85%"),
                        marginLeft: -10,
                      }}
                      editable={false}
                      placeholder={STRINGS.reg_state_placeholder}
                      placeholderTextColor={COLORS.placeholder_color}
                      value={stateName}
                      onChangeText={(text) =>
                        this.setState({
                          stateName: text.trim(),
                        })
                      }
                      inputStyle={{
                        color: COLORS.black_color,
                        fontSize: FONT.TextSmall_2,
                        marginLeft: wp(2),
                      }}
                      rightIconContainerStyle={{ marginRight: wp(2) }}
                      rightIcon={
                        <Image
                          source={ICONS.DOWN_ARROW}
                          resizeMode={"contain"}
                        />
                      }
                    />
                  </ShadowViewContainer>
                </TouchableOpacity>
                <Spacer space={0.5} />
                <TouchableOpacity
                  onPress={() => this.triggerCountryPicker("city")}
                >
                  <ShadowViewContainer
                    style={{
                      width: wp("85%"),
                      // backgroundColor: 'hotpink',
                      height: Platform.OS === "ios" ? wp(10.5) : wp(12.5),
                    }}
                  >
                    <Input
                      inputContainerStyle={{
                        height: Platform.OS === "ios" ? wp(10.5) : wp(12.5),
                        backgroundColor: COLORS.white_color,
                        borderRadius: wp(3),
                        borderBottomWidth: 0,
                        width: wp("85%"),
                        marginLeft: -10,
                      }}
                      editable={false}
                      placeholder={STRINGS.reg_city_placeholder}
                      placeholderTextColor={COLORS.placeholder_color}
                      value={cityName}
                      onChangeText={(text) =>
                        this.setState({
                          cityName: text.trim(),
                        })
                      }
                      inputStyle={{
                        color: COLORS.black_color,
                        fontSize: FONT.TextSmall_2,
                        marginLeft: wp(2),
                      }}
                      rightIconContainerStyle={{ marginRight: wp(2) }}
                      rightIcon={
                        <Image
                          source={ICONS.DOWN_ARROW}
                          resizeMode={"contain"}
                        />
                      }
                    />
                  </ShadowViewContainer>
                </TouchableOpacity>
                <Spacer space={1} />
                <TouchableOpacity onPress={() => this.UpdatePress()}>
                  <View style={styles.buttonStyle}>
                    <Text style={styles.buttonText}>
                      {STRINGS.cust_profile_button}
                    </Text>
                  </View>
                </TouchableOpacity>
                <Spacer space={1} />
              </View>
              {/*<Spacer space={5}/>*/}
              <CountryModal
                visibleCountry={displayCountryModal}
                value={
                  modal_type === "country"
                    ? countryName
                    : modal_type === "state"
                    ? stateName
                    : cityName
                }
                passingValue={
                  modal_type === "country"
                    ? countryData
                    : modal_type === "state"
                    ? stateData
                    : cityData
                }
                onItemClick={(item, value) => {
                  this.onItemClick(item, value);
                }}
                closeCountryModal={() => {
                  this.closeCountryModal();
                }}
              />
              {this._renderCustomLoader()}
            </MainContainer>
          </ScrollContainer>
        </DismissKeyboard>
      </SafeAreaViewContainer>
    );
  }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state) => ({
  GetProfileState: state.GetProfileReducer,
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    GetProfileApi: (payload) => dispatch(GetProfileAction(payload)),
    UpdateProfileApi: (payload) => dispatch(UpdateProfileAction(payload)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(CustomerProfile);
