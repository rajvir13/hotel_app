import React from 'react';
import styled from 'styled-components';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { StyleSheet } from 'react-native';

import {FONT} from "../../../utils/FontSizes";
import COLORS from "../../../utils/Colors";
const styles = StyleSheet.create({

    mainContainerStyle: {
        alignSelf: 'center',
        width: wp('100%'),
        padding: wp('1%'),
        justifyContent: 'center',
        flex: 1
    },
    profileImgContainer: {
        justifyContent: 'center',
        alignItems: "center",
        padding:wp('5%'),
        // backgroundColor:'pink'
    },
    addIcon: {
        margin: wp('1%'),
        resizeMode: 'stretch'
    },

    inputStyle: {
        color: COLORS.black_color,
        fontSize: FONT.TextSmall_2,
        // fontFamily: 'Montserrat',
        paddingHorizontal:wp(2),
        paddingVertical:wp(3),
        borderRadius:wp('2%')
    },
    containerStyle: {
        marginVertical:wp(0.5),
        width:wp('85%'),
        
    },

    // imgOuterView:{
    //     height: wp('27%'),
    //     width: wp('27%'),
    //     borderRadius: wp('27%') / 2,
    //     padding:wp('1%'),
    //     backgroundColor:'white'
    // },
    profilePicStyle: {
        resizeMode: "cover",
        height: wp('25%'),
        width: wp('25%'),
        borderRadius: wp('25%') / 2,
        borderWidth:0.5,
        borderColor: COLORS.light_grey3,
    },


    imgProfile:{

        resizeMode: "cover",
        height: wp('25%'),
        width: wp('25%'),
        borderRadius: wp('25%') / 2,
        borderWidth:0.5,
        borderColor: COLORS.light_grey3,
        alignSelf:'center'
    },
    bgStyle: {
        backgroundColor: COLORS.app_theme_color,
        width: wp('100%'),
        padding:wp('5%'),
        paddingHorizontal:wp('8%'),
        borderTopLeftRadius:wp('10%'),
        borderTopRightRadius:wp('10%'),
        justifyContent:'center',
        alignItems:'center',
        paddingBottom: wp(17)
    },

    phoneContainerStyle: {
        flexDirection: "row",
        height: Platform.OS === "ios" ? wp(19) :wp(20),
        paddingVertical: wp(2)
            },
    buttonStyle:{
        marginVertical:10,
        alignSelf:"center",
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#CB3B3B',
        height: wp('12%'),
        width: wp('80%'),
        borderRadius: wp('12%') / 2,
    },
    buttonText:{
        fontSize:16,
        fontStyle:"normal",
        fontWeight:"500",
        lineHeight:20,
        display:"flex",
        // fontFamily: 'Montserrat',
        color: '#FFFFFF',
        textAlign: 'center',
    },

});

export default styles;
