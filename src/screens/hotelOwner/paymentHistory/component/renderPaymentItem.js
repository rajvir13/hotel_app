import React from "react";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { AirbnbRating } from "react-native-elements";
import { Text, TouchableOpacity, View ,Image} from "react-native";
import FastImage from "react-native-fast-image";
import { ShadowViewContainer } from "../../../../utils/BaseStyle";

import { Spacer } from "../../../../customComponents/Spacer";
import { FONT } from "../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../utils/Font";
import LocationIconSVG from "../../../../../assets/images/CustomerHome/LocationIconSVG";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import {ICONS} from "../../../../utils/ImagePaths";

import styles from '../styles'
const RenderPayementItem = (item) => {
  return (
    <ShadowViewContainer style={{  marginTop:wp('3%'), width: wp(92),padding:wp('1%') }}>
      <View style={styles.outerView}>
  <Text style={styles.paymentText}>Paid Amount {"\u20B9"} :{item.item.amount}</Text>
        <Text style={styles.textStyle}>Interval : {item.item.interval[0]} to {item.item.interval[1]}</Text>
  <Text style={styles.textStyle}>Plan : {item.item.plan}</Text>
        <View style={styles.historyView}>
          <View style={{flexDirection:'row',}}>
            <Text style={styles.commisionText}>Commission : </Text>
            <Text style={styles.commisionText}>{"\u20B9"}{item.item.commision_amount} </Text>
          </View>
          <View style={{flexDirection:'row',}}>
        
          <Text style={styles.commisionText}>Plan Charges : </Text>
  <Text style={styles.commisionText}>  {"\u20B9"}{item.item.plan_amount}</Text>
          </View>
        </View>
        <View
          style={styles.viewLine}
        ></View>
        <View style={{ flexDirection: "row" ,justifyContent:'space-between' }}>
  <Text style={styles.textStyle}>Paid On : {item.item.paid_on}</Text>
          <Image style={{width:wp('5%'),height:wp('5%'),alignSelf:'center'}} source={ICONS.appliedImg} resizeMode={'contain'}/>
        </View>
        <View style={{width:wp('90%')}}>
     <Text style={styles.textStyle}>Transaction Id :{item.item.transaction_id}</Text></View>
      </View>
    </ShadowViewContainer>
  );
};
export default RenderPayementItem;
