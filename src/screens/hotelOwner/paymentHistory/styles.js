import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Platform, StyleSheet } from "react-native";
import { FONT } from "../../../utils/FontSizes";
import COLORS from "../../../utils/Colors";
import { FONT_FAMILY } from "../../../utils/Font";

const styles = StyleSheet.create({
  outerView: {
    padding: wp("3%"),
  },
  paymentText: {
    fontSize: FONT.TextMedium,
    margin: wp("1%"),
    color: COLORS.app_theme_color,
    fontWeight:'bold'
  },

  commisionText:{
    fontSize: FONT.TextSmall,
    color: COLORS.greyButton_color2,
  },
  historyView: {
    flexDirection: "row",
    padding: wp("1%"),
    justifyContent:'space-between'
  },
viewLine:{

  height: 1,alignSelf:'center', width: wp("85%"), backgroundColor: "gray",margin:wp('1%')
},
  textStyle: {
    fontSize: FONT.TextSmall,
    margin: wp("1%"),
    color: COLORS.greyButton_color2,
  },
});

export default styles;
