// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react";
import {
  View,
  FlatList,
  Text,
  Image,
  Platform,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  Dimensions,
} from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Header } from "react-native-elements";
import AsyncStorage from "@react-native-community/async-storage";
import { CommonActions } from "@react-navigation/native";
import * as _ from "lodash";

import BaseClass from "../../../utils/BaseClass";
import {
  ShadowViewContainer,
  SafeAreaViewContainer,
  MainContainer,
  ScrollContainer,
} from "../../../utils/BaseStyle";
import { Spacer } from "../../../customComponents/Spacer";
import COLORS from "../../../utils/Colors";
import {
  ListingIcon,
  LocationIcon,
  RightIcon,
  DeleteIcon,
} from "../../../customComponents/icons";
import STRINGS from "../../../utils/Strings";
import { FONT } from "../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../utils/Font";
import styles from "./styles";
import { LeftIcon } from "@customComponents/icons";
import { ICONS } from "../../../utils/ImagePaths";
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import RenderPayementItem from "./component/renderPaymentItem";
import { PaymentHistoryAction } from "../../../redux/actions/PaymentHistory";

import moment from "moment";

const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

class PaymentHistory extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      accessToken: undefined,
      ownerLoginId: undefined,
      paymentHistory: [],
    };
  }

  componentDidMount() {
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
       
        if(loginData.hotel_details!==undefined)
        {
          console.log("if case",loginData.access_token)

          this.setState({
            accessToken: loginData.access_token,
            ownerLoginId: loginData.hotel_details.owner,
          });
          this.getPaymentHistory(loginData.access_token,loginData.hotel_details.owner);
        }
        else
        {
          console.log("else case",loginData.access_token)
          this.setState({
            accessToken: loginData.access_token,
            ownerLoginId: loginData.owner.id,
          });
          this.getPaymentHistory(loginData.access_token,loginData.owner.id,);

        }
      }
    });
  }

  getPaymentHistory(accessToken, ownerId) {

    this.showDialog();
    PaymentHistoryAction(
      {
        accessToken: accessToken,
        ownerId: ownerId,
      },
      (data) => this.paymentHistoryResponse(data)
    );
  }
  paymentHistoryResponse = (data) => {
    this.hideDialog();
    const { response, message, status } = data;
    if (data === "Network request failed" || data == "TypeError: Network request failed") {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (response !== undefined) {
        if (status === 200) {
          this.setState({
            paymentHistory: response,
          });
        } else if (status === 111) {
          this.showToastAlert(message);
        } else if (status === 401) {
          this.showToastAlert(message);
        } else {
          this.showToastAlert(message);
        }
      } else if (message.non_field_errors !== undefined) {
        this.showToastAlert(message.non_field_errors[0]);
      } else {
        this.showToastAlert(message);
      }
      this.hideDialog();
    }
  };

  _renderHeader(props) {
    const { navigation, comesFrom } = this.props;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={<LeftIcon onPress={() => navigation.goBack()} />}
        centerComponent={{
          text: "Payment history",
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  _renderCustomLoader = () => {
    // const {isLoading} = this.state;
    return (
      <OrientationLoadingOverlay
        // visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  render() {
    const { paymentHistory } = this.state;
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <DismissKeyboard>
          <ScrollContainer>
            <MainContainer>
              {paymentHistory.length !== 0 ? (
                <FlatList
                  style={styles.listStyle}
                  extraData={this.state}
                  data={paymentHistory}
                  keyExtractor={(item, index) => index}
                  renderItem={({ item, index }) => (
                    <RenderPayementItem item={item} index={index} />
                  )}
                />
              ) : (
                <View style={{ width: wp("100%"), height: wp("100%") }}>
                  <Text
                    style={{
                      paddingVertical: wp(3),
                      textAlign: "center",
                      fontSize: FONT.TextNormal,
                      fontFamily: FONT_FAMILY.MontserratBold,
                    }}
                  >
                    NO DATA{" "}
                  </Text>
                </View>
              )}

              {this._renderCustomLoader()}
            </MainContainer>
          </ScrollContainer>
        </DismissKeyboard>
      </SafeAreaViewContainer>
    );
  }
}

export default PaymentHistory;
