import React from "react";
import {
  View,
  FlatList,
  Text,
  Image,
  Platform,
  StatusBar,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ImageBackground,
  Keyboard,
} from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Header, Input } from "react-native-elements";
import BaseClass from "../../../utils/BaseClass";
import {
  SafeAreaViewContainer,
  ScrollContainer,
} from "../../../utils/BaseStyle";
import { Spacer } from "../../../customComponents/Spacer";
import COLORS from "../../../utils/Colors";
import { CommonActions } from "@react-navigation/native";
import { LeftIcon } from "../../../customComponents/icons";
import STRINGS from "../../../utils/Strings";
import { FONT } from "../../../utils/FontSizes";
import { ICONS } from "../../../utils/ImagePaths";
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import styles from "./styles";
import { CustoemTextComponents } from "./components/customeTextField";
import ImagePicker from "react-native-image-crop-picker";
import * as _ from "lodash";
import { connect } from "react-redux";
import { UpdateHotelAction } from "../../../redux/actions/UpdateHotelAddress";
import * as DeviceInfo from "react-native-device-info";
import AsyncStorage from "@react-native-community/async-storage";
import { FONT_FAMILY } from "../../../utils/Font";

//==================Dismiss keyboard  =======================
const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

//=================== main class declareation======================
class HotelProfile extends BaseClass {
  //===================== constructor================================
  //===================== constructor================================
  constructor(props) {
    super(props);
    const {
      owner_Name,
      owner_Last_Name,
      owner_email,
      owner_mobileNo,
      hotelName,
      hotelAddr,
      hotelID,
      ownerId,
      lat,
      long,
    } = this.props.route.params;

    this.state = {
      ownerName: owner_Name,
      ownerLastName: owner_Last_Name,
      emailText: owner_email,
      contactNo: owner_mobileNo,
      hotelName: hotelName,
      imagesList: [],
      imagesList2: [],
      hotelAddress: hotelAddr,
      isAddShow: true,
      deviceToken: "41345qf1gy2y",
      deviceId: "",
      valueLat: lat,
      valueLng: long,
      accessToken: "",
      device_type: undefined,
      hotelID: hotelID,
      authToken: undefined,
      ownerId: ownerId,
      isLoading: false,
    };
  }

  //====================life cycle method==================
  componentDidMount() {
    const { hotels_image } = this.props.route.params;
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        this.setState({
          authToken: loginData.access_token,
        });
        // this.showDialog();
      }
    });

    if (hotels_image !== undefined) {
      if (hotels_image.length > 0) {
        let imageArr = [];
        for (let i = 0; i < hotels_image.length; i++) {
          if (hotels_image[i].image !== null) {
            imageArr.push({
              uri: hotels_image[i].image,
              id: hotels_image[i].id,
            });
          }
        }
        // this.setHotelImage(hotels_image)
        this.setState({
          imagesList: imageArr,
        });
      }
    }
  }

  //========================= customer loder ==========================
  _renderCustomLoader = () => {
    const { isLoading } = this.state;

    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  callback(lat, lng, response) {
    this.setState({
      hotelAddress: response.results[0].formatted_address,
    });
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const { navigation } = this.props;
    const { isLoading } = this.state;
    this.setState({ isLoading: false });
    const { HotelAddressResponse } = nextProps.hotelAddressState;
    if (HotelAddressResponse !== undefined && HotelAddressResponse !== null) {
      if (HotelAddressResponse === "Network Error") {
        this.hideDialog();
        this.showToastAlert(STRINGS.CHECK_INTERNET);
      } else {
        const { response, message, status } = HotelAddressResponse;
        if (response !== undefined) {
          if (status === 200) {
            this.hideDialog();
            this.showToastSucess(message);
            this.props.navigation.goBack();
          } else {
            this.hideDialog();
            this.showToastAlert(message);
          }
        } else if (status === 111 && message !== undefined) {
          this.hideDialog();
          if (message.non_field_errors !== undefined) {
            this.showToastAlert(message.non_field_errors[0]);
          } else {
            AsyncStorage.removeItem(STRINGS.loginCredentials);
            navigation.dispatch(
              CommonActions.reset({
                index: 0,
                routes: [{ name: "login" }],
              })
            );
            this.showToastAlert(message);
          }
        } else if (status === 401) {
          this.hideDialog();
          this.showToastAlert(message);
        } else {
          this.hideDialog();
          this.showToastAlert("server error");
        }
        this.hideDialog();
      }
    }
  }

  //============================ render header ===============================

  _renderHeader() {
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={<LeftIcon onPress={() => navigation.goBack()} />}
        centerComponent={{
          text: STRINGS.HOTEL_PROFILE_HEADER,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  // HotelProfile
  //======================== add image =======================
  AddIcon = () => {
    return (
      this.state.isAddShow && (
        <View style={styles.addIconOuterView}>
          <TouchableWithoutFeedback onPress={() => this.AddImageAction()}>
            <View style={styles.imgOuterView}>
              <TouchableWithoutFeedback onPress={() => this.AddImageAction()}>
                <Image
                  style={styles.hotelImage}
                  source={ICONS.ADD_IMAGE_ICON}
                />
              </TouchableWithoutFeedback>
            </View>
          </TouchableWithoutFeedback>
        </View>
      )
    );
  };

  //============================ Add Image Button Action=====================================
  AddImageAction() {
    const { imagesList } = this.state;
    ImagePicker.openPicker({
      compressImageQuality: 0.2,
      includeBase64: true,
      multiple: true,
      maxFiles: 10,
      mediaType: "photo",
    }).then((response) => {
      let tempArray = [];
      response.forEach((item) => {
        tempArray.push(item);
      });
      let count = imagesList.length + response.length;
      if (count >= 1 && count < 11) {
        // if (imagesList.length !== 0) {
        response.forEach((item) => {
          let image = {
            completeData: item,
            uri: item.path,
          };
          imagesList.push(image);
        });
        if (count === 10) this.setState({ isAddShow: false });
        this.setState({
          refresh: !this.state.refresh,
        });
      } else {
        this.showToastAlert(STRINGS.ONLY_TEN_IMAGES);
      }
    });
  }

  _renderChooseImage = () => {
    const { imagesList } = this.state;
    return (
      <FlatList
        style={styles.flatListStyle}
        data={imagesList}
        renderItem={this._renderImageList}
        horizontal={true}
        showsHorizontalScrollIndicator={true}
        keyExtractor={(item, index) => index}
      />
    );
  };

  _renderImageList = ({ item, index }) => (
    <>
      <ImageBackground
        style={styles.imageStyle}
        source={{ uri: item !== undefined ? item.uri : "" }}
        imageStyle={styles.crossIconOuterView}
      >
        <TouchableWithoutFeedback onPress={() => this.CrossAction(item, index)}>
          <Image style={styles.crossIconStyle} source={ICONS.CROSS_ICON} />
        </TouchableWithoutFeedback>
      </ImageBackground>
    </>
  );

  //========================= Remove  Image from list  Action==========================

  CrossAction(i, indexClicked) {
    //remove value from imagelist
    const { imagesList, imagesList2 } = this.state;
    let imagesData = imagesList;
    let deletedArray = imagesList2;

    /** Add image ids to new array for deletion */
    for (let j = 0; j < imagesData.length; j++) {
      if (indexClicked === j) {
        if (imagesData[j].id !== undefined) {
          deletedArray.push(imagesData[j].id);
        }
      }
    }

    /** Deleting item from array */
    let tempArray = _.remove(imagesList, (item, index) => {
      return indexClicked !== index;
    });

    /** Setting deleted item's id and array to state */
    this.setState({
      imagesList: tempArray,
      imagesList2: deletedArray,
      refresh: !this.state.refresh,
    });
    if (imagesList.length !== 10) this.setState({ isAddShow: true });
  }

  //=======================end===================================

  changePassword = () => {
    const { ownerId, authToken } = this.state;
    this.props.navigation.navigate("ownerChangePassword", {
      ownerId: ownerId,
      authToken: authToken,
    });
  };

  updateAddress = () => {
    const {
      hotelName,
      imagesList2,
      imagesList,
      hotelAddress,
      hotelID,
      ownerName,
      ownerLastName,
      emailText,
      contactNo,
      authToken,
      valueLat,
      valueLng,
      isLoading,
    } = this.state;

    let imagesData = imagesList;
    let tempArr = imagesData.filter(function (e) {
      if (e.completeData !== undefined) {
        return e;
      }
    });
    if (ownerName !== undefined && ownerName.length < 1) {
      this.showToastAlert(STRINGS.empty_owner_name);
    } else if (ownerName !== undefined && ownerName.trimRight().length < 2) {
      this.showToastAlert(STRINGS.name_minimum_three);
    } else if (ownerLastName !== undefined && ownerLastName.length < 1) {
      this.showToastAlert(STRINGS.empty_last_name);
    } else if (
      ownerLastName !== undefined &&
      ownerLastName.trimRight().length < 2
    ) {
      this.showToastAlert(STRINGS.name_minimum_three);
    } else if (imagesList.length === 0) {
      this.showToastAlert("Image field is required.");
    } else if (hotelAddress.trim().length === 0) {
      this.showToastAlert("Address field is required.");
    } else if (hotelName.trim().length === 0) {
      this.showToastAlert("hotel name is required.");
    } else {
      console.log("hotelAddress", hotelAddress);
      this.props.updatehotelProfileApi({
        ownerName,
        ownerLastName,
        emailText,
        contactNo,
        device_id: DeviceInfo.getUniqueId(),
        hotelName,
        newAddedImages: tempArr,
        deletedImages: imagesList2,
        hotelAddress,
        authToken,
        hotelID,
        valueLng,
        valueLat,
      });
      this.setState({ isLoading: true });
    }
  };

  //=======================End Change and update password ========================
  //call back

  MapAction() {
    this.props.navigation.navigate("Maps", {
      callback: this.callback.bind(this),
    });
  }

  //=======================End Change and update password ========================

  render() {
    const {
      hotelAddress,
      emailText,
      ownerName,
      ownerLastName,
      contactNo,
      hotelName,
    } = this.state;
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <DismissKeyboard>
          <ScrollContainer>
            <View style={styles.mainProfileContainer}>
              <Spacer space={2.5} />
              <CustoemTextComponents
                placHolder="First Name"
                returnKeyType={"next"}
                keyboardType={"default"}
                value={ownerName}
                ipOnChangeText={(text) => this.setState({ ownerName: text })}
                // isEditable={false}
              />
              <Spacer space={2.5} />
              <CustoemTextComponents
                placHolder="Last Name"
                returnKeyType={"next"}
                keyboardType={"default"}
                value={ownerLastName}
                ipOnChangeText={(text) =>
                  this.setState({ ownerLastName: text })
                }
                // isEditable={false}
              />

              <Spacer space={2.5} />
              <CustoemTextComponents
                placHolder="Email"
                returnKeyType={"next"}
                keyboardType={"default"}
                value={emailText}
                ipOnChangeText={(text) => this.setState({ emailText: text })}
                isEditable={false}
              />
              <Spacer space={2.5} />
              <CustoemTextComponents
                placHolder="Contact"
                returnKeyType={"next"}
                keyboardType={"default"}
                value={contactNo}
                ipOnChangeText={(text) => this.setState({ contactNo: text })}
                isEditable={false}
              />
              <Spacer space={2.5} />
              <CustoemTextComponents
                placHolder="Hotel Name"
                returnKeyType={"next"}
                keyboardType={"default"}
                value={hotelName}
                ipOnChangeText={(text) => this.setState({ hotelName: text })}
              />
              <Spacer space={5} />
              <View style={{ flexDirection: "row" }}>
                {this._renderChooseImage()}
                {this.AddIcon()}
              </View>
              <Spacer space={2.5} />

              {/* <View style={styles.hotelAddressTextViewStyle}> */}
              <Input
                // editable={false}
                inputContainerStyle={styles.textViewStyle}
                placeholder={"Hotel Address"}
                placeholderTextColor={COLORS.placeholder_color}
                value={hotelAddress}
                onChangeText={(text) =>
                  this.setState({
                    hotelAddress: text.trim() && text.replace(/^0+/, ""),
                    // isChangedValue: true
                  })
                }
                rightIcon={
                  <TouchableOpacity
                    style={{ alignContent: "flex-end" }}
                    onPress={() => this.MapAction()}
                  >
                    <Image
                      style={{
                        height: wp(8),
                        width: wp(8),
                        alignSelf: "center",
                      }}
                      source={ICONS.MAP_ICON}
                    />
                  </TouchableOpacity>
                }
                returnKeyType={"next"}
                inputStyle={{
                  paddingLeft: wp(0.5), //fontFamily: FONTNAME.RalewayRegular
                  fontSize: FONT.TextSmall_2,
                }}
              />

              <Spacer space={3} />
              <TouchableOpacity onPress={() => this.changePassword()}>
                <View style={styles.changePasswordStyle}>
                  <Text style={styles.changePasswordText}>
                    {STRINGS.CHANGE_PASSWORD}
                  </Text>
                </View>
              </TouchableOpacity>
              <Spacer space={2} />
              <TouchableOpacity onPress={() => this.updateAddress()}>
                <View style={styles.buttonStyle}>
                  <Text style={styles.buttonText}>{STRINGS.UPDATE_BTN}</Text>
                </View>
              </TouchableOpacity>
            </View>
          </ScrollContainer>
        </DismissKeyboard>
      </SafeAreaViewContainer>
    );
  }
}

const mapStateToProps = (state) => ({
  hotelAddressState: state.UpdateHotelAddressReducer,
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    updatehotelProfileApi: (payload) => dispatch(UpdateHotelAction(payload)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(HotelProfile);
