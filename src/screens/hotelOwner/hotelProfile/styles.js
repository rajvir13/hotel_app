import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { StyleSheet, Dimensions } from "react-native";
import { FONT } from "../../../utils/FontSizes";
import COLORS from "../../../utils/Colors";

const window = Dimensions.get("window");

const styles = StyleSheet.create({
  mainProfileContainer: {
    flex: 1,
    alignItems: "center",
    backgroundColor: COLORS.backGround_color,
  },
  mapIconStyle: {
    height: wp(8),
    width: wp(8),
    alignSelf: "center",
  },
  textViewStyle: {
    width: wp("90%"),
    height: wp("13%"),
    backgroundColor: COLORS.white_color,
    borderWidth: 2,
    borderRadius: wp("2%"),
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: "center",
    paddingVertical: wp("2%"),
    marginLeft: wp("2%"),
    marginRight: wp("2%"),
    fontSize: FONT.TextMedium,
  },
  input: {
    paddingVertical: wp("2%"),
    marginLeft: wp("2%"),
    fontSize: wp("4.5%"),
    height: wp("13%"),
  },

  imgOuterView: {
    height: wp("20%"),
    width: wp("20%"),
    borderRadius: wp("20%") / 2,
    justifyContent: "center",
    backgroundColor: COLORS.light_grey,
    marginRight: wp("3%"),
  },
  imageStyle: {
    height: wp("20%"),
    width: wp("20%"),
    borderRadius: wp("20%") / 2,
    // backgroundColor: "orange",
    marginHorizontal: wp(2),
  },

  buttonStyle: {
    marginVertical: 10,
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: COLORS.app_theme_color,
    height: wp("12%"),
    width: wp("80%"),
    borderRadius: wp("12%") / 2,
  },

  changePasswordStyle: {
    marginVertical: 10,
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: COLORS.white_color,
    height: wp("12%"),
    width: wp("80%"),
    borderRadius: wp("12%") / 2,
    borderColor: COLORS.app_theme_color,
    borderWidth: 2,
  },

  changePasswordText: {
    color: COLORS.app_theme_color,
    fontWeight: "700",
    fontSize: wp("4.5%"),
  },

  buttonText: {
    fontSize: wp("4.5%"),
    fontStyle: "normal",
    fontWeight: "700",
    display: "flex",
    color: "#FFFFFF",
    textAlign: "center",
  },

  hotelAddressTextViewStyle: {
    width: wp("90%"),
    height: wp("12%"),
    backgroundColor: COLORS.white_color,
    borderWidth: 1,
    borderRadius: wp("2%"),
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    flexDirection: "row",
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: "center",
  },
  hotelAddressTextStyle: {
    width: wp("75%"),
    height: wp("11%"),
    marginLeft: wp("7%"),
  },

  addIconOuterView: {
    justifyContent: "center",
    alignSelf: "flex-end",
  },

  hotelImage: {
    height: wp("10%"),
    width: wp("10%"),
    alignSelf: "center",
    marginLeft: wp("1%"),
  },
  flatListStyle: {
    width: wp("60%"),
  },

  crossIconOuterView: {
    borderRadius: wp("20%") / 2,
    resizeMode: "cover",
  },
  crossIconStyle: {
    height: 15,
    width: 15,
    alignSelf: "flex-end",
    marginLeft: 15,
    marginBottom: 15,
  },
});
export default styles;
