import React from "react";
import {
    View,
    FlatList,
    Text,
    Image,
    Platform,
    StatusBar,
    TouchableOpacity,
    TextInput,
    TouchableWithoutFeedback,
    Keyboard,
    Alert,
} from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import {ICONS} from "../../../../utils/ImagePaths";
import styles from '../styles';
import COLORS from "../../../../utils/Colors";

//================== room type and sorting view ===================
const CustoemTextComponents = (props) => {


    return (
        <View style={styles.textViewStyle}>
            <TextInput style={styles.input}
                       underlineColorAndroid="transparent"
                       placeholder={props.placHolder}
                       placeholderTextColor={COLORS.greyButton_color}
                       autoCapitalize="none"
                       value={props.value}
                       onChangeText={(username) => props.ipOnChangeText(username)}
                       editable={props.isEditable}
                       secureTextEntry={props.isSecured}
            />
        </View>
    );
};


const HotelAddressTextComponents = (props) => {
    return (
        <View style={styles.hotelAddressTextViewStyle}>
            <TextInput style={styles.hotelAddressTextStyle}
                       underlineColorAndroid="transparent"
                       placeholder={props.placHolder}
                       placeholderTextColor={COLORS.greyButton_color}
                       autoCapitalize="none"
                       onChangeText={(username) => props.ipOnChangeText(handleEmail)}
            />
            <Image style={{height: 30, width: 30, alignSelf: 'center', marginRight: wp('10%')}}
                   source={ICONS.MAP_ICON}/>
        </View>
    );
};

export {
    CustoemTextComponents, HotelAddressTextComponents

}
