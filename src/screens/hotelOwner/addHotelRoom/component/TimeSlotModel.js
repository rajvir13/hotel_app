import React, { Component } from "react";
import {
  View,
  Text,
  Modal,
  StyleSheet,
  Image,
  TouchableOpacity,
  Alert,
  FlatList,
} from "react-native";

import propTypes from "prop-types";
import * as _ from "lodash";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Spacer } from "../../../../customComponents/Spacer";
import { FONT_FAMILY } from "../../../../utils/Font";
import { FONT } from "../../../../utils/FontSizes";
import COLORS from "../../../../utils/Colors";
import STRINGS from "../../../../utils/Strings";
import { ICONS } from "../../../../utils/ImagePaths";
import FastImage from "react-native-fast-image";
import { API } from "../../../../redux/constant";

import CrossComponent from "../../../../../assets/images/BookingIcons/CrossSVG";

export default class TimeSlotModelView extends Component {
  onCancelPress() {
    this.props.closeRoomModal();
  }

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: "100%",
          backgroundColor: "#000",
          marginTop: 5,
          marginBottom: 5,
        }}
      />
    );
  };

  getListViewItem = (item) => {
    this.props.timeSlotClicked(item.hour);
  };

  render() {
    const { value } = this.props;
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.isTimeSlotVisible}
      >
        <View
          style={{
            // flex:1,
            backgroundColor: "white",
            height: hp("50%"),
            width: wp("40%"),
            marginTop: wp("20%"),
            alignItems: "center",
            alignSelf: "center",
          }}
        >
          <View
            style={{
              height: wp("6%"),
              width: wp("40%"),
              backgroundColor: "blue",
            }}
          >
            <Text
              style={{
                color: "white",
                alignSelf: "center",
                fontWeight: "bold",
              }}
            >
              Time Slot
            </Text>
          </View>
          <FlatList
            data={[
              { key: "1 Hour", hour: "1" },
              { key: "2 Hour", hour: "2" },
              { key: "3 Hour", hour: "3" },
              { key: "4 Hour", hour: "4" },
              { key: "5 Hour", hour: "5" },
              { key: "6 Hour", hour: "6" },
              { key: "7 Hour", hour: "7" },
              { key: "8 Hour", hour: "8" },
              { key: "9 Hour", hour: "9" },
              { key: "10 Hour", hour: "10" },
              { key: "11 Hour", hour: "11" },
              { key: "12 Hour", hour: "12" },
            ]}
            renderItem={({ item }) => (
              <Text
                style={{ color: "black", fontWeight: "bold" }}
                onPress={this.getListViewItem.bind(this, item)}
              >
                {item.key}
              </Text>
            )}
            ItemSeparatorComponent={this.renderSeparator}
          />
          <TouchableOpacity
            hitSlop={{ top: wp(5), left: wp(5), bottom: wp(5) }}
            style={{
              position: "absolute",
              alignSelf: "flex-end",
              marginTop: wp(-3),
            }}
            onPress={() => {
              this.onCancelPress();
            }}
          >
            <CrossComponent width={wp(7)} height={wp(7)} />
          </TouchableOpacity>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    // justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.6)",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});
