import React from "react";
import {
  View,
  FlatList,
  Text,
  Image,
  Platform,
  StatusBar,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { ICONS } from "../../../../utils/ImagePaths";
import styles from "../styles";
import { SearchBar } from "react-native-elements";
import { Dropdown } from "react-native-material-dropdown";
import SearchIconSVG from "../../../../../assets/images/CustomerHome/searchIcon.svg";
import {
  SafeAreaViewContainer,
  MainContainer,
  ShadowViewContainer,
} from "../../../../utils/BaseStyle";
// import { Rating, AirbnbRating } from "react-native-elements";
import COLORS from "../../../../utils/Colors";
import { Spacer } from "../../../../customComponents/Spacer";
import { AirbnbRating } from "react-native-elements";
import { FONT_FAMILY } from "../../../../utils/Font";

//================== room type and sorting view ===================

const RenderUserNotification = (props) => {
  console.log("notification props are", props);
  return (
    <View
      style={
       styles.notificationView
      }
    >
      <Text style={{ fontWeight: "bold", fontSize: wp("4%"),fontStyle:'normal',fontFamily:FONT_FAMILY.Montserrat,marginBottom:wp('1.5%')}}>
      {props.item.name}
      </Text>
      <Text style={{color:'gray'}}>{props.item.description}</Text>
    </View>
  );
};

//================== search hotel view ===================

const CalenderView = (props) => {
  return (
    <View style={styles.headerCalenderView}>
      <View style={styles.calendar_Style1}>
        <TouchableOpacity onPress={props.fromDateTimeClikced}>
          <Image
            source={ICONS.CALENDER_IMG}
            style={styles.calenderIcon}
          ></Image>
        </TouchableOpacity>
        <Text style={styles.dateTimeText}>{props.fromDate}</Text>
      </View>

      <View style={styles.calendar_Style1}>
        <TouchableOpacity onPress={props.toDateTimeClikced}>
          <Image
            source={ICONS.CALENDER_IMG}
            style={styles.calenderIcon}
          ></Image>
        </TouchableOpacity>
        <Text style={styles.dateTimeText}>{props.toDate}</Text>
      </View>
    </View>
  );
};

export { CalenderView, RenderUserNotification };
