// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react";
import {
  View,
  FlatList,
  Text,
  Image,
  Platform,
  StatusBar,
  TouchableOpacity,ScrollView,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  Dimensions,
} from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Header } from "react-native-elements";
import AsyncStorage from "@react-native-community/async-storage";
import { CommonActions } from "@react-navigation/native";
import * as _ from "lodash";

import BaseClass from "../../../utils/BaseClass";
import {
  ShadowViewContainer,
  SafeAreaViewContainer,
  MainContainer,
  ScrollContainer,
} from "../../../utils/BaseStyle";
import { Spacer } from "../../../customComponents/Spacer";
import COLORS from "../../../utils/Colors";
import {
  ListingIcon,
  LocationIcon,
  RightIcon,
  DeleteIcon,
} from "../../../customComponents/icons";
import STRINGS from "../../../utils/Strings";
import { FONT } from "../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../utils/Font";
import styles from "./styles";
import { LeftIcon } from "@customComponents/icons";
import { ICONS } from "../../../utils/ImagePaths";

import moment from "moment";

const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

class ContactUs extends BaseClass {
  _renderHeader() {
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={(<LeftIcon drawer onPress={() => navigation.openDrawer()}/>)}
        centerComponent={{
          text: "Contact Us",
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  supportView = () => {
    return (
      <View>
        <View style={styles.outerView}>
          <Image source={ICONS.mailIcon} style={styles.imgIcon} />
          <Text style={styles.heavyText}>Support Team</Text>
        </View>
        <Text style={styles.grayText}>Supportteam@hotel.com</Text>
      </View>
    );
  };

  viewLine = () => {
    return (
      <View
        style={{ height: 2, width: wp("80%"), backgroundColor: "gray",justifyContent:'center',alignSelf:'center' }}
      ></View>
    );
  };

addressView=()=>{
  return(

    <View>
    <View style={styles.outerView}>
      <Image source={ICONS.iconLocation} style={styles.imgIcon} />
      <Text style={styles.heavyText}>Address</Text>
    </View>
    <Text style={styles.companyName}>Hotel Ap Team Pvt Ltd.</Text>
    <Text style={styles.grayText}>Chandigarh,Punjab India</Text>

  </View>


  )
}

phoneView=()=>{

 return(

  <View>
  <View style={styles.outerView}>
    <Image source={ICONS.phoneIcon} style={styles.imgIcon} />
    <Text style={styles.heavyText}>Phone</Text>
  </View>
  <Text style={styles.grayText}>01720-8435878</Text>

</View>


 )

}


  render() {
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <DismissKeyboard>
          <ScrollView>
            <View style={{backgroundColor:'white'}}>
              <Image
                source={ICONS.ContactUs}
                style={{
                  width: wp("100%"),
                  height: wp("50%"),
                  resizeMode: "cover",
                }}
              />
              <Spacer space={3} />
              {this.supportView()}
              {this.viewLine()}
              <Spacer space={2} />
              {this.addressView()}
              {this.viewLine()}
              <Spacer space={2} />
              {this.phoneView()}
              {this.viewLine()}
            </View>
          </ScrollView>
        </DismissKeyboard>
      </SafeAreaViewContainer>
    );
  }
}

export default ContactUs;
