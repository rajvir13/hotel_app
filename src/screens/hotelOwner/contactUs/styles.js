import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Platform, StyleSheet } from "react-native";
import { FONT } from "../../../utils/FontSizes";
import COLORS from "../../../utils/Colors";
import { FONT_FAMILY } from "../../../utils/Font";

const styles = StyleSheet.create({
  outerView: {
    flexDirection: "row",
    padding: wp("3%"),
    width: wp("90%"),
    marginRight:wp('3%')
    
  },

  imgIcon: {
    width: wp("5%"),
    height: wp("5%"),
    resizeMode: "cover",
  },

  heavyText: {
    fontWeight: "bold",
    color: COLORS.app_theme_color,
    marginLeft: wp("3%"),
    fontSize: wp("4.5%"),
    
  },
  grayText: {
    marginLeft: wp("10%"),
    color: COLORS.greyButton_color2,
    marginBottom: wp("3%"),
  },
  companyName:{
    marginLeft: wp("10%"),
    color: COLORS.greyButton_color2,
    marginBottom: wp("1%"),

  }
  
});

export default styles;
