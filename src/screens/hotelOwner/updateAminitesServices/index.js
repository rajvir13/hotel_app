import React from "react";
import {
    View,
    FlatList,
    Text,
    Image,
    Platform,
    StatusBar,
    Dimensions,
    Button,
    TouchableOpacity,
    TextInput,
    TouchableWithoutFeedback,
    ImageBackground,
    TouchableHighlight,
    Keyboard,
    ScrollView,
    AppRegistry,
    Alert,
} from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {Rating, AirbnbRating, Header, Input} from "react-native-elements";
import {CommonActions} from "@react-navigation/native";
import BaseClass from "../../../utils/BaseClass";
import {
    ShadowViewContainer,
    SafeAreaViewContainer,
    MainContainer,
    ScrollContainer,
} from "../../../utils/BaseStyle";
import {Spacer} from "../../../customComponents/Spacer";
import COLORS from "../../../utils/Colors";
import {
    LeftIcon,
    RightIcon,
    SecondRightIcon,
} from "../../../customComponents/icons";
import STRINGS from "../../../utils/Strings";
import {FONT} from "../../../utils/FontSizes";
import {FONT_FAMILY} from "../../../utils/Font";
import {LogoutAction} from "../../../redux/actions/LogoutAction";
import {ICONS} from "../../../utils/ImagePaths";
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import styles from "./styles";
import {
    CustoemTextComponents,
    EmailTextComponents,
    HotelAddressTextComponents,
} from "../hotelProfile/components/customeTextField";
import ImagePicker from "react-native-image-crop-picker";
import * as _ from "lodash";
import {CheckBox} from "react-native-elements";
import RadioForm from "react-native-simple-radio-button";
import {connect} from "react-redux";
// import {SocialLoginAction} from "../../../../redux/actions/SocialLoginAction"
//==================Dismiss keyboard  =======================
const DismissKeyboard = ({children}) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);

//=================== main class declareation======================
class UpdateAmenities extends BaseClass {
    //===================== constructor================================
    constructor(props) {
        super(props);
        let userLocaleCountryCode = "IN";
        this.state = {
            hotelId: undefined,
            accessToken: undefined,
            checked: [],
            checkedAmenitiesData: [],
            tempOtherService: [
                {label: "Party Halls", value: 0},
                {label: "Bonquets", value: 1},
                {label: "Restaurant", value: 2},
            ],
            amnitiesData: [
                {
                    name: "Personal items",
                    id: 0,
                },
                {
                    name: "food",
                    id: 1,
                },
                {
                    name: "Hair Drayer",
                    id: 2,
                },
                {
                    name: "Travels",
                    id: 3,
                },
                {
                    name: "Draving",
                    id: 4,
                },
                {
                    name: "parking",
                    id: 5,
                },
                {
                    name: "Apple",
                    id: 6,
                },
                {
                    name: "lunch",
                    id: 7,
                },
                {
                    name: "dinner",
                    id: 8,
                },
            ],

            radioButton: [],
            HotelOtherSerivces: [],
            otherService: 1,
            authToken: undefined,
        };
    }

    //============================ render header ===============================
    _renderHeader() {
        const {navigation} = this.props;
        return (
            <Header
                backgroundColor={COLORS.white_color}
                statusBarProps={{
                    translucent: true,
                    backgroundColor: COLORS.black_color,
                }}
                leftComponent={<LeftIcon onPress={() => navigation.goBack()}/>}
                centerComponent={{
                    text: STRINGS.HEADER_OWNER_AMENTIES,
                    style: {
                        color: COLORS.greyButton_color,
                        fontSize: FONT.TextMedium_2,
                        fontFamily: FONT_FAMILY.PoppinsBold
                    },
                }}
                containerStyle={{
                    borderBottomColor: COLORS.greyButton_color,
                    paddingTop: Platform.OS === "ios" ? 0 : 25,
                    height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
                }}
            />
        );
    }

    //====================== amennities View============================
    _renderAmenitiesView = (item, index) => {
        const {checked} = this.state;
        return (
            <View style={styles.renderAmenitiesOuterView}>
                <CheckBox
                    textStyle={styles.checkBoxTextStyle}
                    style={styles.checkboxStyle}
                    title={item.name}
                    containerStyle={styles.checkBoxContainerStyle}
                    onPress={() => this.handleChange(index, item.id)}
                    checked={checked[index]}
                />
            </View>
        );
    };

    //========================= customer loder ==========================
    _renderCustomLoader = () => {
        const {isLoading} = this.state;

        return (
            <OrientationLoadingOverlay
                visible={isLoading}
                message={STRINGS.LOADING_TEXT}
            />
        );
    };

    //******************************* handle change  action */
    handleChange = (index, itemId) => {
        const {amnitiesData, checkedAmenitiesData, checked} = this.state;
        _.map(amnitiesData, (item, cIndex) => {
            if (cIndex === index) {
                // checkedAmenitiesData[cIndex].checked = !checked[index]
            }
        });
        checked[index] = !checked[index];
        this.setState({checked});
    };

    //===============================update Amentites and servicew action===========

    updateAmentites = () => {
        const {
            amnitiesData,
            checkedAmenitiesData,
            checked,
            otherService,
        } = this.state;

        let AminitiesData = [];
        for (let i = 0; i < checked.length; i++) {
            if (checked[i] === true) {
                for (let j = 0; j < amnitiesData.length; j++) {
                    AminitiesData.push(amnitiesData[i].id);
                    break;
                }
            }
        }
    };

    //=======================End Change and update password ========================

    render() {
        const {amnitiesData, tempOtherService} = this.state;
        return (
            <SafeAreaViewContainer>
                {this._renderHeader()}
                <DismissKeyboard>
                    <ScrollContainer>
                        <View style={styles.mainProfileContainer}>
                            <Spacer space={3}/>
                            <Text style={styles.amenetiesTextStyle}>Amenites</Text>
                            <Spacer space={2}/>
                            <View style={styles.MainContainer}>
                                <FlatList
                                    data={amnitiesData}
                                    numColumns={2}
                                    keyExtractor={(item) => item.id}
                                    renderItem={({item, index}) =>
                                        this._renderAmenitiesView(item, index)
                                    }
                                />
                            </View>

                            <Spacer space={2}/>

                            <Text style={styles.otherServiceTextStyle}>Other Services</Text>

                            <View style={styles.radioView}>
                                <ScrollView horizontal={true}>
                                    <RadioForm
                                        style={styles.radioFormStyle}
                                        formHorizontal={true}
                                        labelStyle={styles.radioFormLableStyle}
                                        buttonSize={12}
                                        buttonColor={COLORS.placeholder_color}
                                        radio_props={tempOtherService}
                                        initial={0}
                                        selectedButtonColor={COLORS.radio_color}
                                        onPress={(value) => {
                                            this.setState({otherService: value});
                                        }}
                                    />
                                </ScrollView>
                            </View>
                            <Spacer space={3}/>

                            <TouchableOpacity onPress={() => this.updateAmentites()}>
                                <View style={styles.changeAmenitiesStyle}>
                                    <Text style={styles.updateAmentiesText}>
                                        {STRINGS.UPDATE_PASSWORD}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            <Spacer space={2.5}/>
                            {this._renderCustomLoader()}
                        </View>
                    </ScrollContainer>
                </DismissKeyboard>
            </SafeAreaViewContainer>
        );
    }
}

// const mapStateToProps = state => ({
//   // loginState: state.LoginReducer
// });

// // ----------------------------------------

// const mapDispatchToProps = (dispatch) => {
//   return {
//       loginApi: (payload) => dispatch(LoginAction(payload))
//   };
// };

// ----------------------------------------

// export default connect(mapStateToProps, mapDispatchToProps)(UpdateAmenities);
export default UpdateAmenities;
