import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { StyleSheet, Dimensions } from "react-native";
import { FONT } from "../../../utils/FontSizes";
import COLORS from "../../../utils/Colors";

const window = Dimensions.get("window");

const styles = StyleSheet.create({

    mainProfileContainer: {
        height: hp("100%"),
        alignItems: "center",
        backgroundColor: COLORS.white_color,
      },
    
  changeAmenitiesStyle: {
        marginVertical: 10,
        alignSelf: "center",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: COLORS.app_theme_color,
        height: wp("12%"),
        width: wp("80%"),
        borderRadius: wp("12%") / 2,
        borderColor: COLORS.app_theme_color,
        borderWidth: 2,
      },
      updateAmentiesText: {
        color: COLORS.white_color,
        fontWeight: "700",
        fontSize:wp('4%')
      },

      MainContainer: {
        justifyContent: "center",
        flex: 0.5,
        paddingTop: 10,
       
      },
      renderAmenitiesOuterView:{
        margin: 1, width: wp(45) 

      },
      checkBoxTextStyle:{

        color: COLORS.placeholder_color,fontSize:wp('4%')

      },
      checkBoxContainerStyle:{

        borderColor: COLORS.white_color,
        backgroundColor: COLORS.white_color,

      },
      amenetiesTextStyle:{
        fontWeight: "600",
        marginLeft: wp("4%"),
        fontSize: wp('5%'),
        color: "#554E4E",
        alignSelf: "flex-start",

      },
      otherServiceTextStyle:{
        fontWeight: "600",
        marginLeft: wp("4%"),
        fontSize: wp('5%'),
        color: "#554E4E",
        alignSelf: "flex-start",
        marginTop:wp('5%')

      },
      radioView:{
        height:wp('20%')

      },
      radioFormStyle:{

        alignSelf: "center",marginLeft:wp('4%')
      },
      radioFormLableStyle:{

        marginRight: 10, fontSize: wp('4%')
      }

});
export default styles;