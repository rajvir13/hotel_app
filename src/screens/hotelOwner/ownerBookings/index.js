import React from 'react';
import {Platform, StatusBar, View} from 'react-native';
import {SafeAreaViewContainer, MainContainer, ScrollContainer} from "../../../utils/BaseStyle";
import BaseClass from "../../../utils/BaseClass";
import {Header} from "react-native-elements";
import COLORS from "../../../utils/Colors";
import STRINGS from "../../../utils/Strings";
import {FONT} from "../../../utils/FontSizes";
import {FONT_FAMILY} from "../../../utils/Font";
import OwnerBookingTopTab from "./ownerTopTabBar";
import {LeftIcon} from "../../../customComponents/icons";

class OwnerBooking extends BaseClass {
  
  constructor(props) {
    super(props);
    this.state = {
        notificationData: undefined
    }
}
  componentDidMount() {
    if (this.props.route.params !== null && this.props.route.params !== undefined) {
        if (this.props.route.params.data !== undefined) {
            this.setState({
                notificationData: this.props.route.params.data
            })
        }
    }
}

  //============================= Render Hedaer==========================
  _renderHeader() {
    const { toggleMapView, user_profile } = this.state;
    const {navigation} = this.props;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={(<LeftIcon onPress={() => navigation.goBack()}/>)}
        centerComponent={{
          text: "Bookings",
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

    render() {
        return (
            <SafeAreaViewContainer>
                {this._renderHeader()}
                <View style={{backgroundColor: COLORS.backGround_color, flex: 1}}>
                    <OwnerBookingTopTab data={this.state.notificationData}  navigationInfo={this.props} />
                </View>
            </SafeAreaViewContainer>
        )
    }
}

export default OwnerBooking;
