import React, {useEffect, useState, useCallback, useRef} from "react";
import {
    Animated,
    Dimensions,
    StyleSheet,
    SafeAreaView,
    View,
} from "react-native";
import _ from "lodash";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {TabView, SceneMap, TabBar} from "react-native-tab-view";

import COLORS from "../../../../utils/Colors";
import TabTipSVG from "../../../../../assets/images/TabView/TabTipSVG";
import {FONT} from "../../../../utils/FontSizes";
import {FONT_FAMILY} from "../../../../utils/Font";
import PendingConfirmation from "../tabBarScreens/PendingConfirmation";
import OnGoingBookings from "../tabBarScreens/onGoingBookings";
import BookingCancelByOwner from "../tabBarScreens/BookingCancelByOwner";
import CheckoutByOwner from "../tabBarScreens/CheckOut";

// const FirstRoute = () => (
//   <View style={[styles.container]}>
//     <PendingConfirmation />
//   </View>
// );
// const SecondRoute = () => (
//   <View style={[styles.container]}>
//     <OnGoingBookings />
//   </View>
// );
// const ThirdRoute = () => (
//   <View style={[styles.container]}>
//     <BookingCancelByOwner />
//   </View>
// );
// const FourthRoute = () => (
//   <View style={[styles.container]}>
//     <CheckoutByOwner />
//   </View>
// );

const TabIndicator = ({width, tabWidth, index}) => {
    const marginLeftRef = useRef(new Animated.Value(index ? tabWidth : 0))
        .current;
    useEffect(() => {
        Animated.timing(marginLeftRef, {
            toValue: tabWidth,
            duration: 400,
        }).start();
    }, [tabWidth]);

    return (
        <Animated.View
            style={{
                justifyContent: "flex-end",
                alignItems: "center",
                flex: 1,
                width: width,
                marginLeft: marginLeftRef,
                position: "absolute",
                bottom: -12,
            }}
        >
            <TabTipSVG fillColor={COLORS.app_theme_color}/>
        </Animated.View>
    );
};

const OwnerBookingTopTab = ({data,navigationInfo}) => {
    const [index, setIndex] = useState(0);
    const routes = [
        {key: "first", title: "Pending Confirmation"},
        {key: "second", title: "Booked"},
        {key: "third", title: "Cancelled"},
        {key: "fourth", title: "Check out "},
    ];

    // const renderScene = ({ route }) => {
    //   switch (route.key) {
    //     case "first":
    //       return <FirstRoute />;
    //     case "second":
    //       return <SecondRoute />;
    //     case "third":
    //       return <ThirdRoute />;
    //     case "fourth":
    //       return <FourthRoute />;
    //     default:
    //       return null;
    //   }
    // };

    const renderIndicator = useCallback(
        ({getTabWidth}) => {
            const tabWidth = _.sum(
                [...Array(index).keys()].map((i) => getTabWidth(i))
            );

            return (
                <TabIndicator
                    width={getTabWidth(index)}
                    tabWidth={tabWidth}
                    index={index}
                />
            );
        },
        [index]
    );

    return (
        <View style={styles.container}>
            <TabView
                lazy={true}
                navigationState={{
                    index,
                    routes,
                }}
                renderScene={({route}) => {
                    switch (route.key) {
                        case "first":
                            return <PendingConfirmation routeKey={index} notificationData={data} navigationInfo={navigationInfo}/>;
                        case "second":
                            return <OnGoingBookings routeKey={index} notificationData={data} navigationInfo={navigationInfo}/>;
                        case "third":
                            return <BookingCancelByOwner routeKey={index} notificationData={data} navigationInfo={navigationInfo}/>;
                        case "fourth":
                            return <CheckoutByOwner routeKey={index} notificationData={data} navigationInfo={navigationInfo}/>;
                        default:
                            return null;
                    }
                }}
                onIndexChange={setIndex}
                renderTabBar={(props) => (
                    <TabBar
                        {...props}
                        scrollEnabled
                        tabStyle={styles.tabBar}
                        // indicatorStyle={styles.indicator}
                        style={styles.tabBarContainer}
                        labelStyle={styles.labelStyle}
                        renderIndicator={renderIndicator}
                    />
                )}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    tabBar: {
        // width: wp(100) / 3,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: COLORS.app_theme_color,
        marginHorizontal: wp(0.3),
    },
    indicator: {
        // backgroundColor: "#ccc"
        backgroundColor: "red",
    },
    tabBarContainer: {
        backgroundColor: COLORS.white_color,
        marginBottom: wp(3),
    },
    labelStyle: {
        marginVertical: 0,
        fontSize: FONT.TextExtraSmall,
        fontFamily: FONT_FAMILY.MontserratSemiBold,
        textAlign: "center",
        // backgroundColor: "#fff",
        color: COLORS.white_color,
    },
});

export default OwnerBookingTopTab;
