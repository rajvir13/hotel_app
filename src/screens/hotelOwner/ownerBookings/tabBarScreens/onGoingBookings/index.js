import React from "react";
import { FlatList, Image, Text, TouchableOpacity, View } from "react-native";
import {
  MainContainer,
  ShadowViewContainer,
  ScrollContainer,
} from "../../../../../utils/BaseStyle";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLORS from "../../../../../utils/Colors";
import { FONT } from "../../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../../utils/Font";
import { Dropdown } from "react-native-material-dropdown";
import { ICONS } from "../../../../../utils/ImagePaths";
import BaseClass from "../../../../../utils/BaseClass";
import STRINGS from "../../../../../utils/Strings";
import { Spacer } from "../../../../../customComponents/Spacer";
import { Data } from "../../../../../utils/FixtureData";
import FastImage from "react-native-fast-image";
import CancelBooking from "./CancelBooking";
import AsyncStorage from "@react-native-community/async-storage";
import {
  OwnerBookedData,
  OwnerCheckingOutBooking,
} from "../../../../../redux/actions/OwnerPendingConfirmation";
import { API } from "../../../../../redux/constant";
import { CustomeMonthYearPicker } from "../../../../../customComponents/CustomeMonthYearPicker";
import moment from "moment";
import propTypes from "prop-types";
import { OwnerCancelPaindingBooking } from "../../../../../redux/actions/OwnerCancelPaindingBooking";
import RoomView from "../../../../../customComponents/roomView/index";
import * as _ from "lodash";

import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from "react-native-popup-menu";

let checkVar = false;

class OnGoingBookings extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      accessToken: undefined,
      bookData: [],
      isModalVisible: false,
      itemValue: {},
      bookDataCount: 0,
      customerLoginId: undefined,
      selectdMonthText: "Select Month",
      isMonthYearPickerVisible: false,
      isRoomViewVisible: false,
      userName: undefined,
      filterArr: [
        { value: "Single", isChecked: false },
        { value: "Double", isChecked: false },
        { value: "Triple", isChecked: false },
        { value: "Quad", isChecked: false },
        { value: "Twin", isChecked: false },
        { value: "Day Slot", isChecked: false },
        { value: "All", isChecked: false },
      ],
      filteredListArray: [],
    };
  }

  static propTypes = {
    routeKey: propTypes.number,
  };

  componentDidMount() {
    this.hitApi();
  }

  componentDidUpdate(prevProps, preState, snapShot) {
    if (this.props.routeKey === 1) {
      if (checkVar === true) {
        checkVar = false;
        this.hitApi();
      }
    } else {
      checkVar = true;
      // } else if (this.props.routeKey === 2) {
      //     checkVar = 2;
      // } else if (this.props.routeKey === 3) {
      //     checkVar = 3;
    }
  }

  hitApi = () => {
    const { filterArr } = this.state;
    filterArr.map((element) => (element.isChecked = false));
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        // const { hotel_details } = loginData;

        if (loginData.hotel_details !== undefined) {
          this.setState({
            accessToken: loginData.access_token,
            customerLoginId: loginData.hotel_details.owner,
            selectdMonthText: "Select Month",
            filterArr: filterArr,
          });
          this.bookedData(
            loginData.hotel_details.owner,
            loginData.access_token,
            "",
            ""
          );
        } else {
          this.setState({
            accessToken: loginData.access_token,
            customerLoginId: loginData.owner.id,
            selectdMonthText: "Select Month",
            filterArr: filterArr,
          });
          this.bookedData(loginData.owner.id, loginData.access_token, "", "");
        }
      }
    });
  };

  bookedData = (id, accessToken, monthSelected, yearSelected) => {
    this.showDialog();
    OwnerBookedData(
      {
        accessToken: accessToken,
        ownerId: id,
        month: monthSelected,
        year: yearSelected,
      },
      (bookedData) => this.bookResponseData(bookedData)
    );
  };

  bookResponseData = (bookResponse) => {
    console.warn("booking res is", bookResponse);
    this.hideDialog();
    const { response, status, message } = bookResponse;
    if (bookResponse === "Network request failed") {
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (response !== undefined) {
        if (status === 200) {
          this.setState({
            bookData: response,
            bookDataCount: response.length,
            filteredListArray: _.cloneDeep(response),
          });
        } else if (status === 111) {
          this.showToastAlert(message);
        } else if (status === 401) {
          this.showToastAlert(message);
        } else {
          this.showToastAlert(message);
        }
      } else if (message.non_field_errors !== undefined) {
        this.showToastAlert(message.non_field_errors[0]);
      } else {
        this.showToastAlert(message);
      }
      this.hideDialog();
    }
  };

  onModalCancelPress = (text, value) => {
    const { accessToken, customerLoginId } = this.state;
    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();

    OwnerCancelPaindingBooking(
      {
        accessToken: accessToken,
        bookingId: value.id,
        cancelled_by: customerLoginId,
        cancelled_by_date: year + "-" + month + "-" + date,
      },
      (data) => this.getOwnerCancelRequestResponse(data)
    );
  };
  getOwnerCancelRequestResponse = (data) => {
    const { customerLoginId, accessToken } = this.state;
    const { response, status, message } = data;
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (response !== undefined) {
        if (status === 200) {
          this.showToastSucess(message);
          this.bookedData(customerLoginId, accessToken, "", "");
        } else if (status === 111) {
          this.hideDialog();
          this.showToastAlert(message);
        } else if (status === 401) {
          this.hideDialog();
          this.showToastAlert(message);
        } else {
          this.hideDialog();
          this.showToastAlert(message);
        }
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      } else {
        this.hideDialog();
        this.showToastAlert(message);
      }
      this.hideDialog();
    }
  };

  onModalCheckoutPress = (text, value) => {
    const { accessToken } = this.state;
    OwnerCheckingOutBooking(
      {
        accessToken: accessToken,
        bookingId: value.id,
      },
      (canceledData) => this.bookingCanceled(canceledData)
    );
  };

  bookingCanceled = (canceledData) => {
    const { customerLoginId, accessToken } = this.state;
    const { response, status, message } = canceledData;
    this.hideDialog();
    if (canceledData === "Network request failed") {
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (response !== undefined) {
        if (status === 200) {
          this.bookedData(customerLoginId, accessToken, "", "");
        } else if (status === 111) {
          this.showToastAlert(message);
        } else if (status === 401) {
          this.showToastAlert(message);
        } else {
          this.showToastAlert(message);
        }
      } else if (message.non_field_errors !== undefined) {
        this.showToastAlert(message.non_field_errors[0]);
      } else {
        this.showToastAlert(message);
      }
    }
  };

  monthYearViewClick = () => {
    this.setState({ isMonthYearPickerVisible: true });
  };

  newMonthYearSelected = (monthYear) => {
    const {
      accessToken,
      customerLoginId,
      isMonthYearPickerVisible,
    } = this.state;
    var monthYearData = moment(monthYear).format("MM-YYYY");

    var selectedmonth = monthYearData.toString().slice(0, 2);
    var selectedyear = monthYearData.toString().slice(3, 7);

    this.setState({
      isMonthYearPickerVisible: !isMonthYearPickerVisible,
      selectdMonthText: selectedmonth + "/" + selectedyear,
    });

    this.bookedData(customerLoginId, accessToken, selectedmonth, selectedyear);
  };

  closeMonthYearPicker = () => {
    this.setState({ isMonthYearPickerVisible: false });
  };
  onMenuPress = (index) => {
    const { filterArr } = this.state;
    this.setState({
      filterArr: _.map(filterArr, (data, arrIndex) => {
        if (index === arrIndex) {
          // data.value = !data.value;
          console.log("data", data);
          data.isChecked = true;
          return data;
        } else {
          data.isChecked = false;
          return data;
        }
      }),
    });

    this.appliedFiltersAction(index);
  };

  appliedFiltersAction = (index) => {
    filterIndex = index;
    const { bookData, filteredListArray } = this.state;
    let tempArr = [];
    if (filteredListArray.length > 0) {
      switch (index) {
        case 0:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Single"
          );
          break;
        case 1:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Double"
          );

          break;
        case 2:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Triple"
          );

          break;
        case 3:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Quad"
          );
          break;
        case 4:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Twin"
          );
          break;
        case 5:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Day Slot"
          );
          break;
        case 6:
          tempArr = filteredListArray;
          break;
        default:
          break;
      }
      this.setState({
        bookData: tempArr,
      });
    }
  };

  getAdditionalDiscount = (regularPrice) => {
    return (Number(regularPrice) * 40) / 100;
  };

  bookingFilter = () => {
    const { filterArr } = this.state;
    return (
      <View style={{ marginRight: wp("3%") }}>
        <Menu>
          <MenuTrigger>
            <View style={{ flexDirection: "row" }}>
              <Text style={{ marginRight: 10 }}>Filters</Text>
              <Image source={ICONS.FILTERS_ICON} />
            </View>
          </MenuTrigger>
          <MenuOptions>
            <MenuOption>
              {_.map(filterArr, (item1, index) => {
                return (
                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: wp("2%"),
                      marginBottom: wp("2%"),
                    }}
                  >
                    <TouchableOpacity onPress={() => this.onMenuPress(index)}>
                      <FastImage
                        resizeMode={FastImage.resizeMode.contain}
                        style={{
                          borderRadius: wp(1),
                          borderColor: COLORS.black_color,
                          borderWidth: wp(0.2),
                          width: wp(4.5),
                          height: wp(4.5),
                          justifyContent: "center",
                        }}
                        source={
                          item1.isChecked
                            ? ICONS.CHECKED_ICON
                            : ICONS.UNCHECKED_ICON
                        }
                      />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onMenuPress(index)}>
                      <Text
                        style={{
                          justifyContent: "center",

                          color: COLORS.black_color,
                          fontFamily: FONT_FAMILY.Montserrat,
                          fontSize: FONT.TextMedium,
                          alignSelf: "center",
                          marginLeft: wp("2%"),
                          marginRight: wp("2%"),
                        }}
                      >
                        {item1.value}
                      </Text>
                    </TouchableOpacity>
                  </View>
                );
              })}
            </MenuOption>
          </MenuOptions>
        </Menu>
      </View>
    );
  };
  render() {
    const {
      bookData,
      bookDataCount,
      isModalVisible,
      isMonthYearPickerVisible,
      itemValue,
      selectdMonthText,
      isRoomViewVisible,
    } = this.state;
    return (
      <View style={{ backgroundColor: COLORS.backGround_color, flex: 1 }}>
        <View
          style={{
            width: wp(100),
            flexDirection: "row",
            paddingVertical: wp(3),
            backgroundColor: COLORS.light_grey2,
            alignItems: "center",
            justifyContent: "space-around",
          }}
        >
          <Text
            numberOfLines={1}
            style={{
              color: COLORS.placeholder_color,
              fontSize: FONT.TextSmall,
              fontFamily: FONT_FAMILY.Montserrat,
              width: wp(38),
            }}
          >
            Total Booking {bookDataCount}
          </Text>

          <View
            style={{
              backgroundColor: COLORS.white_color,
              borderRadius: wp(4),
              width: wp("30%"),
              height: wp("8%"),
              justifyContent: "center",
            }}
          >
            <TouchableOpacity onPress={() => this.monthYearViewClick()}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-evenly",
                }}
              >
                <Text style={{ color: COLORS.greyButton_color2 }}>
                  {selectdMonthText}
                </Text>
                <Image
                  style={{ height: 15, width: 15, alignSelf: "center" }}
                  source={ICONS.DOWN_ARROW}
                />
              </View>
            </TouchableOpacity>
          </View>

          {this.bookingFilter()}
        </View>
        {isMonthYearPickerVisible === true && (
          <CustomeMonthYearPicker
            isMonthYearPickerVisible={isMonthYearPickerVisible}
            newDateMonthSelected={(date) => this.newMonthYearSelected(date)}
            crossPickerAction={() => this.closeMonthYearPicker()}
          />
        )}
        <Spacer space={3} />
        {bookData.length !== 0 ? (
          <FlatList
            extraData={this.state}
            data={bookData}
            keyExtractor={(item, index) => index}
            renderItem={({ item, index }) => (
              <View style={{ width: wp(100) }}>
                <ShadowViewContainer>
                  <View style={{ flexDirection: "row", width: wp(90) }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          isRoomViewVisible: true,
                          itemValue: item,
                        });
                      }}
                    >
                      <FastImage
                        style={{
                          width: wp(31),
                          height: wp(40),
                        }}
                        source={{
                          uri:
                            item.room_images !== null &&
                            item.room_images.image !== null
                              ? API.IMAGE_BASE_URL + item.room_images.image
                              : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                        }}
                      />
                    </TouchableOpacity>
                    <Spacer row={1} />
                    <View style={{ width: wp(57), justifyContent: "center" }}>
                      <Spacer space={1} />
                      <View style={{ flexDirection: "row" }}>
                        <Text
                          numberOfLines={2}
                          style={{
                            width: wp(37),
                            color: COLORS.black_color,
                            fontSize: FONT.TextSmall_2,
                            fontFamily: FONT_FAMILY.Poppins,
                          }}
                        >
                          Booked by {item.customer_name}{" "}
                        </Text>
                        <Spacer row={0.5} />
                        {item.no_of_hours !== null &&
                        item.no_of_hours !== undefined ? (
                          <Text
                            numberOfLines={2}
                            style={{
                              textAlign: "center",
                              width: wp(18),
                              color: COLORS.green_color,
                              fontSize: FONT.TextExtraSmall,
                              fontFamily: FONT_FAMILY.MontserratSemiBold,
                            }}
                          >
                            Stay {item.no_of_hours} hr
                          </Text>
                        ) : (
                          <Text
                            numberOfLines={2}
                            style={{
                              textAlign: "center",
                              width: wp(18),
                              color: COLORS.green_color,
                              fontSize: FONT.TextExtraSmall,
                              fontFamily: FONT_FAMILY.MontserratSemiBold,
                            }}
                          >
                            Stay {item.no_of_days} days
                          </Text>
                        )}
                      </View>
                      <Spacer space={0.5} />
                      <Text
                        numberOfLines={1}
                        style={{
                          color: COLORS.black_color,
                          fontSize: FONT.TextSmall_2,
                          fontFamily: FONT_FAMILY.Montserrat,
                        }}
                      >
                        Date {item.booking_created}{" "}
                      </Text>
                      <Spacer space={1} />
                      <Text
                        numberOfLines={1}
                        style={{
                          color: COLORS.black_color,
                          fontSize: FONT.TextSmall_2,
                          fontFamily: FONT_FAMILY.Montserrat,
                        }}
                      >
                        Room Type {item.room_type}{" "}
                      </Text>
                      <Spacer space={1} />
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between",
                        }}
                      >
                        <Text
                          numberOfLines={1}
                          style={{
                            fontSize: FONT.TextSmall_2,
                            fontFamily: FONT_FAMILY.MontserratSemiBold,
                          }}
                        >
                          Price{" "}
                          {item.is_special === true ? (
                            <Text style={{ color: COLORS.customerType }}>
                              {"\u20B9"}
                              {this.getAdditionalDiscount(
                                item.room_regular_price
                              )}
                            </Text>
                          ) : (
                            <Text style={{ color: COLORS.customerType }}>
                              {"\u20B9"}
                              {item.room_price}
                            </Text>
                          )}
                        </Text>
                        <TouchableOpacity
                          style={{
                            marginRight: wp(2),
                            backgroundColor: COLORS.customerType,
                            borderRadius: wp(2),
                          }}
                          onPress={() =>
                            this.setState({
                              isModalVisible: true,
                              itemValue: item,
                            })
                          }
                        >
                          <Text
                            numberOfLines={1}
                            style={{
                              color: COLORS.white_color,
                              fontSize: FONT.TextSmall_2,
                              fontFamily: FONT_FAMILY.Poppins,
                              paddingHorizontal: wp(5),
                              paddingVertical: wp(1),
                            }}
                          >
                            Cancel
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </ShadowViewContainer>
                <Spacer space={0} />
              </View>
            )}
          />
        ) : (
          <View>
            <Text
              style={{
                textAlign: "center",
                fontSize: FONT.TextNormal,
                fontFamily: FONT_FAMILY.MontserratBold,
              }}
            >
              NO DATA{" "}
            </Text>
          </View>
        )}

        {isRoomViewVisible === true && (
          <RoomView
            isRoomViewVisible={isRoomViewVisible}
            value={itemValue}
            comesFromBooking={true}
            closeModal={() =>
              this.setState({ isRoomViewVisible: !isRoomViewVisible })
            }
          />
        )}

        <CancelBooking
          isModalVisible={isModalVisible}
          value={itemValue}
          closeModal={() => this.setState({ isModalVisible: !isModalVisible })}
          onCancelPress={(text, value) => this.onModalCancelPress(text, value)}
          onCheckOutPress={(text, value) =>
            this.onModalCheckoutPress(text, value)
          }
        />
      </View>
    );
  }
}

export default OnGoingBookings;
