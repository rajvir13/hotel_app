import React from 'react';
import {
    Modal,
    StyleSheet,
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    Platform,
    TouchableWithoutFeedback, Keyboard
} from 'react-native';
import propTypes from "prop-types";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import COLORS from "../../../../../../utils/Colors";
import {FONT} from "../../../../../../utils/FontSizes";
import {FONT_FAMILY} from "../../../../../../utils/Font";
import FastImage from "react-native-fast-image";
import {Spacer} from "../../../../../../customComponents/Spacer";
import CrossComponent from "../../../../../../../assets/images/BookingIcons/CrossSVG";
import STRINGS from "../../../../../../utils/Strings";
import BaseClass from "../../../../../../utils/BaseClass";
import {API} from "../../../../../../redux/constant";

export default class CancelBooking extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            modalVisiblePass: false,
            value: '',
            indexValue: 0,
            isDatePickerVisible: false,
            dateSelected: 'Check In Date',
            stayingDays: '',
            confirmationCode: ''
        }
    }

    static propTypes = {
        isModalVisible: propTypes.any,
        onHotelClick: propTypes.func,
        value: propTypes.string,
        closeModal: propTypes.func,
        indexRoom: propTypes.number,
        onCancelPress: propTypes.func,
        onCheckOutPress: propTypes.func
    };

    closeModal = () => {
        this.props.closeModal();
    };

    onCancelClick = () => {
        this.props.closeModal();
        this.props.onCancelPress("CancelPress", this.props.value);
    };

    onCheckOutClick = () => {
        this.props.closeModal();
        this.props.onCheckOutPress("CheckOutPress", this.props.value);
    };

    render() {
        const {value} = this.props;
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={this.props.isModalVisible}>

                <KeyboardAvoidingView
                    style={{flex: 1}}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                    <TouchableWithoutFeedback
                        onPress={() => {
                            Keyboard.dismiss()
                        }}>
                        <View style={styles.centeredView}>
                            <View style={{backgroundColor: 'transparent', alignItems: 'center', width: wp(98)}}>
                                <View style={{
                                    backgroundColor: COLORS.white_color,
                                    width: wp(93),
                                    borderRadius: wp(5)
                                }}>
                                    <View style={{
                                        alignItems: 'center',
                                        paddingVertical: wp(4),
                                        backgroundColor: COLORS.app_theme_color,
                                        justifyContent: 'flex-start'
                                    }}>
                                        <Text style={{
                                            fontSize: FONT.TextMedium_2,
                                            fontFamily: FONT_FAMILY.MontserratSemiBold,
                                            color: COLORS.white_color
                                        }}>Cancel Booking
                                        </Text>
                                    </View>
                                   {Object.keys(value).length !== 0 && <View style={{
                                        flexDirection: 'row',
                                        paddingVertical: wp(4),
                                        paddingHorizontal: wp(2),
                                        backgroundColor: COLORS.white_color, width: wp(93)
                                    }}>
                                      
                                        <FastImage
                                            style={{
                                                borderRadius: wp(3),
                                                width: wp(43),
                                                height: wp(40),
                                            }}
                                            source={{uri: (value.hotel_images !== null) ? API.IMAGE_BASE_URL + value.hotel_images.image : 'https://travelji.com/wp-content/uploads/Hotel-Tips.jpg'}}/>
                                        <Spacer row={1}/>
                                        <View style={{width: wp(45), justifyContent: 'center'}}>
                                            <Text numberOfLines={2} style={{
                                                color: COLORS.black_color,
                                                fontSize: FONT.TextSmall,
                                                fontFamily: FONT_FAMILY.Poppins
                                            }}>Booked by {value.customer_name}</Text>
                                            <Spacer space={1}/>
                                            <Text numberOfLines={1} style={{
                                                color: COLORS.black_color,
                                                fontSize: FONT.TextSmall_2,
                                                fontFamily: FONT_FAMILY.Montserrat
                                            }}>Date {value.booking_created} </Text>
                                            <Spacer space={1}/>
                                            <Text numberOfLines={1} style={{
                                                color: COLORS.black_color,
                                                fontSize: FONT.TextSmall_2,
                                                fontFamily: FONT_FAMILY.Montserrat
                                            }}>Room Type {value.room_type} </Text>
                                            <Spacer space={1}/>
                                            <Text numberOfLines={1} style={{
                                                fontSize: FONT.TextSmall_2,
                                                fontFamily: FONT_FAMILY.MontserratSemiBold
                                            }}>Price <Text
                                                style={{color: COLORS.customerType}}>{'\u20B9'}{value.room_price}</Text> </Text>
                                        </View>
                                    </View>}
                                    <Spacer space={3}/>
                                    <View style={{flexDirection: 'row', alignSelf: 'center'}}>
                                        <TouchableOpacity style={{
                                            borderRadius: wp(2),
                                            alignSelf: 'center',
                                            width: wp(40),
                                            paddingVertical: wp(3),
                                            backgroundColor: COLORS.customerType
                                        }} onPress={() => {
                                            this.onCancelClick()
                                        }}>
                                            <Text style={{
                                                textAlign: 'center',
                                                color: COLORS.white_color,
                                                fontSize: FONT.TextSmall,
                                                fontFamily: FONT_FAMILY.MontserratSemiBold
                                            }}>Cancel Booking</Text>
                                        </TouchableOpacity>
                                        <Spacer row={2}/>
                                        <TouchableOpacity style={{
                                            borderRadius: wp(2),
                                            alignSelf: 'center',
                                            width: wp(40),
                                            paddingVertical: wp(3),
                                            backgroundColor: COLORS.green_color
                                        }} onPress={() => {
                                            this.onCheckOutClick()
                                        }}>
                                            <Text style={{
                                                textAlign: 'center',
                                                color: COLORS.white_color,
                                                fontSize: FONT.TextSmall,
                                                fontFamily: FONT_FAMILY.MontserratSemiBold
                                            }}>Check Out</Text>
                                        </TouchableOpacity>
                                    </View>
                                    <Spacer space={4}/>
                                </View>
                                <TouchableOpacity
                                    hitSlop={{top: wp(5), left: wp(5), bottom: wp(5)}}
                                    style={{
                                        position: 'absolute',
                                        alignSelf: 'flex-end',
                                        marginTop: wp(-3),
                                    }}
                                    onPress={() => {
                                        this.closeModal()
                                    }}>
                                    <CrossComponent width={wp(9)} height={wp(9)}/>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </Modal>
        );
    }
}

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'rgba(0,0,0,0.6)'
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    }
});
