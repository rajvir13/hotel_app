import React from "react";
import { StyleSheet } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLORS from "../../../../../utils/Colors";
import { FONT } from "../../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../../utils/Font";

const styles = StyleSheet.create({
  mainView: {
    width: wp(100),
    flexDirection: "row",
    paddingVertical: wp(3),
    backgroundColor: COLORS.light_grey2,
    alignItems: "center",
    justifyContent: "flex-end",
    paddingRight: wp(3),
  },
  outerView: {
    backgroundColor: COLORS.white_color,
    borderRadius: wp(4),
    width: wp("30%"),
    height: wp("8%"),
    justifyContent: "center",
  },
  touchableStyle: {
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  downArrowImage: {
    height: 15,
    width: 15,
    alignSelf: "center",
  },
  bookedBytext: {
    color: COLORS.black_color,
    fontSize: FONT.TextSmall,
    fontFamily: FONT_FAMILY.Poppins,
  },
  dateText: {
    color: COLORS.black_color,
    fontSize: FONT.TextSmall_2,
    fontFamily: FONT_FAMILY.Montserrat,
  },
  roomTypeText: {
    color: COLORS.black_color,
    fontSize: FONT.TextSmall_2,
    fontFamily: FONT_FAMILY.Montserrat,
  },
  priceOuterView: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  priceText: {
    fontSize: FONT.TextSmall_2,
    fontFamily: FONT_FAMILY.MontserratSemiBold,
  },
  touchRoomView:{
    width: wp(25),
    marginRight: wp(2),
    backgroundColor: COLORS.app_theme_color,
    borderRadius: wp(2),
    marginBottom:wp('1.5%')

  },
  roomViewText:{
    textAlign: "center",
    color: COLORS.white_color,
    fontSize: FONT.TextExtraSmall,
    fontFamily: FONT_FAMILY.Poppins,
    // paddingHorizontal: wp(5),
    paddingVertical: wp(1),

  },
  noDataStyle:{

    textAlign: "center",
    fontSize: FONT.TextNormal,
    fontFamily: FONT_FAMILY.MontserratBold,
  }
});

export { styles };
