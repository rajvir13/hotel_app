import React from "react";
import { FlatList, Image, Text, TouchableOpacity, View } from "react-native";
import { ShadowViewContainer } from "../../../../../utils/BaseStyle";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLORS from "../../../../../utils/Colors";
import { FONT } from "../../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../../utils/Font";
import { ICONS } from "../../../../../utils/ImagePaths";
import BaseClass from "../../../../../utils/BaseClass";
import STRINGS from "../../../../../utils/Strings";
import { Spacer } from "../../../../../customComponents/Spacer";
import FastImage from "react-native-fast-image";
import AsyncStorage from "@react-native-community/async-storage";
import { OwnerCheckOutData } from "../../../../../redux/actions/OwnerPendingConfirmation";
import propTypes from "prop-types";
import { API } from "../../../../../redux/constant";
import { CustomeMonthYearPicker } from "../../../../../customComponents/CustomeMonthYearPicker";
import moment from "moment";
import OrientationLoadingOverlay from "@customComponents/Loader";
import { styles } from "./styles";
import RoomView from "../../../../../customComponents/roomView/index";
import * as _ from "lodash";
import {
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from "react-native-popup-menu";

let checkVar = false;

class CheckoutByOwner extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      checkoutData: [],
      selectdMonthText: "Select Month",
      isMonthYearPickerVisible: false,
      accessToken: undefined,
      customerLoginId: undefined,
      isRoomViewVisible: false,
      itemValue: {},
      filterArr: [
        { value: "Single", isChecked: false },
        { value: "Double", isChecked: false },
        { value: "Triple", isChecked: false },
        { value: "Quad", isChecked: false },
        { value: "Twin", isChecked: false },
        { value: "Day Slot", isChecked: false },
        { value: "All", isChecked: false },
      ],
      filteredListArray: [],
    };
  }

  static propTypes = {
    routeKey: propTypes.number,
  };

  componentDidMount() {
    this.hitApi();
  }

  componentDidUpdate(prevProps, preState, snapShot) {
    if (this.props.routeKey === 2) {
      if (checkVar === true) {
        checkVar = false;
        this.hitApi();
      }
    } else {
      checkVar = true;
    }
  }

  hitApi = () => {
    const { filterArr } = this.state;
    filterArr.map((element) => (element.isChecked = false));
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        // const { hotel_details } = loginData;

        if (loginData.hotel_details !== undefined) {
          this.setState({
            accessToken: loginData.access_token,
            customerLoginId: loginData.hotel_details.owner,
            selectdMonthText: "Select Month",
            filterArr: filterArr,
          });

          this.ownerCheckOutData(
            loginData.hotel_details.owner,
            loginData.access_token,
            "",
            ""
          );
        } else {
          this.setState({
            accessToken: loginData.access_token,
            customerLoginId: loginData.owner.id,
            selectdMonthText: "Select Month",
            filterArr: filterArr,
          });

          this.ownerCheckOutData(
            loginData.owner.id,
            loginData.access_token,
            "",
            ""
          );
        }
      }
    });
  };

  ownerCheckOutData = (id, accessToken, monthSelected, yearSelected) => {
    this.showDialog();
    OwnerCheckOutData(
      {
        accessToken: accessToken,
        ownerId: id,
        month: monthSelected,
        year: yearSelected,
      },
      (checkOutData) => this.checkoutResponseData(checkOutData)
    );
  };

  checkoutResponseData = (checkOutData) => {
    const { response, status, message } = checkOutData;
    this.hideDialog();
    if (checkOutData === "Network request failed") {
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (response !== undefined) {
        if (status === 200) {
          this.setState({
            checkoutData: response,
            filteredListArray: _.cloneDeep(response),
          });
        } else if (status === 111) {
          this.showToastAlert(message);
        } else if (status === 401) {
          this.showToastAlert(message);
        } else {
          this.showToastAlert(message);
        }
      } else if (message.non_field_errors !== undefined) {
        this.showToastAlert(message.non_field_errors[0]);
      } else {
        this.showToastAlert(message);
      }
    }
  };

  monthYearViewClick = () => {
    this.setState({ isMonthYearPickerVisible: true });
  };

  newMonthYearSelected = (monthYear) => {
    const {
      accessToken,
      customerLoginId,
      isMonthYearPickerVisible,
    } = this.state;
    let monthYearData = moment(monthYear).format("MM-YYYY");

    let selectedmonth = monthYearData.toString().slice(0, 2);
    let selectedyear = monthYearData.toString().slice(3, 7);

    this.setState({
      isMonthYearPickerVisible: !isMonthYearPickerVisible,
      selectdMonthText: selectedmonth + "/" + selectedyear,
    });

    this.ownerCheckOutData(
      customerLoginId,
      accessToken,
      selectedmonth,
      selectedyear
    );
  };

  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  roomViewClicked = (item) => {
    this.setState({
      isRoomViewVisible: true,
      itemValue: item,
    });
  };

  closeMonthYearPicker = () => {
    this.setState({ isMonthYearPickerVisible: false });
  };

  bookingFilter = () => {
    const { filterArr } = this.state;
    return (
      <View style={{ marginRight: wp("3%") }}>
        <Menu>
          <MenuTrigger>
            <View style={{ flexDirection: "row" }}>
              <Text style={{ marginRight: 10 }}>Filters</Text>
              <Image source={ICONS.FILTERS_ICON} />
            </View>
          </MenuTrigger>
          <MenuOptions>
            <MenuOption>
              {_.map(filterArr, (item1, index) => {
                return (
                  <View
                    style={{
                      flexDirection: "row",
                      marginTop: wp("2%"),
                      marginBottom: wp("2%"),
                    }}
                  >
                    <TouchableOpacity onPress={() => this.onMenuPress(index)}>
                      <FastImage
                        resizeMode={FastImage.resizeMode.contain}
                        style={{
                          borderRadius: wp(1),
                          borderColor: COLORS.black_color,
                          borderWidth: wp(0.2),
                          width: wp(4.5),
                          height: wp(4.5),
                          justifyContent: "center",
                        }}
                        source={
                          item1.isChecked
                            ? ICONS.CHECKED_ICON
                            : ICONS.UNCHECKED_ICON
                        }
                      />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => this.onMenuPress(index)}>
                      <Text
                        style={{
                          justifyContent: "center",

                          color: COLORS.black_color,
                          fontFamily: FONT_FAMILY.Montserrat,
                          fontSize: FONT.TextMedium,
                          alignSelf: "center",
                          marginLeft: wp("2%"),
                          marginRight: wp("2%"),
                        }}
                      >
                        {item1.value}
                      </Text>
                    </TouchableOpacity>
                  </View>
                );
              })}
            </MenuOption>
          </MenuOptions>
        </Menu>
      </View>
    );
  };
  onMenuPress = (index) => {
    const { filterArr } = this.state;
    this.setState({
      filterArr: _.map(filterArr, (data, arrIndex) => {
        if (index === arrIndex) {
          // data.value = !data.value;
          console.log("data", data);
          data.isChecked = true;
          return data;
        } else {
          data.isChecked = false;
          return data;
        }
      }),
    });

    this.appliedFiltersAction(index);
  };

  appliedFiltersAction = (index) => {
    filterIndex = index;
    const { checkoutData, filteredListArray } = this.state;
    let tempArr = [];
    if (filteredListArray.length > 0) {
      switch (index) {
        case 0:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Single"
          );
          break;
        case 1:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Double"
          );

          break;
        case 2:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Triple"
          );

          break;
        case 3:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Quad"
          );
          break;
        case 4:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Twin"
          );
          break;
        case 5:
          tempArr = filteredListArray.filter(
            (listItem) => listItem.room_type === "Day Slot"
          );
          break;
        case 6:
          tempArr = filteredListArray;
          break;
        default:
          break;
      }
      this.setState({
        checkoutData: tempArr,
      });
    }
  };

  getAdditionalDiscount = (regularPrice) => {
    return (Number(regularPrice) * 40) / 100;
  };

  render() {
    const {
      checkoutData,
      selectdMonthText,
      isMonthYearPickerVisible,
      itemValue,
      isRoomViewVisible,
    } = this.state;
    return (
      <View style={{ backgroundColor: COLORS.backGround_color, flex: 1 }}>
        <View style={styles.mainView}>
          <View style={styles.outerView}>
            <TouchableOpacity onPress={() => this.monthYearViewClick()}>
              <View style={styles.touchableStyle}>
                <Text style={{ color: COLORS.greyButton_color2 }}>
                  {selectdMonthText}
                </Text>
                <Image
                  style={styles.downArrowImage}
                  source={ICONS.DOWN_ARROW}
                />
              </View>
            </TouchableOpacity>
          </View>
          <Spacer row={2} />

          {this.bookingFilter()}
        </View>
        {isMonthYearPickerVisible === true && (
          <CustomeMonthYearPicker
            isMonthYearPickerVisible={isMonthYearPickerVisible}
            newDateMonthSelected={(date) => this.newMonthYearSelected(date)}
            crossPickerAction={() => this.closeMonthYearPicker()}
          />
        )}
        <Spacer space={3} />
        {console.log("check out", checkoutData)}
        {checkoutData.length !== 0 ? (
          <FlatList
            style={{ paddingBottom: wp(17) }}
            data={checkoutData}
            extraData={this.state}
            keyExtractor={(item, index) => index}
            renderItem={({ item, index }) => (
              <View style={{ width: wp(100), paddingTop: wp(2) }}>
                <ShadowViewContainer>
                  <View style={{ flexDirection: "row", width: wp(92) }}>
                    {console.log("checkout is", item)}
                    <FastImage
                      style={{
                        width: wp(30),
                        height: wp(42),
                      }}
                      source={{
                        uri:
                          item.room_images.image !== null
                            ? API.IMAGE_BASE_URL + item.room_images.image
                            : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                      }}
                    />
                    <Spacer row={1} />
                    <View style={{ width: wp(57), justifyContent: "center" }}>
                      <Spacer space={1} />
                      <Text numberOfLines={1} style={styles.bookedBytext}>
                        Booked by : {item.customer_name}
                      </Text>
                      <Spacer space={1} />
                      <Text numberOfLines={1} style={styles.dateText}>
                        Check In Date : {item.check_in}
                      </Text>

                      <Spacer space={1} />
                      <Text numberOfLines={1} style={styles.dateText}>
                        Check Out Date : {item.check_out}
                      </Text>

                      <Spacer space={1} />

                      <Text numberOfLines={1} style={styles.roomTypeText}>
                        Room Type : {item.room_type}
                      </Text>
                      <Spacer space={1} />
                      {item.no_of_hours !== null ? (
                        <Text numberOfLines={1} style={styles.roomTypeText}>
                          Staying hour : {item.no_of_hours} hr
                        </Text>
                      ) : (
                        <Text numberOfLines={1} style={styles.roomTypeText}>
                          Staying days : {item.no_of_days} days
                        </Text>
                      )}

                      <Spacer space={1} />

                      <View style={styles.priceOuterView}>
                        <Text numberOfLines={1} style={styles.priceText}>
                          Price:
                          {item.is_special === true ? (
                            <Text style={{ color: COLORS.customerType }}>
                              {"\u20B9"}
                              {this.getAdditionalDiscount(item.room_regular_price)}
                            </Text>
                          ) : (
                            <Text style={{ color: COLORS.customerType }}>
                              {"\u20B9"}
                              {item.room_price}
                            </Text>
                          )}
                        </Text>
                        <TouchableOpacity
                          style={styles.touchRoomView}
                          onPress={() => this.roomViewClicked(item)}
                        >
                          <Text style={styles.roomViewText}>Room View</Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </ShadowViewContainer>
                <Spacer space={0} />
              </View>
            )}
          />
        ) : (
          <View>
            <Text style={styles.noDataStyle}>NO DATA </Text>
          </View>
        )}
        {isRoomViewVisible === true && (
          <RoomView
            isRoomViewVisible={isRoomViewVisible}
            value={itemValue}
            closeModal={() =>
              this.setState({ isRoomViewVisible: !isRoomViewVisible })
            }
          />
        )}

        {this._renderCustomLoader()}
      </View>
    );
  }
}

export default CheckoutByOwner;
