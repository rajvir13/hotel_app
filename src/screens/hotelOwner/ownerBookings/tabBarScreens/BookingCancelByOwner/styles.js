import React from "react";
import { StyleSheet } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLORS from "../../../../../utils/Colors";
import { FONT } from "../../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../../utils/Font";

const styles = StyleSheet.create({
  mainView: {
    backgroundColor: COLORS.light_grey2,
    paddingHorizontal: wp(5),
    paddingVertical: wp(3),
    width: wp(100),
  },
  cancelText: {
    fontSize: FONT.TextSmall,
    fontFamily: FONT_FAMILY.Montserrat,
  },
  bookedByView: {
    width: wp(50),
    color: COLORS.black_color,
    fontSize: FONT.TextSmall_2,
    fontFamily: FONT_FAMILY.Poppins,
  },
  bookedCreated: {
    color: COLORS.black_color,
    fontSize: FONT.TextSmall_2,
    fontFamily: FONT_FAMILY.Montserrat,
  },
  roomType: {
    color: COLORS.black_color,
    fontSize: FONT.TextExtraSmall,
    fontFamily: FONT_FAMILY.Montserrat,
  },
 priceText:{
    fontSize: FONT.TextSmall_2,
    fontFamily: FONT_FAMILY.MontserratSemiBold,
 },
 cancelledBy:{
    textAlign: "center",
    color: COLORS.price_Background_color,
    fontSize: FONT.TextExtraSmall,
    fontFamily: FONT_FAMILY.PoppinsBold,

 },
 dateText:{

    color: COLORS.app_theme_color,
    fontSize: FONT.TextExtraSmall,
    fontFamily: FONT_FAMILY.Montserrat,
    marginLeft:-wp('1%')
 }

});

export { styles };
