import React from "react";
import { FlatList, Text, View, TouchableOpacity } from "react-native";
import { ShadowViewContainer } from "../../../../../utils/BaseStyle";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLORS from "../../../../../utils/Colors";
import { FONT } from "../../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../../utils/Font";
import BaseClass from "../../../../../utils/BaseClass";
import STRINGS from "../../../../../utils/Strings";
import { Spacer } from "../../../../../customComponents/Spacer";
import FastImage from "react-native-fast-image";
import AsyncStorage from "@react-native-community/async-storage";
import { OwnerCancelData } from "../../../../../redux/actions/OwnerPendingConfirmation";
import { API } from "../../../../../redux/constant";
import propTypes from "prop-types";
import { styles } from "./styles";
import RoomView from "../../../../../customComponents/roomView/index";

let checkVar = false;

class BookingCancelByOwner extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      cancelData: [],
      isModalVisible: false,
      itemValue: {},
      authToken: undefined,
      ownerId: undefined,
      cancelDataCount: 0,
      userName: "me",
      isRoomViewVisible: false,
      isFetching: false,
    };
  }

  static propTypes = {
    routeKey: propTypes.number,
  };

  onRefresh() {
    this.setState({ isFetching: true }, () => {
      this.hitApi();
    });
  }
  componentDidMount() {
    this.hitApi();
  }

  componentDidUpdate(prevProps, preState, snapShot) {
    if (this.props.routeKey === 3) {
      if (checkVar === true) {
        checkVar = false;
        this.hitApi();
      }
    } else {
      checkVar = true;
    }
  }

  hitApi = () => {
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        // const { hotel_details } = loginData;

        if (loginData.hotel_details !== undefined) {
          this.setState({
            authToken: loginData.access_token,
            ownerId: loginData.hotel_details.owner,
            userName: loginData.first_name + " " + loginData.last_name,
          });
          OwnerCancelData(
            {
              accessToken: loginData.access_token,
              ownerId: loginData.hotel_details.owner,
            },
            (cancelData) => this.cancelResponseData(cancelData)
          );
        } else {
          this.setState({
            authToken: loginData.access_token,
            ownerId: loginData.owner.id,
            userName:
              loginData.owner.first_name + " " + loginData.owner.last_name,
          });
          OwnerCancelData(
            {
              accessToken: loginData.access_token,
              ownerId: loginData.owner.id,
            },
            (cancelData) => this.cancelResponseData(cancelData)
          );
        }
      }
    });
  };

  cancelResponseData = (cancelData) => {
    const { response, status, message } = cancelData;
    this.setState({ isFetching: false })
    console.warn("cancelData", cancelData);
    if (cancelData === "Network request failed") {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (response !== undefined) {
        if (status === 200) {
          this.setState({
            cancelData: response,
            cancelDataCount: response.length,
          });
        } else if (status === 111) {
          this.hideDialog();
          this.showToastAlert(message);
        } else if (status === 401) {
          this.hideDialog();
          this.showToastAlert(message);
        } else {
          this.hideDialog();
          this.showToastAlert(message);
        }
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      } else {
        this.hideDialog();
        this.showToastAlert(message);
      }
      this.hideDialog();
    }
  };

  getAdditionalDiscount = (regularPrice) => {
    return (Number(regularPrice) * 40) / 100;
  };

  render() {
    const {
      cancelData,
      cancelDataCount,
      userName,
      itemValue,
      isRoomViewVisible,
    } = this.state;
    return (
      <View style={{ backgroundColor: COLORS.backGround_color, flex: 1 }}>
        <View style={styles.mainView}>
          <Text style={styles.cancelText}>Total Cancel {cancelDataCount}</Text>
        </View>

        <Spacer space={3} />
        {cancelData.length !== 0 ? (
          <FlatList
            extraData={this.state}
            data={cancelData}
            onRefresh={() => this.onRefresh()}
            refreshing={this.state.isFetching}
            keyExtractor={(item, index) => index}
            renderItem={({ item, index }) => (
              <View style={{ width: wp(100) }}>
                <ShadowViewContainer>
                  <View style={{ flexDirection: "row", width: wp(90) }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          isRoomViewVisible: true,
                          itemValue: item,
                        });
                      }}
                    >
                      <FastImage
                        style={{
                          width: wp(30),
                          height: wp(35),
                        }}
                        source={{
                          uri:
                            item.room_images !== null &&
                            item.room_images.image !== null
                              ? API.IMAGE_BASE_URL + item.room_images.image
                              : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                        }}
                      />
                    </TouchableOpacity>
                    <Spacer row={1} />
                    <View style={{ width: wp(57), justifyContent: "center" }}>
                      <Spacer space={1} />
                      <Text numberOfLines={1} style={styles.bookedByView}>
                        Booked by : {item.customer_name}{" "}
                      </Text>
                      <Spacer space={0.5} />
                      <Text numberOfLines={1} style={styles.bookedCreated}>
                        Date {item.booking_created}{" "}
                      </Text>
                      <Spacer space={1} />
                      <View style={{ flexDirection: "row" }}>
                        <View style={{ width: wp(28) }}>
                          <Text numberOfLines={1} style={styles.roomType}>
                            Room Type {item.room_type}{" "}
                          </Text>
                          <Spacer space={1} />

                          <Text numberOfLines={1} style={styles.priceText}>
                            Price{" "}
                            {item.is_special === true ? (
                              <Text style={{ color: COLORS.customerType }}>
                                {"\u20B9"}
                                {this.getAdditionalDiscount(
                                  item.room_regular_price
                                )}
                              </Text>
                            ) : (
                              <Text style={{ color: COLORS.customerType }}>
                                {"\u20B9"}
                                {item.room_price}
                              </Text>
                            )}
                          </Text>
                        </View>
                        <Spacer row={1} />
                        <View style={{ width: wp(28) }}>
                          <Text numberOfLines={2} style={styles.cancelledBy}>
                            Cancelled by{" "}
                            {userName === item.cancelled_by_name
                              ? "me"
                              : item.cancelled_by_name}
                          </Text>
                          <Spacer space={1} />
                          <Text numberOfLines={1} style={styles.dateText}>
                            Date {item.cancelled_by_date}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </ShadowViewContainer>
                <Spacer space={0} />
              </View>
            )}
          />
        ) : (
          <View>
            <Text
              style={{
                textAlign: "center",
                fontSize: FONT.TextNormal,
                fontFamily: FONT_FAMILY.MontserratBold,
              }}
            >
              NO DATA{" "}
            </Text>
          </View>
        )}
        {isRoomViewVisible === true && (
          <RoomView
            isRoomViewVisible={isRoomViewVisible}
            value={itemValue}
            userName={userName}
            closeModal={() =>
              this.setState({ isRoomViewVisible: !isRoomViewVisible })
            }
          />
        )}
      </View>
    );
  }
}

export default BookingCancelByOwner;
