import React from "react";
import {
  FlatList,
  Image,
  Text,
  TouchableOpacity,
  View,
  Keyboard,
  Alert,
} from "react-native";
import BaseClass from "../../../../../utils/BaseClass";
import {
  MainContainer,
  ScrollContainer,
  ShadowViewContainer,
} from "../../../../../utils/BaseStyle";
import COLORS from "../../../../../utils/Colors";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { FONT } from "../../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../../utils/Font";
import { Data } from "../../../../../utils/FixtureData";
import { Spacer } from "../../../../../customComponents/Spacer";
import FastImage from "react-native-fast-image";
import STRINGS from "../../../../../utils/Strings";
import ConfirmBooking from "./ConfirmModal";
import CrossComponent from "../../../../../../assets/images/BookingIcons/CrossSVG";
import { OwnerPendingConfirmation } from "../../../../../redux/actions/OwnerPendingConfirmation";
import { OwnerConfirmBooking } from "../../../../../redux/actions/OwnerConfirmBooking";
import { OwnerCancelPaindingBooking } from "../../../../../redux/actions/OwnerCancelPaindingBooking";

import AsyncStorage from "@react-native-community/async-storage";
import { API } from "../../../../../redux/constant";
import propTypes from "prop-types";
import RoomView from "../../../../../customComponents/roomView/index";
let checkVar = false;

class PendingConfirmation extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      authToken: undefined,
      pendingData: undefined,
      onwerId: undefined,
      isModalVisible: false,
      itemValue: {},
      pendingRequestCount: undefined,
      isRoomViewVisible: false,
      userName: undefined,
      modelData: props.value,
      isFetching: false,
    };
  }

  static propTypes = {
    routeKey: propTypes.number,
  };

  componentDidMount() {
    this.hitApi();
    const { navigation } = this.props.navigationInfo;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.onFocusFunction();
    });
  }

  onRefresh() {
    this.setState({ isFetching: true }, () => {
      this.hitApi();
    });
  }
  onFocusFunction = () => {
    debugger;
    this.hitApi();
  };

  componentDidUpdate(prevProps, preState, snapShot) {
    debugger;
    if (this.props.routeKey === 0) {
      if (checkVar === true) {
        checkVar = false;
        this.hitApi();
      } else {
        if (this.props.notificationData !== undefined) {
          checkVar = false;
          this.hitApi();
        }
      }
    } else {
      checkVar = true;
    }
  }

  hitApi = () => {
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        // const { hotel_details } = loginData;
        if (loginData.hotel_details !== undefined) {
          this.setState({
            authToken: loginData.access_token,
            onwerId: loginData.hotel_details.owner,
          });
          this.getPendingConfirmation(
            loginData.access_token,
            loginData.hotel_details.owner
          );
        } else {
          this.setState({
            authToken: loginData.access_token,
            onwerId: loginData.owner.id,
          });
          this.getPendingConfirmation(
            loginData.access_token,
            loginData.owner.id
          );
        }
      }
    });
  };

  getPendingConfirmation = (accesToken, ownerId) => {
    this.showDialog();
    OwnerPendingConfirmation(
      {
        ownerId: ownerId,
        accessToken: accesToken,
      },
      (data) => this.setPendingConfirmationData(data)
    );
  };
  //response of pending confirmation data
  setPendingConfirmationData = (data) => {
    const { response, status, message } = data;
    this.setState({ isFetching: false });
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (response !== undefined) {
        if (status === 200) {
          this.setState({
            pendingData: response,
            pendingRequestCount: response.length,
          });
        } else if (status === 111) {
          this.hideDialog();
          this.showToastAlert(message);
        } else if (status === 401) {
          this.hideDialog();
          this.showToastAlert(message);
        } else {
          this.hideDialog();
          this.showToastAlert(message);
        }
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      } else {
        this.hideDialog();
        this.showToastAlert(message);
      }
      this.hideDialog();
    }
  };

  onCrossIconClick = (index) => {
    const { pendingData, authToken, onwerId } = this.state;

    var date = new Date().getDate();
    var month = new Date().getMonth() + 1;
    var year = new Date().getFullYear();
    Alert.alert("Are you sure you want to cancel this booking ?", "", [
      {
        text: "No ",
        onPress: () => console.log("Cancel Pressed"),
        style: "cancel",
      },
      {
        text: "Yes",
        onPress: () =>
          OwnerCancelPaindingBooking(
            {
              accessToken: authToken,
              bookingId: pendingData[index].id,
              cancelled_by: onwerId,
              cancelled_by_date: year + "-" + month + "-" + date,
            },
            (data) => this.getOwnerCancelRequestResponse(data)
          ),
      },
    ]);
  };
  getOwnerCancelRequestResponse = (data) => {
    const { pendingData, authToken, onwerId } = this.state;
    const { response, status, message } = data;
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (response !== undefined) {
        if (status === 200) {
          this.showToastSucess(message);
          this.getPendingConfirmation(authToken, onwerId);
        } else if (status === 111) {
          this.hideDialog();
          this.showToastAlert(message);
        } else if (status === 401) {
          this.hideDialog();
          this.showToastAlert(message);
        } else {
          this.hideDialog();
          this.showToastAlert(message);
        }
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      } else {
        this.hideDialog();
        this.showToastAlert(message);
      }
      this.hideDialog();
    }
  };
  //when hotel owner click on confirm booking
  hotelVerifyOtp = (time, stayingDays, stayingHour, confirmatinCode) => {
    Keyboard.dismiss();
    const { itemValue, authToken } = this.state;

    this.showDialog();
    OwnerConfirmBooking(
      {
        verified_code: confirmatinCode,
        booking_id: itemValue.id,
        no_of_days: stayingDays,
        check_in_time: time,
        accessToken: authToken,
        no_of_hours: stayingHour,
      },
      (data) => this.getHotelConfirmaitionData(data)
    );
  };

  getHotelConfirmaitionData = (data) => {
    const { pendingData, authToken, onwerId } = this.state;
    const { response, code, message } = data;
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (code === 200) {
        this.showToastSucess(message);
        this.getPendingConfirmation(authToken, onwerId);
      } else if (code === 111) {
        this.hideDialog();
        this.showToastAlert(message);
      } else if (code === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else {
        this.hideDialog();
        this.showToastAlert(message);
      }
      this.hideDialog();
    }
  };

  getAdditionalDiscount = (regularPrice) => {
    return (Number(regularPrice) * 40) / 100;
  };

  render() {
    const {
      isModalVisible,
      itemValue,
      pendingData,
      pendingRequestCount,
      isRoomViewVisible,
      userName,
    } = this.state;

    return (
      <MainContainer>
        <View
          style={{
            backgroundColor: COLORS.light_grey2,
            paddingHorizontal: wp(5),
            paddingVertical: wp(3),
            width: wp(100),
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <Text
              style={{
                fontSize: FONT.TextSmall,
                fontFamily: FONT_FAMILY.Montserrat,
              }}
            >
              Pending Requests :
            </Text>
            <Text
              style={{
                fontSize: FONT.TextSmall,
                fontFamily: FONT_FAMILY.Montserrat,
              }}
            >
              {pendingRequestCount}
            </Text>
          </View>
        </View>
        <Spacer space={3} />
        {pendingRequestCount !== 0 ? (
          <FlatList
            style={{ paddingBottom: wp(17) }}
            data={pendingData}
            onRefresh={() => this.onRefresh()}
            refreshing={this.state.isFetching}
            extraData={this.state}
            keyExtractor={(item, index) => index}
            renderItem={({ item, index }) => (
              <View style={{ width: wp(100), paddingTop: wp(2) }}>
                <ShadowViewContainer>
                  <View style={{ flexDirection: "row", width: wp(90) }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({
                          isRoomViewVisible: true,
                          itemValue: item,
                        });
                      }}
                    >
                      <FastImage
                        style={{
                          width: wp(31),
                          height: wp(42),
                        }}
                        source={{
                          uri:
                            item.image !== null
                              ? API.IMAGE_BASE_URL + item.room_images.image
                              : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                        }}
                        resizeMode={FastImage.resizeMode.stretch}
                      />
                    </TouchableOpacity>
                    <Spacer row={1} />
                    <View style={{ width: wp(57), justifyContent: "center" }}>
                      <Spacer space={1} />
                      <Text
                        numberOfLines={1}
                        style={{
                          color: COLORS.black_color,
                          fontSize: FONT.TextSmall,
                          fontFamily: FONT_FAMILY.Poppins,
                        }}
                      >
                        Booked By : {item.customer_name}
                      </Text>
                      <Spacer space={0.5} />
                      <Text
                        numberOfLines={1}
                        style={{
                          color: COLORS.black_color,
                          fontSize: FONT.TextSmall_2,
                          fontFamily: FONT_FAMILY.Montserrat,
                        }}
                      >
                        Date : {item.booking_created}
                      </Text>
                      <Spacer space={1} />
                      <View style={{ flexDirection: "row" }}>
                        <Text
                          numberOfLines={1}
                          style={{
                            color: COLORS.black_color,
                            fontSize: FONT.TextSmall_2,
                            fontFamily: FONT_FAMILY.Montserrat,
                          }}
                        >
                          Room Type :
                        </Text>
                        <Text
                          numberOfLines={1}
                          style={{
                            color: COLORS.black_color,
                            fontSize: FONT.TextSmall_2,
                            fontFamily: FONT_FAMILY.Montserrat,
                          }}
                        >
                          {item.room_type}
                        </Text>
                      </View>

                      {item.no_of_hours !== null && (
                        <View>
                          <Spacer space={1} />
                          <Text
                            numberOfLines={1}
                            style={{
                              color: COLORS.black_color,
                              fontSize: FONT.TextSmall_2,
                              fontFamily: FONT_FAMILY.Montserrat,
                            }}
                          >
                            Stay hour : {item.no_of_hours} hr
                          </Text>
                        </View>
                      )}

                      <Spacer space={1} />
                      {console.log("sdfsdfs", item)}
                      {item.is_special === true ? (
                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                          }}
                        >
                          <Text
                            numberOfLines={1}
                            style={{
                              fontSize: FONT.TextSmall_2,
                              fontFamily: FONT_FAMILY.MontserratSemiBold,
                            }}
                          >
                            Price :
                            <Text style={{ color: COLORS.customerType }}>
                              {"\u20B9"}
                              {this.getAdditionalDiscount(
                                item.room_regular_price
                              )}
                            </Text>{" "}
                          </Text>
                        </View>
                      ) : (
                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "space-between",
                          }}
                        >
                          <Text
                            numberOfLines={1}
                            style={{
                              fontSize: FONT.TextSmall_2,
                              fontFamily: FONT_FAMILY.MontserratSemiBold,
                            }}
                          >
                            Price :
                            <Text style={{ color: COLORS.customerType }}>
                              {"\u20B9"}
                              {item.room_price}
                            </Text>{" "}
                          </Text>
                        </View>
                      )}

                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-around",
                          marginTop: wp("1%"),
                        }}
                      >
                        <TouchableOpacity
                          style={{
                            width: wp(28),
                            marginRight: wp(2),
                            backgroundColor: COLORS.green_color,
                            borderRadius: wp(2),
                            marginTop: wp("1%"),
                            marginBottom: wp("2%"),
                          }}
                          onPress={() =>
                            this.setState({
                              isModalVisible: true,
                              itemValue: item,
                            })
                          }
                        >
                          <Text
                            style={{
                              textAlign: "center",
                              color: COLORS.white_color,
                              fontSize: FONT.TextExtraSmall,
                              fontFamily: FONT_FAMILY.Poppins,
                              // paddingHorizontal: wp(5),
                              paddingVertical: wp(1),
                            }}
                          >
                            Confirm Booking
                          </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                          style={{
                            width: wp(28),
                            marginRight: wp(2),
                            backgroundColor: COLORS.customerType,
                            borderRadius: wp(2),
                            marginLeft: 5,
                            marginTop: wp("1%"),
                            marginBottom: wp("2%"),
                          }}
                          onPress={() => {
                            this.onCrossIconClick(index);
                          }}
                        >
                          <Text
                            style={{
                              textAlign: "center",
                              color: COLORS.white_color,
                              fontSize: FONT.TextExtraSmall,
                              fontFamily: FONT_FAMILY.Poppins,
                              // paddingHorizontal: wp(5),
                              paddingVertical: wp(1),
                            }}
                          >
                            Cancel Booking
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </View>
                </ShadowViewContainer>

                <Spacer space={0} />
              </View>
            )}
          />
        ) : (
          <View>
            <Text
              style={{
                textAlign: "center",
                fontSize: FONT.TextNormal,
                fontFamily: FONT_FAMILY.MontserratBold,
              }}
            >
              NO DATA{" "}
            </Text>
          </View>
        )}
        {isRoomViewVisible === true && (
          <RoomView
            isRoomViewVisible={isRoomViewVisible}
            value={itemValue}
            userName={userName}
            closeModal={() =>
              this.setState({ isRoomViewVisible: !isRoomViewVisible })
            }
          />
        )}

        {isModalVisible === true && (
          <ConfirmBooking
            isModalVisible={isModalVisible}
            value={itemValue}
            closeModal={() =>
              this.setState({ isModalVisible: !isModalVisible })
            }
            onDonePress={(time, stayingdays, stayingHour, confirmationOtp) =>
              this.hotelVerifyOtp(
                time,
                stayingdays,
                stayingHour,
                confirmationOtp
              )
            }
          />
        )}
      </MainContainer>
    );
  }
}

export default PendingConfirmation;
