import React, { Component } from "react";
import {
  Modal,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
} from "react-native";
import propTypes from "prop-types";
import { Input } from "react-native-elements";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import COLORS from "../../../../../../utils/Colors";
import { FONT } from "../../../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../../../utils/Font";
import FastImage from "react-native-fast-image";
import { Spacer } from "../../../../../../customComponents/Spacer";
import CrossComponent from "../../../../../../../assets/images/BookingIcons/CrossSVG";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import STRINGS from "../../../../../../utils/Strings";
import { ICONS } from "../../../../../../utils/ImagePaths";
import {
  SafeAreaViewContainer,
  ShadowViewContainer,
} from "../../../../../../utils/BaseStyle";
import ClockIconSVG from "../../../../../../../assets/images/Register/clockIconSVG";
import CustomTextInput from "../../../../../../customComponents/CustomTextInput";
import BaseClass from "../../../../../../utils/BaseClass";
import { API } from "../../../../../../redux/constant";
import { RoomView } from "../../../../../../customComponents/roomView/index";

export default class ConfirmBooking extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      modalVisiblePass: false,
      value: "",
      indexValue: 0,
      isDatePickerVisible: false,
      dateSelected: "Check In Time",
      stayingDays: "",
      confirmationCode: "",
      modelData: props.value,
    };
  }

  static propTypes = {
    isModalVisible: propTypes.any,
    onHotelClick: propTypes.func,
    value: propTypes.string,
    closeModal: propTypes.func,
    indexRoom: propTypes.number,
    onDonePress: propTypes.func,
  };

  formatAMPM = (data) => {
    if (data !== null && data !== undefined) {
      const [time] = data.split(" ");
      let [sHours, minutes] = time.split(":");

      // const [sHours, minutes] = time24.match(/([0-9]{1,2}):([0-9]{2})/).slice(1);
      const period = +sHours < 12 ? "AM" : "PM";
      const hours = +sHours % 12 || 12;

      return `${hours}:${minutes} ${period}`;
    } else {
      return "12:00 PM";
    }
  };

  // formatAMPM = (data) => {
  //   console.log("date is", data);
  //   if (data !== undefined && data !== null && data !== "") {
  //     const [time] = data.split(" ");

  //     let [hours, minutes] = time.split(":");
  //     console.log("hour", hours);
  //     console.log("minutes", minutes);

  //     var ampm = hours >= 12 ? "PM" : "AM";
  //     hours = hours % 12;
  //     hours = hours ? hours : 12; // the hour '0' should be '12'
  //     minutes = minutes < 10 ? "0" + minutes : minutes;
  //     var strTime = hours + ":" + minutes + " " + ampm;
  //     return strTime;
  //   } else {
  //     return "12:00 PM";
  //   }
  // };

  handleConfirm = (value) => {
    this.setState({ isDatePickerVisible: false });
    let time = value.toLocaleTimeString();
    console.log("value is", value.toLocaleTimeString());

    var time1 = value.toLocaleTimeString("it-IT");
    let removeSec = time1.slice(0, 5);
    this.setState({ dateSelected: this.formatAMPM(removeSec) });

    // this.setState({ dateSelected: value.toLocaleTimeString()  });
  };

  // formatAMPM = (data) => {
  //   if (data !== undefined && data !== null && data !== "") {
  //     const [time] = data.split(" ");

  //     let [hours, minutes] = time.split(":");

  //     var ampm = hours >= 12 ? "PM" : "AM";
  //     hours = hours % 12;
  //     hours = hours ? hours : 12; // the hour '0' should be '12'
  //     minutes = minutes < 10 ? "0" + minutes : minutes;
  //     var strTime = hours + ":" + minutes + " " + ampm;
  //     return strTime;
  //   } else {
  //     return "12:00 PM";
  //   }
  // };

  formatAMPM = (data) => {
    const [time] = data.split(" ");
    let [sHours, minutes] = time.split(":");

    // const [sHours, minutes] = time24.match(/([0-9]{1,2}):([0-9]{2})/).slice(1);
    const period = +sHours < 12 ? "AM" : "PM";
    const hours = +sHours % 12 || 12;

    return `${hours}:${minutes} ${period}`;
  };

  showDatePicker = () => {
    this.setState({ isDatePickerVisible: true });
  };

  hideDatePicker = () => {
    this.setState({ isDatePickerVisible: false });
  };

  closeModal = () => {
    this.props.closeModal();
  };

  onDoneClick = () => {
    const {
      dateSelected,
      stayingDays,
      confirmationCode,
      modelData,
    } = this.state;

    Keyboard.dismiss(0);
    console.log("stayingDays", stayingDays);

    if (dateSelected === "Check In Time") {
      this.showToastAlert(STRINGS.check_in_date);
    } else if (
      stayingDays.trim().length === 0 &&
      modelData.no_of_hours == null
    ) {
      this.showToastAlert(STRINGS.staying_date);
    } else if (confirmationCode.trim().length === 0) {
      this.showToastAlert(STRINGS.confirm_code);
    } else if (confirmationCode.length < 6) {
      this.showToastAlert(STRINGS.invalid_otp);
    } else {
      this.props.closeModal();

      if (modelData.no_of_hours !== null) {
        this.props.onDonePress(
          dateSelected,
          null,
          modelData.no_of_hours,
          confirmationCode
        );
      } else {
        this.props.onDonePress(
          dateSelected,
          stayingDays,
          null,
          confirmationCode
        );
      }
    }
  };

  getAdditionalDiscount = (regularPrice) => {
    return (Number(regularPrice) * 40) / 100;
  };
  render() {
    const {
      isDatePickerVisible,
      dateSelected,
      stayingDays,
      modelData,
      confirmationCode,
    } = this.state;

    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.isModalVisible}
      >
        <KeyboardAvoidingView
          style={{ flex: 1 }}
          behavior={Platform.OS === "ios" ? "padding" : null}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              Keyboard.dismiss();
            }}
          >
            <View style={styles.centeredView}>
              <View
                style={{
                  backgroundColor: "transparent",
                  alignItems: "center",
                  width: wp(98),
                }}
              >
                <View
                  style={{
                    backgroundColor: COLORS.white_color,
                    width: wp(93),
                    borderRadius: wp(5),
                  }}
                >
                  <View
                    style={{
                      alignItems: "center",
                      paddingVertical: wp(4),
                      backgroundColor: COLORS.app_theme_color,
                      justifyContent: "flex-start",
                    }}
                  >
                    <Text
                      style={{
                        fontSize: FONT.TextMedium_2,
                        fontFamily: FONT_FAMILY.MontserratSemiBold,
                        color: COLORS.white_color,
                      }}
                    >
                      Confirm Customer
                    </Text>
                  </View>
                  <View
                    style={{
                      flexDirection: "row",
                      paddingVertical: wp(4),
                      paddingHorizontal: wp(2),
                      backgroundColor: COLORS.white_color,
                      width: wp(93),
                    }}
                  >
                    <FastImage
                      style={{
                        borderRadius: wp(3),
                        width: wp(43),
                        height: wp(40),
                      }}
                      source={{
                        uri:
                          modelData.room_images !== null &&
                          modelData.room_images.image !== null
                            ? API.IMAGE_BASE_URL + modelData.room_images.image
                            : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                      }}
                      resizeMode={FastImage.resizeMode.stretch}
                    />
                    <Spacer row={1} />
                    <View style={{ width: wp(45), justifyContent: "center" }}>
                      <Text
                        numberOfLines={2}
                        style={{
                          color: COLORS.black_color,
                          fontSize: FONT.TextSmall,
                          fontFamily: FONT_FAMILY.Poppins,
                        }}
                      >
                        Booked by :{modelData.customer_name}
                      </Text>
                      <Spacer space={1} />
                      <Text
                        numberOfLines={1}
                        style={{
                          color: COLORS.black_color,
                          fontSize: FONT.TextSmall_2,
                          fontFamily: FONT_FAMILY.Montserrat,
                        }}
                      >
                        Date : {modelData.booking_created}{" "}
                      </Text>
                      <Spacer space={1} />
                      <Text
                        numberOfLines={1}
                        style={{
                          color: COLORS.black_color,
                          fontSize: FONT.TextSmall_2,
                          fontFamily: FONT_FAMILY.Montserrat,
                        }}
                      >
                        Room Type : {modelData.room_type}{" "}
                      </Text>
                      <Spacer space={1} />
                      {modelData.is_special === true ? (
                        <View>
                          <Text
                            numberOfLines={1}
                            style={{
                              fontSize: FONT.TextSmall_2,
                              fontFamily: FONT_FAMILY.MontserratSemiBold,
                            }}
                          >
                            Price :{" "}
                            <Text style={{ color: COLORS.customerType }}>
                              {"\u20B9"}
                              {this.getAdditionalDiscount(
                                modelData.room_regular_price
                              )}
                            </Text>
                          </Text>
                        </View>
                      ) : (
                        <View>
                          <Text
                            numberOfLines={1}
                            style={{
                              fontSize: FONT.TextSmall_2,
                              fontFamily: FONT_FAMILY.MontserratSemiBold,
                            }}
                          >
                            Price :{" "}
                            <Text style={{ color: COLORS.customerType }}>
                              {"\u20B9"}
                              {modelData.room_price}
                            </Text>
                          </Text>
                        </View>
                      )}
                    </View>
                  </View>
                  <Spacer space={2} />

                  <ShadowViewContainer>
                    <TouchableOpacity
                      style={{
                        flexDirection: "row",
                        justifyContent: "space-around",
                        paddingVertical: wp(3),
                        width: wp(85),
                      }}
                      onPress={() => this.showDatePicker()}
                    >
                      <Text style={{ width: wp(75) }}>{dateSelected}</Text>
                      <ClockIconSVG width={wp(5)} height={wp(5)} />
                    </TouchableOpacity>
                  </ShadowViewContainer>

                  {modelData.room_type === "Day Slot" &&
                  modelData.no_of_hours !== null ? (
                    <CustomTextInput
                      placeholder={"Staying Hour"}
                      returnKeyType={"next"}
                      // keyboardType={"default"}
                      inputvalue={stayingDays}
                      keyboardType={"phone-pad"}
                      isEditable={false}
                      inputvalue={String(modelData.no_of_hours)}
                    />
                  ) : (
                    <CustomTextInput
                      placeholder={"Staying Days"}
                      returnKeyType={"next"}
                      // keyboardType={"default"}
                      inputvalue={stayingDays}
                      keyboardType={"phone-pad"}
                      ipOnChangeText={(text) =>
                        this.setState({ stayingDays: text })
                      }
                    />
                  )}

                  <Spacer space={0.5} />
                  <CustomTextInput
                    maxLength={6}
                    placeholder={"Confirmation Code"}
                    returnKeyType={"next"}
                    inputvalue={confirmationCode}
                    keyboardType={"phone-pad"}
                    ipOnChangeText={(text) =>
                      this.setState({ confirmationCode: text })
                    }
                  />

                  <DateTimePickerModal
                    isVisible={isDatePickerVisible}
                    titleIOS="Pick a time"
                    mode="time"
                    is24Hour={false}
                    date={new Date()}
                    onConfirm={(time) => this.handleConfirm(time)}
                    onCancel={this.hideDatePicker}
                  />
                  <Spacer space={2} />
                  <TouchableOpacity
                    style={{
                      borderRadius: wp(2),
                      alignSelf: "center",
                      width: wp(40),
                      paddingVertical: wp(3),
                      backgroundColor: COLORS.app_theme_color,
                    }}
                    onPress={() => {
                      this.onDoneClick();
                    }}
                  >
                    <Text
                      style={{
                        textAlign: "center",
                        color: COLORS.white_color,
                        fontSize: FONT.TextSmall,
                        fontFamily: FONT_FAMILY.MontserratSemiBold,
                      }}
                    >
                      Done
                    </Text>
                  </TouchableOpacity>
                  <Spacer space={2} />
                </View>
                <TouchableOpacity
                  hitSlop={{ top: wp(5), left: wp(5), bottom: wp(5) }}
                  style={{
                    position: "absolute",
                    alignSelf: "flex-end",
                    marginTop: wp(-3),
                    zIndex: 100,
                  }}
                  onPress={() => {
                    this.closeModal();
                  }}
                >
                  <CrossComponent width={wp(9)} height={wp(9)} />
                </TouchableOpacity>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.6)",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});
