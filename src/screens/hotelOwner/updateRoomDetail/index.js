import React from "react";
import {
  View,
  FlatList,
  Text,
  Image,
  Platform,
  Alert,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  TextInput,
  TouchableWithoutFeedback,
  ImageBackground,
  Keyboard,
} from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Header } from "react-native-elements";
import BaseClass from "../../../utils/BaseClass";
import {
  SafeAreaViewContainer,
  ShadowViewContainer,
} from "../../../utils/BaseStyle";
import { Spacer } from "../../../customComponents/Spacer";
import COLORS from "../../../utils/Colors";
import { LeftIcon, RightIcon } from "../../../customComponents/icons";
import STRINGS from "../../../utils/Strings";
import { FONT } from "../../../utils/FontSizes";
import { ICONS } from "../../../utils/ImagePaths";
import styles from "./styles";
import { Dropdown } from "react-native-material-dropdown";
import ImagePicker from "react-native-image-crop-picker";
import * as _ from "lodash";
import NumericInput from "react-native-numeric-input";
import SectionedMultiSelect from "../../../localModule/react-native-sectioned-multi-select";
import {
  GetRoomComplemantryServiceAction,
  GetRoomTypeAction,
} from "../../../redux/actions/HotelServices";
import { connect } from "react-redux";
import {
  AddHotelRoomsAction,
  UpdateRoomDetailAction,
} from "../../../redux/actions/UpdateRoomDetails";
import AsyncStorage from "@react-native-community/async-storage";
import { API } from "../../../redux/constant";
import { FONT_FAMILY } from "../../../utils/Font";
import { CommonActions } from "@react-navigation/native";
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import TimeSlotModelView from "../../hotelOwner/addHotelRoom/component/TimeSlotModel";

//==================Dismiss keyboard  =======================
const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

class UpdateCustomerRoomDetail extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      roomRegularPrice:undefined,
      roomId: undefined,
      isLoading: false,
      accessToken: undefined,
      value1: 1,
      ViewArray: [],
      textInput: [],
      Disable_Button: false,
      dataRoomType: [],
      selectedItems: [],
      isNewView: false,
      isAddShow: true,
      imagesList: [],
      authToken: undefined,
      data: [],
      complimentaryServices: [],
      imageArr2: [],
      hotelData: [],
      isHideLabel: false,
      previousRegularPrice: undefined,
      defaultdiscountBoxColor: COLORS.app_theme_color,
      timSlotModelView: false,
      currentIndexClicked: undefined,
    };
  }

  componentDidMount() {
    this.GetRoomType(this.props.route.params.accessToken);
    this.setState({
      accessToken: this.props.route.params.accessToken,
    });
  }

  //*********************************component will receive props ********************** */

  componentWillReceiveProps(nextProps, nextContext) {
    const { RoomDetailResponse } = nextProps.hotelUpdatedRoomState;
    this.setState({ isLoading: false });
    this.hideDialog();
    if (RoomDetailResponse === "Network request failed") {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (RoomDetailResponse !== undefined && RoomDetailResponse !== null) {
        const { response, message, status } = RoomDetailResponse;
        if (status === 200) {
          this.hideDialog();
          if (response !== undefined && response !== null) {
            this.showToastSucess(message);
            this.props.navigation.goBack();
          }
        } else if (status === 111) {
          this.hideDialog();

          this.showToastAlert(message);
        } else {
          this.hideDialog();
        }
      } else {
        this.hideDialog();
      }
    }
    this.hideDialog();
  }

  //======================== action methods =======================

  GetRoomType = (tokenAuth) => {
    let payload = {
      authToken: tokenAuth,
    };
    this.setState({ isLoading: true });
    GetRoomTypeAction(payload, (res) => this.RoomTypeApiResponse(res));
  };

  RoomTypeApiResponse(response1) {
    const { navigation } = this.props;
    this.setState({ isLoading: false });
    this.hideDialog();
    if (response1.response !== undefined && response1.status === 200) {
      this.setState({ dataRoomType: response1.response });
      this.GetRoomComplemantryService(this.props.route.params.accessToken);
    } else if (response1.status === 111) {
      AsyncStorage.removeItem(STRINGS.loginCredentials);
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{ name: "login" }],
        })
      );
      this.showToastAlert(message);
    } else {
      this.showToastAlert("server error");
    }
    this.hideDialog();
  }

  GetRoomComplemantryService = (tokenAuth) => {
    let payload = {
      authToken: tokenAuth,
    };
    this.setState({ isLoading: true });
    GetRoomComplemantryServiceAction(payload, (res) =>
      this.RoomComplimentaryServiceResponse(res)
    );
  };

  RoomComplimentaryServiceResponse(response4) {
    const { dataRoomType } = this.state;
    this.setState({ isLoading: false });
    this.hideDialog();
    if (response4.response !== undefined && response4.status === 200) {
      // this.setState({ items: response4.response });
      let fruit = {
        name: "Complimentary Serivces",
        id: 0,
        children: [],
      };
      this.state.complimentaryServices.push(fruit);
      response4.response.forEach((element) => {
        var data = {
          name: element.name,
          id: element.id,
        };
        fruit.children.push(data);
      });

      let receivedData = this.props.route.params.roomData;
      let roomData = [];

      let services = receivedData.room_service;
      let result = services.map((a) => a.service_id);

      let images = [];
      receivedData.room_images.map((item) => {
        let imageUrl = API.IMAGE_BASE_URL + item.image;
        images.push({
          uri: imageUrl,
          id: item.id,
        });
      });

      let roomType = receivedData.room_type;
      let r_type = dataRoomType.filter((el) => {
        if (el.name === roomType) {
          return el.id;
        } else {
        }
      });

      let r_type_id = 0;
      r_type.forEach((item) => {
        r_type_id = item.id;
      });

      for (let i = 0; i < 1; i++) {
        roomData.push({
          room_type: r_type_id,
          room_type_value: receivedData.room_type,
          room_images: images,
          regular_price: receivedData.regular_price,
          discounted_price: receivedData.discounted_price,
          no_of_guest: receivedData.no_of_guest,
          room_service: result,
          IsDiscountApplied: false,
          isMaxPicComplete: false,
          no_of_hours: receivedData.no_of_hours,
          defaultdiscountBoxColor: COLORS.app_theme_color,
        });
      }

      this.setState({
        data: roomData,
        roomId: receivedData.id,
        isHideLabel: true,
        previousRegularPrice: receivedData.regular_price,
        roomRegularPrice:roomData[0].regular_price
      });
    } else {
      this.showToastAlert("server error");
    }
    this.hideDialog();
  }

  //======================== add image =======================
  AddIcon = (index) => {
    return (
      this.state.isAddShow && (
        // <View style={styles.addIconOuterView}>
        <TouchableWithoutFeedback onPress={() => this.AddImageAction(index)}>
          <View style={styles.imgOuterView}>
            <TouchableWithoutFeedback
              onPress={() => this.AddImageAction(index)}
            >
              <Image style={styles.hotelImage} source={ICONS.ADD_IMAGE_ICON} />
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
        // </View>
      )
    );
  };

  //=====================item separator=====================
  _itemSeparator = () => <View style={styles.itemSepratorStyle} />;

  //============================ render header ===============================

  _renderHeader() {
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={<LeftIcon onPress={() => navigation.goBack()} />}
        // rightComponent={<RightIcon onPress={() => this.logoutUser()}/>}
        centerComponent={{
          text: STRINGS.HEADER_ROOM_DETAIL,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  //============================ Add Image Button Action=====================================
  AddImageAction(index) {
    const { data, imageArr2 } = this.state;
    ImagePicker.openPicker({
      compressImageQuality: 0.2,
      includeBase64: true,
      multiple: true,
      maxFiles: 10,
      mediaType: "photo",
    }).then((response) => {
      let tempArray = [];
      let tempArray2 = imageArr2;
      response.forEach((item) => {
        tempArray.push(item);
      });
      let count = data[index].room_images.length + response.length;
      if (count <= 10) {
        _.map(data, (item, index1) => {
          if (index === index1) {
            //new code
            for (let i = 0; i < tempArray.length; i++) {
              let image = {
                completeData: tempArray[i],
                uri: tempArray[i].path,
              };

              item.room_images.push(image);
            }
            //end
          }
          this.setState({
            refresh: !this.state.refresh,
          });
        });
      } else {
        this.showToastAlert(STRINGS.ONLY_TEN_IMAGES);

        data[index].isMaxPicComplete = true;

        //alert show and remove image icon from that index
      }
    });
  }

  _renderChooseImage = (item, index) => {
    return (
      <FlatList
        style={styles.flatListStyle}
        data={item.room_images}
        renderItem={(item1, index1) =>
          this._renderImageList(item1, index1, index)
        }
        horizontal={true}
        showsHorizontalScrollIndicator={true}
        keyExtractor={(item, index) => index}
        ItemSeparatorComponent={this._itemSeparator}
        extraData={this.state.refresh}
        // keyExtractor={(item, index) => index}
        selected={this.state.selected}
        nestedScrollEnabled={true}
        contentContainerStyle={styles.flatListContainerStyle}
      />
    );
  };

  _renderImageList = (item, index, outerIndex) => {
    return (
      <ImageBackground
        style={styles.imageStyle}
        // source={{uri: item !== undefined ? item.item.completeData.uri : ""}}
        source={{ uri: item !== undefined ? item.item.uri : "" }}
        imageStyle={styles.crossIconOuterView}
      >
        <TouchableWithoutFeedback
          onPress={() => this.CrossAction(item, item.index, outerIndex)}
        >
          <Image style={styles.crossIconStyle} source={ICONS.CROSS_ICON} />
        </TouchableWithoutFeedback>
      </ImageBackground>
    );
  };

  //========================= Remove  Image from list  Action==========================

  CrossAction(i, index, parentIndex) {
    /**remove value from imagelist */
    const { data, imageArr2 } = this.state;
    let tempArr = imageArr2;
    let imagesData = data[parentIndex].room_images;

    /** Add image ids to new array for deletion */
    for (let j = 0; j < imagesData.length; j++) {
      if (index === j) {
        if (imagesData[j].id !== undefined) {
          tempArr.push(imagesData[j].id);
        }
      }
    }
    /** Deleting item from array */
    data[parentIndex].room_images.splice(index, 1);

    /** Setting deleted item's id to state */
    this.setState({
      imageArr2: tempArr,
      refresh: !this.state.refresh,
    });

    if (data[parentIndex].room_images.length !== 10)
      this.setState({ isAddShow: true });
  }

  //=======================end===================================
  //=================== room type handling=================
  onChangeDropdown = (value, id, index) => {
    const { data } = this.state;
    _.map(data, (item, index1) => {
      if (index === index1) {
        item.room_type = id + 1;
      }
    });
    this.setState({ data, isHideLabel: true });
  };

  //================================ discount price=========================
  discountAmount = (index) => {
    const { data, previousRegularPrice } = this.state;

    if (data[index].regular_price < previousRegularPrice) {
      if (data[index].IsDiscountApplied === false) {
        _.map(data, (item, index1) => {
          if (index === index1) {
            if (item.regular_price !== "") {
              data[index].IsDiscountApplied = true;
              item.discounted_price =
                parseInt(item.regular_price) -
                (parseInt(item.regular_price) * 20) / 100;

              item.defaultdiscountBoxColor = COLORS.failure_Toast;
            } else {
              this.showToastAlert("Regular price can not be starting from 0.");
            }

            this.setState({ data: data });
          }
        });
      } else this.showToastAlert("Discount already applied for this room");
    } else {
      this.showToastAlert(
        "current price should be smaller then previous current price"
      );
    }
  };

  //==================Number of guest====================

  updateRoomsData = () => {
    const { data, accessToken, roomId, imageArr2,roomRegularPrice } = this.state;
    let imagesData = data[0].room_images;
    let tempArr = imagesData.filter(function (e) {
      if (e.completeData !== undefined) {
        return e;
      }
    });

   
    let check = 0;
    _.map(data, (item, index) => {
      console.log("rgprice",data[index].regular_price)
      console.log("rgprice",roomRegularPrice)
      if (data[index].room_type === undefined || data[index].room_type === "")
        this.showToastAlert("Please select room type");
      else if (
        data[index].regular_price === undefined ||
        data[index].regular_price === ""
      )
        this.showToastAlert("Please enter valid regular price");
      else if (data[index].no_of_guest === undefined)
        this.showToastAlert("Please enter Number of guest");
      else if (data[index].room_images.length === 0)
        this.showToastAlert("Please upload atleast one room image");

      else if(data[index].defaultdiscountBoxColor=== COLORS.app_theme_color && data[index].regular_price!==roomRegularPrice )
      {
        this.showToastAlert("Please apply discount price");
      }
     
      else if (
        data[index].room_type === 6 &&
        data[index].no_of_hours === null
      ) {
        this.showToastAlert("Please select hours");
      } else {
        check = check + 1;
      }
    });

    if (check === data.length) {
      this.showDialog();
      this.props.updateHotel({
        data: data,
        accessToken: accessToken,
        id: roomId,
        imagesArray: tempArr,
        deletedImages: imageArr2,
      });
      this.setState({ isLoading: true });
     
    }
  };

  // ****************** on selected Item  *********************

  onSelectedItemsChange = (index, selectedItems) => {
    const { data } = this.state;

    _.map(data, (item, index1) => {
      if (index === index1) {
        item.room_service = selectedItems;
      }
    });
    this.setState({ data });
  };

  itemSlotClikeed = (index) => {
    const { timSlotModelView } = this.state;

    this.setState({ timSlotModelView: true, currentIndexClicked: index });

    // Alert.alert(
    //   "Time Slot",
    //   "",
    //   [
    //     {
    //       text: "1 hour",
    //       onPress: () => this.setTimeSlot("1", index),
    //     },
    //     {
    //       text: "2 hour",
    //       onPress: () => this.setTimeSlot("2", index),
    //     },
    //     {
    //       text: "3 hour",
    //       onPress: () => this.setTimeSlot("3", index),
    //     },
    //     {
    //       text: "4 hour",
    //       onPress: () => this.setTimeSlot("4", index),
    //     },
    //     {
    //       text: "5 hour",
    //       onPress: () => this.setTimeSlot("5", index),
    //     },
    //     {
    //       text: "6 hour",
    //       onPress: () => this.setTimeSlot("6", index),
    //     },
    //     {
    //       text: "7 hour",
    //       onPress: () => this.setTimeSlot("7", index),
    //       // this.setState({ starSelected: 1, selectStarMarginLeft: wp("13%") }),
    //     },
    //     {
    //       text: "8 hour",
    //       onPress: () => this.setTimeSlot("8", index),
    //     },
    //     {
    //       text: "9 hour",
    //       onPress: () => this.setTimeSlot("9", index),
    //     },
    //     {
    //       text: "10 hour",
    //       onPress: () => this.setTimeSlot("10"),
    //     },
    //     {
    //       text: "11 hour",
    //       onPress: () => this.setTimeSlot("11", index),
    //     },
    //     {
    //       text: "12 hour",
    //       onPress: () => this.setTimeSlot("12", index),
    //     },
    //   ],
    //   { cancelable: false }
    // );
  };

  setTimeSlot = (timeInHour, index) => {
    const { data } = this.state;
    data[index].no_of_hours = timeInHour;
    this.setState({ data });
  };

  _renderUpdateAddMoreBtn = () => {
    return (
      <View>
        <Spacer space={3} />

        <TouchableOpacity onPress={() => this.updateRoomsData()}>
          <View style={styles.buttonStyle}>
            <Text style={styles.buttonText}>{STRINGS.UPDATE_BTN}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  //======================== Add more rooms =============================

  //*******************************Discount Amount ***************** */

  onChangeText = (text, index) => {
    const { data } = this.state;
    _.map(data, (item, index1) => {
      if (index === index1) {
        if (text.charAt(0) !== "0") {
          item.regular_price = text;
        } else {
          item.regular_price = "";
        }
        data[index].IsDiscountApplied = false;
        data[index].defaultdiscountBoxColor = COLORS.app_theme_color
      }
    });
    this.setState({
      data,
    });
  };

  // =================== no of guest action =======================
  numberOfGuest = (num, index) => {
    const { data } = this.state;
    _.map(data, (item, index1) => {
      if (index === index1) {
        item.no_of_guest = num;
      }
    });
    this.setState({ data });
  };

  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  //*******************************remove View ***************** */
  closeRoomView = (closeWindowIndex) => {
    const list = this.state.data;

    list.splice(closeWindowIndex, 1);
    this.setState({ data: list });
  };

  //******************************* loader ***************** */

  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  //================== hotel image flat list item=====================

  _renderHotelList = (item, index) => {
    const {
      dataRoomType,
      isHideLabel,
      complimentaryServices,
      data,
    } = this.state;
    let tempRoomType = [];

    dataRoomType.forEach((item) => {
      let aa = {
        value: item.name,
        id: item.id,
      };

      tempRoomType.push(aa);
    });
    return (
      <View>
        <View style={styles.roomTypeOuterView}>
          {index !== 0 && (
            <TouchableOpacity
              onPress={() => this.closeRoomView(index)}
              hitSlop={styles.closeRoomTouchStyle}
            >
              <Image
                source={ICONS.CLOSE_WINDOW}
                style={styles.closeWindowStyle}
              />
            </TouchableOpacity>
          )}
          <ShadowViewContainer>
            <Dropdown
              containerStyle={{ width: wp("90%"), paddingHorizontal: wp(5) }}
              defaultValue={item.room_type_value}
              label={!isHideLabel && "Room Type"}
              data={tempRoomType}
              onChangeText={(value, id) =>
                this.onChangeDropdown(value, id, index)
              }
            />
          </ShadowViewContainer>
        </View>

        <View style={styles.hotelImageView}>
          <View style={{ flexDirection: "row" }}>
            {this._renderChooseImage(item, index)}
            {this.AddIcon(index)}
          </View>
        </View>

        <View style={styles.hotelView}>
          <Spacer space={3} />

          {item.room_type === 6 ? (
            <TouchableOpacity onPress={() => this.itemSlotClikeed(index)}>
              <Text style={styles.timeSlotTextStyle}>
                {item.no_of_hours} hour
              </Text>

              <Spacer space={3} />
            </TouchableOpacity>
          ) : (
            console.log("")
          )}

          <TextInput
            style={styles.RegularPriceTextStyle}
            placeholder="Regular Price"
            placeholderTextColor="gray"
            defaultValue={item.regular_price.toString()}
            keyboardType="numeric"
            onChangeText={(text) => {
              this.onChangeText(text, index);
            }}
          />
          <Spacer space={3} />
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "space-around",
            }}
          >
            <Text style={styles.TextComponentStyle}>
              {item.discounted_price} {"\u20B9"}
            </Text>

            <TouchableOpacity>
              <Text
                style={[
                  styles.TextDiscountStyle,
                  {
                    borderColor: item.defaultdiscountBoxColor,
                    color: item.defaultdiscountBoxColor,
                  },
                ]}
                onPress={() => this.discountAmount(index)}
              >
                20% discount
              </Text>
            </TouchableOpacity>
          </View>

          <Spacer space={3} />
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              justifyContent: "space-around",
            }}
          >
            <Text style={styles.TextComponentStyle}>
              {STRINGS.no_of_Guests}
            </Text>
            <NumericInput
              type="up-down"
              totalHeight={wp("11%")}
              totalWidth={wp("30%")}
              minValue={0}
              maxValue={10}
              value={item.no_of_guest.toString()}
              onChange={(value) => this.numberOfGuest(value, index)}
              rounded
              containerStyle={{ marginRight: wp("5%") }}
            />
          </View>
          <Spacer space={3} />
          <SectionedMultiSelect
            items={complimentaryServices}
            uniqueKey="id"
            subKey="children"
            selectText="Complimentary Services"
            showDropDowns={true}
            readOnlyHeadings={true}
            onSelectedItemsChange={this.onSelectedItemsChange.bind(this, index)}
            selectedItems={item.room_service}
          />
          <Spacer space={2} />
        </View>
      </View>
    );
  };


  timeSlotAction = (hour) => {
    const { currentIndexClicked,timSlotModelView } = this.state;

    if (currentIndexClicked !== undefined)
    this.setTimeSlot(hour,currentIndexClicked),
      this.setState({timSlotModelView:false})
  };

  //===================== render method ==========================
  render() {
    const { data, timSlotModelView } = this.state;
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <DismissKeyboard>
          <ScrollView>
            <View style={styles.MainContainer}>
              <FlatList
                extraData={this.state}
                contentContainerStyle={{ paddingBottom: 30 }}
                data={data}
                keyExtractor={(item) => item.key}
                renderItem={({ item, index }) =>
                  this._renderHotelList(item, index)
                }
              />
              {timSlotModelView == true && (
                <TimeSlotModelView
                  isTimeSlotVisible={timSlotModelView}
                  closeRoomModal={() =>
                    this.setState({ timSlotModelView: false })
                  }
                  timeSlotClicked={(hour) => this.timeSlotAction(hour)}
                />
              )}
              {this._renderUpdateAddMoreBtn()}
              {this._renderCustomLoader()}
            </View>
            {this._renderCustomLoader()}
          </ScrollView>
        </DismissKeyboard>
      </SafeAreaViewContainer>
    );
  }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state) => ({
  hotelUpdatedRoomState: state.UpdateRoomDetailsReducer,
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    updateHotel: (payload) => dispatch(UpdateRoomDetailAction(payload)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UpdateCustomerRoomDetail);
