import React from "react";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {StyleSheet, Dimensions} from "react-native";
import {FONT} from "../../../utils/FontSizes";
import COLORS from "../../../utils/Colors";
import {FONT_FAMILY} from "../../../utils/Font";

const window = Dimensions.get("window");

const styles = StyleSheet.create({
    mainProfileContainer: {
        height: hp("100%"),
        alignItems: "center",
        backgroundColor: COLORS.white_color,
    },

    MainContainer: {
        flex: 1,
        backgroundColor: COLORS.white_color,
        width: wp("100%"),
    },

    roomTypeStyle: {
        alignSelf: "center",
        backgroundColor: COLORS.white_color,
        borderWidth: 2,
        borderRadius: wp("2%"),
        borderColor: "#ddd",
        borderBottomWidth: 0,
        shadowColor: "#000",
        elevation: 5,
        justifyContent: "center",
        height: wp("10%"),
        paddingLeft: wp("2%"),
        paddingRight: wp("2%"),
    },
    textViewStyle: {
        width: wp("90%"),
        height: wp("10%"),
        backgroundColor: COLORS.white_color,
        borderWidth: 2,
        borderRadius: wp("2%"),
        borderColor: "#ddd",
        borderBottomWidth: 0,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        justifyContent: "center",
    },
    input: {
        paddingVertical: wp("0.5%"),
        marginLeft: wp("2%"),
    },

    imgOuterView: {
        height: wp("20%"),
        width: wp("20%"),
        borderRadius: wp("20%") / 2,
        backgroundColor: COLORS.backGround_color,
        marginRight: wp("3%"),
        justifyContent: "center",
        alignItems: "center",
    },
    imageStyle: {
        height: wp("20%"),
        width: wp("20%"),
        borderRadius: wp("20%") / 2,
    },

    buttonStyle: {
        marginVertical: 10,
        alignSelf: "center",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: COLORS.app_theme_color,
        height: wp("12%"),
        width: wp("80%"),
        borderRadius: wp("12%") / 2,
        fontSize: wp('4.5%'),
        fontWeight: "700"
    },

    changePasswordStyle: {
        marginVertical: 10,
        alignSelf: "center",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: COLORS.white_color,
        height: wp("12%"),
        width: wp("80%"),
        borderRadius: wp("12%") / 2,
        borderColor: COLORS.app_theme_color,
        borderWidth: 2,
        fontSize: wp('4.5%'),
        fontWeight: "700"
    },

    changePasswordText: {
        color: COLORS.app_theme_color,

        // fontWeight: "700",
        fontSize: FONT.TextMedium,
        fontFamily: FONT_FAMILY.Montserrat

    },

    buttonText: {
        fontSize: FONT.TextMedium,
        fontFamily: FONT_FAMILY.Montserrat,
        lineHeight: 20,
        display: "flex",
        color: "#FFFFFF",
        textAlign: "center",
    },

    hotelAddressTextViewStyle: {
        width: wp("90%"),
        height: wp("11%"),
        backgroundColor: COLORS.white_color,
        borderWidth: 2,
        borderRadius: wp("2%"),
        borderColor: "#ddd",
        borderBottomWidth: 0,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        flexDirection: "row",
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        justifyContent: "center",
    },
    hotelAddressTextStyle: {
        width: wp("75%"),
        height: wp("11%"),
        marginLeft: wp("7%"),
    },

    addIconOuterView: {
        justifyContent: "center",
        alignSelf: "flex-end",
    },

    hotelImage: {
        height: wp("10%"),
        width: wp("10%"),
        alignSelf: "center",
        marginLeft: wp("0.5%"),
    },
    flatListStyle: {
        width: wp("60%"),
        marginLeft: wp("5%"),
    },

    crossIconOuterView: {
        borderRadius: wp("20%") / 2,
        resizeMode: "cover",
    },
    crossIconStyle: {
        height: 15,
        width: 15,
        alignSelf: "flex-end",
        marginLeft: 15,
        marginBottom: 15,
    },

    hotelImageView: {
        height: wp("25%"),
        width: wp("100%"),
        backgroundColor: COLORS.white_color,
        paddingVertical: wp("2%"),
    },
    hotelView: {
        flex: 1,

        width: wp("90%"),
        alignSelf: "center",
        marginTop: wp("5%"),
        backgroundColor: COLORS.white_color,
        borderWidth: 2,
        borderRadius: wp("2%"),
        borderColor: "#ddd",
        borderBottomWidth: 0,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },

        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
        justifyContent: "center",
    },

    RegularPriceTextStyle: {
        alignSelf:'center',
        fontSize: FONT.TextSmall,
        fontFamily: FONT_FAMILY.Montserrat,
        marginRight: wp("5%"),
        marginLeft: wp("5%"),
        width: wp("80%"),
        height: wp("12%"),
        backgroundColor: COLORS.white_color,
        borderWidth: 2,
        borderRadius: wp("2%"),
        borderColor: "#ddd",
        elevation: 5,
        justifyContent: "center",
        paddingLeft: wp("2%"),
        paddingRight: wp("2%"),

    },

    timeSlotTextStyle:{
        fontSize: FONT.TextSmall,
        fontFamily: FONT_FAMILY.Montserrat,
        marginRight: wp("5%"),
        marginLeft: wp("5%"),
        width: wp("80%"),
        height: wp("12%"),
        backgroundColor: COLORS.white_color,
        borderWidth: 2,
        borderRadius: wp("2%"),
        borderColor: "#ddd",
        elevation: 5,
        padding: wp("2%"),
        alignSelf:'center',

        

    },

    roomTypeOuterView: {
        backgroundColor: COLORS.backGround_color,
        height: wp("25%"),
        justifyContent: "center",
    },
    TextComponentStyle: {
        alignSelf: "flex-start",
        fontSize: FONT.TextSmall,
        fontFamily: FONT_FAMILY.Montserrat,
        marginRight: wp("5%"),
        marginLeft: wp("5%"),
        width: wp("50%"),
        height: wp("12%"),
        backgroundColor: COLORS.white_color,
        borderWidth: 2,
        borderRadius: wp("2%"),
        borderColor: "#ddd",
        elevation: 5,
        justifyContent: "center",
        paddingLeft: wp("2%"),
        paddingRight: wp("2%"),
        padding: wp('2.5%')
    },
    TextDiscountStyle: {
        borderRadius: wp("2%"),
        borderWidth: 2,
        color: "red",
        fontSize: FONT.TextSmall_2,
        fontFamily: FONT_FAMILY.Montserrat,
        textAlign: "center",
        height: wp("12%"),
        width: wp("30%"),
        marginRight: wp("5%"),

        padding: 10,
        borderColor: "red",
    },
    complementryViewStyle: {
        backgroundColor: "white",
    },
    itemSepratorStyle: {
        width: wp("5%"),
    },
    flatListContainerStyle: {
        flexGrow: 1,
    },

    closeRoomTouchStyle: {
        top: 20,
        bottom: 20,
        left: 50,
        right: 50,
    },
    closeWindowStyle: {
        position: "absolute",
        right: 5,
        top: -wp("4%"),
        height: 30,
        width: 30,
    },
});
export default styles;
