// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react";
import {
  View,
  FlatList,
  Text,
  Image,
  Platform,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
  Dimensions,
} from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Header } from "react-native-elements";
import AsyncStorage from "@react-native-community/async-storage";
import { CommonActions } from "@react-navigation/native";
import * as _ from "lodash";

import BaseClass from "../../../utils/BaseClass";
import {
  ShadowViewContainer,
  SafeAreaViewContainer,
  MainContainer,
  ScrollContainer,
} from "../../../utils/BaseStyle";
import { Spacer } from "../../../customComponents/Spacer";
import COLORS from "../../../utils/Colors";
import {
  ListingIcon,
  LocationIcon,
  RightIcon,
  DeleteIcon,
} from "../../../customComponents/icons";
import STRINGS from "../../../utils/Strings";
import { FONT } from "../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../utils/Font";
import styles from "./styles";
import { LeftIcon } from "@customComponents/icons";
import { ICONS } from "../../../utils/ImagePaths";

import moment from "moment";

const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

class Help extends BaseClass {


  _renderHeader(props) {
    const { navigation, comesFrom } = this.props;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={this.props.route.params.comesFrom !== undefined ? <LeftIcon onPress={() => navigation.goBack()}/> : <LeftIcon drawer onPress={() => navigation.openDrawer()}/> }
        centerComponent={{
          text: "Help",
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  render() {
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <DismissKeyboard>
          <View
            style={{
              flex: 1,
              alignItems: "center",
              backgroundColor: "white",
              height: wp("100%"),
              borderRadius: wp("3%"),
              borderColor: "gray",
              marginLeft: wp("5%"),
              marginTop: wp("5%"),
              marginBottom: wp("5%"),
              marginRight: wp("5%"),
              borderWidth: wp("0.5%"),
            }}
          >
            <View
              style={{
                backgroundColor: "white",
                padding: wp("2%"),
              }}
            >
              <Text>
                All premium plans come with a custom domain name for free, but
                if you prefer, you are welcome to use one that you already own
                at no added cost! There are 2 options to use your own domain:
                Option 1)You can keep your domain name with your existing domain
                registrar, and simply point the site built with the Website.com
                site builder to your preferred domain name (i.e. Connect Your
                Domain option). Option 2)Transfer your domain to Website.com. A
                transfer is included for free with a premium plan subscription.
                With this option, you will get your domain renewal for free.
                {"\n"} {"\n"}
                Yes! Personalized email addresses @ your domain name
                (name@yourwebsitename.com) can boost your reputation and
                professionalism instantly. That's why we include custom email
                addresses for free with Business and Ecommerce premium plans.
                {"\n"} {"\n"}
                While you can choose to subscribe to a monthly term, you save
                even more on your plan by choosing a yearly plan term length.
                Plus, your setup fee will be waived with a yearly plan term
                length subscription.
              </Text>
            </View>
          </View>
        </DismissKeyboard>
      </SafeAreaViewContainer>
    );
  }
}

export default Help;
