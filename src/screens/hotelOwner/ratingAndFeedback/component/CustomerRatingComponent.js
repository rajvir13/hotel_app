import React from "react";
import {
    View,
    FlatList,
    Text,
    Image,
    Platform,
    StatusBar,
    TouchableOpacity,
    TextInput,
    TouchableWithoutFeedback,
    Keyboard,
    Alert,
} from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {ICONS} from "../../../../utils/ImagePaths";
import styles from "../styles";
import {SearchBar} from "react-native-elements";
import {Dropdown} from "react-native-material-dropdown";
import SearchIconSVG from "../../../../../assets/images/CustomerHome/searchIcon.svg";
import {
    SafeAreaViewContainer,
    MainContainer,
    ShadowViewContainer,
} from "../../../../utils/BaseStyle";
// import { Rating, AirbnbRating } from "react-native-elements";
import COLORS from "../../../../utils/Colors";
import {Spacer} from "../../../../customComponents/Spacer";
import {AirbnbRating} from "react-native-elements";

//================== room type and sorting view ===================

//================== search hotel view ===================

const RenderUserComments = (props) => {
    return (
        <View style={styles.viewContainer}>
            <Text style={styles.commentText}>{props.item.comment}</Text>

            <View style={styles.viewLine}/>

            <View
                style={{
                    flexDirection: "row",
                    marginTop: wp("3%"),
                    marginBottom: wp("3%"),
                    justifyContent: "space-evenly",
                }}
            >
                <View style={{flexDirection: "row", width: wp('30%'), justifyContent: 'center'}}>
                    <Text style={styles.givenByText}>Given by :</Text>
                    <Text style={styles.givenByName}>{props.item.customer_name}</Text>
                </View>
                <View>
                    <Text style={styles.givenByText}>{props.item.comment_created}</Text>
                </View>
                <View>
                    <View>
                        <AirbnbRating
                            isDisabled={true}
                            type={"star"}
                            starStyle={{margin: 0}}
                            size={wp(4)}
                            showRating={false}
                            // defaultRating={item.rating}
                            defaultRating={props.item.rating}
                        />
                    </View>
                </View>
            </View>
        </View>
    );
};

const HeaderCalenderView = (props) => {
    return (
        <View style={styles.headerCalenderView}>
            <View style={styles.calendar_Style}>
                <View style={styles.calendar_Style1}>
                    <Text style={styles.fromAndToText}>From</Text>
                    <TouchableOpacity onPress={props.fromDateTimeClikced}>
                        <Image
                            source={ICONS.CALENDER_IMG}
                            style={styles.calenderIcon}
                        />
                    </TouchableOpacity>
                    <Text style={styles.dateTimeText}>{props.fromDate}</Text>
                </View>
                <Spacer row={0.5}/>
                <View style={styles.calendar_Style1}>
                    <Text style={styles.fromAndToText}>To</Text>
                    <TouchableOpacity onPress={props.toDateTimeClikced}>
                        <Image
                            source={ICONS.CALENDER_IMG}
                            style={styles.calenderIcon}
                        />
                    </TouchableOpacity>
                    <Text style={styles.dateTimeText}>{props.toDate}</Text>
                </View>
            </View>
            <Spacer row={0.5}/>
            <View style={{alignItems:'center'}}>
                <TouchableOpacity style={styles.slectStarView} onPress={props.selectStarClick}>
                    <Text style={styles.selectStartsText}>{props.starSelected} </Text>
                    <Image
                        source={ICONS.UPDOWN_ARROW}
                        style={[styles.upDownArrowIcon, {marginLeft: props.selectStarMarginLeft}]}
                    />
                </TouchableOpacity>
            </View>
        </View>
    );
};

export {HeaderCalenderView, RenderUserComments};
