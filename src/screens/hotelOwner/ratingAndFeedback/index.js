/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import React from "react";
import {
  View,
  Text,
  Image,
  Keyboard,
  Alert,
  StatusBar,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Platform,
  FlatList,
} from "react-native";
import styles from "./styles";
import BaseClass from "../../../utils/BaseClass";
import STRINGS from "../../../utils/Strings";
import { Header } from "react-native-elements";
import { Spacer } from "../../../customComponents/Spacer";
import COLORS from "../../../utils/Colors";
import { LeftIcon } from "../../../customComponents/icons";
import { FONT } from "../../../utils/FontSizes";
import {
  SafeAreaViewContainer,
  MainContainer,
  ShadowViewContainer,
  ScrollContainer,
} from "../../../utils/BaseStyle";
import moment from "moment";

import CustomTextInput from "../../../customComponents/CustomTextInput";
import { ICONS } from "../../../utils/ImagePaths";
import DateTimePickerModal from "react-native-modal-datetime-picker";
// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import { FONT_FAMILY } from "../../../utils/Font";
import { API } from "../../../redux/constant";
import {
  HeaderCalenderView,
  RenderUserComments,
} from "./component/CustomerRatingComponent";
import { Button, Paragraph, Dialog, Portal } from "react-native-paper";
import { GetRatingHotelOwner } from "../../../redux/actions/GetRatingHotelOwner";
import RatingModelView from "./component/RatingModel";
const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

const AllRatingData = [];

class RatingAndFeedback extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isDatePickerVisible: false,
      currentModelClicked: undefined,
      toDate: undefined,
      fromDate: undefined,
      starSelected: "Select Stars",
      selectStarMarginLeft: wp("0%"),
      accessToken: undefined,
      hotelId: undefined,
      ratingData: undefined,
      ratingCount: 0,
      isToClicked: false,
      isFromClicked: false,
      isRatingModelVisible: false,
      avgRating: undefined,
    };
  }

  componentDidMount() {
    let currentDate = moment().utcOffset("+05:30").format("YYYY-MM-DD");

    let monthBeforDate = moment(currentDate)
      .subtract(30, "days")
      .format("YYYY-MM-DD");

    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);

   if(loginData.hotel_details!==undefined)
   {
    this.setState({
      accessToken: loginData.access_token,
      hotelId: loginData.hotel_details.id,
      toDate: currentDate,
      fromDate: monthBeforDate,
    });
    this.getRatingWithCommentsHotel(
      loginData.hotel_details.id,
      loginData.access_token,
      currentDate,
      monthBeforDate,
      ""
    );
   }
   else
   {
    this.setState({
      accessToken: loginData.access_token,
      hotelId: loginData.owner.id,
      toDate: currentDate,
      fromDate: monthBeforDate,
    });
    this.getRatingWithCommentsHotel(
      loginData.owner.id,
      loginData.access_token,
      currentDate,
      monthBeforDate,
      ""
    );

   }

        
      }
    });
  }

  getRatingWithCommentsHotel = (
    hotelId,
    accessToken,
    toDate,
    fromDate,
    star
  ) => {
    if (toDate > fromDate) {
      this.showDialog();

      GetRatingHotelOwner(
        {
          hotelId: hotelId,
          accessToken: accessToken,
          toDate: toDate,
          fromDate: fromDate,
          star: star,
        },
        (data) => this.getRatingResponse(data)
      );
    } else {
      this.setState({ fromDate: "__/__/__" });
      this.showToastAlert("From date should be shorter then to date");
    }
  };

  getRatingResponse = (data) => {
  
  console.log("rating data is",data)
    this.hideDialog();
    const { response, message, status } = data;

    if (data === "Network request failed") {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (response !== undefined) {
        if (status === 200) {
          
         

          if(response.length > 0)
          {
            this.setState({
              ratingData: response,
              ratingCount: response.length,
              avgRating: response[0].avg_rating.rating__avg,
            });
          }
          else
          {
            this.setState({
              ratingData: response,
              ratingCount: response.length,
            });

          }
        } else if (status === 111) {
          this.showToastAlert(message);
        } else if (status === 401) {
          this.showToastAlert(message);
        } else {
          this.showToastAlert(message);
        }
      } else {
        this.showToastAlert("Server Error");
      }
    }
  };

  //============================= Render Hedaer==========================
  _renderHeader() {
    const { navigation } = this.props;
    const { avgRating } = this.state;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={this.props.route.params.comesFrom !== undefined ? <LeftIcon onPress={() => navigation.goBack()}/> : <LeftIcon drawer onPress={() => navigation.openDrawer()}/> }

        rightComponent={
          <Text
            style={{
              color: COLORS.avgRatingColor,
              fontWeight: "bold",
              fontSize: wp("4%"),
            }}
          >
            {avgRating !== undefined ? avgRating.toFixed(1): ""}
          </Text>
        }
        centerComponent={{
          text: "Rating and Feedback",
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  getRatingWithCommentsHotel = (
    hotelId,
    accessToken,
    toDate,
    fromDate,
    star
  ) => {
    if (toDate > fromDate) {
      this.showDialog();

      GetRatingHotelOwner(
        {
          hotelId: hotelId,
          accessToken: accessToken,
          toDate: toDate,
          fromDate: fromDate,
          star: star,
        },
        (data) => this.getRatingResponse(data)
      );
    }
  };

  //================date picker clicke============

  onDateConfirm = (date) => {
    const { currentModelClicked, fromDate, toDate } = this.state;
    const newDate = moment(new Date(date.toString().substr(0, 16))).format(
      "YYYY-MM-DD"
    );

    if (currentModelClicked !== undefined) {
      if (currentModelClicked === "To") {
        let dateFrom = new Date(fromDate);
        let dateTo = new Date(newDate);

        if (dateFrom < dateTo) {
          this.hideDatePicker();
          this.setState({ toDate: newDate });
          this.getFiltereByDate(newDate);
        } else {
          alert("To date can't be less then from date");
        }
      } else {
        let dateFrom = new Date(newDate);
        let dateTo = new Date(toDate);

        if (dateFrom < dateTo) {
          this.hideDatePicker();
          this.setState({ fromDate: newDate });
          this.getDateFilterData(newDate);
        } else {
          alert("To date can't be less then from date");
        }
      }
    }
  };

  //=================date filter ==========================

  getFiltereByDate = (toDateSelected) => {
    const { fromDate, hotelId, accessToken, starSelected } = this.state;
    if (fromDate !== "__/__/__") {
      this.getRatingWithCommentsHotel(
        hotelId,
        accessToken,
        toDateSelected,
        fromDate,
        starSelected
      );
    }
  };

  getDateFilterData = (fromDate1) => {
    const { toDate, hotelId, accessToken, starSelected } = this.state;

    this.getRatingWithCommentsHotel(
      hotelId,
      accessToken,
      toDate,
      fromDate1,
      starSelected
    );
  };

  showDatePicker = () => {
    this.setState({ isDatePickerVisible: true });
  };

  hideDatePicker = () => {
    this.setState({ isDatePickerVisible: false });
  };

  //=============== to and from date time click handling
  _toDateTimeClikced = () => {
    this.setState({ currentModelClicked: "To" });
    this.showDatePicker();
  };

  _fromDateTimeClikced = () => {
    this.setState({ currentModelClicked: "From" });
    this.showDatePicker();
  };
  //===============end to and from date time click handling

  starClicked = () => {
    const { isRatingModelVisible } = this.state;
    this.setState({ isRatingModelVisible: true });
    // Alert.alert(
    //   "Rating",
    //   "",
    //   [
    //     {
    //       text: "1",
    //       onPress: () => this.starFilter(1),
    //       // this.setState({ starSelected: 1, selectStarMarginLeft: wp("13%") }),
    //     },
    //     {
    //       text: "2",
    //       onPress: () => this.starFilter(2),
    //     },
    //     {
    //       text: "3",
    //       onPress: () => this.starFilter(3),
    //     },
    //     {
    //       text: "4",
    //       onPress: () => this.starFilter(4),
    //     },
    //     {
    //       text: "5",
    //       onPress: () => this.starFilter(5),
    //     },
    //     {
    //       text: "None",
    //       onPress: () => this.noneStarsSelect(),
    //     },
    //   ],
    //   { cancelable: false }
    // );
  };

  noneStarsSelect = () => {
    const { toDate, accessToken, hotelId, fromDate } = this.state;
    this.setState({
      starSelected: "None",
      selectStarMarginLeft: wp("9%"),
      isRatingModelVisible: false,
    });

    if (fromDate !== "__/__/__") {
      this.getRatingWithCommentsHotel(
        hotelId,
        accessToken,
        toDate,
        fromDate,
        ""
      );
    } else this.showToastAlert("please select from date");
  };

  starFilter = (rating) => {
    const { toDate, accessToken, hotelId, fromDate } = this.state;
    this.setState({
      starSelected: rating,
      selectStarMarginLeft: wp("13%"),
      isRatingModelVisible: false,
    });

    if (fromDate !== "__/__/__") {
      this.getRatingWithCommentsHotel(
        hotelId,
        accessToken,
        toDate,
        fromDate,
        rating
      );
    } else this.showToastAlert("please select from date");
  };

  _renderCustomLoader = () => {
    // const {isLoading} = this.state;
    return (
      <OrientationLoadingOverlay
        // visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  render() {
    const {
      isDatePickerVisible,
      selectStarMarginLeft,
      toDate,
      fromDate,
      ratingData,
      starSelected,
      ratingCount,
      isRatingModelVisible,
    } = this.state;
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <DismissKeyboard>
          <ScrollContainer>
            <MainContainer>
              <Spacer space={2} />
              <HeaderCalenderView
                toDateTimeClikced={() => this._toDateTimeClikced()}
                fromDateTimeClikced={() => this._fromDateTimeClikced()}
                toDate={toDate}
                fromDate={fromDate}
                selectStarClick={() => this.starClicked()}
                starSelected={starSelected}
                selectStarMarginLeft={selectStarMarginLeft}
              />
              <View style={{ backgroundColor: "white" }}>
                <DateTimePickerModal
                  isVisible={isDatePickerVisible}
                  titleIOS="Pick a time"
                  mode="date"
                  locale="en_GB" // Use "en_GB" here
                  date={new Date()}
                  onConfirm={(time) => this.onDateConfirm(time)}
                  onCancel={this.hideDatePicker}
                />
                {isRatingModelVisible === true && (
                  <RatingModelView
                    isRatingModelVisible={isRatingModelVisible}
                    closeRoomModal={() =>
                      this.setState({
                        isRatingModelVisible: !isRatingModelVisible,
                      })
                    }
                    noneStarsSelect={() => this.noneStarsSelect()}
                    starFilter={(rating) => this.starFilter(rating)}
                  />
                )}
                {ratingCount !== 0 ? (
                  <FlatList
                    style={{ width: wp(100) }}
                    data={ratingData}
                    keyExtractor={(item, index) => index}
                    renderItem={({ item, index }) => (
                      <RenderUserComments item={item} />
                    )}
                  />
                ) : (
                  <View style={{ width: wp("100%"), height: wp("100%") }}>
                    <Text
                      style={{
                        paddingVertical: wp(3),
                        textAlign: "center",
                        fontSize: FONT.TextNormal,
                        fontFamily: FONT_FAMILY.MontserratBold,
                      }}
                    >
                      NO DATA{" "}
                    </Text>
                  </View>
                )}
              </View>
              {this._renderCustomLoader()}
            </MainContainer>
          </ScrollContainer>
        </DismissKeyboard>
      </SafeAreaViewContainer>
    );
  }
}

export default RatingAndFeedback;
