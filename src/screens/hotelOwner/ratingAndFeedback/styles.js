import React from "react";
import styled from "styled-components";
import COLORS from "../../../utils/Colors";
import { FONT } from "../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../utils/Font";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  viewContainer: {
    alignSelf: "center",
    width: wp("90%"),
    flex: 1,
    marginTop: wp("4%"),
    borderRadius: 12,
    shadowColor: "red",
    shadowOpacity: 0.2,
    shadowRadius: 2,
    shadowOffset: {
      height: 0.9,
      width: 0,
    },
    borderColor: "gray",
    borderWidth: 1,
  },
  commentText: {
    marginTop: wp("3%"),
    fontSize: FONT.TextSmall,
    color: COLORS.greyButton_color2,
    marginLeft: wp("2%"),
    marginRight: wp("2%"),
    paddingVertical: wp("1%"),
    fontFamily: FONT_FAMILY.Montserrat,
  },

  viewLine: {
    width: wp("90%"),
    backgroundColor: "gray",
    height: 1,
    flex: 1,
    marginTop: wp("1%"),
    marginBottom: wp("1%"),
  },

  givenByText: {
    fontWeight: "bold",
    fontFamily: FONT_FAMILY.Montserrat,
    fontSize: wp("3.5%"),
    
  },

  givenByName:{

  width:wp('20%'),
  fontSize: wp("3.5%"),
  color:'gray'

  },

  slectStarView: {
    flexDirection: "row",
    width: wp("25%"),
    backgroundColor: "white",
    height: wp("8%"),
    borderRadius: 12,
    shadowColor: "red",
    shadowOpacity: 0.2,
    shadowRadius: 2,
    shadowOffset: {
      height: 0.9,
      width: 0,
    },
    borderColor: "white",
    borderWidth: 1,
    justifyContent: "space-evenly",
    alignItems: "center",
    padding: 3,
  },
  calenderIcon: {
    height: wp("4%"),
    width: wp("4%"),
    marginLeft: wp("1%"),
    alignSelf: "center",
  },
  upDownArrowIcon: {
    height: wp("4%"),
    width: wp("4%"),
  },

  slectStarView: {
    flexDirection: "row",
    width: wp("28%"),
    backgroundColor: "white",
    height: wp("8%"),
    borderRadius: 12,
    shadowColor: "red",
    shadowOpacity: 0.2,
    shadowRadius: 2,
    shadowOffset: {
      height: 0.9,
      width: 0,
    },
    borderColor: "white",
    borderWidth: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  calenderIcon: {
    height: wp("4%"),
    width: wp("4%"),
    marginLeft: wp("1%"),
    alignSelf: "center",
  },
  upDownArrowIcon: {
    height: wp("4%"),
    width: wp("4%"),
    //  marginLeft:wp('1%'),
    marginRight: wp("0.5%"),
  },

  selectStartsText: {
    fontFamily: FONT_FAMILY.Montserrat,
  },

  selectStartsText: {
    marginLeft: wp("2%"),
    fontFamily: FONT_FAMILY.Montserrat,
  },
  calendar_Style: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    marginTop: 3,
  },

  calendar_Style1: {
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
    // marginTop: 3,
  },
  fromAndToText: {
    fontFamily: FONT_FAMILY.Montserrat,
    fontWeight: "bold",
    marginLeft: wp("1%"),
    fontSize: FONT.TextSmall_2,
  },
  dateTimeText: {
    marginLeft: wp("1%"),
    fontSize: FONT.TextExtraSmall,
    fontFamily: FONT_FAMILY.Montserrat,
  },
  headerCalenderView: {
    // height: wp("10%"),
    paddingBottom: wp(2),
    width: wp("100%"),
    flexDirection: "row",
    justifyContent: "space-evenly",
    alignItems: "center",
  },
});

export default styles;
