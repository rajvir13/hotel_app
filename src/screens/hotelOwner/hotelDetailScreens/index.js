// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React from "react";
import {
  FlatList,
  Image,
  Platform,
  Alert,
  ScrollView,
  StatusBar,
  Text,
  View,
} from "react-native";
import { Header } from "react-native-elements";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import AsyncStorage from "@react-native-community/async-storage";
import * as _ from "lodash";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

import BaseClass from "../../../utils/BaseClass";
import COLORS from "../../../utils/Colors";
import { LeftIcon, OwnerRightIcon } from "../../../customComponents/icons";
import STRINGS from "../../../utils/Strings";
import { FONT } from "../../../utils/FontSizes";
import {
  SafeAreaViewContainer,
  ScrollContainer,
  ShadowViewContainer,
} from "../../../utils/BaseStyle";
import { CommonActions } from "@react-navigation/native";
import { Spacer } from "../../../customComponents/Spacer";
import AddressView from "./component/addressView";
import HotelRoomsView from "./component/roomsAndAmenitiesView";
import PolicyDocument from "./component/policyDocument";
import {
  GetHotelDetailAction,
  GetHotelRoomDetailAction,
} from "../../../redux/actions/GetHotelDetail";
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import { styles } from "./styles";
import { FONT_FAMILY } from "../../../utils/Font";
import { DeleteRoomAction } from "../../../redux/actions/UpdateRoomDetails";
import FastImage from "react-native-fast-image";
import moment from "moment";
import { hideMenu } from "./component/roomsAndAmenitiesView/index";
let isUserActive = false;
export default class HotelDetails extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      ownerName: "",
      ownerLastName: "",
      email: "",
      contact: "",
      isAddressClicked: false,
      isRoomsClicked: false,
      isPolicyDocument: false,
      hotelName: "",
      hotelImage: "",
      hotelImages: [],
      hotelID: undefined,
      hotelDescription: "",
      roomImgModelVisible: false,
      currentClickedData: undefined,
      hotel_documents: [
        {
          file: "",
          title: "",
        },
      ],
      roomsData: [
        {
          id: 114,
          name: "KY Hotel",
          room_hotel: [
            {
              id: 21,
              room_type: "single",
              discounted_price: 440,
              no_of_guest: 1,
              room_service: [
                {
                  service: "AC",
                },
              ],
              room_images: [
                "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
              ],
            },
          ],
          room_type_count: 1,
          room_count: 1,
        },
      ],
      hotel_id: 0,
      authToken: "",
      address: "",
      check_in: "",
      check_out: "",
      phone_number: "",
      ownerId: "",
      hotelData: [],
      lat: "",
      long: "",
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.onFocusFunction();
    });
    // this.getHotelDetails();
  }

  getHotelDetails = () => {
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        let hotelId = "";
        if (loginData.hotel_details !== undefined) {
          hotelId = loginData.hotel_details.id;
        } else {
          hotelId = loginData.id;
        }
        this.setState({
          authToken: loginData.access_token,
          hotel_id: hotelId,
        });
        this.showDialog();
        GetHotelDetailAction(
          {
            id: hotelId,
            accessToken: loginData.access_token,
          },
          (data) => this.hotelResponse(data)
        );
        GetHotelRoomDetailAction(
          {
            id: hotelId,
            accessToken: loginData.access_token,
          },
          (data) => this.hotelRoomResponse(data)
        );

        // setTimeout(() => {
        //     this.hideDialog()
        // }, 1000)
      }
    });
  };

  //******************* method call every time ************** */
  onFocusFunction = () => {
    // this.componentDidMount()
    this.hideDialog()
    this.getHotelDetails();
  };

  componentWillUnmount() {
    this._unsubscribe();
    console.warn("************ screen leave hotelDetail **********");
  }

  //******************end here */

  hotelResponse = (data) => {
    this.hideDialog();
    const { navigation } = this.props;
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      this.hideDialog();
      const { response, message, status } = data;
      if (response !== undefined) {
        if (status === 200) {
          this.hideDialog();
          let imageData = response[0].hotel_images;
          let image = "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg";
          if (imageData.length !== 0) {
            if (imageData[0].image !== null) {
              image = imageData[0].image;
            }
          }
          this.setState({
            hotelData: response[0],
            ownerId: response[0].owner.id,
            hotel_name: response[0].name,
            address: response[0].address,
            check_in: response[0].check_in,
            check_out: response[0].check_out,
            phone_number: response[0].owner.mobile_number,
            hotelImage: image,
            ownerName: response[0].owner.first_name,
            ownerLastName: response[0].owner.last_name,
            email: response[0].owner.email,
            hotelImages: response[0].hotel_images,
            hotelID: response[0].id,
            hotelDescription: response[0].description,
            hotel_documents: response[0].hotel_documents,
            lat: response[0].latitude,
            long: response[0].longitude,
          });
        }
      } else if (status === 111) {
        this.hideDialog();
        if (message.non_field_errors !== undefined) {
          // if (message.non_field_errors[0] === "User with this social_id does not exist")
        } else {
          if (isUserActive === false) {
            isUserActive = true;
            AsyncStorage.removeItem(STRINGS.loginCredentials);
            navigation.dispatch(
              CommonActions.reset({
                index: 0,
                routes: [{ name: "login" }],
              })
            );
            this.showToastAlert(message);
          }
        }
      } else if (status === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      }
    }
    this.hideDialog();
  };

  hotelRoomResponse = (data) => {
    console.warn("room respone is", data);
    this.hideDialog();
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      const { response, message, status } = data;
      if (response !== undefined) {
        if (status === 200) {
          this.hideDialog();
          this.setState({
            roomsData: response,
          });
        }
      } else if (status === 111) {
        this.hideDialog();
        if (message.non_field_errors !== undefined) {
          // if (message.non_field_errors[0] === "User with this social_id does not exist")
        }
      } else if (status === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      }
    }
    this.hideDialog();
  };

  _goOwnerAddressProfile = () => {
    const {
      ownerName,
      ownerLastName,
      email,
      phone_number,
      hotel_name,
      address,
      hotelImages,
      hotelID,
      ownerId,
      lat,
      long,
    } = this.state;
    this.props.navigation.navigate("ownerAddressProfile", {
      owner_Name: ownerName,
      owner_Last_Name: ownerLastName,
      owner_email: email,
      owner_mobileNo: phone_number,
      hotelName: hotel_name,
      hotelAddr: address,
      hotels_image: hotelImages,
      hotelID: hotelID,
      ownerId: ownerId,
      lat: lat,
      long: long,
    });
  };

  onRoomsEdit = (item) => {
    const { hotelData, authToken } = this.state;
    this.props.navigation.navigate("ownerRoomProfile", {
      roomData: item,
      accessToken: authToken,
      // hotelData: hotelData
    });
  };

  onRoomDelete = (item, index) => {
    const { authToken } = this.state;

    if (item.is_available) {
      this.showDialog();

      Alert.alert(
        //title
        "Are you sure you want to delete this room ?",
        //body
        "",
        [
          {
            text: "Yes",
            onPress: () => {
              DeleteRoomAction(
                {
                  id: item.id,
                  accessToken: authToken,
                },
                (data) => this.deleteRoomResponse(data)
              );
            },
          },
          {
            text: "No",
            onPress: () => console.log("No Pressed"),
            style: "cancel",
          },
        ],
        { cancelable: true }
        //clicking out side of alert will not cancel
      );
    } else {
      this.showToastAlert(
        "This room is booked and you can't delete this room at this time"
      );
    }
  };

  deleteRoomResponse = (data) => {
    this.hideDialog();
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      const { response, message, status } = data;
      if (response !== undefined) {
        if (status === 200) {
          this.hideDialog();
          this.showToastSucess(message);
          this.getHotelDetails();
        }
      } else if (status === 111) {
        this.hideDialog();
        if (message.non_field_errors !== undefined) {
          // if (message.non_field_errors[0] === "User with this social_id does not exist")
        }
      } else if (status === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      }
    }
    this.hideDialog();
  };

  formatAMPM = (data) => {
    if(data!==null && data !== undefined)
    {
      const [time] = data.split(" ");
      let [sHours, minutes] = time.split(":");
  
      // const [sHours, minutes] = time24.match(/([0-9]{1,2}):([0-9]{2})/).slice(1);
      const period = +sHours < 12 ? "AM" : "PM";
      const hours = +sHours % 12 || 12;
  
      return `${hours}:${minutes} ${period}`;
    }
    else {
      return "12:00 PM";
   }
   
  };

  // formatAMPM = (data) => {
  //   if (data !== undefined && data !== null && data !== "") {
  //     const [time] = data.split(" ");

  //     let [hours, minutes] = time.split(":");

  //     var ampm = hours >= 12 ? "PM" : "AM";
  //     hours = hours % 12;
  //     hours = hours ? hours : 12; // the hour '0' should be '12'
  //     minutes = minutes < 10 ? "0" + minutes : minutes;
  //     var strTime = hours + ":" + minutes + " " + ampm;
  //     return strTime;
  //   } else {
  //     return "12:00 PM";
  //   }
  // };

  // =============================================================================================
  // Render methods for Header
  // =============================================================================================

  _renderHeader() {
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={
          this.props.route.params !== undefined &&
          this.props.route.params.comesFrom !== undefined ? (
            <LeftIcon onPress={() => navigation.goBack()} />
          ) : (
            <LeftIcon drawer onPress={() => navigation.openDrawer()} />
          )
        }
        rightComponent={
          <OwnerRightIcon onPress={() => this._goOwnerAddressProfile()} />
        }
        centerComponent={{
          text: STRINGS.owner_profile_header,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  _renderCustomLoader = () => {
    // const {isLoading} = this.state;
    return (
      <OrientationLoadingOverlay
        // visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  _renderAddress = () => {
    const { isAddressClicked, address, phone_number } = this.state;
    return (
      <AddressView
        isAddressClicked={isAddressClicked}
        onIconPress={() =>
          this.setState({ isAddressClicked: !isAddressClicked })
        }
        onEditPress={() => this._goOwnerAddressProfile()}
        address={address}
        phoneNumber={phone_number}
      />
    );
  };

  _renderRoomsView = () => {
    const {
      isRoomsClicked,
      roomsData,
      roomImgModelVisible,
      currentClickedData,
    } = this.state;
    return (
      <HotelRoomsView
        isRoomsClicked={isRoomsClicked}
        onIconPress={() => this.setState({ isRoomsClicked: !isRoomsClicked })}
        roomsData={roomsData}
        onEditPress={(item) => this.onRoomsEdit(item)}
        onDeleteClick={(item, index) => this.onRoomDelete(item, index)}
        onAddButtonPress={() => this.props.navigation.navigate("addNewRooms")}
        roomImgModelVisible={roomImgModelVisible}
        onImageClick={(item) =>
          this.setState({ roomImgModelVisible: true, currentClickedData: item })
        }
        currentDataRoomClicked={currentClickedData}
        onCloseRoomListModel={() =>
          this.setState({ roomImgModelVisible: !roomImgModelVisible })
        }
      />
    );
  };

  viewPolicyDocs = (docFile) => {
    if (docFile !== undefined && docFile !== null) {
      this.props.navigation.navigate("PdfViewScreen", {
        pdfData: docFile,
      });
    }
  };

  _renderPolicyDocument = () => {
    const {
      isPolicyDocument,
      check_in,
      check_out,
      hotelDescription,
      hotel_documents,
      hotelID,
    } = this.state;
    let docTitle = "";
    let docFile = null;
    if (hotel_documents.length !== 0) {
      docTitle = hotel_documents[0].title;
      docFile = hotel_documents[0].file;
    }
   
    console.log("check_in------",check_in)
    return (
      <PolicyDocument
        isPolicyDocument={isPolicyDocument}
        onIconPress={() =>
          this.setState({ isPolicyDocument: !isPolicyDocument })
        }
        checkIn={this.formatAMPM(check_in)}
        checkOut={this.formatAMPM(check_out)}
        policyDescription={docTitle}
        viewpolicy={() => this.viewPolicyDocs(docFile)}
        onPolicyEditPress={() =>
          this.props.navigation.navigate("ownerPolicyProfile", {
            checkInTime: check_in,
            checkoutTime: check_out,
            genralIntstruction: hotelDescription,
            docFile: docFile,
            docTitle: docTitle,
            // policyDoc: hotel_documents,
            hotelID: hotelID,
          })
        }
      />
    );
  };

  render() {
    const { hotel_name, hotelImage, hotelImages } = this.state;
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <ScrollContainer>
          <View style={styles.mainContainer}>
            <View style={styles.innerView}>
              <Spacer space={3} />
              <Text style={styles.hotelTitle}>{hotel_name}</Text>
              <Spacer space={2} />
              {hotelImages.length > 0 ? (
                <FlatList
                  horizontal={true}
                  contentContainerStyle={{ alignItems: "center" }}
                  style={styles.flatListStyle}
                  data={hotelImages}
                  renderItem={({ item, index }) => (
                    <ShadowViewContainer style={{ marginBottom: 0 }}>
                      {console.log("item is", item)}
                      <FastImage
                        style={styles.imageStyle}
                        source={{
                          uri:
                            item.image !== null
                              ? item.image
                              : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                        }}
                        resizeMode={FastImage.resizeMode.cover}
                      />
                    </ShadowViewContainer>
                  )}
                />
              ) : (
                <ShadowViewContainer style={{ marginBottom: 0 }}>
                  <FastImage
                    style={styles.imageStyle}
                    source={{
                      uri:
                        "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                    }}
                    resizeMode={FastImage.resizeMode.cover}
                  />
                </ShadowViewContainer>
              )}

              <Spacer space={2} />
              {this._renderAddress()}
              <Spacer space={2} />
              {this._renderRoomsView()}
              <Spacer space={2} />
              {this._renderPolicyDocument()}
            </View>
            {this._renderCustomLoader()}
          </View>
        </ScrollContainer>
      </SafeAreaViewContainer>
    );
  }
}
