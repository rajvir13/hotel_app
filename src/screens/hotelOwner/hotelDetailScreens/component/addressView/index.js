import React from 'react';
import {Image, Text, TouchableOpacity, View} from "react-native";

import {ShadowViewContainer} from "../../../../../utils/BaseStyle";
import {styles} from "./styles";
import STRINGS from "../../../../../utils/Strings";
import {ICONS} from "../../../../../utils/ImagePaths";
import {Spacer} from "../../../../../customComponents/Spacer";


const AddressView = ({isAddressClicked, address, phoneNumber, onIconPress, onEditPress}) => {
    return (
        <ShadowViewContainer>
            <View style={styles.addressContainer}>
                <TouchableOpacity onPress={onIconPress}>
                    <View style={styles.addressTopView}>
                        <Text style={styles.addressHeaderTextStyle}>{STRINGS.address_string}</Text>
                        {/*<TouchableOpacity onPress={onIconPress}>*/}
                        <Image style={styles.dropdownIconStyle}
                               resizeMode={'contain'}
                               source={(isAddressClicked) ? ICONS.DOWN_ARROW : ICONS.LEFT_ARROW}/>
                        {/*</TouchableOpacity>*/}
                    </View>
                </TouchableOpacity>
                {isAddressClicked &&
                <View>
                    <Spacer space={2}/>
                    <View style={styles.separator}/>
                    <Spacer space={2}/>
                    <Text numberOfLines={2}
                          style={styles.addressText}>{address}</Text>
                    <View style={styles.phoneView}>
                        <Text style={styles.addressText}>Ph.Num-{phoneNumber}</Text>
                        <TouchableOpacity style={styles.editView}
                                          onPress={onEditPress}>
                            <Text style={styles.buttonText}>{STRINGS.edit_string}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                }
            </View>
        </ShadowViewContainer>
    )
};

export default AddressView
