import React from 'react';
import {StyleSheet} from "react-native";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import COLORS from "../../../../../utils/Colors";
import {FONT_FAMILY} from "../../../../../utils/Font";
import {FONT} from "../../../../../utils/FontSizes";


const styles = StyleSheet.create({
    addressContainer: {
        alignSelf:'center',
        width: wp(90),
        paddingVertical: wp(5),
        backgroundColor: COLORS.white_color,
        borderWidth: 1,
        borderColor: COLORS.light_grey1,
        borderRadius: wp(3),
    },
    addressTopView:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: wp(4),
    },
    addressHeaderTextStyle:{
        fontFamily: FONT_FAMILY.Montserrat,
        fontSize: FONT.TextSmall_2,
        fontWeight: '600',
        color: COLORS.floating_color
    },
    dropdownIconStyle:{
        width: wp(4),
        height: wp(4),
    },
    separator:{
        height: wp(0.2),
        backgroundColor: COLORS.light_grey3
    },
    addressText:{
        lineHeight: wp(5),
        fontSize: FONT.TextSmall_2,
        fontFamily: FONT_FAMILY.Montserrat,
        width: wp(65),
        paddingHorizontal: wp(4),
    },
    phoneView:{
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    editView:{
        width: wp(15),
        backgroundColor: COLORS.app_theme_color,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: wp(5),
        paddingVertical: wp(1.5),
        marginRight: wp(4)
    },
    buttonText:{
        fontFamily: FONT_FAMILY.Montserrat,
        fontSize: FONT.TextExtraSmall,
        fontWeight: '600',
        color: COLORS.white_color
    }
});

export {styles};
