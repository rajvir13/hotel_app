import React from 'react';
import {StyleSheet} from "react-native";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import COLORS from "../../../../../utils/Colors";
import {FONT_FAMILY} from "../../../../../utils/Font";
import {FONT} from "../../../../../utils/FontSizes";


const styles = StyleSheet.create({
    roomsContainer: {
        width: wp(90),
        alignSelf:'center',
        paddingVertical: wp(5),
        backgroundColor: COLORS.white_color,
        borderWidth: 1,
        borderColor: COLORS.light_grey1,
        borderRadius: wp(3),
    },
    roomsTopView:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: wp(4),
    },
    roomsHeaderTextStyle:{
        fontFamily: FONT_FAMILY.Montserrat,
        fontSize: FONT.TextSmall_2,
        fontWeight: '600',
        color: COLORS.floating_color
    },
    dropdownIconStyle:{
        width: wp(4),
        height: wp(4),
    },
    separator:{
        height: wp(0.2),
        backgroundColor: COLORS.light_grey3
    },
    roomsText:{
        lineHeight: wp(5),
        fontSize: FONT.TextSmall_2,
        fontFamily: FONT_FAMILY.Montserrat,
        width: wp(65),
        paddingHorizontal: wp(4),
    },
    topView:{
        paddingHorizontal: wp(2),
        flexDirection: 'row',
        justifyContent:'space-between',
        width: wp(90),
    },
    typeView:{
        // width: wp(15),
        backgroundColor: COLORS.type_backgroundColor,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: wp(3.5),
        paddingVertical: wp(2),
        marginRight: wp(4),
        paddingHorizontal: wp(2)
    },
    buttonText:{
        fontFamily: FONT_FAMILY.Montserrat,
        fontSize: FONT.TextExtraSmall,
        fontWeight: '600',
        color: COLORS.app_theme_color
    }
});

export {styles};
