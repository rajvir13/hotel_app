import React, { useState, useEffect } from "react";
import { FlatList, Image, Text, TouchableOpacity, View } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import Menu, { MenuItem } from "react-native-material-menu";

import { ShadowViewContainer } from "../../../../../utils/BaseStyle";
import { styles } from "./styles";
import STRINGS from "../../../../../utils/Strings";
import { ICONS } from "../../../../../utils/ImagePaths";
import { Spacer } from "../../../../../customComponents/Spacer";
import { FONT } from "../../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../../utils/Font";
import COLORS from "../../../../../utils/Colors";
import { API } from "../../../../../redux/constant";
import DisplayRoomImages from '../../../../../customComponents/Modals/ShowRoomImages/DisplayRoomImages'
const HotelRoomsView = ({
  isRoomsClicked,
  onIconPress,
  roomsData,
  onEditPress,
  onDeleteClick,
  onAddButtonPress,
  roomImgModelVisible,
  onImageClick,
  currentDataRoomClicked,
  onCloseRoomListModel

}) => {
  //============================= pop up Menu handling ========================

  const _menu = [];

  const hideMenu = (item, index) => {
    _menu[index].hide();
  };

  const showMenu = (item, index) => {
    _menu[index].show();
  };

  const onMenuPress = (item, index) => {
    hideMenu(item, index);
    onEditPress(item, index);
  };

  const onDeletePress = (item, index) => {
    // hideMenu(item, index);
    onDeleteClick(item, index);
  };

  const displayDropdown = (item, index) => {
    return (
      <View
        style={{
          padding: wp(2),
        }}
      >
        <Menu
          ref={(menu) => {
            _menu[index] = menu;
          }}
          button={
            <TouchableOpacity onPress={() => showMenu(item, index)}>
              <Image
                resizeMode={"contain"}
                style={{
                  alignSelf: "flex-end",
                  width: wp(5),
                  // height: wp(15),
                }}
                source={ICONS.THREE_DOT_ICON}
              />
            </TouchableOpacity>
          }
        >
          <MenuItem onPress={() => onMenuPress(item, index)}>Edit</MenuItem>
          <MenuItem onPress={() => onDeletePress(item, index)}>Delete</MenuItem>
        </Menu>
      </View>
    );
  };

  let isDataAvailable = roomsData[0].room_hotel;
  isDataAvailable = isDataAvailable.length !== 0;

  

  return (
    <ShadowViewContainer>
      <View style={styles.roomsContainer}>
        <TouchableOpacity onPress={onIconPress}>
          <View style={styles.roomsTopView}>
            <Text style={styles.roomsHeaderTextStyle}>
              {STRINGS.rooms_amenities}
            </Text>
            {/*<TouchableOpacity onPress={onIconPress}>*/}
            <Image
              style={styles.dropdownIconStyle}
              resizeMode={"contain"}
              source={isRoomsClicked ? ICONS.DOWN_ARROW : ICONS.LEFT_ARROW}
            />
            {/*</TouchableOpacity>*/}
          </View>
        </TouchableOpacity>
        {isRoomsClicked && (
          <View>
            <Spacer space={2} />
            <View style={styles.separator} />
            <Spacer space={2} />

            <View style={styles.topView}>
              <View style={{ flexDirection: "row", width: wp(40) }}>
                <View style={styles.typeView}>
                  <Text style={styles.buttonText}>
                    Type {roomsData[0].room_type_count}
                  </Text>
                </View>
                <Spacer row={2} />
                <View style={styles.typeView}>
                  <Text style={styles.buttonText}>
                    Rooms {roomsData[0].room_count}
                  </Text>
                </View>
              </View>
              <TouchableOpacity onPress={() => onAddButtonPress()}>
                <View style={[styles.typeView, { alignSelf: "flex-end" }]}>
                  <Text style={styles.buttonText}>+ Add Room</Text>
                </View>
              </TouchableOpacity>
            </View>
            <Spacer space={2} />
            {isDataAvailable ? (
              <>
                <FlatList
                  nestedScrollEnabled={true}
                  // contentContainerStyle={{height: wp(80)}}
                  data={roomsData[0].room_hotel}
                  keyExtractor={(item, index) => index}
                  renderItem={({ item, index }) => (
                    <>
                      <View
                        style={{
                          flexDirection: "row",
                        }}
                      >
                        <Spacer row={1} />
                        <TouchableOpacity
                          onPress={() =>
                            onImageClick(item)
                          }
                        >
                          <Image
                            resizeMode={"stretch"}
                            style={{
                              width: wp(25),
                              height: wp(35),
                            }}
                            source={{
                              uri:
                                item.room_images.length !== 0
                                  ? API.IMAGE_BASE_URL +
                                    item.room_images[0].image
                                  : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                            }}
                          />
                        </TouchableOpacity>
                        <Spacer row={1} />
                        <ShadowViewContainer>
                          <View style={{ backgroundColor: COLORS.white_color }}>
                            <View
                              style={{
                                width: wp(60),
                                flexDirection: "row",
                                justifyContent: "space-between",
                                // backgroundColor: 'hotpink',
                              }}
                            >
                              <View
                                style={{
                                  paddingTop: wp(2),
                                  paddingHorizontal: wp(2),
                                }}
                              >
                                <Text
                                  style={{
                                    fontSize: FONT.TextSmall,
                                    fontFamily: FONT_FAMILY.Montserrat,
                                    fontWeight: "500",
                                    // lineHeight: wp(1)
                                  }}
                                >
                                  Room Type: {item.room_type}
                                </Text>
                                <Text
                                  style={{
                                    fontSize: FONT.TextSmall_2,
                                    lineHeight: wp(6),
                                    fontFamily: FONT_FAMILY.Montserrat,
                                  }}
                                >
                                  {STRINGS.no_of_Guests}: {item.no_of_guest}
                                </Text>
                              </View>
                   {item.is_available && displayDropdown(item, index)}
                            
                            </View>
                            <Spacer space={1.5} />
                            <View>
                              <Text
                                style={{
                                  paddingHorizontal: wp(2),
                                  fontSize: FONT.TextSmall_2,
                                  fontFamily: FONT_FAMILY.Montserrat,
                                  fontWeight: "500",
                                }}
                              >
                                Complimentary Services
                              </Text>
                              <Spacer space={0.5} />
                              <View
                                style={{
                                  // width: wp(45),
                                  paddingHorizontal: wp(2),
                                  flexDirection: "row",
                                }}
                              >
                                <FlatList
                                  style={{
                                    width: wp(40),
                                  }}
                                  data={item.room_service}
                                  keyExtractor={(item, index) => index}
                                  numColumns={2}
                                  renderItem={(itemA) => (
                                    <View style={{ flexDirection: "row" }}>
                                      <Text
                                        style={{
                                          fontSize: FONT.TextExtraSmall,
                                          fontFamily: FONT_FAMILY.Montserrat,
                                        }}
                                      >
                                        {itemA.item.service}
                                      </Text>
                                      {itemA.index !==
                                        item.room_service.length - 1 && (
                                        <Text>,</Text>
                                      )}
                                      <Spacer row={0.5} />
                                    </View>
                                  )}
                                />
                                <Spacer row={1} />
                                <View
                                  style={{
                                    width: wp(15),
                                    paddingHorizontal: wp(1),
                                    height: wp(6),
                                    alignSelf: "flex-end",
                                    justifyContent: "center",
                                    borderRadius: wp(2),
                                    backgroundColor: COLORS.app_theme_color,
                                    marginBottom: wp(2),
                                  }}
                                >
                                  <Text
                                    numberOfLines={1}
                                    style={{
                                      fontFamily: FONT_FAMILY.Montserrat,
                                      textAlign: "center",
                                      fontSize: FONT.TextSmall_2,
                                      color: COLORS.white_color,
                                    }}
                                  >
                                    {"\u20B9"}
                                    {item.discounted_price}
                                  </Text>
                                </View>
                              </View>
                              <View
                                style={{
                                  flexDirection: "row",
                                  marginBottom: wp("2%"),
                                  marginLeft: wp("2%"),
                                }}
                              >
                                <Text
                                  style={{
                                    fontSize: FONT.TextSmall_2,
                                    fontFamily: FONT_FAMILY.Montserrat,
                                    fontWeight: "bold",
                                    color: "black",
                                  }}
                                >
                                  Status:
                                </Text>

                                {item.is_admin_approved === 0 ? (
                                  <Text
                                    style={{
                                      fontSize: FONT.TextSmall_2,
                                      fontFamily: FONT_FAMILY.Montserrat,
                                      fontWeight: "bold",
                                      color: "blue",
                                      marginLeft: wp("1%"),
                                    }}
                                  >
                                    Pending
                                  </Text>
                                ) : item.is_admin_approved === 1 ? (
                                  <Text
                                    style={{
                                      fontSize: FONT.TextSmall_2,
                                      fontFamily: FONT_FAMILY.Montserrat,
                                      fontWeight: "bold",
                                      color: "green",
                                      marginLeft: wp("1%"),
                                    }}
                                  >
                                    Confirm By Admin
                                  </Text>
                                ) : (
                                  <Text
                                    style={{
                                      fontSize: FONT.TextSmall_2,
                                      fontFamily: FONT_FAMILY.Montserrat,
                                      fontWeight: "bold",
                                      color: "red",
                                      marginLeft: wp("1%"),
                                    }}
                                  >
                                    Rejected
                                  </Text>
                                )}
                              </View>
                            </View>
                          </View>
                        </ShadowViewContainer>
                      </View>
                      <Spacer space={1.5} />
                    </>
                  )}
                />
              </>
            ) : (
              <Text
                style={{
                  textAlign: "center",
                  fontSize: FONT.TextNormal,
                  fontFamily: FONT_FAMILY.MontserratBold,
                }}
              >
                NO DATA{" "}
              </Text>
            )}
             {roomImgModelVisible === true && (
              <DisplayRoomImages
                roomModalVisible={roomImgModelVisible}
                currentClickedData={currentDataRoomClicked}
                closeRoomModal={() => {
                  onCloseRoomListModel()
                }}
              />
            )}
          </View>
        )}
      </View>
    </ShadowViewContainer>
  );
};

export default HotelRoomsView;
