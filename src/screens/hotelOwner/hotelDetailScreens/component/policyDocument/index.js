import React from "react";
import { Image, Text, TouchableOpacity, View } from "react-native";

import { ShadowViewContainer } from "../../../../../utils/BaseStyle";
import STRINGS from "../../../../../utils/Strings";
import { ICONS } from "../../../../../utils/ImagePaths";
import { Spacer } from "../../../../../customComponents/Spacer";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { FONT } from "../../../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../../../utils/Font";
import COLORS from "../../../../../utils/Colors";
import { styles } from "./styles";

const PolicyDocument = ({
  isPolicyDocument,
  onIconPress,
  onPolicyEditPress,
  checkIn,
  checkOut,
  viewpolicy,
  policyDescription,
}) => {
  return (
    <ShadowViewContainer>
      <View style={styles.addressContainer}>
        <TouchableOpacity onPress={onIconPress}>
          <View style={styles.addressTopView}>
            <Text style={styles.addressHeaderTextStyle}>
              {STRINGS.policy_document}
            </Text>
            {/*<TouchableOpacity onPress={onIconPress}>*/}
            <Image
              style={styles.dropdownIconStyle}
              resizeMode={"contain"}
              source={isPolicyDocument ? ICONS.DOWN_ARROW : ICONS.LEFT_ARROW}
            />
            {/*</TouchableOpacity>*/}
          </View>
        </TouchableOpacity>
        {isPolicyDocument && (
          <View style={{ flex: 1 }}>
            <Spacer space={2} />
            <View style={styles.separator} />
            <Spacer space={2} />
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                width: wp(90),
                justifyContent: "space-evenly",
                alignSelf: "center",
                // backgroundColor:'pink'
              }}
            >
              <View style={{ flexDirection: "row", marginRight: 5 }}>
                <Text style={styles.addressText}>Check In: </Text>
                <Spacer row={0.5} />
                <View style={styles.typeView}>
                  <Text style={styles.buttonText}>{checkIn}</Text>
                </View>
              </View>
              {/*<Spacer row={0.5}/>*/}
              <View style={{ flexDirection: "row", marginRight: 5 }}>
                <Text style={styles.addressText}>Check Out: </Text>
                <Spacer row={0.5} />
                <View style={styles.typeView}>
                  <Text style={styles.buttonText}>{checkOut}</Text>
                </View>
              </View>
              {/*<Spacer row={1}/>*/}
              <TouchableOpacity
                style={styles.editView}
                onPress={onPolicyEditPress}
              >
                <Text style={styles.buttonText2}>{STRINGS.edit_string}</Text>
              </TouchableOpacity>
            </View>
            <Spacer space={2} />
            <View style={styles.separator} />
            <Spacer space={2} />
            <Text numberOfLines={5} style={styles.addressText2}>
              {policyDescription}
            </Text>
            <Spacer space={2} />
            <View style={{ flexDirection: "row", marginHorizontal: wp(3) }}>
              <Image
                style={{ width: wp(15), height: wp(15) }}
                source={ICONS.PDF_ICON}
              />
              <Spacer row={2} />

              <TouchableOpacity onPress={viewpolicy}>
                <Text
                  style={{
                    alignSelf: "center",
                    fontSize: FONT.TextMedium,
                    fontFamily: FONT_FAMILY.Montserrat,
                    fontWeight: "600",
                    marginTop:wp('5%'),
                    color: COLORS.app_theme_color,
                  }}
                >
                  View Policy Document
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        )}
      </View>
    </ShadowViewContainer>
  );
};

export default PolicyDocument;
