import React from 'react';
import {StyleSheet} from "react-native";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {FONT} from "../../../utils/FontSizes";
import {FONT_FAMILY} from "../../../utils/Font";
import COLORS from "../../../utils/Colors";


const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: COLORS.backGround_color,
        justifyContent: 'center',

    },
    flatListStyle:{
        width: wp(90), height: wp(60),
        borderRadius: wp(3),

    },
    innerView: {
        justifyContent: 'center',
        alignSelf:'center'
    },
    hotelTitle: {
        marginLeft: wp(4),
        alignSelf: 'flex-start',
        fontSize: FONT.TextMedium_2,
        fontFamily: FONT_FAMILY.Montserrat,
        fontWeight: '600'
    },
    imageStyle: {
        // justifyContent: 'center',
        alignSelf: 'center',
        width: wp(90),
        height: wp(60),
        borderRadius: wp(3),
    }
});

export {styles};
