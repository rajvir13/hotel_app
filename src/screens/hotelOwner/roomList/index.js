import React from "react";
import {
  FlatList,
  Image,
  Platform,
  StatusBar,
  Alert,
  Text,
  TouchableOpacity,
  View,
  Switch,
} from "react-native";
import { Header } from "react-native-elements";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import Menu, { MenuItem } from "react-native-material-menu";
import * as _ from "lodash";
import { CommonActions } from "@react-navigation/native";

import COLORS from "../../../utils/Colors";
import { ICONS } from "../../../utils/ImagePaths";
import { LeftIcon } from "../../../customComponents/icons";
import STRINGS from "../../../utils/Strings";
import { FONT } from "../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../utils/Font";
import { Spacer } from "../../../customComponents/Spacer";
import { GetHotelRoomDetailAction } from "../../../redux/actions/GetHotelDetail";
import AsyncStorage from "@react-native-community/async-storage";
import FastImage from "react-native-fast-image";
import { API } from "../../../redux/constant";
import BaseClass from "../../../utils/BaseClass";
import {
  SafeAreaViewContainer,
  MainContainer,
  ShadowViewContainer,
} from "../../../utils/BaseStyle";
import { DeleteRoomAction } from "../../../redux/actions/UpdateRoomDetails";
import { styles } from "../hotelDetailScreens/component/roomsAndAmenitiesView/styles";
import OrientationLoadingOverlay from "../../../customComponents/Loader";

import DisplayRoomImages from "../../../customComponents/Modals/ShowRoomImages/DisplayRoomImages";

let isUserActive = false;
export default class RoomsList extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      isEnabled: false,
      isLoading: false,
      roomsData: [],
      roomImgModelVisible: false,
      currentClickedData: undefined,
    };
    this._menu = [];
  }

  componentDidMount() {
    const { navigation } = this.props;
    this._unsubscribe = navigation.addListener("focus", () => {
      this.onFocusFunction();
    });
    this.getHotelDetails();
  }

  getHotelDetails = () => {
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        let hotelId = "";
        if (loginData.hotel_details !== undefined) {
          hotelId = loginData.hotel_details.id;
        } else {
          console.log("id is",loginData.id)
          hotelId = loginData.id;
        }
        this.setState({
          authToken: loginData.access_token,
          hotel_id: hotelId,
          isLoading: true,
        });
        this.showDialog();
        GetHotelRoomDetailAction(
          {
            id: hotelId,
            accessToken: loginData.access_token,
          },
          (data) => this.hotelRoomResponse(data)
        );

       
      }
    });
  };

  onFocusFunction = () => {
    // this.componentDidMount()
    this.getHotelDetails();
  };

  componentWillUnmount() {
    this._unsubscribe();
  }

  hotelRoomResponse = (data) => {
    this.hideDialog()
    const { navigation } = this.props;
    this.setState({ isLoading: false });
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      const { response, message, status } = data;
      if (response !== undefined) {
        if (status === 200) {
          this.hideDialog();
          // let roomArray = [];
          //
          // response.map((item) => {
          //     if (item.room_hotel.length !== 0) {
          //         item.room_hotel.map((item1) => {
          //             let image = [];
          //             if (item1.room_images.length !== 0) {
          //                 image.push(API.IMAGE_BASE_URL + item1.room_images[0]);
          //             } else {
          //                 image.push("https://travelji.com/wp-content/uploads/Hotel-Tips.jp")
          //             }
          //             roomArray.push({
          //                 id: item1.id,
          //                 roomImage: image,
          //                 room_service: item1.room_service,
          //                 discounted_price: item1.discounted_price,
          //                 regular_price: item1.regular_price,
          //                 no_of_guest: item1.no_of_guest,
          //                 room_type: item1.room_type,
          //                 isAvailable: item1.is_available
          //             })
          //         })
          //     }
          // });
          this.setState({
            // roomsData: response,
            roomsData: response[0].room_hotel,
          });
        }
      } else if (status === 111) {
        this.hideDialog();
        if (message.non_field_errors !== undefined) {
          // if (message.non_field_errors[0] === "User with this social_id does not exist")
        } else {
          if (!isUserActive) {
            isUserActive = true;
            AsyncStorage.removeItem(STRINGS.loginCredentials);
            navigation.dispatch(
              CommonActions.reset({
                index: 0,
                routes: [{ name: "login" }],
              })
            );
            this.showToastAlert(message);
          }
        }
      } else if (status === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      }
    }
  };

  onRoomDelete = (item, index) => {
    const { authToken } = this.state;
    // this.hideMenu(item, index);

    if (item.is_available) {
      Alert.alert(
        "Are you sure you want to delete this room ?",
        "",
        [
          {
            text: "Yes",
            onPress: () => {
              DeleteRoomAction(
                {
                  id: item.id,
                  accessToken: authToken,
                },
                (data) => this.deleteRoomResponse(data)
              );
              this.hideMenu(item, index);
            },
          },
          {
            text: "No",
            onPress: () => this.hideMenu(item, index),
            style: "cancel",
          },
        ],
        {
          cancelable: true,
        }
      );
    } else {
      this.showToastAlert(
        "This room is booked and you can't delete this room at this time"
      );
    }
  };

  deleteRoomResponse = (data) => {
    const { navigation } = this.props;
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      const { response, message, status } = data;
      if (response !== undefined) {
        if (status === 200) {
          this.hideDialog();
          this.showToastSucess(message);
          this.getHotelDetails();
        }
      } else if (status === 111) {
        this.hideDialog();
        if (message.non_field_errors !== undefined) {
          // if (message.non_field_errors[0] === "User with this social_id does not exist")
        } else {
          AsyncStorage.removeItem(STRINGS.loginCredentials);
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{ name: "login" }],
            })
          );
          this.showToastAlert(message);
        }
      } else if (status === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      }
    }
    this.hideDialog();
  };

  onRoomAAvailability = (index) => {
    const { roomsData } = this.state;
    _.map(roomsData, (item, rIndex) => {
      if (rIndex === index) {
        item.is_available = !item.is_available;
        return item;
      } else {
        return item;
      }
    });
    this.setState({ roomsData });
  };

  // _menu = [];

  hideMenu = (item, index) => {
    this._menu[index].hide();
  };

  showMenu = (item, index) => {
    ~this._menu[index].show();
  };

  onMenuPress = (item, index) => {
    const { hotel_id, authToken } = this.state;
    this.hideMenu(item, index);
    this.props.navigation.navigate("ownerRoomProfile", {
      roomData: item,
      accessToken: authToken,
      // hotelId: hotel_id
    });
  };

  displayDropdown = (item, index) => {
    return (
      <View
        style={{
          paddingHorizontal: wp(2),
          paddingVertical: wp(1),
        }}
      >
        {item.is_available && (
          <Menu
            ref={(menu) => {
              this._menu[index] = menu;
            }}
            button={
              <TouchableOpacity onPress={() => this.showMenu(item, index)}>
                <Image
                  resizeMode={"contain"}
                  style={{
                    alignSelf: "flex-end",
                    width: wp(5),
                    // height: wp(15),
                  }}
                  source={ICONS.THREE_DOT_ICON}
                />
              </TouchableOpacity>
            }
          >
            <MenuItem onPress={() => this.onMenuPress(item, index)}>
              Edit
            </MenuItem>
            <MenuItem onPress={() => this.onRoomDelete(item, index)}>
              Delete
            </MenuItem>
          </Menu>
        )}
      </View>
    );
  };

  _renderTotalRooms = (roomsNumber) => {
    return (
      <View>
        <Text
          style={{
            color: COLORS.black_color,
            fontSize: FONT.TextSmall_2,
            fontFamily: FONT_FAMILY.Roboto,
          }}
        >
          Total{" "}
          <Text style={{ color: COLORS.app_theme_color }}> {roomsNumber}</Text>
        </Text>
      </View>
    );
  };

  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  _renderHeader(props) {
    const { navigation } = this.props;
    const { roomsData, comesFrom } = this.state;

    return (
      <Header
        backgroundColor={COLORS.backGround_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={
          this.props.route.params !== undefined &&
          this.props.route.params.comesFrom ? (
            <LeftIcon onPress={() => navigation.goBack()} />
          ) : (
            <LeftIcon drawer onPress={() => navigation.openDrawer()} />
          )
        }
        rightComponent={this._renderTotalRooms(roomsData.length)}
        centerComponent={{
          text: STRINGS.room_list_header,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  onRoomModalOpen = (item) => {
    const { currentClickedData, roomImgModelVisible } = this.state;
    this.setState({ currentClickedData: item, roomImgModelVisible: true });
  };

  render() {
    const {
      roomsData,
      isEnabled,
      roomImgModelVisible,
      currentClickedData,
    } = this.state;
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <MainContainer>
          <View style={{ width: wp(100) }}>
            <Spacer space={2} />
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("addNewRooms")}
            >
              <View style={[styles.typeView, { alignSelf: "flex-end" }]}>
                <Text style={styles.buttonText}>+ Add Room</Text>
              </View>
            </TouchableOpacity>
            <Spacer space={2} />
            {roomsData.length !== 0 ? (
              <FlatList
                style={{ marginBottom: wp(15) }}
                nestedScrollEnabled={true}
                // contentContainerStyle={{height: wp(80)}}
                data={roomsData}
                keyExtractor={(item, index) => index}
                renderItem={({ item, index }) => (
                  <ShadowViewContainer>
                    <View
                      style={{
                        flexDirection: "row",
                        paddingTop: wp(2),
                        paddingHorizontal: wp(2),
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => this.onRoomModalOpen(item)}
                      >
                        <FastImage
                          resizeMode={FastImage.resizeMode.stretch}
                          style={{
                            width: wp(25),
                            height: wp(35),
                          }}
                          source={{
                            uri:
                              item.room_images.length !== 0
                                ? API.IMAGE_BASE_URL + item.room_images[0].image
                                : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                          }}
                        />
                      </TouchableOpacity>
                      <Spacer row={1} />
                      <View style={{ backgroundColor: COLORS.white_color }}>
                        <View
                          style={{
                            width: wp(60),
                            flexDirection: "row",
                          }}
                        >
                          <View
                            style={
                              {
                                // paddingTop: wp(2),
                              }
                            }
                          >
                            <Text
                              numberOfLines={1}
                              style={{
                                width: wp(38),
                                fontSize: FONT.TextSmall,
                                fontFamily: FONT_FAMILY.Montserrat,
                              }}
                            >
                              Room Type: {item.room_type}
                            </Text>
                            <Text
                              style={{
                                width: wp(38),
                                fontSize: FONT.TextSmall_2,
                                lineHeight: wp(6),
                                fontFamily: FONT_FAMILY.Montserrat,
                              }}
                            >
                              {STRINGS.no_of_Guests}: {item.no_of_guest}
                            </Text>
                          </View>
                          <Text
                            numberOfLines={1}
                            style={{
                              // width: wp(14),
                              textAlign: "center",
                              fontSize: FONT.TextSmall,
                              fontFamily: FONT_FAMILY.Montserrat,
                            }}
                          >
                            {"\u20B9"}
                            {item.discounted_price}
                          </Text>

                          {this.displayDropdown(item, index)}
                        </View>
                        <Spacer space={1.5} />
                        <View>
                          <Text
                            style={{
                              // paddingHorizontal: wp(2),
                              // paddingHorizontal: wp(2),
                              fontSize: FONT.TextSmall_2,
                              fontFamily: FONT_FAMILY.Montserrat,
                              fontWeight: "500",
                            }}
                          >
                            Complimentary Services
                          </Text>
                          <Spacer space={0.5} />
                          <View
                            style={{
                              width: wp(60),
                              // paddingHorizontal: wp(2),
                              // flexDirection: 'row',
                            }}
                          >
                            <FlatList
                              data={item.room_service}
                              keyExtractor={(item1, index1) => index1}
                              numColumns={2}
                              renderItem={(item1, index1) => {
                                return (
                                  <>
                                    <Text
                                      style={{
                                        fontSize: FONT.TextExtraSmall,
                                        fontFamily: FONT_FAMILY.Montserrat,
                                      }}
                                    >
                                      {item1.item.service}
                                    </Text>
                                    <Spacer row={0.8} />
                                  </>
                                );
                              }}
                            />
                          </View>
                          <Spacer space={1} />
                          <View
                            style={{
                              flexDirection: "row",
                              justifyContent: "space-between",
                            }}
                          >
                            <Text
                              style={{
                                fontFamily: FONT_FAMILY.Poppins,
                                fontSize: FONT.TextSmall_2,
                                color: COLORS.black_color,
                              }}
                            >
                              Room Available
                            </Text>
                            <View>
                              <Switch
                             
                                trackColor={{
                                  false: "#767577",
                                  true: COLORS.backGround_color,
                                }}
                                thumbColor={
                                  item.is_available ? "#46E760" : "#f4f3f4"
                                }
                                ios_backgroundColor="#3e3e3e"
                                value={item.is_available}
                              />
                            </View>
                          </View>
                          <View></View>
                          <View style={{ flexDirection: "row",  }}>
                            <Text
                              style={{
                                fontSize: FONT.TextSmall_2,
                                fontFamily: FONT_FAMILY.Montserrat,
                                fontWeight: "bold",
                                color: "black",
                              }}
                            >
                              Status:
                            </Text>

                            {item.is_admin_approved === 0 ? (
                              <Text
                                style={{
                                  fontSize: FONT.TextSmall_2,
                                  fontFamily: FONT_FAMILY.Montserrat,
                                  fontWeight: "bold",
                                  color: "blue",
                                  marginLeft: wp("1%"),
                                }}
                              >
                                Pending
                              </Text>
                            ) : item.is_admin_approved === 1 ? (
                              <Text
                                style={{
                                  fontSize: FONT.TextSmall_2,
                                  fontFamily: FONT_FAMILY.Montserrat,
                                  fontWeight: "bold",
                                  color: "green",
                                  marginLeft: wp("1%"),
                                }}
                              >
                                Confirmed By Admin
                              </Text>
                            ) : (
                              <Text
                                style={{
                                  fontSize: FONT.TextSmall_2,
                                  fontFamily: FONT_FAMILY.Montserrat,
                                  fontWeight: "bold",
                                  color: "red",
                                  marginLeft: wp("1%"),
                                }}
                              >
                                Rejected
                              </Text>
                            )}
                          </View>
                        </View>
                      </View>
                    </View>
                    <Spacer space={1.5} />
                  </ShadowViewContainer>
                )}
              />
            ) : (
              <View>
                <Text
                  style={{
                    textAlign: "center",
                    fontSize: FONT.TextNormal,
                    fontFamily: FONT_FAMILY.MontserratBold,
                  }}
                >
                  NO DATA{" "}
                </Text>
              </View>
            )}
            {roomImgModelVisible === true && (
              <DisplayRoomImages
                roomModalVisible={roomImgModelVisible}
                currentClickedData={currentClickedData}
                closeRoomModal={() => {
                  this.setState({ roomImgModelVisible: !roomImgModelVisible });
                }}
              />
            )}
          </View>
          {this._renderCustomLoader()}
        </MainContainer>
      </SafeAreaViewContainer>
    );
  }
}
