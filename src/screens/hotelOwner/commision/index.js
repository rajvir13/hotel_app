/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import React from "react";
import {
    View,
    Text,
    Image,
    FlatList,
    Keyboard,
    StatusBar,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Platform,
} from "react-native";
import styles from "./styles";
import BaseClass from "../../../utils/BaseClass";
import STRINGS from "../../../utils/Strings";
import {Header} from "react-native-elements";
import {Spacer} from "../../../customComponents/Spacer";
import COLORS from "../../../utils/Colors";
import {LeftIcon} from "../../../customComponents/icons";
import {FONT} from "../../../utils/FontSizes";
import {
    SafeAreaViewContainer,
    MainContainer,
    ShadowViewContainer,
    ScrollContainer,
} from "../../../utils/BaseStyle";
import CustomTextInput from "../../../customComponents/CustomTextInput";
import {ICONS} from "../../../utils/ImagePaths";

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import {FONT_FAMILY} from "../../../utils/Font";
import CommisionBookingTopTab from "./component/tabBarComponent"

const DismissKeyboard = ({children}) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);
export default class Commission extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,

            paidData: [
                {
                    bookedBy: "Will Smith",
                    date: "12-05-2020",
                    roomType: "Single",
                    checkInTime: "01:00 PM",
                    checkDate: "5-07-2020",
                    price: 2000,
                },
                {
                    bookedBy: "Will Smith",
                    date: "12-05-2020",
                    roomType: "Double",
                    checkInTime: "01:00 PM",
                    checkDate: "5-07-2020",
                    price: 1500,
                },
                {
                    bookedBy: "Will Smith",
                    date: "12-05-2020",
                    roomType: "Single",
                    checkInTime: "01:00 PM",
                    checkDate: "5-07-2020",
                    price: 2000,
                },
                {
                    bookedBy: "Will Smith",
                    date: "12-05-2020",
                    roomType: "Single",
                    checkInTime: "01:00 PM",
                    checkDate: "5-07-2020",
                    price: 2000,
                },
                {
                    bookedBy: "Will Smith",
                    date: "12-05-2020",
                    roomType: "Single",
                    checkInTime: "01:00 PM",
                    checkDate: "5-07-2020",
                    price: 2000,
                },
            ],

            currentMonthData: [
                {
                    bookedBy: "Nick Smith",
                    date: "12-05-2020",
                    roomType: "Single",
                    checkInTime: "01:00 PM",
                    checkDate: "5-07-2020",
                    price: 5000,
                },
                {
                    bookedBy: "Max Smith",
                    date: "12-05-2020",
                    roomType: "Single",
                    checkInTime: "01:00 PM",
                    checkDate: "5-07-2020",
                    price: 2000,
                },
                {
                    bookedBy: "Jay Smith",
                    date: "12-05-2020",
                    roomType: "Single",
                    checkInTime: "01:00 PM",
                    checkDate: "5-07-2020",
                    price: 2000,
                },
                {
                    bookedBy: "lasly Smith",
                    date: "12-05-2020",
                    roomType: "Single",
                    checkInTime: "01:00 PM",
                    checkDate: "5-07-2020",
                    price: 2000,
                },
                {
                    bookedBy: "Vin Smith",
                    date: "12-05-2020",
                    roomType: "Single",
                    checkInTime: "01:00 PM",
                    checkDate: "5-07-2020",
                    price: 2000,
                },
            ],
        };
    }

    //============================= Render Hedaer==========================

    //============================= Render Hedaer==========================
    _renderHeader() {
        const {toggleMapView, user_profile} = this.state;
        const {navigation} = this.props;
        return (
            <Header
                backgroundColor={COLORS.white_color}
                barStyle={"dark-content"}
                statusBarProps={{
                    translucent: true,
                    backgroundColor: COLORS.transparent,
                }}
                leftComponent={(<LeftIcon onPress={() => navigation.goBack()}/>)}         
                centerComponent={{
                    text: "Commission",
                    style: {
                        color: COLORS.greyButton_color,
                        fontSize: FONT.TextMedium_2,
                        fontFamily: FONT_FAMILY.PoppinsBold,
                    },
                }}
                containerStyle={{
                    borderBottomColor: COLORS.greyButton_color,
                    paddingTop: Platform.OS === "ios" ? 0 : 25,
                    height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
                }}
            />
        );
    }

    //========================custome loader==============================
    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay
                visible={isLoading}
                message={STRINGS.LOADING_TEXT}
            />
        );
    };

    render() {
        const {paidData, currentIndex} = this.state;
        return (
            <SafeAreaViewContainer>
                {this._renderHeader()}
                <DismissKeyboard>
                    <CommisionBookingTopTab/>

                </DismissKeyboard>
            </SafeAreaViewContainer>
        );
    }
}
