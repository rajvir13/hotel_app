/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import React from "react";
import {
  View,
  Text,
  Image,
  FlatList,
  Keyboard,
  Modal,
  StatusBar,
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Platform,
} from "react-native";
import styles from "../styles";
import BaseClass from "../../../../utils/BaseClass";
import STRINGS from "../../../../utils/Strings";
import { Header } from "react-native-elements";
import { Spacer } from "../../../../customComponents/Spacer";
import COLORS from "../../../../utils/Colors";
import { LeftIcon } from "../../../../customComponents/icons";
import {
  SafeAreaViewContainer,
  MainContainer,
  ShadowViewContainer,
  ScrollContainer,
} from "../../../../utils/BaseStyle";
import { ICONS } from "../../../../utils/ImagePaths";
import { FONT } from "@utils/FontSizes";
import { FONT_FAMILY } from "@utils/Font";
// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";
import OrientationLoadingOverlay from "../../../../customComponents/Loader";
import { HotelItems } from "../component/itemComponent";

import { CustomeMonthYearPicker } from "../../../../customComponents/CustomeMonthYearPicker";
import moment from "moment";
import { GetCommsionPaidAction } from "../../../../redux/actions/GetCommsionAction";

let checkVar = false;

const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);
export default class CommisionPaidTab extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      selectdMonthText: "Select Month",
      isMonthYearPickerVisible: false,
      accessToken: undefined,
      customerLoginId: undefined,
      isModelVisible: false,
      currentItemClicked: undefined,
      accessToken: undefined,
      paidData: [],
      ownerLoginId: undefined,
    };
  }

  componentDidMount() {
    var month = new Date().getMonth();
    var year = new Date().getFullYear();
    console.log("month", month);
    console.log("year", year);
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        this.setState({
          accessToken: loginData.access_token,
          ownerLoginId: loginData.id,
          selectdMonthText:month.toString() + "/"+ year.toString()
        });

        this.showDialog();
        this.getCommision(loginData.id, loginData.access_token, month, year);
      }
    });
  }

  getCommision = (loginUserId, accssToken, month, year) => {
    this.showDialog();

    GetCommsionPaidAction(
      {
        accessToken: accssToken,
        ownerId: loginUserId,
        month: month,
        year: year,
      },
      (data) => this.getCommisonResponse(data)
    );
  };

  getCommisonResponse = (data) => {
    console.log(" paid data", data);
    this.hideDialog();
    const { response, status, message, booking_details, booking_count } = data;
    console.log("paid data for api is", data);
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (status === 200) {
        this.setState({
          paidData: booking_details,
        });
      } else if (status === 111) {
        this.hideDialog();
        this.showToastAlert(message);
      } else if (status === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else {
        this.hideDialog();
        this.showToastAlert(message);
      }

      this.hideDialog();
    }
  };

  //========================custome loader==============================
  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  componentDidUpdate(prevProps, preState, snapShot) {
    if (this.props.routeKey === 0) {
      if (checkVar === true) {
        checkVar = false;
        // this.hitApi();
      }
    } else if (this.props.routeKey === 1) {
      checkVar = true;
    }
  }

  newMonthYearSelected = (monthYear) => {
    const { accessToken, ownerLoginId, isMonthYearPickerVisible } = this.state;
    var monthYearData = moment(monthYear).format("MM-YYYY");

    var selectedmonth = monthYearData.toString().slice(0, 2);
    var selectedyear = monthYearData.toString().slice(3, 7);

    this.setState({
      isMonthYearPickerVisible: !isMonthYearPickerVisible,
      selectdMonthText: selectedmonth + "/" + selectedyear,
    });

    this.getCommision(ownerLoginId, accessToken, selectedmonth, selectedyear);
  };

  monthYearViewClick = () => {
    this.setState({ isMonthYearPickerVisible: true });
  };

  closeMonthYearPicker = () => {
    this.setState({ isMonthYearPickerVisible: false });
  };

  render() {
    const {
      paidData,
      currentIndex,
      selectdMonthText,
      isMonthYearPickerVisible,
      currentItemClicked,
      isModelVisible,
    } = this.state;

    return (
      <SafeAreaViewContainer>
        <DismissKeyboard>
          <ScrollContainer>
            <MainContainer>
              <View>
                <View style={styles.bookingView}>
                  <View style={{ flexDirection: "row" }}>
                    <Text style={styles.bookingCompletedText}>
                      Booking Completed :
                    </Text>
                    <Text style={styles.bookingCompletedText}>
                      {paidData.length}
                    </Text>
                  </View>
                  <View
                    style={{
                      backgroundColor: COLORS.white_color,
                      borderRadius: wp(4),
                      width: wp("30%"),
                      height: wp("8%"),
                      justifyContent: "center",
                    }}
                  >
                    <TouchableOpacity onPress={() => this.monthYearViewClick()}>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-evenly",
                        }}
                      >
                        <Text style={{ color: COLORS.greyButton_color2 }}>
                          {selectdMonthText}
                        </Text>
                        <Image
                          style={{ height: 15, width: 15, alignSelf: "center" }}
                          source={ICONS.DOWN_ARROW}
                        />
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
                {isMonthYearPickerVisible === true ? (
                  <CustomeMonthYearPicker
                    isMonthYearPickerVisible={isMonthYearPickerVisible}
                    newDateMonthSelected={(date) =>
                      this.newMonthYearSelected(date)
                    }
                    crossPickerAction={() => this.closeMonthYearPicker()}
                  />
                ) : (
                  console.log("isMonthYearPickerVisible")
                )}

                {paidData.length !== 0 ? (
                  <View style={{ backgroundColor: "white" }}>
                    <FlatList
                      style={{ width: wp(100) }}
                      data={paidData}
                      keyExtractor={(item, index) => index}
                      renderItem={({ item, index }) => (
                        <HotelItems
                          item={item}
                          itemData={(data) =>
                            this.setState({
                              currentItemClicked: data,
                              isModelVisible: true,
                            })
                          }
                          isModelVisible={isModelVisible}
                          currentItemClicked={currentItemClicked}
                          closeModel={() =>
                            this.setState({ isModelVisible: !isModelVisible })
                          }
                        />
                      )}
                    />
                  </View>
                ) : (
                  <Text
                    style={{
                      paddingVertical: wp(3),
                      textAlign: "center",
                      fontSize: FONT.TextNormal,
                      fontFamily: FONT_FAMILY.MontserratBold,
                    }}
                  >
                    NO DATA{" "}
                  </Text>
                )}
              </View>
            </MainContainer>

            {this._renderCustomLoader}
          </ScrollContainer>
        </DismissKeyboard>
      </SafeAreaViewContainer>
    );
  }
}
