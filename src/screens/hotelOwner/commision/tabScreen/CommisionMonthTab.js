/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import React from "react";
import {
  View,
  Text,
  Image,
  FlatList,
  Keyboard,
  StatusBar,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Platform,
} from "react-native";
import styles from "../styles";
import BaseClass from "../../../../utils/BaseClass";
import STRINGS from "../../../../utils/Strings";
import { Header } from "react-native-elements";
import { Spacer } from "../../../../customComponents/Spacer";
import COLORS from "../../../../utils/Colors";
import {
  SafeAreaViewContainer,
  MainContainer,
  ShadowViewContainer,
  ScrollContainer,
} from "../../../../utils/BaseStyle";
import { ICONS } from "../../../../utils/ImagePaths";

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";
import OrientationLoadingOverlay from "../../../../customComponents/Loader";
import { HotelItems } from "../component/itemComponent";
import { GetCommsionAction } from "../../../../redux/actions/GetCommsionAction";
import { FONT } from "@utils/FontSizes";
import { FONT_FAMILY } from "@utils/Font";
import DisplayRoomImages from "../component/ShowImage";
const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);
let checkVar = false;

export default class CommisionMonthTab extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      accessToken: "",
      ownerLoginId: "",
      commisionData: undefined,
      bookingCount: 0,
      isModelVisible: false,
      currentItemClicked:undefined
    };
  }

  componentDidMount() {
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        this.setState({
          accessToken: loginData.access_token,
          ownerLoginId: loginData.id,
        });
        this.showDialog();
        this.getCommision(loginData.id, loginData.access_token);
      }
    });
  }

  componentDidUpdate(prevProps, preState, snapShot) {
    if (this.props.routeKey === 0) {
      if (checkVar === true) {
        checkVar = false;
        AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
          if (result !== undefined && result !== null) {
            let loginData = JSON.parse(result);
            this.setState({
              accessToken: loginData.access_token,
              ownerLoginId: loginData.id,
            });
            this.showDialog();
            this.getCommision(loginData.id, loginData.access_token);
          }
        });
      }
    } else if (this.props.routeKey === 1) {
      checkVar = true;
    }
  }

  getCommision = (loginUserId, accssToken) => {
    this.showDialog();
    GetCommsionAction(
      {
        accessToken: accssToken,
        ownerId: loginUserId,
      },
      (data) => this.getCommisonResponse(data)
    );
  };

  getCommisonResponse = (data) => {
    const { response, status, message, booking_details, booking_count } = data;
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (status === 200) {
        this.setState({
          commisionData: booking_details,
          bookingCount: booking_count,
        });
      } else if (status === 111) {
        this.hideDialog();
        this.showToastAlert(message);
      } else if (status === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else {
        this.hideDialog();
        this.showToastAlert(message);
      }

      this.hideDialog();
    }
  };
  //========================custome loader==============================
  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  render() {
    const { commisionData, bookingCount,isModelVisible,currentItemClicked } = this.state;
    return (
      <SafeAreaViewContainer>
        <DismissKeyboard>
          <ScrollContainer>
            <MainContainer>
              <View>
                <View style={styles.bookingViewMonth}>
                  <View style={{ flexDirection: "row", marginLeft: wp("5%") }}>
                    <Text style={styles.bookingCompletedText}>
                      Booking Completed :
                    </Text>
                    <Text style={styles.bookingCompletedText}>
                      {" "}
                      {bookingCount}
                    </Text>
                  </View>
                </View>

                {bookingCount !== 0 ? (
                  <View style={{ backgroundColor: "white" }}>
                    <FlatList
                      style={{ width: wp(100) }}
                      data={commisionData}
                      keyExtractor={(item, index) => index}
                      renderItem={({ item, index }) => (
                        <HotelItems item={item} 
                         itemData={(data)=>this.setState({currentItemClicked:data,isModelVisible:true})}
                         isModelVisible={isModelVisible}
                         currentItemClicked={currentItemClicked}
                         closeModel={()=>this.setState({isModelVisible:!isModelVisible})}
                         />
                      )}
                    />
                  </View>
                ) : (
                  <Text
                    style={{
                      paddingVertical: wp(3),
                      textAlign: "center",
                      fontSize: FONT.TextNormal,
                      fontFamily: FONT_FAMILY.MontserratBold,
                    }}
                  >
                    NO DATA{" "}
                  </Text>
                )}
                
              </View>
            </MainContainer>

            {this._renderCustomLoader}
          </ScrollContainer>
        </DismissKeyboard>
      </SafeAreaViewContainer>
    );
  }
}
