import React, {useEffect, useState, useCallback, useRef} from "react";
import {
    Animated,
    Dimensions,
    StyleSheet,
    SafeAreaView,
    View,
} from "react-native";
import _ from "lodash";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import {TabView, SceneMap, TabBar} from "react-native-tab-view";

import COLORS from "../../../../utils/Colors";
import TabTipSVG from "../../../../../assets/images/TabView/TabTipSVG";
import {FONT} from "../../../../utils/FontSizes";
import {FONT_FAMILY} from "../../../../utils/Font";
import CommisionMonthTab from '../tabScreen/CommisionMonthTab'
import CommisionPaidTab from '../tabScreen/CommisionPaidTab'


const FirstRoute = () => (
    <View style={[styles.container]}>
        <CommisionMonthTab/>
    </View>);
    
const SecondRoute = () => (
    <View style={[styles.container]}>
        <CommisionPaidTab/>
    </View>);


const TabIndicator = ({width, tabWidth, index}) => {
    const marginLeftRef = useRef(new Animated.Value(index ? tabWidth : 0))
        .current;
    useEffect(() => {
        Animated.timing(marginLeftRef, {
            toValue: tabWidth,
            duration: 400
        }).start();
    }, [tabWidth]);

    return (
        <Animated.View
            style={{
                justifyContent: "flex-end",
                alignItems: "center",
                flex: 1,
                width: width,
                marginLeft: marginLeftRef,
                position: 'absolute',
                bottom: -12,
            }}
        >
            <TabTipSVG fillColor={COLORS.app_theme_color}/>
        </Animated.View>
    );
};

const CommisionBookingTopTab = () => {
    const [index, setIndex] = useState(0);
    const routes = [
        {key: "first", title: "This Month"},
        {key: "second", title: "Paid "},
      
    ];

    const renderScene = ({route}) => {
        switch (route.key) {
            case 'first':
                return <CommisionMonthTab/>;
            case 'second':
                return <CommisionPaidTab/>;
           
            default:
                return null;
        }
    };

    const renderIndicator = useCallback(
        ({getTabWidth}) => {
            const tabWidth = _.sum([...Array(index).keys()].map(i => getTabWidth(i)));

            return (
                <TabIndicator
                    width={getTabWidth(index)}
                    tabWidth={tabWidth}
                    index={index}
                />
            );
        },
        [index]
    );

    return (
        <View style={styles.container}>
            <TabView
                navigationState={{
                    index,
                    routes
                }}
                renderScene={renderScene}
                onIndexChange={setIndex}
                // initialLayout={{width: Dimensions.get("window").width}}
                renderTabBar={props => (
                    <TabBar
                        {...props}
                        scrollEnabled
                        tabStyle={styles.tabBar}
                        // indicatorStyle={styles.indicator}
                        style={styles.tabBarContainer}
                        labelStyle={styles.labelStyle}
                        renderIndicator={renderIndicator}
                    />
                )}
            />
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    tabBar: {
        width: wp(100) / 2,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.app_theme_color,
        marginHorizontal: wp(0.3)
    },
    indicator: {
        // backgroundColor: "#ccc"
        backgroundColor: "red",
    },
    tabBarContainer: {
        backgroundColor: COLORS.white_color,
        marginBottom: wp(3),
    },
    labelStyle: {
        marginVertical: 0,
        fontSize: FONT.TextExtraSmall,
        fontFamily: FONT_FAMILY.MontserratSemiBold,
        textAlign: 'center',
        // backgroundColor: "#fff",
        color: COLORS.white_color,
    }
});

export default CommisionBookingTopTab
