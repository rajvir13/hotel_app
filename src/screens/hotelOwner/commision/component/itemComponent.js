import React from "react";
import {
  View,
  FlatList,
  Text,
  Image,
  Platform,
  StatusBar,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
  Alert,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { ICONS } from "../../../../utils/ImagePaths";
import styles from "../styles";
import { SearchBar } from "react-native-elements";
import { Dropdown } from "react-native-material-dropdown";
import SearchIconSVG from "../../../../../assets/images/CustomerHome/searchIcon.svg";
import {
  SafeAreaViewContainer,
  MainContainer,
  ShadowViewContainer,
} from "../../../../utils/BaseStyle";
// import { Rating, AirbnbRating } from "react-native-elements";
import COLORS from "../../../../utils/Colors";
import { Spacer } from "../../../../customComponents/Spacer";
import { AirbnbRating } from "react-native-elements";
import { API } from "../../../../redux/constant";
import DisplayRoomImages from "../component/ShowImage";

const ImagIcicked = (props, item) => {
  props.itemData(item);
};

const clostModel = (props) => {
  props.closeModel();
};

const getAdditionalDiscount = (regularPrice) => {
  return (Number(regularPrice) * 40) / 100;
};

const HotelItems = (props) => {
  return (
    <View  style={styles.flatListOuterView}>
      <TouchableOpacity
        onPress={
          () => ImagIcicked(props, props.item)
          // props.itemClicked(props.item)
        }
      >
        <Image
          source={{
            uri:
              props.item.room_images.image !== null
                ? API.IMAGE_BASE_URL + props.item.room_images.image
                : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
          }}
          style={styles.hotelImageView}
        />
      </TouchableOpacity>
      <View style={{ justifyContent: "space-around",height:wp('35%'), flex: 1, }}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            marginTop: 5,
           
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.bookedByText}>Booked By :</Text>
            <Text style={styles.bookedByText}>{props.item.customer_name}</Text>
          </View>

          <View style={styles.rightTickImage}>
            <Image source={ICONS.RightTickImage} />
          </View>
        </View>

        <View style={{ flexDirection: "row", marginLeft: wp("2%") }}>
          <Text style={styles.dateText}>Date :</Text>
          <Text style={styles.dateText}>{props.item.booking_created}</Text>
        </View>

        <View
          style={{
            flexDirection: "row",
            marginTop: wp("1%"),
            marginLeft: wp("2%"),
          }}
        >
          <Text style={styles.roomTypeText}>Room Type :</Text>
          <Text style={styles.roomTypeText}>{props.item.room_type}</Text>
        </View>

        <View style={{ flexDirection: "row", marginTop: wp("2%") }}>
          <Text style={styles.checkInText}>Check In Time :</Text>
          <Text style={styles.timeTextStyle}>{props.item.check_in_time}</Text>
        </View>

        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            marginBottom: 5,
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.checkInText}>Check Date :</Text>
            <Text style={styles.timeTextStyle}>{props.item.check_in}</Text>
          </View>
          <View style={{ flexDirection: "row" }}>
            <Text style={styles.checkInText}>Price :</Text>
            {props.item.is_special === true ? (
              <Text style={styles.priceText}>
                {"\u20B9"}
                {getAdditionalDiscount(props.item.room_regular_price)}
              </Text>
            ) : (
              <Text style={styles.priceText}>
                {"\u20B9"}
                {props.item.room_price}
              </Text>
            )}
          </View>
        </View>
      </View>

      {props.isModelVisible === true && (
        <DisplayRoomImages
          roomModalVisible={props.isModelVisible}
          data={props.currentItemClicked}
          closeRoomModal={() => clostModel(props)}
        />
      )}
    </View>
  );
};

const MonthsComponent = (props) => {
  return (
    <View
      style={{ height: wp("50%"), width: wp("50%"), backgroundColor: "red" }}
    />
  );
};

export { HotelItems, MonthsComponent };
