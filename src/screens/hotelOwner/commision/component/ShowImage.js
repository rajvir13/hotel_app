import React, { Component } from "react";
import {
  View,
  Text,
  Modal,
  StyleSheet,
  Image,
  TouchableOpacity,
  Alert,
  FlatList,
} from "react-native";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { FONT_FAMILY } from "../../../../utils/Font";
import { FONT } from "../../../../utils/FontSizes";
import COLORS from "../../../../utils/Colors";
import { API } from "../../../../redux/constant";
import STRINGS from "../../../../utils/Strings";
import { ICONS } from "../../../../utils/ImagePaths";
import FastImage from "react-native-fast-image";
import CrossComponent from "../../../../../assets/images/BookingIcons/CrossSVG";

export default class DisplayRoomImages extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisiblePass: false,
      value: "",
      indexValue: 0,
    };
  }

  render() {
    const { value } = this.props.roomModalVisible;
  console.log("props are",this.props.roomModalVisible)
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={this.props.roomModalVisible}
      >
        <View style={styles.centeredView}>
          <View
            style={{
              marginBottom: wp(8),
              backgroundColor: "transparent",
              width: wp(100),
              height:wp(50),
              borderRadius: wp(3),
              height: hp(70),
              alignSelf: "center",
            }}
          >
            <View
              style={{
                height: hp(50),
                width: wp(100),
                paddingHorizontal: wp(1),
              }}
            >
              <FastImage
                style={{
                  justifyContent: "center",
                  marginVertical: wp(1),
                  borderRadius: wp(3),
                  width: wp(100),
                  height: hp(60),
                }}
                source={{
                  uri:
                  this.props.data.room_images.image !== null
                      ? API.IMAGE_BASE_URL + this.props.data.room_images.image
                      : "https://travelji.com/wp-content/uploads/Hotel-Tips.jpg",
                }}
                resizeMode={FastImage.resizeMode.stretch}
              />
             
              <TouchableOpacity
                hitSlop={{ top: wp(5), left: wp(5), bottom: wp(5) }}
                style={{
                  position: "absolute",
                  alignSelf: "flex-end",
                  marginTop: wp(-3),
                }}
                onPress={() => {
                  this.props.closeRoomModal();
                }}
              >
                <CrossComponent width={wp(9)} height={wp(9)} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "flex-end",
    // justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.6)",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});
