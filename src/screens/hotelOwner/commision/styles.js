import React from "react";
import styled from "styled-components";
import COLORS from "../../../utils/Colors";
import { FONT } from "../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../utils/Font";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  bookingView: {
    flexDirection: "row",
    width: wp("100%"),
    height: wp("12%"),
    alignItems: "center",
    justifyContent: "space-around",
  },

  bookingViewMonth:{
    flexDirection: "row",
    width: wp("100%"),
    height: wp("12%"),
    alignItems: "center",
  },

  selectMonthView: {
    width: wp("35%"),
    backgroundColor: "white",
    height: wp("8%"),
    justifyContent: 'space-evenly',
    paddingHorizontal: 5,
    borderRadius: wp("2%"),
    flexDirection:'row',
    alignItems:'center'
  },
  bookingCompletedText: {
    fontFamily: FONT_FAMILY.Montserrat,
    fontSize: FONT.TextSmall_2,
  },
  hotelImageView: {
    height: wp("35%"),
    width: wp("27%"),
  },

  flatListOuterView: {
    flexDirection: "row",
    alignSelf: "center",
    width: wp("95%"),

    marginTop: wp("2%"),
    height: wp("35%"),
    borderRadius: wp("2%"),
    borderWidth: 1,
    borderColor: "white",

    borderWidth: 1,
    borderRadius: 2,
    borderColor: "#ddd",
    borderBottomWidth: 1,
    // shadowColor: '#000',
    // shadowRadius: 0.3,
  },
  rightTickImage: {
    width: wp("8%"),
    height: wp("8%"),
    resizeMode: "cover",
    justifyContent: "center",
    alignItems: "center",
    marginRight: wp("3%"),
  },

  bookedByText: {
    fontFamily: FONT_FAMILY.Poppins,
    fontWeight: "bold",
    marginLeft: wp("2%"),
    fontSize:wp('3.7%')
  },
  checkInText: {
    fontFamily: FONT_FAMILY.Roboto,
    fontWeight: "bold",
    marginLeft: wp("1.5%"),
    fontSize: FONT.TextSmall_2,
  },

  priceText: {
    fontFamily: FONT_FAMILY.Roboto,
    fontWeight: "bold",
    marginRight: wp("3%"),
    color: "red",
    fontSize: FONT.TextSmall_2,
  },
  roomTypeText: {
    color: COLORS.greyButton_color2,
    fontFamily: FONT_FAMILY.Montserrat,
    fontSize: FONT.TextSmall_2,
  },
  mainViewStyle: {
    height: wp("20%"),
    flexDirection: "row",
    // alignItems: "stretch",
    width: wp("100%"),

    justifyContent: "space-evenly",
    padding: 0,
    margin: 0,
  },
  dateText: {
    fontSize: FONT.TextSmall_2,

    fontFamily: FONT_FAMILY.Montserrat,
  },
  timeTextStyle: {
    fontSize: FONT.TextSmall_2,
  },
  selectMonthDonwArrowImg:{

    height:wp('5%'),
    height:wp('5%')

  }
});

export default styles;
