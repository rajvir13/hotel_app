import React from "react";
import {
  View,
  Text,
  Image,
  Keyboard,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  StyleSheet,KeyboardAvoidingView,
  Button,
  TouchableWithoutFeedback,
  Platform,
} from "react-native";
import {
  SafeAreaViewContainer,
  MainContainer,
  ShadowViewContainer,
  ScrollContainer,
} from "../../../utils/BaseStyle";
import { Header } from "react-native-elements";
import COLORS from "../../../utils/Colors";
import { FONT } from "../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../utils/Font";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { CreditCardInput } from "../../../localModule/react-native-credit-card-input";
import { ICONS } from "../../../utils/ImagePaths";
import { LeftIcon } from "../../../customComponents/icons";

import CustomTextInput from "../../../customComponents/CustomTextInput";
import { TextInput } from "react-native-paper";
import BaseClass from "../../../utils/BaseClass";
import { PaymentByCard } from "../../../redux/actions/PaymentByCardAction";
import AsyncStorage from "@react-native-community/async-storage";
import STRINGS from "../../../utils/Strings";
import SuccessModel from "./successModel/paymentSuccesfulModil";
import OrientationLoadingOverlay from "../../../customComponents/Loader";

export default class AddPayment extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      amount: undefined,
      accessToken: undefined,
      ownerId: undefined,
      cardData: { valid: false },
      total: this.props.route.params.amount,
      commision: this.props.route.params.commisionAmount,
      basicPlanAmt: this.props.route.params.basicPlanAmt,
      successModelVisible: false,
      transectionId: undefined,
      isLoading: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        // const { hotel_details } = loginData;
       
        if(loginData.hotel_details!==undefined)
        {
          this.setState({
            accessToken: loginData.access_token,
            ownerId: loginData.hotel_details.owner,
          });
        }
       else
       {
        this.setState({
          accessToken: loginData.access_token,
          ownerId:loginData.owner.id,
        });

       }
      }
    });
  }

  _renderHeader() {
    const { toggleMapView, user_profile } = this.state;
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={<LeftIcon onPress={() => navigation.goBack()} />}
        centerComponent={{
          text: "Payment",
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  submitPress = (totalAmt, commissionAmt, basicPlanAmount) => {
    const { amount, cardData, accessToken, ownerId } = this.state;


if(cardData.values !== undefined)
{
  let expMonth = cardData.values.expiry.substring(0, 2);
  let expYear = "20" + cardData.values.expiry.substring(3, 5);




  if (
    cardData.status.number === "invalid" ||
    cardData.status.number === "incomplete"
  ) {
    this.showToastAlert("Card number is not valid");
  } else if (
    cardData.status.name === "invalid" ||
    cardData.status.name === "incomplete"
  ) {
    this.showToastAlert("Card holder Name is not valid");
  } else if (
    cardData.status.expiry === "invalid" ||
    cardData.status.expiry === "incomplete"
  ) {
    this.showToastAlert("Card expiry is not valid");
  } else if (
    cardData.status.cvc === "invalid" ||
    cardData.status.cvc === "incomplete"
  ) {
    this.showToastAlert("Card CVC is not valid");
  }
  
  else {
    this.setState({ isLoading: true });
    this.showDialog();

    PaymentByCard(
      {
        card_number: cardData.values.number,
        card_holder_name: cardData.values.name,
        cvc: Number(cardData.values.cvc),
        expiry_mm: Number(expMonth),
        expiry_yy: expYear,
        plan_amount: Number(basicPlanAmount),
        amount: Number(totalAmt),
        commission_amount: Number(commissionAmt),
        accessToken: accessToken,
        user_id: Number(ownerId),
      },
      (data) => this.paymentResponse(data)
    );
  }

}

   
  };

  paymentResponse = (data) => {
    this.setState({ isLoading: false });

    this.hideDialog();
    const { response, transaction_id, code } = data;
    if (data === "Network request failed" || data == "TypeError: Network request failed") {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (code === 200) {
        this.setState({
          successModelVisible: true,
          transectionId: transaction_id,
        });
      } else if (code === 400) {
        this.showToastAlert("Invalid Parameters");
        
      } else {
        this.showToastAlert("Payment Failed");
      }
    }
  };

  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  okSuccessClicked = () => {
    const { successModelVisible } = this.state;
    const { navigation } = this.props;
    this.setState({ successModelVisible: !successModelVisible });
    navigation.goBack();
  };

  render() {
    const {
      submitted,
      error,
      total,
      commision,
      successModelVisible,
      transectionId,
      basicPlanAmt,
    } = this.state;

    console.log("successModelVisible", successModelVisible);
    console.log("transectionId", transectionId);

    return (
      <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : "height"}
      style={styles.container}
    >
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <ScrollView>
         
          <View style={{ marginTop: wp("2%") }}>
            {/* <Image source={ICONS.paymentImg}
             style={styles.paymentImgStyle}/> */}
            <CreditCardInput
              requiresName
              onChange={(cardData) => this.setState({ cardData })}
            />
          </View>
          <View style={styles.outerView}>
            <Text style={styles.amountText}>Amount</Text>
            <Text
              placeholder="amount"
              type="number"
              editable={false}
              inputmode="numeric"
              pattern="[0-9]*"
              onChangeText={(text) => this.setState({ amount: text })}
              style={styles.amountTextInput}
            >
              {total}
            </Text>
          </View>
          {successModelVisible == true && successModelVisible !== undefined ? (
            <SuccessModel
              isModelVisible={successModelVisible}
              transID={transectionId}
              onCancelPress={() => this.okSuccessClicked()}
            />
          ) : (
            console.log("")
          )}
          <TouchableOpacity
            onPress={() => this.submitPress(total, commision, basicPlanAmt)}
          >
            <View style={styles.buttonStyle}>
              <Text style={styles.buttonText}>Submit</Text>
            </View>
          </TouchableOpacity>
         
        </ScrollView>
        {this._renderCustomLoader()}
      </SafeAreaViewContainer>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
  },
  amountTextInput: {
    marginTop: wp("3%"),
    fontSize: wp("4%"),
    borderWidth: 2,
    borderColor: "gray",
    borderRadius: wp("2%"),
    width: wp("90%"),
    alignSelf: "center",
    height: wp("10%"),
    fontWeight: "bold",
    padding: wp("1.5%"),
  },
  amountText: {
    marginTop: wp("3%"),
    marginLeft: wp("5%"),
    fontWeight: "bold",
    fontSize: wp("3.9%"),
  },
  outerView: {
    justifyContent: "center",
  },
  paymentImgStyle: {
    width: wp("100%"),
    height: wp("48%"),
    resizeMode: "cover",
    position: "absolute",
  },
  buttonText: {
    fontSize: 15,
    fontWeight: "bold",
    color: "white",
    textAlign: "center",
    textAlignVertical: "center",
    fontFamily: FONT_FAMILY.Montserrat,
  },
  buttonStyle: {
    width: wp(90),
    paddingVertical: wp(3.5),
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#4582E5",
    borderWidth: 1,
    borderColor: "transparent",
    height: 50,
    // width:wp('80%'),
    borderRadius: 25,
    margin: 10,
    marginTop: wp("5%"),
    alignSelf: "center",
  },
  buttonWrapper: {
    padding: 10,
    zIndex: 100,
  },
  alertTextWrapper: {
    flex: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  alertIconWrapper: {
    padding: 5,
    flex: 4,
    justifyContent: "center",
    alignItems: "center",
  },
  alertText: {
    color: "#c22",
    fontSize: 16,
    fontWeight: "400",
  },
  alertWrapper: {
    backgroundColor: "#ecb7b7",
    justifyContent: "space-between",
    alignItems: "center",
    flexDirection: "row",
    flexWrap: "wrap",
    borderRadius: 5,
    paddingVertical: 5,
    marginTop: 10,
  },
});
