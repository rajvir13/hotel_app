import React, { Component } from "react";
import {
  View,
  Text,
  Modal,
  StyleSheet,
  Image,
  TouchableOpacity,
  Alert,
  FlatList,
} from "react-native";

import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Spacer } from "../../../../customComponents/Spacer";
import { FONT_FAMILY } from "../../../../utils/Font";
import { FONT } from "../../../../utils/FontSizes";
import COLORS from "../../../../utils/Colors";
import STRINGS from "../../../../utils/Strings";
import { ICONS } from "../../../../utils/ImagePaths";
import FastImage from "react-native-fast-image";
import { API } from "../../../../redux/constant";

export default class SuccessModel extends Component {
 
    render() {
    // const { value } = this.props;
    console.log("value is", this.props);
    return (
      <Modal animationType="slide" transparent={true} visible={this.props.isModelVisible}>
        <View style={styles.centeredView}>
          <View
            style={{
              marginBottom: wp(8),
              backgroundColor: "white",
              justifyContent: "center",
              alignItems: "center",
              width: wp(90),
              borderRadius: wp(3),
            }}
          >
            <Spacer space={2} />
            <View style={{justifyContent:'center',alignSelf:'center'}}>
              <Text
                style={{
                  fontSize: FONT.TextSmall,
                  fontFamily: FONT_FAMILY.Montserrat,
                  fontWeight: "bold",
                  color: "green",
                  fontSize:wp('4.5%'),
                  alignSelf:'center'
                }}
              >
               TRANSACTION SUCCESSFUL 
              </Text>
              <View>
                <Text
                  style={{
                    fontSize: FONT.TextSmall,
                    fontFamily: FONT_FAMILY.Montserrat,
                    fontWeight: "bold",
                    marginTop:wp('2%'),
                    alignSelf:'center'
                    
                 
                  }}
                >
                  Transection Id : {this.props.transID}
                </Text>
              </View>
            </View>
            <Spacer space={1} />

            <View
              style={{
                justifyContent: "space-around",
                alignItems: "center",
                paddingHorizontal: wp(1),
                flexDirection: "row",
                paddingTop: wp(6),
                paddingBottom: wp(12),
              }}
            >
              <TouchableOpacity
                onPress={() => this.props.onCancelPress()}
                style={{
                  borderRadius: wp(3),
                  width: wp(33),
                  paddingVertical: wp(3.5),
                  backgroundColor: COLORS.customerType,
                }}
              >
                <Text
                  style={{
                    textAlign: "center",
                    letterSpacing: wp(0.1),
                    fontSize: FONT.TextSmall,
                    fontFamily: FONT_FAMILY.MontserratSemiBold,
                    color: COLORS.white_color,
                  }}
                >
                  OK
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    // justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.6)",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center",
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
  },
});
