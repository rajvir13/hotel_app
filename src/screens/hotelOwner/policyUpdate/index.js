import React from "react";
import {
  View,
  FlatList,
  Text,
  Image,
  Platform,
  StatusBar,
  Dimensions,
  Button,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  ImageBackground,
  TouchableHighlight,
  Keyboard,
  AppRegistry,
  Alert,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Rating, AirbnbRating, Header, Input } from "react-native-elements";
import { CommonActions } from "@react-navigation/native";
import BaseClass from "../../../utils/BaseClass";
import {
  ShadowViewContainer,
  SafeAreaViewContainer,
  MainContainer,
  ScrollContainer,
} from "../../../utils/BaseStyle";
import { Spacer } from "../../../customComponents/Spacer";
import COLORS from "../../../utils/Colors";
import {
  LeftIcon,
  RightIcon,
  SecondRightIcon,
} from "../../../customComponents/icons";
import STRINGS from "../../../utils/Strings";
import { FONT } from "../../../utils/FontSizes";
import { FONT_FAMILY } from "../../../utils/Font";
import { LogoutAction } from "../../../redux/actions/LogoutAction";
import { ICONS } from "../../../utils/ImagePaths";
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import styles from "./styles";
import { ListItem, CheckBox } from "react-native-elements";
import ImagePicker from "react-native-image-crop-picker";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import DocumentPicker from "react-native-document-picker";
import * as _ from "lodash";
import ClockIconSVG from "../../../../assets/images/Register/clockIconSVG";
import { ScrollView } from "react-native-gesture-handler";
import { UpdateHotelPolicyAction } from "../../../redux/actions/UpdateHotelPolicy";
import { connect } from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";
import * as DeviceInfo from "react-native-device-info";

//==================Dismiss keyboard  =======================
const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

//=================== main class declareation======================
class UpdateOwnerPolicy extends BaseClass {
  //===================== constructor================================
  constructor(props) {
    super(props);
    console.warn("clicked me");
    const {
      checkInTime,
      checkoutTime,
      genralIntstruction,
      docTitle,
      docFile,
      hotelID,
    } = this.props.route.params;

    console.log("type is", typeof checkInTime);

    // let checkInTime1 = checkInTime.toLocaleTimeString('it-IT');
    let timeCheckIn = this.formatAMPM(checkInTime.slice(0, 5));
    let timeCheckOut = this.formatAMPM(checkoutTime.slice(0, 5));

    this.state = {
      value1: 1,
      checked: false,
      dateTimeVisibility: false,
      currentSelectedData: "",
      inDate: timeCheckIn,
      outDate: timeCheckOut,
      addPolicyFile: "Add Policy File",
      pdfFile: docFile,
      pdfFileName: "",
      GeneralInstruction: docTitle,
      // pdfDocument: policyDoc[0].file,
      // GeneralInstruction: genralIntstruction,
      isDatePickerOpen: false,
      accessToken: "",
      hotelId: hotelID,
      authToken: undefined,
      isPdfRemoved: false,
    };
  }

  //===================policy checked======================

  handleChange = () => {
    let { checked } = this.state;
    checked = !checked;
    this.setState({ checked });
  };

  //================ component life cycle method==========
  componentDidMount() {
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        this.setState({
          authToken: loginData.access_token,
        });
      }
    });
  }

  componentWillReceiveProps(nextProps, nextContext) {
    this.hideDialog();
    const { navigation } = this.props;
    const { HotelPolicyResponse } = nextProps.hotelHotelPolicyState;
    if (HotelPolicyResponse !== undefined && HotelPolicyResponse !== null) {
      if (HotelPolicyResponse === "Network Error") {
        this.hideDialog();
        this.showToastAlert(STRINGS.CHECK_INTERNET);
      } else {
        const { response, message, status } = HotelPolicyResponse;
        if (response !== undefined) {
          if (status === 200) {
            this.hideDialog();
            this.showToastSucess(message);
            this.props.navigation.goBack();
          } else {
            this.hideDialog();
            this.showToastAlert(message);
          }
        } else if (status === 111 && message !== undefined) {
          this.hideDialog();
          if (message.non_field_errors !== undefined) {
            this.showToastAlert(message.non_field_errors[0]);
          } else {
            AsyncStorage.removeItem(STRINGS.loginCredentials);
            navigation.dispatch(
              CommonActions.reset({
                index: 0,
                routes: [{ name: "login" }],
              })
            );
            this.showToastAlert(message);
          }
        } else if (status === 401) {
          this.hideDialog();
          this.showToastAlert(message);
        } else {
          this.hideDialog();
          this.showToastAlert("server error");
        }
        this.hideDialog();
      }
    }
  }

  //**************************** check in button clicked ******************/
  CheckIn = () => {
    let { dateTimeVisibility, isDatePickerOpen } = this.state;
    dateTimeVisibility = true;
    this.setState({
      dateTimeVisibility: dateTimeVisibility,
      isDatePickerOpen: true,
    });
    this.setState({ currentSelectedData: "In" });
  };
  //**************************** check Out button clicked ******************/

  CheckOut = () => {
    let { dateTimeVisibility } = this.state;
    dateTimeVisibility = true;
    this.setState({
      dateTimeVisibility: dateTimeVisibility,
      isDatePickerOpen: true,
    });

    this.setState({ currentSelectedData: "Out" });

    console.warn("out:", this.state.dateTimeVisibility);
  };

  //********************  hide Data picker View ***************** */
  hideDatePicker = () => {
    console.warn("hide");
    let { dateTimeVisibility } = this.state;
    dateTimeVisibility = false;
    this.setState({ dateTimeVisibility: false });
  };

  openDocument = (url) => {
    console.warn("url is", url);
    if (url !== undefined) {
      const { navigate } = this.props.navigation;
      this.props.navigation.navigate("PdfViewScreen", { pdfData: url });
    }

    // let urlLink = '';
    // if (url.uri !== undefined) {
    //     urlLink = url.uri
    // } else {
    //     urlLink = url
    // }
    // const localFile = `${RNFS.DocumentDirectoryPath}/PolicyDocument.pdf`;

    // const options = {
    //     fromUrl: urlLink,
    //     toFile: localFile
    // };
    // RNFS.downloadFile(options).promise
    //     .then(() => FileViewer.open(localFile))
    //     .then(() => {
    //         // success
    //         console.log("success");
    //     })
    //     .catch(error => {
    //         console.log("error", error);
    //     });
  };

  //***************************** Get Date and time  ******************/
  handleConfirm = (time) => {
    console.warn("A date has been picked: ", time);

    let { currentSelectedData } = this.state;

    if (currentSelectedData === "In") {
      this.setState({ dateTimeVisibility: false });
      var time1 = time.toLocaleTimeString("it-IT");
      var newTime = time1.slice(0, 5);

      this.setState({ inDate: this.formatAMPM(newTime) });
    } else {
      this.setState({ dateTimeVisibility: false });
      var time1 = time.toLocaleTimeString("it-IT");
      var newTime = time1.slice(0, 5);
      this.setState({ outDate: this.formatAMPM(newTime) });
    }
  };

  formatAMPM = (data) => {
    if (data !== null && data !== undefined) {
      const [time] = data.split(" ");
      let [sHours, minutes] = time.split(":");

      // const [sHours, minutes] = time24.match(/([0-9]{1,2}):([0-9]{2})/).slice(1);
      const period = +sHours < 12 ? "AM" : "PM";
      const hours = +sHours % 12 || 12;

      return `${hours}:${minutes} ${period}`;
    } else {
      return "12:00 PM";
    }
  };

  // formatAMPM = (data) => {
  //   if (data !== undefined && data !== null && data !== "") {
  //     const [time] = data.split(" ");

  //     let [hours, minutes] = time.split(":");
  //     console.log("hour", hours);
  //     console.log("minutes", minutes);

  //     var ampm = hours >= 12 ? "PM" : "AM";
  //     hours = hours % 12;
  //     hours = hours ? hours : 12; // the hour '0' should be '12'

  //     minutes = minutes < 10 ? "0" + minutes : minutes;
  //     var strTime = hours + ":" + minutes + " " + ampm;
  //     return strTime;
  //   } else {
  //     return "12:00 PM";
  //   }
  // };

  //******************************** pdf picker  ****************************/

  async OpenPdfPicker(pdfFile) {
    // const {isPdfRemoved, pdfFile} = this.state;
    if (pdfFile === null) {
      try {
        var res = await DocumentPicker.pick({
          type: [DocumentPicker.types.pdf],
        });
        this.setState({
          pdfFile: res,
          pdfFileName: res.name,
          isPdfRemoved: false,
        });
        this.showToastSucess("PDF added");
      } catch (err) {
        if (DocumentPicker.isCancel(err)) {
          // User cancelled the picker, exit any dialogs or menus and move on
        } else {
          throw err;
        }
      }
    } else {
      Alert.alert(
        "Hotel App",
        STRINGS.choose_option,
        [
          { text: "Cancel", style: "cancel" },
          {
            text: "Remove PDF",
            onPress: () => this.removePdfFile(),
          },
          {
            text: "Open PDF",
            onPress: () => this.openDocument(pdfFile),
          },
        ],
        { cancelable: false }
      );
    }
  }

  //========================= customer loder ==========================
  _renderCustomLoader = () => {
    const { isLoading } = this.state;

    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  //============================ render header ===============================
  _renderHeader() {
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={<LeftIcon onPress={() => navigation.goBack()} />}
        centerComponent={{
          text: STRINGS.HEADER_POLICY_UPDATE,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  //===================== update password=========================

  removePdfFile = () => {
    const { isPdfRemoved } = this.state;
    this.showToastAlert("PDF removed successfully");
    this.setState({ isPdfRemoved: true, pdfFile: null, pdfFileName: "" });
  };

  _renderCheckInOutView = () => {
    const { dateTimeVisibility, inDate, outDate } = this.state;
    return (
      <View style={styles.CheckInCheckOutTextStyle}>
        <View
          style={{ flexDirection: "column", marginTop: wp("4%") }}
          onStartShouldSetResponder={() => this.CheckIn()}
        >
          <Text style={styles.TextCheckin}>{STRINGS.check_in}</Text>
          <Spacer space={1} />
          <ShadowViewContainer>
            <View style={styles.CheckInFrameStyle}>
              <Spacer row={1} />
              <ClockIconSVG width={wp(5)} height={wp(5)} />
              <Text style={styles.timeTextStyle}>{inDate}</Text>
              <Image style={styles.clockIconStyle} source={ICONS.DOWN_ARROW} />
            </View>
          </ShadowViewContainer>
          <DateTimePickerModal
            isVisible={dateTimeVisibility}
            titleIOS="Pick a time"
            mode="time"
            locale="en_GB" // Use "en_GB" here
            date={new Date()}
            onConfirm={(time) => this.handleConfirm(time)}
            onCancel={this.hideDatePicker}
          />
        </View>

        <View
          style={{ flexDirection: "column", marginTop: wp("4%") }}
          onStartShouldSetResponder={() => this.CheckOut()}
        >
          <Text style={styles.TextCheckin}>{STRINGS.check_out}</Text>
          <Spacer space={1} />
          <ShadowViewContainer>
            <View style={styles.CheckInFrameStyle}>
              <Spacer row={1} />
              <ClockIconSVG width={wp(5)} height={wp(5)} />
              <Text style={styles.timeTextStyle}>{outDate}</Text>
              <Image style={styles.clockIconStyle} source={ICONS.DOWN_ARROW} />
            </View>
          </ShadowViewContainer>
        </View>
      </View>
    );
  };

  _renderPolicyAndDocView = () => {
    const { checked, GeneralInstruction, isPdfRemoved, pdfFile } = this.state;
    return (
      <View style={styles.PolicyDocumentView}>
        <View style={{ marginLeft: wp("5%") }}>
          <Text style={styles.PolicyTextStyle}>{STRINGS.policy}</Text>
          <Spacer space={2} />
          <View style={styles.GeneralInstructionTextStyle}>
            <TextInput
              placeholder="General Instruction"
              placeholderTextColor="gray"
              multiline={true}
              value={GeneralInstruction}
              marginLeft={wp("2%")}
              onChangeText={(text) =>
                this.setState({ GeneralInstruction: text })
              }
            />
          </View>
          <Spacer space={2} />

          <View style={styles.AddPolicyFileView}>
            <TouchableOpacity
              activeOpacity={0.5}
              onPress={() => this.OpenPdfPicker(pdfFile)}
            >
              <Image
                style={styles.pdfIconStyle}
                // source={ICONS.PDF_ICON}
                source={ICONS.PDF_ICON}
              />

              <TouchableOpacity
                style={{ position: "absolute", alignSelf: "flex-end" }}
                activeOpacity={0.5}
                onPress={() => this.removePdfFile()}
              >
                {pdfFile === null ? (
                  <Image style={styles.cancelPdfStyle} />
                ) : (
                  <Image
                    style={styles.cancelPdfStyle}
                    source={ICONS.CLOSE_WINDOW}
                  />
                )}
              </TouchableOpacity>
            </TouchableOpacity>
            <Spacer row={1.5} />
            <Text style={styles.AddPolicyFileText}>
              {STRINGS.addPolicyFile}
            </Text>
          </View>
          <Spacer space={2} />

          <View style={styles.AddPolicyFileView}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <View>
                <CheckBox
                  title={""}
                  textStyle={{
                    fontFamily: FONT_FAMILY.Montserrat,
                    fontSize: FONT.TextSmall,
                    fontWeight: "normal",
                  }}
                  checked={checked}
                  onPress={() => this.handleChange()}
                />
              </View>
              <TouchableOpacity
                onPress={() => this.termAndConditionAction()}
                underlayColor="white"
              >
                <Text style={styles.termConditionTextStyle}>
                  {STRINGS.TERM_CONDITION_TEXT}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <Spacer space={2} />

          <TouchableOpacity onPress={() => this.onSubmit()}>
            <View style={styles.buttonStyle}>
              <Text style={styles.buttonText}>{STRINGS.HEADER_UPDATE}</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  termAndConditionAction = () => {
    const { navigate } = this.props.navigation;
    navigate("MyWebComponent");
  };

  //=============== policy submit=================
  onSubmit = () => {
    const {
      inDate,
      outDate,
      pdfFileName,
      authToken,
      GeneralInstruction,
      hotelId,
      checked,
      pdfFile,
      isPdfRemoved,
    } = this.state;
    if (!checked) this.showToastAlert("Please accept terms and conditions");
    else if (pdfFile === null)
      this.showToastAlert("Please upload policy & documents pdf file");
    // console.log("indate",inDate)
    // console.log("outdate",outDate)
    // console.log("24 hour indate",this.convertTime12to24(inDate))
    // console.log("24 hour outdate",this.convertTime12to24(outDate))
    else {
      let inDate24Hour = this.convertTime12to24(inDate);
      let outDate24Hour = this.convertTime12to24(outDate);
      this.props.updatehotelPolicyApi({
        inDate24Hour,
        outDate24Hour,
        pdfFileName,
        device_id: DeviceInfo.getUniqueId(),
        authToken,
        pdfFile,
        hotelId,
        GeneralInstruction,
        isPdfRemoved,
      });
      this.showDialog();
    }
  };

  convertTime12to24 = (time12h) => {
    const [time, modifier] = time12h.split(" ");

    let [hours, minutes] = time.split(":");

    if (hours === "12") {
      hours = "00";
    }

    if (modifier === "PM") {
      hours = parseInt(hours, 10) + 12;
    }

    return `${hours}:${minutes}`;
  };

  //=======================End Change and update password ========================

  render() {
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <DismissKeyboard>
          <MainContainer>
            <ScrollContainer style={{ backgroundColor: COLORS.white_color }}>
              <View style={{ width: wp(100) }}>
                {this._renderCheckInOutView()}
                <Spacer space={2} />
                {this._renderPolicyAndDocView()}
              </View>
            </ScrollContainer>
            {this._renderCustomLoader()}
          </MainContainer>
        </DismissKeyboard>
      </SafeAreaViewContainer>
    );
  }
}

const mapStateToProps = (state) => ({
  hotelHotelPolicyState: state.UpdateHotelPolicyReducer,
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    updatehotelPolicyApi: (payload) =>
      dispatch(UpdateHotelPolicyAction(payload)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(UpdateOwnerPolicy);
