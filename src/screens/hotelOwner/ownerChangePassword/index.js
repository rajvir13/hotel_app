import React from "react";
import {
    View,
    Text,
    Platform,
    StatusBar,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Keyboard,
} from "react-native";
import {Header} from "react-native-elements";
import BaseClass from "../../../utils/BaseClass";
import {
    SafeAreaViewContainer,
    ScrollContainer,
} from "../../../utils/BaseStyle";
import {Spacer} from "../../../customComponents/Spacer";
import COLORS from "../../../utils/Colors";
import {
    LeftIcon,
} from "../../../customComponents/icons";
import STRINGS from "../../../utils/Strings";
import {FONT} from "../../../utils/FontSizes";
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import styles from "./styles";
import {
    CustoemTextComponents,
} from "../hotelProfile/components/customeTextField";
import {OwnerChangePasswordAction} from "../../../redux/actions/ChangePassword";
import {FONT_FAMILY} from "../../../utils/Font";
import {API} from "../../../redux/constant";
import * as Validations from "../../../utils/Validations";
import {CommonActions} from "@react-navigation/native";
import AsyncStorage from "@react-native-community/async-storage";


//==================Dismiss keyboard  =======================
const DismissKeyboard = ({children}) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);

//=================== main class declareation======================
export default class OwnerUpdatePassword extends BaseClass {
    //===================== constructor================================
    constructor(props) {
        super(props);
        const {authToken, ownerId} = this.props.route.params;
        this.state = {
            changePassword: "",
            confirmPassword: "",
            authToken: authToken,
            ownerId: ownerId,
        };
    }

    //========================= customer loder ==========================
    _renderCustomLoader = () => {
        const {isLoading} = this.state;

        return (
            <OrientationLoadingOverlay
                visible={isLoading}
                message={STRINGS.LOADING_TEXT}
            />
        );
    };

    //============================ render header ===============================
    _renderHeader() {
        const {navigation} = this.props;
        return (
            <Header
                backgroundColor={COLORS.white_color}
                barStyle={"dark-content"}
                statusBarProps={{
                    translucent: true,
                    backgroundColor: COLORS.transparent,
                }}
                leftComponent={<LeftIcon onPress={() => navigation.goBack()}/>}
                centerComponent={{
                    text: STRINGS.HEADER_CHANGE_PASSWORD, style: {
                        color: COLORS.greyButton_color,
                        fontSize: FONT.TextMedium_2,
                        fontFamily: FONT_FAMILY.PoppinsBold
                    }
                }}
                containerStyle={{
                    borderBottomColor: COLORS.greyButton_color,
                    paddingTop: Platform.OS === 'ios' ? 0 : 25,
                    height: Platform.OS === 'ios' ? 60 : StatusBar.currentHeight + 60,
                }}
            />
        )
    }


    //===================== update password=========================

    updatePassword = () => {
        const {changePassword, confirmPassword, authToken, ownerId} = this.state;
        if (changePassword.trim().length === 0) {
            this.showToastAlert(STRINGS.empty_password);
        } else if (!Validations.validatePassword(changePassword)) {
            this.showToastAlert(STRINGS.enter_valid_password);
        } else if (confirmPassword.trim().length === 0) {
            this.showToastAlert(STRINGS.empty_confirm_password);
        } else if (changePassword !== confirmPassword) {
            this.showToastAlert(STRINGS.password_does_not_match);
        } else {
            OwnerChangePasswordAction({
                id: ownerId,
                accessToken: authToken,
                password: confirmPassword
            }, (data) => this.getChangePasswordData(data))
        }
    };

    getChangePasswordData = (data) => {
        if (data === "Network request failed" || data == "TypeError: Network request failed") {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET)
        } else {
            const {response, message, status} = data;
            if (response !== undefined) {
                if (status === 200) {
                    this.hideDialog();
                    this.showToastSucess(message);
                    this.props.navigation.goBack()
                }
            } else if (status === 111) {
                this.hideDialog();
                if (message.non_field_errors !== undefined) {
                    this.showToastAlert(message.non_field_errors[0]);
                } else {
                    AsyncStorage.removeItem(STRINGS.loginCredentials);
                    navigation.dispatch(
                        CommonActions.reset({
                            index: 0,
                            routes: [
                                {name: 'login'},
                            ],
                        })
                    );
                    this.showToastAlert(message);
                }
            } else if (status === 401) {
                this.hideDialog();
                this.showToastAlert(message);
            } else if (message.non_field_errors !== undefined) {
                this.hideDialog();
                this.showToastAlert(message.non_field_errors[0]);
            }
        }
    };

    //=======================End Change and update password ========================

    render() {
        return (
            <SafeAreaViewContainer>
                {this._renderHeader()}
                <DismissKeyboard>
                    <ScrollContainer>
                        <View style={styles.mainProfileContainer}>
                            <Spacer space={2.5}/>
                            <CustoemTextComponents
                                isSecured={true}
                                placHolder="Enter New Password"
                                returnKeyType={"next"}
                                keyboardType={"default"}
                                ipOnChangeText={(text) =>
                                    this.setState({changePassword: text.trim()})
                                }
                            />
                            <Spacer space={3}/>
                            <CustoemTextComponents
                                isSecured={true}
                                placHolder="Confirm Password"
                                returnKeyType={"next"}
                                keyboardType={"default"}
                                ipOnChangeText={(text) =>
                                    this.setState({confirmPassword: text.trim()})
                                }
                            />
                            <Spacer space={3}/>
                            <TouchableOpacity onPress={() => this.updatePassword()}>
                                <View style={styles.changePasswordStyle}>
                                    <Text style={styles.changePasswordText}>
                                        {STRINGS.UPDATE_PASSWORD}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </ScrollContainer>
                </DismissKeyboard>
            </SafeAreaViewContainer>
        );
    }
}
