import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { StyleSheet, Dimensions } from "react-native";
import { FONT } from "../../../utils/FontSizes";
import COLORS from "../../../utils/Colors";

const window = Dimensions.get("window");

const styles = StyleSheet.create({

    mainProfileContainer: {
        height: hp("100%"),
        alignItems: "center",
        backgroundColor: COLORS.white_color,
      },
    

    changePasswordStyle: {
        marginVertical: 10,
        alignSelf: "center",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: COLORS.app_theme_color,
        height: wp("12%"),
        width: wp("80%"),
        borderRadius: wp("12%") / 2,
        borderColor: COLORS.app_theme_color,
        borderWidth: 2,
      },
      changePasswordText: {
        color: COLORS.white_color,
        fontWeight: "700",
        fontSize:wp('4%')
      },

});
export default styles;
