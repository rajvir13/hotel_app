// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import PropTypes from "prop-types";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import {Spacer} from "../../../../customComponents/Spacer";
import SleepSVG from "../../../../../assets/images/HotelOwnerHome/SleepSVG";
import SpeakerSVG from "../../../../../assets/images/HotelOwnerHome/SpeakerSVG";
import HotelSVG from "../../../../../assets/images/HotelOwnerHome/HotelSVG";
import LikeSVG from "../../../../../assets/images/HotelOwnerHome/LikeSVG";
import SubscriptionSVG from "../../../../../assets/images/HotelOwnerHome/SubscriptionSVG";
import HelpSVG from "../../../../../assets/images/HotelOwnerHome/HelpSVG";
import BookingSVG from '../../../../../assets/images/Drawer/OwnerBookingSVG';
import CommissionSVG from '../../../../../assets/images/Drawer/OwnerCommisionSVG';
import {styles} from './style'

const RenderView = ({onPress, color, labelText, badge, badgeText, badgeColor, svg}) => {
    return (
        <TouchableOpacity onPress={onPress}>
            <View style={[styles.container, {backgroundColor: color}]}>
                <Spacer row={2.5}/>
                <View style={{justifyContent: 'center'}}>
                    {(svg === "sleep") ? <SleepSVG/> :
                        (svg === "speaker") ? <SpeakerSVG/> :
                            (svg === "hotel") ? <HotelSVG/> :
                                (svg === "rating") ? <LikeSVG/> :
                                    (svg === "subscription") ? <SubscriptionSVG/> :
                                        (svg === "help") ? <HelpSVG/> :
                                            (svg === 'booking') ? <BookingSVG/> :
                                                (svg === 'commission' && <CommissionSVG/>)}
                </View>
                <Spacer row={2.5}/>
                <View style={styles.separatorStyle}/>
                <Spacer row={2.5}/>
                <View style={{justifyContent: 'center'}}>
                    <Text style={styles.labelTextStyle}>{labelText}</Text>
                </View>
                {badge && badgeText!==undefined && badgeText!==null ?
                <View style={[styles.badgeView, {backgroundColor: badgeColor}]}>
                    <Text numberOfLines={1}
                          style={styles.badgeText}>{badgeText}
                    </Text>

                </View>:
                console.log("")}
            </View>
        </TouchableOpacity>
    )
};

RenderView.defaultProps = {
    badge: false,
    svg: 'sleep'
};

RenderView.PropTypes = {
    badge: PropTypes.boolean,
    svg: PropTypes.string
};

export default RenderView
