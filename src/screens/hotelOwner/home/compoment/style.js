// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React from 'react';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen'
import {StyleSheet} from 'react-native';

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import COLORS from "../../../../utils/Colors";
import {FONT} from "../../../../utils/FontSizes";
import {FONT_FAMILY} from "../../../../utils/Font";

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderRadius: wp(5) / 2,
        width: wp(90),
    },
    separatorStyle: {
        height: wp(20),
        width: wp(0.3),
        backgroundColor: COLORS.white_color
    },
    labelTextStyle:{
        width: wp(45),
        color: COLORS.white_color,
        fontSize: FONT.TextMedium,
        fontFamily: FONT_FAMILY.Montserrat,
        fontWeight: "600"
    },
    badgeView:{
        justifyContent: 'center',
        alignSelf: 'center',
        width: wp(10),
        height: wp(10),
        borderRadius: wp(10) / 2
    },
    badgeText:{
        width: wp(10),
        textAlign: 'center',
        color: COLORS.white_color,
        fontSize: FONT.TextSmall,
        fontWeight: "600",
        fontFamily: FONT_FAMILY.Montserrat
    }
});

export {styles}
