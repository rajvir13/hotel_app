// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import React from 'react';
import {
    FlatList,
    Platform,
    StatusBar,
    View
} from 'react-native';
import AsyncStorage from "@react-native-community/async-storage";
import {Header} from "react-native-elements";
import * as _ from 'lodash';
import {CommonActions} from "@react-navigation/native";
// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import BaseClass from "../../../utils/BaseClass";
import {SafeAreaViewContainer, MainContainer} from "../../../utils/BaseStyle";
import {FONT} from "../../../utils/FontSizes";

import COLORS from "../../../utils/Colors";
import STRINGS from "../../../utils/Strings";
import {LeftIcon, OwnerRightIcon,} from "../../../customComponents/icons";
import RenderView from "./compoment/renderView";
import {Spacer} from "../../../customComponents/Spacer";
import {HotelHomeDetailAction} from "../../../redux/actions/GetHotelHomeDetails";
import {GetHotelDetailAction, GetHotelRoomDetailAction} from "../../../redux/actions/GetHotelDetail";
import {FONT_FAMILY} from "../../../utils/Font";


export default class HotelHomeScreen extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            accessToken: '',
            renderList: [{
                "svgText": 'sleep',
                "text": "Total Rooms",
                "baseColor": '#FF9900',
                "badgeColor": '#FFC46C',
                "badgeText": undefined,
                "isBadge": true
            }, {
                "svgText": 'speaker',
                "text": "Promotions",
                "baseColor": '#78C492',
                "badgeColor": '#8BD7A5',
                "badgeText": undefined,
                "isBadge": true
            }, {
                "svgText": 'hotel',
                "text": "Hotel Details",
                "baseColor": '#4582E5',
                "badgeColor": '',
                "badgeText": '15',
                "isBadge": false
            }, {
                "svgText": 'rating',
                "text": "Rating Avg",
                "baseColor": '#E7BA1B',
                "badgeColor": '#FACB24',
                "badgeText": undefined,
                "isBadge": true
            }, {
                "svgText": 'subscription',
                "text": "Subscription",
                "baseColor": '#C64949',
                "badgeColor": '',
                "badgeText": '15',
                "isBadge": false
            }, {
                "svgText": 'help',
                "text": "Help and Support",
                "baseColor": '#AC6DC2',
                "badgeColor": '',
                "badgeText": '15',
                "isBadge": false
            }, {
                "svgText": 'booking',
                "text": "Bookings",
                "baseColor": '#FA5A64',
                "badgeColor": '',
                "badgeText": '15',
                "isBadge": false
            }, {
                "svgText": 'commission',
                "text": "Commission",
                "baseColor": '#4BBF9F',
                "badgeColor": '',
                "badgeText": '15',
                "isBadge": false
            },]
        }
    }


    componentDidMount() {
        const {navigation} = this.props;
        this._unsubscribe = navigation.addListener('focus', () => {
            this.onFocusFunction()
        });
        // this.getHotelDetails();
    }


    getHotelDetails = () => {
        AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
            if (result !== undefined && result !== null) {
                let loginData = JSON.parse(result);
                let hotelId = '';
                if (loginData.hotel_details !== undefined) {
                    hotelId = loginData.hotel_details.id
                } else {
                    hotelId = loginData.id
                }
                this.setState({
                    authToken: loginData.access_token,
                    hotel_id: hotelId
                });

                HotelHomeDetailAction({
                    id: hotelId,
                    accessToken: loginData.access_token
                }, (data) => this.getHotelData(data));
                // this.showDialog();
            }
        });
    };

    onFocusFunction = () => {
        // this.componentDidMount()
        this.getHotelDetails();
    };

    componentWillUnmount() {
        this._unsubscribe();
    }

    getHotelData = (data) => {
        console.log("home list is",data)

        const {navigation} = this.props;
        const {renderList} = this.state;
        if (data === "Network request failed" || data == "TypeError: Network request failed") {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET)
        } else {
            const {response, message, status} = data;
            if (response !== undefined) {
                if (status === 200) {
                    this.hideDialog();
                    // let data = response;
                    _.map(renderList, (item, index) => {
                        if (index === 0) {
                            item.badgeText = data.response[0].room_count;
                            return item
                        } 
                        else if(index === 1)
                        {
                            item.badgeText = data.response[0].promotion_count;
                            return item

                        }
                        else if(index === 3)
                        {
                            let rating;
                            rating=data.response[0].avg_rating.rating__avg;
                            item.badgeText = rating.toFixed(1);
                            return item

                        }
                        else {
                            return item
                        }
                    });
                    this.setState({
                        renderList
                    })
                }
            } 
            else if (status === 111) {
                this.hideDialog();
                if (message.non_field_errors !== undefined) {
                    // if (message.non_field_errors[0] === "User with this social_id does not exist")
                } else {
                    AsyncStorage.removeItem(STRINGS.loginCredentials);
                    navigation.dispatch(
                        CommonActions.reset({
                            index: 0,
                            routes: [
                                {name: 'login'},
                            ],
                        })
                    );
                    this.showToastAlert(message);
                }
            } else if (status === 401) {
                this.hideDialog();
                this.showToastAlert(message);
            } else if (message.non_field_errors !== undefined) {
                this.hideDialog();
                this.showToastAlert(message.non_field_errors[0]);
            }
        }
    };

    onBarPress = (index) => {
        const {navigation} = this.props;
        switch (index) {
            case 0:
                navigation.navigate('roomList',{comesFrom:"homeScreen"});
                break;
            case 1:
                navigation.navigate('NotificationAndPramotion',{comesFrom:"homeScreen"});
                break;
            case 2:
                navigation.navigate("hotelDetail",{comesFrom:"homeScreen"});
                break;
            case 3:
                navigation.navigate("ratingAndFeedBack",{comesFrom:"homeScreen"});
                break;
            case 4:
                navigation.navigate("ownerSubscription",{comesFrom:"homeScreen"});
                break;
            case 5:
                navigation.navigate("Help",{comesFrom:"homeScreen"});
                break;
            case 6:
                navigation.navigate('ownerBooking',{comesFrom:"homeScreen"});
                break;
            case 7:
                navigation.navigate('ownerCommission',{comesFrom:"homeScreen"});
                break;

            default:
                this.showToastAlert(STRINGS.COMING_SOON);
                break;

        }
    };

    // =============================================================================================
    // Render methods for Header
    // =============================================================================================

    _renderHeader() {
        const {navigation} = this.props;
        return (
            <Header
                backgroundColor={COLORS.white_color}
                barStyle={"dark-content"}
                statusBarProps={{
                    translucent: true,
                    backgroundColor: COLORS.transparent,
                }}
                leftComponent={(<LeftIcon drawer onPress={() => navigation.openDrawer()}/>)}
                // rightComponent={(<OwnerRightIcon onPress={() => this.logoutUser()}/>)}
                centerComponent={{
                    text: STRINGS.home_header_title, style: {
                        color: COLORS.greyButton_color,
                        fontSize: FONT.TextMedium_2,
                        fontFamily: FONT_FAMILY.PoppinsBold
                    }
                }}
                containerStyle={{
                    borderBottomColor: COLORS.greyButton_color,
                    paddingTop: Platform.OS === 'ios' ? 0 : 25,
                    height: Platform.OS === 'ios' ? 60 : StatusBar.currentHeight + 60,
                }}
            />
        )
    }

    render() {
        return (
            <SafeAreaViewContainer>
                {this._renderHeader()}
                <MainContainer>
                    <Spacer space={4}/>
                    <View style={{justifyContent: "center", alignItems: 'center', flex: 1}}>
                        <FlatList
                            data={this.state.renderList}
                            keyExtractor={(item, index) => index}
                            extraData={this.state}
                            renderItem={({item, index}) =>
                                <>
                                    <RenderView
                                        color={item.baseColor}
                                        labelText={item.text}
                                        badge={item.isBadge}
                                        badgeText={item.badgeText}
                                        badgeColor={item.badgeColor}
                                        svg={item.svgText}
                                        onPress={() => this.onBarPress(index)}
                                    />
                                    <Spacer space={2}/>
                                </>

                            }
                        />
                    </View>
                </MainContainer>
            </SafeAreaViewContainer>
        )
    }
}
