import React from "react";
import {
    View,
    FlatList,
    Text,
    Image,
    Platform,
    StatusBar,
    Dimensions,
    Button,
    TouchableOpacity,
    ScrollView,
    TextInput,
    TouchableWithoutFeedback,
    ImageBackground,
    TouchableHighlight,
    Keyboard,
    AppRegistry,
    Alert,
} from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {Rating, AirbnbRating, Header, Input} from "react-native-elements";
import {CommonActions} from "@react-navigation/native";
import BaseClass from "../../../../utils/BaseClass";
import {
    ShadowViewContainer,
    SafeAreaViewContainer,
    MainContainer,
    ScrollContainer,
} from "../../../../utils/BaseStyle";
import {Spacer} from "../../../../customComponents/Spacer";
import COLORS from "../../../../utils/Colors";
import {
    LeftIcon,
    RightIcon,
    SecondRightIcon,
} from "../../../../customComponents/icons";
import STRINGS from "../../../../utils/Strings";
import {FONT} from "../../../../utils/FontSizes";
import {FONT_FAMILY} from "../../../../utils/Font";
import {LogoutAction} from "../../../../redux/actions/LogoutAction";
import {ICONS} from "../../../../utils/ImagePaths";
import OrientationLoadingOverlay from "../../../../customComponents/Loader";
import styles from "./styles";
import {Dropdown} from "react-native-material-dropdown";
import ImagePicker from "react-native-image-crop-picker";
import * as _ from "lodash";
import NumericInput from "react-native-numeric-input";
import SectionedMultiSelect from "react-native-sectioned-multi-select";

//==================Dismiss keyboard  =======================
const DismissKeyboard = ({children}) => (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        {children}
    </TouchableWithoutFeedback>
);

export default class UpdateCustomerRoomDetail extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            HotelId: undefined,
            accessToken: undefined,
            value1: 1,
            ViewArray: [],
            textInput: [],
            Disable_Button: false,
            dataRoomType: [],
            items: [
                {
                    name: "Fruits",
                    id: 0,
                    // these are the children or 'sub items'
                    children: [
                        {
                            name: "Apple",
                            id: 10,
                        },
                        {
                            name: "Strawberry",
                            id: 17,
                        },
                        {
                            name: "Pineapple",
                            id: 13,
                        },
                        {
                            name: "Banana",
                            id: 14,
                        },
                        {
                            name: "Watermelon",
                            id: 15,
                        },
                        {
                            name: "Kiwi fruit",
                            id: 16,
                        },
                    ],
                },
            ],
            selectedItems: [],
            isNewView: false,
            isAddShow: true,
            imagesList: [],
            authToken: undefined,

            data: [
                {
                    RoomType: "",
                    RoomImages: [],
                    Regular_price: undefined,
                    Discount_Price: "Discount price",
                    IsDiscountApplied: false,
                    Numberofguest: undefined,
                    AddComplimentery_service: [],
                    isMaxPicComplete: false,
                },
            ],
        };
    }

    //======================== add image =======================
    AddIcon = (index) => {
        return (
            this.state.isAddShow && (
                <View style={styles.addIconOuterView}>
                    <TouchableWithoutFeedback onPress={() => this.AddImageAction(index)}>
                        <View style={styles.imgOuterView}>
                            <TouchableWithoutFeedback
                                onPress={() => this.AddImageAction(index)}
                            >
                                <Image
                                    style={styles.hotelImage}
                                    source={ICONS.ADD_IMAGE_ICON}
                                />
                            </TouchableWithoutFeedback>
                        </View>
                    </TouchableWithoutFeedback>
                </View>
            )
        );
    };

//=====================item separator=====================
    _itemSeparator = () => <View style={{width: wp("5%")}}/>;

    //============================ render header ===============================
    _renderHeader() {
        const {navigation} = this.props;
        return (
            <Header
                backgroundColor={COLORS.white_color}
                statusBarProps={{
                    translucent: true,
                    backgroundColor: COLORS.black_color,
                }}
                leftComponent={<LeftIcon onPress={() => navigation.goBack()}/>}
                centerComponent={{
                    text: STRINGS.HEADER_ROOM_DETAIL,
                    style: {
                        color: COLORS.greyButton_color,
                        fontSize: FONT.TextMedium_2,
                        fontFamily: FONT_FAMILY.PoppinsBold
                    },
                }}
                containerStyle={{
                    borderBottomColor: COLORS.greyButton_color,
                    paddingTop: Platform.OS === "ios" ? 0 : 25,
                    height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
                }}
            />
        );
    }

    //============================ Add Image Button Action=====================================
    AddImageAction(index) {
        const {data} = this.state;

        const {imagesList} = this.state;
        ImagePicker.openPicker({
            compressImageQuality: 0.2,
            includeBase64: true,
            multiple: true,
            maxFiles: 10,
            mediaType: "photo",
        }).then((response) => {
            let tempArray = [];
            response.forEach((item) => {
                let image = {
                    completeData: item,
                    uri: item.path,
                };

                tempArray.push(image);
            });

            let count = data[index].RoomImages.length + response.length;

            if (count <= 10) {
                _.map(data, (item, index1) => {
                    if (index === index1) {
                        // item.RoomImages = tempArray;
                        //new code
                        for (let i = 0; i < tempArray.length; i++) {
                            let image = {
                                completeData: tempArray[i],
                                uri: tempArray[i].path,
                            };


                            item.RoomImages.push(image);
                        }
                        //end
                    }

                    this.setState({
                        refresh: !this.state.refresh,
                    });
                });

            } else {
                this.showToastAlert(STRINGS.ONLY_TEN_IMAGES);

                data[index].isMaxPicComplete = true;
                return;
                //alert show and remove image icon from that index
            }
        });
    }

    _renderChooseImage = (item, index) => {

        return (
            <FlatList
                style={styles.flatListStyle}
                data={item.RoomImages}

                renderItem={(item1, index1) =>
                    this._renderImageList(item1, index1, index)
                }
                horizontal={true}
                showsHorizontalScrollIndicator={true}
                keyExtractor={(item, index) => index}

                ItemSeparatorComponent={this._itemSeparator}
                extraData={this.state.refresh}
                keyExtractor={(item, index) => index}
                selected={this.state.selected}
                nestedScrollEnabled={true}
                contentContainerStyle={{flexGrow: 1, justifyContent: 'center'}}

            />
        );
    };

    _renderImageList = (item, index, outerIndex) => {
        //  console.warn("uri is=============",item.item.completeData.uri)
        return (
            <ImageBackground
                style={styles.imageStyle}
                source={{uri: item !== undefined ? item.item.completeData.uri : ""}}
                imageStyle={styles.crossIconOuterView}
            >
                <TouchableWithoutFeedback
                    onPress={() => this.CrossAction(item, item.index, outerIndex)}
                >
                    <Image style={styles.crossIconStyle} source={ICONS.CROSS_ICON}/>
                </TouchableWithoutFeedback>
            </ImageBackground>
        )
    };

    //========================= Remove  Image from list  Action==========================

    CrossAction(i, index, parentIndex) {

        //remove value from imagelist

        const {data} = this.state;
        let tempArray = [];
        data[parentIndex].RoomImages.splice(index, 1);
        // tempArray = imagesList;
        // this.setState({ imagesList: tempArray });
        this.setState({
            refresh: !this.state.refresh,
        });
        if (data[parentIndex].RoomImages.length !== 10)
            this.setState({isAddShow: true});
    }

    //=======================end===================================
    //=================== room type handling=================
    onChangeDropdown = (value, id, index) => {

        const {data} = this.state;
        _.map(data, (item, index1) => {
            if (index === index1) {
                item.RoomType = id;
            }
        });
        this.setState({data});
    };

    //================================ discount price=========================
    discountAmount = (index) => {

        const {data} = this.state;

        if (data[index].IsDiscountApplied === false) {
            _.map(data, (item, index1) => {
                if (index === index1) {
                    item.Discount_Price =
                        parseInt(item.Regular_price) -
                        (parseInt(item.Regular_price) * 20) / 100;
                }
                data[index].IsDiscountApplied = true;
                this.setState({data: data});
            });
        } else alert("Discount already applied for this room");
    };

    //==================Number of guest====================

    // ****************** on selected Item  *********************

    onSelectedItemsChange = (index, selectedItems) => {
        const {data} = this.state;

        _.map(data, (item, index1) => {

            if (index === index1) {
                item.AddComplimentery_service = selectedItems;
            }
        });
        this.setState({data});
    };

    _renderUpdateAddMoreBtn = () => {
        return (
            <View>
                <Spacer space={3}/>
                <TouchableOpacity onPress={() => this.addMoreRooms()}>
                    <View style={styles.changePasswordStyle}>
                        <Text style={styles.changePasswordText}>
                            {STRINGS.ADD_MORE_ROOMS}
                        </Text>
                    </View>
                </TouchableOpacity>
                <Spacer space={2}/>
                <TouchableOpacity onPress={() => this.updatePassword()}>
                    <View style={styles.buttonStyle}>
                        <Text style={styles.buttonText}>{STRINGS.UPDATE_BTN}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        );
    };

    //======================== Add more rooms =============================

    addMoreRooms = () => {
        var newlyCreatedData = {
            RoomType: undefined,
            RoomImages: [],
            Regular_price: undefined,
            Discount_Price: undefined,
            IsDiscountApplied: false,
            Numberofguest: undefined,
            AddComplimentery_service: [],
        };

        const {data} = this.state;
        data.push(newlyCreatedData);

        this.setState({data: data});
    };

    //*******************************Discount Amount ***************** */

    onChangeText = (text, index) => {
        const {data} = this.state;
        _.map(data, (item, index1) => {
            if (index === index1) {
                item.Regular_price = text;
            }
        });
    };

    //=============================== flatlist discount price action==================

    discountAmount = (index) => {

        const {data} = this.state;

        if (data[index].IsDiscountApplied === false) {
            _.map(data, (item, index1) => {
                if (index === index1) {
                    item.Discount_Price =
                        parseInt(item.Regular_price) -
                        (parseInt(item.Regular_price) * 20) / 100;
                }
                data[index].IsDiscountApplied = true;
                this.setState({data: data});
            });
        } else alert("Discount already applied for this room");
    };

    // =================== no of guest action =======================
    numberOfGuest = (num, index) => {
        const {data} = this.state;
        _.map(data, (item, index1) => {
            if (index === index1) {
                item.Numberofguest = num;
            }
        });
        this.setState({data});
    };


    //*******************************remove View ***************** */
    closeRoomView = (closeWindowIndex) => {
        const list = this.state.data;

        list.splice(closeWindowIndex, 1);
        this.setState({data: list});
    };

    //================== hotel image flat list item=====================

    _renderHotelList = (item, index) => {

        let tempRoomType = [
            {
                value: "Single",
            },
            {
                value: "Double",
            },
            {
                value: "Triple",
            },
        ];

        return (
            <View>
                <View style={styles.roomTypeOuterView}>
                    {index !== 0 && (
                        <TouchableOpacity
                            onPress={() => this.closeRoomView(index)}
                            hitSlop={{
                                top: 20,
                                bottom: 20,
                                left: 50,
                                right: 50,
                            }}
                        >
                            <Image
                                source={ICONS.CLOSE_WINDOW}
                                style={{
                                    position: "absolute",
                                    right: 5,
                                    top: -wp('4%'),
                                    height: 30,
                                    width: 30,
                                }}
                            />
                        </TouchableOpacity>
                    )}
                    <View style={styles.roomTypeStyle}>
                        <Dropdown
                            containerStyle={{width: wp("80%")}}
                            label="Room Type"
                            data={tempRoomType}
                            onChangeText={(value, id) =>
                                this.onChangeDropdown(value, id, index)
                            }
                        />
                    </View>
                </View>

                <View style={styles.hotelImageView}>
                    <View style={{flexDirection: "row"}}>
                        {this._renderChooseImage(item, index)}
                        {this.AddIcon(index)}
                    </View>
                </View>

                <View style={styles.hotelView}>
                    <Spacer space={3}/>
                    <TextInput
                        style={styles.RegularPriceTextStyle}
                        placeholder=" Regular Price"
                        placeholderTextColor="gray"
                        // value="120"
                        keyboardType="numeric"
                        onChangeText={(text) => this.onChangeText(text, index)}
                        keyboardType="numeric"
                    ></TextInput>
                    <Spacer space={3}/>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: "row",
                            justifyContent: "space-around",
                        }}
                    >
                        <Text style={styles.TextComponentStyle}>

                            {item.Discount_Price}
                        </Text>

                        <TouchableOpacity>
                            <Text
                                style={styles.TextDiscountStyle}
                                onPress={() => this.discountAmount(index)}
                            >
                                20% discount
                            </Text>
                        </TouchableOpacity>
                    </View>

                    <Spacer space={3}/>
                    <View
                        style={{
                            flex: 1,
                            flexDirection: "row",
                            justifyContent: "space-around",
                        }}
                    >
                        <Text style={styles.TextComponentStyle}>Number of guest</Text>
                        <NumericInput
                            type="up-down"
                            totalHeight={wp("11%")}
                            totalWidth={wp("30%")}
                            value="3"
                            minValue={0}
                            maxValue={10}
                            value={item.Numberofguest}
                            onChange={(value) => this.numberOfGuest(value, index)}
                            rounded
                        />
                    </View>
                    <Spacer space={3}/>
                    <SectionedMultiSelect
                        items={this.state.items}
                        uniqueKey="id"
                        subKey="children"
                        selectText="Complimentary Services"
                        showDropDowns={true}
                        readOnlyHeadings={true}
                        onSelectedItemsChange={this.onSelectedItemsChange.bind(this, index)}
                        selectedItems={item.AddComplimentery_service}
                    />
                    <Spacer space={2}/>
                </View>
            </View>
        );
    };

    //===================== render method ==========================
    render() {
        const {items, data} = this.state;

        return (
            <SafeAreaViewContainer>
                {this._renderHeader()}
                <DismissKeyboard>
                    <ScrollView>
                        <View style={styles.MainContainer}>
                            <FlatList
                                extraData={this.state}
                                contentContainerStyle={{paddingBottom: 30}}
                                data={data}
                                keyExtractor={(item) => item.key}
                                renderItem={({item, index}) =>
                                    this._renderHotelList(item, index)
                                }
                            />

                            {this._renderUpdateAddMoreBtn()}
                        </View>
                    </ScrollView>
                </DismissKeyboard>
            </SafeAreaViewContainer>
        );
    }
}
