import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { StyleSheet, Dimensions } from "react-native";
import { FONT } from "../../../../utils/FontSizes";
import COLORS from "../../../../utils/Colors";

const window = Dimensions.get("window");

const styles = StyleSheet.create({
  mainProfileContainer: {
    height: hp("100%"),
    alignItems: "center",
    backgroundColor: COLORS.white_color,
  },

  MainContainer: {
    flex: 1,
    backgroundColor: COLORS.white_color,
    width: wp("100%"),
  },

  roomTypeStyle: {
    alignSelf: "center",
    backgroundColor: COLORS.white_color,
    justifyContent: "center",
    borderWidth: 1,
    borderRadius: wp("2%"),
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    height: wp("10%"),
    paddingLeft: wp("2%"),
    paddingRight: wp("2%"),
  },
  textViewStyle: {
    width: wp("90%"),
    height: wp("10%"),
    backgroundColor: COLORS.white_color,
    borderWidth: 1,
    borderRadius: wp("2%"),
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: "center",
  },
  input: {
    paddingVertical: wp("0.5%"),
    marginLeft: wp("2%"),
  },

  imgOuterView: {
    height: wp("20%"),
    width: wp("20%"),
    borderRadius: wp("20%") / 2,
    justifyContent: "center",
    backgroundColor: COLORS.backGround_color,
    marginRight: wp("3%"),
  },
  imageStyle: {
    height: wp("20%"),
    width: wp("20%"),
    borderRadius: wp("20%") / 2,

  },

  buttonStyle: {
    marginVertical: 10,
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: COLORS.app_theme_color,
    height: wp("12%"),
    width: wp("80%"),
    borderRadius: wp("12%") / 2,
  },

  changePasswordStyle: {
    marginVertical: 10,
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: COLORS.white_color,
    height: wp("12%"),
    width: wp("80%"),
    borderRadius: wp("12%") / 2,
    borderColor: COLORS.app_theme_color,
    borderWidth: 2,
  },

  changePasswordText: {
    color: COLORS.app_theme_color,
    fontWeight: "700",
  },

  buttonText: {
    fontSize: 16,
    fontStyle: "normal",
    fontWeight: "500",
    lineHeight: 20,
    display: "flex",
    color: "#FFFFFF",
    textAlign: "center",
  },

  hotelAddressTextViewStyle: {
    width: wp("90%"),
    height: wp("11%"),
    backgroundColor: COLORS.white_color,
    borderWidth: 1,
    borderRadius: wp("2%"),
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    flexDirection: "row",
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: "center",
  },
  hotelAddressTextStyle: {
    width: wp("75%"),
    height: wp("11%"),
    marginLeft: wp("7%"),
  },

  addIconOuterView: {
    justifyContent: "center",
    alignSelf: "flex-end",
  },

  hotelImage: {
    height: wp("10%"),
    width: wp("10%"),
    alignSelf: "center",
  },
  flatListStyle: {
    width: wp("60%"),
  },

  crossIconOuterView: {
    borderRadius: wp("20%") / 2,
    resizeMode: "cover",
  },
  crossIconStyle: {
    height: 15,
    width: 15,
    alignSelf: "flex-end",
    marginLeft: 15,
    marginBottom: 15,
  },

  hotelImageView: {
    height: wp("25%"),
    width: wp("100%"),
    backgroundColor: COLORS.white_color,
    paddingVertical: wp("2%"),
  },
  hotelView: {
    flex: 1,

    width: wp("90%"),
    alignSelf: "center",
    marginTop: wp("5%"),
    backgroundColor: COLORS.white_color,
    borderWidth: 1,
    borderRadius: wp("2%"),
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },

    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: "center",
  },

  RegularPriceTextStyle: {
    alignSelf: "flex-start",
    fontSize: wp("4%"),
    marginRight: wp("5%"),
    marginLeft: wp("5%"),
    width: wp("80%"),
    // alignSelf: "center",
    backgroundColor: COLORS.white_color,
    justifyContent: "center",
    borderWidth: 1,
    borderRadius: wp("2%"),
    borderColor: "#ddd",
    borderBottomWidth: 0,
    shadowColor: "#000",
    // shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    // justifyContent: "center",
    height: wp("10%"),
    paddingLeft: wp("2%"),
    paddingRight: wp("2%"),
  },

  roomTypeOuterView: {
    backgroundColor: COLORS.backGround_color,
    height: wp("25%"),
    justifyContent: "center",
  },
  TextComponentStyle: {
    padding: 8,
    width: wp("50%"),
    fontSize: wp("4%"),
    borderWidth: 1,
    borderRadius: wp("2%"),
    borderColor: "#ddd",
    borderBottomWidth: 1,
    shadowColor: "#000",
    shadowOffset: {
      width: 1,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    justifyContent: "center",
    height: wp("10%"),

  },
  TextDiscountStyle: {
    borderRadius: 8,
    borderWidth: 1,
    borderColor: "red",
    color: "red",
    padding: 8,
    fontSize: wp("4%"),
    textAlign: "center",
    height: wp("10%"),
    textShadowColor: "rgba(0, 0, 0, 0.75)",
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
    marginRight: wp("3%"),
  },
  complementryViewStyle:{
    backgroundColor:'white',

  }

});
export default styles;
