/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import React from "react";
import {
  View,
  Text,
  Image,
  Keyboard,
  StatusBar,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Platform,
} from "react-native";
import styles from "./styles";
import BaseClass from "../../../utils/BaseClass";
import STRINGS from "../../../utils/Strings";
import { Header } from "react-native-elements";
import { Spacer } from "../../../customComponents/Spacer";
import COLORS from "../../../utils/Colors";
import { LeftIcon, PaymentHistoryIcon } from "../../../customComponents/icons";
import { FONT } from "../../../utils/FontSizes";
import {
  SafeAreaViewContainer,
  MainContainer,
  ShadowViewContainer,
  ScrollContainer,
} from "../../../utils/BaseStyle";
import CustomTextInput from "../../../customComponents/CustomTextInput";
import { ICONS } from "../../../utils/ImagePaths";

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import AsyncStorage from "@react-native-community/async-storage";
import { connect } from "react-redux";
import OrientationLoadingOverlay from "../../../customComponents/Loader";
import { FONT_FAMILY } from "../../../utils/Font";
import { PlanView, CommisionView } from "./component/PlanCommision";
import {
  BasicSubsciptionAction,
  CommisionSubsciptionAction,
} from "../../../redux/actions/SubscriptionPlanAction";

const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);
export default class SubscriptionPlan extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      accessToken: undefined,
      basicPlan: undefined,
      commision_price: undefined,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginData = JSON.parse(result);
        // const { hotel_details } = loginData;

        if (loginData.hotel_details !== undefined) {
         
          // console.log("if condition");
          // console.log("accessToken", loginData.access_token);
          // console.log("customerLoginId", loginData.hotel_details.owner);
          this.setState({
            accessToken: loginData.access_token,
            customerLoginId: loginData.hotel_details.owner,
          });

          this.subscriptionPlanData(
            loginData.access_token,
            loginData.hotel_details.owner
          );
          this.callCommisionPlanApi(
            loginData.access_token,
            loginData.hotel_details.owner
          );
        } else {
          // console.log("else condition");
          // console.log("accessToken", loginData.access_token);
          // console.log("customerLoginId", loginData.owner.id);

          this.setState({
            accessToken: loginData.access_token,
            customerLoginId: loginData.owner.id,
          });

          this.subscriptionPlanData(loginData.access_token, loginData.owner.id);
          this.callCommisionPlanApi(loginData.access_token, loginData.owner.id);
        }
      }
    });
  }

  subscriptionPlanData = (accessToken, ownerId) => {
    this.showDialog();
    BasicSubsciptionAction(
      {
        accessToken: accessToken,
        ownerId: ownerId,
      },
      (bookedData) => this.subscriptionPlanResponse(bookedData)
    );
  };

  subscriptionPlanResponse = (data) => {
    this.hideDialog();
    const { response, message, status } = data;
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (response !== undefined) {
        if (status === 200) {
          this.setState({
            basicPlan: response,
          });
        } else if (status === 111) {
          this.showToastAlert(message);
        } else if (status === 401) {
          this.showToastAlert(message);
        } else {
          this.showToastAlert(message);
        }
      } else if (message.non_field_errors !== undefined) {
        this.showToastAlert(message.non_field_errors[0]);
      } else {
        this.showToastAlert(message);
      }
      this.hideDialog();
    }
  };

  callCommisionPlanApi = (accessToken, ownerId) => {
    this.showDialog();
    CommisionSubsciptionAction(
      {
        accessToken: accessToken,
        ownerId: ownerId,
      },
      (data) => this.CommisionPlanResponse(data)
    );
  };

  CommisionPlanResponse = (data) => {
    console.log("subscription data is", data);
    this.hideDialog();
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      if (data.status === 200) {
        this.setState({ commision_price: data.commision_price });
      } else {
        this.showToastAlert("something went wrong");
      }
    }
    this.hideDialog()
  };

  //============================= Render Hedaer==========================
  _renderHeader() {
    const { toggleMapView, user_profile } = this.state;
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={<LeftIcon onPress={() => navigation.goBack()} />}
        rightComponent={
          <PaymentHistoryIcon
            onPress={() => navigation.navigate("PaymentHistory")}
          />
        }
        centerComponent={{
          text: STRINGS.SUBSCRIPTION_HEADER,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  //========================custome loader==============================
  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  viewCommisionDetail = () => {
    this.props.navigation.navigate("ownerCommission");
  };

  paymentProcess = (totalAmount, commissionAmt, basicPlanAmount) => {
    this.props.navigation.navigate("AddPayment", {
      amount: totalAmount,
      commisionAmount: commissionAmt,
      basicPlanAmt: basicPlanAmount,
    });
  };
  render() {
    const { basicPlan, commision_price } = this.state;
    let commision;
    let total;
    let basicPlanAmount;
    if (basicPlan !== undefined && commision_price !== undefined) {
      total = parseInt(commision_price) + parseInt(basicPlan[0].plan_price);
      commision = Math.floor(commision_price);
      basicPlanAmount = parseInt(basicPlan[0].plan_price);
    }
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <DismissKeyboard>
          <ScrollContainer>
            {basicPlan !== undefined && commision_price !== undefined ? (
              <MainContainer>
                <PlanView plan_Name={basicPlan} />
                <CommisionView
                  plan_Name={basicPlan}
                  commisionprice={commision}
                  viewDetailClicked={() => this.viewCommisionDetail()}
                />
                <Spacer space={2} />
                <View style={styles.totalPaybleView}>
                  <Text style={styles.mainText}>Total Payable Dues :</Text>
                  <Text style={styles.totalPaybleText}>
                    {"\u20B9"}
                    {basicPlan[0].plan_price} + {"\u20B9"}
                    {commision} = {"\u20B9"}
                    {total} (all inclusive)
                  </Text>
                </View>
                <Spacer space={1} />

                {!basicPlan[0].is_paid ? (
                  <View style={styles.buttonStyleForPaind}>
                    <Text style={styles.buttonText}>{STRINGS.PayText}</Text>
                  </View>
                ) : (
                  <TouchableOpacity
                    onPress={() =>
                      this.paymentProcess(total, commision, basicPlanAmount)
                    }
                  >
                    <View style={styles.buttonStyle}>
                      <Text style={styles.buttonText}>{STRINGS.PayText}</Text>
                    </View>
                  </TouchableOpacity>
                )}

                {this._renderCustomLoader()}
              </MainContainer>
            ) : (
              <View style={{ width: wp("100%"), height: wp("100%") }}>
                <Text
                  style={{
                    paddingVertical: wp(3),
                    textAlign: "center",
                    fontSize: FONT.TextNormal,
                    fontFamily: FONT_FAMILY.MontserratBold,
                  }}
                ></Text>
              </View>
            )}
          </ScrollContainer>
        </DismissKeyboard>
      </SafeAreaViewContainer>
    );
  }
}
