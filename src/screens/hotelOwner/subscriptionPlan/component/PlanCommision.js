import React from "react";
import {View, Text, alert, TouchableOpacity,KeyboardAvoidingView} from "react-native";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";
import styles from "../styles";
import COLORS from "../../../../utils/Colors";

//================== room type and sorting view ===================

//================== search hotel view ===================

const PlanView = (props) => {
    console.warn("props", props.plan_Name[0].commence_date);
    return (
        <View style={styles.viewContainer}>
            <Text style={styles.mainText}>Plan Name</Text>
    <Text style={styles.basicText}>{props.plan_Name[0].plan_name}</Text>

            <View style={{flexDirection: "row"}}>
                <Text style={styles.otherText}>Commencement Date : </Text>
                <Text style={[styles.otherText, {color: "gray"}]}>{props.plan_Name[0].commence_date}</Text>
            </View>

            <View style={{flexDirection: "row"}}>
                <Text style={[styles.otherText, {marginBottom: wp("2%")}]}>
                    Expiration Date :
                </Text>
                <Text
                    style={[
                        styles.otherText,
                        {marginTop: wp("0.3%"), marginBottom: wp("2%"), color: "gray"},
                    ]}
                >
                    {props.plan_Name[0].end_date}
                </Text>
            </View>
            <View style={styles.viewLine}/>
            <View style={{flexDirection: "row"}}>
                <Text style={[styles.basicText, {paddingVertical: wp("3%")}]}>
                    Amount Due :{" "}
                </Text>
                <View style={styles.priceView}>
                    {/*<Text style={styles.priceText}>{props.plan_Price}</Text>*/}
                <Text style={styles.priceText}>{props.plan_Name[0].plan_price}</Text>
                </View>
            </View>
        </View>
    );
};

const comingSoonAlert = (props) => {
    props.comingSoonAlert
};

const CommisionView = (props) => {
  
    return (
        <View style={styles.viewContainer}>
            <View style={{flexDirection: "row"}}>
                <Text style={styles.mainText}>Commission :</Text>
                <Text style={[styles.mainText, {color: COLORS.greyButton_color2}]}>
                {props.plan_Name[0].plan_duration}
                </Text>
            </View>
            <Text style={styles.basicText}>Interval</Text>

            <View style={{flexDirection: "row"}}>
                <Text style={[styles.otherText, {fontWeight: "bold"}]}>From : </Text>
                <Text
                    style={[
                        styles.otherText,
                        {marginTop: wp("0.3%"), marginBottom: wp("2%"), color: "gray"},
                    ]}>
                  {props.plan_Name[0].commence_date}
                </Text>
                <Text style={[styles.otherText, {fontWeight: "bold"}]}>To : </Text>
                <Text
                    style={[
                        styles.otherText,
                        {marginTop: wp("0.3%"), marginBottom: wp("2%"), color: "gray"},
                    ]}>
                    {props.plan_Name[0].end_date}
                </Text>
            </View>

            <View style={{flexDirection: "row", marginTop: wp("2%")}}>
                <TouchableOpacity onPress={() => props.viewDetailClicked()}>

                    <Text
                        style={[
                            styles.otherText,
                            {
                                // color: COLORS.greyButton_color2,
                                textDecorationLine: "underline",
                                color: COLORS.app_theme_color,
                                fontWeight: "bold",
                            },
                        ]}
                    >
                        View Details
                    </Text>
                </TouchableOpacity>
            </View>
            <View style={styles.viewLine}/>
            <View style={{flexDirection: "row"}}>
                <Text style={[styles.basicText, {paddingVertical: wp("3%")}]}>
                    Amount Due :{" "}
                </Text>
                <View style={styles.priceView}>
                    <Text style={styles.priceText}>{props.commisionprice}</Text>
                </View>
            </View>
        </View>
    );
};

export {CommisionView, PlanView};
