import React from 'react';
import styled from 'styled-components';
import COLORS from "../../../utils/Colors";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { StyleSheet } from 'react-native';
import {FONT} from "../../../utils/FontSizes";
import {FONT_FAMILY} from "../../../utils/Font";
const styles = StyleSheet.create({

       viewContainer: {
        alignSelf: 'center',
        width:wp('90%'),
        flex:1,
        backgroundColor:'white',
        marginTop:wp('4%'),
        borderRadius:10,
        shadowColor: "#000000",
        shadowOpacity: 0.2,
        shadowRadius: 2,
        shadowOffset: {
          height: 0.9,
          width: 0
        }
    },
  mainText:{
    marginTop:wp('3%'),
  fontSize:FONT.TextMedium,
  fontWeight:'bold',
  marginLeft:wp('4%'),
  paddingVertical:wp('1%'),
  color:COLORS.app_theme_color,
  fontFamily:FONT_FAMILY.Montserrat

  },
basicText:{

    fontSize:FONT.TextMedium,
    fontWeight:'bold',
    marginLeft:wp('4%'),
    marginBottom:wp('1%'),
    color:COLORS.greyButton_color,
    fontFamily:FONT_FAMILY.Montserrat
},

otherText:{
    fontSize:FONT.TextSmall_2,
    marginLeft:wp('4%'),
    paddingVertical:wp('0.5%'),
    fontWeight:'bold',
    color:COLORS.greyButton_color,
    fontFamily:FONT_FAMILY.Montserrat

},
priceView:{

    marginLeft:wp('2%'),
    marginBottom:wp('2.5%'),
    marginTop:wp('2.5%'),
    justifyContent:'center',
    backgroundColor:COLORS.price_Background_color,
    borderRadius:wp('10%'),
    width:wp('20%'),
    height:wp('8%'),
    shadowColor: "#000000",
    shadowOpacity: 0.2,
    shadowRadius: 2,
    shadowOffset: {
      height: 0.9,
      width: 0
    }
},
priceText:{

    color:COLORS.white_color,
    alignSelf:'center',
    fontWeight:'bold',
    fontFamily:FONT_FAMILY.Montserrat

},
viewLine:{

    width: wp("90%"),
    backgroundColor: "gray",
    height: 1,
    flex: 1,
    marginTop:wp('1%'),
    marginBottom:wp('1%')

},
totalPaybleView:{
    alignSelf: 'center',
    width:wp('90%'),
    flex:1,

},
totalPaybleText:{
    fontSize:FONT.TextMedium,
 marginTop:wp('1%'),

 fontWeight:'500',
 marginLeft:wp('4%'),
 paddingVertical:wp('1%'),
color:COLORS.greyButton_color,
fontFamily:FONT_FAMILY.Montserrat
},
buttonStyle:{
    marginVertical:10,
    alignSelf:"center",
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.app_theme_color,
    height: wp('12%'),
    width: wp('80%'),
    borderRadius: wp('12%') / 2,
},

buttonStyleForPaind:{
    marginVertical:10,
    alignSelf:"center",
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.greyButton_color2,
    height: wp('12%'),
    width: wp('80%'),
    borderRadius: wp('12%') / 2,
},
buttonText:{
    fontSize:FONT.TextMedium,
    fontStyle:"normal",
    fontWeight:"500",
    lineHeight:20,
    display:"flex",
    // fontFamily: 'Montserrat',
    color: '#FFFFFF',
    textAlign: 'center',
    fontFamily:FONT_FAMILY.Montserrat
},


});

export default styles;
