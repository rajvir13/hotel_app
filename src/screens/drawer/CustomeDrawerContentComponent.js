import React from 'react';
import {
    View, Image, Text, TouchableOpacity, Alert
} from "react-native";
import {DrawerContentScrollView, DrawerItem} from "@react-navigation/drawer";
import {connect} from "react-redux";
import {CommonActions} from "@react-navigation/native";
import AsyncStorage from "@react-native-community/async-storage";

import {Spacer} from "../../customComponents/Spacer";
import {styles} from "./styles";
import OwnerHomeComponent from "../../../assets/images/Drawer/OwnerHomeSVG";
import OwnerRatingComponent from "../../../assets/images/Drawer/OwnerRatingSVG";
import OwnerRoomsListingComponent from "../../../assets/images/Drawer/OwnerRoomsListSVG";
import OwnerHelpComponent from "../../../assets/images/Drawer/OwnerHelpSVG";
import OwnerContactComponent from "../../../assets/images/Drawer/OwnerContactSVG";
import OwnerLogoutComponent from "../../../assets/images/Drawer/OwnerLogotSVG";
import BaseClass from "../../utils/BaseClass";
import OrientationLoadingOverlay from '../../customComponents/Loader/index';
import {LogoutAction} from "../../redux/actions/LogoutAction";
import STRINGS from "../../utils/Strings";
import {HotelHomeDetailAction} from "../../redux/actions/GetHotelHomeDetails";
import * as _ from "lodash";
import FastImage from "react-native-fast-image";
import {GetHotelDetailAction} from "../../redux/actions/GetHotelDetail";
import OwnerHotelDetailComponent from "../../../assets/images/Drawer/OwnerHotelDetailSVG";

class CustomDrawerContent extends BaseClass {
    constructor(props) {
        super(props);
        this.setState({
            isLogOut: false,
            authToken: '',
            name: 'Grand Plaza',
            hotelProfile: 'https://travelji.com/wp-content/uploads/Hotel-Tips.jpg',
            ownerId: '',
            address: "",
            phone_number: '',
            ownerName: '',
            ownerLastName: '',
            email: "",
            hotelImages: [],
            hotelID: "",
            lat: "",
            long: ""
        })
    }

    componentDidMount() {
        this.getHotelProfile();
    }

    getHotelProfile = () => {
        AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
            if (result !== undefined && result !== null) {
                let loginData = JSON.parse(result);
                let hotelId = '';
                if (loginData.hotel_details !== undefined) {
                    hotelId = loginData.hotel_details.id
                } else {
                    hotelId = loginData.id
                }
                this.setState({
                    authToken: loginData.access_token,
                    hotel_id: hotelId
                });

                GetHotelDetailAction({
                    id: hotelId,
                    accessToken: loginData.access_token
                }, (data) => this.getHotelData(data));
                // this.showDialog();
            }
        });
    };

    getHotelData = (data) => {
        if (data === "Network request failed" || data == "TypeError: Network request failed") {
            this.hideDialog();
            this.showToastAlert(STRINGS.CHECK_INTERNET)
        } else {
            const {response, message, status} = data;
            if (response !== undefined) {
                if (status === 200) {
                    this.hideDialog();
                    // let data = response;
                    let imagesArr = response[0].hotel_images;
                    let image = 'https://travelji.com/wp-content/uploads/Hotel-Tips.jpg';
                    if (imagesArr.length !== 0) {
                        if (imagesArr[0].image !== null) {
                            image = imagesArr[0].image;
                        }
                    }
                    this.setState({
                        name: response[0].name,
                        hotelProfile: image,
                        ownerId: response[0].owner.id,
                        address: response[0].address,
                        phone_number: response[0].owner.mobile_number,
                        ownerName: response[0].owner.first_name,
                        ownerLastName: response[0].owner.last_name,
                        email: response[0].owner.email,
                        hotelImages: response[0].hotel_images,
                        hotelID: response[0].id,
                        lat: response[0].latitude,
                        long: response[0].longitude
                    })
                }
            } else if (status === 111) {
                this.hideDialog();
                if (message.non_field_errors !== undefined) {
                    // if (message.non_field_errors[0] === "User with this social_id does not exist")
                }
            } else if (status === 401) {
                this.hideDialog();
                this.showToastAlert(message);
            } else if (message.non_field_errors !== undefined) {
                this.hideDialog();
                this.showToastAlert(message.non_field_errors[0]);
            }
        }
    };

    // componentWillReceiveProps(nextProps, nextContext) {
    //     this.getHotelProfile()
    // }

    componentWillReceiveProps(nextProps, nextContext) {
        const {isLogOut} = this.state;
        const {navigation} = this.props;
        const {logoutResponse} = nextProps.logoutState;
        if (logoutResponse !== undefined && isLogOut) {
            if (logoutResponse === "Network request failed") {
                this.hideDialog();
                this.showToastAlert(STRINGS.CHECK_INTERNET)
            } else {
                const {message, status} = logoutResponse;
                if (status === 200) {
                    this.hideDialog();
                    this.setState({
                        isLogOut: !isLogOut
                    });
                    AsyncStorage.removeItem(STRINGS.loginToken);
                    AsyncStorage.removeItem(STRINGS.loginData);
                    AsyncStorage.removeItem(STRINGS.loginCredentials);
                    AsyncStorage.removeItem(STRINGS.fcmToken);
                    navigation.dispatch(
                        CommonActions.reset({
                            index: 0,
                            routes: [
                                {name: 'login'},
                            ],
                        })
                    );
                } else if (status === 111) {
                    this.hideDialog();
                    console.warn(message)
                } else {
                    this.showToastAlert(STRINGS.session_expired);
                    this.hideDialog();
                }
            }
        }
        // this.updateUserData();
        this.getHotelProfile();
    }

    // ----------------------------------------
    // ----------------------------------------
    // METHODS
    // ----------------------------------------

    _goOwnerAddressProfile = () => {
        const {ownerName, ownerLastName, email, phone_number, name, address, hotelImages, hotelID, ownerId, lat, long} = this.state;
        this.props.navigation.navigate("ownerAddressProfile", {
            owner_Name: ownerName,
            owner_Last_Name: ownerLastName,
            owner_email: email,
            owner_mobileNo: phone_number,
            hotelName: name,
            hotelAddr: address,
            hotels_image: hotelImages,
            hotelID: hotelID,
            ownerId: ownerId,
            lat: lat,
            long: long
        });
    };

    logoutUser = () => {
        const {isLogOut, authToken} = this.state;
        Alert.alert(
            'Hotel App',
            STRINGS.EXIT_ALERT,
            [
                {
                    text: 'Cancel', style: 'cancel'
                },
                {
                    text: 'OK', onPress: () => {
                        this.setState({isLogOut: !isLogOut});
                        this.props.logout({accessToken: authToken});
                        this.showDialog()
                    }
                },

            ],
            {cancelable: false});
    };


    // updateUserData() {
    //     const {loginData} = this.state;
    //
    //     AsyncStorage.getItem(STRINGS.loginData).then((result) => {
    //         let userData = JSON.parse(result);
    //         if (loginData !== result) {
    //             if (userData !== undefined && userData !== null) {
    //                 this.setState({
    //                     // loginData: result,
    //                     // name: userData.name,
    //                     // profilePic: userData.profilepic,
    //                     authToken: userData.access_token,
    //                 })
    //
    //             }
    //         }
    //
    //     }).catch((error) => {
    //         console.log(error)
    //     })
    //         .done();
    // }


    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message="Loading.."/>
        )
    };


    render() {
        const {name, hotelProfile} = this.state;
        return (
            <View style={{flex: 1}}>
                <DrawerContentScrollView>
                    <View style={styles.headerStyle}>
                        <FastImage
                            style={styles.imageStyle}
                            source={{uri: hotelProfile}}/>
                        <Spacer row={1}/>
                        <View style={styles.textViewStyle}>
                            <Text numberOfLines={1}
                                  style={styles.textStyle1}>{name}</Text>
                            <TouchableOpacity onPress={() => this._goOwnerAddressProfile()}>
                                <Text style={styles.textStyle2}>Edit Profile </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <Spacer space={8}/>
                    <DrawerItem
                        labelStyle={styles.labelTextStyle}
                        label="Home"
                        icon={() => <OwnerHomeComponent/>}
                        onPress={() => this.props.navigation.navigate('hotelHome')}
                    />
                    <DrawerItem
                        labelStyle={styles.labelTextStyle}
                        label="Room List"
                        icon={() => <OwnerRoomsListingComponent/>}
                        onPress={() => this.props.navigation.navigate('roomList')}
                    />
                    <DrawerItem
                        labelStyle={styles.labelTextStyle}
                        label="Hotel Details"
                        icon={() => <OwnerHotelDetailComponent/>}
                        onPress={() => this.props.navigation.navigate("hotelDetail")}
                    />
                    <DrawerItem
                        labelStyle={styles.labelTextStyle}
                        label="Rating & Feedback"
                        icon={() => <OwnerRatingComponent/>}
                        onPress={() => this.props.navigation.navigate("ratingAndFeedBack", {from: 'Drawer'})}
                    />
                    <DrawerItem
                        labelStyle={styles.labelTextStyle}
                        label="Help"
                        icon={() => <OwnerHelpComponent/>}
                        onPress={() => this.props.navigation.navigate("Help", {from: 'Drawer'})}
                    />
                    <DrawerItem
                        labelStyle={styles.labelTextStyle}
                        label="Contact Us"
                        icon={() => <OwnerContactComponent/>}
                        onPress={() => this.props.navigation.navigate("ContactUs", {from: 'Drawer'})}
                    />
                    <DrawerItem
                        labelStyle={styles.labelTextStyle}
                        label="Logout"
                        icon={() => <OwnerLogoutComponent/>}
                        onPress={() => this.logoutUser()}
                    />
                </DrawerContentScrollView>
                {this._renderCustomLoader()}
            </View>
        )
    }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state) => ({
    logoutState: state.LogoutReducer,
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
    return {
        logout: (payload) => dispatch(LogoutAction(payload)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CustomDrawerContent);

// export default CustomDrawerContent
