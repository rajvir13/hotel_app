import React, { Component } from "react";
import { StyleSheet, Text, View, StatusBar } from "react-native";
import { WebView } from "react-native-webview";
import STRINGS from "../../../utils/Strings";
import { Header } from "react-native-elements";
import COLORS from "../../../utils/Colors";
import { FONT } from "../../../utils/FontSizes";
import { LeftIcon } from "../../../customComponents/icons";
import { FONT_FAMILY } from "../../../utils/Font";
import {
  SafeAreaViewContainer,
  MainContainer,
  ShadowViewContainer,
  ScrollContainer,
} from "../../../utils/BaseStyle";
export default class MyWebComponent extends Component {
  _renderHeader() {
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.backGround_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={<LeftIcon onPress={() => navigation.goBack()} />}
        centerComponent={{
          text: "Term And Conditions",
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }
  render() {
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <WebView
          javaScriptEnabled
          source={{ uri: "https://reactnative.dev/" }}
          domStorageEnabled={true}
          startInLoadingState={true}
          useWebKit={true}
          scalesPageToFit={true}
        ></WebView>
      </SafeAreaViewContainer>
    );
  }
}
