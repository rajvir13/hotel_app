import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({

    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },

    bottom: {
        alignSelf: 'center',
        justifyContent: 'center',
        bottom: -wp('13%'),
        position: 'absolute',
    },
    planNameContainer: {
        flexDirection: "row",
        alignSelf: "center",
        width: wp('85%'),
        paddingVertical: wp('2%'),
        justifyContent: 'center',
        borderBottomColor: 'white',
        borderBottomWidth: 1,
        alignItems: 'center',
    },
    planNameText: {
        fontSize: 25,
        fontStyle: "normal",
        fontWeight: "bold",
        lineHeight: 30,
        display: "flex",
        // fontFamily: 'Montserrat',
        color: '#FFFFFF',
        alignSelf: 'center',
        marginHorizontal: wp(2),
    },
    tickContainer: {
        height: wp('10%'),
        width: wp('10%'),
        borderRadius: wp('10%') / 2,
        paddingVertical: wp(3),
        marginHorizontal: wp(2),
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    circleTick: {
        height: wp('8%'),
        width: wp('8%'),
        borderRadius: wp('8%') / 2,
        paddingVertical: wp(3),
        marginHorizontal: wp(2),
    },
    planDetailText: {
        fontSize: 14,
        fontStyle: "normal",
        fontWeight: "600",
        lineHeight: 17,
        display: "flex",
        // fontFamily: 'Montserrat',
        color: '#FFFFFF',
        textAlign: 'center',

    },
    amountContainer: {
        backgroundColor: '#FFF7F7',
        height: wp('10%'),
        width: wp('18%'),
        borderRadius: wp('10%') / 2,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    planAmountText: {
        fontSize: 16,
        fontStyle: "normal",
        fontWeight: "500",
        lineHeight: 20,
        display: "flex",
        fontFamily: 'Montserrat',
        color: 'black',
        textAlign: 'center',
        padding: 10, alignSelf: "center"
    },

    buttonStyle: {
        //marginVertical: 0,
        alignItems: 'center',
        backgroundColor: 'white',
        borderWidth: 1,
        alignSelf: 'center',
        borderColor: 'transparent',
        height: wp('15%'), width: wp('60%'),
        borderRadius: wp('16%') / 2,
        justifyContent: 'center',
    },
    buttonText: {
        fontSize: 20,
        fontStyle: "normal",
        fontWeight: "600",
        lineHeight: 30,
        display: "flex",
        // fontFamily: 'Montserrat',
        color: '#F44336',
        textAlign: 'center',
    },

});

export default styles;
