/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import React from "react";
import {
    View, Text, Image, FlatList,
    TouchableOpacity, TouchableWithoutFeedback, Platform, StatusBar
} from "react-native";
import styles from "./selectPlanStyle";
import BaseClass from "../../../utils/BaseClass";
import STRINGS from '../../../utils/Strings'
import {Header, Card} from 'react-native-elements';
import {Spacer} from '../../../customComponents/Spacer'
import COLORS from "../../../utils/Colors";
import {LeftIcon} from "../../../customComponents/icons";
import {FONT} from "../../../utils/FontSizes";
import {
    SafeAreaViewContainer, MainContainer, ShadowViewContainer,
    ScrollContainer, BorderViewContainer
} from "../../../utils/BaseStyle";
import {ICONS} from "../../../utils/ImagePaths";
import * as _ from 'lodash';

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
import LinearGradient from 'react-native-linear-gradient';
import {CommonActions} from "@react-navigation/native";
import {FONT_FAMILY} from "../../../utils/Font";


export default class SelectPlan extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            refresh: false,
            planListItems: [
                {name: 'Patrick star', isSelected: 0},
                {name: 'Gallileo', isSelected: -1},
                {name: 'Einsten', isSelected: -1},
            ],
            colorsList: [
                '#CC2A2A',
                '#105EDE',
                '#654321',
            ]
        }
    }

    // ============================METHODS=====================================


    // =============================================================================================
    // Click on Row of FlatList //// separator
    // =============================================================================================

    onPlanSelect(item, index) {
        const {planListItems} = this.state;
        this.setState({
            refresh: !this.state.refresh,
            planListItems: _.map(planListItems, (item1, index1) => {
                if (index1 === index) {
                    item1.isSelected = index
                } else {
                    item1.isSelected = -1
                }
                return item1;
            })
        })
    }

    _itemSeparator = () => (<View style={{width: wp('5%')}}/>);

    onPayWithClick = (index) => {
        const {planListItems} = this.state;
        _.map(planListItems, (item1, index1) => {
            if (index === index1) {
                this.props.navigation.dispatch(
                    CommonActions.reset({
                        index: 0,
                        routes: [
                            {name: 'customerHome'},
                        ],
                    })
                );
            }
        })
    };

    /*
  // =============================================================================================
  // Render method for Header
  // =============================================================================================
  */

    _renderHeader() {
        const {navigation} = this.props;
        return (
            <Header
                backgroundColor={COLORS.backGround_color}
                barStyle={"dark-content"}
                statusBarProps={{
                    translucent: true,
                    backgroundColor: COLORS.transparent,
                }}
                leftComponent={(<LeftIcon onPress={() => navigation.goBack()}/>)}
                centerComponent={{
                    text: STRINGS.sel_plan_header_title, style: {
                        color: COLORS.greyButton_color,
                        fontSize: FONT.TextMedium_2,
                        fontFamily: FONT_FAMILY.PoppinsBold
                    }
                }}
                containerStyle={{
                    borderBottomColor: COLORS.greyButton_color,
                    paddingTop: Platform.OS === 'ios' ? 0 : 25,
                    height: Platform.OS === 'ios' ? 60 : StatusBar.currentHeight + 60,
                }}
            />
        )
    }

    /*
  // =============================================================================================
  // Render method for Listview
  // =============================================================================================
  */

    _renderMyList = ({item, index}) => (
        <TouchableWithoutFeedback onPress={() => this.onPlanSelect(item, index)}>
            {/* <LinearGradient
            colors={['#CB3B3B', '#CC2A2A', '#CB3B3B']}
                style={styles.mainCellContainer}  > */}

            <View style={styles.container}>

                <View style={{
                    backgroundColor: this.state.colorsList[index % this.state.colorsList.length],
                    borderRadius: 15,
                }}>

                    <View style={styles.planNameContainer}>
                        <Text style={styles.planNameText}>
                            {STRINGS.sel_plan_name}
                        </Text>
                        <View style={styles.tickContainer}>
                            {this.TickIcon(item, index)}
                        </View>
                    </View>
                    <Spacer space={2.5}/>
                    <Text style={styles.planDetailText}>
                        {STRINGS.sel_plan_detail}
                    </Text>
                    <Spacer space={2.5}/>
                    <View style={styles.amountContainer}>
                        <Text style={styles.planAmountText}>
                            {STRINGS.sel_plan_amount}
                        </Text>
                    </View>
                    <Spacer space={5}/>
                    <View style={styles.bottom}>
                        <TouchableOpacity onPress={() => this.onPayWithClick(index)}>
                            <ShadowViewContainer style={styles.buttonStyle}>
                                {/*<TouchableOpacity style={{backgroundColor:"orange",*/}
                                {/*    paddingHorizontal: wp(8), paddingVertical:wp(2)}} onPress={() => this.onPayWithClick(index)}>*/}
                                <Text style={styles.buttonText}>{STRINGS.sel_plan_pay_button}</Text>

                            </ShadowViewContainer>
                        </TouchableOpacity>
                    </View>
                </View>
                {/* </LinearGradient> */}
            </View>
        </TouchableWithoutFeedback>
    );

    TickIcon = (item, index) => {
        return ((item.isSelected === index) ? <Image style={styles.circleTick} source={ICONS.RED_TICK_ICON}/> : <View/>
        );
    };


    // =============================================================================================
    // Render method
    // =============================================================================================

    render() {
        const {planListItems} = this.state;
        return (
            <SafeAreaViewContainer>
                {this._renderHeader()}
                <MainContainer backgroundColor='#E5E5E5'>
                    {/* <Spacer space={5} /> */}
                    <FlatList
                        //contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}
                        horizontal
                        style={{padding: wp('5%'), paddingRight: wp('15%')}}
                        data={planListItems}
                        renderItem={this._renderMyList}
                        ItemSeparatorComponent={this._itemSeparator}
                        extraData={this.state.refresh}
                        keyExtractor={(item, index) => index}
                        // selected={selected}
                    />
                    {/* <Spacer space={5} /> */}
                </MainContainer>
            </SafeAreaViewContainer>
        )
    }
}
