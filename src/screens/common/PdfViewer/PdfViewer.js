import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TextInput,
  Modal,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  TouchableWithoutFeedback,
  PermissionsAndroid,
  Keyboard,
  Platform,
  StatusBar,
  Alert,
} from "react-native";
import Pdf from "react-native-pdf";
import {
  SafeAreaViewContainer,
  MainContainer,
  ScrollContainer,
  ShadowViewContainer,
} from "../../../utils/BaseStyle";
import COLORS from "../../../utils/Colors";
import { LeftIcon } from "../../../customComponents/icons";

import { ListItem, CheckBox, Header } from "react-native-elements";
import { FONT_FAMILY } from "../../../utils/Font";
import { FONT } from "../../../utils/FontSizes";

export default class PdfViewer extends Component {
  constructor(props) {
    super(props);
    const { pdfData } = this.props.route.params;
    if (Platform.OS === "android") {
      this.storagePermission();
    }
    this.pdf = null;
    this.state = {
      pdfFileData: pdfData,
      baseFileData: undefined,
      permissionStorage: undefined,
    };
  }

  //=======================Header==========================
  _renderHeader() {
    const { navigation } = this.props;

    // console.warn('from',from)
    return (
      <Header
        backgroundColor={COLORS.backGround_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={<LeftIcon onPress={() => navigation.goBack()} />}
        centerComponent={{
          text: "Policy document",
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  storagePermission = async () => {
    var granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: "Cool Photo App Storage Permission",
        message: "This App needs access to your Storage ",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK",
      }
    );

    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      this.setState({ permissionStorage: granted });
      console.log("You can use the camera");
    } else {
      console.log("Camera permission denied");
    }
  };



  pdfShow = (src) => {
    // const srce = {
    //   uri: "http://samples.leanpub.com/thereactnativebook-sample.pdf",
    //   cache: true,
    // };
    return (
      <View>
        <Pdf
          ref={(pdf) => {
            this.pdf = pdf;
          }}
          source={src}
          onLoadComplete={(numberOfPages, filePath) => {
            console.log(`number of pages: ${numberOfPages}`);
          }}
          style={styles.pdf}
          onPageChanged={(page, numberOfPages) => {
            console.log(`current page: ${page}`);
          }}
          onPressLink={(uri) => {
            console.log(`Link presse: ${uri}`);
          }}
          onError={(error) => {
            console.warn("err is:", error);
          }}
        ></Pdf>
      </View>
    );
  };

  render() {
    const { pdfFileData } = this.state;

    // const source = {uri:'http://samples.leanpub.com/thereactnativebook-sample.pdf',cache:true};

    // if(pdfFileData)

    var source= undefined;
   console.warn("pdf uri is",typeof(pdfFileData))

    if(typeof(pdfFileData) === "string")
    {
       source = { uri: pdfFileData ,cache:true};

    }
    else{

  source = { uri: pdfFileData.uri };
    }

    // const source = { uri: pdfFileData.uri };

    console.warn("source is", pdfFileData);
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}

        <View style={styles.container}>
       {this.pdfShow(source)}
        </View>
      </SafeAreaViewContainer>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
  },
  pdf: {
    flex: 1,
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
  },
});

//Storage permission
