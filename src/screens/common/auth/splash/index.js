import React from "react";
import {
  Image,
  View,
  Text,
  Platform,
  DeviceEventEmitter,
  Alert,
} from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
import { CommonActions } from "@react-navigation/native";

import BaseClass from "../../../../utils/BaseClass";
import { TokenAuthAction } from "../../../../redux/actions/TokenAuthAction";
import STRINGS from "../../../../utils/Strings";
import { ICONS } from "../../../../utils/ImagePaths";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { currentLocation } from "../../../../customComponents/FetchLocation";
import * as DeviceInfo from "react-native-device-info";
import LocationServicesDialogBox from "react-native-android-location-services-dialog-box";
import Geolocation from "@react-native-community/geolocation";
import { connect } from "react-redux";
import { LoginAction } from "../../../../redux/actions/LoginAction";
import { SocialLoginAction } from "../../../../redux/actions/SocialLoginAction";
import firebase, {
  Notification,
  NotificationOpend,
} from "react-native-firebase";
class Splash extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      loginData: {},
    };
  }

  componentDidMount() {
    this.createNotificationListeners();
    const { navigation } = this.props;
    console.disableYellowBox = true;
    TokenAuthAction({}, (Data) => this.tokenAuthResponse(Data));
    this.getCurrentLocation();
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const { navigation } = this.props;
    const { loginResponse } = nextProps.loginState;

    if (loginResponse !== undefined && loginResponse !== null) {
      if (loginResponse === "Network request failed") {
        this.hideDialog();
        this.showToastAlert(STRINGS.CHECK_INTERNET);
      } else {
        const { response, message, status } = loginResponse;
        console.log("mszes is", message);
        if (response !== undefined) {
          if (status === 200) {
            this.hideDialog();
            AsyncStorage.setItem(
              STRINGS.loginToken,
              JSON.stringify(response.access_token)
            );
            AsyncStorage.setItem(STRINGS.loginData, JSON.stringify(response));
            this.handlingLoginResponse(response);
          } else if (status === 111) {
            this.hideDialog();
            this.showToastAlert(message);
          } else if (status === 401) {
            this.hideDialog();
            this.showToastAlert(message);
          } else {
            this.hideDialog();
            this.showToastAlert(message);
          }
        } else if (message.non_field_errors !== undefined) {
          if (status === 111) {
            this.hideDialog();
            this.showToastAlert(message.non_field_errors[0]);
            navigation.dispatch(
              CommonActions.reset({
                index: 0,
                routes: [{ name: "login" }],
              })
            );
          } else {
            this.hideDialog();
            this.showToastAlert(message.non_field_errors[0]);
          }
        } else {
          this.hideDialog();
          this.showToastAlert(message);
        }
        this.hideDialog();
      }
    }
  }

  componentWillUnmount() {
    if (Platform.OS !== "ios") {
      LocationServicesDialogBox.stopListener();
    }
    // this.notificationListener();
    // this.notificationOpenedListener();
    this.createNotificationListeners();
    this.notificationListener();
    this.notificationOpenedListener();
  }

  /** ****************** Notification Methods ********************************** */

  async createNotificationListeners() {
    const channelId = new firebase.notifications.Android.Channel(
      "Default",
      "Default",
      firebase.notifications.Android.Importance.High
    );
    firebase.notifications().android.createChannel(channelId);

    // This listener triggered when notification has been received in foreground
    this.notificationListener = firebase
      .notifications()
      .onNotification((notification) => {
        const { title, body, data } = notification;
        // alert("notification come to me")
        debugger;
        if (Platform.OS === "android") {          
          const notificationDisplay = new firebase.notifications.Notification()
            .setNotificationId(notification.notificationId)
            .setTitle(title)
            .setBody(notification.body)
            .setData(notification.data)
            .setSubtitle("notification")
            .setSound("default");
          notificationDisplay.android
            .setChannelId("Default")
            .android.setSmallIcon("ic_launcher")
            .android.setAutoCancel(true)
            .android.setPriority(firebase.notifications.Android.Priority.High);
          firebase.notifications().displayNotification(notificationDisplay);

          // firebase.notifications().removeDeliveredNotification(notificationDisplay.notificationId);


        } else {
          const localNotification = new firebase.notifications.Notification()
            .setNotificationId(notification.notificationId)
            .setTitle(notification.title)
            .setSubtitle(notification.subtitle)
            .setBody(notification.body)
            .setData(notification.data)
            .ios.setBadge(1);

          firebase
            .notifications()
            .displayNotification(localNotification)
            .catch((err) => console.error(err));

            // firebase.notifications().removeDeliveredNotification(notification.notificationId);

        }          


        if (data.direct_screen === 3 || data.direct_screen === "3") {
          this.displayNotification(title, body, data);
        }
      });

    // This listener triggered when app is in backgound and we click, tapped and opened notifiaction
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened((notificationOpen) => {
        const { title, body, data } = notificationOpen.notification;
        this.displayNotification(title, body, data);
      });

    // This listener triggered when app is closed and we click,tapped and opened notification
    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      const { title, body, data } = notificationOpen.notification;
      this.displayNotification(title, body, data);
    }
    this.messageListener = firebase.messaging().onMessage((message) => {
      debugger;
      
      console.log("Notification recieved====== onMessage",JSON.stringify(message));
     
      
    });
  }

  displayNotification(title, body, data) {
    const { navigation } = this.props;
    const { navigate } = navigation;
    debugger;
    // we display notification in alert box with title and body
    if (Object.keys(data).length !== 0) {
      if (data.direct_screen === 1 || data.direct_screen === "1") {
        Promise.all([
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              // TabNav is a TabNavigator nested in a StackNavigator
              routes: [
                {
                  name: "customerHome",
                },
              ],
            })
          ),
        ]).then(() => navigate("MyPlan"));
      } else if (data.direct_screen === 2 || data.direct_screen === "2") {
        Promise.all([
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              // TabNav is a TabNavigator nested in a StackNavigator
              routes: [
                {
                  name: "hotelHomeScreen",
                },
              ],
            })
          ),
        ]).then(() => navigate("ownerBooking",{ data: data }));
      } else if (data.direct_screen === 3 || data.direct_screen === "3") {
             Promise.all([
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              // TabNav is a TabNavigator nested in a StackNavigator
              routes: [
                {
                  name: "customerHome",
                },
              ],
            })
          ),
        ]).then(() =>
          this.props.navigation.navigate("MyPlan", { data: data })
        );
      } else if (data.direct_screen === 4 || data.direct_screen === "4") {
        Promise.all([
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              // TabNav is a TabNavigator nested in a StackNavigator
              routes: [
                {
                  name: "customerHome",
                },
              ],
            })
          ),
        ]).then(() =>
          navigate("particularHotelProfile", {
            id: data.hotel_id,
            bookingId: data.booking_id,
          })
        );
      }
    }

    //   if (data.hotel_id !== undefined && data.hotel_id !== null) {
    //     Promise.all([
    //       navigation.dispatch(
    //         CommonActions.reset({
    //           index: 0,
    //           // TabNav is a TabNavigator nested in a StackNavigator
    //           routes: [
    //             {
    //               name: "customerHome",
    //             },
    //           ],
    //         })
    //       ),
    //     ]).then(() =>
    //       navigate("particularHotelProfile", {
    //         id: data.hotel_id,
    //         bookingId: data.booking_id,
    //       })
    //     );
    //   } else if (data.booking_id !== undefined && data.booking_id !== null) {
    //     Promise.all([
    //       navigation.dispatch(
    //         CommonActions.reset({
    //           index: 0,
    //           // TabNav is a TabNavigator nested in a StackNavigator
    //           routes: [
    //             {
    //               name: "customerHome",
    //             },
    //           ],
    //         })
    //       ),
    //     ]).then(() =>
    //       this.props.navigation.navigate("MyPlan", { data: data })
    //     );
    //   } else {
    //     console.log("nothing");
    //   }
    // }
  }

  /** ****************** End of Notification Methods ********************************** */

  /** ****************** Login Api Response ********************************** */

  handlingLoginResponse = (response) => {
    const { navigation } = this.props;
    if (response.city === null) {
      if (response.hotel_details.hotel_service.length === 0) {
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              {
                name: "RoomsAmenities",
                params: { from: "Splash" },
              },
            ],
          })
        );
      } else if (response.hotel_details.hotel_documents.length === 0) {
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [
              {
                name: "CheckInCheckOut",
                params: { from: "Splash" },
              },
            ],
          })
        );
      } else {
        this.props.navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{ name: "hotelHomeScreen" }],
          })
        );
      }
    } else {
      navigation.dispatch(
        CommonActions.reset({
          index: 0,
          routes: [{ name: "customerHome" }],
        })
      );
    }
  };

  /** ****************** Current Location Method ********************************** */

  getCurrentLocation() {
    currentLocation.fetchLocation((position) => {
      console.warn("position", position);

      if (
        Platform.OS === "ios" &&
        position !== false &&
        position !== undefined
      ) {
        console.warn("position ios", position.message);

        if (position.message !== "User denied access to location services.") {
          AsyncStorage.setItem(
            STRINGS.latlong,
            JSON.stringify({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
            })
          );
        } else {
          DeviceInfo.getAvailableLocationProviders().then((providers) => {
            if (providers.gps || providers.network) {
              Geolocation.getCurrentPosition(
                (position) => {
                  console.log("position is-------", position);
                  AsyncStorage.setItem(
                    STRINGS.latlong,
                    JSON.stringify({
                      latitude: position.coords.latitude,
                      longitude: position.coords.longitude,
                    })
                  );
                },
                (error) =>
                  console.warn(
                    "Please turn on device GPS for using location services.",
                    error
                  )
              );
            }
          });
        }
      } else if (Platform.OS === "android" && position !== false) {
        DeviceInfo.getAvailableLocationProviders().then((providers) => {
          if (!providers.gps && !providers.network) {
            LocationServicesDialogBox.checkLocationServicesIsEnabled({
              message:
                "<h2 style='color: #0af13e'>Use Location ?</h2>This app wants to change your device settings:<br/><br/>Please enable GPS/location services for better experience<br/><br/>",
              ok: "YES",
              cancel: "NO",
              enableHighAccuracy: true, // true => GPS AND NETWORK PROVIDER, false => GPS OR NETWORK PROVIDER
              showDialog: true, // false => Opens the Location access page directly
              openLocationServices: true, // false => Directly catch method is called if location services are turned off
              preventOutSideTouch: false, //true => To prevent the location services popup from closing when it is clicked outside
              preventBackClick: false, //true => To prevent the location services popup from closing when it is clicked back button
              providerListener: true, // true ==> Trigger "locationProviderStatusChange" listener when the location state changes
            })
              .then(
                function (success) {
                  console.warn("success message", success);
                  // success => {alreadyEnabled: true, enabled: true, status: "enabled"}
                  Geolocation.getCurrentPosition(
                    (position) => {
                      AsyncStorage.setItem(
                        STRINGS.latlong,
                        JSON.stringify({
                          latitude: position.coords.latitude,
                          longitude: position.coords.longitude,
                        })
                      );
                    },
                    (error) =>
                      console.warn(
                        "Please turn on device GPS for using location services.",
                        error
                      )
                  );
                }.bind(this)
              )
              .catch((error) => {
                console.warn("error", error.message);
              });

            DeviceEventEmitter.addListener(
              "locationProviderStatusChange",
              function (status) {
                // only trigger when "providerListener" is enabled
                console.log(status); //  status => {enabled: false, status: "disabled"} or {enabled: true, status: "enabled"}
              }
            );

            /** ------------------------------- */
          } else if (providers.gps || providers.network) {
            Geolocation.getCurrentPosition(
              (position) => {
                AsyncStorage.setItem(
                  STRINGS.latlong,
                  JSON.stringify({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                  })
                );
              },
              (error) =>
                console.warn(
                  "Please turn on device GPS for using location services.",
                  error
                )
            );
          }
        });
      }
    });
  }

  /** ****************** Social Login Api Response ********************************** */

  socialResponse = (data) => {
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      const { response, message, status } = data;
      if (response !== undefined) {
        if (status === 200) {
          this.hideDialog();
          // this.showToastSucess(message);
          AsyncStorage.setItem(
            STRINGS.loginToken,
            JSON.stringify(response.access_token)
          );
          AsyncStorage.setItem(STRINGS.loginData, JSON.stringify(response));
          // this.props.navigation.navigate('customerHome')
          this.handlingLoginResponse(response);
        }
      } else if (status === 111) {
        this.hideDialog();
        if (message.non_field_errors !== undefined) {
          if (
            message.non_field_errors[0] ===
            "User with this social_id does not exist"
          ) {
            if (this.facebookData.length !== 0) {
              this.props.navigation.navigate("registerAs", {
                social_data: this.facebookData,
                socialProfile: this.social_profile,
              });
            } else if (this.googleData.length !== 0) {
              this.props.navigation.navigate("registerAs", {
                social_data: this.googleData,
                socialProfile: this.social_profile,
              });
            }
          } else {
            this.showToastAlert(message.non_field_errors[0]);
          }
        }
      } else if (status === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      }
    }
  };

  /** ****************** Get Auth Token API ********************************** */

  tokenAuthResponse = (tokenAuthResponse) => {
    const { navigation } = this.props;
    if (tokenAuthResponse !== undefined) {
      AsyncStorage.setItem(
        STRINGS.accessToken,
        JSON.stringify(tokenAuthResponse.access_token)
      );
      AsyncStorage.getItem(STRINGS.loginCredentials, (error, result) => {
        if (result !== undefined && result !== null) {
          let loginData = JSON.parse(result);
          if (loginData !== undefined && loginData !== null) {
            AsyncStorage.getItem("fcmToken", (error, fcmToken) => {
              if (fcmToken !== undefined && fcmToken !== null) {
                let deviceToken = this.deviceToken();
                if (loginData.social_id !== undefined) {
                  SocialLoginAction(
                    {
                      social_profile: loginData.social,
                      social_id: loginData.social_id,
                      accessToken: tokenAuthResponse.access_token,
                      username: loginData.email,
                      device_id: fcmToken,
                      deviceToken: deviceToken,
                    },
                    (data) => this.socialResponse(data)
                  );
                } else {
                  this.props.loginApi({
                    email: loginData.email,
                    password: loginData.password,
                    deviceToken,
                    device_id: fcmToken,
                    accessToken: tokenAuthResponse.access_token,
                  });
                }
              }
            });
            // let deviceToken = this.deviceToken();
          }
        } else {
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{ name: "login" }],
            })
          );
        }
      });
    }
  };

  /** ****************** Render ********************************** */

  render() {
    return (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <Image
          resizeMode={"contain"}
          style={{ height: wp(40), width: wp(40), padding: wp(20) }}
          source={ICONS.LOGO}
        />
      </View>
    );
  }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state) => ({
  loginState: state.LoginReducer,
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    loginApi: (payload) => dispatch(LoginAction(payload)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(Splash);
