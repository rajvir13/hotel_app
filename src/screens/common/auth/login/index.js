// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react";
import {
  View,
  Text,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Keyboard,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Platform,
} from "react-native";
import { LoginManager } from "react-native-fbsdk";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { CommonActions } from "@react-navigation/native";
import { GoogleSignin } from "@react-native-community/google-signin";
import { Header, Input } from "react-native-elements";
import AsyncStorage from "@react-native-community/async-storage";
import * as DeviceInfo from "react-native-device-info";
import { connect } from "react-redux";
import OrientationLoadingOverlay from "../../../../customComponents/Loader";
import firebase from "react-native-firebase";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import styles from "./loginStyle";
import BaseClass from "../../../../utils/BaseClass";
import STRINGS from "../../../../utils/Strings";
import {
  SafeAreaViewContainer,
  MainContainer,
  ShadowViewContainer,
} from "../../../../utils/BaseStyle";
import { facebookService } from "../../../../customComponents/button/FacebookButton";
import { googleService } from "../../../../customComponents/button/GoogleButton";
import { ICONS } from "../../../../utils/ImagePaths";
import { Spacer } from "../../../../customComponents/Spacer";
import COLORS from "../../../../utils/Colors";
import { FONT } from "../../../../utils/FontSizes";
import { PrimaryButton } from "../../../../customComponents/button/AppButton";
import * as Validations from "../../../../utils/Validations";
import { LoginAction } from "../../../../redux/actions/LoginAction";
import { SocialLoginAction } from "../../../../redux/actions/SocialLoginAction";
import { FONT_FAMILY } from "../../../../utils/Font";
import { TokenAuthAction } from "../../../../redux/actions/TokenAuthAction";

class LoginScreen extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      // email: 'newuser_25@gmail.com',
      // password: '123456$',
      email: "",
      password: "",
      accessToken: "",
      device_type: undefined,
      fcmToken: undefined,
    };
    this.facebookData = [];
    this.googleData = [];
    this.social_profile = "";
    this.social_id = undefined;
  }

  componentDidMount() {
    this.getFirebaseToken();
    DeviceInfo.getDeviceToken().then((deviceToken) => {
      console.warn("Device Token isss", deviceToken);
    });

    AsyncStorage.getItem(STRINGS.accessToken, (error, result) => {
      if (result !== undefined && result !== null) {
        let accessToken = JSON.parse(result);
        console.warn("accessToken", accessToken);
        if (accessToken !== undefined) {
          this.setState({ accessToken: accessToken });
        }
      } else {
        TokenAuthAction({}, (Data) => {
          if (Data !== undefined) {
            AsyncStorage.setItem(
              STRINGS.accessToken,
              JSON.stringify(Data.access_token)
            );
            this.setState({ accessToken: accessToken });
          }
        });
      }
    });

    GoogleSignin.configure({
      iosClientId:
        "966175069283-59t7c0tlt6kegat11gvgqadnbs1qekk2.apps.googleusercontent.com",
    });
  }

  checkPermission = async () => {
    debugger;
    const enabled = await firebase.messaging().hasPermission();
    console.log("enabled status login class", enabled);
    // If Premission granted proceed towards token fetch
    if (enabled) {
      this.getToken();
    } else {
      // If permission hasn’t been granted to our app, request user in requestPermission method.
      this.requestPermission();
    }
  };
  requestPermission = async () => {
    try {
      const authStatus = await firebase.messaging().requestPermission();
      const enabled =
        authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
        authStatus === messaging.AuthorizationStatus.PROVISIONAL;
        console.log("enable status", enabled);
        console.log("authStatus ", authStatus);
      if (enabled) {
        this.getToken();
        console.log("Authorization status:", authStatus);
      } else {
        this.requestPermission();
      }
    } catch (error) {
      // User has rejected permissions
      console.log("error", error);
      console.log("login  class permission rejected");
    }
  };
  getToken = async () => {
    const { fcmToken } = this.state;
    let fcmTokenNew = await firebase.messaging().getToken();
    if (fcmTokenNew) {
      // user has a device token
      console.warn("fcm token==>", fcmTokenNew);
      await AsyncStorage.setItem("fcmToken", fcmTokenNew);
      this.setState({ fcmToken: fcmTokenNew });
    }
  };

  getFirebaseToken = () => {
    AsyncStorage.getItem("fcmToken", (error, result) => {
      if (result !== undefined && result !== null) {
        this.setState({ fcmToken: result });
      } else {
        this.checkPermission();
      }
    });
  };

  componentDidUpdate(
    prevProps: Readonly<P>,
    prevState: Readonly<S>,
    snapshot: SS
  ): void {
    if (this.state.fcmToken === undefined) {
      this.getFirebaseToken();
    }
  }

  componentWillReceiveProps(nextProps, nextContext) {
    const { email, password, accessToken } = this.state;
    const { loginResponse } = nextProps.loginState;
    if (loginResponse !== undefined && loginResponse !== null) {
      if (loginResponse === "Network request failed") {
        this.hideDialog();
        this.showToastAlert(STRINGS.CHECK_INTERNET);
      } else {
        const { response, message, status } = loginResponse;
        if (response !== undefined) {
          if (status === 200) {
            this.hideDialog();
            this.showToastSucess(message);
            AsyncStorage.setItem(
              STRINGS.loginToken,
              JSON.stringify(response.access_token)
            );
            AsyncStorage.setItem(STRINGS.loginData, JSON.stringify(response));
            AsyncStorage.setItem(
              STRINGS.loginCredentials,
              JSON.stringify({
                email: email,
                password: password,
                social: this.social_profile,
                accessToken: accessToken,
                social_id: undefined,
              })
            );

            this.handlingLoginResponse(response);
            // if (response.city === null) {
            //     setTimeout(() => {
            //         this.props.navigation.dispatch(
            //             CommonActions.reset({
            //                 index: 0,
            //                 routes: [
            //                     {name: 'hotelHomeScreen'},
            //                 ],
            //             })
            //         );
            //     }, 2000)
            //
            // } else {
            //     this.props.navigation.dispatch(
            //         CommonActions.reset({
            //             index: 0,
            //             routes: [
            //                 {name: 'customerHome'},
            //             ],
            //         })
            //     );
            // }
          } else if (status === 111) {
            this.hideDialog();
            this.showToastAlert(message);
          } else if (status === 401) {
            this.hideDialog();
            this.showToastAlert(message);
          } else {
            this.hideDialog();
            this.showToastAlert(message);
          }
        } else if (message.non_field_errors !== undefined) {
          this.hideDialog();
          this.showToastAlert(message.non_field_errors[0]);
        } else {
          this.hideDialog();
          this.showToastAlert(message);
        }
        this.hideDialog();
      }
    }
  }

  handlingLoginResponse = (response) => {
    const { navigation } = this.props;
    // if (response.city === undefined) {
    //     if (response.hotel_service.length === 0) {
    //         setTimeout(() => {
    //             navigation.dispatch(
    //                 CommonActions.reset({
    //                     index: 0,
    //                     routes: [
    //                         {
    //                             name: 'RoomsAmenities',
    //                             params: {from: "Splash"}
    //                         },
    //                     ],
    //                 })
    //             );
    //         }, 2000)
    //     } else if (response.hotel_documents.length === 0) {
    //         setTimeout(() => {
    //             navigation.dispatch(
    //                 CommonActions.reset({
    //                     index: 0,
    //                     routes: [
    //                         {
    //                             name: 'CheckInCheckOut',
    //                             params: {from: "Splash"}
    //                         },
    //                     ],
    //                 })
    //             );
    //         }, 2000)
    //     } else {
    //         setTimeout(() => {
    //             this.props.navigation.dispatch(
    //                 CommonActions.reset({
    //                     index: 0,
    //                     routes: [
    //                         {name: 'hotelHomeScreen'},
    //                     ],
    //                 })
    //             );
    //         }, 2000)
    //     }
    // } else
    if (response.city === null) {
      if (response.hotel_details.hotel_service.length === 0) {
        setTimeout(() => {
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [
                {
                  name: "RoomsAmenities",
                  params: { from: "Splash" },
                },
              ],
            })
          );
        }, 2000);
      } else if (response.hotel_details.hotel_documents.length === 0) {
        setTimeout(() => {
          navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [
                {
                  name: "CheckInCheckOut",
                  params: { from: "Splash" },
                },
              ],
            })
          );
        }, 2000);
      } else {
        setTimeout(() => {
          this.props.navigation.dispatch(
            CommonActions.reset({
              index: 0,
              routes: [{ name: "hotelHomeScreen" }],
            })
          );
        }, 2000);
      }
    } else {
      setTimeout(() => {
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{ name: "customerHome" }],
          })
        );
      }, 2000);
    }
  };

  // ------------------------------------------------------

  forgotPassword() {
    const { navigate } = this.props.navigation;
    navigate("recoverPassword");
  }

  // ---------------------------------------------------

  login() {
    const { email, password, accessToken, fcmToken } = this.state;
    if (email.length < 1) {
      this.showToastAlert(STRINGS.username_email);
    } else if (email.includes("@")) {
      if (!Validations.validateEmail(email)) {
        this.showToastAlert(STRINGS.enter_valid_email);
      } else if (password.length < 1) {
        this.showToastAlert(STRINGS.empty_password);
      } else {
        let pushToken;
        if (fcmToken === undefined) {
          pushToken = "";
        } else {
          pushToken = fcmToken;
        }

        let deviceToken = this.deviceToken();
        this.props.loginApi({
          email,
          password,
          deviceToken,
          device_id: pushToken,
          accessToken,
        });
        this.showDialog();
      }
    } else {
      if (!email.match(/^\d{10}$/)) {
        this.showToastAlert("Phone number is not valid");
      } else if (password.length < 1) {
        this.showToastAlert(STRINGS.empty_password);
      } else {
        let pushToken;
        if (fcmToken === undefined) {
          pushToken = "";
        } else {
          pushToken = fcmToken;
        }

        let deviceToken = this.deviceToken();
        this.props.loginApi({
          email,
          password,
          deviceToken,
          device_id: pushToken,
          accessToken,
        });
        this.showDialog();
      }
    }
  }

  socialResponse = (data) => {
    const { email, password, accessToken } = this.state;
    if (
      data === "Network request failed" ||
      data == "TypeError: Network request failed"
    ) {
      this.hideDialog();
      this.showToastAlert(STRINGS.CHECK_INTERNET);
    } else {
      const { response, message, status } = data;
      if (response !== undefined) {
        if (status === 200) {
          this.hideDialog();
          this.showToastSucess(message);
          AsyncStorage.setItem(
            STRINGS.loginToken,
            JSON.stringify(response.access_token)
          );
          AsyncStorage.setItem(STRINGS.loginData, JSON.stringify(response));
          AsyncStorage.setItem(
            STRINGS.loginCredentials,
            JSON.stringify({
              email: email,
              password: undefined,
              social: this.social_profile,
              accessToken: accessToken,
              social_id: this.social_id,
            })
          );
          // this.props.navigation.navigate('customerHome')
          this.handlingLoginResponse(response);
          // if (response.hotel_details !== undefined) {
          //     setTimeout(() => {
          //         this.props.navigation.dispatch(
          //             CommonActions.reset({
          //                 index: 0,
          //                 routes: [
          //                     {name: 'hotelHomeScreen'},
          //                 ],
          //             })
          //         );
          //     }, 2000)
          // } else {
          //     this.props.navigation.dispatch(
          //         CommonActions.reset({
          //             index: 0,
          //             routes: [
          //                 {name: 'customerHome'},
          //             ],
          //         })
          //     );
          // }
        }
      } else if (status === 111) {
        this.hideDialog();
        if (message.non_field_errors !== undefined) {
          if (
            message.non_field_errors[0] ===
            "User with this social_id does not exist"
          ) {
            if (this.facebookData.length !== 0) {
              this.props.navigation.navigate("registerAs", {
                social_data: this.facebookData,
                socialProfile: this.social_profile,
              });
            } else if (this.googleData.length !== 0) {
              this.props.navigation.navigate("registerAs", {
                social_data: this.googleData,
                socialProfile: this.social_profile,
              });
            }
          } else {
            this.showToastAlert(message.non_field_errors[0]);
          }
        }
      } else if (status === 401) {
        this.hideDialog();
        this.showToastAlert(message);
      } else if (message.non_field_errors !== undefined) {
        this.hideDialog();
        this.showToastAlert(message.non_field_errors[0]);
      }
    }
  };

  //******************************** Google Login ********************** */
  _renderGoogleButton = () => {
    const { fcmToken } = this.state;

    return (
      <View>
        {googleService.googleLogin((data) => {
          this.removeSessionFromDevice();
          // let deviceToken = this.deviceToken();
          if (data.code === undefined) {
            this.social_profile = "Google";
            this.facebookData = [];
            this.social_id = data.user.id;
            if (data.user.photo !== null) {
              let photo;
              photo = data.user.photo.includes("=")
                ? data.user.photo.substr(0, data.user.photo.indexOf("="))
                : data.user.photo;
              data.user.photo = photo;
              this.googleData = data;
            } else {
              this.googleData = data;
            }
            this.setState({
              email: data.user.email,
            });
            SocialLoginAction(
              {
                social_profile: "Google",
                social_id: data.user.id,
                is_social: "1",
                accessToken: this.state.accessToken,
                username: data.user.email,
                device_id: fcmToken,
                deviceToken: this.deviceToken(),
                device_type: this.state.device_type,
              },
              (data) => this.socialResponse(data)
            );
            this.showDialog();
          }
        })}
      </View>
    );
  };

  removeSessionFromDevice = () => {
    GoogleSignin.signOut()
      .then(() => {
        /*
                  something
                */
      })
      .catch((err) => {
        console.log(err);
        /*
                  something
                */
      })
      .done();
  };
  // ------------------------------------------------------- facebook button click ---------------------

  _renderFacebookButton = () => {
    const { fcmToken } = this.state;
    return (
      <View>
        {facebookService.facebookLoginButton(async (fbData) => {
          // const profile = await facebookService.fetchProfile();
          LoginManager.logOut();
          LoginManager.setLoginBehavior("web_only");
          this.facebookData = fbData;
          this.social_profile = "Facebook";
          this.googleData = [];
          this.social_id = fbData.id;
          // let deviceToken = this.deviceToken();
          this.setState({
            email: fbData.email,
          });
          SocialLoginAction(
            {
              social_profile: "Facebook",
              is_social: "1",
              accessToken: this.state.accessToken,
              username: fbData.email,
              device_id: fcmToken,
              deviceToken: this.deviceToken(),
              device_type: this.state.device_type,
              social_id: fbData.id,
            },
            (data) => this.socialResponse(data)
          );
          this.showDialog();
        })}
      </View>
    );
  };

  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  static _renderHeader() {
    return (
      <Header
        // backgroundColor={COLORS.backGround_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        containerStyle={{
          // borderBottomColor: COLORS.greyButton_color,
          paddingTop: 0,
          height: 0,
        }}
      />
    );
  }

  // ----------------------------------------
  // ----------------------------------------
  // RENDERS
  // ----------------------------------------

  _renderInputText = () => {
    const { email, password } = this.state;
    return (
      <View
        style={{
          paddingVertical: wp(6),
          paddingHorizontal: wp(4),
          justifyContent: "space-evenly",
        }}
      >
        <Input
          inputContainerStyle={{
            borderBottomColor: COLORS.greyButton_color,
            alignItems: "center",
          }}
          placeholder={STRINGS.email_placeholder}
          placeholderTextColor={COLORS.placeholder_color}
          ref="Login"
          value={email}
          autoCapitalize="none"
          onChangeText={(text) =>
            this.setState({
              email: text.trim(),
            })
          }
          keyboardType="email-address"
          returnKeyType={"next"}
          onSubmitEditing={() => {
            this.refs.Password.focus();
          }}
          inputStyle={{
            fontSize: FONT.TextSmall,
            paddingLeft: 10,
            fontFamily: FONT_FAMILY.Montserrat,
          }}
          leftIconContainerStyle={{ marginLeft: 2 }}
          leftIcon={<Image source={ICONS.EMAIL_ICON} resizeMode={"contain"} />}
        />
        {/*<Spacer space={2}/>*/}
        <Input
          inputContainerStyle={{
            borderBottomColor: COLORS.greyButton_color,
            alignItems: "center",
          }}
          placeholder="Password"
          placeholderTextColor={COLORS.placeholder_color}
          ref="Password"
          value={password}
          onChangeText={(text) =>
            this.setState({
              password: text.trim(),
            })
          }
          secureTextEntry={true}
          returnKeyType={"done"}
          inputStyle={{
            paddingLeft: 10,
            fontSize: FONT.TextSmall,
            fontFamily: FONT_FAMILY.Montserrat,
          }}
          leftIconContainerStyle={{ marginLeft: 2 }}
          leftIcon={
            <Image source={ICONS.PASSWORD_ICON} resizeMode={"contain"} />
          }
        />
        {/*<Spacer space={2}/>*/}
        <TouchableOpacity
          style={{
            paddingVertical: wp(2),
            justifyContent: "flex-end",
            alignSelf: "flex-end",
          }}
          onPress={() => {
            this.forgotPassword();
          }}
        >
          <Text
            style={{
              justifyContent: "flex-end",
              fontFamily: FONT_FAMILY.Montserrat,
              fontSize: FONT.TextSmall,
              color: COLORS.black_color,
              // "fontFamily": FONTNAME.RalewayRegular
            }}
          >
            Forgot Password?
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  // -------------------------------------------------------
  _renderPrimaryButton = () => {
    return (
      <View style={{ paddingVertical: wp(1.5) }}>
        <PrimaryButton
          btnText={"LOGIN"}
          onPress={() => {
            this.login();
          }}
        />
      </View>
    );
  };

  // -------------------------------------------------------

  _renderORText = () => {
    return (
      <View style={{ flexDirection: "row", justifyContent: "center" }}>
        <View
          style={{
            width: wp(25),
            justifyContent: "center",
            alignSelf: "center",
            height: wp(0.3),
            backgroundColor: COLORS.step_indicator_unfinished_circle,
          }}
        />
        <View style={styles.ORView}>
          <Text style={styles.orText}>{STRINGS.or_text}</Text>
        </View>
        <View
          style={{
            width: wp(25),
            justifyContent: "center",
            alignSelf: "center",
            height: wp(0.3),
            backgroundColor: COLORS.step_indicator_unfinished_circle,
          }}
        />
      </View>
    );
  };

  render() {
    return (
      <SafeAreaViewContainer>
        {LoginScreen._renderHeader()}
        <KeyboardAvoidingView
          style={{ flex: 1, backgroundColor: COLORS.white_color }}
          behavior={Platform.OS === "ios" ? "padding" : null}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              Keyboard.dismiss();
            }}
          >
            {/*<ScrollContainer>*/}
            <ScrollView style={{ flex: 1 }}>
              <MainContainer>
                <View style={styles.containerStyle}>
                  <Image
                    style={styles.bgImageStyle}
                    source={ICONS.IMAGE_BACKGROUND}
                  />
                </View>
                <ScrollView
                  style={{ position: "absolute", top: 2, bottom: 10 }}
                >
                  <Spacer space={2} />

                  <View style={styles.scrollContainer}>
                    <Image source={ICONS.LOGO} style={styles.logoImg} />

                    <ShadowViewContainer style={{ marginBottom: 0 }}>
                      <View
                        style={{
                          backgroundColor: COLORS.white_color,
                          borderRadius: wp(5),
                          width: wp(80),
                          justifyContent: "center",
                          alignSelf: "center",
                        }}
                      >
                        <Text style={styles.loginAccText}>
                          {STRINGS.login_account}
                        </Text>
                        {this._renderInputText()}
                        {this._renderPrimaryButton()}
                        <Spacer space={2.5} />
                        {this._renderORText()}
                        <Spacer space={1} />
                        <View style={styles.socialContainer}>
                          {this._renderFacebookButton()}
                          <Spacer row={1} />
                          {this._renderGoogleButton()}
                        </View>
                        <Spacer space={2.5} />
                      </View>
                    </ShadowViewContainer>

                    <View style={{ marginTop: wp(8) }}>
                      <Text style={styles.dontText}>
                        {STRINGS.dont_have_text}
                      </Text>
                      <TouchableOpacity
                        onPress={() => {
                          this.props.navigation.navigate("registerAs");
                          // this.props.navigation.navigate('customerHome')
                        }}
                      >
                        <Text style={styles.registerText}>
                          {STRINGS.register_text}
                        </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </ScrollView>
                {this._renderCustomLoader()}
              </MainContainer>
            </ScrollView>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </SafeAreaViewContainer>
    );
  }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state) => ({
  loginState: state.LoginReducer,
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    loginApi: (payload) => dispatch(LoginAction(payload)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
