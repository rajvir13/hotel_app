// import React from 'react';
// import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
// import {StyleSheet, Dimensions} from 'react-native';
// import {FONT} from "../../../../utils/FontSizes";
//
// const window = Dimensions.get('window');
// const styles = StyleSheet.create({
//     containerStyle: {
//         alignSelf: 'center',
//         width: window.width,
//         overflow: 'hidden',
//         // height: window.width * 1.3
//         height: wp(100) * 1.3
//     },
//     bgContainerStyle: {
//         borderRadius: window.width,
//         width: wp(100) * 2,
//         height: wp(100) * 5,
//         marginLeft: -(wp(100) / 2),
//         position: 'absolute',
//         bottom: 0,
//         overflow: 'hidden'
//     },
//     bgImageStyle: {
//         height: wp(100) * 1.3,
//         width: wp(100),
//         position: 'absolute',
//         bottom: 0,
//         marginLeft: wp(100) / 2,
//         backgroundColor: '#9DD6EB', marginTop: 10
//     },
//     mainContainer: {
//         position: 'absolute',
//         // padding: wp('10%'),
//         width: wp('100%'),
//         // height: hp('95%'),
//         justifyContent: 'center',
//         // alignContent: 'space-around',
//         // backgroundColor:'red',
//         alignItems: 'center',
//         marginTop: wp(10)
//
//     },
//     scrollContainer: {
//         position: 'absolute',
//         padding: wp('10%'), width: wp('100%'), height: hp('100%'),
//     },
//     SectionStyle: {
//         flexDirection: 'row',
//         justifyContent: 'center',
//         alignItems: 'center',
//         height: 40,
//         margin: 5, padding: 5,
//     },
//     socialContainer: {
//         flexDirection: 'row', alignSelf: 'center',
//         alignContent: 'space-around', marginVertical: 10
//     },
//     ImageStyle: {
//         padding: 10,
//         margin: 5,
//         height: 25,
//         width: 25,
//         resizeMode: 'stretch',
//         alignItems: 'center'
//     },
//     logoImg: {
//         width: wp(35),
//         height: wp(35),
//         borderRadius: wp(35) / 2,
//         alignSelf: 'center', marginVertical: 10, paddingVertical: 10
//     },
//
//     loginAccText: {
//         alignSelf: 'center',
//         marginTop: wp(8),
//         // fontFamily: 'bold',
//         fontSize: 20,
//         fontWeight: "bold"
//     },
//     forgotText: {
//         alignSelf: 'flex-end', marginVertical: 10,
//         fontStyle: "normal",
//         fontWeight: "normal",
//         fontSize: 14,
//         lineHeight: 17,
//         display: "flex",
//         paddingRight: 5,
//         alignItems: "center",
//
//     },
//     ORView: {
//         backgroundColor: 'rgba(69, 130, 229, 0.3)',
//         justifyContent: 'center',
//         height: wp(7),
//         paddingHorizontal: wp(4),
//         borderRadius: wp(5)
//     },
//     orText: {
//         textAlign: 'center',
//         marginVertical: 5,
//         // fontStyle: "normal",
//         // fontWeight:"600",
//         fontSize: FONT.TextSmall_2,
//         lineHeight: 20,
//         display: "flex",
//     },
//     lineView:{
//         marginVertical: wp(2.5),
//         backgroundColor: 'pink',
//         height: wp(2)
//     },
//
//     dontText: {
//         textAlign: 'center',
//         marginVertical: 5,
//         fontStyle: "normal",
//         fontWeight: "600",
//         fontSize: FONT.TextMedium,
//         lineHeight: 20,
//         display: "flex",
//     },
//     centerShadow: {
//         marginHorizontal: 10,
//         paddingHorizontal: 10,
//         marginTop: 10
//     },
//
//     emailTI: {
//         margin: 8, padding: 5,
//         height: 40, borderBottomWidth: 1,
//         borderBottomColor: '#000000',
//         backgroundColor: 'transparent',
//         fontSize: 15
//     },
//     buttonStyle: {
//         alignSelf: 'center',
//         flexDirection: 'row',
//         justifyContent: 'center',
//         alignItems: 'center',
//         backgroundColor: '#4582E5',
//         borderWidth: 1,
//         borderColor: '#F1F1F1',
//         height: 48,
//         width: wp('55%'),
//         borderRadius: 24,
//         margin: 10
//     },
//     buttonText: {
//         fontSize: 15, fontFamily: 'bold', color: 'white',
//         textAlign: 'center', textAlignVertical: 'center'
//     },
//     registerText: {
//         alignSelf: 'center', marginVertical: 5,
//         fontStyle: "normal",
//         fontWeight: "600",
//         fontSize: 16,
//         lineHeight: 20,
//         display: "flex",
//         alignItems: "center", color: '#4582E5'
//     },
//     socialButton: {
//         height: 35, width: 35, margin: 10, paddingVertical: 5,
//         borderRadius: 17.5
// //
// //         width: ${wp('20%')};
// // height: ${hp('13%')};
// // borderRadius: ${wp("20%") / 2}
//     }
//
// });
//
// export default styles;

import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { StyleSheet, Dimensions } from "react-native";
import { FONT } from "../../../../utils/FontSizes";
import COLORS from "../../../../utils/Colors";
import { FONT_FAMILY } from "../../../../utils/Font";

const window = Dimensions.get("window");
const styles = StyleSheet.create({
  containerStyle: {
    width: window.width,
    overflow: "hidden",
    height: hp(100),
    top: 0,
  },
  bgImageStyle: {
    width: wp(100),
    bottom: 0,
    height:hp('50%'),
    // marginLeft: wp(100) / 2,
    backgroundColor: "#9DD6EB",
    borderBottomLeftRadius:wp('10%'),
    borderBottomRightRadius:wp('10%')

  },

  bgContainerStyle: {
    borderRadius: window.width,
    width: wp(100) * 2,
    height: wp(100) * 5,
    alignSelf: "center",
    position: "absolute",
    bottom: 0,
    overflow: "hidden",
  },

  mainContainer: {
    width: wp("100%"),
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: 'pink'
  },
  scrollContainer: {
    flex: 1,
    backgroundColor: COLORS.transparent,
    width: wp(100),
  },
  SectionStyle: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: 40,
    margin: 5,
    padding: 5,
  },
  socialContainer: {
    flexDirection: "row",
    alignSelf: "center",
    alignContent: "space-around",
    paddingTop: wp(3),
  },
  ImageStyle: {
    padding: 10,
    margin: 5,
    height: 25,
    width: 25,
    resizeMode: "stretch",
    alignItems: "center",
  },
  logoImg: {
    width: wp(30),
    height: wp(30),
    borderRadius: wp(35) / 2,
    alignSelf: "center",
    marginVertical: 10,
    paddingVertical: 10,
  },
  logoView: {
    width: wp(35),
    height: wp(35),
    borderRadius: wp(35) / 2,
  },
  loginAccText: {
    alignSelf: "center",
    marginTop: wp(8),
    fontFamily: FONT_FAMILY.MontserratSemiBold,
    fontSize: 20,
    // fontWeight: "bold"
  },
  forgotText: {
    alignSelf: "flex-end",
    marginVertical: 10,
    // fontStyle: "normal",
    // fontWeight: "normal",
    fontSize: FONT.TextSmall,
    fontFamily: FONT_FAMILY.MontserratBold,
    lineHeight: 17,
    display: "flex",
    paddingRight: 5,
    alignItems: "center",
  },
  ORView: {
    backgroundColor: "rgba(69, 130, 229, 0.3)",
    justifyContent: "center",
    height: wp(7),
    paddingHorizontal: wp(4),
    borderRadius: wp(5),
  },
  orText: {
    textAlign: "center",
    marginVertical: 5,
    // fontStyle: "normal",
    // fontWeight:"600",
    fontSize: FONT.TextSmall_2,
    lineHeight: 20,
    display: "flex",
  },
  lineView: {
    marginVertical: wp(2.5),
    // backgroundColor: 'pink',
    height: wp(2),
  },

  dontText: {
    textAlign: "center",
    marginVertical: 5,
    fontFamily: FONT_FAMILY.MontserratSemiBold,
    fontSize: FONT.TextMedium,
    lineHeight: 20,
    display: "flex",
  },
  centerShadow: {
    marginHorizontal: 10,
    paddingHorizontal: 10,
    marginTop: 10,
  },

  emailTI: {
    margin: 8,
    padding: 5,
    height: 40,
    borderBottomWidth: 1,
    borderBottomColor: "#000000",
    backgroundColor: "transparent",
    fontSize: 15,
  },
  buttonStyle: {
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#4582E5",
    borderWidth: 1,
    borderColor: "#F1F1F1",
    height: 48,
    width: wp("55%"),
    borderRadius: 24,
    margin: 10,
  },
  buttonText: {
    fontSize: 15,
    fontFamily: "bold",
    color: "white",
    textAlign: "center",
    textAlignVertical: "center",
  },
  registerText: {
    alignSelf: "center",
    marginVertical: 5,
    // fontStyle: "normal",
    // fontWeight: "600",
    // fontSize: 16,
    fontSize: FONT.TextSmall,
    fontFamily: FONT_FAMILY.Montserrat,
    lineHeight: 20,
    display: "flex",
    alignItems: "center",
    color: "#4582E5",
  },
  socialButton: {
    height: 35,
    width: 35,
    margin: 10,
    paddingVertical: 5,
    borderRadius: 17.5,
    //
    //         width: ${wp('20%')};
    // height: ${hp('13%')};
    // borderRadius: ${wp("20%") / 2}
  },
});

export default styles;
