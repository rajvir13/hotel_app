import React from "react";
import {
    View,
    Text,
    TextInput,
    Keyboard,
    TouchableWithoutFeedback,
    TouchableOpacity,
    Platform, StatusBar, KeyboardAvoidingView
} from "react-native";
import {Header} from 'react-native-elements';

import styles from "./forgotStyle";
import BaseClass from "../../../../utils/BaseClass";
import STRINGS from '../../../../utils/Strings'
import {SafeAreaViewContainer, MainContainer, ScrollContainer, ShadowViewContainer} from "../../../../utils/BaseStyle";
import COLORS from "../../../../utils/Colors";
import {FONT} from "../../../../utils/FontSizes";
import {LeftIcon} from "../../../../customComponents/icons";
import * as Validations from "../../../../utils/Validations";
import {ForgotAction} from "../../../../redux/actions/ForgotAction";
import AsyncStorage from "@react-native-community/async-storage";

//library imports
import {connect} from "react-redux";
import {Spacer} from "../../../../customComponents/Spacer";
import OrientationLoadingOverlay from "../../../../customComponents/Loader";
import {FONT_FAMILY} from "../../../../utils/Font";

class RecoverPassword extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            emailText: "",
            accessToken: '',
        };
    }

    //new code
    componentDidMount() {

        AsyncStorage.getItem(STRINGS.accessToken, (error, result) => {
            if (result !== undefined && result !== null) {
                let accessToken = JSON.parse(result);
                if (accessToken !== undefined) {
                    this.setState({accessToken: accessToken});
                }
            }
        });
    }

    componentWillReceiveProps(nextProps, nextContext) {
        const {ForgotResponse} = nextProps.forgotState;
        if (ForgotResponse !== undefined && ForgotResponse !== null) {
            if (ForgotResponse === "Network Error") {
                this.hideDialog();
                this.showToastAlert(STRINGS.CHECK_INTERNET)
            } else {
                const {response, message, status} = ForgotResponse;
                if (response !== undefined) {
                    if (status === 200) {
                        this.hideDialog();
                        this.showToastSucess("Reset password link has been sent to your email");
                        this.setState({emailText: ""});
                        this.setTimePassed(() => {
                            this.BackToLogin();
                        }, 4000);

                    } else {
                        this.hideDialog();
                        this.showToastAlert(message);
                    }
                } else if (status === 111 && message.non_field_errors !== undefined) {
                    this.hideDialog();
                    this.showToastAlert(message.non_field_errors[0]);
                } else if (status === 401) {
                    this.hideDialog();
                    this.showToastAlert(message);
                } else {
                    this.hideDialog();
                    this.showToastAlert("server error");
                }
                this.hideDialog()
            }
        }
    }

    setTimePassed() {
        this.props.navigation.goBack(null);
        this.showToastSucess("Reset password link has been sent to your email");
    }

    // send button action
    sendPress = () => {
        const {emailText, accessToken} = this.state;
        console.warn("test", accessToken);
        Keyboard.dismiss();
        if (emailText.length < 1) {
            this.showToastAlert(STRINGS.empty_email);
        } else if (!Validations.validateEmail(emailText)) {
            this.showToastAlert(STRINGS.enter_valid_email);
        } else {            //hit api
            console.warn('api called');
            this.showDialog();
            this.props.forgotApi({
                emailText,
                accessToken
            })
        }

    };

    // =============================================================================================
    // Render methods for Header
    // =============================================================================================

    _renderHeader() {
        const {navigation} = this.props;
        return (
            <Header
                backgroundColor={COLORS.white_color}
                barStyle={"dark-content"}
                statusBarProps={{
                    translucent: true,
                    backgroundColor: COLORS.transparent,
                }}
                leftComponent={(<LeftIcon onPress={() => navigation.goBack()}/>)}
                centerComponent={{
                    text: STRINGS.forgot_header_title, style: {
                        color: COLORS.greyButton_color,
                        fontSize: FONT.TextMedium_2,
                        fontFamily: FONT_FAMILY.PoppinsBold
                    }
                }}
                containerStyle={{
                    borderBottomColor: COLORS.greyButton_color,
                    paddingTop: Platform.OS === 'ios' ? 0 : 25,
                    height: Platform.OS === 'ios' ? 60 : StatusBar.currentHeight + 60,
                }}
            />
        )
    }

    _renderCustomLoader = () => {
        const {isLoading} = this.state;
        return (
            <OrientationLoadingOverlay visible={isLoading} message={STRINGS.LOADING_TEXT}/>
        )
    };

    render() {
        return (
            <SafeAreaViewContainer>
                 <KeyboardAvoidingView
                    style={{flex: 1, backgroundColor: COLORS.WHITE_COLOR}}
                    behavior={(Platform.OS === 'ios') ? 'padding' : null}>
                {this._renderHeader()}

                    <TouchableWithoutFeedback
                        onPress={() => {
                            Keyboard.dismiss()
                        }}>
                        <ScrollContainer style={{backgroundColor: COLORS.white_color}}>
                            <MainContainer>
                                <View style={styles.containerStyle}>
                                    {/*<ScrollView style={{paddingVertical: wp('15%'), backgroundColor:'pink'}}>*/}
                                    <Text style={styles.recoverText}>{STRINGS.recover_password}</Text>
                                    <Text style={styles.enterText}>{STRINGS.enter_register}</Text>
                                    <Spacer space={5}/>
                                    <ShadowViewContainer>
                                        <TextInput style={styles.emailTI}
                                                   placeholder={STRINGS.enter_email}
                                                   selectionColor={'#4582E5'}
                                                   keyboardType='email-address'
                                                   onChangeText={(text) => this.setState({emailText: text})}/>
                                    </ShadowViewContainer>
                                    <TouchableOpacity onPress={() => this.sendPress()}>
                                        <View style={styles.buttonStyle}>
                                            <Text style={styles.buttonText}>{STRINGS.send_button}</Text>
                                        </View>
                                    </TouchableOpacity>
                                    {/*</ScrollView>*/}
                                </View>
                                {this._renderCustomLoader()}
                            </MainContainer>
                        </ScrollContainer>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaViewContainer>
        );
    }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = state => ({
    forgotState: state.ForgotReducer
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
    return {
        forgotApi: (payload) => dispatch(ForgotAction(payload))
    };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(RecoverPassword);
