import React from 'react';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
import {StyleSheet,} from 'react-native';

import {FONT_FAMILY} from '../../../../utils/Font'
import COLORS from "../../../../utils/Colors";
import {FONT} from "../../../../utils/FontSizes";

const styles = StyleSheet.create({
    containerStyle: {
        width: wp('100%'),
        // height: hp('100%'),
        paddingTop: wp('22%'),
        justifyContent: 'center',
        alignItems:'center',
        // position: 'absolute',
        // alignContent: 'space-between',
        // backgroundColor:'white',         Colors define kiye hai use them.. not text
        backgroundColor: COLORS.white_color,
    },
    headerStyle: {},
    shadowContainer: {
        height: 50,
        borderRadius: 25,
        margin: 10
    },
    buttonStyle: {
        width: wp(90),
        paddingVertical: wp(3.5),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#4582E5',
        borderWidth: 1,
        borderColor: 'transparent',
        height: 50,
        // width:wp('80%'),
        borderRadius: 25,
        margin: 10
    },
    emailTI: {
        width: wp(85),
        paddingVertical: wp(3),
        textAlign: 'center',
        // backgroundColor: 'red',
        fontSize: 15,
        fontFamily: FONT_FAMILY.Montserrat
    },

    buttonText: {
        fontSize: 15, fontWeight: 'bold', color: 'white',
        textAlign: 'center', textAlignVertical: 'center',
        fontFamily: FONT_FAMILY.Montserrat
    },
    enterText: {
        margin: 5,
        fontSize: FONT.TextSmall,
        fontStyle: "normal",
        fontWeight: "normal",
        lineHeight: 18,
        color: '#000000',
        textAlign: 'center',
        padding: 5,
        textAlignVertical: 'center',
        fontFamily: FONT_FAMILY.Montserrat
    },
    recoverText: {
        // margin: 5,
        // padding: 5,
        fontSize: FONT.TextLarge,
        fontStyle: "normal",
        fontWeight: "500",
        lineHeight: 37,
        color: '#000000',
        textAlign: 'center',
        textAlignVertical: 'center',
        fontFamily: FONT_FAMILY.Montserrat
    },

});
export default styles;
