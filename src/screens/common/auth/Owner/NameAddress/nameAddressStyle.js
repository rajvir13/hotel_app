import React from 'react';
import styled from 'styled-components';
import COLORS from '../../../../../utils/Colors';
import {FONT} from "../../../../../utils/FontSizes";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
    mainContainerStyle: {
        width: wp('100%'),
        // padding: wp('8%'),
        alignItems: "center",
        justifyContent: 'center',
        flex: 1,
        marginTop: wp(5)
    },
    imagesContainer: {
        width: wp(84),
        flexDirection: "row",
    },
    imgOuterView: {
        height: wp('25%'),
        width: wp('25%'),
        borderRadius: wp('25%') / 2,
        backgroundColor: COLORS.imgage_bg,
        justifyContent: "center"
    },
    phoneContainerStyle: {
        flexDirection: "row",
        // height: Platform.OS === "ios" ? wp(19) : wp(20),
        // paddingVertical: wp(1),
    },
    imageStyle: {
        // // resizeMode: "stretch",
        // height: wp('20%'),
        // width: wp('20%'),
        // borderRadius: wp('20%') / 2,
        // backgroundColor: 'orange',
        // marginHorizontal: wp(2)
        height: wp('20%'),
        width: wp('20%'),
        borderRadius: wp('20%') / 2,
    },
    textinputStyle: {
        marginVertical: wp(0.5),
        width: wp('85%'),
        flexDirection: "row"
    },
    inputStyle: {
        color: COLORS.black_color,
        fontSize: FONT.TextSmall_2,
        paddingHorizontal: wp(2),
        paddingVertical: wp(3),
        width: wp(73),
        // flex:.95,

    },
    buttonStyle: {
        marginVertical: 10,
        alignSelf: "center",
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.app_theme_color,
        height: wp('12%'),
        width: wp('80%'),
        borderRadius: wp('12%') / 2,
    },
    buttonText: {
        fontSize: 16,
        fontStyle: "normal",
        fontWeight: "500",
        lineHeight: 20,
        display: "flex",

        color: '#FFFFFF',
        textAlign: 'center',
    },
});

export default styles;
