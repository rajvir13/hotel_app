/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import React from "react";
import {
  View,
  Text,
  Image,
  FlatList,
  TextInput,
  Keyboard,
  StatusBar,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ImageBackground,
  KeyboardAvoidingView,
} from "react-native";
import styles from "./nameAddressStyle";
import BaseClass from "../../../../../utils/BaseClass";
import STRINGS from "../../../../../utils/Strings";
import { Header, Input } from "react-native-elements";
import { Spacer } from "../../../../../customComponents/Spacer";
import COLORS from "../../../../../utils/Colors";
import { LeftIcon } from "../../../../../customComponents/icons";
import { FONT } from "../../../../../utils/FontSizes";
import {
  SafeAreaViewContainer,
  MainContainer,
  ShadowViewContainer,
  ScrollContainer,
  BorderViewContainer,
} from "../../../../../utils/BaseStyle";
import { ICONS } from "../../../../../utils/ImagePaths";
import CustomTextInput from "../../../../../customComponents/CustomTextInput";
import { OwnerRegNameAction } from "../../../../../redux/actions/OwnerNameAction";
import * as Validations from "../../../../../utils/Validations";
// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import ImagePicker from "react-native-image-crop-picker";
import { connect } from "react-redux";
import * as DeviceInfo from "react-native-device-info";
import CountryPicker from "react-native-country-picker-modal";
import AsyncStorage from "@react-native-community/async-storage";
import * as _ from "lodash";
import {
  EmailVerifyAction,
  MobileVerifyAction,
} from "../../../../../redux/actions/registerAction";
import firebase from "react-native-firebase";
import OrientationLoadingOverlay from "../../../../../customComponents/Loader";
import { FONT_FAMILY } from "../../../../../utils/Font";

const userCountryData = "91";

export default class NameAddress extends BaseClass {
  constructor(props) {
    super(props);
    let userLocaleCountryCode = "IN";
    this.state = {
      ownerFirstName: "",
      ownerLastName: "",
      emailText: "",
      contactText: "",
      phoneNoText: "",
      hotelName: "",
      imagesList: [],
      hotelAddress: "",
      passwordText: "",
      confirmPassword: "",
      isAddShow: true,
      cca2: userLocaleCountryCode,
      callingCode: userCountryData,
      deviceToken: "41345qf1gy2y",
      deviceId: "",
      valueLat: "",
      valueLng: "",
      isDropvisible: false,
      details: "",
    };
    this.social_profile = "";
    this.social_data = {};
  }

  onCountrySelect = (value) => {
    this.setState({
      cca2: value.cca2,
      callingCode: value.callingCode[0],
    });
  };
  componentDidMount() {
    if (this.props.route.params !== undefined) {
      if (
        this.props.route.params.socialData !== undefined &&
        this.props.route.params.socialData !== null &&
        this.props.route.params.socialData !== ""
      ) {
        this.social_profile = this.props.route.params.socialProfile;
        this.social_data = this.props.route.params.socialData;
        let social_data = this.props.route.params.socialData;
        let firstName = "";
        let LastName = "";
        if (this.social_profile === "Facebook") {
          if (social_data.first_name !== "null")
            firstName = social_data.first_name;
        } else {
          if (social_data.user.name !== "null")
            firstName = social_data.user.name;
        }

        if (this.social_profile === "Facebook") {
          if (social_data.last_name !== "null")
            LastName = social_data.last_name;
        } else {
          if (social_data.user.familyName !== "null")
            LastName = social_data.user.familyName;
        }

        this.setState({
          emailText:
            this.social_profile === "Facebook"
              ? social_data.email
              : social_data.user.email,
          ownerFirstName:
            this.social_profile === "Facebook" ? firstName : firstName,
          ownerLastName:
            this.social_profile === "Facebook" ? LastName : LastName,
        });
      }
    }

    AsyncStorage.getItem(STRINGS.accessToken, (error, result) => {
      if (result !== undefined && result !== null) {
        let token = JSON.parse(result);
        if (token !== undefined) {
          this.setState({
            authToken: token,
          });
        }
      }
    });
  }

  /*
// =============================================================================================
// Render method for Header
// =============================================================================================
*/
  _renderHeader() {
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.backGround_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={<LeftIcon onPress={() => navigation.goBack()} />}
        centerComponent={{
          text: STRINGS.register_text,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  /*   // =============================================================================================
    // Render method for Listview
    // =============================================================================================
    */

  _renderImageList = ({ item, index }) => (
    <>
      <ImageBackground
        style={styles.imageStyle}
        source={{ uri: item !== undefined ? item.uri : "" }}
        imageStyle={{ borderRadius: wp("20%") / 2 }}
        resizeMode={"cover"}
      >
        <TouchableWithoutFeedback onPress={() => this.CrossAction(item, index)}>
          <Image
            style={{
              height: wp("8%"),
              width: wp("8%"),
              alignSelf: "flex-end",
              position: "absolute",

              top: wp("-1%"),
            }}
            source={ICONS.CLOSE_WINDOW}
          />
        </TouchableWithoutFeedback>
      </ImageBackground>
    </>
  );

  // =============================================================================================
  // Click on Row of FlatList //// separator
  // =============================================================================================

  CrossAction(i, indexClicked) {
    //remove value from imagelist
    const { imagesList } = this.state;
    let tempArray = _.remove(imagesList, (item, index) => {
      return indexClicked !== index;
    });
    this.setState({
      imagesList: tempArray,
    });
    this.setState({
      refresh: !this.state.refresh,
    });
    if (imagesList.length !== 10) this.setState({ isAddShow: true });
  }

  AddImageAction() {
    const { imagesList } = this.state;
    ImagePicker.openPicker({
      compressImageQuality: 0.2,
      includeBase64: true,
      multiple: true,
      maxFiles: 10,
      mediaType: "photo",
    }).then((response) => {
      let tempArray = [];
      response.forEach((item) => {
        let image = {
          // src:item.path,
          base64Data: item.data,
          uri: item.path,
          type: item.mime,
          name: item.path.substring(item.path.lastIndexOf("/") + 1),
        };
        tempArray.push(image);
      });
      let count = imagesList.length + response.length;
      if (count >= 1 && count < 11) {
        if (imagesList.length !== 0) {
          response.forEach((item) => {
            let image = {
              // src:item.path,
              base64Data: item.data,
              uri: item.path,
              type: item.mime,
              name: item.path.substring(item.path.lastIndexOf("/") + 1),
            };
            imagesList.push(image);
          });
        } else {
          this.setState({ imagesList: tempArray });
        }
        if (count === 10) this.setState({ isAddShow: false });
        this.setState({
          refresh: !this.state.refresh,
        });
      } else {
        this.showToastAlert(STRINGS.ONLY_TEN_IMAGES);
      }
    });
  }

  _itemSeparator = () => <View style={{ paddingHorizontal: wp(2) }} />;

  // AddIcon = () => {
  //     return ((this.state.isAddShow) &&
  //         <View style={{justifyContent: "center"}}>
  //             <TouchableWithoutFeedback onPress={() => this.AddImageAction()}>
  //                 <View style={styles.imgOuterView}>
  //                     <TouchableWithoutFeedback onPress={() => this.AddImageAction()}>
  //                         <Image style={{height: 40, width: 40, alignSelf: 'center'}} source={ICONS.ADD_IMAGE_ICON}/>
  //                     </TouchableWithoutFeedback>
  //                 </View>
  //             </TouchableWithoutFeedback>
  //         </View>
  //     );
  // };

  AddIcon = (index) => {
    return this.state.isAddShow ? (
      <View style={{ justifyContent: "center" }}>
        <TouchableWithoutFeedback onPress={() => this.AddImageAction(index)}>
          <ImageBackground style={styles.imgOuterView}>
            <TouchableWithoutFeedback
              onPress={() => this.AddImageAction(index)}
            >
              <Image
                style={{
                  height: wp("13%"),
                  width: wp("13%"),
                  alignSelf: "center",
                }}
                source={ICONS.ADD_IMAGE_ICON}
              />
            </TouchableWithoutFeedback>
          </ImageBackground>
        </TouchableWithoutFeedback>
      </View>
    ) : (
      <View />
    );
  };

  _renderChooseImage = () => {
    return (
      <FlatList
        style={{ width: wp("60%") }}
        data={this.state.imagesList}
        renderItem={this._renderImageList}
        horizontal={true}
        showsHorizontalScrollIndicator={true}
        keyExtractor={(item, index) => index}
      />
    );
  };

  //call back
  callback(lat, lng, response) {
    this.setState({
      hotelAddress: response.results[0].formatted_address,
    });
  }

  //end screen

  MapAction() {
    this.props.navigation.navigate("Maps", {
      callback: this.callback.bind(this),
    });
  }

  // api call

  NextAction() {
    const {
      ownerFirstName,
      ownerLastName,
      emailText,
      phoneNoText,
      hotelName,
      hotelAddress,
      details,
      passwordText,
      confirmPassword,
      callingCode,
      imagesList,
      authToken,
      deviceId,
      deviceToken,
      valueLat,
      valueLng,
    } = this.state;

    console.log("hotelAddress", hotelAddress);
    if (ownerFirstName.length < 1) {
      this.showToastAlert(STRINGS.empty_owner_name);
    } else if (ownerFirstName.trimRight().length < 2) {
      this.showToastAlert(STRINGS.name_minimum_three);
    } else if (ownerLastName.length < 1) {
      this.showToastAlert(STRINGS.empty_last_name);
    } else if (ownerLastName.trimRight().length < 2) {
      this.showToastAlert(STRINGS.name_minimum_three);
    } else if (emailText.length < 1) {
      this.showToastAlert(STRINGS.empty_email);
    } else if (!Validations.validateEmail(emailText)) {
      this.showToastAlert(STRINGS.valid_email);
    } else if (phoneNoText.length < 1) {
      this.showToastAlert(STRINGS.enter_mobileNo);
      // } else if (phoneNoText.length < 1) {
      //     this.showToastAlert(STRINGS.empty_owner_phone_number);
    } else if (hotelName.length < 1) {
      this.showToastAlert(STRINGS.empty_owner_hotel_name);
    } else if (hotelAddress == "hotelAddress") {
      this.showToastAlert("Please Choose map location.");
    } else if (hotelAddress.length < 1) {
      this.showToastAlert(STRINGS.empty_owner_hotel_address);
    } else if (passwordText.length < 1) {
      this.showToastAlert(STRINGS.empty_password);
    } else if (!Validations.validatePassword(passwordText)) {
      this.showToastAlert(STRINGS.enter_valid_password);
    } else if (confirmPassword.length < 1) {
      this.showToastAlert(STRINGS.empty_confirm_password);
    } else if (confirmPassword.trim() !== passwordText.trim()) {
      this.showToastAlert(STRINGS.password_does_not_match);
    } else if (details.trim().length < 1) {
      this.showToastAlert(STRINGS.empty_owner_hotel_detail);
    } else {
      EmailVerifyAction(
        {
          email: emailText,
          accessToken: authToken,
        },
        (data) => this.emailVerifyResponse(data)
      );
      this.showDialog();
    }
  }

  emailVerifyResponse = (data) => {
    const { authToken, phoneNoText, callingCode } = this.state;
    if (data !== undefined) {
      this.hideDialog();
      if (data.status === 200) {
        MobileVerifyAction(
          {
            phoneNumber: phoneNoText,
            accessToken: authToken,
          },
          (data) => this.phoneVerifyResponse(data)
        );
        // this.showDialog();
      } else if (data.status === 111) {
        if (data.message.email !== undefined) {
          this.showToastAlert(data.message.email[0]);
        } else {
          this.showToastAlert(data.message);
        }
      } else if (data.status === 401) {
        this.showToastAlert(data.message);
      }
    } else {
      this.hideDialog();
    }
  };

  phoneVerifyResponse = (data) => {
    if (data !== undefined) {
      this.hideDialog();
      if (data.status === 200) {
        this.hideDialog();
        this.sendOTP();
      } else if (data.status === 111) {
        this.hideDialog();
        if (data.message.mobile_number !== undefined) {
          this.showToastAlert(data.message.mobile_number[0]);
          this.hideDialog();
        } else {
          this.showToastAlert(data.message);
          this.hideDialog();
        }
      } else if (data.status === 401) {
        this.hideDialog();
        this.showToastAlert(data.message);
      }
    } else {
      this.hideDialog();
    }
  };

  sendOTP = async () => {
    const {
      ownerFirstName,
      ownerLastName,
      emailText,
      phoneNoText,
      hotelName,
      hotelAddress,
      passwordText,
      details,
      callingCode,
      imagesList,
      authToken,
      deviceId,
      deviceToken,
      valueLat,
      valueLng,
    } = this.state;
    let data = {
      firstName: ownerFirstName,
      lastName: ownerLastName,
      email: emailText,
      hotelAddress: hotelAddress,
      lat: valueLat,
      long: valueLng,
      hotelName: hotelName,
      callingCode: `+${callingCode}`,
      phoneNumber: phoneNoText,
      password: passwordText,
      accessToken: authToken,
      images: imagesList,
      socialData: this.social_data,
      socialType: this.social_profile,
      hotelDetail: details,
    };
    await firebase
      .auth()
      .signInWithPhoneNumber(`+${callingCode}${phoneNoText}`)
      // await firebase.auth().signInWithPhoneNumber("+919878717918")
      .then((confirmResult) => {
        console.log("success", confirmResult);
        this.hideDialog();
        this.props.navigation.navigate("verification", {
          confirmResult: confirmResult,
          registerOwnerData: data,
        });
      })
      .catch((error) => {
        this.hideDialog();
        if (
          error.code === "auth/unknown" ||
          error.code === "auth/too-many-requests"
        ) {
          this.showToastAlert(STRINGS.limit_exceeded_text);
        } else if (error.code === "auth/invalid-phone-number") {
          this.showToastAlert(STRINGS.unauthorized_phone_number);
        } else {
          this.hideDialog();
          console.log(error, error.code, "ios error");
        }
      });
  };

  /*
  // =============================================================================================
  // Render method for country code picker
  // =============================================================================================
  */
  _renderCountryPicker = () => {
    return (
      <CountryPicker
        ref={(picker) => {
          this.picker = picker;
        }}
        countryList={this.state.countryData}
        onSelect={(value) => {
          console.warn("selected country code is ", value);
          this.setState({
            cca2: value.cca2,
            callingCode: value.callingCode[0],
          });
        }}
        hideAlphabetFilter={true}
        showCallingCode={true}
        filterPlaceholder={STRINGS.SEARCH_TEXT}
        closeable={true}
        filterable={true}
        cca2={this.state.cca2}
        translation="eng"
        autoFocusFilter={false}
        countryCode={this.state.cca2}
        withFilter={true}
      />
    );
  };

  DropDownView = () => {
    const { isDropvisible } = this.state;
    return (
      <TouchableOpacity
        onPress={() => this.setState({ isDropvisible: !isDropvisible })}
      >
        <Image
          source={ICONS.DOWN_ARROW}
          style={{ height: wp(4), width: wp(4) }}
        />
      </TouchableOpacity>
    );
  };

  _renderCountryPicker2 = () => {
    return (
      <CountryPicker
        ref={(picker) => {
          this.picker = picker;
        }}
        renderFlagButton={this.DropDownView}
        countryList={this.state.countryData}
        onSelect={(value) => {
          this.onCountrySelect(value);
        }}
        hideAlphabetFilter={true}
        showCallingCode={true}
        filterPlaceholder={STRINGS.SEARCH_TEXT}
        closeable={true}
        filterable={true}
        cca2={this.state.cca2}
        translation="eng"
        autoFocusFilter={false}
        countryCode={this.state.cca2}
        withFilter={true}
        visible={this.state.isDropvisible}
        onClose={() => this.setState({ isDropvisible: false })}
      />
    );
  };
  /*
        // =============================================================================================
        // Render method for Phone number field
        // =============================================================================================
        */

  _renderPhoneNumberView = () => {
    const { phoneNoText, callingCode } = this.state;
    // let countryCodeLength = (callingCode.length > 2) ? 9 : 10;
    return (
      <View style={styles.phoneContainerStyle}>
        <ShadowViewContainer>
          <Input
            inputContainerStyle={{
              borderBottomColor: "transparent",
            }}
            editable={false}
            pointerEvents={"none"}
            // leftIcon={this._renderCountryPicker}
            // leftIconContainerStyle={{marginLeft: 0, alignItems: "center"}}
            value={`+${callingCode}`}
            containerStyle={{
              width: wp("24%"),
              height: wp(12.5),
            }}
            rightIcon={
              this._renderCountryPicker2
              // <TouchableOpacity onPress={() => this.picker.setState({ modalVisible: true })}  >
              //     <Image source={ICONS.DOWN_ARROW} />
              // </TouchableOpacity>
            }
            inputStyle={{
              fontSize: FONT.TextSmall_2,
            }}
          />
        </ShadowViewContainer>
        <Spacer row={1.5} />
        <ShadowViewContainer>
          <Input
            inputContainerStyle={{
              borderBottomColor: "transparent",
            }}
            // editable={false}
            placeholder={STRINGS.MOBILE_NUMBER}
            placeholderTextColor={COLORS.placeholder_color}
            ref="Mobile"
            value={phoneNoText}
            onChangeText={(text) =>
              this.setState({
                phoneNoText: text.trim() && text.replace(/^0+/, ""),
                // isChangedValue: true
              })
            }
            maxLength={10}
            keyboardType="phone-pad"
            returnKeyType={"next"}
            inputStyle={{
              paddingLeft: wp(0.5), //fontFamily: FONTNAME.RalewayRegular
              fontSize: FONT.TextSmall_2,
            }}
            containerStyle={{
              paddingBottom: 0,
              alignItems: "center",
              width: wp("58%"),
              height: wp(12.5),
            }}
          />
        </ShadowViewContainer>
      </View>
    );
  };

  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  /*
    // =============================================================================================
    // Render method for screen
    // =============================================================================================
    */
  render() {
    const {
      ownerFirstName,
      ownerLastName,
      emailText,
      contactText,
      hotelName,
      hotelAddress,
      passwordText,
      confirmPassword,
      details,
    } = this.state;
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <KeyboardAvoidingView
          style={{ flex: 1, backgroundColor: COLORS.white_color }}
          behavior={Platform.OS === "ios" ? "padding" : null}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              Keyboard.dismiss();
            }}
          >
            <>
              {/*<OwnerRegistrationStepTabs visibleIndex={1}/>*/}
              <ScrollContainer>
                <MainContainer>
                  <View style={styles.mainContainerStyle}>
                    <CustomTextInput
                      placeholder={STRINGS.own_reg_name_placeholder}
                      returnKeyType={"next"}
                      keyboardType={"default"}
                      // nextRef={"firstName"}
                      // moveToNext={"lastName"}
                      inputvalue={ownerFirstName}
                      ipOnChangeText={(text) =>
                        this.setState({ ownerFirstName: text })
                      }
                    />
                    <Spacer space={0.5} />
                    <CustomTextInput
                      placeholder={STRINGS.reg_last_name_placeholder}
                      returnKeyType={"next"}
                      // nextRef={"lastName"}
                      keyboardType={"default"}
                      inputvalue={ownerLastName}
                      ipOnChangeText={(text) =>
                        this.setState({ ownerLastName: text })
                      }
                    />
                    <Spacer space={0.5} />
                    <CustomTextInput
                      placeholder={STRINGS.own_reg_email_placeholder}
                      returnKeyType={"next"}
                      inputvalue={emailText}
                      keyboardType={"email-address"}
                      ipOnChangeText={(text) =>
                        this.setState({ emailText: text })
                      }
                      // isEditable={this.social_profile === ""}
                    />
                    <Spacer space={0.5} />

                    {this._renderPhoneNumberView()}

                    <Spacer space={0.5} />
                    <CustomTextInput
                      placeholder={STRINGS.own_reg_hotel_name_placeholder}
                      returnKeyType={"next"}
                      inputvalue={hotelName}
                      keyboardType={"default"}
                      ipOnChangeText={(text) =>
                        this.setState({ hotelName: text })
                      }
                    />
                    <Spacer space={0.5} />

                    <View style={styles.imagesContainer}>
                      {this.AddIcon()}
                      {this._renderChooseImage()}
                    </View>
                    <Spacer space={2} />

                    <ShadowViewContainer>
                      <Input
                        inputContainerStyle={{
                          borderBottomColor: "transparent",
                        }}
                        // editable={false}
                        placeholder={"Hotel Address"}
                        placeholderTextColor={COLORS.placeholder_color}
                        value={hotelAddress}
                        onChangeText={(text) =>
                          this.setState({
                            hotelAddress:
                              text.trim() && text.replace(/^0+/, ""),
                            // isChangedValue: true
                          })
                        }
                        rightIcon={
                          <TouchableOpacity
                            style={{ alignContent: "flex-end" }}
                            onPress={() => this.MapAction()}
                          >
                            <Image
                              style={{
                                height: wp(8),
                                width: wp(8),
                                alignSelf: "center",
                                margin: 5,
                              }}
                              source={ICONS.MAP_ICON}
                            />
                          </TouchableOpacity>
                        }
                        // keyboardType="phone-pad"
                        returnKeyType={"next"}
                        inputStyle={{
                          paddingLeft: wp(0.5), //fontFamily: FONTNAME.RalewayRegular
                          fontSize: FONT.TextSmall_2,
                        }}
                        containerStyle={{
                          height: wp(12.5),
                          alignItems: "center",
                          width: wp("85%"),
                        }}
                      />
                    </ShadowViewContainer>
                    <Spacer space={0.5} />
                    <CustomTextInput
                      placeholder={STRINGS.own_reg_password_placeholder}
                      returnKeyType={"next"}
                      keyboardType={"default"}
                      isSecure={true}
                      inputvalue={passwordText}
                      ipOnChangeText={(text) =>
                        this.setState({ passwordText: text })
                      }
                    />
                    <Spacer space={0.5} />
                    <CustomTextInput
                      placeholder={STRINGS.own_reg_confirm_password_placeholder}
                      ipOnChangeText={(text) =>
                        this.setState({ confirmPassword: text })
                      }
                      keyboardType={"default"}
                      isSecure={true}
                      inputvalue={confirmPassword}
                    />
                    <Spacer space={0.5} />
                    <ShadowViewContainer>
                      <TouchableOpacity activeOpacity={1}>
                        <Input
                          inputContainerStyle={{
                            borderBottomColor: "white",
                          }}
                          multiline={true}
                          numberOfLines={1}
                          placeholder={"About Hotel"}
                          placeholderTextColor={COLORS.placeholder_color}
                          ref="Detail"
                          value={details}
                          onChangeText={(text) =>
                            this.setState({
                              details: text,
                            })
                          }
                          returnKeyType={"done"}
                          inputStyle={{
                            paddingLeft: 10,
                            fontSize: FONT.TextSmall,
                            fontFamily: FONT_FAMILY.Montserrat,
                          }}
                          containerStyle={{
                            paddingTop: 0,
                            width: wp("85%"),
                            height: hp(Platform.OS === "ios" ? "12%" : "16%"),
                          }}
                          blurOnSubmit={true}
                         
                        />
                      </TouchableOpacity>
                    </ShadowViewContainer>
                    <TouchableOpacity onPress={() => this.NextAction()}>
                      <View style={styles.buttonStyle}>
                        <Text style={styles.buttonText}>
                          {STRINGS.own_reg_next_button}
                        </Text>
                      </View>
                    </TouchableOpacity>
                    <Spacer space={2.5} />
                  </View>
                  <Spacer space={1} />
                  {this._renderCustomLoader()}
                </MainContainer>
              </ScrollContainer>
            </>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </SafeAreaViewContainer>
    );
  }
}
