/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import React from "react";
import {
  View,
  Text,
  Image,
  TextInput,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
  StatusBar,
  Alert,
} from "react-native";
import BaseClass from "../../../../../utils/BaseClass";
import STRINGS from "../../../../../utils/Strings";
import { Spacer } from "../../../../../customComponents/Spacer";
import {
  SafeAreaViewContainer,
  MainContainer,
  ScrollContainer,
  ShadowViewContainer,
} from "../../../../../utils/BaseStyle";
import { ICONS } from "../../../../../utils/ImagePaths";
import styles from "./styles";
import Icons from "react-native-vector-icons/MaterialIcons";
import { ListItem, CheckBox, Header } from "react-native-elements";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import DocumentPicker from "react-native-document-picker";
import { OwnerRegistrationStepTabs } from "../../../../../customComponents/OwnerRegistrationStepTabs";
import { connect } from "react-redux";
import { CommonActions } from "@react-navigation/native";

Icons.loadFont();
import { HotelPolicyAction } from "../../../../../redux/actions/HotelPolicyAction";
// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import ClockIconSVG from "../../../../../../assets/images/Register/clockIconSVG";
import COLORS from "../../../../../utils/Colors";
import { FONT_FAMILY } from "../../../../../utils/Font";
import { FONT } from "../../../../../utils/FontSizes";
import { LeftIcon } from "../../../../../customComponents/icons";
import OrientationLoadingOverlay from "../../../../../customComponents/Loader";
import AsyncStorage from "@react-native-community/async-storage";
// import Pdf from 'react-native-pdf';
import moment from "moment";

const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

class CheckInCheckOut extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      value1: 1,
      checked: false,
      dateTimeVisibility: false,
      currentSelectedData: "",
      inDate: undefined,
      outDate: undefined,
      addPolicyFile: "Add Policy File",
      pdfFile: null,
      pdfFileName: "",
      GeneralInstruction: "",
      isDatePickerOpen: false,
      accessToken: "",
      hotelId: "",
      isModalVisible: false,
    };
  }

  componentDidMount() {
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let hotelData = JSON.parse(result);
        let hotelId = "";
        if (hotelData.hotel_details !== undefined) {
          hotelId = hotelData.hotel_details.id;
        } else {
          hotelId = hotelData.id;
        }
        this.setState({
          accessToken: hotelData.access_token,
          hotelId: hotelId,
        });
      }
    });
    // let params = this.props.route.params;
    // this.setState({accessToken: params.accessToken, hotelId: params.hotelId})
  }

  componentWillMount() {
    let { checked } = this.state;
    this.setState({ checked: false });
  }

  handleChange = () => {
    let { checked } = this.state;
    checked = !checked;
    this.setState({ checked });
  };

  //***************************** props will receive here *****************************/
  componentWillReceiveProps(nextProps, nextContext) {
    console.log("next props", nextProps);
    this.hideDialog();
    const { navigation } = this.props;
    const { hotelPolicyResponse } = nextProps.policyUpdateState;
    if (hotelPolicyResponse !== undefined && hotelPolicyResponse !== null) {
      const { response, message, status } = hotelPolicyResponse;
      if (status === 200) {
        this.hideDialog();
        console.log("response is-----------------", response);
        AsyncStorage.setItem(STRINGS.loginData, JSON.stringify(response));
        navigation.dispatch(
          CommonActions.reset({
            index: 0,
            routes: [{ name: "hotelHomeScreen" }],
          })
        );
      } else if (status === 111) {
        this.hideDialog();
        if (message.description !== undefined) {
          this.showToastAlert("Description field can not be empty.");
        }
      } else {
        this.hideDialog();
      }
    } else {
      this.hideDialog();
    }
  }

  //*****************************End props will receive here *****************************/

  //**************************** check in button clicked ******************/
  CheckIn = () => {
    let { dateTimeVisibility, isDatePickerOpen } = this.state;
    dateTimeVisibility = true;
    this.setState({
      dateTimeVisibility: dateTimeVisibility,
      isDatePickerOpen: true,
    });
    this.setState({ currentSelectedData: "In" });
  };
  //**************************** check Out button clicked ******************/

  CheckOut = () => {
    let { dateTimeVisibility } = this.state;
    dateTimeVisibility = true;
    this.setState({
      dateTimeVisibility: dateTimeVisibility,
      isDatePickerOpen: true,
    });

    this.setState({ currentSelectedData: "Out" });
  };

  //********************  hide Data picker View ***************** */
  hideDatePicker = () => {
    let { dateTimeVisibility } = this.state;
    dateTimeVisibility = false;
    this.setState({ dateTimeVisibility: false });
  };

  //***************************** Get Date and time  ******************/
  handleConfirm = (time) => {
    let { currentSelectedData } = this.state;

    if (currentSelectedData === "In") {
      this.setState({ dateTimeVisibility: false });
      var time = time.toLocaleTimeString("it-IT");
      let removeSec = time.slice(0, 5);

      this.setState({ inDate: this.formatAMPM(removeSec) });
    } else {
      this.setState({ dateTimeVisibility: false });

      var time = time.toLocaleTimeString("it-IT");
      let removeSec = time.slice(0, 5);
      this.setState({ outDate: this.formatAMPM(removeSec) });
    }
  };

  formatAMPM = (data) => {
   debugger
    if(data!==undefined && data!==null)
    {
      const [time] = data.split(" ");
      let [sHours, minutes] = time.split(":");
  
      // const [sHours, minutes] = time24.match(/([0-9]{1,2}):([0-9]{2})/).slice(1);
      const period = +sHours < 12 ? "AM" : "PM";
      const hours = +sHours % 12 || 12;
  
      return `${hours}:${minutes} ${period}`;

    }
    else {
      return "12:00 PM";
   }
   
  };

  // formatAMPM = (data) => {
  //   if (data !== undefined && data !== null && data !== "") {
  //     const [time] = data.split(" ");

  //     let [hours, minutes] = time.split(":");

  //     var ampm = hours >= 12 ? "PM" : "AM";
  //     hours = hours % 12;
  //     hours = hours ? hours : 12; // the hour '0' should be '12'
  //     minutes = minutes < 10 ? "0" + minutes : minutes;
  //     var strTime = hours + ":" + minutes + " " + ampm;
  //     return strTime;
  //   } else {
  //     return "12:00 PM";
  //   }
  // };

  //******************************** pdf picker  ****************************/

  openDocument = async (url) => {
    if (url !== undefined) {
      const { navigate } = this.props.navigation;
      this.props.navigation.navigate("PdfViewScreen", {
        pdfData: this.state.pdfFile,
      });
    }
  };

  async OpenPdfPicker(pdfFile) {
    // const {isPdfRemoved, pdfFile} = this.state;
    if (pdfFile === null) {
      try {
        let res = await DocumentPicker.pick({
          type: [DocumentPicker.types.pdf],
        });
        this.setState({
          pdfFile: res,
          pdfFileName: res.name,
          isPdfRemoved: false,
        });
        this.showToastSucess("PDF added");
      } catch (err) {
        if (DocumentPicker.isCancel(err)) {
          // User cancelled the picker, exit any dialogs or menus and move on
        } else {
          throw err;
        }
      }
    } else {
      Alert.alert(
        "Hotel App",
        STRINGS.choose_option,
        [
          { text: "Cancel", style: "cancel" },
          {
            text: "Open PDF",
            onPress: () => {
              this.openDocument(pdfFile);
            },
          },
          {
            text: "Remove PDF",
            onPress: () => this.removePdfFile(),
          },
        ],
        { cancelable: false }
      );
    }
  }

  removePdfFile = () => {
    const { isPdfRemoved } = this.state;
    this.showToastAlert("PDF removed successfully");
    this.setState({ isPdfRemoved: true, pdfFile: null, pdfFileName: "" });
  };

  OnSubmit = () => {
    const {
      checked,
      inDate,
      outDate,
      pdfFile,
      pdfFileName,
      GeneralInstruction,
      accessToken,
      hotelId,
    } = this.state;
    console.log("accessToken", hotelId);
    if (inDate === undefined) this.showToastAlert("Please enter Check In date");
    else if (outDate === undefined)
      this.showToastAlert("Please enter Check Out date");
    else if (pdfFile === null)
      this.showToastAlert("Please upload your policy file");
    else if (!checked)
      this.showToastAlert("Please accept terms and conditions");
    else {
      this.props.policyUpdate({
        check_in: inDate,
        check_out: outDate,
        description: GeneralInstruction,
        file: pdfFile,
        accessToken: accessToken,
        hotelId,
      });
      this.showDialog();
    }
  };

  /*
// =============================================================================================
// Render method for Header
// =============================================================================================
*/
  _renderHeader() {
    const { navigation } = this.props;
    const { from } = this.props.route.params;
    return (
      <Header
        backgroundColor={COLORS.backGround_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={
          from !== "Splash" && <LeftIcon onPress={() => navigation.goBack()} />
        }
        centerComponent={{
          text: STRINGS.own_reg_header_title,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  _renderCheckInOutView = () => {
    const { dateTimeVisibility, inDate, outDate } = this.state;
    return (
      <View style={styles.CheckInCheckOutTextStyle}>
        <View
          style={{ flexDirection: "column" }}
          onStartShouldSetResponder={() => this.CheckIn()}
        >
          <Text style={styles.TextCheckin}>{STRINGS.check_in}</Text>
          <Spacer space={1} />
          <ShadowViewContainer>
            <View style={styles.CheckInFrameStyle}>
              <Spacer row={1} />
              <ClockIconSVG width={wp(5)} height={wp(5)} />
              <Text style={styles.timeTextStyle}>{inDate}</Text>
              <Image style={styles.clockIconStyle} source={ICONS.DOWN_ARROW} />
            </View>
          </ShadowViewContainer>
          <DateTimePickerModal
            isVisible={dateTimeVisibility}
            titleIOS="Pick a time"
            mode="time"
            locale="en_GB" // Use "en_GB" here
            date={new Date()}
            onConfirm={(time) => this.handleConfirm(time)}
            onCancel={this.hideDatePicker}
          />
        </View>

        <View
          style={{ flexDirection: "column" }}
          onStartShouldSetResponder={() => this.CheckOut()}
        >
          <Text style={styles.TextCheckin}>{STRINGS.check_out}</Text>
          <Spacer space={1} />
          <ShadowViewContainer>
            <View style={styles.CheckInFrameStyle}>
              <Spacer row={1} />
              <ClockIconSVG width={wp(5)} height={wp(5)} />
              <Text style={styles.timeTextStyle}>{outDate}</Text>
              <Image style={styles.clockIconStyle} source={ICONS.DOWN_ARROW} />
            </View>
          </ShadowViewContainer>
        </View>
      </View>
    );
  };

  termAndConditionAction = () => {
    const { navigate } = this.props.navigation;
    navigate("MyWebComponent");
  };

  _renderPolicyAndDocView = () => {
    const { checked, pdfFile } = this.state;
    return (
      <View style={styles.PolicyDocumentView}>
        <View style={{ marginLeft: wp("5%") }}>
          <Text style={styles.PolicyTextStyle}>{STRINGS.policy}</Text>
          <Spacer space={2} />
          <TextInput
            style={styles.GeneralInstructionTextStyle}
            placeholder=" General Instruction"
            multiline={true}
            // numberOfLines={2}
            onChangeText={(text) => this.setState({ GeneralInstruction: text })}
          />

          <Spacer space={2} />
          <View style={styles.AddPolicyFileView}>
            <TouchableOpacity onPress={() => this.OpenPdfPicker(pdfFile)}>
              <Image
                source={ICONS.PDF_ICON}
                style={{
                  width: wp(20),
                  height: wp(20),
                }}
              />
            </TouchableOpacity>
            {pdfFile !== null && (
              <TouchableOpacity
                onPress={() => this.removePdfFile()}
                style={{
                  position: "absolute",
                  marginTop: wp(-2),
                  marginLeft: wp(12),
                }}
              >
                <Image
                  source={ICONS.CLOSE_WINDOW}
                  style={{
                    position: "absolute",
                    width: wp(8),
                    height: wp(8),
                    backgroundColor: "pink",
                  }}
                />
              </TouchableOpacity>
            )}
            <Spacer row={1.5} />
            <Text style={styles.AddPolicyFileText}>
              {STRINGS.addPolicyFile}
            </Text>
          </View>
          <Spacer space={2} />

          <View style={styles.AddPolicyFileView}>
            <View
              style={{
                flexDirection: "row",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <View>
                <CheckBox
                  title={""}
                  textStyle={{
                    fontFamily: FONT_FAMILY.Montserrat,
                    fontSize: FONT.TextSmall,
                    fontWeight: "normal",
                  }}
                  checked={checked}
                  onPress={() => this.handleChange()}
                />
              </View>
              <TouchableOpacity
                onPress={() => this.termAndConditionAction()}
                underlayColor="white"
              >
                <Text style={styles.termConditionTextStyle}>
                  {STRINGS.TERM_CONDITION_TEXT}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <Spacer space={2} />

          <TouchableOpacity onPress={() => this.OnSubmit()}>
            <View style={styles.buttonStyle}>
              <Text style={styles.buttonText}>
                {STRINGS.own_reg_next_button}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  render() {
    const { isPdfOpend } = this.state;
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <DismissKeyboard>
          <MainContainer>
            <Spacer space={2} />
            {/*<OwnerRegistrationStepTabs visibleIndex={3} navigate={this.props.navigation}/>*/}
            <ScrollContainer style={{ backgroundColor: COLORS.white_color }}>
              <View style={{ width: wp(100) }}>
                {this._renderCheckInOutView()}
                <Spacer space={2} />
                {this._renderPolicyAndDocView()}
              </View>
            </ScrollContainer>
            {this._renderCustomLoader()}
          </MainContainer>
        </DismissKeyboard>
      </SafeAreaViewContainer>
    );
  }
}

const mapStateToProps = (state) => ({
  policyUpdateState: state.HotelPolicyReducer,
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    policyUpdate: (payload) => dispatch(HotelPolicyAction(payload)),
  };
};

// ----------------------------------------

export default connect(mapStateToProps, mapDispatchToProps)(CheckInCheckOut);
