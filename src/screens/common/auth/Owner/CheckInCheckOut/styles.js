import React from "react";
import styled from "styled-components";
import COLORS from "../../../../../utils/Colors";

import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {StyleSheet} from "react-native";
import {FONT_FAMILY} from "../../../../../utils/Font";
import {FONT} from "../../../../../utils/FontSizes";

const styles = StyleSheet.create({

    containerStyle: {
        alignSelf: "center",
        width: wp("100%"),
        padding: wp("8%"),
        flex: 1,
    },


    timeTextStyle: {
        padding: 7,
        width: wp('25%')
    },

    CheckInCheckOutTextStyle: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: COLORS.backGround_color
    },

    CheckInFrameStyle:
        {
            flexDirection: "row",
            borderRadius: 10,
            // borderWidth: 0.2,
            paddingVertical: wp(1),
            borderColor: COLORS.textColor,
            // marginRight:5,
            backgroundColor: COLORS.white_color,
            justifyContent: 'center',
            alignItems: 'center'
        },

    TextCheckin:
        {
            // padding: 5,
            color: COLORS.textColor,
            fontFamily: FONT_FAMILY.Montserrat,
            fontSize: FONT.TextSmall_2
        },

    clockIconStyle: {
        alignSelf: 'center',
        marginLeft: 10,
        marginRight: 10
    },

    PolicyDocumentView: {
        // backgroundColor: 'red',
    },
    PolicyTextStyle: {
        fontFamily: FONT_FAMILY.Montserrat,
        fontSize: FONT.TextMedium,
        borderColor: "white",
        // marginLeft: wp('5%'),
        // backgroundColor: 'white',
        fontWeight: '600'
    },


    GeneralInstructionTextStyle: {
        alignSelf:'center',
        borderRadius: 8,
        borderWidth: 1,
        borderColor: "gray",
        paddingVertical: wp(2),
        width: wp("90%"),
        // alignSelf: 'center',
        fontFamily: FONT_FAMILY.Montserrat,
        fontSize: FONT.TextSmall,
    },


    AddPolicyFileView: {

        flexDirection: 'row',
        justifyContent: 'flex-start',
        // marginLeft: wp('5%')

    },

    AddPolicyFileText: {

        // marginLeft: wp('5'),
        alignSelf: 'center',
        fontFamily: FONT_FAMILY.Montserrat,
        fontSize: FONT.TextSmall_2,

    },
    buttonStyle: {
        marginVertical: 10,
        alignSelf: "center",
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.app_theme_color,
        height: wp('12%'),
        width: wp('80%'),
        borderRadius: wp('12%') / 2,
        marginBottom: 40
    },
    termConditionTextStyle:{

        fontSize:wp('4%'), alignSelf:'center', fontWeight:'bold',color:COLORS.greyButton_color,marginLeft:wp('-5%')
      
      },
    buttonText: {
        fontSize: 16,
        fontStyle: "normal",
        fontWeight: "500",
        lineHeight: 20,
        display: "flex",
        color: '#FFFFFF',
        textAlign: 'center',
    },
    pdfIconStyle: {
        alignSelf: 'center',
        marginLeft: 10,
        marginRight: 10,
        height:wp('20%'),
        width:wp('15%')
    },
    cancelPdfStyle:{
        position:'absolute',
        height:wp(10),
        width:wp(10),
        // marginLeft:wp('-5%'),
        // marginTop:wp('-2%')

    },
  pdfModelStyle:{
    height: hp("100%"),
    width: wp("100%"),
    marginTop: wp("10%"),

  },
  closeModelIconStyle:{
    position: "absolute",
    width: wp(12),
    height: wp(12),
    marginTop: 10,
    alignSelf: "flex-end",
    backgroundColor: "pink",

  }
})

export default styles;
