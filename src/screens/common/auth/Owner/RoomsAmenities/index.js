/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */
import React from "react";
// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

import BaseClass from "../../../../../utils/BaseClass";
import STRINGS from "../../../../../utils/Strings";
import { Spacer } from "../../../../../customComponents/Spacer";
import COLORS from "../../../../../utils/Colors";
import { LeftIcon } from "../../../../../customComponents/icons";
import { FONT } from "../../../../../utils/FontSizes";
import {
  SafeAreaViewContainer,
  MainContainer,
  ShadowViewContainer,
  ScrollContainer,
} from "../../../../../utils/BaseStyle";
import { FONT_FAMILY } from "../../../../../utils/Font";
import OrientationLoadingOverlay from "../../../../../customComponents/Loader";
import { ICONS } from "../../../../../utils/ImagePaths";
import styles from "../../../../common/auth/Owner/RoomsAmenities/roomsAmenitiesStyle";
import {
  GetRoomTypeAction,
  GetHotelAminitiesAction,
  GetAllHotelServiceAction,
  GetRoomComplemantryServiceAction,
} from "../../../../../redux/actions/HotelServices";
import { RegisterHotelsAction } from "../../../../../redux/actions/RegisterHoelsAction";

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import {
  View,
  Text,
  Image,
  FlatList,
  TextInput,
  Alert,
  TouchableOpacity,
  Keyboard,
  Platform,
  Animated,
  TouchableWithoutFeedback,
  ImageBackground,
  StatusBar,
  KeyboardAvoidingView,
} from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Header } from "react-native-elements";
import * as _ from "lodash";
import ImagePicker from "react-native-image-crop-picker";
import { connect } from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";
import { Dropdown } from "react-native-material-dropdown";
import NumericInput from "react-native-numeric-input";
import Icons from "react-native-vector-icons/MaterialIcons";
import { CheckBox } from "react-native-elements";
import RadioForm from "react-native-simple-radio-button";
import SectionedMultiSelect from "../../../../../localModule/react-native-sectioned-multi-select";
import TimeSlotModelView from "../../../../hotelOwner/addHotelRoom/component/TimeSlotModel";

Icons.loadFont();

class RoomsAmenities extends BaseClass {
  constructor(props) {
    super(props);
    this.state = {
      hotelId: undefined,
      accessToken: undefined,
      value1: 1,
      checked: [],
      hideLabel: false,
      selectedAminities: [],
      ViewArray: [],
      textInput: [],
      Disable_Button: false,
      amnitiesData: [],
      dataRoomType: [],
      items: [],
      radioButton: [],
      HotelOtherSerivces: [],
      selectedItems: [],
      isNewView: false,
      isAddShow: true,
      imagesList: [],
      otherService: 1,
      authToken: undefined,
      timSlotModelView: false,
      currentIndexClicked: undefined,
      data: [
        {
          defaultdiscountBoxColor: COLORS.app_theme_color,
          timeSlot: "Time Slot",
          RoomType: "",
          RoomImages: [],
          Regular_price: undefined,
          Discount_Price: undefined,
          IsDiscountApplied: false,
          Numberofguest: 1,
          AddComplimentery_service: [],
          isMaxPicComplete: false,
        },
      ],
      checkedAmenitiesData: [],
    };
    this.animatedValue = new Animated.Value(0);

    this.Array_Value_Index = 0;
  }

  //************************  component will Mount  **************************** */

  componentDidMount() {
    AsyncStorage.getItem(STRINGS.loginToken, (error, result) => {
      if (result !== undefined && result !== null) {
        let loginToken = JSON.parse(result);
        this.setState({
          accessToken: loginToken,
        });
        this.GetRoomType(loginToken);
        this.GetAmenitiesList(loginToken);
        this.GetOtherServiceList(loginToken);
        this.GetRoomComplemantryService(loginToken);
      }
    });
    AsyncStorage.getItem(STRINGS.loginData, (error, result) => {
      if (result !== undefined && result !== null) {
        let hotelData = JSON.parse(result);
        let hotelId = "";
        if (hotelData.hotel_details !== undefined) {
          hotelId = hotelData.hotel_details.id;
        } else {
          hotelId = hotelData.id;
        }
        this.setState({
          authToken: hotelData.access_token,
          hotelId: hotelId,
        });
      }
    });

    // let params = this.props.route.params;
    // this.setState({accessToken: params.accessToken, hotelId: params.hotelId, authToken: params.authToken})
    //
    // this.GetRoomType(params.authToken);
    // this.GetAmenitiesList(params.authToken);
    // this.GetOtherServiceList(params.authToken);
    // this.GetRoomComplemantryService(params.authToken);
  }

  //*********************************component will receive props ********************** */

  componentWillReceiveProps(nextProps, nextContext) {
    // hotelRoomResponse
    const { accessToken, hotelId } = this.state;
    const { navigate } = this.props.navigation;
    const { hotelRoomResponse } = nextProps.hotelRoomState;
    if (hotelRoomResponse !== undefined && hotelRoomResponse !== null) {
      const { response, message, status } = hotelRoomResponse;
      if (status === 200) {
        this.hideDialog();
        if (response !== undefined && response !== null) {
          AsyncStorage.setItem(STRINGS.loginData, JSON.stringify(response));
          navigate("CheckInCheckOut", {
            accessToken,
            hotelId,
            from: "roomsAmenities",
          });
        }
      } else if (status === 111) {
        this.hideDialog();
        if (message.discounted_price !== undefined) {
          this.hideDialog();
          this.showToastAlert(message.discounted_price[0]);
        } else {
          this.hideDialog();
          console.warn(message);
        }
      } else {
        this.showToastAlert(
          "Image added exceeds size limit. Please choose image of smaller size"
        );
        this.hideDialog();
      }
    } else {
      this.showToastAlert(
        "Image added exceeds size limit. Please choose image of smaller size"
      );
      this.hideDialog();
    }
  }

  //********************************* end component will receive props ********************** */

  //************************  Get Room Type **************************** */

  GetRoomType = (tokenAuth) => {
    let payload = {
      authToken: tokenAuth,
    };
    GetRoomTypeAction(payload, (res) => this.RoomTypeApiResponse(res));
  };

  RoomTypeApiResponse(response1) {
    if (response1.response !== undefined && response1.status === 200) {
      this.setState({ dataRoomType: response1.response });
    } else this.showToastAlert("server error");
  }

  //************************  Get Amenities List **************************** */

  GetAmenitiesList = (tokenAuth) => {
    let payload = {
      authToken: tokenAuth,
    };
    GetHotelAminitiesAction(payload, (res) => this.AminitiesResponse(res));
  };

  AminitiesResponse(response2) {
    if (response2.response !== undefined && response2.status === 200) {
      this.setState({
        amnitiesData: response2.response,
        checkedAmenitiesData: response2.response,
      });
    } else this.showToastAlert("server error");
  }

  //************************  Hotel Services **************************** */
  GetOtherServiceList = (tokenAuth) => {
    let payload = {
      authToken: tokenAuth,
    };
    GetAllHotelServiceAction(payload, (res) =>
      this.OtherHotelServicesResponse(res)
    );
  };

  timeSlotAction = (hour) => {
    const { currentIndexClicked, timSlotModelView } = this.state;

    if (currentIndexClicked !== undefined)
      this.setTimeSlot(hour, currentIndexClicked);
    this.setState({ timSlotModelView: false });
  };

  OtherHotelServicesResponse(response3) {
    if (response3.response !== undefined && response3.status === 200) {
      this.setState({ HotelOtherSerivces: response3.response });
    } else this.showToastAlert("server error");
  }

  //************************ End Hotel Services **************************** */

  //*************************** Room complementary services ***********************/
  GetRoomComplemantryService = (tokenAuth) => {
    let payload = {
      authToken: tokenAuth,
    };
    GetRoomComplemantryServiceAction(payload, (res) =>
      this.RoomComplimentaryServiceResponse(res)
    );
  };

  RoomComplimentaryServiceResponse(response4) {
    if (response4.response !== undefined && response4.status === 200) {
      // this.setState({ items: response4.response });
      let fruit = {
        name: "Complimentary Serivces",
        id: 0,
        children: [],
      };
      this.state.items.push(fruit);
      response4.response.forEach((element) => {
        var data = {
          name: element.name,
          id: element.id,
        };
        fruit.children.push(data);
      });
    } else {
      this.showToastAlert("server error");
    }
  }

  //***************************End Room complementary services ***********************/

  //************************  component will Mount  **************************** */
  componentWillMount() {
    let { amnitiesData, checked } = this.state;
    let intialCheck = amnitiesData.map((x) => false);
    this.setState({ checked: intialCheck });

    //room service
  }

  //******************************* handle change  action */
  handleChange = (index, itemId) => {
    const { amnitiesData, checkedAmenitiesData, checked } = this.state;
    _.map(amnitiesData, (item, cIndex) => {
      if (cIndex === index) {
        checkedAmenitiesData[cIndex].checked = !checked[index];
      }
    });
    checked[index] = !checked[index];
    this.setState({ checked });
  };

  //radio button handling
  toggleRadio = (index) => {
    let { checked } = this.state;
    checked[index] = !checked[index];
    this.setState({ checked });
  };

  // ****************** on selected Item  *********************

  onSelectedItemsChange = (index, selectedItems) => {
    const { data } = this.state;

    _.map(data, (item, index1) => {
      if (index === index1) {
        item.AddComplimentery_service = selectedItems;
      }
    });
    this.setState({ data });
  };

  //********************* add new View function ************ */
  Add_New_View_Function = () => {
    // var tempOtherService = [];
    var newlyCreatedData = {
      defaultdiscountBoxColor: COLORS.app_theme_color,
      RoomType: undefined,
      RoomImages: [],
      Regular_price: undefined,
      Discount_Price: undefined,
      IsDiscountApplied: false,
      Numberofguest: 1,
      AddComplimentery_service: [],
    };

    const { data } = this.state;
    data.push(newlyCreatedData);

    this.setState({ data: data });
  };

  //************************************ gallery code ******************************** */
  _renderImageList = (item, index, outerIndex) => {
    return (
      <>
        <ImageBackground
          style={styles.imageStyle}
          source={{ uri: item !== undefined ? item.item.completeData.uri : "" }}
          imageStyle={{ borderRadius: wp("20%") / 2 }}
          resizeMode={"cover"}
        >
          <TouchableWithoutFeedback
            onPress={() => this.CrossAction(item, item.index, outerIndex)}
          >
            <Image
              style={{
                height: wp("8%"),
                width: wp("8%"),
                alignSelf: "flex-end",
                position: "absolute",

                top: wp("-1%"),
                // height: 30,
                // width: 30,
              }}
              source={ICONS.CLOSE_WINDOW}
            />
          </TouchableWithoutFeedback>
        </ImageBackground>
      </>
    );
  };

  // =============================================================================================
  // Click on Row of FlatList //// separator
  // =============================================================================================

  CrossAction(i, index, parentIndex) {
    //remove value from imagelist

    const { data } = this.state;
    let tempArray = [];
    data[parentIndex].RoomImages.splice(index, 1);
    // tempArray = imagesList;
    // this.setState({ imagesList: tempArray });
    this.setState({
      refresh: !this.state.refresh,
    });
    if (data[parentIndex].RoomImages.length !== 10)
      this.setState({ isAddShow: true });
  }

  AddImageAction(index) {
    const { data } = this.state;

    const { imagesList } = this.state;
    ImagePicker.openPicker({
      compressImageQuality: 0.2,
      includeBase64: true,
      multiple: true,
      maxFiles: 10,
      mediaType: "photo",
    }).then((response) => {
      let tempArray = [];
      response.forEach((item) => {
        let image = {
          completeData: item,
          uri: item.path,
        };

        tempArray.push(image);
      });

      let count = data[index].RoomImages.length + response.length;

      if (count <= 10) {
        _.map(data, (item, index1) => {
          if (index === index1) {
            // item.RoomImages = tempArray;
            //new code
            for (let i = 0; i < tempArray.length; i++) {
              let image = {
                completeData: tempArray[i],
                uri: tempArray[i].path,
              };

              item.RoomImages.push(image);
            }
            //end
          }

          this.setState({
            refresh: !this.state.refresh,
          });
        });
      } else {
        this.showToastAlert(STRINGS.ONLY_TEN_IMAGES);

        data[index].isMaxPicComplete = true;
        return;
        //alert show and remove image icon from that index
      }
    });
  }

  onChangeDropdown = (value, id, index) => {
    const { data } = this.state;

    _.map(data, (item, index1) => {
      if (index === index1) {
        item.RoomType = id + 1;
      }
    });
    this.setState({
      data,
      hideLabel: true,
    });
  };

  numberOfGuest = (num, index) => {
    const { data } = this.state;
    _.map(data, (item, index1) => {
      if (index === index1) {
        item.Numberofguest = num;
      }
    });
    this.setState({ data });
  };

  //*******************************Discount Amount ***************** */

  onChangeText = (text, index) => {
    const { data } = this.state;
    _.map(data, (item, index1) => {
      if (index === index1) {
        item.Regular_price = text;
        item.IsDiscountApplied = false;
        item.defaultdiscountBoxColor=COLORS.app_theme_color
      }
    });
    this.setState({
      data,
    });
  };

  discountAmount = (index) => {
    const { data } = this.state;

    if (data[index].IsDiscountApplied === false) {
      _.map(data, (item, index1) => {
        if (index === index1) {
          item.Discount_Price =
            parseInt(item.Regular_price) -
            (parseInt(item.Regular_price) * 20) / 100;
          item.defaultdiscountBoxColor = COLORS.failure_Toast;
        }
        data[index].IsDiscountApplied = true;
        this.setState({ data: data });
      });
    } else alert("Discount already applied for this room");
  };

  _itemSeparator = () => <View style={{ width: wp("5%") }} />;

  AddIcon = (index) => {
    return this.state.isAddShow ? (
      <View style={{ justifyContent: "center" }}>
        <TouchableWithoutFeedback onPress={() => this.AddImageAction(index)}>
          <ImageBackground style={styles.imgOuterView}>
            <TouchableWithoutFeedback
              onPress={() => this.AddImageAction(index)}
            >
              <Image
                style={{
                  height: wp("13%"),
                  width: wp("13%"),
                  alignSelf: "center",
                }}
                source={ICONS.ADD_IMAGE_ICON}
              />
            </TouchableWithoutFeedback>
          </ImageBackground>
        </TouchableWithoutFeedback>
      </View>
    ) : (
      <View />
    );
  };
  //****************************************End gallery code  ****************************/

  //*******************************remove View ***************** */
  closeRoomView = (closeWindowIndex) => {
    const list = this.state.data;

    list.splice(closeWindowIndex, 1);
    this.setState({ data: list });
  };

  //*********************end add new View function ************ */

  //*********************** OnSubmit  *************************/
  OnSubmit = () => {
    const {
      data,
      checked,
      amnitiesData,
      selectedAminities,
      otherService,
      hotelId,
      accessToken,
    } = this.state;
    //get id fro checkd

    let checkValidData = 0;

    let AminitiesData = [];
    for (let i = 0; i < checked.length; i++) {
      if (checked[i] === true) {
        for (let j = 0; j < amnitiesData.length; j++) {
          AminitiesData.push(amnitiesData[i].id);
          break;
        }
      }
    }

    let otherServiceTempArray = [];

    otherServiceTempArray.push(otherService);
    // if(data.length ===1)
    // {

    // }
    _.map(data, (item, index) => {
      if (data[index].RoomType === undefined || data[index].RoomType === "")
        this.showToastAlert("please select room type");
      else if (data[index].Regular_price === undefined)
        this.showToastAlert("please enter regular price");
      else if (data[index].Numberofguest === undefined)
        this.showToastAlert("please enter Number of guest");
      else if (data[index].RoomImages.length === 0)
        this.showToastAlert("please upload atleast one room image");
      else if (data[index].IsDiscountApplied === false) {
        this.showToastAlert("please choose discount");
      } else if (
        data[index].RoomType === 6 &&
        data[index].timeSlot === "Time Slot"
      ) {
        this.showToastAlert("please select hour in room " + index);
      } else {
        checkValidData = checkValidData + 1;
      }
    });

    if (checkValidData === data.length) {
      this.props.registerHotel({
        data: data,
        accessToken: accessToken,
        HotelId: hotelId,
        checked: AminitiesData,
        otherService: otherServiceTempArray,
      });
      this.showDialog();
    }
  };

  /*
// =============================================================================================
// Render method for Header
// =============================================================================================
*/
  _renderHeader() {
    const { navigation } = this.props;
    const { from } = this.props.route.params;
    return (
      <Header
        backgroundColor={COLORS.backGround_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={
          from !== "Splash" && <LeftIcon onPress={() => navigation.goBack()} />
        }
        centerComponent={{
          text: STRINGS.own_reg_header_title,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  _renderCrossIcon = (index) => {
    return (
      <TouchableOpacity
        style={{ alignSelf: "flex-end", marginRight: wp(2) }}
        onPress={() => this.closeRoomView(index)}
        hitSlop={{
          top: 20,
          bottom: -20,
          left: 50,
          right: 50,
        }}
      >
        <Image
          source={ICONS.CLOSE_WINDOW}
          style={{
            width: wp(10),
            height: wp(10),
          }}
        />
      </TouchableOpacity>
    );
  };
  //===========================time slot ===========================

  itemSlotClikeed = (index) => {
    const { timSlotModelView } = this.state;

    this.setState({ timSlotModelView: true, currentIndexClicked: index });
    // Alert.alert(
    //   "Time Slot",
    //   "",
    //   [
    //     {
    //       text: "1 hour",
    //       onPress: () => this.setTimeSlot("1", index),
    //     },
    //     {
    //       text: "2 hour",
    //       onPress: () => this.setTimeSlot("2", index),
    //     },
    //     {
    //       text: "3 hour",
    //       onPress: () => this.setTimeSlot("3", index),
    //     },
    //     {
    //       text: "4 hour",
    //       onPress: () => this.setTimeSlot("4", index),
    //     },
    //     {
    //       text: "5 hour",
    //       onPress: () => this.setTimeSlot("5", index),
    //     },
    //     {
    //       text: "6 hour",
    //       onPress: () => this.setTimeSlot("6", index),
    //     },
    //     {
    //       text: "7 hour",
    //       onPress: () => this.setTimeSlot("7", index),
    //       // this.setState({ starSelected: 1, selectStarMarginLeft: wp("13%") }),
    //     },
    //     {
    //       text: "8 hour",
    //       onPress: () => this.setTimeSlot("8", index),
    //     },
    //     {
    //       text: "9 hour",
    //       onPress: () => this.setTimeSlot("9", index),
    //     },
    //     {
    //       text: "10 hour",
    //       onPress: () => this.setTimeSlot("10"),
    //     },
    //     {
    //       text: "11 hour",
    //       onPress: () => this.setTimeSlot("11", index),
    //     },
    //     {
    //       text: "12 hour",
    //       onPress: () => this.setTimeSlot("12", index),
    //     },
    //   ],
    //   { cancelable: false }
    // );
  };

  setTimeSlot = (timeInHour, index) => {
    const { data } = this.state;
    data[index].timeSlot = timeInHour;
    this.setState({ data });
  };

  _renderRoomView = (item, index) => {
    const { items, hideLabel, dataRoomType } = this.state;
    let tempRoomType = [];

    dataRoomType.forEach((item) => {
      let aa = {
        value: item.name,
        id: item.id,
      };

      tempRoomType.push(aa);
    });

    return (
      <>
        <View style={styles.RepeatRoomView}>
          {index !== 0 && this._renderCrossIcon(index)}

          {/*<View style={{alignSelf: "center",}}>*/}
          <ShadowViewContainer>
            <Dropdown
              containerStyle={{
                width: wp("92%"),
                paddingHorizontal: wp("5%"),

                // backgroundColor: 'red',
              }}
              label={!hideLabel && "Room Type"}
              shadeOpacity={12}
              data={tempRoomType}
              onChangeText={(value, id) =>
                this.onChangeDropdown(value, id, index)
              }
            />
          </ShadowViewContainer>
          {/*</View>*/}
          <Spacer space={1} />
          <View style={styles.OwnerRoomDetailView}>
            <ShadowViewContainer>
              <View style={styles.imagesContainer}>
                {this.AddIcon(index)}
                <FlatList
                  horizontal
                  //inverted
                  showsHorizontalScrollIndicator={false}
                  style={{
                    width: 0,
                    paddingRight: wp("2%"),
                    marginRight: wp(5),
                    // backgroundColor:'orange'
                  }}
                  data={item.RoomImages}
                  renderItem={(item1, index1) =>
                    this._renderImageList(item1, index1, index)
                  }
                  ItemSeparatorComponent={this._itemSeparator}
                  extraData={this.state.refresh}
                  keyExtractor={(item, index1) => index1}
                  selected={this.state.selected}
                />
              </View>
              {/*<Spacer space={2}/>*/}
              <View
                style={{
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                {item.RoomType === 6 && (
                  <View style={styles.roomOuterView}>
                    <TouchableOpacity
                      onPress={() => this.itemSlotClikeed(index)}
                    >
                      {item.timeSlot === "Time Slot" ? (
                        <Text style={styles.regularPriceText}>
                          {item.timeSlot}
                        </Text>
                      ) : (
                        <Text style={styles.regularPriceText}>
                          {item.timeSlot} hour
                        </Text>
                      )}
                    </TouchableOpacity>

                    <Spacer space={1.5} />
                  </View>
                )}

                <TextInput
                  style={{
                    width: wp(85),
                    borderWidth: 2,
                    borderRadius: 12,
                    borderColor: "#E8E8E8",
                    paddingVertical: wp(2.5),
                    paddingLeft: wp(2),
                    color: COLORS.placeholder_color,
                    fontFamily: FONT_FAMILY.Montserrat,
                  }}
                  placeholder="Regular Price"
                  onChangeText={(text) => this.onChangeText(text, index)}
                  keyboardType={"numeric"}
                  value={item.Regular_price}
                />
              </View>
              <Spacer space={1.5} />

              <View
                style={{
                  // paddingHorizontal: wp(5),
                  flexDirection: "row",
                  justifyContent: "space-around",
                  alignItems: "center",
                }}
              >
                <View
                  style={{
                    width: wp(45),
                    borderWidth: 2,
                    borderRadius: 12,
                    borderColor: "#E8E8E8",
                  }}
                >
                  <Text
                    style={{
                      paddingLeft: wp(2),
                      paddingVertical: wp(3.2),
                      fontSize: 13,
                      fontFamily: FONT_FAMILY.Montserrat,
                    }}
                  >
                    {item.Discount_Price !== undefined
                      ? (item.Discount_Price + "\u20B9")
                      : "Discounted Price"}
                  </Text>
                </View>
                <TouchableOpacity
                  disabled={item.Regular_price === undefined && true}
                  onPress={() => this.discountAmount(index)}
                >
                  <View style={[styles.discountView,{borderColor:item.defaultdiscountBoxColor}]}>
                    <Text style={[styles.discountText,{borderColor:item.defaultdiscountBoxColor,color:item.defaultdiscountBoxColor}]}>
                 
                      20% discount
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <Spacer space={1.5} />
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-around",
                  alignItems: "center",
                }}
              >
                <View
                  style={{
                    width: wp(45),
                    borderWidth: 2,
                    borderRadius: 12,
                    borderColor: "#E8E8E8",
                    fontFamily: FONT_FAMILY.Montserrat,
                  }}
                >
                  <Text
                    style={{
                      paddingLeft: wp(2),
                      paddingVertical: wp(3.2),
                      fontSize: 13,
                      fontFamily: FONT_FAMILY.Montserrat,
                    }}
                  >
                    {STRINGS.no_of_Guests}
                  </Text>
                </View>

                <NumericInput
                  type="up-down"
                  // totalHeight={wp("10%")}
                  totalWidth={wp("30%")}
                  value={item.Numberofguest}
                  minValue={1}
                  containerStyle={{
                    borderWidth: 2,
                    color: COLORS.placeholder_color,
                  }}
                  maxValue={10}
                  textColor="#828282"
                  onChange={(value) => this.numberOfGuest(value, index)}
                  rounded
                  inputStyle={{ height: wp(11), width: wp(15) }}
                />
              </View>

              <Spacer space={2} />
              <SectionedMultiSelect
                styles={{
                  selectToggle: {
                    borderWidth: wp(0.4),
                    borderColor: "#ddd",
                    borderRadius: wp(2),
                    alignSelf: "center",
                    paddingVertical: wp(2),
                    paddingHorizontal: wp(2),
                    width: wp(85),
                  },
                }}
                iconKey={"arrow-drop-down"}
                items={items}
                uniqueKey="id"
                subKey="children"
                selectText="Add Complimentary Services"
                showDropDowns={true}
                readOnlyHeadings={true}
                onSelectedItemsChange={this.onSelectedItemsChange.bind(
                  this,
                  index
                )}
                selectedItems={item.AddComplimentery_service}
              />
              <Spacer space={2} />
            </ShadowViewContainer>
          </View>
          <Spacer space={1} />
        </View>
      </>
    );
  };

  _renderAmenitiesView = (item, index) => {
    const { checked } = this.state;
    return (
      <>
        <View style={{ flexDirection: "column", margin: 1, width: wp(45) }}>
          <CheckBox
            textStyle={{ color: COLORS.placeholder_color }}
            style={styles.checkboxStyle}
            title={item.name}
            containerStyle={{
              borderColor: COLORS.backGround_color,
              backgroundColor: COLORS.backGround_color,
            }}
            onPress={() => this.handleChange(index, item.id)}
            checked={checked[index]}
          />
        </View>
      </>
    );
  };

  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  render() {
    let {
      amnitiesData,
      HotelOtherSerivces,
      data,
      timSlotModelView,
    } = this.state;

    var tempOtherService = [];
    HotelOtherSerivces.forEach((item) => {
      var aa = {
        label: item.name,
        size: "10",
        value: item.id,
      };

      tempOtherService.push(aa);
    });

    //********************************end
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <KeyboardAvoidingView
          style={{ flex: 1, backgroundColor: COLORS.white_color }}
          behavior={Platform.OS === "ios" ? "padding" : null}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              Keyboard.dismiss();
            }}
          >
            <ScrollContainer>
              <MainContainer>
                <Spacer space={3} />
                <FlatList
                  extraData={this.state}
                  // contentContainerStyle={{paddingBottom: 30}}
                  data={data}
                  keyExtractor={(item) => item.key}
                  renderItem={({ item, index }) =>
                    this._renderRoomView(item, index)
                  }
                />
                {timSlotModelView == true && (
                  <TimeSlotModelView
                    isTimeSlotVisible={timSlotModelView}
                    closeRoomModal={() =>
                      this.setState({ timSlotModelView: false })
                    }
                    timeSlotClicked={(hour) => this.timeSlotAction(hour)}
                  />
                )}
                <TouchableOpacity
                  activeOpacity={0.7}
                  onPress={this.Add_New_View_Function}
                >
                  <Text style={styles.AddMoreRoomButton}>+ Add More Rooms</Text>
                </TouchableOpacity>
                <Spacer space={3} />
                <Text
                  style={{
                    fontWeight: "600",
                    marginLeft: wp("4%"),
                    fontSize: 18,
                    color: "#554E4E",
                  }}
                >
                  Amenities
                </Text>
                <Spacer space={1} />
                <View style={styles.MainContainer}>
                  <FlatList
                    data={amnitiesData}
                    numColumns={2}
                    keyExtractor={(item) => item.id}
                    renderItem={({ item, index }) =>
                      this._renderAmenitiesView(item, index)
                    }
                  />
                </View>

                <Spacer space={2} />

                <Text
                  style={{
                    fontWeight: "600",
                    marginLeft: wp("4%"),
                    fontSize: 18,
                    color: "#554E4E",
                  }}
                >
                  Other Services
                </Text>
                <Spacer space={2} />
                <View style={{ width: wp(80) }}>
                  <RadioForm
                    style={{ alignSelf: "flex-start", paddingVertical: wp(2) }}
                    // formHorizontal={true}
                    labelStyle={{
                      marginRight: wp(2),
                      fontFamily: FONT_FAMILY.Montserrat,
                      paddingVertical: wp(1),
                      fontSize: FONT.TextSmall,
                    }}
                    buttonSize={12}
                    buttonColor={COLORS.placeholder_color}
                    radio_props={tempOtherService}
                    initial={0}
                    selectedButtonColor={COLORS.radio_color}
                    onPress={(value) => {
                      this.setState({ otherService: value });
                    }}
                  />
                </View>
                <Spacer space={3} />

                <TouchableOpacity onPress={() => this.OnSubmit()}>
                  <View style={styles.buttonStyle}>
                    <Text style={styles.buttonText}>
                      {STRINGS.own_reg_next_button}
                    </Text>
                  </View>
                </TouchableOpacity>
                <Spacer space={2.5} />
                {this._renderCustomLoader()}
              </MainContainer>
            </ScrollContainer>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </SafeAreaViewContainer>
    );
  }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state) => ({
  hotelRoomState: state.RegisterHotelsReducer,
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    registerHotel: (payload) => dispatch(RegisterHotelsAction(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RoomsAmenities);
