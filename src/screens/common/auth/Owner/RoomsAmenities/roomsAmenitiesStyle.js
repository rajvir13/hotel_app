import React from "react";
import styled from "styled-components";
import COLORS from "../../../../../utils/Colors";
import { FONT_FAMILY } from "../../../../../utils/Font";

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { StyleSheet } from "react-native";
const styles = StyleSheet.create({
  containerStyle: {
    alignSelf: "center",
    width: wp("100%"),
    padding: wp("15%"),
    justifyContent: "center",
    flex: 1,
  },

  closeWindowIcon: {
    height: wp("8%"),
    width: wp("8%"),
    alignSelf: "flex-end",
    position: "absolute",
    top: wp("-1%"),
  },

  addImageIcon: {
    height: wp("13%"),
    width: wp("13%"),
    alignSelf: "center",
  },

  discountView: {
    width: wp(30),
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 2,
    borderRadius: 12,
    borderColor: "#E54242",
  },
  discountText:{
    paddingVertical: wp(3.2),
    color: "#E54242",
    fontSize: 13,
    fontFamily: FONT_FAMILY.Montserrat,
  },
noOfGuestOuterView:{
  flexDirection: "row",
                  justifyContent: "space-around",
                  alignItems: "center",

},

noOfGuestInnerView:{
  width: wp(45),
  borderWidth: 2,
  borderRadius: 12,
  borderColor: "#E8E8E8",
  fontFamily: FONT_FAMILY.Montserrat,
},
  dropDownContainer: {
    width: wp(92),
    paddingHorizontal: wp(5),
    alignContent: "space-around",
  },
  listStyle: {
    width: 0,
    paddingRight: wp("2%"),
    marginRight: wp(5),
  },

  view1: {
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center",
  },
  view2: {
    width: wp(45),
    borderWidth: 2,
    borderRadius: 12,
    borderColor: "#E8E8E8",
  },

  roomOuterView: {
    justifyContent: "center",
    alignItems: "center",
  },
  closeWindoIcon: {
    width: wp(10),
    height: wp(10),
  },

  regularPriceText: {
    width: wp(85),
    borderWidth: 2,
    borderRadius: 12,
    borderColor: "#E8E8E8",
    paddingVertical: wp(2.5),
    paddingLeft: wp(2),
    color: COLORS.placeholder_color,
    fontFamily: FONT_FAMILY.Montserrat,
  },
  TextComponentStyle: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "gray",
    color: "gray",
    padding: 8,
    fontSize: wp("4%"),
    paddingHorizontal: 20,
    width: wp("50%"),
    height: wp("10%"),
  },
  buttonStyle: {
    marginVertical: 10,
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: COLORS.app_theme_color,
    height: wp("12%"),
    width: wp("80%"),
    borderRadius: wp("12%") / 2,
    marginBottom: wp("9%"),
  },
  imgOuterView: {
    height: wp("25%"),
    width: wp("25%"),
    borderRadius: wp("25%") / 2,
    backgroundColor: COLORS.imgage_bg,
    justifyContent: "center",
  },
  imageStyle: {
    height: wp("20%"),
    width: wp("20%"),
    borderRadius: wp("20%") / 2,
    alignSelf:'center'
  },
  imagesContainer: {
    flexDirection: "row",
    height: wp("25%"),
    width: "100%",
    // backgroundColor:'red'
  },

  discountView:{
    width: wp(30),
    alignItems: "center",
    justifyContent: "center",
    borderWidth: 2,
    borderRadius: 12,
    borderColor: "#E54242",

  },

  discountText:{
    paddingVertical: wp(3.2),
    color: "#E54242",
    fontSize: 13,
    fontFamily: FONT_FAMILY.Montserrat,
  },
  buttonText: {
    fontSize: 16,
    fontStyle: "normal",
    fontWeight: "500",
    lineHeight: 20,
    display: "flex",
    fontFamily: "Montserrat",
    color: "#FFFFFF",
    textAlign: "center",
  },
  TextDiscountStyle: {
    borderRadius: 8,
    borderWidth: 1,
    borderColor: "red",
    color: "red",
    padding: 8,
    fontSize: wp("4%"),
    textAlign: "center",
    height: wp("10%"),

    textShadowRadius: 5,
  },
  ComplementryServiceText: {
    marginLeft: wp("3%"),
    borderRadius: 5,
    borderWidth: 1,
    borderColor: "gray",
    color: "gray",
    padding: 8,
    fontSize: wp("4%"),
    width: wp("80%"),
    marginTop: wp("5%"),
  },

  AddMoreText: {
    color: "red",
    padding: 8,
    fontSize: wp("4%"),
    textAlign: "center",
    width: wp("80%"),
    // textAlign: "right",
    fontWeight: "bold",
  },
  container: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  valueText: {
    fontSize: 18,
    marginBottom: 50,
  },

  checkboxStyle: {
    height: 100,
  },

  MainContainer: {
    justifyContent: "center",
    flex: 1,
    paddingTop: 10,
  },

  AddMoreRoomButton: {
    padding: 10,
    fontSize: wp("4%"),
    width: wp("80%"),
    textAlign: "center",
    borderRadius: 20,
    borderWidth: 1,
    borderColor: "blue",
    color: "blue",
    alignSelf: "center",
  },

  OwnerRoomDetailView: {
    width: wp("93%"),
  },

  RepeatRoomView: {
    width: wp(100),
    alignItems: "center",
    // flex: 0.5,
  },
  roomParentView: {
    // flex: 1,
    justifyContent: "center",
    alignContent: "center",
  },
});

export default styles;
