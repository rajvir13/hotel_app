import React from 'react';
import styled from 'styled-components';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen'
import {StyleSheet} from 'react-native';
import COLORS from "../../../../utils/Colors";
import {FONT} from "../../../../utils/FontSizes";

const styles = StyleSheet.create({

    containerStyle: {
        width: wp('100%'),
        paddingTop: wp('15%'),
        justifyContent: 'center',
    },
    headerStyle: {},
    shadowContainer: {
        height: 50,
        borderRadius: 25,
        margin: 10
    },

    emailTI: {
        margin: 8, padding: 5, flex: 1,
        height: 40, textAlign: 'center',
        backgroundColor: 'transparent',
        fontSize: 15
    },


    enterText: {
        margin: 5,
        fontSize: 15,
        fontStyle: "normal",
        fontWeight: "normal",
        lineHeight: 18,
        fontFamily: 'Montserrat',
        color: '#000000',
        textAlign: 'center',
        padding: 5,
        textAlignVertical: 'center',
    },
    otpIcon: {
        alignSelf: 'center'
    },
    recoverText: {
        margin: 5,
        padding: 5,
        fontSize: 30,
        fontStyle: "normal",
        fontWeight: "500",
        lineHeight: 37,
        fontFamily: 'Montserrat',
        color: '#000000',
        textAlign: 'center',
        textAlignVertical: 'center',
    },

});

const InputTextContainer = styled.View`
width: ${wp("90%")};
alignItems: center;
marginBottom: ${wp("2%")}
`;

const TextTypeVerifyCode = styled.Text`
width: ${wp('100%')}; 
color: ${COLORS.app_theme_color};
text-align: center; 
fontSize: ${FONT.TextLarge}
fontWeight:400
`;

const TextEnterOtp = styled.Text`
width: ${wp('100%')}; 
color:${COLORS.black_color};
text-align: left; 
padding-left: ${hp("8%")}
marginTop:${wp("5%")};
padding-right: ${hp("8%")}
fontSize: ${FONT.TextSmall}
fontWeight:100
`;

const OTPView = styled.View`
marginTop:${hp('0%')};
height: ${hp('15%')};
backgroundColor: red;
flexDirection: column;
width: ${wp('80%')}
`;

const TextCodeOtp = styled.Text`
width: ${wp('100%')}; 
color:${COLORS.black_color};
text-align: center; 
fontSize: ${FONT.TextSmall}
fontWeight:100
`;
export {
    styles, InputTextContainer, TextTypeVerifyCode,
    TextEnterOtp, OTPView, TextCodeOtp
};
