/*
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 *  IMPORTS
 * ---------------------------------------------------------------------------------------
 * ---------------------------------------------------------------------------------------
 */

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import React from "react";
import {
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
  Platform,
  StatusBar,
  KeyboardAvoidingView,
  ScrollViewComponent,
} from "react-native";
import OtpInputs from "react-native-otp-inputs";
import { CommonActions } from "@react-navigation/native";

import { styles } from "./verificationStyle";
import BaseClass from "../../../../utils/BaseClass";
import STRINGS from "../../../../utils/Strings";
import { Header } from "react-native-elements";
import { Spacer } from "../../../../customComponents/Spacer";
import COLORS from "../../../../utils/Colors";
import { LeftIcon } from "../../../../customComponents/icons";
import { FONT } from "../../../../utils/FontSizes";
import {
  SafeAreaViewContainer,
  MainContainer,
  ScrollContainer,
} from "../../../../utils/BaseStyle";
import { ICONS } from "../../../../utils/ImagePaths";
import { PrimaryButton } from "../../../../customComponents/button/AppButton";
import OrientationLoadingOverlay from "../../../../customComponents/Loader";

// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import firebase from "react-native-firebase";
import { RegisterAction } from "../../../../redux/actions/registerAction";
import { connect } from "react-redux";
import * as DeviceInfo from "react-native-device-info";
import AsyncStorage from "@react-native-community/async-storage";
import Icons from "react-native-vector-icons/MaterialIcons";
import { OwnerRegNameAction } from "../../../../redux/actions/OwnerNameAction";
import CountDown from "../../../../customComponents/CountDown";
import { FONT_FAMILY } from "../../../../utils/Font";

Icons.loadFont();
const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
    {children}
  </TouchableWithoutFeedback>
);

class Verification extends BaseClass {
  constructor(props) {
    super(props);
    this.hideDialog();
    this.state = {
      confirmResult: undefined,
      registerData: undefined,
      registerOwnerData: undefined,
      otpCode: "",
      authAccessToken: "",
      resendDisabled: true,
      phoneNumber: "",
      callingCode: "",
      user: undefined,
    };
    this.isOtpVerified = true;
    this.unsubscribe = null;
  }

  /*
// =============================================================================================
// Render method for Header
// =============================================================================================
*/

  componentDidMount() {
    this.hideDialog()
    if (
      this.props.route.params.registerData !== undefined &&
      this.props.route.params.registerData !== null
    ) {
      this.setState({
        confirmResult: this.props.route.params.confirmResult,
        registerData: this.props.route.params.registerData,
        authAccessToken: this.props.route.params.registerData.accessToken,
        phoneNumber: this.props.route.params.registerData.phoneNumber,
        callingCode: this.props.route.params.registerData.callingCode,
      });
    } else if (
      this.props.route.params.registerOwnerData !== undefined &&
      this.props.route.params.registerOwnerData !== null
    ) {
      this.setState({
        confirmResult: this.props.route.params.confirmResult,
        registerOwnerData: this.props.route.params.registerOwnerData,
        authAccessToken: this.props.route.params.registerOwnerData.accessToken,
        phoneNumber: this.props.route.params.registerOwnerData.phoneNumber,
        callingCode: this.props.route.params.registerOwnerData.callingCode,
      });
    }
    this.unsubscribe = firebase
      .auth()
      .onAuthStateChanged(this.onAuthStateChanged);
    // this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
    //     if (user) {
    //         if (Platform.OS !== "ios" && this.isOtpVerified) {
    //
    //             if (user._user.phoneNumber === this.props.route.params.registerData.phoneNumber) {
    //                 this.showDialog();
    //                 this.isOtpVerified = false;
    //             }
    //         }
    //     } else {
    //         this.setState({user: null});
    //         console.warn("user else==>", user)
    //     }
    // });
    // DeviceInfo.getDeviceToken().then(deviceToken => {
    //     console.warn("Device Token isss", deviceToken);
    // });
  }

  onAuthStateChanged = (user) => {
    if (user?.uid) this.setState({ user });
  };

  //component will receive props

  componentWillReceiveProps(nextProps, nextContext) {
    this.hideDialog();
    const { registerData, registerOwnerData } = this.state;
    const { navigate } = this.props.navigation;
    const { responseRegister } = nextProps.registerState;
    const { OwnerNameResponse } = nextProps.ownerRegNameState;
    if (responseRegister !== undefined && responseRegister !== null) {
      if (responseRegister === "Network request failed") {
        this.hideDialog();
        this.showToastAlert(STRINGS.CHECK_INTERNET);
      } else {
        const { response, message, status } = responseRegister;
        if (status === 200) {
          this.hideDialog();
          if (response !== undefined && response !== null) {
            AsyncStorage.setItem(
              STRINGS.loginToken,
              JSON.stringify(response.access_token)
            );
            AsyncStorage.setItem(STRINGS.loginData, JSON.stringify(response));
            AsyncStorage.setItem(
              STRINGS.loginCredentials,
              JSON.stringify({
                email: registerData.email,
                password: registerData.password,
                social: registerData.socialType === "Google" ? "1" : "2",
                social_id:
                  registerData.socialType === ""
                    ? undefined
                    : registerData.socialType !== "Google"
                    ? registerData.socialData.id
                    : registerData.socialData.user.id,
              })
            );
            this.props.navigation.dispatch(
              CommonActions.reset({
                index: 0,
                routes: [{ name: "customerHome" }],
              })
            );
          }
        } else if (status === 111) {
          this.hideDialog();
          if (message.email !== undefined) {
            this.hideDialog();
            this.showToastAlert(message.email[0]);
          } else if (message.mobile_number !== undefined) {
            this.hideDialog();
            this.showToastAlert(message.mobile_number[0]);
          } else if (message.registeration_type !== undefined) {
            this.hideDialog();
            this.showToastAlert(message.registeration_type[0]);
          } else {
            this.hideDialog();
            console.warn(message);
          }
        } else {
          this.hideDialog();
        }
      }
    }
    if (OwnerNameResponse !== undefined && OwnerNameResponse !== null) {
      this.hideDialog();
      if (OwnerNameResponse === "Network request failed") {
        this.hideDialog();
        this.showToastAlert(STRINGS.CHECK_INTERNET);
      } else {
        const { response, message, status } = OwnerNameResponse;
        if (response !== undefined && status === 200) {
          this.hideDialog();
          AsyncStorage.setItem(
            STRINGS.loginToken,
            JSON.stringify(response.access_token)
          );
          AsyncStorage.setItem(STRINGS.loginData, JSON.stringify(response));
          AsyncStorage.setItem(
            STRINGS.loginCredentials,
            JSON.stringify({
              email: registerOwnerData.email,
              password: registerOwnerData.password,
              social: registerOwnerData.socialType === "Google" ? "1" : "2",
              social_id:
                registerOwnerData.socialType === ""
                  ? undefined
                  : registerOwnerData.socialType !== "Google"
                  ? registerOwnerData.socialData.id
                  : registerOwnerData.socialData.user.id,
            })
          );
          this.props.navigation.navigate("RoomsAmenities", {
            accessToken: response.access_token,
            hotelId: response.id,
            authToken: this.state.authAccessToken,
            from: "Splash",
          });
        } else if (status === 111) {
          this.hideDialog();
          this.showToastAlert("mobile number or email is already exist");
        } else if (status === 401) {
          this.hideDialog();
          this.showToastAlert("server error");
        } else {
          this.hideDialog();
          this.showToastAlert(message);
        }
      }
    }
  }

  // ----------------------------------------
  componentWillUnmount() {
    // if (this.unsubscribe) this.unsubscribe();
    this.unsubscribe();
  }

  // ----------------------------------------
  // ----------------------------------------
  // OtpInput library method for handling input.
  // ----------------------------------------

  onCodeEntered(code) {
    this.setState({
      otpCode: code,
    });
    if (code.length === 6) {
      Keyboard.dismiss();
      this._onSubmitPress(code);
    }
  }

  _onSubmitPress(code) {
    const { confirmResult, user } = this.state;
    this.isOtpVerified = false;
    // this._hitAPI()
    if (code.length === 6) {
      this.showDialog();
      if (!user) {
        confirmResult
          .confirm(code)
          .then((user) => {
            firebase.auth().signOut();
            this._hitAPI();
          })
          .catch((error) => {
            this.hideDialog();
            if (error.code === "auth/invalid-verification-code") {
              this.showToastAlert(STRINGS.invalid_otp);
            }
            if (error.code === "auth/session-expired") {
              this.showToastAlert(STRINGS.session_expired);
            } else {
              this.showToastAlert(STRINGS.invalid_otp);
            }
          });
      } else {
        firebase.auth().signOut();
        this._hitAPI();
      }
    } else {
      this.hideDialog();
      this.showToastAlert(STRINGS.enter_otp_text);
    }
  }

  /**
   * It is used to resend the otp by verifying phone number
   */
  async _onResendPress() {
    const { phoneNumber, callingCode } = this.state;
    this.setState({
      resendDisabled: true,
      countReset: new Date().getTime(),
    });
    await firebase
      .auth()
      .signInWithPhoneNumber(`${callingCode}${phoneNumber}`)
      // await firebase.auth().signInWithPhoneNumber("+919878717918")
      .then((confirmResult) => {
        this.setState({
          confirmResult,
        });
      })
      .catch((error) => {
        this.hideDialog();
        if (
          error.code === "auth/unknown" ||
          error.code === "auth/too-many-requests"
        ) {
          this.showToastAlert(STRINGS.limit_exceeded_text);
        } else if (error.code === "auth/invalid-phone-number") {
          this.showToastAlert(STRINGS.unauthorized_phone_number);
        }
        this.hideDialog();
        console.log(error, error.code, "ios error");
      });
  }

  _hitAPI() {
    const { registerData, registerOwnerData } = this.state;
    let deviceToken = this.deviceToken();
    if (registerData !== undefined && registerOwnerData === undefined) {
      this.showDialog();
      this.props.SubmitDetails({
        first_name: registerData.firstName,
        last_name: registerData.lastName,
        password: registerData.password,
        mobile_number: registerData.phoneNumber,
        city: registerData.cityId,
        state: registerData.stateId,
        country: registerData.countryId,
        email: registerData.email,
        accessToken: registerData.accessToken,
        callingCode: registerData.callingCode,
        deviceToken,
        user_type: 2,
        device_id: DeviceInfo.getUniqueId(),
        social_id:
          registerData.socialType !== "Google"
            ? registerData.socialData.id
            : registerData.socialData.user.id,
        registeration_type: registerData.socialType === "Google" ? "1" : "2",
      });
    } else if (registerOwnerData !== undefined && registerData === undefined) {
      console.log("lat :", registerOwnerData.lat);
      console.log("long :", registerOwnerData.lat);

      AsyncStorage.getItem(STRINGS.latlong, (error, result) => {
        console.log("result", result);
        if (result !== undefined && result !== null) {
          let latlong = JSON.parse(result);
          if (latlong !== undefined) {
            this.showDialog();
            this.props.ownerNameApi({
              ownerFirstName: registerOwnerData.firstName,
              ownerLastName: registerOwnerData.lastName,
              passwordText: registerOwnerData.password,
              phoneNoText: registerOwnerData.phoneNumber,
              emailText: registerOwnerData.email,
              callingCode: registerOwnerData.callingCode,
              registeration_type:
                registerOwnerData.socialType === "Google" ? "1" : "2",
              hotelName: registerOwnerData.hotelName,
              hotelAddress: registerOwnerData.hotelAddress,
              lat: latlong.latitude,
              lng: latlong.longitude,
              user_type: 1,
              deviceToken,
              deviceId: DeviceInfo.getUniqueId(),
              authToken: registerOwnerData.accessToken,
              social_id:
                registerOwnerData.socialType !== "Google"
                  ? registerOwnerData.socialData.id
                  : registerOwnerData.socialData.user.id,
              imagess: registerOwnerData.images,
              description: registerOwnerData.hotelDetail,
            });
          }
        }
      });
    }
  }

  _renderHeader() {
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.backGround_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={<LeftIcon onPress={() => navigation.goBack()} />}
        centerComponent={{
          text: STRINGS.ver_header_title,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  _renderCountDown = () => {
    const { countReset } = this.state;
    return (
      <CountDown
        id={countReset}
        until={Platform.OS === "ios" ? 60 : 90}
        size={15}
        onFinish={() => this.setState({ resendDisabled: false })}
        digitStyle={{ backgroundColor: COLORS.transparent }}
        digitTxtStyle={{ color: COLORS.black_color }}
        timeToShow={["M", "S"]}
        timeLabels={{ m: "", s: "" }}
        separatorStyle={{ color: COLORS.black_color, paddingBottom: 2 }}
        showSeparator
      />
    );
  };

  // -------------------------------------------------------
  _renderPrimaryButton = () => {
    const { resendDisabled } = this.state;
    return (
      <PrimaryButton
        containerColor={COLORS.light_grey1}
        color={
          resendDisabled === false ? COLORS.app_theme_color : COLORS.light_grey3
        }
        disabled={resendDisabled}
        btnText={STRINGS.ver_button_text}
        onPress={() => {
          this._onResendPress();
        }}
      />
    );
  };
  // ----------------------------------------

  _renderInputText = () => {
    const { number, countryCode } = this.state;
    return (
      <View style={{ height: wp(20) }}>
        <OtpInputs
          ref={this.otpRef}
          handleChange={(code) => this.onCodeEntered(code)}
          numberOfInputs={6}
          clearTextOnFocus={true}
          unfocusedBorderColor={"gray"}
          inputContainerStyles={{
            // borderBottomWidth: 1,
            borderWidth: 1,
            width: wp("11%"),
            borderRadius: 7,
            // alignContent: 'center',
            // textAlign: 'center',
          }}
          inputStyles={{
            padding: 1,
            width: wp("11%"),
            // justifyContent: 'center',
            // alignSelf: 'center',
          }}
          focusedBorderColor={COLORS.placeholder_color}
        />
      </View>
    );
  };

  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  // ----------------------------------------
  // =============================================================================================
  // Render method
  // =============================================================================================

  render() {
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <KeyboardAvoidingView
          style={{ flex: 1, backgroundColor: COLORS.white_color }}
          behavior={Platform.OS === "ios" ? "padding" : null}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              Keyboard.dismiss();
            }}
          >
            <ScrollContainer>
              <MainContainer>
                <View style={styles.containerStyle}>
                  <Image style={styles.otpIcon} source={ICONS.OTP_ICON} />
                  <Spacer space={2.5} />
                  <Text style={styles.recoverText}>
                    {STRINGS.ver_main_text}
                  </Text>
                  <Text style={styles.enterText}>
                    {STRINGS.ver_detail_text}
                  </Text>
                </View>
                {/*<View style={{*/}
                {/*    width: wp('80%'),*/}
                {/*    flexDirection:"row",*/}
                {/*    justifyContent:"center",*/}
                {/*    alignItems:"center"*/}
                {/*}}>*/}
                {this._renderCountDown()}
                {/*</View>*/}
                <Spacer space={2.5} />
                {this._renderInputText()}
                <Spacer space={5} />
                {this._renderPrimaryButton()}
                {/*<Spacer space={2.5}/>*/}
                {this._renderCustomLoader()}
              </MainContainer>
            </ScrollContainer>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </SafeAreaViewContainer>
    );
  }
}

// ----------------------------------------
// ----------------------------------------
// CONNECT
// ----------------------------------------

const mapStateToProps = (state) => ({
  registerState: state.RegisterReducer,
  ownerRegNameState: state.OwnerRegNameReducer,
});

// ----------------------------------------

const mapDispatchToProps = (dispatch) => {
  return {
    SubmitDetails: (payload) => dispatch(RegisterAction(payload)),
    ownerNameApi: (payload) => dispatch(OwnerRegNameAction(payload)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Verification);
