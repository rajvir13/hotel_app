import React from "react";
import {
  View,
  Text,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Platform,
  StatusBar,
  KeyboardAvoidingView,
  Image,
  ImageBackground,
} from "react-native";
import firebase from "react-native-firebase";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

Icon.loadFont();
import { Header } from "react-native-elements";
import CountryPicker from "react-native-country-picker-modal";
import { Input, CheckBox } from "react-native-elements";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import AsyncStorage from "@react-native-community/async-storage";

//library imports

import * as Validations from "../../../../utils/Validations";
import CountryModal from "../../../../customComponents/Modals/CountryPicker";
import { Spacer } from "../../../../customComponents/Spacer";
import {
  SafeAreaViewContainer,
  MainContainer,
  ShadowViewContainer,
  ScrollContainer,
} from "../../../../utils/BaseStyle";
import COLORS from "../../../../utils/Colors";
import { FONT } from "../../../../utils/FontSizes";
import { LeftIcon } from "../../../../customComponents/icons";
import styles from "./registerStyle";
import CustomTextInput from "../../../../customComponents/CustomTextInput";
import BaseClass from "../../../../utils/BaseClass";
import STRINGS from "../../../../utils/Strings";
import {
  RegisterCountryAction,
  RegisterStateAction,
  RegisterCityAction,
  MobileVerifyAction,
  EmailVerifyAction,
} from "../../../../redux/actions/registerAction";
import { ICONS } from "../../../../utils/ImagePaths";
import OrientationLoadingOverlay from "../../../../customComponents/Loader";
import { FONT_FAMILY } from "../../../../utils/Font";

const userCountryData = "91";

export default class Register extends BaseClass {
  constructor(props) {
    let userLocaleCountryCode = "IN";
    super(props);
    this.state = {
      countryData: [],
      stateData: [],
      cityData: [],
      firstNameText: "",
      lastName: "",
      emailText: "",
      countryCode: "",
      phoneNoText: "",
      countryName: "",
      countryId: "",
      stateName: "",
      stateId: "",
      cityName: "",
      cityId: "",
      passwordText: "",
      confirmPassword: "",
      isTermSelected: false,
      cca2: userLocaleCountryCode,
      callingCode: userCountryData,
      checkBox: false,
      displayCountryModal: false,
      modal_type: "",
      accessToken: "",
      user: null,
      confirmResult: null,
      isDropvisible: false,
    };
    this.unsubscribe = null;
    this.social_profile = "";
    this.social_data = {};
  }

  componentDidMount() {
    if (this.props.route.params !== undefined) {
      if (
        this.props.route.params.socialData !== undefined &&
        this.props.route.params.socialData !== null &&
        this.props.route.params.socialData !== ""
      ) {
        this.social_profile = this.props.route.params.socialProfile;
        this.social_data = this.props.route.params.socialData;
        // this.setState({
        //     emailText: (this.social_profile === "Facebook") ?  this.social_data.email :  this.social_data.user.email,
        //     firstNameText: (this.social_profile === "Facebook") ?  this.social_data.first_name :  this.social_data.user.givenName,
        //     lastName: (this.social_profile === "Facebook") ?  this.social_data.last_name :  this.social_data.user.familyName
        // })
        let social_data = this.props.route.params.socialData;
        let firstName = "";
        let LastName = "";
        if (this.social_profile === "Facebook") {
          if (social_data.first_name !== "null")
            firstName = social_data.first_name;
        } else {
          if (social_data.user.name !== "null")
            firstName = social_data.user.name;
        }

        if (this.social_profile === "Facebook") {
          if (social_data.last_name !== "null")
            LastName = social_data.last_name;
        } else {
          if (social_data.user.familyName !== "null")
            LastName = social_data.user.familyName;
        }

        this.setState({
          emailText:
            this.social_profile === "Facebook"
              ? social_data.email
              : social_data.user.email,
          firstNameText:
            this.social_profile === "Facebook" ? firstName : firstName,
          lastName: this.social_profile === "Facebook" ? LastName : LastName,
        });
      }
    }

    AsyncStorage.getItem(STRINGS.accessToken, (error, result) => {
      if (result !== undefined && result !== null) {
        let accessToken = JSON.parse(result);

        if (accessToken !== undefined) {
          this.setState({ accessToken: accessToken });
          this.getCountryList();
        }
      }
    });

    this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.setState({ user: user.toJSON() });
      } else {
        // User has been signed out, reset the state
        this.setState({
          user: null,
          confirmResult: null,
        });
      }
    });
  }

  componentWillUnmount() {
    if (this.unsubscribe) this.unsubscribe();
  }

  //*********************************** get Country List ***************************************

  getCountryList = () => {
    // this.showDialog();
    RegisterCountryAction(
      {
        accessToken: this.state.accessToken,
      },
      (data) => this.countryResponse(data)
    );
   
  };

  countryResponse = (data) => {
    this.hideDialog()
    if (data !== undefined) {
      this.hideDialog();
      if (data.status === 200) {
        this.hideDialog();
        this.setState({
          countryData: data.response,
        });
      } else if (data.status === 401) {
        this.hideDialog();
        this.showToastAlert(data.message);
      }
    } else {
      this.hideDialog();
    }
  };

  stateResponse = (data) => {
    this.hideDialog();
    if (data !== undefined) {
      this.hideDialog();
      if (data.status === 200) {
        this.setState({
          stateData: data.response,
        });
      } else if (data.status === 401) {
        this.showToastAlert(data.message);
      }
    } else {
      this.hideDialog();
    }
  };

  cityResponse = (data) => {
    this.hideDialog();
     if (data !== undefined) {
      this.hideDialog();
      if (data.status === 200) {
        this.setState({
          cityData: data.response,
        });
      } else if (data.status === 401) {
        this.showToastAlert(data.message);
      }
    } else {
      this.hideDialog();
    }
  };

  sendOTP = async () => {
    const {
      callingCode,
      firstNameText,
      lastName,
      emailText,
      cityId,
      countryId,
      stateId,
      passwordText,
      phoneNoText,
      accessToken,
    } = this.state;
    let data = {
      firstName: firstNameText,
      lastName: lastName,
      email: emailText,
      cityId: cityId,
      countryId: countryId,
      stateId: stateId,
      callingCode: `+${callingCode}`,
      phoneNumber: phoneNoText,
      password: passwordText,
      accessToken: accessToken,
      socialData: this.social_data,
      socialType: this.social_profile,
    };
    await firebase
      .auth()
      .signInWithPhoneNumber(`+${callingCode}${phoneNoText}`)
      // await firebase.auth().signInWithPhoneNumber("+919878717918")
      .then((confirmResult) => {
        console.log("success", confirmResult);
        this.hideDialog();
        this.props.navigation.navigate("verification", {
          confirmResult: confirmResult,
          registerData: data,
        });
        this.hideDialog();
      })
      .catch((error) => {
        this.hideDialog();
        if (
          error.code === "auth/unknown" ||
          error.code === "auth/too-many-requests"
        ) {
          this.showToastAlert(STRINGS.limit_exceeded_text);
        } else if (error.code === "auth/invalid-phone-number") {
          this.showToastAlert(STRINGS.unauthorized_phone_number);
        }
        this.hideDialog();
        // alert(error);
        console.log(error, error.code, "ios error");
      });
  };

  /**
   *  Handle modal trigger
   */
  triggerCountryPicker = (value) => {
    this.setState({
      displayCountryModal: true,
      modal_type: value,
    });
  };

  closeCountryModal() {
    this.setState({
      displayCountryModal: false,
    });
  }

  /**
   * handle item selected in modal
   */
  onItemClick = (item, value) => {
    const { accessToken } = this.state;
    if (this.state.modal_type === "country") {
      this.setState({
        displayCountryModal: false,
        countryName: item.name,
        countryId: item.id,
        stateName: "",
        stateId: "",
        cityName: "",
        cityId: "",
        stateData: [],
        cityData: [],
      });
        // this.showDialog()
      RegisterStateAction(
        {
          accessToken: accessToken,
          id: item.id,
        },
        (data) => this.stateResponse(data)
      );
    
    } else if (this.state.modal_type === "state") {
      this.setState({
        displayCountryModal: false,
        stateName: item.name,
        stateId: item.id,
        cityName: "",
        cityId: "",
        cityData: [],
      });
      //  this.showDialog()
      RegisterCityAction(
        {
          accessToken: accessToken,
          id: item.id,
        },
        (data) => this.cityResponse(data)
      );
     
    } else {
      this.setState({
        displayCountryModal: false,
        cityName: item.name,
        cityId: item.id,
      });
    }
  };

  // success button action
  submitPress = () => {
    const {
      firstNameText,
      lastName,
      emailText,
      cityName,
      cityId,
      countryId,
      stateId,
      stateName,
      countryName,
      passwordText,
      phoneNoText,
      confirmPassword,
      accessToken,
      checkBox,
    } = this.state;
    if (firstNameText.length < 1) {
      this.showToastAlert(STRINGS.empty_first_name);
    } else if (firstNameText.trimRight().length < 2) {
      this.showToastAlert(STRINGS.name_minimum_three);
    } else if (lastName.length < 1) {
      this.showToastAlert(STRINGS.empty_last_name);
    } else if (lastName.trimRight().length < 2) {
      this.showToastAlert(STRINGS.name_minimum_three);
    } else if (emailText.length < 1) {
      this.showToastAlert(STRINGS.empty_email);
    } else if (!Validations.validateEmail(emailText)) {
      this.showToastAlert(STRINGS.valid_email);
    } else if (phoneNoText.length < 1) {
      this.showToastAlert(STRINGS.enter_mobileNo);
    } else if (!Validations.validatePassword(passwordText)) {
      this.showToastAlert(STRINGS.enter_valid_password);
    } else if (passwordText.length < 1) {
      this.showToastAlert(STRINGS.empty_password);
    } else if (confirmPassword.length < 1) {
      this.showToastAlert(STRINGS.empty_confirm_password);
    } else if (confirmPassword.trim() !== passwordText.trim()) {
      this.showToastAlert(STRINGS.password_does_not_match);
    } else if (countryName.length < 1) {
      this.showToastAlert(STRINGS.empty_country_name);
    } else if (stateName.length < 1) {
      this.showToastAlert(STRINGS.empty_state_name);
    } else if (cityName.length < 1) {
      this.showToastAlert(STRINGS.empty_city_name);
    } else if (!checkBox) {
      this.showToastAlert(STRINGS.termAndConditionFill);
    } else {
      // this.showDialog();
      EmailVerifyAction(
        {
          email: emailText,
          accessToken: accessToken,
        },
        (data) => this.emailVerifyResponse(data)
      );
     
    }
    // this.sendOTP()
  };

  emailVerifyResponse = (data) => {
    const { accessToken, phoneNoText, callingCode } = this.state;
    this.hideDialog();
    if (data !== undefined) {
      if (data.status === 200) {
        // this.sendOTP()
        // this.showDialog();
        MobileVerifyAction(
          {
            phoneNumber: phoneNoText,
            accessToken: accessToken,
          },
          (data) => this.phoneVerifyResponse(data)
        );
       
      } else if (data.status === 111) {
        if (data.message.email !== undefined) {
          this.showToastAlert(data.message.email[0]);
        } else {
          this.showToastAlert(data.message);
        }
      } else if (data.status === 401) {
        this.showToastAlert(data.message);
      }
    } else {
      this.hideDialog();
    }
  };

  phoneVerifyResponse = (data) => {
    if (data !== undefined) {
      this.hideDialog();
      if (data.status === 200) {
        this.hideDialog();
        this.sendOTP();
      } else if (data.status === 111) {
        this.hideDialog();
        if (data.message.mobile_number !== undefined) {
          this.showToastAlert(data.message.mobile_number[0]);
          this.hideDialog();
        } else {
          this.showToastAlert(data.message);
          this.hideDialog();
        }
      } else if (data.status === 401) {
        this.hideDialog();
        this.showToastAlert(data.message);
      }
    } else {
      this.hideDialog();
    }
  };

  onCountrySelect = (value) => {
    this.setState({
      cca2: value.cca2,
      callingCode: value.callingCode[0],
    });
  };

  /*
     // =============================================================================================
     // Render method for Header
     // =============================================================================================
     */

  _renderHeader() {
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.backGround_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={<LeftIcon onPress={() => navigation.goBack()} />}
        centerComponent={{
          text: STRINGS.reg_header_title,
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }

  /*
     // =============================================================================================
     // Render method for country code picker
     // =============================================================================================
     */
  _renderCountryPicker = () => {
    return (
      <CountryPicker
        ref={(picker) => {
          this.picker = picker;
        }}
        renderFlagButton={this.DropDownView}
        countryList={this.state.countryData}
        // onSelect={(value) => {
        //     console.warn("onSelect")
        //     // this.onCountrySelect(value)
        // }}
        onOpen={() => {
          console.warn("onOpen");
        }}
        hideAlphabetFilter={true}
        showCallingCode={true}
        filterPlaceholder={STRINGS.SEARCH_TEXT}
        closeable={true}
        filterable={true}
        cca2={this.state.cca2}
        translation="eng"
        autoFocusFilter={false}
        countryCode={this.state.cca2}
        withFilter={true}
        visible={false}
      />
    );
  };

  DropDownView = () => {
    const { isDropvisible } = this.state;
    return (
      <TouchableOpacity
        onPress={() => this.setState({ isDropvisible: !isDropvisible })}
      >
        <Image
          source={ICONS.DOWN_ARROW}
          style={{ height: wp(4), width: wp(4) }}
        />
      </TouchableOpacity>
    );
  };

  _renderCountryPicker2 = () => {
    return (
      <CountryPicker
        ref={(picker) => {
          this.picker = picker;
        }}
        renderFlagButton={this.DropDownView}
        countryList={this.state.countryData}
        onSelect={(value) => {
          this.onCountrySelect(value);
        }}
        hideAlphabetFilter={true}
        showCallingCode={true}
        filterPlaceholder={STRINGS.SEARCH_TEXT}
        closeable={true}
        filterable={true}
        cca2={this.state.cca2}
        translation="eng"
        autoFocusFilter={false}
        countryCode={this.state.cca2}
        withFilter={true}
        visible={this.state.isDropvisible}
        onClose={() => this.setState({ isDropvisible: false })}
      />
    );
  };
  /*
        // =============================================================================================
        // Render method for Phone number field
        // =============================================================================================
        */

  _renderPhoneNumberView = () => {
    const { phoneNoText, callingCode } = this.state;
    // let countryCodeLength = (callingCode.length > 2) ? 9 : 10;
    return (
      <View style={styles.phoneContainerStyle}>
        <ShadowViewContainer style={{ height: wp(13) }}>
          <Input
            inputContainerStyle={{
              borderBottomColor: "transparent",
              height: wp(13),
            }}
            editable={false}
            pointerEvents={"none"}
            // leftIcon={this._renderCountryPicker}
            // leftIconContainerStyle={{marginLeft: 0, alignItems: "center"}}
            value={`+${callingCode}`}
            containerStyle={{
              width: wp("24%"),
              // height: wp(12.5),
            }}
            rightIcon={
              this._renderCountryPicker2
              // <TouchableOpacity onPress={() => this.picker.setState({ modalVisible: true })}  >
              //     <Image source={ICONS.DOWN_ARROW} />
              // </TouchableOpacity>
            }
            rightIconContainerStyle={{ alignItems: "center" }}
            inputStyle={{
              fontSize: FONT.TextSmall_2,
            }}
          />
        </ShadowViewContainer>
        <Spacer row={1.5} />
        <ShadowViewContainer style={{ height: wp(13) }}>
          <Input
            inputContainerStyle={{
              borderBottomColor: "transparent",
              height: wp(13),
            }}
            // editable={false}
            placeholder={STRINGS.MOBILE_NUMBER}
            placeholderTextColor={COLORS.placeholder_color}
            ref="Mobile"
            value={phoneNoText}
            onChangeText={(text) =>
              this.setState({
                phoneNoText: text.trim() && text.replace(/^0+/, ""),
                // isChangedValue: true
              })
            }
            maxLength={10}
            keyboardType="phone-pad"
            returnKeyType={"next"}
            inputStyle={{
              // paddingLeft: wp(0.5), //fontFamily: FONTNAME.RalewayRegular
              fontSize: FONT.TextSmall_2,
            }}
            containerStyle={{
              paddingBottom: 0,
              alignItems: "center",
              width: wp("58%"),
              // height: wp(12.5),
            }}
          />
        </ShadowViewContainer>
      </View>
    );
  };

  _renderCheckButton = () => {
    const { checkBox } = this.state;
    return (
      <View style={{ flexDirection: "column", justifyContent: "center" }}>
        <View
          style={{
            height: wp("5%"),
            height: wp("5%"),
            alignSelf: "flex-start",
            justifyContent: "center",
           
          }}
        >
          <CheckBox
          
            title={""}
            textStyle={{ color: COLORS.placeholder_color }}
            checkedIcon={
              <ImageBackground
                source={ICONS.UNCHECKED_ICON}
                style={{
                  height: wp(12),
                  width: wp(12),
                  justifyContent: "center",
                }}
              >
                <Image
                  source={ICONS.CHECKED_ICON}
                  style={{
                    height: wp(6),
                    width: wp(6),
                    resizeMode: "contain",
                    alignSelf: "center",
                  }}
                />
              </ImageBackground>
            }
            uncheckedIcon={
              <Image
                source={ICONS.UNCHECKED_ICON}
                style={{ height: wp(12), width: wp(12), resizeMode: "contain" }}
              />
            }
            checked={this.state.checkBox}
            onPress={() => this.setState({ checkBox: !this.state.checkBox })}
            containerStyle={{
              width: wp("85%"),
              backgroundColor: COLORS.backGround_color,
              borderWidth: 0,
            }}
          />
        </View>
        <TouchableOpacity
          onPress={() => this.termAndConditionAction()}
          underlayColor="white"
        >
          <Text style={styles.termConditionTextStyle}>
            {STRINGS.TERM_CONDITION_TEXT}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  termAndConditionAction = () => {
    const { navigate } = this.props.navigation;
    navigate("MyWebComponent");
  };

  _renderCustomLoader = () => {
    const { isLoading } = this.state;
    return (
      <OrientationLoadingOverlay
        visible={isLoading}
        message={STRINGS.LOADING_TEXT}
      />
    );
  };

  render() {
    const {
      displayCountryModal,
      emailText,
      countryName,
      modal_type,
      firstNameText,
      lastName,
      passwordText,
      confirmPassword,
      cityName,
      stateName,
      countryData,
      stateData,
      cityData,
    } = this.state;
    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}
        <KeyboardAvoidingView
          style={{ flex: 1, backgroundColor: COLORS.white_color }}
          behavior={Platform.OS === "ios" ? "padding" : null}
        >
          <TouchableWithoutFeedback
            onPress={() => {
              Keyboard.dismiss();
            }}
          >
            <ScrollContainer>
              <MainContainer>
                <View style={styles.mainContainerStyle}>
                  <Spacer space={0.5} />
                  <CustomTextInput
                    placeholder={STRINGS.reg_first_name_placeholder}
                    returnKeyType={"next"}
                    ipOnChangeText={(text) =>
                      this.setState({ firstNameText: text })
                    }
                    inputvalue={firstNameText}
                  />
                  <Spacer space={0.5} />
                  <CustomTextInput
                    placeholder={STRINGS.reg_last_name_placeholder}
                    ipOnChangeText={(text) =>
                      this.setState({ lastName: text.trim() })
                    }
                    inputvalue={lastName}
                  />
                  <Spacer space={0.5} />
                  <CustomTextInput
                    placeholder={STRINGS.reg_email_placeholder}
                    keyboardType={"email-address"}
                    ipOnChangeText={(text) =>
                      this.setState({ emailText: text.trim() })
                    }
                    inputvalue={emailText}
                    isEditable={this.social_profile === ""}
                  />
                  <Spacer space={0.5} />
                  {this._renderPhoneNumberView()}
                  {/*<Spacer space={0.5}/>*/}
                  <CustomTextInput
                    placeholder={STRINGS.reg_password_placeholder}
                    isSecure={true}
                    ipOnChangeText={(text) =>
                      this.setState({ passwordText: text.trim() })
                    }
                    inputvalue={passwordText}
                  />
                  <Spacer space={0.5} />
                  <CustomTextInput
                    placeholder={STRINGS.reg_confirm_password_placeholder}
                    isSecure={true}
                    ipOnChangeText={(text) =>
                      this.setState({ confirmPassword: text.trim() })
                    }
                    inputvalue={confirmPassword}
                  />
                  <Spacer space={0.5} />
                  <TouchableOpacity
                    onPress={() => this.triggerCountryPicker("country")}
                  >
                    <ShadowViewContainer
                      style={{
                        width: wp("85%"),
                        // backgroundColor: 'hotpink',
                        height: Platform.OS === "ios" ? wp(10.5) : wp(12.5),
                      }}
                    >
                      <Input
                        inputContainerStyle={{
                          height: Platform.OS === "ios" ? wp(10.5) : wp(12.5),
                          backgroundColor: COLORS.white_color,
                          borderRadius: wp(3),
                          borderBottomWidth: 0,
                          width: wp("85%"),
                          marginLeft: -10,
                        }}
                        editable={false}
                        placeholder={STRINGS.reg_country_placeholder}
                        placeholderTextColor={COLORS.placeholder_color}
                        value={countryName}
                        onChangeText={(text) =>
                          this.setState({ countryName: text.trim() })
                        }
                        inputStyle={{
                          color: COLORS.black_color,
                          fontSize: FONT.TextSmall_2,
                          marginLeft: wp(2),
                        }}
                        rightIconContainerStyle={{ marginRight: wp(2) }}
                        rightIcon={
                          <Image
                            source={ICONS.DOWN_ARROW}
                            resizeMode={"contain"}
                          />
                        }
                      />
                    </ShadowViewContainer>
                  </TouchableOpacity>
                  <Spacer space={0.5} />
                  <TouchableOpacity
                    onPress={() => this.triggerCountryPicker("state")}
                  >
                    <ShadowViewContainer
                      style={{
                        width: wp("85%"),
                        // backgroundColor: 'hotpink',
                        height: Platform.OS === "ios" ? wp(10.5) : wp(12.5),
                      }}
                    >
                      <Input
                        inputContainerStyle={{
                          height: Platform.OS === "ios" ? wp(10.5) : wp(12.5),
                          backgroundColor: COLORS.white_color,
                          borderRadius: wp(3),
                          borderBottomWidth: 0,
                          width: wp("85%"),
                          marginLeft: -10,
                        }}
                        editable={false}
                        placeholder={STRINGS.reg_state_placeholder}
                        placeholderTextColor={COLORS.placeholder_color}
                        value={stateName}
                        onChangeText={(text) =>
                          this.setState({ stateName: text.trim() })
                        }
                        inputStyle={{
                          color: COLORS.black_color,
                          fontSize: FONT.TextSmall_2,
                          marginLeft: wp(2),
                        }}
                        rightIconContainerStyle={{ marginRight: wp(2) }}
                        rightIcon={
                          <Image
                            source={ICONS.DOWN_ARROW}
                            resizeMode={"contain"}
                          />
                        }
                      />
                    </ShadowViewContainer>
                  </TouchableOpacity>
                  <Spacer space={0.5} />
                  <TouchableOpacity
                    onPress={() => this.triggerCountryPicker("city")}
                  >
                    <ShadowViewContainer
                      style={{
                        width: wp("85%"),
                        // backgroundColor: 'hotpink',
                        height: Platform.OS === "ios" ? wp(10.5) : wp(12.5),
                      }}
                    >
                      <Input
                        inputContainerStyle={{
                          height: Platform.OS === "ios" ? wp(10.5) : wp(12.5),
                          backgroundColor: COLORS.white_color,
                          borderRadius: wp(3),
                          borderBottomWidth: 0,
                          width: wp("85%"),
                          marginLeft: -10,
                        }}
                        editable={false}
                        placeholder={STRINGS.reg_city_placeholder}
                        placeholderTextColor={COLORS.placeholder_color}
                        value={cityName}
                        onChangeText={(text) =>
                          this.setState({ cityName: text.trim() })
                        }
                        inputStyle={{
                          color: COLORS.black_color,
                          fontSize: FONT.TextSmall_2,
                          marginLeft: wp(2),
                        }}
                        rightIconContainerStyle={{ marginRight: wp(2) }}
                        rightIcon={
                          <Image
                            source={ICONS.DOWN_ARROW}
                            resizeMode={"contain"}
                          />
                        }
                      />
                    </ShadowViewContainer>
                  </TouchableOpacity>
                  {/*<Spacer space={0.5}/>*/}
                  {this._renderCheckButton()}
                  <Spacer space={0.5} />
                  <TouchableOpacity onPress={() => this.submitPress()}>
                    <View style={styles.buttonStyle}>
                      <Text style={styles.buttonText}>
                        {STRINGS.reg_submit_button}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>

                <CountryModal
                  visibleCountry={displayCountryModal}
                  value={
                    modal_type === "country"
                      ? countryName
                      : modal_type === "state"
                      ? stateName
                      : cityName
                  }
                  passingValue={
                    modal_type === "country"
                      ? countryData
                      : modal_type === "state"
                      ? stateData
                      : cityData
                  }
                  onItemClick={(item, value) => {
                    this.onItemClick(item, value);
                  }}
                  closeCountryModal={() => {
                    this.closeCountryModal();
                  }}
                />
                {this._renderCustomLoader()}
              </MainContainer>
            </ScrollContainer>
          </TouchableWithoutFeedback>
        </KeyboardAvoidingView>
      </SafeAreaViewContainer>
    );
  }
}
