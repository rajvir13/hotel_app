import React from "react";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { Platform, StyleSheet } from "react-native";
import { FONT } from "../../../../utils/FontSizes";
import COLORS from "../../../../utils/Colors";

const styles = StyleSheet.create({
  mainContainerStyle: {
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: wp(5),
    paddingHorizontal: wp(5),
  },
  AddPolicyFileView: {

    flexDirection: 'row',
    justifyContent: 'flex-start',
    // marginLeft: wp('5%')

},


  phoneContainerStyle: {
    flexDirection: "row",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 20,
  },
  checkbox: {
    alignSelf: "center",
  },
  phoneInputStyle: {
    color: "#828282",
    width: wp("45%"),
    // backgroundColor: 'hotpink',
    fontSize: FONT.TextSmall_2,
  },
  inputContainerStyle: {
    padding: 5,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: 48,
    borderRadius: 10,
    paddingVertical: 5,
    margin: 10,
  },
  buttonStyle: {
    // marginVertical: 10,
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#4582E5",
    borderWidth: 1,
    borderColor: "transparent",
    height: 50,
    width: wp("80%"),
    borderRadius: 25,
  },
  buttonText: {
    fontSize: FONT.TextMedium,
    fontStyle: "normal",
    fontWeight: "500",
    lineHeight: 20,
    display: "flex",
    fontFamily: "Montserrat",
    color: "#FFFFFF",
    textAlign: "center",
  },
  checkboxContainer: {
    flexDirection: "row",
    marginBottom: 20,
  },
  checkbox: {
    alignSelf: "center",
    height:wp('4%'),
    width:wp('4%'),
    
  },
  label: {
    margin: 8,
    color: "#828282",
    fontSize: 13,
    fontStyle: "normal",
    fontWeight: "600",
    lineHeight: 16,
    display: "flex",
    textAlign: "center",
  },

  termConditionTextStyle:{

    fontSize:wp('4%'),marginTop:wp(-4),marginLeft:wp('3%'), alignSelf:'center',marginBottom:wp('4%'), fontWeight:'bold',color:COLORS.greyButton_color
  
  }

});

export default styles;
