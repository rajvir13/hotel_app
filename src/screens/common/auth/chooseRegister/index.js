// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react";
import {
    View,
    Text,
    Image,
    TouchableOpacity,
    ImageBackground, Platform, StatusBar
} from "react-native";
import {widthPercentageToDP as wp} from "react-native-responsive-screen";


// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------
import styles from "./chooseregisterStyle";
import BaseClass from "../../../../utils/BaseClass";
import STRINGS from '../../../../utils/Strings'
import {SafeAreaViewContainer, MainContainer, ShadowViewContainer} from "../../../../utils/BaseStyle";
import {ICONS} from "../../../../utils/ImagePaths";
import {Spacer} from "../../../../customComponents/Spacer";
import COLORS from "../../../../utils/Colors";
import {FONT} from "../../../../utils/FontSizes";
import {Header} from "react-native-elements";
import {LeftIcon} from "../../../../customComponents/icons";
import {FONT_FAMILY} from "../../../../utils/Font";


class RegisterAsScreen extends BaseClass {
    constructor(props) {
        super(props);
        this.state = {
            socialResponse: '',
            whichSocialProfile:''
        }
    }

    componentDidMount() {
        if (this.props.route.params !== undefined) {
            if (this.props.route.params.social_data !== undefined && this.props.route.params.social_data !== null) {
                this.setState({
                    socialResponse: this.props.route.params.social_data,
                    whichSocialProfile: this.props.route.params.socialProfile
                })
            }
        }
    }

    _renderHeader() {
        const {navigation} = this.props;
        return (
            <Header
                backgroundColor={COLORS.backGround_color}
                barStyle={"dark-content"}
                statusBarProps={{
                    translucent: true,
                    backgroundColor: COLORS.transparent,
                }}
                leftComponent={<LeftIcon onPress={() => navigation.goBack()}/>}
                centerComponent={{
                    text: STRINGS.select_type,
                    style: {
                        color: COLORS.greyButton_color,
                        fontSize: FONT.TextMedium_2,
                        fontFamily: FONT_FAMILY.PoppinsBold
                    },
                }}
                containerStyle={{
                    borderBottomColor: COLORS.greyButton_color,
                    paddingTop: Platform.OS === "ios" ? 0 : 25,
                    height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
                }}
            />
        );
    }

    render() {
        const {socialResponse, whichSocialProfile } = this.state;
        return (
            <SafeAreaViewContainer>
                 {this._renderHeader()}
                <MainContainer>
                    <View style={{flex: 1, width: wp(100)}}>
                        <ImageBackground
                            resizeMode={'stretch'}
                            style={{flex: 1}}
                            source={ICONS.IMAGE_BACKGROUND}>
                            <View style={styles.mainContainer}>
                                <Image source={ICONS.LOGIN_ICON}
                                       style={styles.logoImg}/>
                                <Spacer space={3}/>
                                <ShadowViewContainer style={{borderRadius: wp(5)}}>
                                    <View style={styles.centreView}>
                                        <Spacer space={2.5}/>
                                        <Text style={styles.registerAsText}>{STRINGS.choose_registerAsText}</Text>
                                        <Spacer space={2.5}/>
                                        <TouchableOpacity onPress={() => {
                                            this.props.navigation.navigate('nameAddress', {socialData: socialResponse, socialProfile: whichSocialProfile})
                                        }}>
                                            <View style={styles.typeHotelContainer}>
                                                <Image source={ICONS.HOTEL_ICON}
                                                       style={styles.typeImg}/>
                                                <Text style={styles.ownerText}>{STRINGS.choose_reg_hotelOwner}</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <Spacer space={2.5}/>
                                        <TouchableOpacity onPress={() => {
                                            this.props.navigation.navigate('register', {socialData: socialResponse, socialProfile: whichSocialProfile})
                                        }}>
                                            <View style={styles.typeCustomerContainer}>
                                                <Image source={ICONS.CUSTOMER_ICON}
                                                       style={styles.typeImg}/>
                                                <Text style={styles.ownerText}>{STRINGS.choose_reg_customer}</Text>
                                            </View>
                                        </TouchableOpacity>
                                        <Spacer space={3}/>
                                    </View>
                                </ShadowViewContainer>
                            </View>
                        </ImageBackground>
                    </View>
                </MainContainer>
            </SafeAreaViewContainer>
        );
    }
}

export default RegisterAsScreen;
