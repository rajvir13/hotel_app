import React from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen'
import { StyleSheet, Dimensions } from 'react-native';
import { FONT } from "../../../../utils/FontSizes";
import COLORS from "../../../../utils/Colors";
import {FONT_FAMILY} from "../../../../utils/Font";

const styles = StyleSheet.create({
    mainContainer: {
        alignSelf: 'center',flex:1,
        justifyContent:'center'
    },
    logoImg: {
        width: wp(35),
        height: wp(35),
        borderRadius: wp(35) / 2,
        alignSelf: 'center', marginVertical: 0, paddingVertical: 0
    },
    registerAsText: {
        alignSelf: 'flex-start',
        fontStyle: "normal",
        fontWeight: "500",
        fontSize: 20,
        lineHeight: 24,
        display: "flex",
        alignItems: "center",
    },
    ownerText:{

        // fontStyle: "normal",
        // fontWeight: "600",
        fontSize: FONT.TextNormal,
        fontFamily: FONT_FAMILY.MontserratSemiBold,
        // lineHeight: 30,
        // display: "flex",
        alignItems: "center",
        color:COLORS.white_color
    },
    centreView:{
        backgroundColor: COLORS.white_color,
        borderRadius: wp(8),
        width: wp(80),
        marginHorizontal:wp(5),
        padding: 5
    },
    typeHotelContainer: {
        flexDirection: "row",
        alignItems:"center",
        borderRadius:wp(3),
        paddingVertical:5,
        backgroundColor:COLORS.app_theme_color,
    },
    typeCustomerContainer: {
        flexDirection: "row",alignItems:"center",
        borderRadius:wp(3),
        paddingVertical:5,
        backgroundColor:COLORS.customerType,
    },
    typeImg:{
        width: wp(15),
        height: wp(15),padding:wp(3),margin:5,
        alignSelf: 'center', resizeMode:"contain",paddingVertical:5
    },

});

export default styles;
