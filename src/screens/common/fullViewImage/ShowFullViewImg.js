import { Dimensions, View,StatusBar } from "react-native";
import ImageZoom from "react-native-image-pan-zoom";
import BaseClass from "../../../utils/BaseClass";
import React from "react";
import { Header } from "react-native-elements";
import { FONT_FAMILY } from "../../../utils/Font";
import COLORS from "@utils/Colors";
import { LeftIcon } from "@customComponents/icons";
import { FONT } from "@utils/FontSizes";
import { Image} from 'react-native-elements';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {
  SafeAreaViewContainer,
  ShadowViewContainer,
  ScrollContainer,
} from "@utils/BaseStyle";
export default class ShowFullViewImg extends BaseClass {
  _renderHeader() {
    const { navigation } = this.props;
    return (
      <Header
        backgroundColor={COLORS.white_color}
        barStyle={"dark-content"}
        statusBarProps={{
          translucent: true,
          backgroundColor: COLORS.transparent,
        }}
        leftComponent={<LeftIcon onPress={() => navigation.goBack()} />}
        centerComponent={{
          text: "",
          style: {
            color: COLORS.greyButton_color,
            fontSize: FONT.TextMedium_2,
            fontFamily: FONT_FAMILY.PoppinsBold,
          },
        }}
        containerStyle={{
          borderBottomColor: COLORS.greyButton_color,
          paddingTop: Platform.OS === "ios" ? 0 : 25,
          height: Platform.OS === "ios" ? 60 : StatusBar.currentHeight + 60,
        }}
      />
    );
  }
  render() {
   

    return (
      <SafeAreaViewContainer>
        {this._renderHeader()}

        <View
          style={{
            justifyContent: "flex-start",
            alignSelf: "flex-start",
          }}
        >
           <ImageZoom  style={{alignSelf:'flex-start',justifyContent:'flex-start'}}
                      cropWidth={Dimensions.get('window').width}
                       cropHeight={Dimensions.get('window').height}
                       imageWidth={wp('100%')}
                       imageHeight={hp('50%')}>
                <Image style={{width:wp('100%'), height:hp('50%'),alignSelf:"center",justifyContent:'center'}}
                       source={{uri:this.props.route.params.imgUrl}}
                       resizeMode='cover'
                       />
            </ImageZoom>
        </View>
      </SafeAreaViewContainer>
    );
  }
}
