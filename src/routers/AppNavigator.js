import * as React from "react";
import { View } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import Animated, { Value } from "react-native-reanimated";
import AnimatedTabBar, { TabsConfigsType } from "curved-bottom-navigation-bar";

import LoginScreen from "../screens/common/auth/login/index";
import RecoverPassword from "../screens/common/auth/forgotPassword";
import RegisterScreen from "../screens/common/auth/register";
import RegisterAs from "../screens/common/auth/chooseRegister";
import SelectPlan from "../screens/common/SelectPlan";
import Verification from "../screens/common/auth/verificationCode";
import NameAddress from "../screens/common/auth/Owner/NameAddress";
import CustomerProfile from "../screens/customer/profile";
import GMapScreen from "../customComponents/GoogleMaps/Maps";
import Splash from "../screens/common/auth/splash";
import RoomsAmenities from "../screens/common/auth/Owner/RoomsAmenities";

import CustomerHome from "../screens/customer/home";
import PlanComponent from "../../assets/images/TabView/PlanSVG";
import ProfileComponent from "../../assets/images/TabView/ProfileSVG";
import RatingComponent from "../../assets/images/TabView/RatingSVG";
import HomeComponent from "../../assets/images/TabView/HomeSVG";
import COLORS from "../utils/Colors";
import CheckInCheckOut from "../screens/common/auth/Owner/CheckInCheckOut";
import BellSVG from "../../assets/images/CustomerHome/BellIconSVG";
import HotelHomeScreen from "../screens/hotelOwner/home";
import HotelDetails from "../screens/hotelOwner/hotelDetailScreens";
import CustomDrawerContent from "../screens/drawer/CustomeDrawerContentComponent";
import HotelListNearMe from "../screens/customer/hotelList";
import ParticularHotelProfile from "../screens/customer/particularHotelProfile";
import HotelProfile from "../screens/hotelOwner/hotelProfile";
import UpdateRoomDetail from "../screens/hotelOwner/updateRoomDetail";
import UpdateOwnerPolicy from "../screens/hotelOwner/policyUpdate";
import UpdateAmenities from "../screens/hotelOwner/updateAminitesServices";
import OwnerUpdatePassword from "../screens/hotelOwner/ownerChangePassword";
import RoomsList from "../screens/hotelOwner/roomList";
import AddNewRooms from "../screens/hotelOwner/addHotelRoom";
import RatingHistory from "../screens/customer/ratingHistory";
import PdfViewer from "../screens/common/PdfViewer/PdfViewer";
import SubscriptionPlan from "../screens/hotelOwner/subscriptionPlan/index";
import RatingAndFeedback from "../screens/hotelOwner/ratingAndFeedback/index";
import ContactUs from "../screens/hotelOwner/contactUs/index";
import CustomerMainBooking from "../screens/customer/booking";
import OwnerBooking from "../screens/hotelOwner/ownerBookings";
import Help from "../screens/hotelOwner/help/index";
import { createAppContainer } from "react-navigation";
import Commission from "../screens/hotelOwner/commision";
import CustomerRoomList from "../screens/customer/roomsList";
import NotificationAndPramotion from "../screens/hotelOwner/notificationAndPromotion/index";
import CustomerNotificationAndPramotion from "../screens/customer/notificationAndPramotion/index";
import AddPayment from "../screens/hotelOwner/payment/index";
import PaymentHistory from "../screens/hotelOwner/paymentHistory/index";
import ShowFullViewImg from "../screens/common/fullViewImage/ShowFullViewImg";
import MyWebComponent from '../screens/common/webViewOpen/MyWebComponent'
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();

const DrawerNavigator = () => {
  const [progress, setProgress] = React.useState(new Animated.Value(0));
  const scale = Animated.interpolate(progress, {
    inputRange: [0, 1],
    outputRange: [1, 0.8],
  });
  const borderRadius = Animated.interpolate(progress, {
    inputRange: [0, 1],
    outputRange: [0, 40],
  });
  const screenStyle = { borderRadius, transform: [{ scale }] };
  return (
    <View style={{ flex: 1, backgroundColor: COLORS.app_theme_color }}>
      <Drawer.Navigator
        drawerType={"slide"}
        overlayColor={COLORS.transparent}
        drawerStyle={{
          width: wp(65),
          backgroundColor: "transparent",
        }}
        drawerContentOption={{
          activeBackgroundColor: "transparent",
          activeTintColor: "green",
          inactiveTintColor: "green",
        }}
        sceneContainerStyle={{ backgroundColor: "transparent" }}
        drawerContent={(props) => {
          setProgress(props.progress);
          return <CustomDrawerContent {...props} />;
        }}
        initialRouteName="Home"
      >
        <Drawer.Screen name="Screens">
          {(props) => <DrawerScreens {...props} style={screenStyle} />}
        </Drawer.Screen>
      </Drawer.Navigator>
    </View>
  );
};

/*const tabs: TabsConfigsType = {

    My_Plan: {
        icon: ({focused}) => <PlanComponent
            fillColor={focused ? COLORS.tabBarActiveColor : COLORS.black_color}/>
    },
    Review: {
        icon: ({focused}) => <RatingComponent
            fillColor={focused ? COLORS.tabBarActiveColor : COLORS.black_color}/>
    },
    Home: {
        icon: ({color}) => <HomeComponent fillColor={color === undefined ? COLORS.black_color : color}/>
    },
    Profile: {
        icon: ({focused}) => <ProfileComponent
            fillColor={focused ? COLORS.tabBarActiveColor : COLORS.black_color}/>
    },
    Notification: {
        icon: ({focused}) => <BellSVG
            fillColor={focused ? COLORS.tabBarActiveColor : COLORS.black_color}/>
    },
};

const TabBar = () => {
    return (
        <Tab.Navigator
            initialRouteName='Home'
            backBehavior='initialRoute'
            tabBar={props => (
                <AnimatedTabBar dotColor={COLORS.light_grey2} barColor={COLORS.light_grey2} tabs={tabs} {...props}
                                style={{color: 'pink'}}/>
            )}
        >
            <Tab.Screen
                name="My_Plan"
                component={CustomerMainBooking}
            />
            <Tab.Screen
                name="Review"
                component={RatingHistory}
            />
            <Tab.Screen
                name="Home"
                component={CustomerHome}
            />
            <Tab.Screen
                name="Profile"
                component={CustomerProfile}
            />
            <Tab.Screen
                name="Notification"
                component={DummyScreen}
            />
        </Tab.Navigator>
    )
};*/

const TabBar = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      backBehavior="initialRoute"
      tabBarOptions={{
        showLabel: false,
        activeTintColor: "#f0edf6",
        inactiveTintColor: "#3e2465",
      }}
      // shifting={true}
      // labeled={false}
      // activeColor={'#f0edf6'}
      // inactiveColor={'#3e2465'}
      barStyle={{
        backgroundColor: COLORS.white_color,
        height: wp(20),
      }}
    >
      <Tab.Screen
        name={"MyPlan"}
        component={CustomerMainBooking}
        options={{
          tabBarIcon: ({ color, focused }) => (
            <View>
              {focused ? (
                <View
                  style={{
                    backgroundColor: COLORS.tabBarActiveColor,
                    width: wp(12),
                    height: wp(12),
                    borderRadius: wp(6),
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <PlanComponent fillColor={COLORS.white_color} />
                </View>
              ) : (
                <PlanComponent fillColor={COLORS.black_color} />
              )}
            </View>
          ),
          tabBarColor: COLORS.white_color,
          // unmountOnBlur: true
        }}
      />
      <Tab.Screen
        name={"Review"}
        component={RatingHistory}
        options={{
          tabBarIcon: ({ color, focused }) => (
            <View>
              {focused ? (
                <View
                  style={{
                    backgroundColor: COLORS.tabBarActiveColor,
                    width: wp(12),
                    height: wp(12),
                    borderRadius: wp(6),
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <RatingComponent fillColor={COLORS.white_color} />
                </View>
              ) : (
                <RatingComponent fillColor={COLORS.black_color} />
              )}
            </View>
          ),
          tabBarColor: COLORS.white_color,
        }}
      />
      <Tab.Screen
        name={"Home"}
        component={CustomerHome}
        options={{
          tabBarIcon: ({ color, focused }) => (
            <View>
              {focused ? (
                <View
                  style={{
                    backgroundColor: COLORS.tabBarActiveColor,
                    width: wp(12),
                    height: wp(12),
                    borderRadius: wp(6),
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <HomeComponent fillColor={COLORS.white_color} />
                </View>
              ) : (
                <HomeComponent fillColor={COLORS.black_color} />
              )}
            </View>
          ),
          tabBarColor: COLORS.white_color,
        }}
      />
      <Tab.Screen
        name={"Profile"}
        component={CustomerProfile}
        options={{
          tabBarIcon: ({ color, focused }) => (
            <View>
              {focused ? (
                <View
                  style={{
                    backgroundColor: COLORS.tabBarActiveColor,
                    width: wp(12),
                    height: wp(12),
                    borderRadius: wp(6),
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <ProfileComponent fillColor={COLORS.white_color} />
                </View>
              ) : (
                <ProfileComponent fillColor={COLORS.black_color} />
              )}
            </View>
          ),
          tabBarColor: COLORS.white_color,
        }}
      />
      <Tab.Screen
        name={"CustomerNotificationAndPramotion"}
        component={CustomerNotificationAndPramotion}
        options={{
          tabBarIcon: ({ color, focused }) => (
            <View>
              {focused ? (
                <View
                  style={{
                    backgroundColor: COLORS.tabBarActiveColor,
                    width: wp(12),
                    height: wp(12),
                    borderRadius: wp(6),
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <BellSVG fillColor={COLORS.white_color} />
                </View>
              ) : (
                <BellSVG fillColor={COLORS.black_color} />
              )}
            </View>
          ),
          tabBarColor: COLORS.white_color,
        }}
      />
    </Tab.Navigator>
  );
};

const Navigator = () => {
  return (
    <Stack.Navigator initialRouteName="splash" headerMode="none">
      {/*<Stack.Screen name="dummy" component={DummyScreen}/>*/}

      <Stack.Screen name="login" component={LoginScreen} />
      <Stack.Screen name="recoverPassword" component={RecoverPassword} />
      <Stack.Screen name="register" component={RegisterScreen} />
      <Stack.Screen name="registerAs" component={RegisterAs} />
      <Stack.Screen name="selectPlan" component={SelectPlan} />
      <Stack.Screen name="verification" component={Verification} />
      <Stack.Screen name="nameAddress" component={NameAddress} />
      <Stack.Screen name="customerProfile" component={CustomerProfile} />
      <Stack.Screen name="splash" component={Splash} />
      <Stack.Screen name="customerHome" component={TabBar} />
      <Stack.Screen name="Maps" component={GMapScreen} />
      <Stack.Screen name="RoomsAmenities" component={RoomsAmenities} />
      <Stack.Screen name="CheckInCheckOut" component={CheckInCheckOut} />
      <Stack.Screen name="hotelHomeScreen" component={DrawerNavigator} />
      <Stack.Screen name="hotelListScreen" component={HotelListNearMe} />
      <Stack.Screen name="Myplan" component={CustomerMainBooking} />
      <Stack.Screen
        name="particularHotelProfile"
        component={ParticularHotelProfile}
      />
      {/*<Stack.Screen name="hotelListOnMap" component={HotelMapView}/>*/}
      <Stack.Screen name="ratingScreen" component={RatingHistory} />
      <Stack.Screen name="PdfViewScreen" component={PdfViewer} />
      {/*<Stack.Screen name="customerBooking" component={CustomerMainBooking}/>*/}
      <Stack.Screen name="customerRoomList" component={CustomerRoomList} />
      <Stack.Screen name="AddPayment" component={AddPayment} />
      <Stack.Screen name="PaymentHistory" component={PaymentHistory} />
      <Stack.Screen name="ShowFullViewImg" component={ShowFullViewImg} />
      <Stack.Screen name="MyWebComponent" component={MyWebComponent} />


      
    </Stack.Navigator>
    // </Animated.View>
  );
};

const DrawerScreens = ({ style }) => {
  return (
    <Animated.View style={[{ flex: 1, overflow: "hidden" }, style]}>
      <Stack.Navigator initialRouteName="hotelHome" headerMode="none">
        <Stack.Screen name="ShowFullViewImg" component={ShowFullViewImg} />
        <Stack.Screen name="hotelHome" component={HotelHomeScreen} />
        <Stack.Screen name="hotelDetail" component={HotelDetails} />
        <Stack.Screen name="ownerAddressProfile" component={HotelProfile} />
        <Stack.Screen name="ownerRoomProfile" component={UpdateRoomDetail} />
        <Stack.Screen
          name="ownerAmenitiesProfile"
          component={UpdateAmenities}
        />
        <Stack.Screen name="ownerPolicyProfile" component={UpdateOwnerPolicy} />
        <Stack.Screen
          name="ownerChangePassword"
          component={OwnerUpdatePassword}
        />
        <Stack.Screen name="roomList" component={RoomsList} />
        <Stack.Screen name="addNewRooms" component={AddNewRooms} />
        <Stack.Screen name="ownerBooking" component={OwnerBooking} />
        <Stack.Screen name="ratingAndFeedBack" component={RatingAndFeedback} />
        <Stack.Screen name="ownerSubscription" component={SubscriptionPlan} />
        <Stack.Screen name="ownerCommission" component={Commission} />
        <Stack.Screen name="ContactUs" component={ContactUs} />
        <Stack.Screen name="Help" component={Help} />
        <Stack.Screen
          name="NotificationAndPramotion"
          component={NotificationAndPramotion}
        />
      </Stack.Navigator>
    </Animated.View>
  );
};

export default Navigator;
// const AppContainer = createAppContainer(Navigator);
// export default AppContainer;
