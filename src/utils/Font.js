export const FONT_FAMILY = {
    "Montserrat": "Montserrat-Regular",
    "MontserratBold": "Montserrat-Bold",
    "MontserratSemiBold": "Montserrat-SemiBold",
    "Roboto": "Roboto-Regular",
    "RobotoBold": "Roboto-Bold",
    "Poppins":"Poppins-Regular",
    "PoppinsBold":"Poppins-Bold"
};
