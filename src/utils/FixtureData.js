const Data = [
    {
        "id": 114,
        "name": "KY Hotel",
        "room_hotel": [
            {
                "id": 21,
                "room_type": "single",
                "discounted_price": 440,
                "no_of_guest": 1,
                "room_service": [
                    {
                        "service": "AC"
                    }, {
                        "service": "Wifi"
                    }, {
                        "service": "Breakfast"
                    },
                ],
                "room_images": [
                    'https://travelji.com/wp-content/uploads/Hotel-Tips.jpg'
                ]
            }
        ],
        "room_type_count": 1,
        "room_count": 1
    }, {
        "id": 114,
        "name": "KY Hotel",
        "room_hotel": [
            {
                "id": 21,
                "room_type": "single",
                "discounted_price": 440,
                "no_of_guest": 1,
                "room_service": [
                    {
                        "service": "AC"
                    }, {
                        "service": "Wifi"
                    }, {
                        "service": "Breakfast"
                    },
                ],
                "room_images": [
                    'https://travelji.com/wp-content/uploads/Hotel-Tips.jpg'
                ]
            }
        ],
        "room_type_count": 1,
        "room_count": 1,
    }, {
        "id": 114,
        "name": "KY Hotel",
        "room_hotel": [
            {
                "id": 21,
                "room_type": "single",
                "discounted_price": 440,
                "no_of_guest": 1,
                "room_service": [
                    {
                        "service": "AC"
                    }, {
                        "service": "Wifi"
                    }, {
                        "service": "Breakfast"
                    },
                ],
                "room_images": [
                    'https://travelji.com/wp-content/uploads/Hotel-Tips.jpg'
                ]
            }
        ],
        "room_type_count": 1,
        "room_count": 1,
    }, {
        "id": 114,
        "name": "KY Hotel",
        "room_hotel": [
            {
                "id": 21,
                "room_type": "single",
                "discounted_price": 440,
                "no_of_guest": 1,
                "room_service": [
                    {
                        "service": "AC"
                    }, {
                        "service": "Wifi"
                    }, {
                        "service": "Breakfast"
                    },
                ],
                "room_images": [
                    'https://travelji.com/wp-content/uploads/Hotel-Tips.jpg'
                ]
            }
        ],
        "room_type_count": 1,
        "room_count": 1,
    },];


const NearBYDATA = [{
    hotel_images: [{image: 'https://images.unsplash.com/photo-1520250497591-112f2f40a3f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'}],
    name: 'Grand Royal Hotel',
    address: 'Hotel Grand Royal London',
    rating: 2,
    review: '150 Reviews',
    distance: '2',
    discount: '20% Discount',
    min_discounted_price: '200',
    min_regular_price: '1700',
}, {
    hotel_images: [{image: 'https://images.unsplash.com/photo-1520250497591-112f2f40a3f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'}],
    name: 'Grand Royal Hotel',
    address: 'Hotel Grand Royal London',
    rating: 4,
    review: '150 Reviews',
    distance: '2',
    discount: '20% Discount',
    min_discounted_price: '200',
    min_regular_price: '1300',
}, {
    hotel_images: [{image: 'https://images.unsplash.com/photo-1520250497591-112f2f40a3f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'}],
    name: 'Grand Royal Hotel',
    address: 'Hotel Grand Royal London',
    rating: 5,
    review: '150 Reviews',
    distance: '2',
    discount: '20% Discount',
    min_discounted_price: '200',
    min_regular_price: '2000',
}, {
    hotel_images: [{image: 'https://images.unsplash.com/photo-1520250497591-112f2f40a3f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'}],
    name: 'Grand Royal Hotel',
    address: 'Hotel Grand Royal London',
    rating: 3,
    review: '150 Reviews',
    distance: '2',
    discount: '20% Discount',
    min_discounted_price: '200',
    min_regular_price: '3000',
}, {
    hotel_images: [{image: 'https://images.unsplash.com/photo-1520250497591-112f2f40a3f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'}],
    name: 'Grand Royal Hotel',
    address: 'Hotel Grand Royal London',
    rating: 2,
    review: '150 Reviews',
    distance: '2',
    discount: '20% Discount',
    min_discounted_price: '200',
    min_regular_price: '1500',
}, {
    hotel_images: [{image: 'https://images.unsplash.com/photo-1520250497591-112f2f40a3f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'}],
    name: 'Grand Royal Hotel',
    address: 'Hotel Grand Royal London',
    rating: 5,
    review: '150 Reviews',
    distance: '2',
    discount: '20% Discount',
    min_discounted_price: '200',
    min_regular_price: '1000',
}, {
    hotel_images: [{image: 'https://images.unsplash.com/photo-1520250497591-112f2f40a3f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'}],
    name: 'Grand Royal Hotel',
    address: 'Hotel Grand Royal London',
    rating: 4,
    review: '150 Reviews',
    distance: '2',
    discount: '20% Discount',
    min_discounted_price: '200',
    min_regular_price: '300',
}, {
    hotel_images: [{image: 'https://images.unsplash.com/photo-1520250497591-112f2f40a3f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'}],
    name: 'Grand Royal Hotel',
    address: 'Hotel Grand Royal London',
    rating: 4,
    review: '150 Reviews',
    distance: '2',
    discount: '20% Discount',
    min_discounted_price: '200',
    min_regular_price: '700',
}, {
    hotel_images: [{image: 'https://images.unsplash.com/photo-1520250497591-112f2f40a3f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'}],
    name: 'Grand Royal Hotel',
    address: 'Hotel Grand Royal London',
    rating: 3,
    review: '150 Reviews',
    distance: '2',
    discount: '20% Discount',
    min_discounted_price: '200',
    min_regular_price: '800',
}, {
    hotel_images: [{image: 'https://images.unsplash.com/photo-1520250497591-112f2f40a3f4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80'}],
    name: 'Grand Royal Hotel',
    address: 'Hotel Grand Royal London',
    rating: 4,
    review: '150 Reviews',
    distance: '2',
    discount: '20% Discount',
    min_discounted_price: '200',
    min_regular_price: '500',
}];

const FilterData = [{
    value: "Guest Rating",
    isChecked: false,
}, {
    value: "High to Low Price",
    isChecked: false,
}, {
    value: "Low to High Price",
    isChecked: false,
}, {
    value: "All",
    isChecked: false,
}];


export {Data, NearBYDATA, FilterData}
