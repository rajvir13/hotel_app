import {Text} from "react-native";
import React from "react";

const STRINGS = {
    //==============================================
    //================ASYNC - STORAGE===============
    //==============================================

    accessToken: "ACCESS_TOKEN",
    loginToken: "LOGIN_TOKEN",
    loginData: "LOGIN_DATA",
    latlong: "LAT_LONG",
    loginCredentials: "LOGIN_CREDENTIALS",
    fcmToken:"fcmToken",

    //==============================================
    CHECK_INTERNET: "Please check your internet connection",
    COMING_SOON: "COMING SOON",
    APP_NAME: "Hotel App",
    SELECT_IMAGE: "Select image to upload.",
    GALLERY_TEXT: "Choose from Gallery",
    CAMERA_TEXT: "Choose from Camera",
    DELETE_UPLOAD_TEXT: "Do you want to delete upload image.",
    YES_SURE_TEXT: "Yes Sure",
    CANCEL_TEXT: "Cancel",
    LOADING_TEXT: "Loading..",
    EXIT_ALERT: "Are you sure, you want to logout from the application.",
    //==============================================
    //================Login Page====================
    //==============================================
    login_account: "Login Account",
    email_placeholder: "Email/Mobile Number",
    password_placeholder: "Password",
    forgot_text: "Forgot Password?",
    login_button: "Login",
    or_text: "OR",
    dont_have_text: "Don't have an account?",
    register_text: "REGISTER",

    //=====================================================
    //================Forgot Password======================
    //=====================================================
    forgot_header_title: "Forgot Password",
    recover_password: "Recover Password",
    enter_register:
        "Please Enter Registered Email Id,\n A temporary Password will be sent \n to registered Email Id.",
    enter_email: "Enter Your Email ID",
    send_button: "Send",

    //==========================================================
    //================Customer Register Page====================
    //==========================================================
    select_type: "Select Type",
    reg_header_title: "Register",
    reg_first_name_placeholder: "First Name",
    reg_last_name_placeholder: "Last Name",
    reg_email_placeholder: "Email",
    limit_exceeded_text: "Limit exceeded. Please try after some time.",
    unauthorized_phone_number: "Invalid phone number",
    //=====================================================
    //================Register Page====================
    //=====================================================

    reg_phone_number_placeholder: "Phone Number",
    reg_city_placeholder: "City",
    reg_state_placeholder: "State",
    reg_country_placeholder: "Country",
    reg_agree_tc: "Agree to terms and conditions",
    reg_submit_button: "Submit",
    reg_password_placeholder: "Enter a Password",
    reg_confirm_password_placeholder: "Confirm Password",
    MOBILE_PLACEHOLDER_TEXT1: "1",
    MOBILE_PLACEHOLDER_TEXT: "Phone Number",
    MOBILE_NUMBER: "Phone Number",
    SEARCH_TEXT: "search",
    TERM_CONDITION_TEXT: "Agree to Terms and Conditions",

    // ----------------------------------------
    // VALIDATION OR API ERROR/SUCCESS MESSAGES
    // ----------------------------------------
    empty_email: "Email field can not be empty.",
    username_email: "Email/Mobile number field can not be empty.",
    name_minimum_three: "Name can not be less then 3 letters",
    empty_password: "Password field can not be empty.",
    empty_confirm_password: "Confirm Password field can not be empty.",
    password_does_not_match: "Password and Confirm Password does not match.",
    enter_valid_email: "Please enter a valid email or phone number.",
    valid_email: "Please enter a valid email.",
    empty_first_name: "First name field can not be empty.",
    empty_last_name: "Last name field can not be empty.",
    empty_city_name: "City name field cannot be empty.",
    empty_state_name: "State name field cannot be empty.",
    empty_country_name: "Country name field can not be empty.",
    enter_valid_password:
        "Password should be strong! Min 8 characters required including 1 upper/lowercase, 1 special & 1 numeric character.",
    ONLY_TEN_IMAGES: "You can select maximum ten images.",
    enter_mobileNo: "Mobile number can not be empty",
    termAndConditionFill: "Please select terms and conditions",
    //==============Hotel owner Register=============================
    empty_owner_name: "Owner first name field can not be empty.",
    empty_owner_hotel_name: "Hotel name field can not be empty.",
    empty_owner_hotel_address: "Hotel address field can not be empty.",
    empty_owner_phone_number: "Phone number field can not be empty.",
    empty_owner_hotel_detail: "Hotel detail field can not be empty.",

    //========================================================
    //================Choose Register Page====================
    //========================================================
    choose_registerAsText: "Register As",
    choose_reg_hotelOwner: "Hotel Owner",
    choose_reg_customer: "Customer",
    //========================================================
    //==================Select Plan Page======================
    //========================================================
    sel_plan_header_title: "Select Plan",
    sel_plan_name: "Plan Name",
    sel_plan_detail:
        "Lorem ipsum dolor sit amet,\n consectetur adipiscing elit, sed \n do eiusmod tempor incididunt ut \n labore et dolore magna aliqua. Ut \n enim ad minim veniam, quis \n nostrud exercitation ullamco \n laboris nisi ut aliquip ex ea \n commodo consequat. Duis aute \n irure dolor in reprehenderit in \n voluptate velit esse cillum dolore \n eu fugiat nulla pariatur.",
    sel_plan_detail1:
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco  laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
    sel_plan_amount: "$150",
    sel_plan_pay_button: "Pay with Stripe",

    //========================================================
    //=================Verification Page======================
    //========================================================
    ver_header_title: "Verify",
    ver_main_text: "Verification",
    ver_detail_text: "OTP Will be Sent to the entered \n Mobile Number",
    ver_button_text: "Resend",
    invalid_otp: "Invalid verification code.",
    session_expired: "Session expired!",
    resend_text: "RESEND",
    enter_otp_text:
        "Please Enter the code that has been sent to your mobile number",

    //==========================================================
    //===================Owner Register Page====================
    //==========================================================
    own_reg_header_title: "Hotel Profile",
    own_reg_name_placeholder: "Owner First Name",
    own_reg_email_placeholder: "Email",
    own_reg_contract_placeholder: "Contact",
    own_reg_hotel_name_placeholder: "Hotel Name",
    own_reg_hotel_address_placeholder: "Hotel Address",
    own_reg_password_placeholder: "Password",
    own_reg_confirm_password_placeholder: "Confirm Password",
    own_reg_next_button: "Next",
    add_room: "Add New Room",

    check_in: "Check In",
    check_out: "Check Out",
    policy: "Policy And Documents",
    addPolicyFile: "Add Policy File",
    view_policy_document: "View Policy Document",
    choose_option: "Do you want to open the PDF",
    //==========================================================
    //===================Customer Profile=======================
    //==========================================================
    cust_profile_header_title: "My Profile",
    cust_profile_first_name_placeholder: "First Name",
    cust_profile_last_name_placeholder: "Last Name",
    cust_profile_email_placeholder: "Email",
    cust_profile_phone_number_placeholder: "Phone Number",
    cust_profile_city_placeholder: "City",
    cust_profile_state_placeholder: "State",
    cust_profile_country_placeholder: "Country",
    cust_profile_button: "Update",

    //=====================================================
    //================Customer Home======================
    //=====================================================
    home_header_title: "Home",

    //=====================================================
    //================Hotel Profile ======================
    //=====================================================

    HOTEL_PROFILE_HEADER: "Hotel Profile",
    CHANGE_PASSWORD: "Change Password",
    UPDATE_BTN: "Update",

    //===============UPdate hotel room detail===============
    HEADER_ROOM_DETAIL: "Room Detail",
    ADD_MORE_ROOMS: "+Add More Rooms",
    near_to_me: "Near To Me",
    hotel_profile: "Hotel Profile",
    no_of_Guests: "No. of Guests",

    //=====================================================
    //================Hotel Owner Home======================
    //=====================================================

    address_string: "Address",
    edit_string: "Edit",
    rooms_amenities: "Rooms & Amenities",
    policy_document: "Policy Document",

    //=====================================================
    //================Hotel Owner Profile==================
    //=====================================================

    owner_profile_header: "Hotel Details",
    change_password: "Change Password",
    update_details: "Update Details",

    //============Owner Change Passsword ==================
    UPDATE_PASSWORD: "Update ",
    HEADER_CHANGE_PASSWORD: "Change Password",

    //============Update Room aminities and services ==================
    HEADER_OWNER_AMENTIES: "Amenities & Services",

    //============Update Policy==================
    HEADER_POLICY_UPDATE: "Policy & Documents",
    HEADER_UPDATE: "Update",

    //============Owner Room List==================
    room_list_header: "Room Listing",

    //============Rating history ==================
    RATING_HEADER: "Rating History",

    //Subscription screen

    SUBSCRIPTION_HEADER: "Subscription Plan",
    PayText: "Pay",

    //============Owner Booking=============
    booking_header_title: 'Bookings',

    //============Confirm Customer===========
    check_in_date: 'Please select check in date.',
    staying_date: 'Please add staying days.',
    confirm_code: 'Please add confirmation code.',
    emojiText:'/\uD83C\uDFF4(?:\uDB40\uDC67\uDB40\uDC62(?:\uDB40\uDC65\uDB40\uDC6E\uDB40\uDC67|\uDB40\uDC77\uDB40\uDC6C\uDB40\uDC73|\uDB40\uDC73\uDB40\uDC63\uDB40\uDC74)\uDB40\uDC7F|\u200D\u2620\uFE0F)|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC68(?:\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D)?\uD83D\uDC68|(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67]))|\uD83D\uDC66\u200D\uD83D\uDC66|\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3]))|\uD83D\uDC69\u200D(?:\u2764\uFE0F\u200D(?:\uD83D\uDC8B\u200D(?:\uD83D[\uDC68\uDC69])|\uD83D[\uDC68\uDC69])|\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|\uD83D\uDC69\u200D\uD83D\uDC66\u200D\uD83D\uDC66|(?:\uD83D\uDC41\uFE0F\u200D\uD83D\uDDE8|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\uD83D\uDC68(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2695\u2696\u2708]|\u200D[\u2695\u2696\u2708])|(?:(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)\uFE0F|\uD83D\uDC6F|\uD83E[\uDD3C\uDDDE\uDDDF])\u200D[\u2640\u2642]|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDD6-\uDDDD])(?:(?:\uD83C[\uDFFB-\uDFFF])\u200D[\u2640\u2642]|\u200D[\u2640\u2642])|\uD83D\uDC69\u200D[\u2695\u2696\u2708])\uFE0F|\uD83D\uDC69\u200D\uD83D\uDC67\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D\uDC69\u200D\uD83D\uDC69\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D\uDC68(?:\u200D(?:(?:\uD83D[\uDC68\uDC69])\u200D(?:\uD83D[\uDC66\uDC67])|\uD83D[\uDC66\uDC67])|\uD83C[\uDFFB-\uDFFF])|\uD83C\uDFF3\uFE0F\u200D\uD83C\uDF08|\uD83D\uDC69\u200D\uD83D\uDC67|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])\u200D(?:\uD83C[\uDF3E\uDF73\uDF93\uDFA4\uDFA8\uDFEB\uDFED]|\uD83D[\uDCBB\uDCBC\uDD27\uDD2C\uDE80\uDE92]|\uD83E[\uDDB0-\uDDB3])|\uD83D\uDC69\u200D\uD83D\uDC66|\uD83C\uDDF6\uD83C\uDDE6|\uD83C\uDDFD\uD83C\uDDF0|\uD83C\uDDF4\uD83C\uDDF2|\uD83D\uDC69(?:\uD83C[\uDFFB-\uDFFF])|\uD83C\uDDED(?:\uD83C[\uDDF0\uDDF2\uDDF3\uDDF7\uDDF9\uDDFA])|\uD83C\uDDEC(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEE\uDDF1-\uDDF3\uDDF5-\uDDFA\uDDFC\uDDFE])|\uD83C\uDDEA(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDED\uDDF7-\uDDFA])|\uD83C\uDDE8(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDEE\uDDF0-\uDDF5\uDDF7\uDDFA-\uDDFF])|\uD83C\uDDF2(?:\uD83C[\uDDE6\uDDE8-\uDDED\uDDF0-\uDDFF])|\uD83C\uDDF3(?:\uD83C[\uDDE6\uDDE8\uDDEA-\uDDEC\uDDEE\uDDF1\uDDF4\uDDF5\uDDF7\uDDFA\uDDFF])|\uD83C\uDDFC(?:\uD83C[\uDDEB\uDDF8])|\uD83C\uDDFA(?:\uD83C[\uDDE6\uDDEC\uDDF2\uDDF3\uDDF8\uDDFE\uDDFF])|\uD83C\uDDF0(?:\uD83C[\uDDEA\uDDEC-\uDDEE\uDDF2\uDDF3\uDDF5\uDDF7\uDDFC\uDDFE\uDDFF])|\uD83C\uDDEF(?:\uD83C[\uDDEA\uDDF2\uDDF4\uDDF5])|\uD83C\uDDF8(?:\uD83C[\uDDE6-\uDDEA\uDDEC-\uDDF4\uDDF7-\uDDF9\uDDFB\uDDFD-\uDDFF])|\uD83C\uDDEE(?:\uD83C[\uDDE8-\uDDEA\uDDF1-\uDDF4\uDDF6-\uDDF9])|\uD83C\uDDFF(?:\uD83C[\uDDE6\uDDF2\uDDFC])|\uD83C\uDDEB(?:\uD83C[\uDDEE-\uDDF0\uDDF2\uDDF4\uDDF7])|\uD83C\uDDF5(?:\uD83C[\uDDE6\uDDEA-\uDDED\uDDF0-\uDDF3\uDDF7-\uDDF9\uDDFC\uDDFE])|\uD83C\uDDE9(?:\uD83C[\uDDEA\uDDEC\uDDEF\uDDF0\uDDF2\uDDF4\uDDFF])|\uD83C\uDDF9(?:\uD83C[\uDDE6\uDDE8\uDDE9\uDDEB-\uDDED\uDDEF-\uDDF4\uDDF7\uDDF9\uDDFB\uDDFC\uDDFF])|\uD83C\uDDE7(?:\uD83C[\uDDE6\uDDE7\uDDE9-\uDDEF\uDDF1-\uDDF4\uDDF6-\uDDF9\uDDFB\uDDFC\uDDFE\uDDFF])|[#\*0-9]\uFE0F\u20E3|\uD83C\uDDF1(?:\uD83C[\uDDE6-\uDDE8\uDDEE\uDDF0\uDDF7-\uDDFB\uDDFE])|\uD83C\uDDE6(?:\uD83C[\uDDE8-\uDDEC\uDDEE\uDDF1\uDDF2\uDDF4\uDDF6-\uDDFA\uDDFC\uDDFD\uDDFF])|\uD83C\uDDF7(?:\uD83C[\uDDEA\uDDF4\uDDF8\uDDFA\uDDFC])|\uD83C\uDDFB(?:\uD83C[\uDDE6\uDDE8\uDDEA\uDDEC\uDDEE\uDDF3\uDDFA])|\uD83C\uDDFE(?:\uD83C[\uDDEA\uDDF9])|(?:\uD83C[\uDFC3\uDFC4\uDFCA]|\uD83D[\uDC6E\uDC71\uDC73\uDC77\uDC81\uDC82\uDC86\uDC87\uDE45-\uDE47\uDE4B\uDE4D\uDE4E\uDEA3\uDEB4-\uDEB6]|\uD83E[\uDD26\uDD37-\uDD39\uDD3D\uDD3E\uDDB8\uDDB9\uDDD6-\uDDDD])(?:\uD83C[\uDFFB-\uDFFF])|(?:\u26F9|\uD83C[\uDFCB\uDFCC]|\uD83D\uDD75)(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u261D\u270A-\u270D]|\uD83C[\uDF85\uDFC2\uDFC7]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66\uDC67\uDC70\uDC72\uDC74-\uDC76\uDC78\uDC7C\uDC83\uDC85\uDCAA\uDD74\uDD7A\uDD90\uDD95\uDD96\uDE4C\uDE4F\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD30-\uDD36\uDDB5\uDDB6\uDDD1-\uDDD5])(?:\uD83C[\uDFFB-\uDFFF])|(?:[\u231A\u231B\u23E9-\u23EC\u23F0\u23F3\u25FD\u25FE\u2614\u2615\u2648-\u2653\u267F\u2693\u26A1\u26AA\u26AB\u26BD\u26BE\u26C4\u26C5\u26CE\u26D4\u26EA\u26F2\u26F3\u26F5\u26FA\u26FD\u2705\u270A\u270B\u2728\u274C\u274E\u2753-\u2755\u2757\u2795-\u2797\u27B0\u27BF\u2B1B\u2B1C\u2B50\u2B55]|\uD83C[\uDC04\uDCCF\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE1A\uDE2F\uDE32-\uDE36\uDE38-\uDE3A\uDE50\uDE51\uDF00-\uDF20\uDF2D-\uDF35\uDF37-\uDF7C\uDF7E-\uDF93\uDFA0-\uDFCA\uDFCF-\uDFD3\uDFE0-\uDFF0\uDFF4\uDFF8-\uDFFF]|\uD83D[\uDC00-\uDC3E\uDC40\uDC42-\uDCFC\uDCFF-\uDD3D\uDD4B-\uDD4E\uDD50-\uDD67\uDD7A\uDD95\uDD96\uDDA4\uDDFB-\uDE4F\uDE80-\uDEC5\uDECC\uDED0-\uDED2\uDEEB\uDEEC\uDEF4-\uDEF9]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD70\uDD73-\uDD76\uDD7A\uDD7C-\uDDA2\uDDB0-\uDDB9\uDDC0-\uDDC2\uDDD0-\uDDFF])|(?:[#*0-9\xA9\xAE\u203C\u2049\u2122\u2139\u2194-\u2199\u21A9\u21AA\u231A\u231B\u2328\u23CF\u23E9-\u23F3\u23F8-\u23FA\u24C2\u25AA\u25AB\u25B6\u25C0\u25FB-\u25FE\u2600-\u2604\u260E\u2611\u2614\u2615\u2618\u261D\u2620\u2622\u2623\u2626\u262A\u262E\u262F\u2638-\u263A\u2640\u2642\u2648-\u2653\u265F\u2660\u2663\u2665\u2666\u2668\u267B\u267E\u267F\u2692-\u2697\u2699\u269B\u269C\u26A0\u26A1\u26AA\u26AB\u26B0\u26B1\u26BD\u26BE\u26C4\u26C5\u26C8\u26CE\u26CF\u26D1\u26D3\u26D4\u26E9\u26EA\u26F0-\u26F5\u26F7-\u26FA\u26FD\u2702\u2705\u2708-\u270D\u270F\u2712\u2714\u2716\u271D\u2721\u2728\u2733\u2734\u2744\u2747\u274C\u274E\u2753-\u2755\u2757\u2763\u2764\u2795-\u2797\u27A1\u27B0\u27BF\u2934\u2935\u2B05-\u2B07\u2B1B\u2B1C\u2B50\u2B55\u3030\u303D\u3297\u3299]|\uD83C[\uDC04\uDCCF\uDD70\uDD71\uDD7E\uDD7F\uDD8E\uDD91-\uDD9A\uDDE6-\uDDFF\uDE01\uDE02\uDE1A\uDE2F\uDE32-\uDE3A\uDE50\uDE51\uDF00-\uDF21\uDF24-\uDF93\uDF96\uDF97\uDF99-\uDF9B\uDF9E-\uDFF0\uDFF3-\uDFF5\uDFF7-\uDFFF]|\uD83D[\uDC00-\uDCFD\uDCFF-\uDD3D\uDD49-\uDD4E\uDD50-\uDD67\uDD6F\uDD70\uDD73-\uDD7A\uDD87\uDD8A-\uDD8D\uDD90\uDD95\uDD96\uDDA4\uDDA5\uDDA8\uDDB1\uDDB2\uDDBC\uDDC2-\uDDC4\uDDD1-\uDDD3\uDDDC-\uDDDE\uDDE1\uDDE3\uDDE8\uDDEF\uDDF3\uDDFA-\uDE4F\uDE80-\uDEC5\uDECB-\uDED2\uDEE0-\uDEE5\uDEE9\uDEEB\uDEEC\uDEF0\uDEF3-\uDEF9]|\uD83E[\uDD10-\uDD3A\uDD3C-\uDD3E\uDD40-\uDD45\uDD47-\uDD70\uDD73-\uDD76\uDD7A\uDD7C-\uDDA2\uDDB0-\uDDB9\uDDC0-\uDDC2\uDDD0-\uDDFF])\uFE0F|(?:[\u261D\u26F9\u270A-\u270D]|\uD83C[\uDF85\uDFC2-\uDFC4\uDFC7\uDFCA-\uDFCC]|\uD83D[\uDC42\uDC43\uDC46-\uDC50\uDC66-\uDC69\uDC6E\uDC70-\uDC78\uDC7C\uDC81-\uDC83\uDC85-\uDC87\uDCAA\uDD74\uDD75\uDD7A\uDD90\uDD95\uDD96\uDE45-\uDE47\uDE4B-\uDE4F\uDEA3\uDEB4-\uDEB6\uDEC0\uDECC]|\uD83E[\uDD18-\uDD1C\uDD1E\uDD1F\uDD26\uDD30-\uDD39\uDD3D\uDD3E\uDDB5\uDDB6\uDDB8\uDDB9\uDDD1-\uDDDD])/g',
 
//=============== Notification And Pramotion=============

 notification_pramotion:'Notification & Promotions'

};
export default STRINGS;
