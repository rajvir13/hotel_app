// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react"
import Svg, {Path, Rect,} from "react-native-svg"
import COLORS from "../../../src/utils/Colors";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function OwnerRoomsListingComponent(props) {
    return (
        <Svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Rect opacity="0.5" width="27" height="27" rx="5" fill="black" fill-opacity="0.5"/>
            <Path d="M17.5271 4.10012C17.3886 4.00122 17.2119 3.9724 17.0518 4.02801L10.8058 6.14515H17.7459V4.52804C17.7459 4.35862 17.6646 4.19954 17.5271 4.10012Z" fill="white"/>
            <Path d="M12.9999 11.4184C11.7264 11.4184 10.6615 12.3258 10.4164 13.5277H15.5835C15.3384 12.3258 14.2735 11.4184 12.9999 11.4184Z" fill="white"/>
            <Path d="M18.2732 7.19981H7.72677C7.43529 7.19981 7.19946 7.43568 7.19946 7.72712V21.4727C7.19946 21.7641 7.43533 22 7.72677 22H18.2732C18.5647 22 18.8005 21.7641 18.8005 21.4727V7.72716C18.8006 7.43568 18.5647 7.19981 18.2732 7.19981ZM14.0546 18.8009H11.9453C11.6539 18.8009 11.418 18.565 11.418 18.2736C11.418 17.9821 11.6539 17.7463 11.9453 17.7463H14.0546C14.3461 17.7463 14.5819 17.9821 14.5819 18.2736C14.582 18.5651 14.3461 18.8009 14.0546 18.8009ZM15.1093 16.6916H10.8907C10.5992 16.6916 10.3634 16.4558 10.3634 16.1643C10.3634 15.8729 10.5993 15.637 10.8907 15.637H15.1093C15.4008 15.637 15.6366 15.8729 15.6366 16.1643C15.6366 16.4558 15.4008 16.6916 15.1093 16.6916ZM16.1639 14.5823H9.83607C9.54459 14.5823 9.30877 14.3465 9.30877 14.055C9.30877 12.2005 10.6886 10.6757 12.4727 10.4171V9.83646C12.4727 9.54498 12.7085 9.30915 13 9.30915C13.2915 9.30915 13.5273 9.54501 13.5273 9.83646V10.4171C15.3114 10.6757 16.6913 12.2004 16.6913 14.055C16.6913 14.3465 16.4554 14.5823 16.1639 14.5823Z" fill="white"/>
        </Svg>
    )
}

OwnerRoomsListingComponent.defaultProps = {
    fillColor: COLORS.white_color,
    width: "24",
    height: "24"
};

export default OwnerRoomsListingComponent
