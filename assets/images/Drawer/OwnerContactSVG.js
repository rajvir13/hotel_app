// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react"
import Svg, {Path, Rect,} from "react-native-svg"
import COLORS from "../../../src/utils/Colors";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function OwnerContactComponent(props) {
    return (
        <Svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Rect opacity="0.5" width="27" height="27" rx="5" fill="black" fill-opacity="0.5"/>
            <Path d="M20.7162 15.5022C19.7366 15.5022 18.7748 15.349 17.8633 15.0478C17.4167 14.8954 16.8676 15.0352 16.595 15.3151L14.796 16.6732C12.7096 15.5595 11.4245 14.2748 10.326 12.204L11.6441 10.4519C11.9865 10.1099 12.1094 9.61029 11.9622 9.14154C11.6597 8.22529 11.506 7.26392 11.506 6.28392C11.5061 5.57596 10.9301 5 10.2222 5H7.28388C6.57596 5 6 5.57596 6 6.28388C6 14.3985 12.6016 21 20.7162 21C21.4241 21 22 20.424 22 19.7161V16.786C22 16.0781 21.424 15.5022 20.7162 15.5022Z" fill="white"/>
        </Svg>
    )
}

OwnerContactComponent.defaultProps = {
    fillColor: COLORS.white_color,
    width: "24",
    height: "24"
};

export default OwnerContactComponent
