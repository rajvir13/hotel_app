// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react"
import Svg, {ClipPath, Defs, G, Path, Rect,} from "react-native-svg"
import COLORS from "../../../src/utils/Colors";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function OwnerHomeComponent(props) {
    return (
        <Svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Rect opacity="0.5" width="27" height="27" rx="5" fill="black" fill-opacity="0.5"/>
            <G clip-path="url(#clip0)">
                <Path d="M21.7147 12.7387L19.1382 10.1623V7.18606C19.1382 6.64792 18.7021 6.2118 18.1632 6.2118C17.6255 6.2118 17.1894 6.64792 17.1894 7.18606V8.21351L15.2711 6.29509C14.3226 5.34715 12.6736 5.34883 11.7273 6.2968L5.28522 12.7387C4.90493 13.1198 4.90493 13.7363 5.28522 14.1168C5.66568 14.4977 6.28343 14.4977 6.66375 14.1168L13.1052 7.67469C13.315 7.46594 13.6849 7.46594 13.8936 7.67407L20.3362 14.1168C20.5272 14.3073 20.7763 14.402 21.0252 14.402C21.2748 14.402 21.5242 14.3072 21.7147 14.1168C22.0951 13.7363 22.0951 13.1198 21.7147 12.7387Z" fill="white"/>
                <Path d="M13.8384 9.52407C13.6513 9.33702 13.3482 9.33702 13.1616 9.52407L7.49516 15.1888C7.4057 15.2783 7.35498 15.4004 7.35498 15.5278V19.6594C7.35498 20.629 8.14109 21.415 9.11058 21.415H11.916V17.0703H15.0834V21.415H17.8889C18.8583 21.415 19.6444 20.629 19.6444 19.6595V15.5278C19.6444 15.4004 19.5941 15.2783 19.5042 15.1888L13.8384 9.52407Z" fill="white"/>
            </G>
            <Defs>
                <ClipPath id="clip0">
                    <Rect width="17" height="17" fill="white" transform="translate(5 5)"/>
                </ClipPath>
            </Defs>
        </Svg>

    )
}

OwnerHomeComponent.defaultProps = {
    fillColor: COLORS.white_color,
    width: "24",
    height: "24"
};

export default OwnerHomeComponent
