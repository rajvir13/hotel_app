// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react"
import Svg, {Path, Rect,} from "react-native-svg"
import COLORS from "../../../src/utils/Colors";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function OwnerHotelDetailComponent(props) {
    return (
        <Svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Rect opacity="0.5" width="27" height="27" rx="5" fill="black" fill-opacity="0.5"/>
            <Path d="M7.40625 14.4062C8.1829 14.4062 8.8125 13.7767 8.8125 13C8.8125 12.2233 8.1829 11.5938 7.40625 11.5938C6.6296 11.5938 6 12.2233 6 13C6 13.7767 6.6296 14.4062 7.40625 14.4062Z" fill="white"/>
            <Path d="M6 15.3438H22V17.2188H6V15.3438Z" fill="white"/>
            <Path d="M19.6562 11.5938H11.1562C9.86391 11.5938 8.8125 12.6452 8.8125 13.9375V14.4062H22V13.9375C22 12.6452 20.9486 11.5938 19.6562 11.5938Z" fill="white"/>
            <Path d="M6 20.0312H6.9375V19.0938H21.0625V20.0312H22V18.1562H6V20.0312Z" fill="white"/>
            <Path d="M13.7103 8.78125H16.3438V7.84375H15.2272L16.1647 5.96875H13.5312V6.90625H14.6478L13.7103 8.78125Z" fill="white"/>
            <Path d="M9.96028 10.6562H12.5938V9.71875H11.4772L12.4147 7.84375H9.78125V8.78125H10.8978L9.96028 10.6562Z" fill="white"/>
        </Svg>
    )
}

OwnerHotelDetailComponent.defaultProps = {
    fillColor: COLORS.white_color,
    width: "24",
    height: "24"
};

export default OwnerHotelDetailComponent
