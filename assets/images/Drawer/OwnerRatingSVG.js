// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react"
import Svg, {Path, Rect,} from "react-native-svg"
import COLORS from "../../../src/utils/Colors";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function OwnerRatingComponent(props) {
    return (
        <Svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Rect opacity="0.5" width="27" height="27" rx="5" fill="black" fill-opacity="0.5"/>
            <Path d="M14 6C9.60596 6 6 9.60596 6 14C6 18.394 9.60596 22 14 22C15.3999 22 16.7782 21.6301 17.9852 20.9302L17.5146 20.119C16.4504 20.7361 15.235 21.0625 14 21.0625C10.1228 21.0625 6.9375 17.8772 6.9375 14C6.9375 10.1228 10.1228 6.9375 14 6.9375C17.8772 6.9375 21.0625 10.1228 21.0625 14C21.0625 15.2469 20.7362 16.4646 20.125 17.5225V16.375H19.1875V19.1875H22V18.25H20.7805C21.5679 16.9897 22 15.493 22 14C22 9.60596 18.394 6 14 6Z" fill="white"/>
            <Path d="M13.8541 8.66666C11.011 8.66666 8.66663 11.011 8.66663 13.8542C8.66663 16.6973 11.011 19.0417 13.8541 19.0417C16.6973 19.0417 19.0416 16.6973 19.0416 13.8542C19.0416 11.011 16.6973 8.66666 13.8541 8.66666ZM17.1354 14.3229H13.3854V10.5417H14.3229V13.3854H17.1354V14.3229Z" fill="white"/>
        </Svg>
    )
}

OwnerRatingComponent.defaultProps = {
    fillColor: COLORS.white_color,
    width: "24",
    height: "24"
};

export default OwnerRatingComponent
