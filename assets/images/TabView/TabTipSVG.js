// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react"
import Svg, {Path} from "react-native-svg"
import COLORS from "../../../src/utils/Colors";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function TabTipSVG(props) {
    return (
        <Svg width="25" height="14" viewBox="0 0 25 14" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Path d="M12.5 14L24.1913 0.5H0.808657L12.5 14Z" fill={props.fillColor}/>
        </Svg>
    )
}

TabTipSVG.defaultProps = {
    fillColor:'#E3E3E3',
    width: "24",
    height: "24"
};

export default TabTipSVG
