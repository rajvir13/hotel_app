// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react"
import Svg, {Path, Rect,} from "react-native-svg"
import COLORS from "../../../src/utils/Colors";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function LocationIconSVG(props) {
    return (
        <Svg width={props.width} height={props.height} viewBox="0 0 10 12" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Path d="M5.00024 0C2.60388 0 0.654297 1.90104 0.654297 4.23772C0.654297 7.13762 4.54349 11.3948 4.70908 11.5747C4.86461 11.7436 5.13616 11.7433 5.29141 11.5747C5.45699 11.3948 9.34619 7.13762 9.34619 4.23772C9.34614 1.90104 7.39659 0 5.00024 0ZM5.00024 6.36984C3.79457 6.36984 2.81371 5.41338 2.81371 4.23772C2.81371 3.06207 3.79459 2.10563 5.00024 2.10563C6.20589 2.10563 7.18675 3.06209 7.18675 4.23775C7.18675 5.4134 6.20589 6.36984 5.00024 6.36984Z" fill={props.fillColor}/>
        </Svg>

    )
}

LocationIconSVG.defaultProps = {
    fillColor: COLORS.app_theme_color,
    width: "24",
    height: "24"
};

export default LocationIconSVG
