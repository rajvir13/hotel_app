// ----------------------------------------
// PACKAGE IMPORTS
// ----------------------------------------

import React from "react"
import Svg, {ClipPath, Defs, G, Path, Rect,} from "react-native-svg"
import COLORS from "../../../src/utils/Colors";

// ----------------------------------------
// LOCAL & CONFIG IMPORTS
// ----------------------------------------

function BellSVG(props) {
    return (
        <Svg width="18" height="20" viewBox="0 0 18 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <Path d="M8.99998 20C10.1333 20 11.0513 19.0821 11.0513 17.9487H6.94873C6.94868 19.0821 7.86666 20 8.99998 20Z" fill="#5A5A5A"/>
            <Path d="M15.1539 13.8462V8.71793C15.1539 5.56409 13.477 2.93332 10.5385 2.23587V1.53846C10.5385 0.687163 9.85136 0 9.00006 0C8.14877 0 7.4616 0.687163 7.4616 1.53846V2.23591C4.52314 2.93337 2.84622 5.56413 2.84622 8.71798V13.8462L0.794922 15.8975V16.9231H17.2052V15.8975L15.1539 13.8462Z" fill="#5A5A5A"/>
        </Svg>

    )
}

BellSVG.defaultProps = {
    fillColor: COLORS.white_color,
    width: "24",
    height: "24"
};

export default BellSVG
